/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import puppeteer from 'puppeteer';
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer';
import { request } from 'http';
import path from 'path';

const chromePath = process.env.CHROME_BIN;

const getBrowserUrl = (): Promise<string> =>
  new Promise((resolve, reject) => {
    const req = request(
      {
        method: 'GET',
        hostname: 'localhost',
        port: 9222,
        path: '/json/version',
      },
      (res) => {
        res.on('data', (d) => {
          resolve(JSON.parse(d).webSocketDebuggerUrl);
        });
      },
    );
    req.on('error', (error: Error) => reject(error));
    req.end();
  });

let browser: puppeteer.Browser;
const viewport = { width: 1920, height: 1080 };

const customizePage = (page: puppeteer.Page): Promise<void> =>
  page.setViewport(viewport);

const beforeScreenshot = async (
  page: puppeteer.Page,
): Promise<void | puppeteer.ElementHandle<Element>> => {
  await page.evaluateHandle('document.fonts.ready');
  await page.setViewport(viewport);
  await page.waitForTimeout(2000); // allow time for mounting animations
  const component = await page.$('#root > *'); // This selector creates a screenshot from the entire component, not limited to the viewport
  return component as void | puppeteer.ElementHandle<Element>;
};

const getScreenshotOptions = (): { encoding: string; fullPage: boolean } => {
  return {
    encoding: 'base64',
    fullPage: false, // needs to be turned off to be able to use the element selector in beforeScreenshot
  };
};

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const localTest = (lib: string) => {
  const testInstance = imageSnapshot({
    getCustomBrowser: async (): Promise<puppeteer.Browser> => {
      const url = await getBrowserUrl().catch((error: Error) => {
        throw error;
      });
      const webBrowser = await puppeteer.connect({
        browserWSEndpoint: url,
        defaultViewport: viewport,
      });
      browser = webBrowser;
      return webBrowser;
    },
    storybookUrl: `file:///usr/src/dist/storybook/${lib}`,
    customizePage,
    beforeScreenshot,
    getScreenshotOptions,
  });
  testInstance.afterAll = async (): Promise<void> =>
    browser ? browser.disconnect() : (null as unknown as void);
  return testInstance;
};

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const pipelineTest = (lib: string) =>
  imageSnapshot({
    chromeExecutablePath: chromePath,
    storybookUrl: `file://${path.resolve(__dirname, `dist/storybook/${lib}`)}`,
    customizePage,
    beforeScreenshot,
    getScreenshotOptions,
  });
