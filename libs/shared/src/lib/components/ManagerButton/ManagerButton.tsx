/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  IconButton,
  IconButtonProps,
  Theme,
  Tooltip,
  SxProps,
} from '@mui/material';

const makeButtonStyles = (
  isActive: boolean,
  isEnabled: boolean,
  isAccessible: boolean,
): SxProps<Theme> => ({
  height: '24px',
  width: '24px',
  borderRadius: '5px',
  marginTop: '4px',
  color: (): string => {
    if (!isEnabled) {
      return 'geowebColors.buttons.flat.disabled.color';
    }
    if (isAccessible) {
      return 'geowebColors.greys.accessible';
    }
    if (isActive) {
      return 'common.white';
    }

    return 'geowebColors.buttons.flat.default.color';
  },
  backgroundColor: (): string =>
    isActive ? 'geowebColors.buttons.primary.active.fill' : 'inherit',
  '&:focus': {
    backgroundColor: (): string =>
      isActive
        ? 'geowebColors.buttons.primary.mouseOver.fill'
        : 'geowebColors.buttons.flat.mouseOver.fill',
    outline: 'geowebColors.buttons.focusDefault.outline',
    borderRadius: 0,
  },
  '&:hover': {
    backgroundColor: (): string =>
      isActive
        ? 'geowebColors.buttons.primary.mouseOver.fill'
        : 'geowebColors.buttons.flat.mouseOver.fill',
  },
});

export interface ManagerButtonProps extends IconButtonProps {
  tooltipTitle: string;
  onClick?: (event: React.MouseEvent) => void;
  isActive?: boolean;
  isEnabled?: boolean;
  icon: React.ReactNode;
  testId: string;
  isAccessible?: boolean;
}

export const ManagerButton: React.FC<ManagerButtonProps> = ({
  tooltipTitle = '',
  onClick = (): void => {},
  isActive = false,
  isEnabled = true,
  icon,
  testId,
  isAccessible = false,
  sx,
  ...props
}: ManagerButtonProps) => {
  const buttonStyles = makeButtonStyles(isActive, isEnabled, isAccessible);

  const handleClick = (event): void => {
    onClick(event);
  };

  return (
    <Tooltip
      title={tooltipTitle}
      placement="top"
      sx={{
        pointerEvents: 'none',
      }}
    >
      <IconButton
        size="small"
        onClick={handleClick}
        data-testid={testId}
        sx={{ ...buttonStyles, ...sx } as SxProps<Theme>}
        disableFocusRipple
        {...props}
      >
        {icon}
      </IconButton>
    </Tooltip>
  );
};
