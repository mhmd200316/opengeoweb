/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';

import { ThemeWrapper } from '@opengeoweb/theme';
import { ManagerButton, ManagerButtonProps } from './ManagerButton';

describe('components/Manager/ManagerButton', () => {
  it('should work with default props', async () => {
    const props: ManagerButtonProps = {
      tooltipTitle: 'test title',
      testId: 'test-1',
      icon: <div className="my-test-icon">test icon</div>,
      onClick: jest.fn(),
    };

    const { container, getByTestId } = render(
      <ThemeWrapper>
        <ManagerButton {...props} />
      </ThemeWrapper>,
    );

    const button = getByTestId(props.testId);

    expect(button).toBeTruthy();
    expect(props.onClick).not.toHaveBeenCalled();
    fireEvent.click(button);
    expect(props.onClick).toHaveBeenCalled();
    expect(button.getAttribute('aria-label')).toEqual(props.tooltipTitle);
    expect(container.querySelector('.my-test-icon')).toBeTruthy();
  });

  it('should show as disabled but still be clickable', async () => {
    const props: ManagerButtonProps = {
      tooltipTitle: 'test title',
      testId: 'test-1',
      icon: <div className="my-test-icon">test icon</div>,
      onClick: jest.fn(),
      isEnabled: false,
    };

    const { getByTestId } = render(
      <ThemeWrapper>
        <ManagerButton {...props} />
      </ThemeWrapper>,
    );

    const button = getByTestId(props.testId);
    expect(button).toBeTruthy();
    const styles = getComputedStyle(button);
    expect(styles.color).toEqual('rgb(112, 112, 112)');
    expect(props.onClick).not.toHaveBeenCalled();
    fireEvent.click(button);
    expect(props.onClick).toHaveBeenCalled();
  });

  it('should show as accessible', async () => {
    const props: ManagerButtonProps = {
      tooltipTitle: 'test title',
      testId: 'test-1',
      icon: <div className="my-test-icon">test icon</div>,
      onClick: jest.fn(),
      isAccessible: true,
    };

    const { getByTestId } = render(
      <ThemeWrapper>
        <ManagerButton {...props} />
      </ThemeWrapper>,
    );

    const button = getByTestId(props.testId);
    expect(button).toBeTruthy();
    const styles = getComputedStyle(button);
    expect(styles.color).toEqual('rgb(118, 118, 118)');
  });

  it('should show as active', async () => {
    const props: ManagerButtonProps = {
      tooltipTitle: 'test title',
      testId: 'test-1',
      icon: <div className="my-test-icon">test icon</div>,
      onClick: jest.fn(),
      isActive: true,
    };

    const { getByTestId } = render(
      <ThemeWrapper>
        <ManagerButton {...props} />
      </ThemeWrapper>,
    );

    const button = getByTestId(props.testId);
    expect(button).toBeTruthy();
    const styles = getComputedStyle(button);
    expect(styles.color).toEqual('rgb(255, 255, 255)');
    expect(styles.backgroundColor).toEqual('rgb(24, 109, 255)');
  });
});
