/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { darkTheme, lightTheme, ThemeWrapper, Expand } from '@opengeoweb/theme';
import { Grid } from '@mui/material';
import { ManagerButton } from './ManagerButton';

export default { title: 'components/Manager/ManagerButton' };

const StoryManagerButton: React.FC<{
  text: string;
}> = ({ text, children }) => {
  return (
    <Grid item>
      <b style={{ fontSize: '0.875rem' }}>{text}</b>
      <br />
      {children}
    </Grid>
  );
};

const StoryButtons: React.FC = () => (
  <div style={{ margin: '10px', padding: '20px 0px', width: '600px' }}>
    <Grid container spacing={5}>
      <StoryManagerButton text="Enabled">
        <ManagerButton
          icon={<Expand />}
          testId=""
          tooltipTitle="Expand"
          isEnabled={true}
        />
      </StoryManagerButton>
      <StoryManagerButton text="Disabled">
        <ManagerButton
          icon={<Expand />}
          testId=""
          tooltipTitle="Expand"
          isEnabled={false}
        />
      </StoryManagerButton>
      <StoryManagerButton text="Active">
        <ManagerButton
          icon={<Expand />}
          testId=""
          tooltipTitle="Expand"
          isActive={true}
        />
      </StoryManagerButton>
      <StoryManagerButton text="Inactive">
        <ManagerButton
          icon={<Expand />}
          testId=""
          tooltipTitle="Expand"
          isActive={false}
        />
      </StoryManagerButton>
      <StoryManagerButton text="Accessible">
        <ManagerButton
          icon={<Expand />}
          testId=""
          tooltipTitle="Expand"
          isAccessible={true}
        />
      </StoryManagerButton>
      <StoryManagerButton text="Inaccessible">
        <ManagerButton
          icon={<Expand />}
          testId=""
          tooltipTitle="Expand"
          isAccessible={false}
        />
      </StoryManagerButton>
    </Grid>
  </div>
);

export const ManagerButtonLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <StoryButtons />
  </ThemeWrapper>
);

export const ManagerButtonDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <StoryButtons />
  </ThemeWrapper>
);

ManagerButtonLight.storyName = 'Manager Button Light (takeSnapshot)';
ManagerButtonDark.storyName = 'Manager Button Dark (takeSnapshot)';
