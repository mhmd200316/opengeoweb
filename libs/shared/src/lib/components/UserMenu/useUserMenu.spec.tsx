/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { renderHook } from '@testing-library/react-hooks';
import { fireEvent, render } from '@testing-library/react';
import { useUserMenu } from './useUserMenu';

describe('components/UserMenu/useUserMenu', () => {
  it('should work with default props', async () => {
    const {
      result: { current },
    } = renderHook(() => useUserMenu());

    expect(current.isOpen).toBeFalsy();
    expect(current.onClose).toBeDefined();
    expect(current.onToggle).toBeDefined();
  });

  it('should be able to provide params', async () => {
    const {
      result: { current },
    } = renderHook(() => useUserMenu(true));

    expect(current.isOpen).toBeTruthy();
    expect(current.onClose).toBeDefined();
    expect(current.onToggle).toBeDefined();
  });

  it('should toggle and close', () => {
    const TestComponent: React.FC = () => {
      const { isOpen, onClose, onToggle } = useUserMenu();
      return (
        <div>
          <span data-testid="title">{isOpen ? 'open' : 'closed'}</span>
          <button data-testid="closeBtn" type="button" onClick={onClose}>
            close button
          </button>
          <button data-testid="toggleBtn" type="button" onClick={onToggle}>
            toggle button
          </button>
        </div>
      );
    };
    const { getByTestId } = render(<TestComponent />);

    const closeBtn = getByTestId('closeBtn');
    const toggleBtn = getByTestId('toggleBtn');
    const title = getByTestId('title');

    expect(title.innerHTML).toEqual('closed');
    // open it
    fireEvent.click(toggleBtn);
    expect(title.innerHTML).toEqual('open');
    // close it
    fireEvent.click(closeBtn);
    expect(title.innerHTML).toEqual('closed');
  });
});
