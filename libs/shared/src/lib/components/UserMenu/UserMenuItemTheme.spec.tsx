/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import UserMenuItemTheme from './UserMenuItemTheme';

interface TestWrapperProps {
  children?: React.ReactNode;
}
const TestWrapper: React.FC<TestWrapperProps> = ({
  children,
}: TestWrapperProps) => <ThemeWrapper>{children}</ThemeWrapper>;

describe('components/UserMenu/UserMenuItemTheme', () => {
  it('should change theming', async () => {
    const { findByText } = render(
      <TestWrapper>
        <UserMenuItemTheme />
      </TestWrapper>,
    );

    const lightElement = await findByText('Light');
    const darkElement = await findByText('Dark');
    expect(lightElement.parentElement.querySelector('input').checked).toEqual(
      true,
    );
    expect(darkElement.parentElement.querySelector('input').checked).toEqual(
      false,
    );

    // toggle
    await waitFor(() => {
      fireEvent.click(darkElement.parentElement.querySelector('input'));
      expect(lightElement.parentElement.querySelector('input').checked).toEqual(
        false,
      );
      expect(darkElement.parentElement.querySelector('input').checked).toEqual(
        true,
      );
    });
  });
});
