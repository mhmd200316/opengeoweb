/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { MenuItem } from '@mui/material';
import { ThemeWrapper } from '@opengeoweb/theme';
import { getInitials } from './utils';
import UserMenu from './UserMenu';

interface TestWrapperProps {
  children?: React.ReactNode;
}
const TestWrapper: React.FC<TestWrapperProps> = ({
  children,
}: TestWrapperProps) => <ThemeWrapper>{children}</ThemeWrapper>;

describe('components/UserMenu/UserMenu', () => {
  it('should work with default props', async () => {
    const { queryByTestId } = render(
      <TestWrapper>
        <UserMenu />
      </TestWrapper>,
    );

    expect(queryByTestId('userButton')).toBeTruthy();
    expect(queryByTestId('userMenu')).toBeFalsy();
  });

  it('should work with props', async () => {
    const props = {
      isOpen: true,
      onToggle: jest.fn(),
      onClose: jest.fn(),
      initials: 'JD',
      children: <MenuItem>Test</MenuItem>,
    };

    const { queryByTestId, findByText } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    expect(queryByTestId('userButton')).toBeTruthy();
    expect(queryByTestId('userMenu')).toBeTruthy();
    expect(await findByText('Test')).toBeTruthy();
    expect(await findByText(props.initials)).toBeTruthy();

    fireEvent.click(queryByTestId('userButton'));

    expect(props.onToggle).toHaveBeenCalled();
  });

  it('should return one or two initials depending on the name', () => {
    expect(getInitials('hello')).toEqual('H');
    expect(getInitials('daan.storm')).toEqual('DS');
    expect(getInitials('daan.storm.wind.regen')).toEqual('DR');
    expect(getInitials('daan.storm@gmail.com')).toEqual('DS');
    expect(getInitials('daan52@gmail.com')).toEqual('D');
  });

  it('should return an empty string when name is empty', () => {
    expect(getInitials('')).toEqual('');
    expect(getInitials(null)).toEqual('');
  });
});
