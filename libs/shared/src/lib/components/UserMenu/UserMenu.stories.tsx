/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import { MenuItem, Paper } from '@mui/material';
import { UserMenuItemTheme, useUserMenu, UserMenu } from '.';
import UserMenuItemCheatSheet from './UserMenuItemCheatSheet';

export default {
  title: 'components/UserMenu',
};

const DemoUserMenu: React.FC = () => {
  const { isOpen, onClose, onToggle } = useUserMenu(true);

  return (
    <UserMenu
      isOpen={isOpen}
      onClose={onClose}
      onToggle={onToggle}
      initials="JD"
    >
      {/** TOOD: add ListItemText with primary and secondary https://gitlab.com/opengeoweb/opengeoweb/-/issues/1162 */}
      <MenuItem selected onClick={onClose} divider>
        Profile
      </MenuItem>
      <UserMenuItemTheme />
      <UserMenuItemCheatSheet />
      <MenuItem onClick={onClose}>Logout</MenuItem>
    </UserMenu>
  );
};

export const ThemeLight = (): React.ReactElement => (
  <ThemeWrapper>
    <Paper>
      <DemoUserMenu />
    </Paper>
  </ThemeWrapper>
);

// theme switch not representative for application
export const ThemeDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <Paper>
      <DemoUserMenu />
    </Paper>
  </ThemeWrapper>
);

export const ThemeLightSnapShot = (): React.ReactElement => (
  <ThemeWrapper>
    <div style={{ position: 'relative', height: 600, width: 400 }}>
      <DemoUserMenu />
    </div>
  </ThemeWrapper>
);
ThemeLightSnapShot.storyName = 'UserMenu theme light (takeSnapshot)';

export const ThemeDarkSnapShot = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <div style={{ position: 'relative', height: 600, width: 400 }}>
      <DemoUserMenu />
    </div>
  </ThemeWrapper>
);
ThemeDarkSnapShot.storyName = 'UserMenu theme dark (takeSnapshot)';
