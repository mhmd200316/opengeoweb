/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { ClickAwayListener, MenuList, Paper, IconButton } from '@mui/material';
import React from 'react';
import { Avatar } from '../Avatar';

const styles = {
  container: {
    position: 'absolute',
    top: 38,
    right: 20,
    width: 256,
    borderRadius: 0,
    boxShadow: 5,
    backgroundColor: 'geowebColors.background.surface',
  },
  userMenu: {
    padding: 0,
    '& > li': {
      paddingTop: 3,
      paddingBottom: 3,
      paddingLeft: 2,
      paddingRight: 2,
    },
  },
  button: {
    color: 'common.white',
    padding: 0,
  },
};

interface UserMenuProps {
  isOpen?: boolean;
  onToggle?: () => void;
  onClose?: () => void;
  children?: React.ReactNode;
  initials?: string;
}

const UserMenu: React.FC<UserMenuProps> = ({
  isOpen = false,
  onToggle = (): void => null,
  onClose = (): void => null,
  children,
  initials,
}: UserMenuProps) => {
  return (
    <>
      <IconButton
        data-testid="userButton"
        onClick={onToggle}
        sx={styles.button}
        size="large"
      >
        <Avatar>{initials}</Avatar>
      </IconButton>

      {isOpen && (
        <Paper sx={styles.container} data-testid="userMenu">
          <ClickAwayListener onClickAway={onClose} mouseEvent="onMouseDown">
            <MenuList sx={styles.userMenu} variant="menu">
              {children}
            </MenuList>
          </ClickAwayListener>
        </Paper>
      )}
    </>
  );
};

export default UserMenu;
