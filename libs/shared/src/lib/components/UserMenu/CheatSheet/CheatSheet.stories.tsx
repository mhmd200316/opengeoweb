/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Link, Paper, Typography } from '@mui/material';
import CheatSheet from './CheatSheet';

export default {
  title: 'components/UserMenu/CheatSheet',
};

interface LinkToZeplinProps {
  link: string;
}

const LinkToZeplin: React.FC<LinkToZeplinProps> = ({
  link,
}: LinkToZeplinProps) => (
  <Typography variant="body1">
    <Link href={link} target="_blank">{`Zeplin design ${link}`}</Link>
  </Typography>
);

export const ThemeLight = (): React.ReactElement => (
  <ThemeWrapper>
    <Paper>
      <CheatSheet />
    </Paper>
    <LinkToZeplin link="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/612e4a4915e8a712cb4610f6" />
  </ThemeWrapper>
);

export const ThemeDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <Paper>
      <CheatSheet />
    </Paper>
    <LinkToZeplin
      link="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/612e4a49fce91411cdf4fa7d
"
    />
  </ThemeWrapper>
);

export const ThemeLightSnapShot = (): React.ReactElement => (
  <div style={{ width: 800 }}>
    <ThemeWrapper>
      <CheatSheet />
    </ThemeWrapper>
  </div>
);
ThemeLightSnapShot.storyName = 'CheatSheet light (takeSnapshot)';

export const ThemeDarkSnapShot = (): React.ReactElement => (
  <div style={{ width: 800 }}>
    <ThemeWrapper theme={darkTheme}>
      <CheatSheet />
    </ThemeWrapper>
  </div>
);
ThemeDarkSnapShot.storyName = 'CheatSheet dark (takeSnapshot)';
