/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Box, Grid, List, ListItem } from '@mui/material';
import {
  ArrowDown,
  ArrowLeft,
  ArrowRight,
  ArrowUp,
  KeyCommand,
  KeyOption,
} from '@opengeoweb/theme';
import ShortcutSegment from './ShortcutSegment';

type ShortCut = {
  title: string;
  description: React.ReactNode;
  descriptionMac?: React.ReactNode;
};

type Platform = 'windows' | 'mac';

export const shortcuts: ShortCut[] = [
  {
    title: 'Choose a selected time',
    description: (
      <ShortcutSegment>Left click on the time slider rail</ShortcutSegment>
    ),
  },

  {
    title: 'Move backwards/forwards in selected time',
    description: (
      <ShortcutSegment>Mouse scroll over the time slider rail</ShortcutSegment>
    ),
  },
  {
    title: 'Move 1 step backwards/forwards in selected time',
    description: (
      <>
        <ShortcutSegment type="icon">Ctrl</ShortcutSegment>
        <ShortcutSegment type="connect">+</ShortcutSegment>
        <ShortcutSegment type="icon">
          <ArrowLeft />
        </ShortcutSegment>
        <ShortcutSegment type="divider">or</ShortcutSegment>
        <ShortcutSegment type="icon">
          <ArrowRight />
        </ShortcutSegment>
      </>
    ),
    descriptionMac: (
      <>
        <ShortcutSegment type="icon">Ctrl</ShortcutSegment>
        <ShortcutSegment type="connect">+</ShortcutSegment>
        <ShortcutSegment type="icon">
          <KeyOption />
        </ShortcutSegment>
        <ShortcutSegment type="connect">+</ShortcutSegment>
        <ShortcutSegment type="icon">
          <ArrowLeft />
        </ShortcutSegment>
        <ShortcutSegment type="divider">or</ShortcutSegment>
        <ShortcutSegment type="icon">
          <ArrowRight />
        </ShortcutSegment>
      </>
    ),
  },
  {
    title: 'Toggle hide/show time slider',
    description: <ShortcutSegment type="icon">t</ShortcutSegment>,
  },
  {
    title: 'Change time slider scale (visible timespan)',
    description: (
      <ShortcutSegment>
        Hold left click over Time Slider legend + drag left or right
      </ShortcutSegment>
    ),
  },
  {
    title: 'Expand/shrink time slider scale (visible timespan)',
    description: (
      <>
        <ShortcutSegment type="icon">Ctrl</ShortcutSegment>
        <ShortcutSegment>+ Scroll the slider rail</ShortcutSegment>
      </>
    ),
  },
  {
    title: 'Mouseover to change selected time',
    description: (
      <ShortcutSegment>
        Hover over Time Slider rail or time indicator
      </ShortcutSegment>
    ),
  },
  {
    title: 'Mouseover switch on/off',
    description: (
      <>
        <ShortcutSegment type="icon">Ctrl</ShortcutSegment>
        <ShortcutSegment type="connect">+</ShortcutSegment>
        <ShortcutSegment type="icon">Alt</ShortcutSegment>
        <ShortcutSegment type="connect">+</ShortcutSegment>
        <ShortcutSegment type="icon">h</ShortcutSegment>
      </>
    ),
    descriptionMac: (
      <>
        <ShortcutSegment type="icon">Ctrl</ShortcutSegment>
        <ShortcutSegment type="connect">+</ShortcutSegment>
        <ShortcutSegment type="icon">
          <KeyOption />
        </ShortcutSegment>
        <ShortcutSegment type="connect">+</ShortcutSegment>
        <ShortcutSegment type="icon">h</ShortcutSegment>
      </>
    ),
  },
  {
    title: 'Change time step or animation speed',
    description: (
      <>
        <ShortcutSegment type="icon">Ctrl</ShortcutSegment>
        <ShortcutSegment type="connect">+</ShortcutSegment>
        <ShortcutSegment type="icon">
          <ArrowUp />
        </ShortcutSegment>
        <ShortcutSegment type="divider">or</ShortcutSegment>
        <ShortcutSegment type="icon">
          <ArrowDown />
        </ShortcutSegment>
        <ShortcutSegment>(when slider active)</ShortcutSegment>
      </>
    ),
    descriptionMac: (
      <>
        <ShortcutSegment type="icon">
          <KeyCommand />
        </ShortcutSegment>
        <ShortcutSegment type="connect">+</ShortcutSegment>
        <ShortcutSegment type="icon">
          <ArrowUp />
        </ShortcutSegment>
        <ShortcutSegment type="divider">or</ShortcutSegment>
        <ShortcutSegment type="icon">
          <ArrowDown />
        </ShortcutSegment>
        <ShortcutSegment>(when slider active)</ShortcutSegment>
      </>
    ),
  },
  {
    title: 'Return to current time',
    description: <ShortcutSegment type="icon">Home</ShortcutSegment>,
    descriptionMac: (
      <>
        <ShortcutSegment type="icon">fn</ShortcutSegment>
        <ShortcutSegment type="connect">+</ShortcutSegment>
        <ShortcutSegment type="icon">
          <ArrowLeft />
        </ShortcutSegment>
      </>
    ),
  },
  {
    title: 'Change layer manager values by scrolling',
    description: (
      <>
        <ShortcutSegment type="icon">Ctrl</ShortcutSegment>
        <ShortcutSegment> + Mouse scroll</ShortcutSegment>
      </>
    ),
    descriptionMac: (
      <>
        <ShortcutSegment type="icon">
          <KeyCommand />
        </ShortcutSegment>
        <ShortcutSegment> + Mouse scroll</ShortcutSegment>
      </>
    ),
  },
];

export const getShortcutList = (
  shortcutList: ShortCut[],
  platform: Platform = 'windows',
): ShortCut[] =>
  shortcutList.map(({ title, description, descriptionMac }) => ({
    title,
    description,
    ...(platform === 'windows'
      ? { description }
      : { description: descriptionMac || description }),
  }));

interface ShortcutListProps {
  platform?: Platform;
}

const ShortcutList: React.FC<ShortcutListProps> = ({
  platform = 'windows',
}: ShortcutListProps) => {
  const rows = getShortcutList(shortcuts, platform);

  return (
    <List data-testid={`list-${platform}`}>
      {rows.map((row, index) => (
        <ListItem
          sx={{ minHeight: 56 }}
          divider={index !== rows.length - 1}
          key={row.title}
        >
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="center"
          >
            <Grid
              item
              sx={{
                fontSize: 12,
                fontWeight: 300,
                letterSpacing: 0.4,
                opacity: 0.67,
              }}
            >
              {row.title}
            </Grid>
            <Grid item>
              <Box
                sx={{
                  alignItems: 'center',
                }}
              >
                {row.description}
              </Box>
            </Grid>
          </Grid>
        </ListItem>
      ))}
    </List>
  );
};

export default ShortcutList;
