/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import CheatSheet from './CheatSheet';

describe('components/UserMenu/CheatSheet/CheatSheet', () => {
  it('should work with default props', async () => {
    const { container } = render(
      <ThemeWrapper>
        <CheatSheet />
      </ThemeWrapper>,
    );

    expect(container.querySelector('[aria-label="platform"]')).toBeTruthy();
  });

  it('should change tabs', async () => {
    const { container } = render(
      <ThemeWrapper>
        <CheatSheet />
      </ThemeWrapper>,
    );
    expect(
      container.querySelector('[aria-selected="true"]').innerHTML,
    ).toContain('Windows');
    // change to mac
    fireEvent.click(container.querySelector('[aria-selected="false"]'));
    expect(
      container.querySelector('[aria-selected="true"]').innerHTML,
    ).toContain('Mac');
  });
});
