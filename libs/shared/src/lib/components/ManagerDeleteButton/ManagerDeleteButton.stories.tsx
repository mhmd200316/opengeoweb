/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { darkTheme, lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Grid } from '@mui/material';
import { ManagerDeleteButton } from './ManagerDeleteButton';

export default { title: 'components/Manager/ManagerDeleteButton' };

const StoryManagerButton: React.FC<{
  text: string;
}> = ({ text, children }) => {
  return (
    <Grid item>
      <b style={{ fontSize: '0.875rem' }}>{text}</b>
      <br />
      {children}
    </Grid>
  );
};

const StoryButtons = (): React.ReactElement => (
  <div style={{ margin: '10px', padding: '20px 0px', width: '600px' }}>
    <Grid container spacing={5}>
      <StoryManagerButton text="Enabled">
        <ManagerDeleteButton
          onClickDelete={(): void => {}}
          tooltipTitle="Delete"
          isEnabled={true}
        />
      </StoryManagerButton>

      <StoryManagerButton text="Disabled">
        <ManagerDeleteButton
          onClickDelete={(): void => {}}
          tooltipTitle="Delete"
          isEnabled={false}
        />
      </StoryManagerButton>
    </Grid>
  </div>
);

export const ManagerDeleteButtonLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <StoryButtons />
  </ThemeWrapper>
);

export const ManagerDeleteButtonDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <StoryButtons />
  </ThemeWrapper>
);

ManagerDeleteButtonLight.storyName =
  'Manager Delete Button Light (takeSnapshot)';
ManagerDeleteButtonDark.storyName = 'Manager Delete Button Dark (takeSnapshot)';
