/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import { CustomToggleButton } from './CustomToggleButton';

const svgIcon: React.ReactElement = <svg data-testid="test-svg" />;

describe('components/CustomToggleButton', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <ThemeWrapper>
        <CustomToggleButton>{svgIcon}</CustomToggleButton>,
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
  });

  it('should render a passed svg element', () => {
    const { getByTestId } = render(
      <ThemeWrapper>
        <CustomToggleButton>{svgIcon}</CustomToggleButton>,
      </ThemeWrapper>,
    );
    expect(getByTestId('test-svg')).toBeTruthy();
  });

  it('should render passed in text', () => {
    const { getByText } = render(
      <ThemeWrapper>
        <CustomToggleButton>testText</CustomToggleButton>
      </ThemeWrapper>,
    );
    expect(getByText('testText')).toBeTruthy();
  });

  it('should be able to pass a variant', () => {
    const variant = 'tertiary';
    const { getByRole } = render(
      <ThemeWrapper>
        <CustomToggleButton variant={variant} />
      </ThemeWrapper>,
    );
    expect(getByRole('button').classList).toContain(variant);
  });
});
