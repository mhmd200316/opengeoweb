/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import { ErrorBoundary } from './ErrorBoundary';

// mock window.location
const storedWindowLocation = window.location;
delete window.location;
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
window.location = {
  reload: (): void => {},
};
afterAll(() => {
  window.location = storedWindowLocation;
});

const TestComponent: React.FC<Record<string, unknown>> = () => {
  const [hasError, triggerError] = React.useState(false);

  if (hasError) {
    throw new Error('Triggered error');
  }
  return (
    <button
      type="button"
      data-testid="test"
      onClick={(): void => triggerError(true)}
    >
      Click to trigger error
    </button>
  );
};

describe('components/ErrorBoundary/ErrorBoundary', () => {
  it('should render with default props', () => {
    const { getByTestId } = render(
      <ErrorBoundary>
        <TestComponent />
      </ErrorBoundary>,
    );

    expect(getByTestId('test')).toBeTruthy();
  });

  it('should handle errors', () => {
    // mute errors in console
    const spy = jest.spyOn(console, 'error');
    spy.mockImplementation(() => null);

    const props = {
      onError: jest.fn(),
    };
    const { getByText, getByTestId } = render(
      <ThemeWrapper>
        <ErrorBoundary {...props}>
          <TestComponent />
        </ErrorBoundary>
      </ThemeWrapper>,
    );

    // trigger error
    fireEvent.click(getByTestId('test'));

    expect(
      getByText(
        'Woops, an error has occurred. Please report the error shown below and reload the page if needed.',
      ),
    ).toBeTruthy();
    expect(props.onError).toHaveBeenCalled();

    // restore mute errors in console
    spy.mockRestore();
  });

  it('should reload the page', () => {
    // mute errors in console
    const spy = jest.spyOn(console, 'error');
    spy.mockImplementation(() => null);

    jest.spyOn(window.location, 'reload');

    const props = {
      onError: jest.fn(),
    };
    const { getByText, getByTestId } = render(
      <ThemeWrapper>
        <ErrorBoundary {...props}>
          <TestComponent />
        </ErrorBoundary>
      </ThemeWrapper>,
    );

    // trigger error
    fireEvent.click(getByTestId('test'));
    expect(props.onError).toHaveBeenCalled();

    expect(getByText('Reload page')).toBeTruthy();
    fireEvent.click(getByText('Reload page'));
    expect(window.location.reload).toHaveBeenCalled();

    // restore mute errors in console
    spy.mockRestore();
  });
});
