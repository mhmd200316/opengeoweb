/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  Menu,
  MenuItem,
  ListItemIcon,
  Typography,
  PopoverOrigin,
  SxProps,
  Theme,
  Tooltip,
} from '@mui/material';
import { Options, ButtonVariant } from '@opengeoweb/theme';
import { ToolButton } from '../ToolButton';

type MenuPosition = 'left' | 'right' | 'bottom';

export const getPosition = (
  menuPosition: MenuPosition,
): { anchorOrigin: PopoverOrigin; transformOrigin: PopoverOrigin } => {
  switch (menuPosition) {
    case 'bottom':
      return {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center',
        },
        transformOrigin: {
          vertical: 'top',
          horizontal: 'center',
        },
      };
    default:
    case 'left':
      return {
        anchorOrigin: {
          vertical: 'center',
          horizontal: 'left',
        },
        transformOrigin: {
          vertical: 'center',
          horizontal: 'right',
        },
      };

    case 'right':
      return {
        anchorOrigin: {
          vertical: 'center',
          horizontal: 'right',
        },
        transformOrigin: {
          vertical: 'center',
          horizontal: 'left',
        },
      };
  }
};

type MenuItemType = {
  text: string;
  action: (event: React.MouseEvent) => void;
  icon?: React.ReactNode;
};

interface ToggleMenuProps {
  buttonTestId?: string;
  menuPosition?: MenuPosition;
  menuItems?: MenuItemType[];
  children?: React.ReactNode;
  buttonSx?: SxProps<Theme>;
  tooltipTitle?: string;
  variant?: ButtonVariant;
  isDefaultOpen?: boolean;
}

export const ToggleMenu: React.FC<ToggleMenuProps> = ({
  buttonTestId = 'toggleMenuButton',
  menuPosition = 'left',
  menuItems,
  children,
  buttonSx,
  tooltipTitle = '',
  variant,
  isDefaultOpen = false,
}: ToggleMenuProps) => {
  const [anchorEl, setAnchorEl] = React.useState<HTMLInputElement>(null);

  const isOpen = Boolean(anchorEl);
  const { anchorOrigin, transformOrigin } = getPosition(menuPosition);

  const handleClick = (event: React.MouseEvent<HTMLInputElement>): void => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (event: React.MouseEvent): void => {
    event.stopPropagation();
    setAnchorEl(null);
  };

  React.useEffect(() => {
    if (isDefaultOpen) {
      setAnchorEl(document.getElementById(buttonTestId) as HTMLInputElement);
    }
  }, [isDefaultOpen, buttonTestId]);

  return (
    <>
      <Tooltip placement="top" title={tooltipTitle}>
        <ToolButton
          active={isOpen}
          sx={buttonSx}
          onClick={handleClick}
          data-testid={buttonTestId}
          variant={variant}
          id={buttonTestId}
        >
          <Options />
        </ToolButton>
      </Tooltip>
      <Menu
        anchorEl={anchorEl}
        open={isOpen}
        onClose={handleClose}
        anchorOrigin={anchorOrigin}
        transformOrigin={transformOrigin}
        BackdropProps={{
          style: {
            opacity: 0,
            transition: 'none',
          },
        }}
        MenuListProps={
          children && {
            style: {
              padding: '0px',
            },
          }
        }
      >
        {menuItems &&
          menuItems.map((menuItem) => {
            const { text, icon, action }: MenuItemType = menuItem;
            return (
              <MenuItem
                key={text}
                onClick={(event: React.MouseEvent): void => {
                  action(event);
                  handleClose(event);
                }}
              >
                {icon && <ListItemIcon>{icon}</ListItemIcon>}
                <Typography>{text}</Typography>
              </MenuItem>
            );
          })}
        {children}
      </Menu>
    </>
  );
};
