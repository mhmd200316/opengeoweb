/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import { getPosition, ToggleMenu } from './ToggleMenu';

describe('components/ToggleMenu', () => {
  it('should render successfully', () => {
    const { getByTestId } = render(
      <ThemeWrapper>
        <ToggleMenu />,
      </ThemeWrapper>,
    );
    expect(getByTestId('toggleMenuButton')).toBeTruthy();
  });

  it('should render passed in children', () => {
    const { getByText, getByTestId } = render(
      <ThemeWrapper>
        <ToggleMenu menuPosition="bottom">
          <div>testText</div>
        </ToggleMenu>
      </ThemeWrapper>,
    );
    fireEvent.click(getByTestId('toggleMenuButton'));
    expect(getByText('testText')).toBeTruthy();
  });

  it('should render passed in menu items', () => {
    const svgIcon: React.ReactElement = <svg data-testid="test-svg" />;

    const { getByText, getByTestId } = render(
      <ThemeWrapper>
        <ToggleMenu
          menuItems={[
            { text: 'first', action: jest.fn(), icon: svgIcon },
            { text: 'second', action: jest.fn() },
          ]}
        />
      </ThemeWrapper>,
    );
    fireEvent.click(getByTestId('toggleMenuButton'));
    expect(getByText('first')).toBeTruthy();
    expect(getByText('second')).toBeTruthy();
    expect(getByTestId('test-svg')).toBeTruthy();
  });

  it('should toggle the menu when clicking a menu item', async () => {
    const mockAction = jest.fn();
    const { getByTestId, queryByText } = render(
      <ThemeWrapper>
        <ToggleMenu menuItems={[{ text: 'first', action: mockAction }]} />,
      </ThemeWrapper>,
    );
    expect(queryByText('first')).toBeFalsy();
    fireEvent.click(getByTestId('toggleMenuButton'));
    expect(queryByText('first')).toBeTruthy();
    fireEvent.click(queryByText('first'));
    await waitFor(() => {
      expect(queryByText('first')).toBeFalsy();
      expect(mockAction).toHaveBeenCalled();
    });
  });

  it('should be able to pass variant', () => {
    const testVariant = 'tool';
    const { getByTestId } = render(
      <ThemeWrapper>
        <ToggleMenu
          variant={testVariant}
          menuItems={[{ text: 'second', action: jest.fn() }]}
        />
      </ThemeWrapper>,
    );
    const button = getByTestId('toggleMenuButton');

    expect(button.className).toContain(testVariant);
  });

  it('should show and hide tooltip', async () => {
    const props = {
      onClick: jest.fn(),
      isHidden: false,
      tooltipTitle: 'example toggle menu tooltip',
      menuItems: [{ text: 'second', action: jest.fn() }],
    };
    const { queryByTestId, queryByText } = render(
      <ThemeWrapper>
        <ToggleMenu {...props} />
      </ThemeWrapper>,
    );
    const button = queryByTestId('toggleMenuButton');
    expect(button).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(queryByText(props.tooltipTitle)).toBeFalsy();

    fireEvent.mouseOver(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(queryByText(props.tooltipTitle)).toBeTruthy();

    fireEvent.mouseOut(button);
    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeFalsy();
      expect(queryByText(props.tooltipTitle)).toBeFalsy();
    });
  });

  describe('getPosition', () => {
    it('should return correct position for left', () => {
      expect(getPosition('left')).toEqual({
        anchorOrigin: {
          vertical: 'center',
          horizontal: 'left',
        },
        transformOrigin: {
          vertical: 'center',
          horizontal: 'right',
        },
      });
    });

    it('should return correct position for right', () => {
      expect(getPosition('right')).toEqual({
        anchorOrigin: {
          vertical: 'center',
          horizontal: 'right',
        },
        transformOrigin: {
          vertical: 'center',
          horizontal: 'left',
        },
      });
    });

    it('should return correct position for bottom', () => {
      expect(getPosition('bottom')).toEqual({
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center',
        },
        transformOrigin: {
          vertical: 'top',
          horizontal: 'center',
        },
      });
    });
  });
});
