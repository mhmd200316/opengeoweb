/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  darkTheme,
  lightTheme,
  ThemeWrapper,
  ThemeWrapperOldTheme,
} from '@opengeoweb/theme';

import AlertBanner from './AlertBanner';

export default {
  title: 'components/AlertBanner',
};

const AlertBanners = (): React.ReactElement => {
  return (
    <div style={{ width: '500px' }}>
      <AlertBanner
        title="An error has occurred while saving, please try again"
        info="Unable to store data."
      />
      <br />
      <AlertBanner
        title="There are 2 unhandled notification triggers"
        severity="warning"
      />
      <br />
      <AlertBanner
        title="There are no available bulletins from the last 24 hours"
        severity="info"
        info="For historical bulletins please see the forecast history list"
      />
      <br />
      <AlertBanner
        title="The product was saved successfully"
        severity="success"
      />
      <br />
      <AlertBanner
        title="This layer could not be loaded: does not exist on the server. Long text is clipped."
        isCompact
        severity="error"
      />
      <br />
      <AlertBanner title="Notification" shouldClose />
      <br />
      <AlertBanner
        title="Notification"
        info="Some more lines when addional information is needed."
        shouldClose
      />
      <br />
      <AlertBanner
        title="Notification"
        info="Some more lines when addional information is needed."
        shouldClose
        actionButtonProps={{
          onClick: (): void => null,
          title: 'TAKE ACTION',
        }}
      />
      <br />
      <AlertBanner
        title="An error has occurred while retrieving the list"
        actionButtonProps={{
          onClick: (): void => null,
          title: 'Try again',
        }}
      />
      <br />
      <AlertBanner
        severity="info"
        title="Auto-update in 00:35"
        actionButtonProps={{
          onClick: (): void => null,
          title: 'Auto-update now',
        }}
        shouldClose
      />
    </div>
  );
};

export const AlertBannersGWTheme = (): React.ReactElement => (
  <ThemeWrapperOldTheme>
    <AlertBanners />
  </ThemeWrapperOldTheme>
);

export const AlertBannersLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <AlertBanners />
  </ThemeWrapper>
);

export const AlertBannersDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <AlertBanners />
  </ThemeWrapper>
);

AlertBannersGWTheme.storyName = 'AlertBanners (takeSnapshot)';
AlertBannersLight.storyName = 'AlertBanners Light (takeSnapshot)';
AlertBannersDark.storyName = 'AlertBanners Dark (takeSnapshot)';
