/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { AlertColor } from '@mui/material';

import { ThemeWrapper } from '@opengeoweb/theme';
import AlertBanner from './AlertBanner';

describe('components/AlertBanner', () => {
  it('should contain the passed title', () => {
    const props = {
      title: 'An error occurred while saving',
    };
    const { getByTestId } = render(
      <ThemeWrapper>
        <AlertBanner {...props} />
      </ThemeWrapper>,
    );
    expect(getByTestId('alert-banner').textContent).toEqual(props.title);
    expect(getByTestId('alert-banner').classList).toContain(
      'MuiAlert-standardError',
    );
  });

  it('should contain the passed text 2', () => {
    const props = {
      title: 'There is a notification',
      severity: 'warning' as AlertColor,
      dataTestId: 'banner-warning',
      info: 'Some more details here.',
    };
    const { getByTestId } = render(
      <ThemeWrapper>
        <AlertBanner {...props} />
      </ThemeWrapper>,
    );
    expect(getByTestId(props.dataTestId).textContent).toEqual(
      props.title + props.info,
    );
    expect(getByTestId(props.dataTestId).classList).toContain(
      'MuiAlert-standardWarning',
    );
  });

  it('should not show close or action buttons by default', () => {
    const props = {
      title: 'An error occurred while saving',
    };
    const { queryByText, queryByRole } = render(<AlertBanner {...props} />);
    expect(queryByText('CLOSE')).toBeFalsy();
    expect(queryByRole('button')).toBeFalsy();
  });

  it('should close the alert when clicking CLOSE', async () => {
    const props = {
      title: 'An error occurred while saving',
      shouldClose: true,
    };
    const { queryByText } = render(<AlertBanner {...props} />);
    expect(queryByText('CLOSE')).toBeTruthy();
    fireEvent.click(queryByText('CLOSE'));
    await waitFor(() => expect(queryByText('CLOSE')).toBeFalsy());
  });

  it('should trigger the passed action when clicked', () => {
    const props = {
      title: 'An error occurred while saving',
      actionButtonProps: {
        title: 'take action',
        onClick: jest.fn(),
      },
    };
    const { queryByText, queryByRole } = render(<AlertBanner {...props} />);
    expect(queryByRole('button')).toBeTruthy();
    fireEvent.click(queryByText(props.actionButtonProps.title.toUpperCase()));
    expect(props.actionButtonProps.onClick).toHaveBeenCalledTimes(1);
    expect(queryByRole('button')).toBeFalsy();
  });
});
