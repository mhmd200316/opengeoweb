/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Resizable, Size } from 're-resizable';

import { Resize } from '@opengeoweb/theme';
import { useTheme } from '@mui/material';
import { ResizableComponentProps } from './types';

const ResizeComponent: React.FC<ResizableComponentProps> = ({
  startPosition = { left: null, right: null, top: null },
  children,
  onResizeStart = (): void => null,
  size,
  setSize,
  minHeight = 50,
  minWidth = 50,
  ...props
}: ResizableComponentProps) => {
  const theme = useTheme();
  const isRightAligned =
    startPosition &&
    startPosition.right !== null &&
    startPosition.right !== undefined;

  const onStartResize = React.useCallback(
    (event, _direction, node: HTMLElement) => {
      event.stopPropagation();
      const { width, height } = node.getBoundingClientRect();
      setSize({ width, height });
      // fix for when initial maxHeight gets cleared
      // eslint-disable-next-line no-param-reassign
      node.style.height = `${height}px`;
      onResizeStart();
    },
    [onResizeStart, setSize],
  );

  const onResizeStop = React.useCallback(
    (_event, _direction, _ref, delta): void => {
      const width = parseInt(size.width as string, 10) + delta.width;
      const height = parseInt(size.height as string, 10) + delta.height;

      setSize({
        width,
        height,
      });
    },
    [setSize, size],
  );

  return (
    <Resizable
      // TODO: implement bounds https://gitlab.com/opengeoweb/opengeoweb/-/issues/1035
      minHeight={minHeight}
      minWidth={minWidth}
      size={size as Size}
      onResizeStop={onResizeStop}
      handleComponent={{
        bottomRight: (
          <Resize
            style={{
              fill: `${theme.palette.geowebColors.buttons.flat.default.color}`,
            }}
          />
        ),
        bottomLeft: (
          <Resize
            isRightAligned
            style={{
              fill: `${theme.palette.geowebColors.buttons.flat.default.color}`,
            }}
          />
        ),
      }}
      enable={{
        top: false,
        right: false,
        bottom: false,
        left: false,
        topRight: false,
        bottomRight: !isRightAligned,
        bottomLeft: isRightAligned,
        topLeft: false,
      }}
      onResizeStart={onStartResize}
      {...props}
    >
      {children}
    </Resizable>
  );
};

export default ResizeComponent;
