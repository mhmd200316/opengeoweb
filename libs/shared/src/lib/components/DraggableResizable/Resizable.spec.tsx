/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import Resizable from './Resizable';

describe('components/Resizable', () => {
  it('should render successfully', async () => {
    const { baseElement, findByText } = render(
      <ThemeWrapper>
        <Resizable startPosition={{ right: 100, top: 100 }}>
          <div>test</div>
        </Resizable>
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(await findByText('test')).toBeTruthy();
  });
  it('should handle resize', async () => {
    const props = {
      startPosition: { right: 100, top: 40 },
      onResizeStart: jest.fn(),
      size: { width: 50, height: 200 },
      setSize: jest.fn(),
    };

    const { findByTestId } = render(
      <ThemeWrapper>
        <Resizable {...props}>
          <div>test</div>
        </Resizable>
      </ThemeWrapper>,
    );
    const resizeHandle = await findByTestId('resizeHandle');
    fireEvent.mouseDown(resizeHandle);
    expect(props.onResizeStart).toHaveBeenCalledTimes(1);
    expect(props.setSize).toHaveBeenLastCalledWith({ width: 0, height: 0 });
    fireEvent.mouseUp(resizeHandle);
    expect(props.setSize).toHaveBeenLastCalledWith(props.size);
    expect(props.setSize).toHaveBeenCalledTimes(2);
  });
});
