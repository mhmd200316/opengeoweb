/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import Draggable from 'react-draggable';

type Position = { x: number; y: number };

interface DraggableComponentProps {
  nodeRef?: React.RefObject<HTMLElement>;
  children?: React.ReactNode;
  bounds?: 'parent' | string;
  position?: Position;
  onStop?: (event: React.MouseEvent<HTMLElement>, pos: Position) => void;
  onMouseDown?: (event: MouseEvent) => void;
  handle?: string;
}

const DraggableComponent: React.FC<DraggableComponentProps> = ({
  bounds = 'body',
  children,
  ...props
}: DraggableComponentProps) => {
  return (
    <Draggable bounds={bounds} {...props}>
      {children}
    </Draggable>
  );
};

export default DraggableComponent;
