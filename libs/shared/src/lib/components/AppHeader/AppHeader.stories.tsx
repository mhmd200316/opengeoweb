/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  darkTheme,
  lightTheme,
  ThemeWrapper,
  HamburgerMenu,
} from '@opengeoweb/theme';
import { Theme, IconButton } from '@mui/material';
import { AppHeader } from '.';
import { UserMenu } from '../UserMenu';

export default {
  title: 'components/AppHeader',
};

const Menu = (): React.ReactElement => (
  <IconButton style={{ padding: 0 }} data-testid="menuButton" size="large">
    <HamburgerMenu style={{ color: 'white' }} />
  </IconButton>
);

interface AppHeaderDemoProps {
  theme?: Theme;
}

const AppHeaderDemo = ({
  theme = lightTheme,
}: AppHeaderDemoProps): React.ReactElement => (
  <div>
    <ThemeWrapper theme={theme}>
      <AppHeader title="GeoWeb" />
    </ThemeWrapper>
    <br />
    <ThemeWrapper theme={theme}>
      <AppHeader
        title="GeoWeb"
        menu={<Menu />}
        userMenu={<UserMenu initials="VL" />}
      />
    </ThemeWrapper>
  </div>
);

export const AppHeaderDark = (): React.ReactElement => (
  <AppHeaderDemo theme={darkTheme} />
);

export const AppHeaderLight = (): React.ReactElement => <AppHeaderDemo />;

AppHeaderDark.storyName = 'App Header Dark (takeSnapshot)';
AppHeaderLight.storyName = 'App Header Light (takeSnapshot)';
