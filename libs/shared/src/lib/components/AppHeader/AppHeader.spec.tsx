/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';

import { ThemeWrapper } from '@opengeoweb/theme';
import AppHeader from './AppHeader';

interface TestComponentProps {
  title: string;
}

const TestComponent: React.FC<TestComponentProps> = ({
  title,
}: TestComponentProps) => <div>{title}</div>;

describe('components/AppHeader', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <ThemeWrapper>
        <AppHeader />
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
  });

  it('should render different layout parts', async () => {
    const props = {
      menu: <TestComponent title="menu" />,
      userMenu: <TestComponent title="userMenu" />,
      title: 'test title',
    };
    const { findByText } = render(
      <ThemeWrapper>
        <AppHeader {...props} />
      </ThemeWrapper>,
    );

    const menu = await findByText('menu');
    const title = await findByText(props.title);
    const userMenu = await findByText('userMenu');

    expect(menu).toBeTruthy();
    expect(title).toBeTruthy();
    expect(userMenu).toBeTruthy();
  });
});
