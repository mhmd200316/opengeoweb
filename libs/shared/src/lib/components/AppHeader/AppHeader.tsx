/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { Grid, Typography, Box, SxProps, Theme } from '@mui/material';
import React from 'react';
import { GeoWebLogo } from '@opengeoweb/theme';

const styles = {
  header: (theme: Theme): SxProps<Theme> => ({
    height: 40,
    backgroundColor: theme.palette.geowebColors.brand.brand,
    color: theme.palette.common.white,
    boxShadow: theme.shadows[1],
    zIndex: 1000,
    [theme.breakpoints.down('sm')]: {
      padding: '0 8px',
    },
    [theme.breakpoints.up('sm')]: {
      padding: '0 24px',
    },
  }),
  title: {
    marginLeft: 1,
    fontSize: '1.25rem',
    fontWeight: 400,
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    letterSpacing: '0.15px',
  },
  container: {
    height: '100%',
  },
};

interface AppHeaderProps {
  menu?: React.ReactNode;
  userMenu?: React.ReactNode;
  title?: string;
}

const AppHeader: React.FC<AppHeaderProps> = ({
  menu,
  userMenu,
  title = 'GeoWeb',
}: AppHeaderProps) => {
  return (
    <Box component="header" sx={styles.header as SxProps<Theme>}>
      <Grid sx={styles.container} container alignItems="center">
        <Grid item xs={1} sm={3}>
          <Grid container justifyContent="flex-start">
            {menu}
          </Grid>
        </Grid>
        <Grid item xs={10} sm={6}>
          <Grid
            container
            justifyContent="center"
            alignItems="center"
            wrap="nowrap"
          >
            <GeoWebLogo />
            <Typography variant="h1" sx={styles.title}>
              {title}
            </Typography>
          </Grid>
        </Grid>
        <Grid item xs={1} sm={3} container justifyContent="flex-end">
          {userMenu}
        </Grid>
      </Grid>
    </Box>
  );
};

export default AppHeader;
