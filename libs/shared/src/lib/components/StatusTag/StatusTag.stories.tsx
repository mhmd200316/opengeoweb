/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid } from '@mui/material';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import StatusTag from './StatusTag';

export default {
  title: 'components/StatusTag',
};

export const StatusTags = (): React.ReactElement => (
  <ThemeWrapperOldTheme>
    <div style={{ width: 800, padding: '20px 0px' }}>
      <Grid container spacing={5}>
        <Grid item>
          <StatusTag content="All good" color="green" />
          <br />
          <StatusTag content="Active" color="green" />
        </Grid>
        <Grid item>
          <StatusTag content="Alert" color="red" />
          <br />
          <StatusTag content="Cancelled" color="red" />
        </Grid>
        <Grid item>
          <StatusTag content="Inactive" color="grey" />
          <br />
          <StatusTag content="Expired" color="grey" />
        </Grid>
        <Grid item>
          <StatusTag content="Watch" color="yellow" />
          <br />
          <StatusTag content="Warning" color="yellow" />
        </Grid>
        <Grid item>
          <StatusTag content="Summary" color="purple" />
        </Grid>
      </Grid>
    </div>
  </ThemeWrapperOldTheme>
);

StatusTags.storyName = 'StatusTag (takeSnapshot)';
