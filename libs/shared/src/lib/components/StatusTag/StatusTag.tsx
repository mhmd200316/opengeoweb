/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box, Typography } from '@mui/material';

const bgColors = {
  green: '#fcffeb',
  grey: '#f8f9f9',
  purple: '#f8f9f9',
  red: '#f9e8ea',
  yellow: '#fff8eb',
};

const borderColors = {
  green: '#72bb23',
  grey: '#9b9fb0',
  purple: '#9d9df2',
  red: '#f24a00',
  yellow: '#ffa800',
};

const tagStyle = (color: string): Record<string, unknown> => {
  // Set color to grey for summary
  const bgcolor = bgColors[color];
  const borderColor = borderColors[color];

  return {
    backgroundColor: bgcolor,
    m: 0,
    p: 0.5,
    border: `2px solid ${borderColor}`,
    borderRadius: '4px',
    width: '85px',
    height: '20px',
    color: '#051039',
    maxHeight: '1em',
    boxSizing: 'content-box',
  };
};

interface StatusTagProps {
  content: string;
  color: string;
}

const StatusTag: React.FC<StatusTagProps> = ({
  content,
  color,
}: StatusTagProps) => {
  return (
    <Box
      display="flex"
      alignItems="center"
      justifyContent="center"
      sx={{ ...tagStyle(color) }}
      data-testid="statusTag"
    >
      <Typography variant="caption">{content}</Typography>
    </Box>
  );
};

export default StatusTag;
