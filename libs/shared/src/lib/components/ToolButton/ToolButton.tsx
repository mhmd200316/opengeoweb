/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Theme, SxProps } from '@mui/material';
import {
  CustomToggleButton,
  CustomToggleButtonProps,
} from '../CustomToggleButton';

export interface ToolButtonProps extends CustomToggleButtonProps {
  active?: boolean;
  width?: string;
  height?: string;
  value?: unknown;
  sx?: SxProps<Theme>;
}

export const ToolButton: React.FC<ToolButtonProps> = React.forwardRef(
  (
    {
      active,
      width = '24px',
      height = '24px',
      children,
      value = 0,
      sx = {},
      variant = 'tool',
      ...props
    }: ToolButtonProps,
    ref,
  ) => {
    return (
      <CustomToggleButton
        ref={ref}
        variant={variant}
        disableRipple
        selected={active}
        value={value}
        sx={
          {
            width: `${width}!important`,
            height: `${height}!important`,
            textTransform: 'none!important',
            padding: '11px!important',
            '& .MuiSvgIcon-root': {
              marginRight: '0px!important',
            },
            ...sx,
          } as SxProps<Theme>
        }
        {...props}
      >
        {children}
      </CustomToggleButton>
    );
  },
);
