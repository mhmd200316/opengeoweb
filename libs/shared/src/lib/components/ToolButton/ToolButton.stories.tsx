/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, Typography, Box, Tooltip } from '@mui/material';
import {
  darkTheme,
  lightTheme,
  ThemeWrapper,
  Options,
  TextIcon,
  Icons as IconList,
} from '@opengeoweb/theme';
import { ToolButton } from './ToolButton';

export default { title: 'components/ToolButton' };

const Row: React.FC = ({ children }) => (
  <Box sx={{ width: '100%', button: { margin: '10px 10px 10px 0' } }}>
    {children}
  </Box>
);

interface SingleToolButtonProps {
  text: string;
  content: React.ReactElement;
}

const SingleToolButton: React.FC<SingleToolButtonProps> = ({
  text,
  content,
}: SingleToolButtonProps) => {
  return (
    <Grid item>
      <b style={{ fontSize: '0.875rem' }}>{text}</b>
      <br />
      {content}
    </Grid>
  );
};

const ToolButtonsContent = (): React.ReactElement => (
  <>
    <Grid container spacing={5}>
      <SingleToolButton
        text="normal"
        content={
          <ToolButton>
            <Options />
          </ToolButton>
        }
      />
      <SingleToolButton
        text="active"
        content={
          <ToolButton active>
            <Options />
          </ToolButton>
        }
      />
      <SingleToolButton
        text="disabled"
        content={
          <ToolButton disabled>
            <Options />
          </ToolButton>
        }
      />
      <SingleToolButton
        text="normal"
        content={
          <ToolButton width="60px">
            <Options />
            <TextIcon text="abc" />
          </ToolButton>
        }
      />
      <SingleToolButton
        text="active"
        content={
          <ToolButton active width="60px">
            <Options />
            <TextIcon text="abc" />
          </ToolButton>
        }
      />
      <SingleToolButton
        text="disabled"
        content={
          <ToolButton disabled width="60px">
            <Options />
            <TextIcon text="abc" />
          </ToolButton>
        }
      />
    </Grid>

    <Typography sx={{ marginTop: 3, fontSize: '1.6rem' }}>All Icons</Typography>
    <Row>
      {Object.values(IconList).map((Icon) => (
        <Tooltip key={Icon.name} title={Icon.name}>
          <ToolButton variant="tertiary">
            <Icon />
          </ToolButton>
        </Tooltip>
      ))}
    </Row>
  </>
);

export const ToolButtonLight = (): React.ReactElement => (
  <div style={{ margin: '10px', padding: '20px 0px', width: '600px' }}>
    <ThemeWrapper theme={lightTheme}>
      <ToolButtonsContent />
    </ThemeWrapper>
  </div>
);
export const ToolButtonDark = (): React.ReactElement => (
  <div style={{ margin: '10px', padding: '20px 0px', width: '600px' }}>
    <ThemeWrapper theme={darkTheme}>
      <ToolButtonsContent />
    </ThemeWrapper>
  </div>
);

ToolButtonLight.storyName = 'Tool Button Light (takeSnapshot)';
ToolButtonDark.storyName = 'Tool Button Dark (takeSnapshot)';
