/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Box, Grid, Link, TextField, Typography } from '@mui/material';
import { ToolContainer } from '.';

export default {
  title: 'components/ToolContainer',
  includeStories: ['ThemeLight', 'ThemeDark'],
};

interface ToolContainerDemoProps {
  isSnapShot?: boolean;
}

export const ToolContainerDemo: React.FC<ToolContainerDemoProps> = ({
  isSnapShot = false,
}: ToolContainerDemoProps) => {
  // eslint-disable-next-line no-console
  const onClose = (): void => console.log('onClose');

  const maxHeight = isSnapShot ? null : 200;
  return (
    <Grid container>
      <Grid item xs={12}>
        <div>
          <ToolContainer style={{ marginBottom: 50, width: 500 }} size="xxs">
            <Typography>xxs header</Typography>
          </ToolContainer>

          <ToolContainer
            style={{ marginBottom: 50, width: 500 }}
            size="xs"
            title="xs header"
            onClose={onClose}
            elevation={1}
          >
            <Typography>low elevation</Typography>
          </ToolContainer>

          <ToolContainer
            style={{ marginBottom: 50, width: 600, maxHeight }}
            size="small"
            title="small header"
            isResizable
            onClose={onClose}
          >
            <div style={{ padding: 12 }}>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
              <Typography>padding maxheight content</Typography>
            </div>
          </ToolContainer>

          <ToolContainer
            style={{ marginBottom: 50, width: 700 }}
            size="medium"
            title="medium header"
            onClose={onClose}
            isResizable
            isDraggable
            elevation={10}
          >
            <Typography>with footer and high elevation</Typography>
          </ToolContainer>

          <ToolContainer
            style={{ marginBottom: 50, width: 800 }}
            size="large"
            title="large header"
            onClose={onClose}
          >
            <Typography>no footer</Typography>
          </ToolContainer>

          <ToolContainer
            style={{ marginBottom: 50, width: 700 }}
            size="medium"
            title="medium custom leftComponent"
            onClose={onClose}
            isResizable
            leftComponent={
              <TextField
                InputProps={{ disableUnderline: true }}
                value="Some dummy text"
                size="small"
                sx={{
                  '& .MuiFilledInput-root': {
                    height: '24px',
                    fontSize: 11,
                  },
                  '& .MuiFilledInput-input': {
                    height: '100%',
                    paddingTop: 0,
                    paddingBottom: 0,
                  },
                }}
              />
            }
          >
            <Typography>with custom leftComponent</Typography>
          </ToolContainer>
        </div>
      </Grid>
    </Grid>
  );
};

interface ToolContainerDemoStoryProps {
  linkToZeplinDesign?: string;
}

const ToolContainerDemoStory: React.FC<ToolContainerDemoStoryProps> = ({
  linkToZeplinDesign,
}: ToolContainerDemoStoryProps) => {
  return (
    <Box sx={{ padding: '20px' }}>
      <Grid container direction="row">
        {/* Content */}
        <ToolContainerDemo />
        {/* Footer */}
        <Grid container>
          <Grid item xs={9}>
            {linkToZeplinDesign && (
              <Typography variant="body1">
                <Link
                  href={linkToZeplinDesign}
                  target="_blank"
                >{`design (uses layer manager for content and header): ${linkToZeplinDesign}`}</Link>
              </Typography>
            )}
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};

export const ThemeLight = (): React.ReactElement => (
  <ThemeWrapper>
    <ToolContainerDemoStory linkToZeplinDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60992c1ecde3bf10bec429d2" />
  </ThemeWrapper>
);

export const ThemeDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <ToolContainerDemoStory linkToZeplinDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6058ba63e21b5d181e3f01df" />
  </ThemeWrapper>
);
