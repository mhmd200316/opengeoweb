/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { throttle } from 'lodash';
import React from 'react';
import Draggable from '../DraggableResizable/Draggable';
import Resizable from '../DraggableResizable/Resizable';
import { ResizableComponentProps, Size } from '../DraggableResizable/types';
import ToolContainer, { BASE_ELEVATION } from './ToolContainer';
import { HeaderSize } from './types';

// calculate dialog size and position to 85% of screensize
export const calculateDialogSizeAndPosition = (): {
  width: number;
  height: number;
  top: number;
  left: number;
} => {
  const width = window.innerWidth * 0.85;
  const height = window.innerHeight * 0.85;
  const top = (window.innerHeight * 0.15) / 2;
  const left = (window.innerWidth * 0.15) / 2;
  return { width, height, top, left };
};

interface DraggableComponentProps
  extends Omit<ResizableComponentProps, 'bounds'> {
  onStartResize?: () => void;
  isOpen?: boolean;
  onClose?: () => void;
  title?: string;
  headerSize?: HeaderSize;
  initialMaxHeight?: string | number;
  onChangeSize?: (size: Size) => void;
  onMouseDown?: () => void;
  bounds?: 'parent' | string;
  order?: number;
  source?: 'app' | 'module'; //
  leftComponent?: React.ReactNode;
}

const BASE_Z_INDEX = 100;

const DraggableComponent: React.FC<DraggableComponentProps> = ({
  title,
  bounds = 'body',
  headerSize = 'small',
  startSize = { width: 'auto', height: 'auto' },
  startPosition = { left: null, right: null, top: null },
  children,
  isOpen = true,
  onClose = (): void => null,
  initialMaxHeight: defaultInitialMaxHeight = 'initial',
  onChangeSize = (): void => null,
  onMouseDown = (): void => null,
  order = 1,
  source = 'app',
  leftComponent = null,
  ...props
}: DraggableComponentProps) => {
  const zIndex = (source === 'app' ? BASE_Z_INDEX : 1000) + order;
  const elevation = BASE_ELEVATION + order;
  const nodeRef = React.useRef(null);
  const [position, setPosition] = React.useState({ x: 0, y: 0 });
  const [size, setSize] = React.useState(startSize);

  const onStopDrag = React.useCallback(
    (_event, position): void => {
      setPosition(position);
    },
    [setPosition],
  );

  const [initialMaxHeight, setInitialMaxHeight] = React.useState<
    string | number
  >(defaultInitialMaxHeight);

  const onStartResize = (): void => {
    setInitialMaxHeight('initial');
  };

  const onResize = React.useCallback(
    (_event, _handle, node) => {
      const { width, height } = node.getBoundingClientRect();
      onChangeSize({ width, height } as Size);
    },
    [onChangeSize],
  );

  const onResizeWindow = React.useCallback(
    (event: Event): void => {
      if (!nodeRef.current) return;
      const bbox = nodeRef.current.firstChild.getBoundingClientRect();

      const windowTarget = event.currentTarget as Window;
      if (!windowTarget) return;
      const { innerHeight, innerWidth } = windowTarget;
      const isHorizontallyLarger = bbox.width > innerWidth;
      const isVerticallyLarger = bbox.height > innerHeight;

      // left screen
      if (bbox.x < 0 || isHorizontallyLarger) {
        setPosition({ ...position, x: position.x - bbox.x });
      }
      // right screen
      else if (bbox.x + bbox.width >= innerWidth) {
        const diffX = innerWidth - bbox.x - bbox.width;
        setPosition({ ...position, x: position.x + diffX });
      }

      // top screen
      else if (bbox.y < 0 || isVerticallyLarger) {
        setPosition({ ...position, y: position.y - bbox.y });
      }
      // bottom screen
      else if (bbox.y + bbox.height >= innerHeight) {
        const diffY = innerHeight - bbox.y - bbox.height;
        setPosition({ ...position, y: position.y + diffY });
      }
    },
    [position],
  );

  React.useEffect(() => {
    const throttledResize = throttle(onResizeWindow, 100);
    window.addEventListener('resize', throttledResize);
    return (): void => {
      window.removeEventListener('resize', throttledResize);
    };
  }, [onResizeWindow]);

  const toolcontainerId = React.useRef(Date.now().toString()).current;
  const headerClassName = `header-${toolcontainerId}`;

  return isOpen ? (
    <Draggable
      nodeRef={nodeRef}
      bounds={bounds}
      position={position}
      onStop={onStopDrag}
      handle={`.${headerClassName}`}
      onMouseDown={onMouseDown}
    >
      <div
        style={{
          position: 'absolute',
          pointerEvents: 'all',
          zIndex,
          ...startPosition,
        }}
        ref={nodeRef}
      >
        <Resizable
          {...props}
          startPosition={startPosition}
          size={size}
          setSize={setSize}
          onResizeStart={onStartResize}
          onResize={onResize}
        >
          <ToolContainer
            maxHeight={initialMaxHeight}
            onClose={onClose}
            title={title}
            size={headerSize}
            isDraggable
            isResizable
            elevation={elevation}
            headerClassName={headerClassName}
            leftComponent={leftComponent}
          >
            {children}
          </ToolContainer>
        </Resizable>
      </div>
    </Draggable>
  ) : null;
};

export default DraggableComponent;
