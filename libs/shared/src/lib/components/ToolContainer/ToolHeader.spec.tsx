/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import ToolHeader from './ToolHeader';
import { HeaderSize } from './types';

describe('components/ToolHeader', () => {
  it('should render successfully with default props', () => {
    const { baseElement, queryByTestId } = render(
      <ThemeWrapper>
        <ToolHeader />
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(queryByTestId('dragBtn')).toBeFalsy();
    expect(queryByTestId('closeBtn')).toBeFalsy();
    expect(baseElement.querySelector('h2')).toBeFalsy();
  });

  it('should handle custom className', () => {
    const className = 'header-test';
    const { baseElement } = render(
      <ThemeWrapper>
        <ToolHeader className={className} />
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(
      baseElement.querySelector('header').classList.contains(className),
    ).toBeTruthy();
  });

  it('should handle different sizes and onClose with *mouse* click', () => {
    const props = {
      onClose: jest.fn(),
      size: 'small' as HeaderSize,
      isDraggable: true,
      title: 'test',
    };
    const { baseElement, queryByTestId } = render(
      <ThemeWrapper>
        <ToolHeader {...props} />
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(baseElement.querySelector('h2').innerHTML).toEqual(props.title);

    fireEvent.click(queryByTestId('closeBtn'));
    expect(props.onClose).toHaveBeenCalled();
    expect(queryByTestId('dragBtn')).toBeTruthy();
    expect(queryByTestId('dragBtn-xxs')).toBeFalsy();
  });

  it('should handle different sizes and onClose with *touch* tap', async () => {
    const props = {
      onClose: jest.fn(),
      size: 'small' as HeaderSize,
      isDraggable: true,
      title: 'test',
    };
    const { baseElement, queryByTestId } = render(
      <ThemeWrapper>
        <ToolHeader {...props} />
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(baseElement.querySelector('h2').innerHTML).toEqual(props.title);

    fireEvent.touchEnd(queryByTestId('closeBtn'));
    expect(props.onClose).toHaveBeenCalled();
    expect(queryByTestId('dragBtn')).toBeTruthy();
    expect(queryByTestId('dragBtn-xxs')).toBeFalsy();
  });

  it('should render handle in center for xxs size', () => {
    const props = {
      size: 'xxs' as HeaderSize,
    };
    const { queryByTestId } = render(
      <ThemeWrapper>
        <ToolHeader {...props} />
      </ThemeWrapper>,
    );
    expect(queryByTestId('dragBtn')).toBeFalsy();
    expect(queryByTestId('dragBtn-xxs')).toBeTruthy();
  });

  it('should pass custom leftComponent', () => {
    const TestComponent = (
      <div data-testid="test-leftcomponent">testing left component</div>
    );
    const { queryByTestId, getByTestId } = render(
      <ThemeWrapper>
        <ToolHeader leftComponent={TestComponent} />
      </ThemeWrapper>,
    );
    expect(queryByTestId('dragBtn')).toBeFalsy();
    expect(getByTestId('test-leftcomponent')).toBeTruthy();
  });
});
