/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import ToolContainerDraggable from './ToolContainerDraggable';
import { HeaderSize } from './types';

describe('components/ToolContainer/ToolContainerDraggable', () => {
  it('should render successfully', async () => {
    const { baseElement, findByText } = render(
      <ThemeWrapper>
        <ToolContainerDraggable startPosition={{ right: 100, top: 100 }}>
          <div>test</div>
        </ToolContainerDraggable>
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(await findByText('test')).toBeTruthy();
  });

  it('should render with props', async () => {
    const props = {
      title: 'test title',
      headerSize: 'medium' as HeaderSize,
      onMouseDown: jest.fn(),
    };
    const { findByText, container } = render(
      <ThemeWrapper>
        <ToolContainerDraggable {...props}>
          <div>test</div>
        </ToolContainerDraggable>
      </ThemeWrapper>,
    );

    expect(props.onMouseDown).not.toHaveBeenCalled();
    expect(await findByText(props.title)).toBeTruthy();

    fireEvent.mouseDown(container.querySelector('.react-draggable'));
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should listen to window resize events', async () => {
    jest.spyOn(window, 'addEventListener');

    render(
      <ThemeWrapper>
        <ToolContainerDraggable>
          <div>test</div>
        </ToolContainerDraggable>
      </ThemeWrapper>,
    );

    // // Trigger the window resize event.
    window.dispatchEvent(new Event('resize'));

    expect(window.addEventListener).toBeCalled();
    expect(window.addEventListener).toBeCalledWith(
      'resize',
      expect.any(Function),
    );
  });

  it('should be able to pass leftComponent', async () => {
    const props = {
      title: 'test title',
      headerSize: 'medium' as HeaderSize,
      onMouseDown: jest.fn(),
      leftComponent: <div>custom-component</div>,
    };
    const { findByText } = render(
      <ThemeWrapper>
        <ToolContainerDraggable {...props}>
          <div>test</div>
        </ToolContainerDraggable>
      </ThemeWrapper>,
    );

    expect(await findByText(props.title)).toBeTruthy();

    expect(await findByText('custom-component')).toBeTruthy();
  });
});
