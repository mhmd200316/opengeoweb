/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { Grid, IconButton, Typography, Box, Tooltip } from '@mui/material';
import { Close, DragHandle } from '@opengeoweb/theme';
import React from 'react';
import { HeaderSize, ToolHeaderProps } from './types';

// https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60f9318c3e123f13c72bbe96
const getStyle = (size: HeaderSize): React.CSSProperties => {
  switch (size) {
    case 'xs': {
      return {
        fontSize: 10,
        fontWeight: 500,
      };
    }
    case 'small': {
      return {
        fontSize: 16,
        fontWeight: 500,
        padding: '4px',
      };
    }
    case 'medium': {
      return {
        fontSize: 20,
        fontWeight: 400,
        padding: '8px',
      };
    }
    case 'large': {
      return {
        fontSize: 16,
        fontWeight: 500,
        padding: '12px',
      };
    }
    default: {
      return {};
    }
  }
};

const iconStyle = {
  color: 'geowebColors.greys.accessible',
  padding: 0,
  '&:hover': {
    background: 'action.hover',
  },
};

const ToolHeader: React.FC<ToolHeaderProps> = ({
  size,
  title,
  onClose = null,
  isDraggable = false,
  className = '',
  leftComponent = null,
}: ToolHeaderProps) => {
  const styles = getStyle(size);
  const columnSizeIcon = leftComponent ? 3 : 1;
  const columnSizeTitle = leftComponent ? 6 : 10;

  return (
    <Box
      component="header"
      className={className}
      sx={{
        boxShadow: 1, // elevation_01
        backgroundColor: 'geowebColors.background.surface',
        padding: styles.padding,
        zIndex: 2,
        cursor: isDraggable ? 'move' : null,
      }}
    >
      <Grid container alignItems="center">
        <Grid item xs={columnSizeIcon} sx={{ paddingRight: 1 }}>
          {isDraggable && size !== 'xxs' && !leftComponent && (
            <Grid container justifyContent="flex-start">
              <IconButton
                data-testid="dragBtn"
                disableRipple
                size="large"
                sx={{
                  ...iconStyle,
                  pointerEvents: 'none',
                }}
              >
                <DragHandle />
              </IconButton>
            </Grid>
          )}
          {leftComponent && leftComponent}
        </Grid>
        <Grid item xs={columnSizeTitle}>
          <Grid container justifyContent="center" alignItems="center">
            {title && (
              <Typography
                variant="h2"
                noWrap
                sx={{
                  fontSize: styles.fontSize,
                  fontWeight: styles.fontWeight,
                }}
              >
                {title}
              </Typography>
            )}
            {size === 'xxs' && (
              <IconButton
                data-testid="dragBtn-xxs"
                disableRipple
                size="small"
                sx={{
                  ...iconStyle,
                  pointerEvents: 'none',
                  transform: 'rotate(90deg)',
                  height: 12,
                }}
              >
                <DragHandle />
              </IconButton>
            )}
          </Grid>
        </Grid>
        <Grid item xs={columnSizeIcon} container justifyContent="flex-end">
          {onClose && (
            <Tooltip
              title="Close"
              placement="bottom"
              sx={{ pointerEvents: 'none' }}
            >
              <IconButton
                data-testid="closeBtn"
                onClick={onClose}
                onTouchEnd={onClose}
                size="large"
                sx={iconStyle}
              >
                <Close data-testid="CloseIcon" />
              </IconButton>
            </Tooltip>
          )}
        </Grid>
      </Grid>
    </Box>
  );
};

export default ToolHeader;
