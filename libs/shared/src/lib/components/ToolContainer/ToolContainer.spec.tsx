/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import ToolContainer from './ToolContainer';

describe('components/ToolContainer', () => {
  it('should render successfully', () => {
    const props = {
      onClose: jest.fn(),
    };
    const { baseElement } = render(
      <ThemeWrapper>
        <ToolContainer {...props} />
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(baseElement.querySelector('footer')).toBeFalsy();
  });

  it('should render children and footer', () => {
    const props = {
      onClose: jest.fn(),
      isResizable: true,
    };

    const { baseElement, queryByTestId } = render(
      <ThemeWrapper>
        <ToolContainer {...props}>
          <div className="testChildren">test children</div>
        </ToolContainer>
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(baseElement.querySelector('footer')).toBeTruthy();
    expect(baseElement.querySelector('.testChildren')).toBeTruthy();
    fireEvent.click(queryByTestId('closeBtn'));
    expect(props.onClose).toHaveBeenCalled();
  });

  it('should pass headerClass', () => {
    const props = {
      onClose: jest.fn(),
      headerClassName: 'test-classname',
      title: 'testing',
    };
    const { baseElement } = render(
      <ThemeWrapper>
        <ToolContainer {...props} />
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(
      baseElement
        .querySelector('header')
        .classList.contains(props.headerClassName),
    ).toBeTruthy();
  });

  it('should pass custom leftComponent', () => {
    const TestComponent = (
      <div data-testid="test-leftcomponent">testing left component</div>
    );
    const { queryByTestId, getByTestId } = render(
      <ThemeWrapper>
        <ToolContainer leftComponent={TestComponent} />
      </ThemeWrapper>,
    );
    expect(queryByTestId('dragBtn')).toBeFalsy();
    expect(getByTestId('test-leftcomponent')).toBeTruthy();
  });
});
