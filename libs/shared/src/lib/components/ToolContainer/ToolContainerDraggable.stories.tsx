/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Button, Grid, Paper, Typography } from '@mui/material';
import { darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import ToolContainerDraggable from './ToolContainerDraggable';

export default {
  title: 'components/ToolContainer/ToolContainerDraggable',
};

const DemoStory: React.FC = () => {
  const [content, setContent] = React.useState<number[]>([1]);
  const [isLeftOpen, toggleLeft] = React.useState<boolean>(true);
  const [isBoundsDialogOpen, toggleBoundsDialog] =
    React.useState<boolean>(true);
  const [isRightOpen, toggleRight] = React.useState<boolean>(true);
  const [isBigOpen, toggleBig] = React.useState<boolean>(false);
  return (
    <div
      style={{
        height: '100vh',
        width: '100vw',
      }}
    >
      <Grid container>
        <Grid item xs={9}>
          <Button
            onClick={(): void => {
              toggleLeft(!isLeftOpen);
            }}
          >
            Toggle left
          </Button>
          <Button
            onClick={(): void => {
              toggleBig(!isBigOpen);
            }}
          >
            Toggle big
          </Button>
          <Button
            onClick={(): void => {
              toggleRight(!isRightOpen);
            }}
          >
            Toggle right
          </Button>
        </Grid>

        <Grid item xs={3}>
          <Button
            onClick={(): void => {
              setContent([...content, 1]);
            }}
          >
            add content
          </Button>
        </Grid>
      </Grid>

      {/* parent bounds */}
      <div
        style={{
          width: 400,
          height: 400,
          bottom: 20,
          left: 20,
          position: 'absolute',
          border: `1px solid red`,
        }}
      >
        <ToolContainerDraggable
          title="parent bounds"
          headerSize="medium"
          isOpen={isBoundsDialogOpen}
          onClose={(): void => {
            toggleBoundsDialog(!isBoundsDialogOpen);
          }}
          bounds="parent"
        >
          <Typography>I can not be dragged outside my parent</Typography>
        </ToolContainerDraggable>
      </div>

      {/* left */}
      <ToolContainerDraggable
        startPosition={{ left: 70, top: 70 }}
        title="Align left"
        headerSize="medium"
        isOpen={isLeftOpen}
        onClose={(): void => {
          toggleLeft(!isLeftOpen);
        }}
      >
        {content.map((_value, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <Typography key={`key-${index}`}>
            left bigger content without width or height
          </Typography>
        ))}
      </ToolContainerDraggable>

      {/* right */}
      <ToolContainerDraggable
        startPosition={{ right: 70, top: 70 }}
        minWidth={200}
        title="Align right"
        headerSize="small"
        initialMaxHeight={200}
        isOpen={isRightOpen}
        onClose={(): void => {
          toggleRight(!isRightOpen);
        }}
      >
        {content.map((_value, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <Typography key={`key-${index}`}>
            aligned right with initial max height
          </Typography>
        ))}
      </ToolContainerDraggable>

      {/** large */}
      <ToolContainerDraggable
        startSize={{ width: '90vw', height: '90vh' }}
        startPosition={{ left: 50, top: 50 }}
        isOpen={isBigOpen}
        onClose={(): void => {
          toggleBig(!isBigOpen);
        }}
        title="Big container"
      >
        <Paper
          style={{
            padding: 20,
            height: '100%',
          }}
        >
          left aligned without bounds
          <ToolContainerDraggable
            startPosition={{ left: 150, top: 150 }}
            title="inner drag container"
            headerSize="small"
            isOpen
            bounds="parent"
          >
            <Typography>inner drag container</Typography>
          </ToolContainerDraggable>
        </Paper>
      </ToolContainerDraggable>
    </div>
  );
};

export const ThemeLight: React.FC = () => (
  <ThemeWrapper>
    <DemoStory />
  </ThemeWrapper>
);

export const ThemeDark: React.FC = () => (
  <ThemeWrapper theme={darkTheme}>
    <DemoStory />
  </ThemeWrapper>
);
