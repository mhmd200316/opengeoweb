/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';

import { ThemeWrapper } from '@opengeoweb/theme';
import { CustomSlider } from './CustomSlider';

describe('components/CustomSlider', () => {
  const props = {
    value: 0,
    marks: [
      { value: 0, text: '0' },
      { value: 1, text: '1' },
      { value: 2, text: '2' },
      { value: 3, text: '3' },
    ],
    min: 0,
    max: 3,
    'data-testid': 'customSlider',
    onChange: jest.fn(),
  };
  const propsDisabled = {
    ...props,
    disabled: true,
  };
  it('should render the slider', () => {
    const { getByTestId, container } = render(
      <ThemeWrapper>
        <CustomSlider {...props} />
      </ThemeWrapper>,
    );

    expect(
      container
        .querySelector('.MuiSlider-thumb')
        .classList.contains('Mui-focusVisible'),
    ).toBeFalsy();
    expect(getByTestId('customSlider')).toBeTruthy();
  });

  it('should render the slider correctly with 4 marks and first one active', () => {
    const { container } = render(
      <ThemeWrapper>
        <CustomSlider {...props} />
      </ThemeWrapper>,
    );

    const marks = container.getElementsByClassName('MuiSlider-mark');
    expect(marks.length === 4).toBeTruthy();
    const activeMarks = container.getElementsByClassName(
      'MuiSlider-markActive',
    );
    expect(activeMarks.length === 1).toBeTruthy();
    // Confirms that the first mark is the active one
    expect(marks[0].classList).toContain('MuiSlider-markActive');
  });

  it('should render a slider as enabled', () => {
    const { getByTestId } = render(
      <ThemeWrapper>
        <CustomSlider {...props} />
      </ThemeWrapper>,
    );

    expect(getByTestId('customSlider')).toBeTruthy();
    expect(
      getByTestId('customSlider').classList.contains('Mui-disabled'),
    ).toBeFalsy();
  });

  it('should render a slider as disabled ', () => {
    const { getByTestId } = render(
      <ThemeWrapper>
        <CustomSlider {...propsDisabled} />
      </ThemeWrapper>,
    );

    expect(getByTestId('customSlider')).toBeTruthy();
    expect(
      getByTestId('customSlider').classList.contains('Mui-disabled'),
    ).toBeTruthy();
  });

  it('should autofocus to thumb ', () => {
    const testProps = {
      ...props,
      shouldAutoFocus: true,
    };
    const { container } = render(
      <ThemeWrapper>
        <CustomSlider {...testProps} />
      </ThemeWrapper>,
    );

    expect(
      container
        .querySelector('.MuiSlider-thumb')
        .classList.contains('Mui-focusVisible'),
    ).toBeTruthy();
  });

  it('handleChange is called', async () => {
    const { getByTestId } = render(
      <ThemeWrapper>
        <CustomSlider {...props} />
      </ThemeWrapper>,
    );

    const slider = getByTestId('customSlider');
    expect(slider).toBeTruthy();
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientY: 0 });
    fireEvent.mouseUp(slider);
    expect(props.onChange).toHaveBeenCalledTimes(1);
  });
});
