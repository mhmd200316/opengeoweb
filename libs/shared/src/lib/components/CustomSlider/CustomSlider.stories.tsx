/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Card, Box } from '@mui/material';

import { CustomSlider, sliderHeaderStyle } from './CustomSlider';

export default { title: 'components/CustomSlider' };

const SliderExamples: React.FC = () => {
  const [value, setValue] = React.useState(1);
  const onChangeSlider = (_event, newValue): void => {
    setValue(newValue);
  };
  const [value2, setValue2] = React.useState(1);
  const onChangeSlider2 = (_event, newValue): void => {
    setValue2(newValue);
  };
  const [value3, setValue3] = React.useState(1);
  const onChangeSlider3 = (_event, newValue): void => {
    setValue3(newValue);
  };

  return (
    <Card
      style={{
        width: '300px',
        height: '300px',
      }}
    >
      <div
        style={{
          height: '200px',
          width: '100px',
          float: 'left',
          marginTop: '10px',
        }}
      >
        <Box sx={sliderHeaderStyle}>Header 1</Box>
        <CustomSlider
          orientation="vertical"
          value={value}
          step={1}
          max={10}
          min={0}
          onChange={onChangeSlider}
        />
      </div>
      <div
        style={{
          height: '200px',
          width: '100px',
          float: 'left',
          marginTop: '10px',
        }}
      >
        <Box sx={sliderHeaderStyle}>Header 2</Box>
        <CustomSlider
          orientation="vertical"
          value={value2}
          max={3}
          min={0}
          marks={[
            { value: 0, label: '0' },
            { value: 1, label: '1' },
            { value: 2, label: '2' },
            { value: 3, label: '3' },
          ]}
          onChange={onChangeSlider2}
        />
      </div>
      <div
        style={{
          height: '200px',
          width: '100px',
          float: 'left',
          marginTop: '10px',
        }}
      >
        <Box sx={sliderHeaderStyle}>Header 3</Box>
        <CustomSlider
          orientation="vertical"
          value={value3}
          max={3}
          min={0}
          marks={[
            { value: 0, label: '0' },
            { value: 1, label: '1' },
            { value: 2, label: '2' },
            { value: 3, label: '3' },
          ]}
          onChange={onChangeSlider3}
          disabled={true}
        />
      </div>
    </Card>
  );
};

export const CustomSliderLightTheme = (): React.ReactElement => (
  <ThemeWrapper>
    <SliderExamples />
  </ThemeWrapper>
);
CustomSliderLightTheme.storyName = 'CustomSlider light theme (takeSnapshot)';

export const CustomSliderDarkTheme = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <SliderExamples />
  </ThemeWrapper>
);
CustomSliderDarkTheme.storyName = 'CustomSlider dark theme (takeSnapshot)';
