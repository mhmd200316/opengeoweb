/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Button } from '@mui/material';
import { ThemeWrapper, darkTheme } from '@opengeoweb/theme';

import {
  ConfirmationServiceProvider,
  useConfirmationDialog,
} from './ConfirmationService';
import { ConfirmationDialog } from './ConfirmationDialog';

export default { title: 'components/ConfirmationDialog' };

const Demo: React.FC = () => {
  const confirmDialog = useConfirmationDialog();
  return (
    <>
      <Button
        variant="contained"
        color="primary"
        onClick={(): void => {
          confirmDialog({
            title: 'Simple confirm dialog',
            description: 'Are you sure you want to do this',
          });
        }}
      >
        simple confirm
      </Button>

      <Button
        variant="contained"
        color="primary"
        onClick={(): void => {
          confirmDialog({
            cancelLabel: "Don't save",
            title: 'Save',
            description: 'Are you sure you want save?',
            confirmLabel: 'Save',
            catchOnCancel: true,
          })
            .then(() => {
              // eslint-disable-next-line no-console
              console.log('save!');
            })
            .catch(() => {
              // eslint-disable-next-line no-console
              console.log('catching is also possible');
            });
        }}
      >
        custom confirm
      </Button>
    </>
  );
};

export const ConfirmationDialogCustom = (): React.ReactElement => {
  return (
    <ThemeWrapper>
      <ConfirmationServiceProvider>
        <Demo />
      </ConfirmationServiceProvider>
    </ThemeWrapper>
  );
};

export const ConfirmationDialogLoading = (): React.ReactElement => {
  return (
    <ThemeWrapper>
      <ConfirmationDialog
        title="Save map preset as"
        open
        onClose={(): void => {}}
        onSubmit={(): void => {}}
        disableAutoFocus
        isLoading
      />
    </ThemeWrapper>
  );
};

export const ConfirmationDialogLight = (): React.ReactElement => {
  return (
    <ThemeWrapper>
      <ConfirmationDialog
        title="Whoops"
        open
        onClose={(): void => {}}
        onSubmit={(): void => {}}
        description="Are you sure you want to exit? All your changes will be lost."
        confirmLabel="Exit without saving"
        disableAutoFocus
      />
    </ThemeWrapper>
  );
};

ConfirmationDialogLight.storyName = 'Confirmation Dialog Light (takeSnapshot)';

export const ConfirmationDialogDark = (): React.ReactElement => {
  return (
    <ThemeWrapper theme={darkTheme}>
      <ConfirmationDialog
        title="Whoops"
        open
        onClose={(): void => {}}
        onSubmit={(): void => {}}
        description="Are you sure you want to exit? All your changes will be lost."
        confirmLabel="Exit without saving"
        disableAutoFocus
      />
    </ThemeWrapper>
  );
};

ConfirmationDialogDark.storyName = 'Confirmation Dialog Dark (takeSnapshot)';
