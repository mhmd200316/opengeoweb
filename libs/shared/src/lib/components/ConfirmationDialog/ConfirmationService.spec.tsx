/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { renderHook, act } from '@testing-library/react-hooks';
import {
  ConfirmationServiceProvider,
  useDialogActions,
  useConfirmationDialog,
} from './ConfirmationService';
import { SharedThemeProvider } from '../Providers';

interface WrapperProps {
  children: React.ReactNode;
}
const Wrapper: React.FC<WrapperProps> = ({ children }: WrapperProps) => {
  return (
    <SharedThemeProvider>
      <ConfirmationServiceProvider>{children}</ConfirmationServiceProvider>
    </SharedThemeProvider>
  );
};

describe('components/ConfirmationDialog/ConfirmationService', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should be able wrap a component with confirmDialog', async () => {
    const options = {
      title: 'test title',
      description: 'test description',
    };
    const TestComp: React.FC = () => {
      const confirmDialog = useConfirmationDialog();
      return (
        <button
          type="button"
          onClick={(): void => {
            confirmDialog(options);
          }}
        >
          simple confirm
        </button>
      );
    };

    const { container, queryByTestId } = render(
      <Wrapper>
        <TestComp />
      </Wrapper>,
    );

    expect(queryByTestId('confirmationDialog')).toBeFalsy();
    const button = container.querySelector('button');

    await waitFor(() => fireEvent.click(button));
    expect(queryByTestId('confirmationDialog')).toBeTruthy();
    expect(
      queryByTestId('confirmationDialog').textContent.includes(options.title),
    ).toBeTruthy();
    expect(
      queryByTestId('confirmationDialog').textContent.includes(
        options.description,
      ),
    ).toBeTruthy();

    // close it again
    await waitFor(() =>
      fireEvent.click(queryByTestId('confirmationDialog-close')),
    );
    await waitFor(() =>
      expect(queryByTestId('confirmationDialog')).toBeFalsy(),
    );
  });

  describe('useDialogActions', () => {
    it('should handle close', async () => {
      const ref = {
        current: {
          reject: jest.fn(),
          resolve: jest.fn(),
        },
      };
      const { result } = renderHook(() =>
        useDialogActions(ref, { title: 'test', catchOnCancel: true }),
      );
      await act(async () => {
        const { handleClose, confirmationState } = result.current;
        expect(confirmationState).toEqual({
          title: 'test',
          catchOnCancel: true,
        });
        handleClose('CLOSED');
        expect(ref.current.reject).toHaveBeenCalled();
        jest.runAllTimers();
      });

      await act(async () => {
        const { confirmationState } = result.current;
        expect(confirmationState).toBeNull();
      });
    });
    it('should handle submit', async () => {
      const ref = {
        current: {
          reject: jest.fn(),
          resolve: jest.fn(),
        },
      };
      const { result } = renderHook(() =>
        useDialogActions(ref, { title: 'test', catchOnCancel: true }),
      );
      await act(async () => {
        const { handleSubmit, confirmationState } = result.current;
        expect(confirmationState).toEqual({
          title: 'test',
          catchOnCancel: true,
        });
        handleSubmit();
        expect(ref.current.resolve).toHaveBeenCalled();
        jest.runAllTimers();
      });
      await act(async () => {
        const { confirmationState } = result.current;
        expect(confirmationState).toBeNull();
      });
    });
    it('should work without provided ref', async () => {
      const { result } = renderHook(() => useDialogActions({ current: null }));
      await act(async () => {
        const { handleSubmit, confirmationState } = result.current;
        handleSubmit();
        expect(confirmationState).toBeNull();
      });
    });

    it('should not trigger callback when not mounted', async () => {
      const TestComp: React.FC = () => {
        const ref = {
          current: {
            reject: jest.fn(),
            resolve: jest.fn(),
          },
        };
        const options = { title: 'testing' };
        const { handleClose } = useDialogActions(ref, options);
        return (
          <button
            type="button"
            onClick={(): void => {
              handleClose('CLOSED');
            }}
          >
            simple confirm
          </button>
        );
      };

      const { container, unmount } = render(
        <Wrapper>
          <TestComp />
        </Wrapper>,
      );
      const button = container.querySelector('button');

      await waitFor(() => fireEvent.click(button));
      expect(container.querySelector('button')).toBeTruthy();

      unmount();
      expect(container.querySelector('button')).toBeNull();

      // run all timers and verify to make sure
      jest.runAllTimers();
      expect(container.querySelector('button')).toBeNull();
    });
  });
});
