/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';

import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Typography,
  IconButton,
  Box,
  CircularProgress,
} from '@mui/material';
import { Close } from '@opengeoweb/theme';

const confirmStyle = {
  width: { xs: '100%', sm: '150px' },
  marginLeft: { xs: '0px!important', sm: '8px!important' },
  marginTop: { xs: 2, sm: 0 },
};

export interface ConfirmationOptions {
  catchOnCancel?: boolean;
  title: string;
  description?: string;
  confirmLabel?: string;
  cancelLabel?: string;
  content?: React.ReactNode;
  disableAutoFocus?: boolean;
}

export type ConfirmationAction = 'CANCELLED' | 'CLOSED';

interface ConfirmationDialogProps extends ConfirmationOptions {
  open: boolean;
  onSubmit: () => void;
  onClose: (reason: ConfirmationAction) => void;
  isLoading?: boolean;
}

export const ConfirmationDialog: React.FC<ConfirmationDialogProps> = ({
  open,
  title,
  description = 'Are you sure?',
  confirmLabel = 'Submit',
  cancelLabel = 'Go back',
  onSubmit,
  onClose,
  content = null,
  disableAutoFocus = false,
  isLoading = false,
  ...other
}: ConfirmationDialogProps) => {
  const { catchOnCancel, ...rest } = other; // remove unwanted props

  return (
    <Dialog
      data-testid="confirmationDialog"
      open={open}
      PaperProps={{
        sx: { width: '560px', margin: 2.5 },
      }}
      // fix for briefly showing default options when closing dialog
      transitionDuration={0}
      onClose={(): void => {
        onClose('CLOSED');
      }}
      {...rest}
    >
      <DialogTitle
        sx={{
          zIndex: 1,
          padding: 2,
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <Typography
          sx={{
            fontSize: { xs: '16px', sm: '34px' },
            fontWeight: { xs: 500, sm: 400 },
          }}
          data-testid="confirmationDialog-title"
          component="span"
        >
          {title}
        </Typography>
        <IconButton
          data-testid="confirmationDialog-close"
          aria-label="close"
          onClick={(): void => onClose('CLOSED')}
          size="small"
        >
          <Close data-testid="CloseIcon" />
        </IconButton>
      </DialogTitle>

      <DialogContent
        sx={{
          padding: '8px 16px',
        }}
      >
        {description && <Typography variant="body1">{description}</Typography>}
        {content}
      </DialogContent>
      <DialogActions
        sx={{
          padding: 2,
          flexDirection: { xs: 'column', sm: 'row' },
        }}
      >
        <Button
          data-testid="confirmationDialog-cancel"
          variant="tertiary"
          // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
          sx={(theme) => ({
            width: {
              xs: '100%',
              sm: '150px',
            },
            '&:focus': {
              outline: theme.palette.geowebColors.buttons.focusDefault.outline,
              borderColor: 'transparent',
            },
          })}
          onClick={(): void => onClose('CANCELLED')}
          disableFocusRipple
        >
          {cancelLabel}
        </Button>
        {isLoading ? (
          <Box
            data-testid="confirm-dialog-spinner"
            sx={{
              ...confirmStyle,
              height: '40px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <CircularProgress size={40} color="secondary" />
          </Box>
        ) : (
          <Button
            data-testid="confirmationDialog-confirm"
            variant="primary"
            // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
            sx={(theme) => ({
              ...confirmStyle,
              '&:focus': {
                outline:
                  theme.palette.geowebColors.buttons.focusDefault.outline,
                borderColor: 'transparent',
              },
            })}
            onClick={onSubmit}
            autoFocus={!disableAutoFocus}
            disableFocusRipple
          >
            {confirmLabel}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmationDialog;
