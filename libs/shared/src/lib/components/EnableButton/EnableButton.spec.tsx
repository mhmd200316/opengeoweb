/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import { EnableButton } from './EnableButton';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/EnableLayer/EnableLayer', () => {
  // Test 1
  const propsTest1 = {
    onChangeEnableLayer: jest.fn(),
    title: 'Test1',
    isEnabled: true,
  };

  it('should be checked if enabled passed in as true', () => {
    const { container } = render(
      <ThemeWrapper>
        <EnableButton {...propsTest1} />
      </ThemeWrapper>,
    );
    const checkbox = getByRole(container, 'checkbox') as HTMLInputElement;
    expect(checkbox.checked).toEqual(propsTest1.isEnabled);
  });

  // Test 2
  const propsTest2 = {
    onChangeEnableLayer: jest.fn(),
    title: 'Test2',
    isEnabled: false,
  };

  it('should not be checked if enabled passed in as false', () => {
    const { container } = render(
      <ThemeWrapper>
        <EnableButton {...propsTest2} />
      </ThemeWrapper>,
    );
    const checkbox = getByRole(container, 'checkbox') as HTMLInputElement;
    expect(checkbox.checked).toEqual(propsTest2.isEnabled);
  });

  // Test 3
  it('should call onChangeEnableLayer when checkbox clicked', () => {
    const { container } = render(
      <ThemeWrapper>
        <EnableButton {...propsTest1} />
      </ThemeWrapper>,
    );
    const checkbox = getByRole(container, 'checkbox') as HTMLInputElement;
    fireEvent.click(checkbox);

    expect(propsTest1.onChangeEnableLayer).toHaveBeenCalledWith(
      !propsTest1.isEnabled,
    );
  });
});
