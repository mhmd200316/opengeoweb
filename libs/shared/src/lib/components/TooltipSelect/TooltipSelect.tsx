/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Tooltip, Select, SelectProps } from '@mui/material';
import { tooltipContainerStyles } from './tooltipContainerStyles';

type ListELement = {
  value: string;
};
interface TooltipSelectProps extends SelectProps {
  tooltip: string;
  list?: ListELement[];
  currentIndex?: number;
  onChangeMouseWheel?: (element: ListELement) => void;
  requiresCtrlToChange?: boolean;
  isEnabled?: boolean;
  noStyling?: boolean;
  hasBackgroundColor?: boolean;
}

export const TooltipSelect: React.FC<TooltipSelectProps> = ({
  tooltip,
  list = [],
  currentIndex = -1,
  onChangeMouseWheel = (): void => {},
  requiresCtrlToChange = false,
  isEnabled,
  noStyling,
  hasBackgroundColor,
  ...props
}: TooltipSelectProps) => {
  const { disableUnderline, ...otherProps } = props;
  const [selectOpen, setSelectOpen] = React.useState(false);
  const [tooltipOpen, setTooltipOpen] = React.useState(false);

  const onWheel = React.useCallback(
    (event: React.WheelEvent): void => {
      if (
        (requiresCtrlToChange && (event.ctrlKey || event.metaKey)) ||
        !requiresCtrlToChange
      ) {
        if (selectOpen) return;
        const direction = event.deltaY < 0 ? 1 : -1;
        const newValue =
          list[
            Math.min(Math.max(currentIndex - direction, 0), list.length - 1)
          ];
        onChangeMouseWheel(newValue);
      }
    },
    [selectOpen, currentIndex, list, onChangeMouseWheel, requiresCtrlToChange],
  );
  const onOpen = React.useCallback(() => {
    setSelectOpen(true);
    setTooltipOpen(false);
  }, []);
  const onClose = React.useCallback(() => {
    setSelectOpen(false);
  }, []);
  const onMouseEnter = React.useCallback(() => {
    if (!selectOpen) setTooltipOpen(true);
  }, [selectOpen]);
  const onMouseLeave = React.useCallback(() => {
    setTooltipOpen(false);
  }, []);
  const onFocus = React.useCallback(() => {
    setTooltipOpen(true);
  }, []);
  const onBlur = React.useCallback(() => {
    setTooltipOpen(false);
  }, []);

  return (
    <Tooltip
      title={tooltip}
      placement="top"
      open={tooltipOpen}
      sx={
        noStyling
          ? undefined
          : tooltipContainerStyles(isEnabled, hasBackgroundColor)
      }
    >
      <Select
        onOpen={onOpen}
        onClose={onClose}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        onFocus={onFocus}
        onBlur={onBlur}
        onWheel={onWheel}
        MenuProps={{
          BackdropProps: {
            sx: {
              '&.MuiBackdrop-root': {
                backgroundColor: 'transparent',
              },
            },
          },
          sx: {
            '& ul': {
              backgroundColor: 'geowebColors.background.surface',
            },
            '& li': {
              fontSize: '16px',
              padding: '12px',
              '&:hover': {
                backgroundColor: 'geowebColors.buttons.tool.mouseOver.fill',
              },
              '&.Mui-selected': {
                color: 'geowebColors.buttons.tool.active.color',
                backgroundColor: 'geowebColors.buttons.tool.active.fill',
                boxShadow: `none`,
                '&:hover': {
                  backgroundColor:
                    'geowebColors.buttons.tool.activeMouseOver.fill',
                },
                '&:focus': {
                  backgroundColor:
                    'geowebColors.buttons.tool.activeMouseOver.fill',
                },
              },
              '&:after': {
                background: 'none!important',
              },
              '&.Mui-disabled': {
                fontSize: '12px',
                opacity: 0.67,
                padding: '0px 12px',
                '&.MuiMenuItem-root': {
                  minHeight: '30px!important',
                },
              },
            },
          },
        }}
        {...otherProps}
      />
    </Tooltip>
  );
};
