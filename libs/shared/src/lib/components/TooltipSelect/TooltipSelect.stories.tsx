/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC } from 'react';
import { darkTheme, lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Grid, MenuItem } from '@mui/material';
import { TooltipSelect } from './TooltipSelect';

export default { title: 'components/Manager/TooltipSelect' };

const StoryTooltipSelect: FC<{ isEnabled: boolean }> = ({ isEnabled }) => (
  <Grid item container direction="column">
    <Grid>{isEnabled ? 'Enabled' : 'Disabled'}</Grid>
    <Grid>
      <TooltipSelect value={1} tooltip="Choose a number" isEnabled={isEnabled}>
        <MenuItem disabled>Numbers</MenuItem>
        <MenuItem value={1}>One</MenuItem>
        <MenuItem value={2}>Two</MenuItem>
        <MenuItem value={3}>Three</MenuItem>
      </TooltipSelect>
    </Grid>
  </Grid>
);

const StoryTooltipSelects: FC = () => (
  <Grid
    container
    spacing={12}
    sx={{ margin: '10px', padding: '20px 0px', width: '300px' }}
  >
    <StoryTooltipSelect isEnabled={true} />
    <StoryTooltipSelect isEnabled={false} />
  </Grid>
);

export const TooltipSelectLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <StoryTooltipSelects />
  </ThemeWrapper>
);

export const TooltipSelectDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <StoryTooltipSelects />
  </ThemeWrapper>
);

TooltipSelectLight.storyName = 'Tooltip Select Light (takeSnapshot)';
TooltipSelectDark.storyName = 'Tooltip Select Dark (takeSnapshot)';
