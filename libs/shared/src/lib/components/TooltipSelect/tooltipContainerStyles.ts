/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { SxProps, Theme } from '@mui/material';

export const tooltipContainerStyles = (
  isEnabled: boolean,
  hasBackgroundColor = true,
): SxProps<Theme> => ({
  width: '100%',
  height: 32,
  fontWeight: 500,
  fontSize: (theme): number | string =>
    theme.palette.geowebColors.layerManager.tableRowDefaultText.fontSize,
  color: isEnabled
    ? 'geowebColors.layerManager.tableRowDefaultText.rgba'
    : 'geowebColors.layerManager.tableRowDisabledText.rgba',

  '& .MuiOutlinedInput-input': {
    padding: '6px 24px 6px 8px',
  },
  '& .MuiOutlinedInput-notchedOutline': {
    borderColor: 'transparent!important',
  },
  '&:hover .MuiSelect-select': {
    outline: (theme: Theme): string | number =>
      theme.palette.geowebColors.buttons.focusDefault.outline,
  },
  '&.Mui-focused .MuiSelect-select': {
    outline: (theme): string | number =>
      theme.palette.geowebColors.buttons.focusDefault.outline,
  },
  '&:focus&.MuiButton-root': {
    outline: (theme): string | number =>
      theme.palette.geowebColors.buttons.focusDefault.outline,
  },
  '& .MuiSvgIcon-root': {
    transition: 'none',
    fill: (theme): string =>
      isEnabled
        ? theme.palette.geowebColors.buttons.flat.default.color
        : theme.palette.geowebColors.buttons.flat.disabled.color,
  },
  '&>div': {
    paddingLeft: 1,
  },
  ...(hasBackgroundColor && backgroundColor),
});

const backgroundColor = {
  '&:hover .MuiSelect-select': {
    outline: (theme: Theme): string | number =>
      theme.palette.geowebColors.buttons.focusDefault.outline,
    backgroundColor: 'geowebColors.buttons.tool.mouseOver.fill',
  },
  '&.Mui-focused .MuiSelect-select': {
    backgroundColor: 'geowebColors.buttons.tool.mouseOver.fill',
  },
  '&:hover&.MuiButton-root': {
    backgroundColor: 'geowebColors.buttons.tool.mouseOver.fill',
  },
  '&:focus&.MuiButton-root': {
    outline: (theme: Theme): string | number =>
      theme.palette.geowebColors.buttons.focusDefault.outline,
    backgroundColor: 'geowebColors.buttons.tool.mouseOver.fill',
  },
};
