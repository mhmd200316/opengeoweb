/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { MenuItem, Box } from '@mui/material';
import { ThemeWrapper } from '@opengeoweb/theme';
import { TooltipSelect } from './TooltipSelect';

describe('components/Manager/TooltipSelect/TooltipSelect', () => {
  it('work with default props', () => {
    const props = {
      tooltip: 'test tooltip',
    };

    const { container, getByText } = render(
      <ThemeWrapper>
        <TooltipSelect {...props}>
          <MenuItem>empty</MenuItem>
        </TooltipSelect>
      </ThemeWrapper>,
    );

    const button = container.querySelector('[role="button"]');
    fireEvent.focus(button);

    expect(
      container.parentElement.querySelector('.MuiTooltip-tooltip'),
    ).toBeTruthy();

    expect(getByText(props.tooltip)).toBeTruthy();
  });

  it('should show correct tooltip on focus', async () => {
    const props = {
      tooltip: 'test tooltip',
    };

    const { container } = render(
      <ThemeWrapper>
        <TooltipSelect {...props} data-testid="testing">
          <MenuItem>empty</MenuItem>
        </TooltipSelect>
      </ThemeWrapper>,
    );

    expect(
      container.parentElement.querySelector('.MuiTooltip-tooltip'),
    ).toBeFalsy();
    const button = container.querySelector('[role="button"]');

    await waitFor(() => fireEvent.focus(button));
    expect(
      container.parentElement.querySelector('.MuiTooltip-tooltip'),
    ).toBeTruthy();
  });

  it('should call onChangeMouseWheel on wheel scroll', async () => {
    const props = {
      tooltip: 'test tooltip',
      list: [{ value: 'test1' }, { value: 'test2' }, { value: 'test3' }],
      currentIndex: 1,
      onChangeMouseWheel: jest.fn(),
    };
    const { getByTestId } = render(
      <ThemeWrapper>
        <TooltipSelect {...props} data-testid="testing">
          <MenuItem>empty</MenuItem>
        </TooltipSelect>
      </ThemeWrapper>,
    );
    const select = getByTestId('testing');

    fireEvent.wheel(select, { deltaY: 1 });
    expect(props.onChangeMouseWheel).toHaveBeenCalledTimes(1);
    expect(props.onChangeMouseWheel).toHaveBeenCalledWith({
      value: 'test3',
    });

    fireEvent.wheel(select, { deltaY: -1 });
    expect(props.onChangeMouseWheel).toHaveBeenCalledTimes(2);
    expect(props.onChangeMouseWheel).toHaveBeenCalledWith({
      value: 'test1',
    });
  });

  it('should hide tooltip, when select component is closed', async () => {
    const props = {
      tooltip: 'test tooltip',
    };

    const { queryByRole, getByTestId } = render(
      <ThemeWrapper>
        <Box data-testid="wrapper">
          <TooltipSelect {...props} data-testid="testing">
            <MenuItem>empty</MenuItem>
          </TooltipSelect>
        </Box>
      </ThemeWrapper>,
    );

    expect(queryByRole('tooltip')).toBeFalsy();

    const button = queryByRole('button');
    await waitFor(() => fireEvent.focus(button));
    expect(queryByRole('tooltip')).toBeTruthy();

    await waitFor(() => fireEvent.mouseDown(button));
    expect(queryByRole('tooltip')).toBeFalsy();

    const wrapperComponent = getByTestId('wrapper');
    await waitFor(() => fireEvent.mouseDown(wrapperComponent));
    expect(queryByRole('tooltip')).toBeFalsy();
  });
});
