/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Button, Typography } from '@mui/material';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import { usePoller } from './usePoller';

export const usePollerDemo = (): React.ReactElement => {
  const [count, setCount] = React.useState(0);

  usePoller(
    [count],
    () => {
      setCount(count + 1);
    },
    10000,
  );

  return (
    <ThemeWrapperOldTheme>
      <Typography variant="subtitle1">
        <b>Using the poller, this counter will increase every 10 seconds</b>
      </Typography>
      <Typography variant="body2">Count: {count}</Typography>
      <br />
      <br />
      <Typography variant="subtitle1">
        Pressing the button will add 2 and reset the timer back to 0
      </Typography>
      <Button
        color="secondary"
        variant="contained"
        onClick={(): void => setCount(count + 2)}
      >
        Click here
      </Button>
    </ThemeWrapperOldTheme>
  );
};
