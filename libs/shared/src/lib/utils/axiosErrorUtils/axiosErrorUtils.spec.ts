/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { AxiosError, AxiosResponseHeaders } from 'axios';
import {
  DEFAULT_ERROR_MESSAGE,
  getAxiosErrorMessage,
  getAxiosWarningMessage,
  getErrorMessage,
  getWarningMessage,
} from './axiosErrorUtils';

const fakeBackendError: AxiosError = {
  isAxiosError: true,
  config: undefined,
  toJSON: undefined,
  name: 'API error',
  message: '',
  response: {
    data: 'Unable to store data',
    status: 400,
    statusText: '',
    config: undefined,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

const fakeBackendNestedError: AxiosError<{ message: string }> = {
  isAxiosError: true,
  config: undefined,
  toJSON: undefined,
  name: 'API error',
  message: '',
  response: {
    data: { message: 'Unable to store nested data' },
    status: 400,
    statusText: '',
    config: undefined,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

const fakeTimedOutMessage: AxiosError = {
  isAxiosError: true,
  config: undefined,
  toJSON: undefined,
  name: 'API error',
  message: 'Timed out error',
  response: {
    data: {},
    status: 400,
    statusText: '',
    config: undefined,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

const fakeBackendDifferentError: AxiosError = {
  isAxiosError: true,
  config: undefined,
  toJSON: undefined,
  name: 'API error',
  message: '',
  response: {
    data: { errorMessage: { innerErrorMessage: 'Unable to store data' } },
    status: 400,
    statusText: '',
    config: undefined,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

const fakeBackendWarning: AxiosError<{ WarningMessage: string }> = {
  isAxiosError: true,
  config: undefined,
  toJSON: undefined,
  name: 'API error',
  message: '',
  response: {
    data: { WarningMessage: 'Example warning' },
    status: 400,
    statusText: '',
    config: undefined,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

describe('AxiosErrorMessage', () => {
  describe('getAxiosErrorMessage', () => {
    it('should return the error message or a default string', () => {
      expect(getAxiosErrorMessage(fakeBackendError)).toEqual(
        fakeBackendError.response.data,
      );
      expect(getAxiosErrorMessage(fakeBackendNestedError)).toEqual(
        fakeBackendNestedError.response.data.message,
      );
      expect(getAxiosErrorMessage(fakeTimedOutMessage)).toEqual(
        fakeTimedOutMessage.message,
      );
      expect(getAxiosErrorMessage(fakeBackendDifferentError)).toEqual(
        DEFAULT_ERROR_MESSAGE,
      );
    });
  });
});
describe('getErrorMessage', () => {
  it('should return error message of non axios errors', () => {
    expect(getErrorMessage(new Error('oops'))).toEqual('oops');
    expect(getErrorMessage(new Error())).toEqual('An error has occurred');
  });

  it('should return error message of axios errors', () => {
    expect(getErrorMessage(fakeBackendError)).toEqual(
      fakeBackendError.response.data,
    );
    expect(getErrorMessage(fakeBackendNestedError)).toEqual(
      fakeBackendNestedError.response.data.message,
    );
    expect(getErrorMessage(fakeTimedOutMessage)).toEqual(
      fakeTimedOutMessage.message,
    );
    expect(getErrorMessage(fakeBackendDifferentError)).toEqual(
      DEFAULT_ERROR_MESSAGE,
    );
  });
});
describe('getAxiosWarningMessage', () => {
  it('should return the WarningMessage or an empty string', () => {
    expect(getAxiosWarningMessage(fakeBackendError)).toEqual('');
    expect(getAxiosWarningMessage(fakeBackendWarning)).toEqual(
      fakeBackendWarning.response.data.WarningMessage,
    );
  });
});
describe('getWarningMessage', () => {
  it('should return the WarningMessage or an empty string', () => {
    expect(getWarningMessage(fakeBackendError)).toEqual('');
    expect(getWarningMessage(new Error('oops'))).toEqual('');
    expect(getWarningMessage(fakeBackendWarning)).toEqual(
      fakeBackendWarning.response.data.WarningMessage,
    );
  });
});
