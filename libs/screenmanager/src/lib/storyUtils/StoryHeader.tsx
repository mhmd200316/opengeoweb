/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Button, MenuItem, Typography } from '@mui/material';

import { TooltipSelect } from '@opengeoweb/shared';
import { ScreenManagerPreset } from '../store/types';

interface HeaderProps {
  screenPreset: ScreenManagerPreset;
  screenPresets: ScreenManagerPreset[];
  changeScreenPreset: (newScreenPreset: ScreenManagerPreset) => void;
  openSyncGroups: (open: boolean) => void;
  autoArrange: () => void;
}

const StoryHeader: React.FC<HeaderProps> = ({
  screenPreset,
  screenPresets,
  changeScreenPreset,
  openSyncGroups,
  autoArrange,
}: HeaderProps) => {
  return (
    <div style={{ padding: '8px 20px' }}>
      <Typography style={{ color: 'white', display: 'inline' }} gutterBottom>
        Screen manager demo
      </Typography>
      <Button
        style={{ color: 'white', float: 'right' }}
        onClick={(): void => {
          autoArrange();
        }}
      >
        Auto Arrange Windows
      </Button>
      <Button
        style={{ color: 'white', float: 'right' }}
        data-testid="openSyncGroups"
        onClick={(): void => {
          openSyncGroups(true);
        }}
      >
        Open SyncGroups
      </Button>
      <Button
        style={{ color: 'white', float: 'right' }}
        onClick={(): void => {
          changeScreenPreset(screenPreset);
        }}
      >
        Reload view
      </Button>
      <TooltipSelect
        variant="standard"
        style={{ color: 'white', float: 'right' }}
        noStyling={true}
        tooltip="Choose a screen preset"
        value={screenPreset.id}
        onChange={(event): void => {
          changeScreenPreset(
            screenPresets.filter(
              (screenPreset) => screenPreset.id === event.target.value,
            )[0],
          );
        }}
        inputProps={{
          SelectDisplayProps: {
            'data-testid': 'selectScreenPreset',
          },
        }}
      >
        {screenPresets.map((preset) => (
          <MenuItem key={preset.id} value={preset.id}>
            {preset.title}
          </MenuItem>
        ))}
      </TooltipSelect>
    </div>
  );
};

export default StoryHeader;
