/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Provider } from 'react-redux';
import { DynamicModuleLoader } from 'redux-dynamic-modules';
import { darkTheme, lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { coreModuleConfig } from '@opengeoweb/core';
import { ApiProvider } from '@opengeoweb/api';
import { createFakeApi as createFakeTafApi } from '@opengeoweb/taf';

import { getScreenPresetsAsObject } from '../utils/jsonPresetFilter';
import defaultScreenPresets from '../utils/screenPresets.json';
import { store } from '../store';
import { InitSyncGroups } from '../components/InitSyncGroups';
import screenManagerModuleModuleConfig from '../store/config';
import { ScreenManagerViewConnect } from '../components/ScreenManagerView';
import { ScreenManagerPreset } from '../store/types';
import { componentsLookUp } from './componentsLookUp';

const {
  screenConfigRadarTemp,
  screenConfigHarmonie,
  screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager,
  screenConfigHarmoniePresetWithoutMultiMapLayerManager,
  screenConfigRadAndObs,
  screenConfigTaf,
  screenConfigActions,
} = getScreenPresetsAsObject(defaultScreenPresets as ScreenManagerPreset[]);

export default {
  title: 'Examples/componentType examples',
};

interface ComponentTypeDemoProps {
  screenPreset: ScreenManagerPreset;
}

const ComponentTypeDemo: React.FC<ComponentTypeDemoProps> = ({
  screenPreset,
}: ComponentTypeDemoProps): React.ReactElement => {
  return (
    <Provider store={store}>
      <DynamicModuleLoader
        modules={[...coreModuleConfig, screenManagerModuleModuleConfig]}
      >
        <InitSyncGroups screenPresets={[screenPreset]} />
        <div
          style={{
            height: '100vh',
          }}
        >
          <ApiProvider
            baseURL="fake"
            appURL="fake"
            auth={{
              username: 'string',
              token: 'string',
              refresh_token: 'string',
            }}
            onSetAuth={(): void => null}
            createApi={createFakeTafApi}
            authTokenUrl="fake"
            authClientId="fakeid"
          >
            <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
          </ApiProvider>
        </div>
      </DynamicModuleLoader>
    </Provider>
  );
};

// each demo has a preset with different componentType prop
export const MapLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <ComponentTypeDemo screenPreset={screenConfigRadarTemp} />
  </ThemeWrapper>
);

export const MapDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <ComponentTypeDemo screenPreset={screenConfigRadarTemp} />
  </ThemeWrapper>
);

export const HarmonieTempAndPrecipPresetLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <ComponentTypeDemo
      screenPreset={screenConfigHarmoniePresetWithoutMultiMapLayerManager}
    />
  </ThemeWrapper>
);

export const HarmonieTempAndPrecipPresetDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <ComponentTypeDemo
      screenPreset={screenConfigHarmoniePresetWithoutMultiMapLayerManager}
    />
  </ThemeWrapper>
);

export const ModelRunIntervalLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <ComponentTypeDemo
      screenPreset={
        screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager
      }
    />
  </ThemeWrapper>
);

export const ModelRunIntervalDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <ComponentTypeDemo
      screenPreset={
        screenConfigHarmoniePrecipMSLRefTimePresetWithoutMultiMapLayerManager
      }
    />
  </ThemeWrapper>
);

export const MultiMapLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <ComponentTypeDemo screenPreset={screenConfigRadAndObs} />
  </ThemeWrapper>
);

export const MultiMapDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <ComponentTypeDemo screenPreset={screenConfigRadAndObs} />
  </ThemeWrapper>
);

export const TimeSliderLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <ComponentTypeDemo screenPreset={screenConfigHarmonie} />
  </ThemeWrapper>
);

export const TimeSliderDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <ComponentTypeDemo screenPreset={screenConfigHarmonie} />
  </ThemeWrapper>
);

export const TafModuleLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <ComponentTypeDemo screenPreset={screenConfigTaf} />
  </ThemeWrapper>
);

export const TafModuleDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <ComponentTypeDemo screenPreset={screenConfigTaf} />
  </ThemeWrapper>
);

export const ActionExamples = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <ComponentTypeDemo screenPreset={screenConfigActions} />
  </ThemeWrapper>
);
