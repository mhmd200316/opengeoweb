/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { LayerType } from '@opengeoweb/webmap';
import StoryHeader from './StoryHeader';
import { ScreenManagerPreset } from '../store/types';
import { TestWrapper } from '../utils/testUtils';

describe('components/storyUtils/StoryHeader', () => {
  const screenPresets: ScreenManagerPreset[] = [
    {
      id: 'screenConfigA',
      title: 'Screen preset A',
      views: {
        byId: {
          screenA: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [
                  {
                    service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                    name: 'RAD_NL25_PCP_CM',
                    format: 'image/png',
                    enabled: true,
                    style: 'knmiradar/nearest',
                    id: 'test',
                    layerType: LayerType.mapLayer,
                  },
                ],
              },
              syncGroupsIds: ['Area_A', 'Time_A'],
            },
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
    },
    {
      id: 'screenConfigB',
      title: 'Screen preset B',
      views: {
        byId: {
          screenA: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [
                  {
                    service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                    name: 'RAD_NL25_PCP_CM',
                    format: 'image/png',
                    enabled: true,
                    style: 'knmiradar/nearest',
                    id: 'test',
                    layerType: LayerType.mapLayer,
                  },
                ],
              },
              syncGroupsIds: ['Area_A', 'Time_A'],
            },
          },
        },
        allIds: ['screenA'],
      },
      mosaicNode: 'screenA',
    },
  ];
  const props = {
    screenPreset: screenPresets[0],
    screenPresets,
    changeScreenPreset: jest.fn(),
    openSyncGroups: jest.fn(),
    addScreenManagerView: (): void => {},
    autoArrange: jest.fn(),
  };
  it('should trigger selection of another screen preset', async () => {
    const { getByTestId, findByText } = render(
      <TestWrapper>
        <StoryHeader {...props} />
      </TestWrapper>,
    );
    const valueSelect = getByTestId('selectScreenPreset');
    fireEvent.mouseDown(valueSelect);
    const menuItemValue = await findByText('Screen preset B');
    await waitFor(() => fireEvent.click(menuItemValue));
    expect(props.changeScreenPreset).toHaveBeenCalled();
  });

  it('should trigger syncgroup dialog', async () => {
    const { getByTestId } = render(
      <TestWrapper>
        <StoryHeader {...props} />
      </TestWrapper>,
    );
    const valueSelect = getByTestId('selectScreenPreset');
    fireEvent.mouseDown(valueSelect);
    const select = getByTestId('openSyncGroups');
    await waitFor(() => fireEvent.click(select));
    expect(props.openSyncGroups).toHaveBeenCalledTimes(1);
  });
});
