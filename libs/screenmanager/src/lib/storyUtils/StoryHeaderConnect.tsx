/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppStore } from '../types/types';
import { screenManagerActions } from '../store/reducer';
import * as screenManagerSelectors from '../store/selectors';
import { ScreenManagerPreset } from '../store/types';
import StoryHeader from './StoryHeader';
import { createBalancedMosaicNode } from '../components/ScreenManagerView/helpers';

interface StoryHeaderConnectProps {
  screenPresets: ScreenManagerPreset[];
  openSyncGroups: (open: boolean) => void;
}

const StoryHeaderConnect: React.FC<StoryHeaderConnectProps> = ({
  screenPresets,
  openSyncGroups,
}: StoryHeaderConnectProps) => {
  const dispatch = useDispatch();
  const screenPreset = useSelector((store: AppStore) =>
    screenManagerSelectors.getScreenManagerState(store),
  );

  const changePreset = useCallback(
    (newScreenPreset: ScreenManagerPreset): void => {
      dispatch(
        screenManagerActions.changePreset({
          screenManagerPreset: newScreenPreset,
        }),
      );
    },
    [dispatch],
  );

  const mosaicNode = useSelector((store: AppStore) =>
    screenManagerSelectors.getMosaicNode(store),
  );

  const autoArrange = useCallback((): void => {
    dispatch(
      screenManagerActions.updateScreenManagerViews({
        mosaicNode: createBalancedMosaicNode(mosaicNode),
      }),
    );
  }, [dispatch, mosaicNode]);

  return (
    <StoryHeader
      screenPresets={screenPresets}
      screenPreset={screenPreset}
      changeScreenPreset={changePreset}
      openSyncGroups={openSyncGroups}
      autoArrange={autoArrange}
    />
  );
};

export default StoryHeaderConnect;
