/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  componentsLookUp as coreComponentsLookUp,
  ComponentsLookUpPayload as CoreComponentsLookUpPayload,
} from '@opengeoweb/core';
import {
  componentsLookUp as timeseriesComponentsLookUp,
  ComponentsLookUpPayload as TimeseriesComponentsLookUpPayload,
} from '@opengeoweb/timeseries';
import { Typography } from '@mui/material';
import {
  TafModule,
  ComponentsLookUpPayload as TafComponentsLookUpPayload,
} from '@opengeoweb/taf';
import ActionExampleComponent from './ActionsExampleComponent';
import { ComponentsLookUpWrapper } from '../components/ComponentsLookUpWrapper';

// taf module without API
const tafComponentsLookUp = (
  payload: TafComponentsLookUpPayload,
): React.ReactElement => {
  const { componentType } = payload;

  switch (componentType) {
    case 'TafModule': {
      return (
        <ComponentsLookUpWrapper component={TafModule} payload={payload} />
      );
    }
    default:
      return null;
  }
};

/**
 * The lookup table is for registering your own components with the screenmanager.
 * @param payload
 * @returns
 */
export const componentsLookUp = (
  payload:
    | CoreComponentsLookUpPayload
    | TimeseriesComponentsLookUpPayload
    | TafComponentsLookUpPayload,
): React.ReactElement => {
  const coreComponent = coreComponentsLookUp(
    payload as CoreComponentsLookUpPayload,
  );
  const timeseriesComponent = timeseriesComponentsLookUp(
    payload as TimeseriesComponentsLookUpPayload,
  );

  const tafComponent = tafComponentsLookUp(
    payload as TafComponentsLookUpPayload,
  );

  const customPanelLookup = ({
    componentType,
    initialProps,
    id,
  }): React.ReactElement => {
    switch (componentType) {
      case 'CustomPanel':
        return (
          <>
            <Typography>{id}</Typography>
            <br />
            <Typography>{`This panel uses minimum ${
              initialProps.minRows || 5
            } grid rows and ${
              initialProps.minCols || 1
            } grid columns.`}</Typography>
            {initialProps.minCols && (
              <Typography>
                This would be useful for a form to ensure a minimum width and/or
                height when resizing.
              </Typography>
            )}
          </>
        );

      case 'ActionsExample':
        return <ActionExampleComponent viewId={id} />;

      default:
        return null;
    }
  };

  const customPanelComponent = customPanelLookup(payload);

  return (
    coreComponent || timeseriesComponent || tafComponent || customPanelComponent
  );
};
