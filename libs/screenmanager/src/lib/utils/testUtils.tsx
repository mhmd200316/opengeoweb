/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { ThemeWrapper } from '@opengeoweb/theme';
import { Provider } from 'react-redux';
import { Store } from '@reduxjs/toolkit';

interface TestWrapperProps {
  children?: React.ReactNode;
}
export const TestWrapper: React.FC<TestWrapperProps> = ({
  children,
}: TestWrapperProps) => <ThemeWrapper>{children}</ThemeWrapper>;

interface TestWrapperWithStoreProps extends TestWrapperProps {
  store: Store;
}
export const TestWrapperWithStore: React.FC<TestWrapperWithStoreProps> = ({
  children,
  store,
}: TestWrapperWithStoreProps) => (
  <Provider store={store}>
    <ThemeWrapper>{children}</ThemeWrapper>;
  </Provider>
);
