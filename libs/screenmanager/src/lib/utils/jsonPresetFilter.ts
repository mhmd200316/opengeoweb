/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { ScreenManagerPreset } from '../store/types';

export const sortByPresetTitle = (
  presetArray: ScreenManagerPreset[],
): ScreenManagerPreset[] => {
  const sortedArray = [...presetArray].sort((a, b) => {
    const presetTitleA = a.title.toUpperCase();
    const presetTitleB = b.title.toUpperCase();
    if (presetTitleA < presetTitleB) return -1;
    if (presetTitleA > presetTitleB) return 1;
    return 0;
  });

  const foundPreset = sortedArray.find(
    (preset) => preset.title === 'Empty Map',
  );
  if (foundPreset) {
    const editedArray = sortedArray.filter((preset) => preset !== foundPreset);
    editedArray.unshift(foundPreset);
    return editedArray;
  }
  return sortedArray;
};

/* Return screen presets or de fault presets sorted by preset title */
export const getScreenPresets = (
  screenPresets: ScreenManagerPreset[],
  defaultScreenPresets: ScreenManagerPreset[],
  enableSorting = true,
): ScreenManagerPreset[] => {
  if (screenPresets && screenPresets.length > 0) {
    const presets = JSON.parse(JSON.stringify(screenPresets));
    return enableSorting ? sortByPresetTitle(presets) : presets;
  }
  const defaultPresets = JSON.parse(JSON.stringify(defaultScreenPresets));
  return enableSorting ? sortByPresetTitle(defaultPresets) : defaultPresets;
};

/*  Return first screen preset from the array or first preset from the default presets */
export const getInitialScreenPreset = (
  screenPresets: ScreenManagerPreset[],
  defaultScreenPresets: ScreenManagerPreset[],
): ScreenManagerPreset[] => {
  if (screenPresets && screenPresets.length > 0) {
    const presets = JSON.parse(JSON.stringify(screenPresets));
    return [presets[0]];
  }
  const defaultPresets = JSON.parse(JSON.stringify(defaultScreenPresets));
  return [defaultPresets[0]];
};

/* Returns presets as object */
export const getScreenPresetsAsObject = (
  presets: ScreenManagerPreset[],
): Record<string, ScreenManagerPreset> =>
  presets.reduce(
    (filteredPreset, preset) => ({
      ...filteredPreset,
      [preset.id]: preset,
    }),
    {},
  );
