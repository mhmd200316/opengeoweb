/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import {
  getScreenPresets,
  getScreenPresetsAsObject,
  sortByPresetTitle,
} from './jsonPresetFilter';
import { ScreenManagerPreset } from '../store/types';
import defaultScreenPresets from './jsonPresetFilterTestScreenPresets.json';

describe('utils/JsonPresetFilter', () => {
  const multiplePresets: ScreenManagerPreset[] = [
    {
      id: 'anotherMap',
      title: 'Anohter Map',
      views: {
        allIds: ['anotherMapView'],
        byId: {
          anotherMapView: {
            title: 'Another Map',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [],
                proj: {
                  bbox: {
                    left: 58703.6377,
                    bottom: 6408480.4514,
                    right: 3967387.5161,
                    top: 11520588.9031,
                  },
                  srs: 'EPSG:3857',
                },
              },
            },
          },
        },
      },
      mosaicNode: 'emptyMapView',
      viewType: 'singleWindow',
    },
    {
      id: 'emptyMap',
      title: 'Empty Map',
      views: {
        allIds: ['emptyMapView'],
        byId: {
          emptyMapView: {
            title: 'Empty Map',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [],
                proj: {
                  bbox: {
                    left: 58703.6377,
                    bottom: 6408480.4514,
                    right: 3967387.5161,
                    top: 11520588.9031,
                  },
                  srs: 'EPSG:3857',
                },
              },
            },
          },
        },
      },
      mosaicNode: 'emptyMapView',
      viewType: 'singleWindow',
    },
  ];
  describe('getScreenPresets', () => {
    it('should sort first item in array of presets to be Empty Map', () => {
      const sortedPresets = sortByPresetTitle(multiplePresets);
      expect(sortedPresets[0].title).toEqual('Empty Map');
    });

    it('should return default screen presets if presets is null', () => {
      expect(
        getScreenPresets(null, defaultScreenPresets as ScreenManagerPreset[]),
      ).toEqual(
        sortByPresetTitle(defaultScreenPresets as ScreenManagerPreset[]),
      );
    });

    it('should return sorted presets', () => {
      const testRow = [
        {
          title: 'cTest',
        },
        {
          title: 'aTest',
        },
        {
          title: 'bTest',
        },
      ];
      expect(getScreenPresets(null, testRow as ScreenManagerPreset[])).toEqual([
        {
          title: 'aTest',
        },
        {
          title: 'bTest',
        },
        {
          title: 'cTest',
        },
      ]);
    });

    it('should return unsorted presets', () => {
      const testRow = [
        {
          title: 'cTest',
        },
        {
          title: 'aTest',
        },
        {
          title: 'bTest',
        },
      ];
      expect(
        getScreenPresets(null, testRow as ScreenManagerPreset[], false),
      ).toEqual(testRow);
    });
  });

  describe('getScreenPresetsAsObject', () => {
    it('should create an object of array of presets', () => {
      expect(getScreenPresetsAsObject(multiplePresets)).toEqual({
        [multiplePresets[0].id]: multiplePresets[0],
        [multiplePresets[1].id]: multiplePresets[1],
      });
    });
  });
});
