/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Provider } from 'react-redux';
import { DynamicModuleLoader } from 'redux-dynamic-modules';
import { Box } from '@mui/material';

import { darkTheme, lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import {
  coreModuleConfig,
  LayerManagerConnect,
  SyncGroupViewerConnect,
  publicServices,
  MultiMapDimensionSelectConnect,
  uiActions,
} from '@opengeoweb/core';
import { ApiProvider } from '@opengeoweb/api';
import { createFakeApi as createFakeTafApi } from '@opengeoweb/taf';

import StoryHeaderConnect from './storyUtils/StoryHeaderConnect';
import { store } from './store';
import { InitSyncGroups } from './components/InitSyncGroups';
import screenManagerModuleModuleConfig from './store/config';
import { ScreenManagerViewConnect } from './components/ScreenManagerView';
import { componentsLookUp } from './storyUtils/componentsLookUp';
import defaultScreenPresets from './utils/screenPresets.json';
import { ScreenManagerPreset } from './store/types';
import { getScreenPresets } from './utils/jsonPresetFilter';

export default {
  title: 'components/ScreenManager',
};

const ScreenManagerDemo = (): React.ReactElement => {
  const screenPresets = getScreenPresets(
    null,
    defaultScreenPresets as ScreenManagerPreset[],
    false,
  );

  const openSyncgroupsDialog = React.useCallback(() => {
    store.dispatch(
      uiActions.setToggleOpenDialog({ type: 'syncGroups', setOpen: true }),
    );
  }, []);

  return (
    <Provider store={store}>
      <DynamicModuleLoader
        modules={[...coreModuleConfig, screenManagerModuleModuleConfig]}
      >
        <InitSyncGroups screenPresets={screenPresets} />
        <SyncGroupViewerConnect />
        <Box
          sx={{
            display: 'grid',
            gridTemplateAreas: '"header"\n "content"\n',
            gridTemplateColumns: '1fr',
            gridTemplateRows: '40px 1fr',
            gridGap: '0px',
            height: '100vh',
            backgroundColor: 'geowebColors.background.surfaceBrowser',
          }}
        >
          <Box
            sx={{
              gridArea: 'header',
              backgroundColor: 'geowebColors.brand.brand',
              border: 'none',
              boxShadow: 1,
            }}
          >
            <StoryHeaderConnect
              screenPresets={screenPresets}
              openSyncGroups={(): void => {
                openSyncgroupsDialog();
              }}
            />
          </Box>
          <Box
            sx={{
              gridArea: 'content',
              height: '100%',
              width: '100%',
              margin: 0,
              position: 'relative',
            }}
          >
            <MultiMapDimensionSelectConnect />
            <LayerManagerConnect
              showTitle
              preloadedBaseServices={[publicServices.KNMIgeoservicesBaselayers]}
              preloadedMapServices={[
                publicServices.KNMIgeoservicesRadar,
                publicServices.KNMIgeoservicesHarmonie,
                publicServices.KNMIgeoservicesHarmonieMLService,
                publicServices.KNMIgeoservicesObs,
                publicServices.DWD,
                publicServices.KNMIgeoservicesBaselayers,
              ]}
            />
            <ApiProvider
              baseURL="fake"
              appURL="fake"
              auth={{
                username: 'string',
                token: 'string',
                refresh_token: 'string',
              }}
              onSetAuth={(): void => null}
              createApi={createFakeTafApi}
              authTokenUrl="fake"
              authClientId="fakeid"
            >
              <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
            </ApiProvider>
          </Box>
        </Box>
      </DynamicModuleLoader>
    </Provider>
  );
};

export const ScreenManagerDemoLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <ScreenManagerDemo />
  </ThemeWrapper>
);

export const ScreenManagerDemoDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <ScreenManagerDemo />
  </ThemeWrapper>
);
