/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { fireEvent, render } from '@testing-library/react';
import { ComponentsLookUpWrapper } from './ComponentsLookUpWrapper';
import { screenManagerActions } from '../../store/reducer';

interface TestModuleComponentProps {
  onPreventCloseView: (on: boolean) => void;
}

const TestModuleComponent: React.FC<TestModuleComponentProps> = ({
  onPreventCloseView,
}: TestModuleComponentProps) => {
  const [shouldPreventClose, togglePreventClose] =
    React.useState<boolean>(true);
  const onClick = (): void => {
    onPreventCloseView(shouldPreventClose);
    togglePreventClose(!shouldPreventClose);
  };

  return (
    <div>
      <button type="button" onClick={onClick}>
        test button
      </button>
    </div>
  );
};

describe('screenmanager/components/ComponentsLookUpWrapper', () => {
  it('should wrap a component and fire default actions', () => {
    const mockState = {};
    const mockStore = configureStore();
    const store = mockStore(mockState);

    const testId = 'test-1';
    const testPayload = {
      id: testId,
      componentType: 'test',
      initialProps: {},
    };

    const { getByRole } = render(
      <Provider store={store}>
        <ComponentsLookUpWrapper
          component={TestModuleComponent}
          payload={testPayload}
          shouldPreventClose={true}
        />
      </Provider>,
    );

    expect(store.getActions()).toHaveLength(0);

    fireEvent.click(getByRole('button'));

    expect(store.getActions()).toHaveLength(1);
    expect(store.getActions()[0]).toEqual(
      screenManagerActions.setPreventCloseView({
        viewId: testId,
        shouldPreventClose: true,
      }),
    );

    fireEvent.click(getByRole('button'));
    expect(store.getActions()).toHaveLength(2);
    expect(store.getActions()[1]).toEqual(
      screenManagerActions.setPreventCloseView({
        viewId: testId,
        shouldPreventClose: false,
      }),
    );
  });
});
