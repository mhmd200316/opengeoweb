/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { MosaicDirection } from 'react-mosaic-component';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { SyncGroups } from '@opengeoweb/core';
import { InitSyncGroups } from './InitSyncGroups';
import { TestWrapperWithStore } from '../../utils/testUtils';
import { initialState, screenManagerActions } from '../../store/reducer';

import { ScreenManagerPreset } from '../../store/types';
import { getScreenPresetsAsObject } from '../../utils/jsonPresetFilter';
import defaultScreenPresets from '../../utils/screenPresets.json';

const { screenConfigOnlyBBOXGroup, screenConfigOnlyTIMEGroup } =
  getScreenPresetsAsObject(defaultScreenPresets as ScreenManagerPreset[]);

const { SYNCGROUPS_TYPE_SETBBOX, SYNCGROUPS_TYPE_SETTIME } =
  SyncGroups.constants;

const { syncGroupAddGroup, syncGroupAddTarget } = SyncGroups.actions;

const screenPresetA: ScreenManagerPreset = {
  id: 'screenConfigA',
  title: 'Screen preset A',
  views: {
    byId: {
      screenA: {
        title: 'Radar view',
        componentType: 'Map',
        initialProps: {
          mapPreset: {
            layers: [],
          },
          syncGroupsIds: ['Area_A', 'Time_A'],
        },
      },
      screenB: {
        title: 'Satellite view',
        componentType: 'Map',
        initialProps: {
          mapPreset: {
            layers: [],
          },
          syncGroupsIds: ['Area_A', 'Time_A'],
        },
      },
      screenC: {
        title: 'Satellite view',
        componentType: 'Map',
        initialProps: {
          mapPreset: {
            layers: [],
          },
          syncGroupsIds: [],
        },
      },
    },
    allIds: ['screenA', 'screenB'],
  },
  syncGroups: [
    {
      id: 'Area_A',
      type: 'SYNCGROUPS_TYPE_SETBBOX',
    },
    {
      id: 'Time_A',
      type: 'SYNCGROUPS_TYPE_SETTIME',
    },
  ],
  mosaicNode: {
    direction: 'row' as MosaicDirection,
    first: 'screenA',
    second: {
      direction: 'column' as MosaicDirection,
      first: 'screenB',
      second: 'screenC',
    },
  },
};

const screenPresetBrokenWithoutSyncgroupIds = {
  id: 'screenConfigA',
  title: 'Screen preset A',
  views: {
    byId: {
      screenA: {
        title: 'Radar view',
        componentType: 'Map',
        initialProps: {
          mapPreset: {
            layers: [],
          },
        },
      },
      screenB: {
        title: 'Satellite view',
        componentType: 'Map',
        initialProps: {
          mapPreset: {
            layers: [],
          },
          syncGroupsIds: 'notanarray',
        },
      },
      screenC: {
        title: 'Satellite view',
        componentType: 'Map',
        initialProps: {
          mapPreset: {
            layers: [],
          },
          syncGroupsIds: [],
        },
      },
    },
    allIds: ['screenA', 'screenB'],
  },
  mosaicNode: {
    direction: 'row' as MosaicDirection,
    first: 'screenA',
    second: {
      direction: 'column' as MosaicDirection,
      first: 'screenB',
      second: 'screenC',
    },
  },
};

describe('components/InitSyncGroups', () => {
  it('should set the first preset by default', () => {
    const mockState = {
      screenManagerStore: initialState,
      syncronizationGroupStore: SyncGroups.initialState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const { container } = render(
      <TestWrapperWithStore store={store}>
        <InitSyncGroups
          screenPresets={[
            screenPresetA,
            { ...screenPresetA, id: 'screenConfigB' },
          ]}
        />
      </TestWrapperWithStore>,
    );
    expect(container).toBeTruthy();

    const expectedPresetAction = screenManagerActions.changePreset({
      screenManagerPreset: screenPresetA,
    });
    expect(store.getActions()).toContainEqual(expectedPresetAction);
  });

  it('should add sync group targets to the store for each screen', () => {
    const mockState = {
      screenManagerStore: screenPresetA,
      syncronizationGroupStore: SyncGroups.initialState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const { container } = render(
      <TestWrapperWithStore store={store}>
        <InitSyncGroups screenPresets={[screenPresetA]} />
      </TestWrapperWithStore>,
    );
    expect(container).toBeTruthy();

    const expectedAddTargetAction1 = syncGroupAddTarget({
      groupId:
        screenPresetA.views.byId[screenPresetA.views.allIds[0]].initialProps
          .syncGroupsIds[0],
      targetId: screenPresetA.views.allIds[0],
      linked: true,
    });

    const expectedAddTargetAction2 = syncGroupAddTarget({
      groupId:
        screenPresetA.views.byId[screenPresetA.views.allIds[0]].initialProps
          .syncGroupsIds[1],
      targetId: screenPresetA.views.allIds[0],
      linked: true,
    });

    const expectedAddTargetAction3 = syncGroupAddTarget({
      groupId:
        screenPresetA.views.byId[screenPresetA.views.allIds[1]].initialProps
          .syncGroupsIds[0],
      targetId: screenPresetA.views.allIds[1],
      linked: true,
    });

    const expectedAddTargetAction4 = syncGroupAddTarget({
      groupId:
        screenPresetA.views.byId[screenPresetA.views.allIds[1]].initialProps
          .syncGroupsIds[1],
      targetId: screenPresetA.views.allIds[1],
      linked: true,
    });
    expect(store.getActions()).toContainEqual(expectedAddTargetAction1);
    expect(store.getActions()).toContainEqual(expectedAddTargetAction2);
    expect(store.getActions()).toContainEqual(expectedAddTargetAction3);
    expect(store.getActions()).toContainEqual(expectedAddTargetAction4);
  });

  it('should not fail if invalid initSyncgroups is provided via the preset', () => {
    const mockState = {
      screenManagerStore: screenPresetBrokenWithoutSyncgroupIds,
      syncronizationGroupStore: SyncGroups.initialState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const { container } = render(
      <TestWrapperWithStore store={store}>
        <InitSyncGroups
          screenPresets={[screenPresetBrokenWithoutSyncgroupIds]}
        />
      </TestWrapperWithStore>,
    );
    expect(container).toBeTruthy();

    expect(store.getActions()).not.toContain(
      SyncGroups.actions.syncGroupAddTarget.type,
    );
  });

  it('should add sync groups for area and time for preset with only area', () => {
    const mockState = {
      screenManagerStore: screenConfigOnlyBBOXGroup,
      syncronizationGroupStore: SyncGroups.initialState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const { container } = render(
      <TestWrapperWithStore store={store}>
        <InitSyncGroups screenPresets={[screenConfigOnlyBBOXGroup]} />
      </TestWrapperWithStore>,
    );
    expect(container).toBeTruthy();

    const expectedAddGroupAction1 = syncGroupAddGroup({
      groupId: screenConfigOnlyBBOXGroup.syncGroups[0].id,
      title: screenConfigOnlyBBOXGroup.syncGroups[0].id,
      type: SYNCGROUPS_TYPE_SETBBOX,
    });

    const expectedAddGroupAction2 = syncGroupAddGroup({
      groupId: 'SYNCGROUPS_TYPE_SETTIME_A',
      title: 'Default group for SYNCGROUPS_TYPE_SETTIME_A',
      type: SYNCGROUPS_TYPE_SETTIME,
    });

    expect(store.getActions()).toContainEqual(expectedAddGroupAction1);
    expect(store.getActions()).toContainEqual(expectedAddGroupAction2);
  });

  it('should add sync groups for area and time for preset with only time', () => {
    const mockState = {
      screenManagerStore: screenConfigOnlyTIMEGroup,
      syncronizationGroupStore: SyncGroups.initialState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const { container } = render(
      <TestWrapperWithStore store={store}>
        <InitSyncGroups screenPresets={[screenConfigOnlyTIMEGroup]} />
      </TestWrapperWithStore>,
    );
    expect(container).toBeTruthy();

    const expectedAddGroupAction1 = syncGroupAddGroup({
      groupId: screenConfigOnlyTIMEGroup.syncGroups[0].id,
      title: screenConfigOnlyTIMEGroup.syncGroups[0].id,
      type: SYNCGROUPS_TYPE_SETTIME,
    });

    const expectedAddGroupAction2 = syncGroupAddGroup({
      groupId: 'SYNCGROUPS_TYPE_SETBBOX_A',
      title: 'Default group for SYNCGROUPS_TYPE_SETBBOX_A',
      type: SYNCGROUPS_TYPE_SETBBOX,
    });

    expect(store.getActions()).toContainEqual(expectedAddGroupAction1);
    expect(store.getActions()).toContainEqual(expectedAddGroupAction2);
  });

  it('should not add sync groups if id is not set', () => {
    const screenConfigNoIdForSyncGroup: ScreenManagerPreset = {
      id: 'screenConfigOnlyBBOXGroup',
      title: 'SyncGroups only BBOX predefined',
      views: {
        byId: {},
        allIds: [],
      },
      syncGroups: [{ id: null, type: 'SYNCGROUPS_TYPE_SETBBOX' }],
      mosaicNode: {
        direction: 'row',
        first: 'demosyncgroupbbox_radar',
        second: 'demosyncgroupbbox_temp',
        splitPercentage: 50,
      },
    };
    const mockState = {
      screenManagerStore: screenConfigNoIdForSyncGroup,
      syncronizationGroupStore: SyncGroups.initialState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();

    jest.spyOn(console, 'warn').mockImplementation();
    const { container } = render(
      <TestWrapperWithStore store={store}>
        <InitSyncGroups screenPresets={[screenConfigNoIdForSyncGroup]} />
      </TestWrapperWithStore>,
    );
    expect(container).toBeTruthy();
    expect(console.warn).toHaveBeenCalledWith(
      expect.stringContaining(
        'Invalid syncGroups preset, id or type missing in syncGroup definition',
      ),
    );
  });

  it('should not add sync groups if type is not set', () => {
    const screenConfigNoTypeForSyncGroup: ScreenManagerPreset = {
      id: 'screenConfigOnlyBBOXGroup',
      title: 'SyncGroups only BBOX predefined',
      views: {
        byId: {},
        allIds: [],
      },
      syncGroups: [{ id: 'my_syncgroup', type: null }],
      mosaicNode: {
        direction: 'row',
        first: 'demosyncgroupbbox_radar',
        second: 'demosyncgroupbbox_temp',
        splitPercentage: 50,
      },
    };
    const mockState = {
      screenManagerStore: screenConfigNoTypeForSyncGroup,
      syncronizationGroupStore: SyncGroups.initialState,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();

    jest.spyOn(console, 'warn').mockImplementation();
    const { container } = render(
      <TestWrapperWithStore store={store}>
        <InitSyncGroups screenPresets={[screenConfigNoTypeForSyncGroup]} />
      </TestWrapperWithStore>,
    );
    expect(container).toBeTruthy();
    expect(console.warn).toHaveBeenCalledWith(
      expect.stringContaining(
        'Invalid syncGroups preset, id or type missing in syncGroup definition',
      ),
    );
  });
});
