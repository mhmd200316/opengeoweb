/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { SyncGroups } from '@opengeoweb/core';
import { screenManagerActions } from '../../store/reducer';
import {
  ScreenManagerPreset,
  ScreenManagerSetPresetPayload,
  ScreenManagerViewType,
} from '../../store/types';
import * as screenManagerSelectors from '../../store/selectors';
import { AppStore } from '../../types/types';

export interface InitSyncGroupsProps {
  screenPresets: ScreenManagerPreset[];
}

export const InitSyncGroups: React.FC<InitSyncGroupsProps> = ({
  screenPresets,
}: InitSyncGroupsProps) => {
  const dispatch = useDispatch();
  const screenPreset: ScreenManagerPreset = useSelector((store: AppStore) =>
    screenManagerSelectors.getScreenManagerState(store),
  );

  const syncGroupAddGroup = React.useCallback(
    (payload: SyncGroups.types.SyncGroupAddGroupPayload): void => {
      dispatch(SyncGroups.actions.syncGroupAddGroup(payload));
    },
    [dispatch],
  );

  const syncGroupAddTarget = React.useCallback(
    (payload: SyncGroups.types.SyncGroupAddTargetPayload): void => {
      dispatch(SyncGroups.actions.syncGroupAddTarget(payload));
    },
    [dispatch],
  );

  const changePreset = React.useCallback(
    (payload: ScreenManagerSetPresetPayload): void => {
      dispatch(screenManagerActions.changePreset(payload));
    },
    [dispatch],
  );

  React.useEffect(() => {
    /*  Make a map with all the types and set them initially to false to indicate that these are not found in the preset.  */
    const foundSyncgroupsTypes:
      | Record<SyncGroups.types.SyncType, boolean>
      | unknown = {};
    SyncGroups.types.SyncGroupTypeList.forEach((type) => {
      foundSyncgroupsTypes[type] = false;
    });

    /* Add the syncgroup based on the preset */
    if (screenPreset.syncGroups) {
      screenPreset.syncGroups.forEach((syncGroup) => {
        if (syncGroup.id && syncGroup.type) {
          syncGroupAddGroup({
            groupId: syncGroup.id,
            title: syncGroup.title || syncGroup.id,
            type: syncGroup.type,
          });
          /* This syncgroup type has been found in the preset, so it does not need to be added later */
          foundSyncgroupsTypes[syncGroup.type] = true;
        } else {
          console.warn(
            'Invalid syncGroups preset, id or type missing in syncGroup definition',
          );
        }
      });
    }
    /* Loop through the different syncgroup types, and check if the group type was already added via the preset, if not add it here */
    Object.keys(foundSyncgroupsTypes).forEach((syncGroupType) => {
      const group = foundSyncgroupsTypes[syncGroupType];
      /* The group type was not found */
      if (group === false) {
        const groupName = `${syncGroupType}_A`;
        const groupDef = {
          groupId: groupName,
          title: `Default group for ${groupName}`,
          type: syncGroupType as SyncGroups.types.SyncType,
        };
        syncGroupAddGroup(groupDef);
      }
    });
    screenPreset.views.allIds.forEach((viewId: string): void => {
      const view: ScreenManagerViewType = screenPreset.views.byId[viewId];
      const { syncGroupsIds } = view.initialProps;

      if (syncGroupsIds && Array.isArray(syncGroupsIds)) {
        syncGroupsIds.forEach((syncGroupsId: string): void => {
          const linked = true;
          syncGroupAddTarget({
            groupId: syncGroupsId,
            targetId: viewId,
            linked,
          });
        });
      }
    });
  }, [
    screenPreset.syncGroups,
    screenPreset.views.allIds,
    screenPreset.views.byId,
    syncGroupAddGroup,
    syncGroupAddTarget,
  ]);

  // Set the first preset on the first render
  React.useEffect(() => {
    changePreset({ screenManagerPreset: screenPresets[0] });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return null;
};
