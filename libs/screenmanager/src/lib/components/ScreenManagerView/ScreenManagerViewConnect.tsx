/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import { uiActions, uiSelectors } from '@opengeoweb/core';
import { ScreenManagerLookupFunctionType } from '../../store/types';
import * as screenManagerSelectors from '../../store/selectors';
import { screenManagerActions } from '../../store/reducer';
import ScreenManagerView from './ScreenManagerView';
import { AppStore } from '../../types/types';

export interface ScreenManagerViewConnectProps {
  componentsLookUp: ScreenManagerLookupFunctionType;
}

const ScreenManagerViewConnect: React.FC<ScreenManagerViewConnectProps> = ({
  componentsLookUp,
}: ScreenManagerViewConnectProps) => {
  const dispatch = useDispatch();

  const screenConfig = useSelector((store: AppStore) =>
    screenManagerSelectors.getScreenManagerState(store),
  );

  const updateViews = useCallback(
    (currentNode): void => {
      dispatch(
        screenManagerActions.updateScreenManagerViews({
          mosaicNode: currentNode,
        }),
      );
    },
    [dispatch],
  );

  const setActiveWindowId = useCallback(
    (activeWindowId: string): void => {
      dispatch(uiActions.setActiveWindowId({ activeWindowId }));
    },
    [dispatch],
  );

  const activeWindowId = useSelector(uiSelectors.getActiveWindowId);

  return (
    <ConfirmationServiceProvider>
      <ScreenManagerView
        screenConfig={screenConfig}
        componentsLookUp={componentsLookUp}
        updateViews={updateViews}
        activeWindowId={activeWindowId}
        setActiveWindowId={setActiveWindowId}
      />
    </ConfirmationServiceProvider>
  );
};

export default ScreenManagerViewConnect;
