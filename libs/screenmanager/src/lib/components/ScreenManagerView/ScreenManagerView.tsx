/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Mosaic, MosaicNode } from 'react-mosaic-component';
import { Box, Typography } from '@mui/material';

import 'react-mosaic-component/react-mosaic-component.css';
import './ScreenManagerView.css';
import { WebAssetOff } from '@opengeoweb/theme';
import { isString } from 'lodash';
import {
  ScreenManagerPreset,
  ScreenManagerLookupFunctionType,
} from '../../store/types';

import ScreenManagerTileConnect from './ScreenManagerTileConnect';

export interface ScreenManagerViewProps {
  screenConfig: ScreenManagerPreset;
  componentsLookUp: ScreenManagerLookupFunctionType;
  updateViews: (currentNode: MosaicNode<string>) => void;
  activeWindowId: string;
  setActiveWindowId: (activeWindowId: string) => void;
}

const ScreenManagerView: React.FC<ScreenManagerViewProps> = ({
  screenConfig,
  componentsLookUp,
  updateViews,
  activeWindowId,
  setActiveWindowId,
}) => {
  const EmptyView = (
    <Box
      sx={{
        backgroundColor: 'geowebColors.background.surface',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
      }}
    >
      <WebAssetOff sx={{ fontSize: '150px', margin: '20px' }} />
      <Typography sx={{ color: 'geowebColors.typographyAndIcons.text' }}>
        Welcome to GeoWeb. Please use one of the presets to open up new views.
      </Typography>
    </Box>
  );

  useControlActiveWindowId(screenConfig, activeWindowId, setActiveWindowId);

  const handleClickOnWindow = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    viewId: string,
  ): void => {
    // Check if a windows close button was clicked
    const { target } = event;
    if (target instanceof Element) {
      const element = target.closest('#close-window-button');
      if (element) {
        const viewIds = screenConfig.views.allIds;
        if (viewIds.length === 0) {
          setActiveWindowId(undefined);
        } else {
          setActiveWindowId(activeWindowId);
        }
        return;
      }
    }
    setActiveWindowId(viewId);
  };

  return (
    <Box
      sx={{
        height: '100%',
        '.geoweb-mosaic .mosaic-window-title': {
          color: 'geowebColors.typographyAndIcons.text',
          fontWeight: 'bold',
          fontSize: '10px',
        },
        '.geoweb-mosaic .mosaic-window-body': {
          backgroundColor: 'geowebColors.background.surfaceApp',
        },
      }}
    >
      <Mosaic<string>
        className="geoweb-mosaic"
        zeroStateView={EmptyView}
        renderTile={(viewId, path): React.ReactElement => {
          const windowIsActive = viewId === activeWindowId;
          return (
            <Box
              onClick={(event): void => handleClickOnWindow(event, viewId)}
              // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
              sx={(theme) => ({
                '> *': { height: '100%', width: '100%' },
                '.mosaic-window .mosaic-window-toolbar': {
                  backgroundColor: windowIsActive
                    ? theme.palette.geowebColors.screenManager.activeWindow
                    : theme.palette.geowebColors.screenManager.inactiveWindow,
                },
                '.mosaic-window-toolbar .MuiSvgIcon-root': {
                  fill: windowIsActive
                    ? theme.palette.geowebColors.buttons.flat.default.color
                    : theme.palette.geowebColors.buttons.flat.disabled.color,
                },
                '#close-window-button .MuiSvgIcon-root': {
                  fill: theme.palette.geowebColors.buttons.flat.disabled.color,
                },
                '.mosaic-window-toolbar .mosaic-window-title': {
                  color: windowIsActive
                    ? theme.palette.geowebColors.typographyAndIcons.text
                    : theme.palette.geowebColors.typographyAndIcons
                        .inactiveText,
                },
              })}
            >
              <ScreenManagerTileConnect
                viewId={viewId}
                path={path}
                componentsLookUp={componentsLookUp}
              />
            </Box>
          );
        }}
        value={screenConfig.mosaicNode}
        onChange={(currentNode: MosaicNode<string> | null): void => {
          // set layout in store
          updateViews(currentNode);
          /* This will trigger all maps and other components to get the correct size */
          window.setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 100);
        }}
        resize={{
          minimumPaneSizePercentage:
            screenConfig.minimumPaneSizePercentage || 20,
        }}
      />
    </Box>
  );
};

export default ScreenManagerView;

export const useControlActiveWindowId = (
  screenConfig: ScreenManagerPreset,
  activeWindowId: string | undefined,
  setActiveWindowId: (activeWindowId: string) => void,
): void => {
  // When screenConfig.id changes it means a new preset was chosen.
  React.useEffect(() => {
    if (screenConfig.id !== '') {
      let { mosaicNode } = screenConfig;
      while (!isString(mosaicNode)) {
        mosaicNode = mosaicNode.first;
      }
      setActiveWindowId(mosaicNode);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [screenConfig.id]);

  // If active window is no longer in screenConfig then set active window to first window
  React.useEffect(() => {
    const viewIds = screenConfig.views.allIds;
    if (activeWindowId !== undefined && !viewIds.includes(activeWindowId)) {
      setActiveWindowId(viewIds[0]);
    }
  }, [activeWindowId, screenConfig.views.allIds, setActiveWindowId]);
};
