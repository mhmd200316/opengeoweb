/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Mosaic, MosaicNode } from 'react-mosaic-component';
import { MosaicKey } from 'react-mosaic-component/lib/types';
import ScreenManagerTileConnect from './ScreenManagerTileConnect';
import { ScreenManagerComponentLookupPayload } from '../../store/types';
import { TestWrapperWithStore } from '../../utils/testUtils';
import { screenManagerActions } from '../../store/reducer';

const componentsLookUp = (
  payload: ScreenManagerComponentLookupPayload,
): React.ReactElement => {
  const { componentType, initialProps: props, id } = payload;
  switch (componentType) {
    case 'MyTestComponent':
    default:
      return (
        <div data-testid="MyTestComponentTestId">
          {id}
          {JSON.stringify(props)}
        </div>
      );
  }
};

describe('components/ScreenManagerView/ScreenManagerTileConnect', () => {
  const screenConfig = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'Map',
          initialProps: {
            mapPreset: {
              layers: [
                {
                  service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                  name: 'RAD_NL25_PCP_CM',
                  format: 'image/png',
                  enabled: true,
                  layerType: 'mapLayer',
                },
              ],
              proj: {
                bbox: {
                  left: -450651.2255879827,
                  bottom: 6490531.093143953,
                  right: 1428345.8183648037,
                  top: 7438773.776232235,
                },
                srs: 'EPSG:3857',
              },
            },
          },
        },
      },
    },
    mosaicNode: {
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
    },
  };
  it('should add a view', () => {
    const mockState = {
      screenManagerStore: screenConfig,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();

    const props = {
      componentsLookUp,
      path: [],
      viewId: 'screen1',
    };

    const { getAllByTestId } = render(
      <TestWrapperWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <ScreenManagerTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </TestWrapperWithStore>,
    );

    expect(store.getActions()).toHaveLength(0);

    fireEvent.click(getAllByTestId('split-horizontal-btn')[0]);

    expect(store.getActions()).toHaveLength(1);
    expect(store.getActions()).toEqual([
      screenManagerActions.addScreenManagerView({
        componentType: 'Map',
        id: expect.any(String),
        initialProps: {
          mapPreset: {},
          syncGroupsIds: [],
        },
      }),
    ]);
  });

  it('should use the bbox and srs of the clicked view when splitting a map', () => {
    const bbox = {
      // different bbox than in screenconfig to simulate user has moved the map
      left: 299317.22579447436,
      bottom: 4028678.1331711058,
      right: 2178314.269747261,
      top: 4976920.816259388,
    };
    const srs = 'EPSG:32661';
    const mockState = {
      screenManagerStore: screenConfig,
      webmap: {
        byId: {
          screen2: {
            bbox,
            srs,
          },
        },
        allIds: ['screen2'],
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();

    const props = {
      componentsLookUp,
      path: [],
      viewId: 'screen2',
    };

    const { getAllByTestId } = render(
      <TestWrapperWithStore store={store}>
        <Mosaic
          value={screenConfig.mosaicNode as MosaicNode<MosaicKey>}
          renderTile={(): React.ReactElement => (
            <ScreenManagerTileConnect {...props} />
          )}
          onChange={jest.fn()}
        />
      </TestWrapperWithStore>,
    );

    expect(store.getActions()).toHaveLength(0);

    fireEvent.click(getAllByTestId('split-vertical-btn')[0]);

    expect(store.getActions()).toHaveLength(1);
    expect(store.getActions()).toEqual([
      screenManagerActions.addScreenManagerView({
        componentType: 'Map',
        id: expect.any(String),
        initialProps: {
          mapPreset: { proj: { bbox, srs } },
          syncGroupsIds: [],
        },
      }),
    ]);
  });
});
