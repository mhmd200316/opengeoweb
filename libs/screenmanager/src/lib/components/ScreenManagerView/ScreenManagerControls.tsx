/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useSelector } from 'react-redux';
import {
  getAndAssertNodeAtPathExists,
  MosaicBranch,
  MosaicContext,
  MosaicNode,
} from 'react-mosaic-component';
import { useConfirmationDialog } from '@opengeoweb/shared';
import { isEqual, isString } from 'lodash';
import {
  SplitHorizontal,
  SplitVertical,
  Expand,
  UnExpand,
  Close,
} from '@opengeoweb/theme';
import ScreenManagerButton from './ScreenManagerButton';
import { AppStore } from '../../types/types';
import * as screenManagerSelectors from '../../store/selectors';

interface ScreenManagerControlsProps {
  path: MosaicBranch[];
  createNode?: () => MosaicNode<string>;
  shouldPreventCloseView?: boolean;
}

const ScreenManagerControls: React.FC<ScreenManagerControlsProps> = ({
  createNode,
  path,
  shouldPreventCloseView = false,
}: ScreenManagerControlsProps) => {
  const confirmDialog = useConfirmationDialog();
  const { mosaicActions: actions } = React.useContext(MosaicContext);

  const mosaicNode = useSelector((store: AppStore) =>
    screenManagerSelectors.getMosaicNode(store),
  );
  const [stateAfterExpand, setStateAfterExpand] =
    React.useState<MosaicNode<string>>();

  const [stateBeforeExpand, setStateBeforeExpand] = React.useState(mosaicNode);
  const [enableUnexpand, setEnableUnexpand] = React.useState(false);

  React.useEffect(() => {
    if (enableUnexpand) {
      if (stateAfterExpand === undefined) {
        setStateAfterExpand(mosaicNode);
        return;
      }
      if (!isEqual(mosaicNode, stateAfterExpand)) {
        setEnableUnexpand(false);
        setStateAfterExpand(undefined);
      }
    }
  }, [enableUnexpand, mosaicNode, stateAfterExpand]);

  const onClose = async (): Promise<void> => {
    if (shouldPreventCloseView) {
      await confirmDialog({
        title: 'Whoops',
        description:
          'Are you sure you want to close this window? All changes will be lost.',
        confirmLabel: 'Close window',
      });
      actions.remove(path);
    } else {
      actions.remove(path);
    }
  };

  return (
    <>
      <ScreenManagerButton
        data-testid="split-horizontal-btn"
        onClick={(): void => {
          const newNode = createNode();
          const previousNode = getAndAssertNodeAtPathExists(
            actions.getRoot(),
            path,
          );
          actions.replaceWith(path, {
            direction: 'column',
            first: previousNode,
            second: newNode,
          });
        }}
        title="Split window horizontally"
      >
        <SplitVertical />
      </ScreenManagerButton>
      <ScreenManagerButton
        data-testid="split-vertical-btn"
        onClick={(): void => {
          const newNode = createNode();
          const previousNode = getAndAssertNodeAtPathExists(
            actions.getRoot(),
            path,
          );
          actions.replaceWith(path, {
            direction: 'row',
            first: previousNode,
            second: newNode,
          });
        }}
        title="Split window vertically"
      >
        <SplitHorizontal />
      </ScreenManagerButton>
      {!enableUnexpand || isString(mosaicNode) ? (
        <ScreenManagerButton
          data-testid="expand-btn"
          onClick={(): void => {
            setStateBeforeExpand(mosaicNode);
            setEnableUnexpand(true);
            actions.expand(path, 70);
          }}
          title="Expand window"
        >
          <Expand />
        </ScreenManagerButton>
      ) : (
        <ScreenManagerButton
          data-testid="unexpand-btn"
          onClick={(): void => {
            const stateBeforeExpandCopy = isString(stateBeforeExpand)
              ? stateBeforeExpand
              : { ...stateBeforeExpand };
            actions.replaceWith([], stateBeforeExpandCopy);
          }}
          title="Unexpand window"
        >
          <UnExpand />
        </ScreenManagerButton>
      )}
      <ScreenManagerButton
        data-testid="close-btn"
        onClick={onClose}
        title="Close window"
        id="close-window-button"
      >
        <Close />
      </ScreenManagerButton>
    </>
  );
};

export default ScreenManagerControls;
