/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { MosaicNode } from 'react-mosaic-component';
import { createBalancedMosaicNode } from './helpers';

describe('components/ScreenManagerView/helpers', () => {
  describe('createBalancedMosaicNode', () => {
    const dummyNode: MosaicNode<string> = {
      direction: 'row',
      second: {
        direction: 'column',
        second: 'screen_no_2',
        first: {
          direction: 'row',
          second: 'screen_no_3',
          first: 'screen_no_1',
        },
      },
      first: 'screen_no_0',
    };

    const expectedResult = {
      direction: 'row',
      first: {
        direction: 'column',
        first: 'screen_no_0',
        second: 'screen_no_1',
      },
      second: {
        direction: 'column',
        first: 'screen_no_3',
        second: 'screen_no_2',
      },
    };

    it('should return a MosaicNode with less nesting', () => {
      const result = createBalancedMosaicNode(dummyNode);
      expect(result).not.toEqual(dummyNode);
      expect(result).toEqual(expectedResult);
    });
  });
});
