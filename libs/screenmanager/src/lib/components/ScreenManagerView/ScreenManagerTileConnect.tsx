/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { MosaicBranch, MosaicNode, MosaicWindow } from 'react-mosaic-component';
import { useSelector, useDispatch } from 'react-redux';
import { mapSelectors, mapTypes } from '@opengeoweb/core';
import 'react-mosaic-component/react-mosaic-component.css';
import './ScreenManagerView.css';
import ScreenManagerControls from './ScreenManagerControls';
import * as screenManagerSelectors from '../../store/selectors';
import { screenManagerActions } from '../../store/reducer';

import { AppStore } from '../../types/types';
import { ScreenManagerLookupFunctionType } from '../../store/types';
import { getNewId } from './helpers';

interface ScreenManagerTileConnectProps {
  viewId: string;
  path: MosaicBranch[];
  componentsLookUp: ScreenManagerLookupFunctionType;
}

const ScreenManagerTileConnect: React.FC<ScreenManagerTileConnectProps> = ({
  viewId = '',
  path,
  componentsLookUp,
}: ScreenManagerTileConnectProps) => {
  const dispatch = useDispatch();
  const view = useSelector((store: AppStore) =>
    screenManagerSelectors.getViewById(store, viewId),
  );
  // TODO: make bbox and srs work independent of core lib https://gitlab.com/opengeoweb/opengeoweb/-/issues/1525
  const bbox: mapTypes.Bbox = useSelector((store: AppStore) =>
    mapSelectors.getBbox(store, viewId),
  );
  const srs: string = useSelector((store: AppStore) =>
    mapSelectors.getSrs(store, viewId),
  );

  const shouldPreventCloseView = useSelector((store: AppStore) =>
    screenManagerSelectors.getShouldPreventClose(store, viewId),
  );

  const addView = React.useCallback(
    (view): void => {
      dispatch(screenManagerActions.addScreenManagerView(view));
    },
    [dispatch],
  );

  const createNode = (): MosaicNode<string> => {
    const newId = getNewId();
    const mapPreset = bbox.bottom ? { proj: { bbox, srs } } : {};

    addView({
      componentType: 'Map',
      id: newId,
      initialProps: {
        mapPreset,
        syncGroupsIds: [],
      },
    });

    return newId;
  };

  const title = view?.title || viewId;

  return (
    <MosaicWindow<string>
      path={path}
      createNode={createNode}
      title={title}
      toolbarControls={
        <ScreenManagerControls
          createNode={createNode}
          path={path}
          shouldPreventCloseView={shouldPreventCloseView}
        />
      }
    >
      {componentsLookUp({
        title,
        id: viewId,
        componentType: view?.componentType,
        initialProps: view?.initialProps,
      })}
    </MosaicWindow>
  );
};

export default ScreenManagerTileConnect;
