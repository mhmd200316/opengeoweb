/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import * as mosaic from 'react-mosaic-component';
import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import ScreenManagerControls from './ScreenManagerControls';
import { TestWrapperWithStore } from '../../utils/testUtils';

jest.mock('react-mosaic-component');

const { MosaicContext } = mosaic;

describe('components/ScreenManagerView/ScreenManagerControls', () => {
  const screenConfig = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'Map',
          initialProps: {
            mapPreset: {
              layers: [
                {
                  service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                  name: 'RAD_NL25_PCP_CM',
                  format: 'image/png',
                  enabled: true,
                  layerType: 'mapLayer',
                },
              ],
              proj: {
                bbox: {
                  left: -450651.2255879827,
                  bottom: 6490531.093143953,
                  right: 1428345.8183648037,
                  top: 7438773.776232235,
                },
                srs: 'EPSG:3857',
              },
            },
          },
        },
      },
    },
    mosaicNode: {
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
      splitPercentage: 33.3,
    },
  };

  const mockState = {
    screenManagerStore: screenConfig,
  };
  const mockStore = configureStore();
  const store = mockStore(mockState);

  it('should render button content', () => {
    const props = {
      createNode: jest.fn(),
      path: ['testing'] as unknown as mosaic.MosaicBranch[],
    };

    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
    };

    const { getByTestId } = render(
      <TestWrapperWithStore store={store}>
        <MosaicContext.Provider value={mosaicActions}>
          <ScreenManagerControls {...props} />,
        </MosaicContext.Provider>
      </TestWrapperWithStore>,
    );

    expect(getByTestId('split-vertical-btn')).toBeTruthy();
    expect(getByTestId('split-horizontal-btn')).toBeTruthy();
    expect(getByTestId('expand-btn')).toBeTruthy();
    expect(getByTestId('close-btn')).toBeTruthy();
  });

  it('should split window vertically', () => {
    const props = {
      createNode: jest.fn(),
      path: ['testing'] as unknown as mosaic.MosaicBranch[],
    };

    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
    };

    const { getByTestId } = render(
      <TestWrapperWithStore store={store}>
        <MosaicContext.Provider value={mosaicActions}>
          <ScreenManagerControls {...props} />,
        </MosaicContext.Provider>
      </TestWrapperWithStore>,
    );
    const splitButton = getByTestId('split-vertical-btn');
    expect(splitButton).toBeTruthy();
    fireEvent.click(splitButton);
    expect(mosaicActions.mosaicActions.replaceWith).toHaveBeenCalledWith(
      props.path,
      {
        direction: 'row',
      },
    );
  });

  it('should split window horizontally', () => {
    const props = {
      createNode: jest.fn(),
      path: ['testing'] as unknown as mosaic.MosaicBranch[],
    };

    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
    };

    const { getByTestId } = render(
      <TestWrapperWithStore store={store}>
        <MosaicContext.Provider value={mosaicActions}>
          <ScreenManagerControls {...props} />,
        </MosaicContext.Provider>
      </TestWrapperWithStore>,
    );
    const splitButton = getByTestId('split-horizontal-btn');
    expect(splitButton).toBeTruthy();
    fireEvent.click(splitButton);
    expect(mosaicActions.mosaicActions.replaceWith).toHaveBeenCalledWith(
      props.path,
      {
        direction: 'column',
      },
    );
  });

  it('should expand and unexpand window', () => {
    const props = {
      createNode: jest.fn(),
      path: ['testing'] as unknown as mosaic.MosaicBranch[],
    };

    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
    };

    const { getByTestId } = render(
      <TestWrapperWithStore store={store}>
        <MosaicContext.Provider value={mosaicActions}>
          <ScreenManagerControls {...props} />,
        </MosaicContext.Provider>
      </TestWrapperWithStore>,
    );

    const expandButton = getByTestId('expand-btn');
    expect(expandButton).toBeTruthy();
    fireEvent.click(expandButton);
    expect(mosaicActions.mosaicActions.expand).toHaveBeenCalledWith(
      props.path,
      70,
    );

    // Now we expect unexpand to take us back to the original split size
    const unexpandButton = getByTestId('unexpand-btn');
    expect(unexpandButton).toBeTruthy();
    fireEvent.click(unexpandButton);
    expect(mosaicActions.mosaicActions.replaceWith).toHaveBeenCalledWith(
      [],
      screenConfig.mosaicNode,
    );
  });

  it('should close window', () => {
    const props = {
      createNode: jest.fn(),
      path: ['testing'] as unknown as mosaic.MosaicBranch[],
    };

    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
    };

    const { getByTestId } = render(
      <TestWrapperWithStore store={store}>
        <MosaicContext.Provider value={mosaicActions}>
          <ScreenManagerControls {...props} />,
        </MosaicContext.Provider>
      </TestWrapperWithStore>,
    );
    const closeButton = getByTestId('close-btn');
    expect(closeButton).toBeTruthy();
    fireEvent.click(closeButton);
    expect(mosaicActions.mosaicActions.remove).toHaveBeenCalledWith(props.path);
  });

  it('should ask for confirmation before closing window', async () => {
    const props = {
      createNode: jest.fn(),
      path: ['testing'] as unknown as mosaic.MosaicBranch[],
      shouldPreventCloseView: true,
    };

    const mosaicActions = {
      mosaicActions: {
        expand: jest.fn(),
        remove: jest.fn(),
        hide: jest.fn(),
        replaceWith: jest.fn(),
        updateTree: jest.fn(),
        getRoot: jest.fn(),
      },
      mosaicId: 'test-ing',
    };

    const { getByTestId } = render(
      <TestWrapperWithStore store={store}>
        <ConfirmationServiceProvider>
          <MosaicContext.Provider value={mosaicActions}>
            <ScreenManagerControls {...props} />,
          </MosaicContext.Provider>
        </ConfirmationServiceProvider>
      </TestWrapperWithStore>,
    );
    const closeButton = getByTestId('close-btn');
    expect(closeButton).toBeTruthy();
    fireEvent.click(closeButton);
    expect(mosaicActions.mosaicActions.remove).not.toHaveBeenCalledWith(
      props.path,
    );

    await waitFor(() => {
      const confirmationDialog = getByTestId('confirmationDialog');
      expect(confirmationDialog).toBeTruthy();
      fireEvent.click(
        confirmationDialog.querySelector(
          '[data-testid="confirmationDialog-confirm"]',
        ),
      );
    });
    expect(mosaicActions.mosaicActions.remove).toHaveBeenCalledWith(props.path);
  });
});
