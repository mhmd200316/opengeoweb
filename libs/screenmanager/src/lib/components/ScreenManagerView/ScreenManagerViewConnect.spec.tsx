/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import createSagaMiddleware from 'redux-saga';
import produce from 'immer';
import { uiActions } from '@opengeoweb/core';
import ScreenManagerViewConnect from './ScreenManagerViewConnect';
import {
  ScreenManagerComponentLookupPayload,
  ScreenManagerModuleState,
  ScreenManagerPreset,
} from '../../store/types';
import { TestWrapperWithStore } from '../../utils/testUtils';
import screenManagerSaga from '../../store/sagas';
import { screenManagerActions } from '../../store/reducer';

const componentsLookUp = (
  payload: ScreenManagerComponentLookupPayload,
): React.ReactElement => {
  const { componentType, initialProps: props, id } = payload;
  switch (componentType) {
    case 'MyTestComponent':
      return (
        <div data-testid="MyTestComponentTestId">
          {id}
          {JSON.stringify(props)}
        </div>
      );
    default:
      return null;
  }
};

describe('components/ScreenManagerView/ScreenManagerViewConnect', () => {
  const mosaicNode = {
    direction: 'row',
    first: 'screen1',
    second: 'screen2',
  } as const;
  const screenConfig: ScreenManagerPreset = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode,
  };
  it('should return a view with correct title', () => {
    const mockState = {
      screenManagerStore: screenConfig,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const { container } = render(
      <TestWrapperWithStore store={store}>
        <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
      </TestWrapperWithStore>,
    );
    expect(container.querySelector('.mosaic-window-title').textContent).toEqual(
      'screen 1',
    );
  });

  it('should expand a view', () => {
    const mockState = {
      screenManagerStore: screenConfig,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const { getAllByLabelText } = render(
      <TestWrapperWithStore store={store}>
        <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
      </TestWrapperWithStore>,
    );
    fireEvent.click(getAllByLabelText('Expand window')[0]);
    const expectedAction = screenManagerActions.updateScreenManagerViews({
      mosaicNode: {
        ...mosaicNode,
        splitPercentage: 70,
      },
    });
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should set active window', () => {
    const mockState = {
      screenManagerStore: screenConfig,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const { getAllByText } = render(
      <TestWrapperWithStore store={store}>
        <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
      </TestWrapperWithStore>,
    );

    const expectedInitialAction = uiActions.setActiveWindowId({
      activeWindowId: 'screen1',
    });

    expect(store.getActions()).toEqual([expectedInitialAction]);

    const toolbar = getAllByText('screen 2')[0];
    fireEvent.click(toolbar);

    const expectedAction = produce(expectedInitialAction, (draft) => {
      draft.payload.activeWindowId = 'screen2';
    });

    expect(store.getActions()).toEqual([expectedInitialAction, expectedAction]);
  });

  it('should split a view vertically', () => {
    const mockState = {
      screenManagerStore: screenConfig,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const { getAllByLabelText } = render(
      <TestWrapperWithStore store={store}>
        <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
      </TestWrapperWithStore>,
    );
    fireEvent.click(getAllByLabelText('Split window vertically')[0]);

    const expectedAddAction = screenManagerActions.addScreenManagerView({
      componentType: 'Map',
      id: expect.any(String),
      initialProps: {
        mapPreset: {},
        syncGroupsIds: [],
      },
    });

    const expectedUpdateAction = screenManagerActions.updateScreenManagerViews({
      mosaicNode: {
        direction: 'row',
        first: {
          direction: 'row',
          first: 'screen1',
          second: expect.any(String),
        },
        second: 'screen2',
      },
    });
    expect(store.getActions()).toContainEqual(expectedAddAction);
    expect(store.getActions()).toContainEqual(expectedUpdateAction);
  });

  it('should split a view horizontally', () => {
    const mockState = {
      screenManagerStore: screenConfig,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const { getAllByLabelText } = render(
      <TestWrapperWithStore store={store}>
        <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
      </TestWrapperWithStore>,
    );
    fireEvent.click(getAllByLabelText('Split window horizontally')[0]);

    const expectedAddAction = screenManagerActions.addScreenManagerView({
      componentType: 'Map',
      id: expect.any(String),
      initialProps: {
        mapPreset: {},
        syncGroupsIds: [],
      },
    });

    const expectedUpdateAction = screenManagerActions.updateScreenManagerViews({
      mosaicNode: {
        direction: 'row',
        first: {
          direction: 'column',
          first: 'screen1',
          second: expect.any(String),
        },
        second: 'screen2',
      },
    });
    expect(store.getActions()).toContainEqual(expectedAddAction);
    expect(store.getActions()).toContainEqual(expectedUpdateAction);
  });

  it('should resize a view', async () => {
    const resizeSpy = jest.fn();
    window.addEventListener('resize', resizeSpy);

    const mockState = {
      screenManagerStore: screenConfig,
    };
    const sagaMiddleware = createSagaMiddleware();
    const mockStore = configureStore([sagaMiddleware]);
    const store = mockStore(mockState);
    sagaMiddleware.run(screenManagerSaga);

    const { container } = render(
      <TestWrapperWithStore store={store}>
        <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
      </TestWrapperWithStore>,
    );
    const draggableLine = container.querySelector('.mosaic-split-line');
    fireEvent.mouseDown(draggableLine, { clientX: 1 });
    fireEvent.mouseUp(draggableLine);
    const expectedAction = screenManagerActions.updateScreenManagerViews({
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
        splitPercentage: expect.any(Number),
      },
    });
    expect(store.getActions()).toContainEqual(expectedAction);
    await waitFor(() => expect(resizeSpy).toHaveBeenCalled());
  });

  it('should not have unexpand after split window', async () => {
    const mockState: ScreenManagerModuleState = {
      screenManagerStore: screenConfig,
    };
    const sagaMiddleware = createSagaMiddleware();
    const mockStore = configureStore([sagaMiddleware]);
    const store = mockStore(mockState);
    sagaMiddleware.run(screenManagerSaga);

    const { rerender } = render(
      <TestWrapperWithStore store={store}>
        <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
      </TestWrapperWithStore>,
    );

    // First of all there should be two expand buttons
    const expandButtons = screen.getAllByLabelText('Expand window');
    expect(expandButtons).toHaveLength(2);
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();

    // Now we click one expand button
    fireEvent.click(expandButtons[0]);
    const expectedAction = screenManagerActions.updateScreenManagerViews({
      mosaicNode: {
        ...mosaicNode,
        splitPercentage: 70,
      },
    });
    expect(store.getActions()).toContainEqual(expectedAction);

    // And expect to find one expand button and on unexpand button
    expect(screen.getAllByLabelText('Expand window')).toHaveLength(1);
    expect(screen.getAllByLabelText('Unexpand window')).toHaveLength(1);

    // Now we split a window
    fireEvent.click(screen.getAllByLabelText('Split window horizontally')[0]);
    const expectedAddAction = screenManagerActions.addScreenManagerView({
      componentType: 'Map',
      id: expect.any(String),
      initialProps: {
        mapPreset: {},
        syncGroupsIds: [],
      },
    });

    const expectedUpdateAction = screenManagerActions.updateScreenManagerViews({
      mosaicNode: {
        direction: 'row',
        first: {
          direction: 'column',
          first: 'screen1',
          second: expect.any(String),
        },
        second: 'screen2',
      },
    });
    const actions = store.getActions();
    expect(actions[3]).toEqual(expectedAddAction);

    const screenManagerUpdateViewsAction = actions[4];
    expect(screenManagerUpdateViewsAction).toEqual(expectedUpdateAction);

    // grab the action, update the screenconfig and rerender
    const updatedScreenConfig: ScreenManagerPreset = {
      id: screenConfig.id,
      title: screenConfig.title,
      views: screenConfig.views,
      mosaicNode: {
        ...screenManagerUpdateViewsAction.payload.mosaicNode,
      },
    };

    const updatedMockState: ScreenManagerModuleState = {
      screenManagerStore: updatedScreenConfig,
    };
    const updatedStore = mockStore(updatedMockState);

    rerender(
      <TestWrapperWithStore store={updatedStore}>
        <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
      </TestWrapperWithStore>,
    );

    expect(screen.getAllByLabelText('Expand window')).toHaveLength(3);
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();
  });

  it('should not have unexpand after resize window', async () => {
    const resizeSpy = jest.fn();
    window.addEventListener('resize', resizeSpy);

    const mockState = {
      screenManagerStore: screenConfig,
    };
    const sagaMiddleware = createSagaMiddleware();
    const mockStore = configureStore([sagaMiddleware]);
    const store = mockStore(mockState);
    sagaMiddleware.run(screenManagerSaga);

    const { container, rerender } = render(
      <TestWrapperWithStore store={store}>
        <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
      </TestWrapperWithStore>,
    );

    // First of all there should be two expand buttons
    const expandButtons = screen.getAllByLabelText('Expand window');
    expect(expandButtons).toHaveLength(2);
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();

    // Now we click one expand button
    fireEvent.click(expandButtons[0]);

    // And expect to find one expand button and on unexpand button
    expect(screen.getAllByLabelText('Expand window')).toHaveLength(1);
    expect(screen.getAllByLabelText('Unexpand window')).toHaveLength(1);

    // Now we resize a window
    const draggableLine =
      container.getElementsByClassName('mosaic-split-line')[0];
    fireEvent.mouseDown(draggableLine, { clientX: 1 });
    fireEvent.mouseUp(draggableLine);
    const expectedAction = screenManagerActions.updateScreenManagerViews({
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
        splitPercentage: expect.any(Number),
      },
    });
    const screenManagerUpdateViewsAction = store.getActions()[3];
    expect(screenManagerUpdateViewsAction).toEqual(expectedAction);
    await waitFor(() => expect(resizeSpy).toHaveBeenCalled());

    // grab the action, update the screenconfig and rerender
    const updatedScreenConfig = {
      id: screenConfig.id,
      title: screenConfig.title,
      views: screenConfig.views,
      mosaicNode: {
        ...screenManagerUpdateViewsAction.payload.mosaicNode,
      },
    };

    const updatedMockState = {
      screenManagerStore: updatedScreenConfig,
    };
    const updatedStore = mockStore(updatedMockState);

    rerender(
      <TestWrapperWithStore store={updatedStore}>
        <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
      </TestWrapperWithStore>,
    );

    expect(screen.getAllByLabelText('Expand window')).toHaveLength(2);
    expect(screen.queryByLabelText('Unexpand window')).toBeNull();
  });

  it('should close a view', async () => {
    const mockState = {
      screenManagerStore: screenConfig,
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const { getAllByLabelText } = render(
      <TestWrapperWithStore store={store}>
        <ScreenManagerViewConnect componentsLookUp={componentsLookUp} />
      </TestWrapperWithStore>,
    );
    fireEvent.click(getAllByLabelText('Close window')[0]);
    const expectedAction = screenManagerActions.updateScreenManagerViews({
      mosaicNode: 'screen2',
    });
    expect(store.getActions()).toContainEqual(expectedAction);
  });
});
