/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { SagaIterator } from 'redux-saga';
import {
  takeLatest,
  put,
  call,
  debounce,
  select,
  delay,
} from 'redux-saga/effects';
import { syncGroupActions, mapActions } from '@opengeoweb/core';
import { ScreenManagerPreset } from './types';
import { getScreenManagerState } from './selectors';
import { screenManagerActions } from './reducer';

const {
  setPreset,
  deleteScreenManagerView,
  updateScreenManagerViews,
  changePreset,
} = screenManagerActions;

export const emptyPlaceholderPreset = {
  screenManagerPreset: {
    id: '',
    title: '',
    views: {
      allIds: [],
      byId: {},
    },
    mosaicNode: '',
  },
};

export function* changePresetSaga(
  action: ReturnType<typeof screenManagerActions.changePreset>,
): SagaIterator {
  const state: ScreenManagerPreset = yield select(getScreenManagerState);
  // If the current preset is reselected, reload the UI by briefly switching to an
  // empty preset, before switching back to current preset
  yield put(syncGroupActions.syncGroupClear());
  if (state.id === action.payload.screenManagerPreset.id) {
    yield put(setPreset(emptyPlaceholderPreset));
    yield delay(0);
    yield put(syncGroupActions.syncGroupClear());
    yield put(setPreset(action.payload));
  } else {
    yield put(setPreset(action.payload));
  }
}

export function* removeMapSaga(
  action: ReturnType<typeof mapActions.unregisterMap>,
): SagaIterator {
  yield put(deleteScreenManagerView({ id: action.payload.mapId }));
}

export function* updateViewsSaga(): SagaIterator {
  /* This will trigger all maps and other components to get the correct size after views are updated */
  yield call(window.dispatchEvent, new Event('resize'));
}

function* screenManagerSaga(): SagaIterator {
  yield takeLatest(changePreset.type, changePresetSaga);
  yield takeLatest(mapActions.unregisterMap.type, removeMapSaga);
  yield debounce(100, updateScreenManagerViews.type, updateViewsSaga);
}

export default screenManagerSaga;
