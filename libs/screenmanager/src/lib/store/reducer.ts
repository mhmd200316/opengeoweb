/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { cloneDeep } from 'lodash';
import {
  ScreenManagerAddViewPayload,
  ScreenManagerDeleteViewPayload,
  ScreenManagerPreset,
  ScreenManagerSetPresetPayload,
  ScreenManagerSetPreventCloseViewPayload,
  ScreenManagerUpdateViewPayload,
} from './types';

export const initialState: ScreenManagerPreset = {
  id: '',
  title: '',
  views: {
    byId: {},
    allIds: [],
  },
  mosaicNode: undefined,
};

const slice = createSlice({
  initialState,
  name: 'screenManager',
  reducers: {
    addScreenManagerView: (
      draft,
      action: PayloadAction<ScreenManagerAddViewPayload>,
    ) => {
      const newId = action.payload.id;
      draft.views.allIds.push(newId);
      draft.views.byId[newId] = {
        componentType: action.payload.componentType,
        initialProps: action.payload.initialProps,
      };
    },
    deleteScreenManagerView: (
      draft,
      action: PayloadAction<ScreenManagerDeleteViewPayload>,
    ) => {
      const { id } = action.payload;
      draft.views.allIds = draft.views.allIds.filter((viewId) => viewId !== id);
      delete draft.views.byId[id];
    },

    updateScreenManagerViews: (
      draft,
      action: PayloadAction<ScreenManagerUpdateViewPayload>,
    ) => {
      draft.mosaicNode = action.payload.mosaicNode;
    },
    setPreset: (
      draft,
      action: PayloadAction<ScreenManagerSetPresetPayload>,
    ) => {
      // do not change anything when the preset is already set
      if (draft.id === action.payload.screenManagerPreset.id) {
        return draft;
      }

      // check if any viewIds are duplicate between presets
      const newViews = cloneDeep(action.payload.screenManagerPreset.views);
      const duplicateIdBetweenPresets = draft.views.allIds.some((id) =>
        newViews.allIds.includes(id),
      );

      // check if any viewIds are duplicate within the selected preset
      const duplicateIdWithinPreset = newViews.allIds.filter(
        (item, index) => newViews.allIds.indexOf(item) !== index,
      );

      if (duplicateIdBetweenPresets || duplicateIdWithinPreset.length) {
        console.warn('Invalid preset: duplicate viewId found.');
        return initialState;
      }

      draft.views = newViews;
      if (action.payload.screenManagerPreset.syncGroups) {
        draft.syncGroups = cloneDeep(
          action.payload.screenManagerPreset.syncGroups,
        );
      } else {
        draft.syncGroups = [];
      }
      draft.id = action.payload.screenManagerPreset.id;
      draft.title = action.payload.screenManagerPreset.title;
      draft.mosaicNode = action.payload.screenManagerPreset.mosaicNode;
      draft.minimumPaneSizePercentage =
        action.payload.screenManagerPreset.minimumPaneSizePercentage;
      return draft;
    },
    setPreventCloseView: (
      draft,
      action: PayloadAction<ScreenManagerSetPreventCloseViewPayload>,
    ) => {
      const { viewId, shouldPreventClose } = action.payload;
      draft.views.byId[viewId] = {
        ...draft.views.byId[viewId],
        shouldPreventClose,
      };
    },
  },
});

const changePreset = createAction<ScreenManagerSetPresetPayload>(
  'screenManager/changePreset',
);

export const { reducer } = slice;
export const screenManagerActions = { ...slice.actions, changePreset };
