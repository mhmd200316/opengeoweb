/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { MosaicDirection, MosaicNode } from 'react-mosaic-component';
import {
  reducer as screenManagerReducer,
  initialState,
  screenManagerActions,
} from './reducer';
import { ScreenManagerSyncGroup } from './types';

describe('store/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(screenManagerReducer(undefined, {})).toEqual(initialState);
  });

  it('should add a view', () => {
    const result = screenManagerReducer(
      undefined,
      screenManagerActions.addScreenManagerView({
        id: 'test',
        componentType: 'Map',
        initialProps: {},
      }),
    );
    expect(result.views.allIds.length).toBe(1);
  });

  it('should remove a view', () => {
    const result = screenManagerReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB', 'testviewC'],
          byId: {
            testviewA: {
              componentType: 'bla',
            },
            testviewB: {
              componentType: 'bla',
            },
            testviewC: {
              componentType: 'bla',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: {
            direction: 'row',
            first: 'testviewB',
            second: 'testviewC',
          },
        },
      },
      screenManagerActions.deleteScreenManagerView({
        id: 'testviewB',
      }),
    );
    expect(result.views.allIds.length).toBe(2);
    expect(result.views.allIds).toMatchObject(['testviewA', 'testviewC']);
  });

  it('should update the views', () => {
    const newViews = {
      direction: 'row',
      first: 'testviewA',
      second: 'testviewB',
    };
    const result = screenManagerReducer(
      {
        id: '',
        title: '',
        views: {
          allIds: ['testviewA', 'testviewB', 'testviewC'],
          byId: {
            testviewA: {
              componentType: 'bla',
            },
            testviewB: {
              componentType: 'bla',
            },
            testviewC: {
              componentType: 'bla',
            },
          },
        },
        mosaicNode: {
          direction: 'row',
          first: 'testviewA',
          second: {
            direction: 'row',
            first: 'testviewB',
            second: 'testviewC',
          },
        },
      },
      screenManagerActions.updateScreenManagerViews({
        mosaicNode: newViews as MosaicNode<string>,
      }),
    );
    expect(result.mosaicNode).toEqual(newViews);
  });

  it('should set a preset', () => {
    const result = screenManagerReducer(
      undefined,
      screenManagerActions.setPreset({
        screenManagerPreset: {
          id: 'screenConfigA',
          title: 'Screen preset A',
          views: {
            byId: {
              screenA: {
                title: 'Radar view',
                componentType: 'Map',
                initialProps: null,
              },
              screenB: {
                title: 'Temperature observations',
                componentType: 'Map',
                initialProps: null,
              },
            },
            allIds: ['screenA', 'screenB'],
          },
          mosaicNode: {
            direction: 'row',
            first: 'screenA',
            second: 'screenB',
          },
          minimumPaneSizePercentage: 40,
        },
      }),
    );
    expect(result.id).toBe('screenConfigA');
    expect(result.title).toBe('Screen preset A');
    expect(result.views.allIds.length).toBe(2);
    expect(result.views.byId['screenB'].title).toBe('Temperature observations');
    expect(result.views.byId['screenB'].componentType).toBe('Map');
    expect(result.mosaicNode).toEqual({
      direction: 'row',
      first: 'screenA',
      second: 'screenB',
    });
    expect(result.minimumPaneSizePercentage).toEqual(40);

    expect(result.syncGroups).toEqual([]);
  });

  it('should set a preset with syncgroups', () => {
    const preset = {
      screenManagerPreset: {
        id: 'screenConfigA',
        title: 'Screen preset A',
        views: {
          byId: {
            screenA: {
              title: 'Radar view',
              componentType: 'Map',
              initialProps: null,
            },
            screenB: {
              title: 'Temperature observations',
              componentType: 'Map',
              initialProps: null,
            },
          },
          allIds: ['screenA', 'screenB'],
        },
        mosaicNode: {
          direction: 'row' as MosaicDirection,
          first: 'screenA',
          second: 'screenB',
        },
        minimumPaneSizePercentage: 40,
        syncGroups: [
          {
            id: 'Area_radar',
            type: 'SYNCGROUPS_TYPE_SETBBOX' as ScreenManagerSyncGroup['type'],
          },
          {
            id: 'Time_radar',
            type: 'SYNCGROUPS_TYPE_SETTIME' as ScreenManagerSyncGroup['type'],
          },
          {
            id: 'Area_temp',
            type: 'SYNCGROUPS_TYPE_SETBBOX' as ScreenManagerSyncGroup['type'],
          },
          {
            id: 'Time_temp',
            type: 'SYNCGROUPS_TYPE_SETTIME' as ScreenManagerSyncGroup['type'],
          },
        ],
      },
    };
    const result = screenManagerReducer(
      undefined,
      screenManagerActions.setPreset(preset),
    );
    expect(result.id).toBe('screenConfigA');
    expect(result.title).toBe('Screen preset A');
    expect(result.views.allIds.length).toBe(2);
    expect(result.views.byId['screenB'].title).toBe('Temperature observations');
    expect(result.views.byId['screenB'].componentType).toBe('Map');
    expect(result.mosaicNode).toEqual({
      direction: 'row',
      first: 'screenA',
      second: 'screenB',
    });
    expect(result.minimumPaneSizePercentage).toEqual(40);
    expect(result.syncGroups).toEqual(preset.screenManagerPreset.syncGroups);
  });

  it('should not set a preset when the preset itself contains duplicate viewIds', () => {
    const consoleSpy = jest.spyOn(console, 'warn').mockImplementation();

    const result = screenManagerReducer(
      undefined,
      screenManagerActions.setPreset({
        screenManagerPreset: {
          id: 'screenConfigA',
          title: 'Screen preset A',
          views: {
            byId: {
              screenA: {
                title: 'Radar view',
                componentType: 'Map',
                initialProps: null,
              },
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              // eslint-disable-next-line no-dupe-keys
              screenA: {
                title: 'Temperature observations',
                componentType: 'Map',
                initialProps: null,
              },
            },
            allIds: ['screenA', 'screenA'],
          },
          mosaicNode: {
            direction: 'row',
            first: 'screenA',
            second: 'screenA',
          },
          minimumPaneSizePercentage: 40,
        },
      }),
    );
    expect(result).toEqual(initialState);
    expect(consoleSpy).toHaveBeenCalledWith(
      'Invalid preset: duplicate viewId found.',
    );
  });

  it('should not set a preset when the new preset has duplicate viewIds compared to the previous preset', () => {
    const consoleSpy = jest.spyOn(console, 'warn').mockImplementation();

    const previousPreset = {
      screenManagerPreset: {
        id: 'screenConfigA',
        title: 'Screen preset A',
        views: {
          byId: {
            screenA: {
              title: 'Radar view',
              componentType: 'Map',
              initialProps: null,
            },
          },
          allIds: ['screenA'],
        },
        mosaicNode: 'screenA',
      },
    };

    const newPreset = {
      screenManagerPreset: {
        id: 'screenConfigB',
        title: 'Screen preset B',
        views: {
          byId: {
            screenA: {
              title: 'Radar view B',
              componentType: 'Map',
              initialProps: null,
            },
          },
          allIds: ['screenA'],
        },
        mosaicNode: 'screenA',
      },
    };
    const result = screenManagerReducer(
      previousPreset.screenManagerPreset,
      screenManagerActions.setPreset(newPreset),
    );
    expect(result).toEqual(initialState);
    expect(consoleSpy).toHaveBeenCalledWith(
      'Invalid preset: duplicate viewId found.',
    );
  });

  it('should not change the preset when new preset is equal to the previous preset', () => {
    const preset = {
      screenManagerPreset: {
        id: 'screenConfigA',
        title: 'Screen preset A',
        views: {
          byId: {
            screenA: {
              title: 'Radar view',
              componentType: 'Map',
              initialProps: null,
            },
          },
          allIds: ['screenA'],
        },
        mosaicNode: 'screenA',
      },
    };
    const result = screenManagerReducer(
      preset.screenManagerPreset,
      screenManagerActions.setPreset(preset),
    );
    expect(result).toEqual(preset.screenManagerPreset);
  });

  it('should toggle prevent close view', () => {
    const preset = {
      screenManagerPreset: {
        id: 'screenConfigA',
        title: 'Screen preset A',
        views: {
          byId: {
            screenA: {
              title: 'Radar view',
              componentType: 'Map',
              initialProps: null,
            },
          },
          allIds: ['screenA'],
        },
        mosaicNode: 'screenA',
      },
    };
    const result = screenManagerReducer(
      preset.screenManagerPreset,
      screenManagerActions.setPreventCloseView({
        viewId: 'screenA',
        shouldPreventClose: true,
      }),
    );
    expect(result.views.byId.screenA.shouldPreventClose).toBeTruthy();
  });
});
