/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { mapActions, syncGroupActions } from '@opengeoweb/core';
import { debounce, put, takeLatest, call, delay } from 'redux-saga/effects';
import configureStore from 'redux-mock-store';
import createSagaMiddleware from 'redux-saga';

import screenManagerSaga, {
  emptyPlaceholderPreset,
  removeMapSaga,
  changePresetSaga,
  updateViewsSaga,
} from './sagas';
import { screenManagerActions } from './reducer';

const {
  setPreset,
  changePreset,
  deleteScreenManagerView,
  updateScreenManagerViews,
} = screenManagerActions;

describe('store/sagas', () => {
  describe('screenManagerSaga', () => {
    it('should intercept actions and fire sagas', () => {
      const generator = screenManagerSaga();

      expect(generator.next().value).toEqual(
        takeLatest(changePreset.type, changePresetSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(mapActions.unregisterMap.type, removeMapSaga),
      );
      expect(generator.next().value).toEqual(
        debounce(100, updateScreenManagerViews.type, updateViewsSaga),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('changePresetSaga', () => {
    const screenManagerStore = {
      id: 'radarSingleMapScreenConfig',
      title: 'Radar view',
      views: {
        allIds: ['radarView'],
        byId: {
          radarView: {
            title: 'Radar view',
            componentType: 'Map',
            initialProps: {
              mapPreset: {
                layers: [],
                proj: {
                  bbox: {
                    left: 58703.6377,
                    bottom: 6408480.4514,
                    right: 3967387.5161,
                    top: 11520588.9031,
                  },
                  srs: 'EPSG:3857',
                },
                activeLayerId: 'radar_precipitation_intensity_nordic_id',
                toggleTimestepAuto: false,
                setTimestep: 5,
              },
            },
          },
        },
      },
      mosaicNode: 'radarView',
      syncGroups: [],
    };

    const changeAction = changePreset({
      screenManagerPreset: {
        id: 'harmSingleMapScreenConfig',
        title: 'HARM',
        views: {
          allIds: ['harm'],
          byId: {
            harm: {
              title: 'HARM',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [],
                  proj: {
                    bbox: {
                      left: -450651.2255879827,
                      bottom: 6490531.093143953,
                      right: 1428345.8183648037,
                      top: 7438773.776232235,
                    },
                    srs: 'EPSG:3857',
                  },
                },
              },
            },
          },
        },
        mosaicNode: 'harm',
      },
    });

    it('should trigger setPreset when switching presets', () => {
      const generator = changePresetSaga(changeAction);
      generator.next();
      generator.next(screenManagerStore);

      expect(generator.next().value).toEqual(
        put(setPreset(changeAction.payload)),
      );
    });

    it('should toggle empty preset when reselecting current preset', () => {
      jest.useFakeTimers();

      const action = changePreset({
        screenManagerPreset: {
          ...screenManagerStore,
        },
      });

      const generator = changePresetSaga(action);
      generator.next();
      generator.next(screenManagerStore);

      expect(generator.next().value).toEqual(
        put(setPreset(emptyPlaceholderPreset)),
      );
      expect(generator.next().value).toEqual(delay(0));
      jest.runOnlyPendingTimers();
      generator.next();
      expect(generator.next().value).toEqual(put(setPreset(action.payload)));

      jest.clearAllTimers();
      jest.useRealTimers();
    });

    it('should clear syncgroups twice when reloading preset', async () => {
      jest.useFakeTimers();

      const saga = createSagaMiddleware();
      const mockStore = configureStore([saga]);
      const store = mockStore({
        screenManagerStore: { ...screenManagerStore },
      });

      let actionsToCheck;
      store.subscribe(() => {
        const actions = store.getActions();
        if (actions.length >= 1) {
          actionsToCheck = actions;
        }
      });

      const action = changePreset({
        screenManagerPreset: {
          ...screenManagerStore,
        },
      });
      saga.run(changePresetSaga, action);
      jest.runAllTimers();
      await Promise.resolve(); // allow pending microtasks to run (= remainder of saga after delay)

      expect(
        actionsToCheck.filter(
          (actn) => actn.type === syncGroupActions.syncGroupClear.type,
        ).length,
      ).toEqual(2);
      jest.clearAllTimers();
      jest.useRealTimers();
    });

    it('should clear syncgroups once when loading a different preset', async () => {
      jest.useFakeTimers();

      const saga = createSagaMiddleware();
      const mockStore = configureStore([saga]);
      const store = mockStore({
        screenManagerStore: { ...screenManagerStore },
      });

      let actionsToCheck;
      store.subscribe(() => {
        const actions = store.getActions();
        if (actions.length >= 1) {
          actionsToCheck = actions;
        }
      });

      saga.run(changePresetSaga, changeAction);
      jest.runAllTimers();
      await Promise.resolve(); // allow pending microtasks to run (= remainder of saga after delay)

      expect(
        actionsToCheck.filter(
          (actn) => actn.type === syncGroupActions.syncGroupClear.type,
        ).length,
      ).toEqual(1);
      jest.clearAllTimers();
      jest.useRealTimers();
    });
  });

  describe('removeMapSaga', () => {
    it('should delete the screenmanager view when the map is removed', () => {
      const mapId = 'map_no_1';
      const generator = removeMapSaga(
        mapActions.unregisterMap({
          mapId,
        }),
      );
      expect(generator.next().value).toEqual(
        put(
          deleteScreenManagerView({
            id: mapId,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('updateViewsSaga', () => {
    it('should handle updateViewsSaga', async () => {
      const generator = updateViewsSaga();
      expect(generator.next().value).toEqual(
        call(window.dispatchEvent, new Event('resize')),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
});
