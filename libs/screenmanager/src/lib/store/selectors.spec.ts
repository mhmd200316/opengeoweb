/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { MosaicDirection } from 'react-mosaic-component';
import * as screenManagerSelectors from './selectors';

describe('store/selectors', () => {
  describe('getScreenManagerState', () => {
    it('should return screenmanager state', () => {
      const mockStore = {
        screenManagerStore: {
          id: '',
          title: '',
          views: {
            allIds: ['testviewA', 'testviewB', 'testviewC'],
            byId: {
              testviewA: {
                componentType: 'bla',
                syncGroupsIds: null,
              },
              testviewB: {
                componentType: 'bla',
                syncGroupsIds: null,
              },
              testviewC: {
                componentType: 'bla',
                syncGroupsIds: null,
              },
            },
          },
          mosaicNode: {
            direction: 'row' as MosaicDirection,
            first: 'testviewA',
            second: {
              direction: 'row' as MosaicDirection,
              first: 'testviewB',
              second: 'testviewC',
            },
          },
        },
      };

      expect(screenManagerSelectors.getScreenManagerState(mockStore)).toEqual(
        mockStore.screenManagerStore,
      );
    });
  });
  describe('getMosaicNode', () => {
    it('should return mosaicNode from screenmanager state', () => {
      const mockStore = {
        screenManagerStore: {
          id: '',
          title: '',
          views: {
            allIds: ['testviewA', 'testviewB', 'testviewC'],
            byId: {
              testviewA: {
                componentType: 'bla',
                syncGroupsIds: null,
              },
              testviewB: {
                componentType: 'bla',
                syncGroupsIds: null,
              },
              testviewC: {
                componentType: 'bla',
                syncGroupsIds: null,
              },
            },
          },
          mosaicNode: {
            direction: 'row' as MosaicDirection,
            first: 'testviewA',
            second: {
              direction: 'row' as MosaicDirection,
              first: 'testviewB',
              second: 'testviewC',
            },
          },
        },
      };

      expect(screenManagerSelectors.getMosaicNode(mockStore)).toEqual(
        mockStore.screenManagerStore.mosaicNode,
      );
    });
  });

  describe('getViewById', () => {
    it('should return view from screenmanager state', () => {
      const mockStore = {
        screenManagerStore: {
          id: '',
          title: '',
          views: {
            allIds: ['testviewA', 'testviewB', 'testviewC'],
            byId: {
              testviewA: {
                componentType: 'bla',
                syncGroupsIds: null,
              },
              testviewB: {
                componentType: 'bla',
                syncGroupsIds: null,
              },
              testviewC: {
                componentType: 'bla',
                syncGroupsIds: null,
              },
            },
          },
          mosaicNode: {
            direction: 'row' as MosaicDirection,
            first: 'testviewA',
            second: {
              direction: 'row' as MosaicDirection,
              first: 'testviewB',
              second: 'testviewC',
            },
          },
        },
      };

      expect(
        screenManagerSelectors.getViewById(mockStore, 'testviewA'),
      ).toEqual(mockStore.screenManagerStore.views.byId['testviewA']);
      expect(
        screenManagerSelectors.getViewById(mockStore, 'testviewB'),
      ).toEqual(mockStore.screenManagerStore.views.byId['testviewB']);
      expect(
        screenManagerSelectors.getViewById(mockStore, 'testviewC'),
      ).toEqual(mockStore.screenManagerStore.views.byId['testviewC']);
    });

    it('should return null from screenmanager state when id is not present', () => {
      const mockStore = {
        screenManagerStore: {
          id: '',
          title: '',
          views: {
            allIds: [],
            byId: {},
          },
          mosaicNode: {
            direction: 'row' as MosaicDirection,
            first: 'testviewA',
            second: {
              direction: 'row' as MosaicDirection,
              first: 'testviewB',
              second: 'testviewC',
            },
          },
        },
      };

      expect(
        screenManagerSelectors.getViewById(mockStore, 'testviewA'),
      ).toBeNull();
      expect(
        screenManagerSelectors.getViewById(mockStore, 'testviewB'),
      ).toBeNull();
      expect(
        screenManagerSelectors.getViewById(mockStore, 'testviewC'),
      ).toBeNull();
    });
  });

  describe('shouldPreventClose', () => {
    it('should return shouldPreventClose from view state', () => {
      const mockStore = {
        screenManagerStore: {
          id: '',
          title: '',
          views: {
            allIds: ['testviewA', 'testviewB', 'testviewC'],
            byId: {
              testviewA: {
                componentType: 'test',
                syncGroupsIds: null,
                shouldPreventClose: true,
              },
              testviewB: {
                componentType: 'test',
                syncGroupsIds: null,
                shouldPreventClose: false,
              },
              testviewC: {
                componentType: 'test',
                syncGroupsIds: null,
                shouldPreventClose: true,
              },
            },
          },
          mosaicNode: {
            direction: 'row' as MosaicDirection,
            first: 'testviewA',
            second: {
              direction: 'row' as MosaicDirection,
              first: 'testviewB',
              second: 'testviewC',
            },
          },
        },
      };
      expect(
        screenManagerSelectors.getShouldPreventClose(mockStore, 'testviewA'),
      ).toBeTruthy();
      expect(
        screenManagerSelectors.getShouldPreventClose(mockStore, 'testviewB'),
      ).toBeFalsy();
      expect(
        screenManagerSelectors.getShouldPreventClose(mockStore, 'testviewC'),
      ).toBeTruthy();
    });
    it('should return shouldPreventClose false if not present', () => {
      const mockStore = {
        screenManagerStore: {
          id: '',
          title: '',
          views: {
            allIds: ['testviewA', 'testviewB', 'testviewC'],
            byId: {
              testviewA: {
                componentType: 'test',
                syncGroupsIds: null,
              },
            },
          },
          mosaicNode: {
            direction: 'row' as MosaicDirection,
            first: 'testviewA',
            second: {
              direction: 'row' as MosaicDirection,
              first: 'testviewB',
              second: 'testviewC',
            },
          },
        },
      };
      expect(
        screenManagerSelectors.getShouldPreventClose(mockStore, 'testviewA'),
      ).toBeFalsy();
      expect(
        screenManagerSelectors.getShouldPreventClose(mockStore, 'testviewB'),
      ).toBeFalsy();
      expect(
        screenManagerSelectors.getShouldPreventClose(mockStore, 'testviewC'),
      ).toBeFalsy();
    });
  });
});
