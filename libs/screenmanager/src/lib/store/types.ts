/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { MosaicNode } from 'react-mosaic-component';
import { SyncGroups } from '@opengeoweb/core';

type InitialProps = Record<string, unknown>; // empty

export interface ScreenManagerViewType {
  title?: string; // If not provided, id will be used instead
  componentType: string;
  initialProps?: InitialProps;
  activeLayerId?: string;
  shouldPreventClose?: boolean;
}

export interface ScreenManagerViews {
  byId: Record<string, ScreenManagerViewType>;
  allIds: string[];
}

export type ViewType = 'singleWindow' | 'multiWindow' | 'tiledWindow';

export interface ScreenManagerSyncGroup {
  id: string;
  type: SyncGroups.types.SyncType;
  title?: string;
}

export interface ScreenManagerPreset {
  id: string;
  title: string;
  syncGroups?: ScreenManagerSyncGroup[];
  views: ScreenManagerViews;
  viewType?: ViewType;
  mosaicNode: MosaicNode<string>;
  minimumPaneSizePercentage?: number;
}

export interface ScreenManagerAddViewPayload {
  id: string;
  componentType: string;
  initialProps: InitialProps;
}

export interface ScreenManagerDeleteViewPayload {
  id: string;
}

export interface ScreenManagerUpdateViewPayload {
  mosaicNode: MosaicNode<string>;
}

export interface ScreenManagerSetPresetPayload {
  screenManagerPreset: ScreenManagerPreset;
}

export interface ScreenManagerSetPreventCloseViewPayload {
  viewId: string;
  shouldPreventClose: boolean;
}

export interface ScreenManagerModuleState {
  screenManagerStore?: ScreenManagerPreset;
}

// base ScreenManagerComponentLookupPayload type. This can be extended in libraries to make it more specific
export interface ScreenManagerComponentLookupPayload {
  componentType: string;
  id: string;
  title?: string;
  initialProps: unknown;
}

export type ScreenManagerLookupFunctionType = (
  payload: ScreenManagerComponentLookupPayload,
) => React.ReactElement;
