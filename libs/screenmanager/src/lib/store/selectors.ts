/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from '@reduxjs/toolkit';
import { AppStore } from '../types/types';
import { initialState } from './reducer';
import { ScreenManagerPreset, ScreenManagerViewType } from './types';

const screenManagerStore = (store: AppStore): ScreenManagerPreset =>
  store.screenManagerStore || initialState;

/**
 * Gets screenmanager state
 *
 * @param {object} store store: object - Store object
 * @returns {object} returnType: ScreenManagerState
 */
export const getScreenManagerState = createSelector(
  screenManagerStore,
  (store) => store || initialState,
);

/**
 * Gets mosaicNode from screenmanager state
 *
 * @param {object} store store: object - Store object
 * @returns {MosaicNode<string>} returnType: MosaicNode<string>
 */
export const getMosaicNode = createSelector(
  getScreenManagerState,
  (state) => state && state.mosaicNode,
);

/**
 * Gets view from screenmanager state
 *
 * @param {object} store store: object - Store object
 * @param {string} viewId viewId: string - view id
 * @returns {object} returnType: ScreenManagerViewType
 */
export const getViewById = (
  store: AppStore,
  viewId: string,
): ScreenManagerViewType => {
  if (store?.screenManagerStore?.views.byId[viewId]) {
    return store.screenManagerStore.views.byId[viewId];
  }
  return null;
};

/**
 * Gets getShouldPreventClose from screenmanager state
 *
 * @param {object} store store: object - Store object
 * @param {string} viewId viewId: string - view id
 * @returns {boolean} returnType: boolean
 */
export const getShouldPreventClose = createSelector(
  getViewById,
  (store) => store?.shouldPreventClose || false,
);
