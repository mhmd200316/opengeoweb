![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/libs/screenmanager/package.json)
![coverage](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg?job=test-screenmanager)

# ScreenManager

React component library with ScreenManager components for the opengeoweb project. This library was generated with [Nx](https://nx.dev).

## Installation

```
npm install @opengeoweb/screenmanager
```

## Use

You can use any component exported from screenmanager by importing them, for example:

```
import { InitSyncGroups,
  ScreenManagerViewConnect, } from '@opengeoweb/screenmanager';
```

## Running unit tests

Run `nx test screenmanager` to execute the unit tests via [Jest](https://jestjs.io).

## Documentation

https://opengeoweb.gitlab.io/opengeoweb/docs/screenmanager/
