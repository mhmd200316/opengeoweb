![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/libs/taf/package.json)
![coverage](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg?job=test-taf)

# Taf

React component library with Taf components for the opengeoweb project.
This library was generated with [Nx](https://nx.dev).

## Installation

```
npm install @opengeoweb/taf
```

## API

The front-end uses the [axios](https://github.com/axios/axios) library to fetch data from the api. Importing components from the Taf repository should be wrapped by the ApiProvider component with a specified baseURL. Example:

```
import { ApiProvider } from '@opengeoweb/api';
import { TafModule } from '@opengeoweb/taf';

const myAuth = {
  username: 'string',
  token: 'string',
  refresh_token: 'string',
};

const Component = () => {
  const [auth, onSetAuth] = React.useState(myAuth);

  return (
      <ApiProvider
        baseURL="http://test.com"
        appURL="http://app.com"
        auth={auth}
        onSetAuth={onSetAuth}
        createApi={createApi}
        authTokenUrl="url/tokenrefresh"
      >
        <TafModule />
      </ApiProvider>
  )}
```

## Documentation

https://opengeoweb.gitlab.io/opengeoweb/docs/taf/
