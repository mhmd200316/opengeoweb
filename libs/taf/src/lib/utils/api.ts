/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { createApiInstance, Credentials } from '@opengeoweb/api';
import { AxiosInstance } from 'axios';
import { Taf, TafFromBackend, TafFromFrontEnd } from '../types';

export type TafApi = {
  getTafList: () => Promise<{ data: TafFromBackend[] }>;
  postTaf: (formData: TafFromFrontEnd) => Promise<void>;
  getTAC: (tafData: Taf, signal?: AbortSignal) => Promise<{ data: string }>;
  patchTaf: (formData: TafFromFrontEnd) => Promise<void>;
};

const getApiRoutes = (axiosInstance: AxiosInstance): TafApi => ({
  getTafList: (): Promise<{ data: TafFromBackend[] }> => {
    return axiosInstance.get('/taflist');
  },
  postTaf: (formData: TafFromFrontEnd): Promise<void> => {
    return axiosInstance.post('/taf', { ...formData });
  },
  getTAC: (tafData: Taf, signal?: AbortSignal): Promise<{ data: string }> =>
    axiosInstance.post(
      '/taf2tac',
      ...[{ ...tafData }, ...(signal ? [{ signal }] : [])],
    ),
  patchTaf: (formData: TafFromFrontEnd): Promise<void> => {
    return axiosInstance.patch('/taf', { ...formData });
  },
});

export const createApi = (
  url: string,
  appUrl: string,
  auth: Credentials,
  setAuth: (cred: Credentials) => void,
  authTokenUrl: string,
  authClientId: string,
): TafApi => {
  const axiosInstance = createApiInstance(
    url,
    appUrl,
    auth,
    setAuth,
    authTokenUrl,
    authClientId,
  );
  return getApiRoutes(axiosInstance);
};
