/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as utils from '@opengeoweb/api';
import { createFakeApiInstance } from '@opengeoweb/api';
import { createApi } from './api';
import { fakeTafList } from './mockdata/fakeTafList';

jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual(
    '../../../../api/src/lib/components/ApiContext/utils',
  ) as Record<string, unknown>),
}));

describe('src/utils/api', () => {
  const fakeAxiosInstance = createFakeApiInstance();
  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );
      expect(api.getTafList).toBeTruthy();
      expect(api.postTaf).toBeTruthy();
      expect(api.getTAC).toBeTruthy();
    });

    it('should call with the right params for getTafList', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      await api.getTafList();
      expect(spy).toHaveBeenCalledWith('/taflist');
    });
    it('should call with the right params for postTaf', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const params = fakeTafList[0];
      await api.postTaf(params);
      expect(spy).toHaveBeenCalledWith('/taf', {
        ...params,
      });
    });
    it('should call with the right params for getTAC', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const params = fakeTafList[0].taf;
      await api.getTAC(params);
      expect(spy).toHaveBeenCalledWith('/taf2tac', {
        ...params,
      });
    });
  });
});
