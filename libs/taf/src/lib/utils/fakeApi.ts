/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { createFakeApiInstance } from '@opengeoweb/api';
import { AxiosInstance } from 'axios';
import { Taf, TafFromBackend, TafFromFrontEnd } from '../types';
import { TafApi } from './api';
import { fakeTafList } from './mockdata/fakeTafList';

export const fakeTafTac =
  'TAF EHAM 0812/0918 20020G35KT 0500 SN BKN040\nBECMG 0814/0816 0700 BKN001=';

let fakeUpdatedTafList = [...fakeTafList];
export const setFakeUpdatedList = (newList: TafFromBackend[]): void => {
  fakeUpdatedTafList = newList;
};
export const getFakeUpdatedList = (): TafFromBackend[] => fakeUpdatedTafList;

const getApiRoutes = (axiosInstance: AxiosInstance): TafApi => ({
  getTafList: (): Promise<{ data: TafFromBackend[] }> => {
    // eslint-disable-next-line no-console
    console.log('Retrieving new TAF list');

    return axiosInstance.get('/taflist').then(() => {
      return {
        data: getFakeUpdatedList(),
      };
    });
  },
  postTaf: (postTaf: TafFromFrontEnd): Promise<void> => {
    // eslint-disable-next-line no-console
    console.log(`The new status of the taf will be ${postTaf.changeStatusTo}`);

    // eslint-disable-next-line no-console
    console.log('form has been posted with following values:', postTaf.taf);
    return axiosInstance.post('/taf', postTaf);
  },
  getTAC: (taf: Taf, signal?: AbortSignal): Promise<{ data: string }> => {
    return axiosInstance
      .post('/taf2tac', ...[{ ...taf }, ...(signal ? [{ signal }] : [])])
      .then(() => ({
        data: fakeTafTac,
      }));
  },
  patchTaf: (patchTaf: TafFromFrontEnd): Promise<void> => {
    const updatedList = getFakeUpdatedList().map((tafFromBackend) => {
      if (tafFromBackend.taf.uuid === patchTaf.taf.uuid) {
        return { ...tafFromBackend, editor: patchTaf.editor };
      }
      return tafFromBackend;
    });

    setFakeUpdatedList(updatedList);

    // eslint-disable-next-line no-console
    console.log(
      `The new editor of the taf will be ${patchTaf.editor || 'empty'}`,
    );
    return axiosInstance.patch('/taf', patchTaf);
  },
});

export const createApi = (): TafApi => {
  const instance = createFakeApiInstance();
  return getApiRoutes(instance);
};
