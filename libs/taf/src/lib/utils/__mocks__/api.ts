/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { TafFromBackend } from '../../types';
import { fakeTafList } from '../mockdata/fakeTafList';

export const fakeTestTac = 'TAF EHAM 0812/0918 20020G35KT 0500 SN BKN040=';

export const api = {
  postTaf: (): Promise<void> => {
    return new Promise((resolve) => {
      resolve();
    });
  },
  patchTaf: (): Promise<void> => {
    return new Promise((resolve) => {
      resolve();
    });
  },
  getTafList: (): Promise<{ data: TafFromBackend[] }> => {
    return new Promise((resolve) => {
      resolve({ data: fakeTafList });
    });
  },
  getTAC: (): Promise<{ data: string }> => {
    return new Promise((resolve) => {
      resolve({
        data: fakeTestTac,
      });
    });
  },
};

// eslint-disable-next-line @typescript-eslint/ban-types
export const createApi = (): object => api;
