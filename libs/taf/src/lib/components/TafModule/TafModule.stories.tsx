/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  ToolContainerDraggable,
  calculateDialogSizeAndPosition,
} from '@opengeoweb/shared';
import { darkTheme } from '@opengeoweb/theme';
import { Paper } from '@mui/material';
import { TafThemeApiProvider } from '../Providers';

import TafModule from './TafModule';
import {
  fakeTafTac,
  createApi as createFakeApi,
  getFakeUpdatedList,
} from '../../utils/fakeApi';
import { TafFromBackend } from '../../types';
import { fakeTafList } from '../../utils/mockdata/fakeTafList';

export default { title: 'components/Taf Module' };

const { width, height, top, left } = calculateDialogSizeAndPosition();
const DemoWrapped = (): React.ReactElement => {
  return (
    <ToolContainerDraggable
      startSize={{ width, height }}
      startPosition={{ top, left }}
      onClose={(): void => {}}
      title="TAF"
    >
      <TafModule />
    </ToolContainerDraggable>
  );
};

const Demo = (): React.ReactElement => (
  <Paper sx={{ maxHeight: '100%', height: '100vh', overflow: 'auto' }}>
    <TafModule />
  </Paper>
);

export const TafModuleLightThemeWithErrorOnAction = (): React.ReactElement => {
  const getTafList = (): Promise<unknown> => {
    return new Promise((resolve) => {
      resolve({ data: fakeTafList });
    });
  };
  const postTaf = (): Promise<unknown> => {
    return new Promise<void>((resolve, reject) => {
      return setTimeout(() => {
        reject(new Error('Status change not allowed'));
      }, 1000);
    });
  };
  const patchTaf = (): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      return setTimeout(() => {
        // eslint-disable-next-line no-console
        console.log('Editor change failed');
        reject(new Error('Editor change failed'));
      }, 300);
    });
  };
  const getTAC = (): Promise<unknown> => {
    return new Promise((resolve) => {
      resolve({
        data: fakeTafTac,
      });
    });
  };
  return (
    <TafThemeApiProvider
      createApiFunc={(): {
        postTaf;
        getTafList;
        getTAC;
        patchTaf;
      } => {
        return {
          postTaf,
          getTafList,
          getTAC,
          patchTaf,
        };
      }}
    >
      <Demo />
    </TafThemeApiProvider>
  );
};

export const TafModuleLightThemeWithErrorOnLoadingList =
  (): React.ReactElement => {
    const getTafList = (): Promise<unknown> => {
      return new Promise<void>((resolve, reject) => {
        return setTimeout(() => {
          reject(new Error('Request failed with status code 502'));
        }, 1000);
      });
    };

    return (
      <TafThemeApiProvider
        createApiFunc={(): {
          getTafList;
        } => {
          return {
            getTafList,
          };
        }}
      >
        <Demo />
      </TafThemeApiProvider>
    );
  };

export const TafModuleWithNewData = (): React.ReactElement => {
  // returns different list every time it fetches to simulate new data
  let counter = 0;
  const getTafList = (): Promise<{ data: TafFromBackend[] }> => {
    return new Promise((resolve) => {
      const updatedFakeTafList = getFakeUpdatedList();

      counter += 1;
      const emptyList: TafFromBackend[] = updatedFakeTafList.map(
        (tafFromBackend) => ({
          ...tafFromBackend,
          taf: {
            ...tafFromBackend.taf,
            baseForecast: {
              valid: tafFromBackend.taf.baseForecast.valid,
            },
            changeGroups: [],
          },
        }),
      );

      const data = counter % 2 === 0 ? emptyList : updatedFakeTafList;

      resolve({ data });
    });
  };

  return (
    <TafThemeApiProvider
      createApiFunc={(): {
        getTafList;
      } => {
        return {
          ...createFakeApi(),
          getTafList,
        };
      }}
    >
      <Demo />
    </TafThemeApiProvider>
  );
};

export const TafModuleLightTheme = (): React.ReactElement => {
  return (
    <TafThemeApiProvider createApiFunc={createFakeApi}>
      <Demo />
    </TafThemeApiProvider>
  );
};

export const TafModuleDarkTheme = (): React.ReactElement => {
  return (
    <TafThemeApiProvider theme={darkTheme} createApiFunc={createFakeApi}>
      <Demo />
    </TafThemeApiProvider>
  );
};

export const TafModuleToolContainerExample = (): React.ReactElement => {
  return (
    <TafThemeApiProvider createApiFunc={createFakeApi}>
      <DemoWrapped />
    </TafThemeApiProvider>
  );
};
