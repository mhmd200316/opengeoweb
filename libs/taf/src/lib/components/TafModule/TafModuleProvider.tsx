/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { useApiContext } from '@opengeoweb/api';
import * as React from 'react';
import { BaseTaf, Taf, TafActions } from '../../types';
import { TafApi } from '../../utils/api';
import { useTACGenerator } from '../TafForm/utils';

type ValidateFormPromise = () => Promise<void | BaseTaf>;

interface TafModuleMethods {
  onPreventCloseView?: (shouldPreventClose: boolean) => void;
}

interface TafModuleContextState {
  onValidateForm: ValidateFormPromise;
  registerValidateForm: (promiseToRegister: ValidateFormPromise) => void;
  tafAction: TafActions;
  setTafAction: (action: TafActions) => void;
  TAC: string;
  updateTAC: (taf: Taf) => void;
  setTAC: (tac: string) => void;
  isFormDirty: boolean;
  setIsFormDirty: (isDirty: boolean) => void;
}

const TafModuleContext = React.createContext({
  onValidateForm: null,
  registerValidateForm: null,
  tafAction: null,
  setTafAction: null,
  isFormDirty: null,
  setIsFormDirty: null,
});

export const useTafModuleContext = (): Partial<TafModuleContextState> =>
  React.useContext(TafModuleContext);

export const useTafModuleState = ({
  onPreventCloseView,
}: TafModuleMethods): TafModuleContextState => {
  const { api } = useApiContext<TafApi>();
  const [onValidateForm, registerValidateForm] =
    React.useState<ValidateFormPromise>(() => new Promise(() => true));
  const [tafAction, setTafAction] = React.useState<TafActions>();
  const [isFormDirty, setIsFormDirty] = React.useState<boolean>(false);
  const [TAC, updateTAC, setTAC] = useTACGenerator(undefined, api.getTAC);

  const onRegisterValidateForm = (
    promiseToRegister: ValidateFormPromise,
  ): void => {
    registerValidateForm(() => promiseToRegister);
  };

  const onChangeFormDirty = (isDirty: boolean): void => {
    if (isDirty !== isFormDirty) {
      setIsFormDirty(isDirty);
      onPreventCloseView(isDirty);
    }
  };

  return {
    onValidateForm,
    registerValidateForm: onRegisterValidateForm,
    tafAction,
    setTafAction,
    TAC,
    updateTAC,
    setTAC,
    isFormDirty,
    setIsFormDirty: onChangeFormDirty,
  };
};

export interface TafModuleProviderProps extends TafModuleMethods {
  children?: React.ReactNode;
}

export const TafModuleProvider: React.FC<TafModuleProviderProps> = ({
  children,
  onPreventCloseView = (): void => {},
}: TafModuleProviderProps) => {
  const contextState = useTafModuleState({ onPreventCloseView });

  return (
    <TafModuleContext.Provider value={contextState}>
      {children}
    </TafModuleContext.Provider>
  );
};

export default TafModuleContext;
