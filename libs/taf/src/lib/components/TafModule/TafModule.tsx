/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { useApiContext } from '@opengeoweb/api';
import { AlertBanner, usePoller } from '@opengeoweb/shared';
import * as React from 'react';
import LinearProgress from '@mui/material/LinearProgress';
import { Box } from '@mui/material';
import moment from 'moment';
import TafLayout from '../TafLayout/TafLayout';
import { TafWrapper } from '../TafWrapper';
import { TafModuleProviderProps } from './TafModuleProvider';
import { TafFromBackend } from '../../types';
import { TafApi } from '../../utils/api';

export const getCurrentDate = (): string => moment.utc().format('HH:mm');

const TafModule = (): React.ReactElement => {
  const { api } = useApiContext<TafApi>();
  const [tafList, setTafList] = React.useState<TafFromBackend[]>([]);
  const [isLoading, setIsLoading] = React.useState<boolean>(true);
  const [getTafListError, setError] = React.useState<Error>(null);
  const [lastUpdateTime, setLastUpdateTime] = React.useState<string>();

  const fetchNewTafList = async (): Promise<void> => {
    setIsLoading(true);
    setError(null);
    try {
      const result = await api.getTafList();
      setTafList(result.data);
      setIsLoading(false);
      setLastUpdateTime(getCurrentDate());
    } catch (error) {
      setError(error);
      setIsLoading(false);
    }
  };

  const onUpdateTaf = (): void => {
    fetchNewTafList();
  };

  React.useEffect(() => {
    fetchNewTafList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  usePoller([tafList, getTafListError], fetchNewTafList);

  const hasTafListError = getTafListError !== null;

  return (
    <Box component="div" id="tafmodule" sx={{ height: '100%' }}>
      {isLoading && (
        <LinearProgress
          data-testid="loading-bar"
          color="secondary"
          sx={{ marginBottom: '-4px' }}
        />
      )}

      {hasTafListError && (
        <AlertBanner
          severity="error"
          title="An error has occurred while retrieving the list"
          info={getTafListError.message}
          dataTestId="taf-list-error"
          actionButtonProps={{
            title: 'Try again',
            onClick: fetchNewTafList,
          }}
        />
      )}
      <TafLayout
        tafList={tafList}
        onUpdateTaf={onUpdateTaf}
        lastUpdateTime={lastUpdateTime}
      />
    </Box>
  );
};

const Wrapper: React.FC<TafModuleProviderProps> = (
  props: TafModuleProviderProps,
) => (
  <TafWrapper {...props}>
    <TafModule />
  </TafWrapper>
);

export default Wrapper;
