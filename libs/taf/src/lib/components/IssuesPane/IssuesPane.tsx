/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box, Typography } from '@mui/material';
import { FieldError } from 'react-hook-form';
import { ToolContainerDraggable } from '@opengeoweb/shared';
import ReactDOM from 'react-dom';
import { ThumbsUp } from '@opengeoweb/theme';

import { IssuesPaneProps, IssuesPanePosition } from './types';

const getPaneTitle = (sumOfErrors: number): string => {
  switch (sumOfErrors) {
    case 0:
      return 'Looks good';
    case 1:
      return 'There is an issue to address';
    default:
      return 'There are a few issues to address';
  }
};

export const capitalizeWord = ([firstLetter, ...restOfWord]: string): string =>
  firstLetter.toUpperCase() + restOfWord.join('');

export const humanizeErrorMessage = (nameAsString: string): string => {
  const nameWithoutBrackets = nameAsString.replace(/[[\]']+/g, '');
  const nameLowerCase = nameWithoutBrackets.toLowerCase();
  const [fieldName, fieldNumberedValue] = nameLowerCase
    .split(/([0-9]+)/) // split num bers from field and build array
    .filter((s: string) => s.length);
  const nameCapitalized = capitalizeWord(fieldName);

  if (fieldNumberedValue) {
    // when name contains a number we assume it's a formgroup and we add a index +1 for human readability
    const fieldNumber = parseInt(fieldNumberedValue, 10) + 1;
    return `${nameCapitalized} ${fieldNumber}`;
  }

  return nameCapitalized;
};

export const getErrorMessage = (error: FieldError, index: number): string => {
  const { name } = error.ref;
  const [firstNamePart] = name.split('.');
  const fieldErrorTitle = humanizeErrorMessage(firstNamePart);

  return `${index + 1}. ${fieldErrorTitle}: ${error.message}`;
};

export const MODAL_DIALOG_ELEMENT = 'modal-dialog';

export const useIssuesPane = (): {
  isIssuesPaneOpen: boolean;
  startPosition: IssuesPanePosition;
  onToggleIssuesPane: (
    isOpen: boolean,
    newPosition: IssuesPanePosition,
  ) => void;
  onCloseIssuesPane: () => void;
} => {
  const [isIssuesPaneOpen, setIsIssuesPaneOpen] =
    React.useState<boolean>(false);
  const [startPosition, setStartPosition] =
    React.useState<IssuesPanePosition>(null);

  const onToggleIssuesPane = (
    isOpen: boolean,
    newPosition: IssuesPanePosition,
  ): void => {
    if (!startPosition) {
      setStartPosition(newPosition);
    }
    setIsIssuesPaneOpen(isOpen);
  };
  const onCloseIssuesPane = (): void => setIsIssuesPaneOpen(false);

  return {
    isIssuesPaneOpen,
    startPosition,
    onToggleIssuesPane,
    onCloseIssuesPane,
  };
};

const IssuesPane: React.FC<IssuesPaneProps> = ({
  errorList = [],
  isOpen,
  handleClose,
  startPosition = {
    left: 20,
    top: 50,
  },
}: IssuesPaneProps) => {
  const rootElement =
    document.getElementById(MODAL_DIALOG_ELEMENT) || document.body;

  return ReactDOM.createPortal(
    <ToolContainerDraggable
      startSize={{ width: '577px', height: '200px' }}
      startPosition={startPosition}
      minWidth={300}
      onClose={handleClose}
      title={getPaneTitle(errorList.length)}
      headerSize="xs"
      isOpen={isOpen}
      data-testid="moveable-issues-pane"
      bounds="parent"
    >
      <Box
        component="div"
        sx={{
          marginTop: '16px',
          marginLeft: '24px',
        }}
      >
        {errorList.length === 0 && (
          <Box
            component="div"
            sx={{ fontSize: '4rem' }}
            data-testid="zeroIssuesImage"
          >
            <ThumbsUp sx={{ height: '69px', width: '58px' }} />
          </Box>
        )}
        {errorList.length > 0 &&
          errorList.map((error, index) => (
            <Typography
              // eslint-disable-next-line react/no-array-index-key
              key={`message-${index}`}
              variant="body1"
              data-testid="errorMessage"
            >
              {getErrorMessage(error, index)}
            </Typography>
          ))}
      </Box>
    </ToolContainerDraggable>,
    rootElement,
  );
};

export default IssuesPane;
