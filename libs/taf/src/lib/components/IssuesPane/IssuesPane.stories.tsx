/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { FieldError } from 'react-hook-form';
import { Paper } from '@mui/material';
import IssuesPane from './IssuesPane';
import IssuesButton from './IssuesButton';
import { TafThemeApiProvider } from '../Providers';

export default {
  title: 'components/Issues Pane',
};

export const NoIssues = (): React.ReactElement => {
  return (
    <TafThemeApiProvider>
      <Paper sx={{ height: '300px', width: '700px' }}>
        <IssuesButton />
        <IssuesPane
          // eslint-disable-next-line no-console
          handleClose={(): void => console.log('close')}
          errorList={[]}
          isOpen
        />
      </Paper>
    </TafThemeApiProvider>
  );
};

NoIssues.storyName = 'IssuesButton no issues (takeSnapshot)';
const windError = {
  type: 'test wind error',
  ref: {
    name: 'wind',
  },
  message: 'incorrect format (e.g. format: 24015G25)',
};
const oneError = {
  baseForecast: {
    wind: windError,
  },
} as unknown as Record<string, FieldError>;

export const OneIssue = (): React.ReactElement => {
  return (
    <TafThemeApiProvider>
      <Paper sx={{ height: '300px', width: '700px' }}>
        <IssuesButton errors={oneError} />
        <IssuesPane
          // eslint-disable-next-line no-console
          handleClose={(): void => console.log('close')}
          errorList={[windError]}
          isOpen
        />
      </Paper>
    </TafThemeApiProvider>
  );
};

OneIssue.storyName = 'IssuesButton one issue (takeSnapshot)';

const multipleErrors = [
  'Wind: incorrect format (e.g. format: 24015G25)',
  'Visibility: incorrect format',
  'Wind: incorrect format (e.g. format: 24015G25)',
  'Visibility: incorrect format',
  'Wind: incorrect format (e.g. format: 24015G25)',
  'Visibility: incorrect format',
  'Wind: incorrect format (e.g. format: 24015G25)',
  'Visibility: incorrect format',
].map((error) => ({ ref: { name: 'test' }, message: error })) as FieldError[];

export const MultipleIssues = (): React.ReactElement => {
  return (
    <TafThemeApiProvider>
      <IssuesPane
        // eslint-disable-next-line no-console
        handleClose={(): void => console.log('close')}
        errorList={multipleErrors}
        isOpen
      />
    </TafThemeApiProvider>
  );
};

MultipleIssues.storyName = 'IssuesPane multiple issues';
