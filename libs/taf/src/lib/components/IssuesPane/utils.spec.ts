/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { filterDuplicateErrors } from './utils';

describe('components/IssuesPane/utils', () => {
  describe('filterDuplicateErrors', () => {
    it('should filter out duplicate errors', () => {
      const errorList = [
        {
          isChangeGroup: true,
          message: 'wind, visibility, weather or cloud is required',
          ref: { name: 'changeGroups[0].cloud.cloud1' },
          row: 0,
          type: 'validateRequiredSub',
          types: {
            validateRequiredSub:
              'wind, visibility, weather or cloud is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'wind, visibility, weather or cloud is required',
          ref: { name: 'changeGroups[0].weather.weather1' },
          row: 0,
          type: 'validateRequiredSub',
          types: {
            validateRequiredSub:
              'wind, visibility, weather or cloud is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'wind, visibility, weather or cloud is required',
          ref: { name: 'changeGroups[0].visibility' },
          row: 0,
          type: 'validateRequiredSub',
          types: {
            validateRequiredSub:
              'wind, visibility, weather or cloud is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'wind, visibility, weather or cloud is required',
          ref: { name: 'changeGroups[1].visibility' },
          row: 1,
          type: 'validateRequiredSub',
          types: {
            validateRequiredSub:
              'wind, visibility, weather or cloud is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'Probability or change is required',
          ref: { name: 'changeGroups[0].probability' },
          row: 0,
          type: 'validateRequiredMainOptional',
          types: {
            validateRequiredMainOptional: 'Probability or change is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'Probability or change is required',
          ref: { name: 'changeGroups[0].change' },
          row: 0,
          type: 'validateRequiredMainOptional',
          types: {
            validateRequiredMainOptional: 'Probability or change is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'Probability or change is required',
          ref: { name: 'changeGroups[1].change' },
          row: 1,
          type: 'validateRequiredMainOptional',
          types: {
            validateRequiredMainOptional: 'Probability or change is required',
          },
        },
        {
          isChangeGroup: true,
          message: 'Valid is required',
          ref: { name: 'changeGroups[1].valid' },
          row: 1,
          type: 'validateRequiredMain',
          types: {
            validateRequiredMain: 'Valid is required',
          },
        },
      ];
      expect(filterDuplicateErrors(errorList)).toEqual([
        errorList[0],
        errorList[3],
        errorList[4],
        errorList[6],
        errorList[7],
      ]);
    });
  });
});
