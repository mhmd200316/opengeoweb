/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { DeepMap, FieldError } from 'react-hook-form';

interface CustomErrorField extends FieldError {
  isChangeGroup: boolean;
  row: number;
}

export const getCustomError = (error: FieldError): CustomErrorField => {
  const { name } = error.ref;
  const isChangeGroup = name.includes('changeGroups');
  const matchRowNumber = name.match(/\[(.*?)\]/);
  const row = matchRowNumber ? parseInt(matchRowNumber[1], 10) : null;

  return {
    ...error,
    isChangeGroup,
    row,
  };
};

export const getIssuesLabel = (sumOfErrors: number): string => {
  switch (sumOfErrors) {
    case 0:
      return '0 issues';
    case 1:
      return '1 issue';
    default:
      return `${sumOfErrors} issues`;
  }
};

const getErrorValues = (fieldErrors: FieldError): FieldError[] => {
  if (!fieldErrors) return [];
  return Object.keys(fieldErrors).reduce((list, key) => {
    const error = { ...fieldErrors[key] };
    if (error.ref) {
      return list.concat(getCustomError(error));
    }
    return list.concat(getErrorValues(error));
  }, []);
};

export const filterDuplicateErrors = (
  errors: CustomErrorField[],
): CustomErrorField[] =>
  errors.reduce((list, error) => {
    // check on type validateRequiredSub and validateRequiredMainOptional since these are giving 'duplicates'
    if (error.type === 'validateRequiredSub') {
      const exist = list.find(
        (e) => e.type === 'validateRequiredSub' && e.row === error.row,
      );
      return exist ? list : list.concat(error);
    }
    if (error.type === 'validateRequiredMainOptional') {
      const exist = list.find(
        (e) => e.type === 'validateRequiredMainOptional' && e.row === error.row,
      );
      return exist ? list : list.concat(error);
    }
    return list.concat(error);
  }, []);

export const getErrors = (
  errors: DeepMap<Record<string, unknown>, FieldError>,
): CustomErrorField[] => {
  const errorList = Object.keys(errors).reduce((list, key) => {
    const errorValue = errors[key];

    if (Array.isArray(errorValue)) {
      const errorValues = errorValue.reduce(
        (l, i) => l.concat(getErrorValues(i)),
        [],
      );
      return list.concat(...errorValues);
    }
    return list.concat(getErrorValues(errorValue));
  }, []);
  return errorList;
};
