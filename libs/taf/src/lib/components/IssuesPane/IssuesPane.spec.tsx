/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { FieldError } from 'react-hook-form';
import IssuesPane, {
  capitalizeWord,
  getErrorMessage,
  humanizeErrorMessage,
} from './IssuesPane';

import { TafThemeApiProvider } from '../Providers';

describe('components/IssuesPane/IssuesPane', () => {
  it('should show the correct pane for zero issues', () => {
    const props = {
      handleClose: jest.fn(),
      errorList: [],
      isOpen: true,
    };
    const { container, getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <IssuesPane {...props} />
      </TafThemeApiProvider>,
    );

    expect(
      container.parentNode.querySelector(
        '[data-testid=moveable-issues-pane] h2',
      ).textContent,
    ).toEqual('Looks good');
    expect(getByTestId('zeroIssuesImage')).toBeTruthy();
    expect(queryByTestId('errorMessage')).toBeFalsy();
  });

  it('should show the correct pane for one issue', () => {
    const fakeError = {
      ref: {
        name: 'dummy wind',
      },
      message: 'wind has problems',
      type: 'validationWind',
    };
    const props = {
      handleClose: jest.fn(),
      errorList: [fakeError],
      isOpen: true,
    };
    const { container, getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <IssuesPane {...props} />
      </TafThemeApiProvider>,
    );

    expect(
      container.parentNode.querySelector(
        '[data-testid=moveable-issues-pane] h2',
      ).textContent,
    ).toEqual('There is an issue to address');
    expect(queryByTestId('zeroIssuesImage')).toBeFalsy();
    expect(getByTestId('errorMessage').textContent).toEqual(
      getErrorMessage(fakeError, 0),
    );
  });

  it('should show the correct pane for multiple issues', () => {
    const props = {
      isOpen: true,
      handleClose: jest.fn(),
      errorList: [
        {
          ref: {
            name: 'error name 1',
          },
          message: 'error 1 has problems',
          type: 'error1Problem',
        },
        {
          ref: {
            name: 'error name 2',
          },
          message: 'error 2 has problems',
          type: 'error2Problem',
        },
        {
          ref: {
            name: 'error name 3',
          },
          message: 'error 3 has problems',
          type: 'error3Problem',
        },
      ],
    };
    const { container, queryByTestId, getAllByTestId } = render(
      <TafThemeApiProvider>
        <IssuesPane {...props} />
      </TafThemeApiProvider>,
    );

    expect(
      container.parentNode.querySelector(
        '[data-testid=moveable-issues-pane] h2',
      ).textContent,
    ).toEqual('There are a few issues to address');
    expect(queryByTestId('zeroIssuesImage')).toBeFalsy();
    expect(getAllByTestId('errorMessage').length).toEqual(
      props.errorList.length,
    );
  });

  it('should close the pane', () => {
    const props = {
      isOpen: true,
      handleClose: jest.fn(),
      errorList: [],
    };
    const { getByTestId } = render(
      <TafThemeApiProvider>
        <IssuesPane {...props} />
      </TafThemeApiProvider>,
    );

    fireEvent.click(getByTestId('closeBtn'));
    expect(props.handleClose).toHaveBeenCalledTimes(1);
  });

  it('should open the pane on given start position', () => {
    const props = {
      isOpen: true,
      handleClose: jest.fn(),
      errorList: [],
      startPosition: {
        left: 100,
        top: 100,
      },
    };
    const { container } = render(
      <TafThemeApiProvider>
        <IssuesPane {...props} />
      </TafThemeApiProvider>,
    );
    const containerStyle = getComputedStyle(
      container.parentElement.querySelector('.react-draggable'),
    );

    expect(containerStyle.left).toEqual(`${props.startPosition.left}px`);
    expect(containerStyle.top).toEqual(`${props.startPosition.top}px`);
  });

  it('should return the correct message', () => {
    const [error, error2, error3, error4] = [
      {
        ref: {
          name: 'testing',
        },
        message: 'this is a test',
      },
      {
        ref: {
          name: 'formgroups[0].testing1',
        },
        message: 'this is another test',
      },
      {
        ref: {
          name: 'formgroups[3].cloud.cloud2',
        },
        message: 'this is a test',
      },
      {
        ref: {
          name: 'baseforecast.wind',
        },
        message: 'wind is required',
      },
    ] as FieldError[];

    expect(getErrorMessage(error, 0)).toEqual(
      `${1}. ${capitalizeWord(error.ref.name)}: ${error.message}`,
    );

    expect(getErrorMessage(error2, 3)).toEqual(
      '4. Formgroups 1: this is another test',
    );

    expect(getErrorMessage(error3, 0)).toEqual(
      '1. Formgroups 4: this is a test',
    );

    expect(getErrorMessage(error4, 2)).toEqual(
      '3. Baseforecast: wind is required',
    );
  });

  it('should capitalize a word', () => {
    expect(capitalizeWord('test')).toEqual('Test');
    expect(capitalizeWord('2Test')).toEqual('2Test');
    expect(capitalizeWord('with.some.dots')).toEqual('With.some.dots');
    expect(capitalizeWord('WithCapital')).toEqual('WithCapital');
  });

  it('should make error message readable for humans', () => {
    expect(humanizeErrorMessage('test')).toEqual('Test');
    expect(humanizeErrorMessage('test[0]')).toEqual('Test 1');
    expect(humanizeErrorMessage('test[39]')).toEqual('Test 40');
  });
});
