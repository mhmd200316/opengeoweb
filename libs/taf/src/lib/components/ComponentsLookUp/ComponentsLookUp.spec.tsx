/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { store } from '@opengeoweb/core';
import { render, waitFor } from '@testing-library/react';
import * as React from 'react';
import { TafThemeApiProvider } from '../Providers';

import {
  componentsLookUp,
  ComponentsLookUpPayload,
  TafApiWrapper,
} from './ComponentsLookUp';

jest.mock('../../utils/api', () => ({
  createApi: (): unknown => ({
    getTafList: jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: [] });
      });
    }),
  }),
}));

describe('components/ComponentsLookUp/componentsLookUp', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should return TAF module', () => {
    const testPayload = {
      componentType: 'TafModule',
      id: 'test',
      initialProps: {},
    } as ComponentsLookUpPayload;

    const testConfig = {
      baseUrl: 'test',
      appUrl: 'test',
      authTokenUrl: 'test',
      authClientId: 'test',
    };

    const testAuth = { username: 'test user', token: '', refresh_token: '' };
    const testOnSetAuth = jest.fn();

    const result = componentsLookUp(testPayload, {
      config: testConfig,
      auth: testAuth,
      onSetAuth: testOnSetAuth,
    });

    expect(result.props['data-testid']).toEqual('tafmodule');
    expect(result.props.config).toEqual(testConfig);
    expect(result.props.auth).toEqual(testAuth);
  });

  it('should render TafApiWrapper', async () => {
    const config = {
      baseUrl: 'test',
      appUrl: 'test',
      authTokenUrl: 'test',
      authClientId: 'test',
    };

    const auth = { username: 'test user', token: '', refresh_token: '' };
    const onSetAuth = jest.fn();
    const props = {
      config,
      auth,
      onSetAuth,
    };
    const { container } = render(
      <TafThemeApiProvider store={store}>
        <TafApiWrapper {...props} />
      </TafThemeApiProvider>,
    );

    jest.runOnlyPendingTimers();
    await waitFor(() => {
      expect(container.querySelector('[id="tafmodule"]')).toBeTruthy();
    });
    expect(Element.prototype.scrollTo).toHaveBeenCalledTimes(1);
  });

  it('should not render TafApiWrapper when config is not provided', async () => {
    const config = undefined;
    const auth = { username: 'test user', token: '', refresh_token: '' };
    const onSetAuth = jest.fn();
    const props = {
      config,
      auth,
      onSetAuth,
    };
    const { container } = render(
      <TafThemeApiProvider store={store}>
        <TafApiWrapper {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(container.querySelector('[id="tafmodule"]')).toBeNull();
    });
  });
});
