/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';

import SwitchButton from './SwitchButton';
import { TafThemeApiProvider } from '../Providers';

describe('components/SwitchButton/SwitchButton', () => {
  it('should render SwitchButton with labels', () => {
    const { getByTestId, getByText } = render(
      <TafThemeApiProvider>
        <SwitchButton checked={false} onChange={jest.fn()} />
      </TafThemeApiProvider>,
    );
    expect(getByTestId('switchMode')).toBeTruthy();
    expect(getByTestId('switchMode').classList).not.toContain('Mui-checked');
    expect(getByText('Viewer')).toBeTruthy();
    expect(getByText('Editor')).toBeTruthy();
  });

  it('should handle onChange', () => {
    const mockOnChange = jest.fn();
    const checked = true;
    const { getByTestId, container } = render(
      <TafThemeApiProvider>
        <SwitchButton checked={checked} onChange={mockOnChange} />
      </TafThemeApiProvider>,
    );
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    fireEvent.click(
      container.querySelector('[data-testid="switchMode"] input'),
    );
    expect(mockOnChange).toHaveBeenCalled();
  });
});
