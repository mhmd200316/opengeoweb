/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Button } from '@mui/material';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import Spinner from './Spinner';

export default { title: 'components/Spinner' };

export const SpinnerDemo = (): React.ReactElement => {
  const [open, setOpen] = React.useState(false);

  return (
    <ThemeWrapperOldTheme>
      <Button
        variant="outlined"
        color="secondary"
        onClick={(): void => setOpen(true)}
      >
        Show Spinner
      </Button>
      <Button
        variant="outlined"
        color="secondary"
        // eslint-disable-next-line no-console
        onClick={(): void => console.log('Click!')}
      >
        Console log clicks
      </Button>
      {open && <Spinner />}
    </ThemeWrapperOldTheme>
  );
};
