/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box, Tooltip } from '@mui/material';
import { Edit, Clock, Success, Cancel } from '@opengeoweb/theme';
import { TafStatus, TimeSlot } from '../../types';

const IconWithTooltip: React.FC<{ title: string }> = ({ title, children }) => (
  <Tooltip title={title} placement="right">
    <Box component="span">{children}</Box>
  </Tooltip>
);

interface StatusIconProps {
  timeSlot: TimeSlot;
  status: TafStatus;
}

const StatusIcon: React.FC<StatusIconProps> = ({
  timeSlot,
  status,
}: StatusIconProps) => {
  if (status === 'NEW') {
    return null;
  }
  if (status === 'EXPIRED') {
    return (
      <IconWithTooltip title="Expired">
        <Clock
          data-testid="status-expired"
          sx={{
            transform: 'scaleX(-1)',
            color: 'geowebColors.buttons.tertiary.disabled.color',
          }}
        />
      </IconWithTooltip>
    );
  }

  if (status === 'CANCELLED') {
    return (
      <IconWithTooltip title="Cancelled">
        <Cancel
          data-testid="status-cancelled"
          sx={{ color: 'geowebColors.captions.captionError.rgba' }}
        />
      </IconWithTooltip>
    );
  }

  if (
    status === 'DRAFT' ||
    status === 'DRAFT_CORRECTED' ||
    status === 'DRAFT_AMENDED'
  ) {
    return (
      <IconWithTooltip title="Draft">
        <Edit
          data-testid="status-draft"
          sx={{ color: 'geowebColors.buttons.tertiary.default.color' }}
        />
      </IconWithTooltip>
    );
  }

  return timeSlot === 'ACTIVE' ? (
    <IconWithTooltip title="Active">
      <Success
        data-testid="status-active"
        sx={{ color: 'geowebColors.functional.success' }}
      />
    </IconWithTooltip>
  ) : (
    <IconWithTooltip title="Upcoming">
      <Clock
        data-testid="status-upcoming"
        sx={{ color: 'geowebColors.functional.success' }}
      />
    </IconWithTooltip>
  );
};

export default StatusIcon;
