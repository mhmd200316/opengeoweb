/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';

import { render, screen } from '@testing-library/react';
import StatusIcon from './StatusIcon';
import { TafThemeApiProvider } from '../Providers';

describe('components/StatusIcon/StatusIcon', () => {
  it('should return correct status icons', () => {
    const { rerender } = render(<StatusIcon status="NEW" timeSlot="ACTIVE" />);

    // status new
    expect(
      render(
        <StatusIcon status="NEW" timeSlot="ACTIVE" />,
      ).container.querySelector('.MuiSvgIcon-root'),
    ).toBeFalsy();
    // status expired
    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="EXPIRED" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-expired')).toBeTruthy();
    // status cancelled
    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="CANCELLED" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-cancelled')).toBeTruthy();
    // status draft
    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="DRAFT" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.queryByTestId('status-draft')).toBeTruthy();

    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="DRAFT_CORRECTED" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.queryByTestId('status-draft')).toBeTruthy();

    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="DRAFT_AMENDED" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.queryByTestId('status-draft')).toBeTruthy();

    // upcoming
    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="PUBLISHED" timeSlot="UPCOMING" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-upcoming')).toBeTruthy();
  });
});
