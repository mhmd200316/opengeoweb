/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import moment from 'moment';
import { isEqual } from 'lodash';
import React from 'react';
import Sortable from 'sortablejs';
import { useIsMounted } from '@opengeoweb/shared';
import {
  formatValidToString,
  parseValidToObject,
} from './TafFormRow/validations/validField';
import {
  BaseForecast,
  BaseForecastFormData,
  ChangeGroup,
  ChangeGroupFormData,
  MessageType,
  Taf,
  TafActions,
  TafFormData,
  TafStatus,
} from '../../types';
import {
  formatVisibilityToString,
  parseVisibilityToObject,
} from './TafFormRow/validations/visibility';
import { parseWeatherToObject } from './TafFormRow/validations/weather';
import {
  formatWindToString,
  parseWindToObject,
} from './TafFormRow/validations/wind';
import {
  formatCloudsToString,
  parseCloudsToObject,
} from './TafFormRow/validations/clouds';
import { FieldNames } from './TafFormRow/types';

export const emptyBaseForecast = {
  wind: '',
  visibility: '',
  weather: {
    weather1: '',
    weather2: '',
    weather3: '',
  },
  cloud: {
    cloud1: '',
    cloud2: '',
    cloud3: '',
    cloud4: '',
  },
};

// formatters
export const formatIssueTime = (issueDate: string): string => {
  return issueDate
    ? `${moment(issueDate).utc().format('DDHHmm')}Z`
    : 'Not issued';
};

type BaseForecastOrChangeGroupFormData<T extends BaseForecast | ChangeGroup> =
  T extends ChangeGroup ? ChangeGroupFormData : BaseForecastFormData;

export function formatFormValues<T extends BaseForecast | ChangeGroup>(
  row: T,
): BaseForecastOrChangeGroupFormData<T> {
  return Object.keys(row).reduce((list, key) => {
    switch (key) {
      case 'valid': {
        return {
          ...list,
          valid: formatValidToString(list[key]),
        };
      }
      case 'wind': {
        return {
          ...list,
          wind: formatWindToString(list[key]),
        };
      }
      case 'visibility': {
        // Assume if visibility field set - no cavOK
        return {
          ...list,
          visibility: formatVisibilityToString(list[key]),
        };
      }
      case 'cavOK': {
        // Assume if cavOK field set to true - no visibility field, otherwise return the original list containing the visibility
        return list[key]
          ? {
              ...list,
              visibility: formatVisibilityToString(list[key]),
            }
          : list;
      }
      case 'cloud': {
        return {
          ...list,
          cloud: formatCloudsToString(list[key]),
        };
      }
      default: {
        return list;
      }
    }
  }, row) as unknown as BaseForecastOrChangeGroupFormData<T>;
}

type BaseForecastOrChangeGroup<
  T extends BaseForecastFormData | ChangeGroupFormData,
> = T extends ChangeGroupFormData ? ChangeGroup : BaseForecast;

// parsers
function reduceRowValues<T extends BaseForecastFormData | ChangeGroupFormData>(
  row: T,
  tafStart: string,
  tafEnd: string,
): BaseForecastOrChangeGroup<T> {
  return Object.keys(row).reduce((list, key) => {
    switch (key) {
      case 'valid': {
        return {
          ...list,
          valid: parseValidToObject(list[key], tafStart, tafEnd),
        };
      }
      case 'wind': {
        return {
          ...list,
          wind: parseWindToObject(list[key]),
        };
      }
      case 'visibility': {
        return {
          ...list,
          ...parseVisibilityToObject(list[key]),
        };
      }
      case 'weather': {
        return {
          ...list,
          weather: parseWeatherToObject(list[key]),
        };
      }
      case 'cloud': {
        return {
          ...list,
          cloud: parseCloudsToObject(list[key]),
        };
      }
      // Trim string field
      case 'change':
      case 'probability': {
        return {
          ...list,
          [key]: list[key]?.trim(),
        };
      }
      default: {
        return list;
      }
    }
  }, row) as unknown as BaseForecastOrChangeGroup<T>;
}

export function parseFormValues<
  T extends ChangeGroupFormData | BaseForecastFormData,
>(row: T, tafStart: string, tafEnd: string): BaseForecastOrChangeGroup<T> {
  const reducedRow = reduceRowValues(row, tafStart, tafEnd);
  return Object.keys(reducedRow).reduce((list, key) => {
    switch (key) {
      case 'cavOK': {
        // if cavOK is set - remove visibility from the list
        if (reducedRow.cavOK) {
          const { visibility, ...rest } = list;
          return rest;
        }
        return list;
      }
      case 'cloud': {
        // If cloud is undefined or empty, remove it from the list
        if (!reducedRow.cloud || Object.keys(reducedRow.cloud).length === 0) {
          const { cloud, ...rest } = list;
          return rest;
        }
        return list;
      }
      case 'weather': {
        // If empty, remove it from the list
        if (!reducedRow.weather.weather1) {
          const { weather, ...rest } = list;
          return rest;
        }
        return list;
      }
      case 'wind': {
        // If empty, remove it from the list
        if (
          !reducedRow.wind.direction &&
          isNaN(Number(reducedRow.wind.direction))
        ) {
          const { wind, ...rest } = list;
          return rest;
        }
        return list;
      }
      case 'visibility': {
        // If empty, remove it from the list
        if (
          !reducedRow.visibility.range &&
          isNaN(reducedRow.visibility.range)
        ) {
          const { visibility, ...rest } = list;
          return rest;
        }
        return list;
      }
      case 'change':
      case 'probability':
        // If empty, remove it from the list
        if (!reducedRow[key]) {
          const { [key]: emptyKey, ...rest } = list as ChangeGroup;
          return rest;
        }
        return list;

      case 'valid': {
        // If empty, remove it from the list
        if (!reducedRow[key]) {
          const { [key]: emptyKey, ...rest } = list;
          return rest;
        }
        return list;
      }
      default: {
        return list;
      }
    }
  }, reducedRow) as unknown as BaseForecastOrChangeGroup<T>;
}

export const convertTafValuesToObject = (formData: TafFormData): Taf => {
  const { IS_DRAFT, ...productToPost } = formData;
  const tafStart = productToPost.validDateStart;
  const tafEnd = productToPost.validDateEnd;

  const parsedChangeGroups =
    productToPost.changeGroups &&
    productToPost.changeGroups
      // parse data for each changegroup row
      .map((row) => parseFormValues(row, tafStart, tafEnd) as ChangeGroup)
      // filter out empty changegroup rows
      .filter((value: ChangeGroup) => Object.keys(value).length);

  const parsedBaseForecast =
    productToPost.baseForecast &&
    parseFormValues(productToPost.baseForecast, tafStart, tafEnd);

  const parsedFormValues = {
    ...productToPost,
    baseForecast: parsedBaseForecast,
    changeGroups: parsedChangeGroups,
  };

  return Object.keys(parsedFormValues).reduce((list, key) => {
    switch (key) {
      case 'changeGroups': {
        // remove changeGroups when empty
        if (
          !parsedFormValues.changeGroups ||
          !parsedFormValues.changeGroups.length
        ) {
          const { changeGroups, ...rest } = list;
          return rest;
        }
        return list;
      }
      case 'previousId':
      case 'previousValidDateStart':
      case 'previousValidDateEnd':
      case 'issueDate': {
        // remove when empty
        if (!parsedFormValues[key]) {
          const { [key]: emptyKey, ...rest } = list;
          return rest;
        }
        return list;
      }

      default: {
        return list;
      }
    }
  }, parsedFormValues);
};

// Ensure we have enough data to retrieve a TAC - we only need to check for wind and visibility/ cavok as those are the only ones required in all cases
export const shouldRetrieveTAC = (product: Taf): boolean => {
  if (
    product &&
    product.baseForecast &&
    product.baseForecast.wind &&
    'speed' in product.baseForecast.wind &&
    product.baseForecast.wind.speed !== '' &&
    ((product.baseForecast.visibility &&
      'range' in product.baseForecast.visibility &&
      product.baseForecast.visibility.range !== ('' as unknown)) ||
      product.baseForecast.cavOK)
  ) {
    return true;
  }
  return false;
};

export const getFieldNames = (
  isChangeGroup: boolean,
  index: number,
): FieldNames => {
  const namePrefix = !isChangeGroup ? 'baseForecast' : `changeGroups[${index}]`;
  return {
    ...(!isChangeGroup && {
      messageType: 'messageType',
      location: 'location',
      issueDate: 'issueDate',
    }),
    ...(isChangeGroup && {
      probability: `${namePrefix}.probability`,
      change: `${namePrefix}.change`,
    }),
    valid: `${namePrefix}.valid`,
    wind: `${namePrefix}.wind`,
    visibility: `${namePrefix}.visibility`,
    weather1: `${namePrefix}.weather.weather1`,
    weather2: `${namePrefix}.weather.weather2`,
    weather3: `${namePrefix}.weather.weather3`,
    cloud1: `${namePrefix}.cloud.cloud1`,
    cloud2: `${namePrefix}.cloud.cloud2`,
    cloud3: `${namePrefix}.cloud.cloud3`,
    cloud4: `${namePrefix}.cloud.cloud4`,
  };
};

export const TAC_MISSING_DATA = 'Missing data: no TAC can be generated';
export const TAC_NOT_AVAILABLE = 'No TAC available';
export const TAC_RETRIEVING_DATA = 'Retrieving TAC';
export const TAC_FAILED = 'Failed to retrieve TAC';

export const tacHasError = (tac: string): boolean => {
  return (
    tac === TAC_MISSING_DATA ||
    tac === TAC_NOT_AVAILABLE ||
    tac === TAC_RETRIEVING_DATA ||
    tac === TAC_FAILED
  );
};

export const useTACGenerator = (
  taf: Taf,
  requestTAC: (
    postTaf: unknown,
    abortSignal?: AbortSignal,
  ) => Promise<{ data: string }>,
): [string, (tafToPost) => void, (newTAC: string) => void] => {
  const [TAC, setTAC] = React.useState(
    !taf || taf.status === 'NEW' ? TAC_NOT_AVAILABLE : TAC_RETRIEVING_DATA,
  );
  const { isMounted } = useIsMounted();

  const controller = React.useRef(null);

  const onSetTac = (newTAC: string): void => {
    if (controller.current) {
      controller.current.abort();
    }
    setTAC(newTAC);
  };

  const retrieveTAC = React.useCallback(
    (tafToPost): void => {
      if (shouldRetrieveTAC(tafToPost)) {
        if (controller.current) {
          controller.current.abort();
        }

        controller.current = new AbortController();
        setTAC(TAC_RETRIEVING_DATA);
        requestTAC(tafToPost, controller.current.signal)
          .then((result) => {
            if (isMounted.current) {
              setTAC(result.data);
            }
          })
          .catch((error) => {
            if (isMounted && error.message !== 'canceled') {
              setTAC(TAC_FAILED);
            }
          });
      } else {
        setTAC(TAC_MISSING_DATA);
      }
    },
    [isMounted, requestTAC],
  );

  React.useEffect(() => {
    if (taf && taf.status !== 'NEW') {
      retrieveTAC(taf);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return [TAC, retrieveTAC, onSetTac];
};

export type SortableField = {
  id: string;
  chosen: boolean; // more info https://github.com/SortableJS/Sortable#options
};

export const useDragOrder = (
  trigger: (name?: string | string[]) => Promise<boolean>,
  move: (oldIndex: number, newIndex: number) => void,
  onFormBlur: () => void,
): {
  onChangeOrder: (event: Sortable.SortableEvent) => void;
  onStartDrag: (event: Sortable.SortableEvent) => void;
  activeDraggingIndex: number;
} => {
  const [activeDraggingIndex, setIsDragging] = React.useState(null);
  const [lastDragOrder, onUpdatelastDragOrder] = React.useState([null, null]);

  const onChangeOrder = ({ oldIndex, newIndex }): void => {
    if (oldIndex === undefined || newIndex === undefined) return;
    move(oldIndex, newIndex);
    onUpdatelastDragOrder([oldIndex, newIndex]);
    setIsDragging(false);
  };

  const onStartDrag = ({ oldIndex }): void => setIsDragging(oldIndex);

  // triggers validation AFTER render of new sorted elements
  React.useEffect(() => {
    const [oldIndex, newIndex] = lastDragOrder;
    if (oldIndex !== null && newIndex !== null) {
      // trigger validation of all fields
      trigger();
    }
  }, [lastDragOrder, trigger]);

  // trigger onFormBlur AFTER render of new sorted elements so the TAC gets updated
  React.useEffect(() => {
    const [oldIndex, newIndex] = lastDragOrder;
    if (oldIndex !== null && newIndex !== null) {
      onFormBlur();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lastDragOrder]);

  return { onChangeOrder, activeDraggingIndex, onStartDrag };
};

// dialog utils
export const getConfirmationDialogTitle = (action: TafActions): string => {
  if (action === 'DISCARD') return 'Discard TAF';
  if (action === 'PUBLISH') return 'Publish';
  if (action === 'AMEND') return 'This TAF is an amendment';
  if (action === 'CORRECT') return 'This TAF is a correction';
  if (action === 'CANCEL') return 'Cancel TAF';
  return 'Cancel TAF';
};

// Check if the amended/corrected TAF has been changed. Only compare the fields users are able to fill in
// (i.e. exclude baseforecast valid time as that gets updated when doing an amendment automatically outside of the users influence)
export const isTafAmendedCorrectedChanged = (
  isFormDirty: boolean,
  originalTaf: Taf,
  currentTaf: Taf,
): boolean => {
  const { baseForecast: originalBaseF, changeGroups: originalChG } =
    originalTaf;
  const { baseForecast: currentBaseF, changeGroups: currentChG } = currentTaf;
  const { valid: validOriginal, ...originalNoValid } = originalBaseF;
  const { valid: validCurrent, ...currentNoValid } = currentBaseF;
  return (
    isFormDirty &&
    (!isEqual(originalNoValid, currentNoValid) ||
      !isEqual(originalChG, currentChG))
  );
};

export const getConfirmationDialogContent = (
  action: TafActions,
  isFormDirty: () => boolean,
): string => {
  switch (action) {
    case 'DISCARD':
      return 'Are you sure you would like to discard this TAF? Any changes made will be lost';
    case 'PUBLISH':
      return 'Are you sure you want to publish this TAF?';
    case 'AMEND':
    case 'CORRECT':
      return isFormDirty()
        ? 'Do you want to publish it?'
        : 'Are you sure you want to publish the TAF? You did not make any changes!';
    default:
      return 'Are you sure you want to cancel this TAF?';
  }
};

export const getConfirmationDialogButtonLabel = (
  action: TafActions,
): string => {
  switch (action) {
    case 'DISCARD': {
      return 'Discard';
    }
    case 'CANCEL':
      return 'Cancel';
    default: {
      return 'Publish';
    }
  }
};

export const emptyChangegroup = {
  valid: '',
  wind: '',
  visibility: '',
  weather: {
    weather1: null,
    weather2: null,
    weather3: null,
  },
  cloud: {
    cloud1: null,
    cloud2: null,
    cloud3: null,
    cloud4: null,
  },
  change: null,
  probability: null,
};

export const getTafStatusLabel = (status: TafStatus): string => {
  switch (status) {
    case 'DRAFT_AMENDED':
      return 'draft amended';
    case 'DRAFT_CORRECTED':
      return 'draft corrected';
    default:
      return status.toLocaleLowerCase();
  }
};

export const getMessageType = (taf: Taf, tafAction: TafActions): string => {
  if (tafAction === 'CORRECT') {
    return 'COR';
  }
  if (tafAction === 'AMEND') {
    return 'AMD';
  }
  if (tafAction === 'CANCEL') {
    return 'CNL';
  }

  return taf.messageType;
};

// For corrections and amendment we need to add the previousValidStart and End for the TAC converter
export const getAmendedCorrectedTafTimes = (
  taf: Taf,
  tafAction: TafActions,
): Taf => {
  const previousValidDateStart =
    taf.previousValidDateStart && tafAction !== 'AMEND'
      ? taf.previousValidDateStart
      : taf.validDateStart;
  const previousValidDateEnd =
    taf.previousValidDateEnd && tafAction !== 'AMEND'
      ? taf.previousValidDateEnd
      : taf.validDateEnd;

  // For amendment change valid start time to current time
  // only do this if we're already within the validity period
  if (
    (tafAction === 'AMEND' || tafAction === 'DRAFT_AMEND') &&
    moment.utc() >= moment.utc(taf.validDateStart)
  ) {
    const now = moment.utc().format('YYYY-MM-DDTHH:mm:ss[Z]');

    const validObj = { start: now, end: taf.baseForecast.valid.end };
    return {
      ...taf,
      previousValidDateStart,
      previousValidDateEnd,
      validDateStart: now,
      baseForecast: { ...taf.baseForecast, valid: validObj },
    };
  }
  return {
    ...taf,
    previousValidDateStart,
    previousValidDateEnd,
  };
};

export const prepareTafValues = (
  _taf: Taf,
  tafAction: TafActions,
  addEmptyChangeGroup = false,
): TafFormData => {
  // If action is AMEND/CORRECT/DRAFT_AMEND/DRAFT_CORRECTION - add previous valid start and end
  // If action is AMEND/DRAFT_AMEND - update validity start time to current time
  const taf =
    tafAction === 'AMEND' ||
    tafAction === 'DRAFT_AMEND' ||
    tafAction === 'CORRECT' ||
    tafAction === 'DRAFT_CORRECT'
      ? getAmendedCorrectedTafTimes(_taf, tafAction)
      : _taf;

  const formattedBaseForecast = formatFormValues(taf.baseForecast);
  const baseForecast = {
    ...emptyBaseForecast,
    ...formattedBaseForecast,
    weather: {
      ...emptyBaseForecast.weather,
      ...formattedBaseForecast.weather,
    },
    cloud: {
      ...emptyBaseForecast.cloud,
      ...formattedBaseForecast.cloud,
    },
  };

  return {
    ...taf,
    messageType: getMessageType(taf, tafAction) as MessageType,
    baseForecast,
    ...(taf.changeGroups && {
      changeGroups: taf.changeGroups.map(formatFormValues),
    }),
    ...(addEmptyChangeGroup && {
      changeGroups: [emptyChangegroup],
    }),
  };
};

export const getIsFormDefaultDisabled = (status: TafStatus): boolean => {
  return !(
    status === 'NEW' ||
    status === 'DRAFT' ||
    status === 'DRAFT_AMENDED' ||
    status === 'DRAFT_CORRECTED'
  );
};

export const fromStatusToAction = (tafStatus: TafStatus): TafActions => {
  switch (tafStatus) {
    case 'DRAFT':
      return 'DRAFT';
    case 'DRAFT_AMENDED':
      return 'DRAFT_AMEND';
    case 'DRAFT_CORRECTED':
      return 'DRAFT_CORRECT';
    case 'PUBLISHED':
      return 'PUBLISH';
    case 'AMENDED':
      return 'AMEND';
    case 'CORRECTED':
      return 'CORRECT';
    case 'CANCELLED':
      return 'CANCEL';
    default:
      return 'NEW';
  }
};
