/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { ThemeProvider } from '@opengeoweb/theme';
import TafFormOptionsButton from './TafFormOptionsButton';

describe('components/TafForm/TafFormButtons/TafFormOptionsButton', () => {
  it('should insert 1 row below', () => {
    const props = {
      isChangeGroup: true,
      addChangeGroupAbove: jest.fn(),
      addChangeGroupBelow: jest.fn(),
      removeChangeGroup: jest.fn(),
      clearRow: jest.fn(),
      index: 0,
    };

    const { getByTestId, queryByText } = render(
      <ThemeProvider>
        <TafFormOptionsButton {...props} />
      </ThemeProvider>,
    );
    fireEvent.click(getByTestId('tafFormOptions[0]'));
    fireEvent.click(queryByText('Insert 1 row below'));
    expect(props.addChangeGroupBelow).toHaveBeenCalled();
    expect(props.removeChangeGroup).not.toHaveBeenCalled();
  });

  it('should insert 1 row above', () => {
    const props = {
      isChangeGroup: true,
      addChangeGroupAbove: jest.fn(),
      addChangeGroupBelow: jest.fn(),
      removeChangeGroup: jest.fn(),
      clearRow: jest.fn(),
      index: 0,
    };

    const { getByTestId, queryByText } = render(
      <ThemeProvider>
        <TafFormOptionsButton {...props} />
      </ThemeProvider>,
    );
    fireEvent.click(getByTestId('tafFormOptions[0]'));
    fireEvent.click(queryByText('Insert 1 row above'));
    expect(props.addChangeGroupAbove).toHaveBeenCalled();
  });

  it('should delete a changegroup row', () => {
    const props = {
      isChangeGroup: true,
      addChangeGroupAbove: jest.fn(),
      addChangeGroupBelow: jest.fn(),
      removeChangeGroup: jest.fn(),
      clearRow: jest.fn(),
      index: 0,
    };

    const { getByTestId, queryByText } = render(
      <ThemeProvider>
        <TafFormOptionsButton {...props} />
      </ThemeProvider>,
    );
    fireEvent.click(getByTestId('tafFormOptions[0]'));
    fireEvent.click(queryByText('Delete row'));
    expect(props.removeChangeGroup).toHaveBeenCalled();
  });

  it('should clear a row', () => {
    const props = {
      isChangeGroup: true,
      addChangeGroupAbove: jest.fn(),
      addChangeGroupBelow: jest.fn(),
      removeChangeGroup: jest.fn(),
      clearRow: jest.fn(),
      index: 0,
    };

    const { getByTestId, queryByText } = render(
      <ThemeProvider>
        <TafFormOptionsButton {...props} />
      </ThemeProvider>,
    );
    fireEvent.click(getByTestId('tafFormOptions[0]'));
    fireEvent.click(queryByText('Clear row'));
    expect(props.clearRow).toHaveBeenCalled();
  });

  it('should show the correct menu items for the baseforecast row', async () => {
    const props = {
      isChangeGroup: false,
      addChangeGroupAbove: jest.fn(),
      addChangeGroupBelow: jest.fn(),
      removeChangeGroup: jest.fn(),
      clearRow: jest.fn(),
      index: -1,
    };

    const { getByTestId, queryByText } = render(
      <ThemeProvider>
        <TafFormOptionsButton {...props} />
      </ThemeProvider>,
    );
    fireEvent.click(getByTestId('tafFormOptions[-1]'));
    expect(queryByText('Insert 1 row below')).toBeTruthy();
    expect(queryByText('Clear row')).toBeTruthy();
    expect(queryByText('Delete row')).toBeFalsy();
    expect(queryByText('Insert 1 row above')).toBeFalsy();
  });
});
