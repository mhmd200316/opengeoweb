/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';

import TafFormButtons from './TafFormButtons';
import { TafActions, TafCanbe } from '../../../types';
import { TafThemeApiProvider } from '../../Providers';

describe('components/TafForm/TafFormButtons/TafFormButtons', () => {
  it('should call the correct actions on buttonpress for status NEW', () => {
    const props = {
      tafAction: 'NEW' as TafActions,
      onTafEditModeButtonPress: jest.fn(),
      onTafViewModeButtonPress: jest.fn(),
    };

    const { getByTestId } = render(
      <TafThemeApiProvider>
        <TafFormButtons {...props} />
      </TafThemeApiProvider>,
    );

    expect(getByTestId('discardtaf')).toBeTruthy();
    expect(getByTestId('savedrafttaf')).toBeTruthy();
    expect(getByTestId('publishtaf')).toBeTruthy();

    fireEvent.click(getByTestId('discardtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('DISCARD');
    fireEvent.click(getByTestId('savedrafttaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('DRAFT');
    fireEvent.click(getByTestId('publishtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('PUBLISH');
  });

  it('should call the correct actions on buttonpress for status DRAFT', () => {
    const props = {
      tafAction: 'DRAFT' as TafActions,
      onTafEditModeButtonPress: jest.fn(),
      onTafViewModeButtonPress: jest.fn(),
    };

    const { getByTestId } = render(
      <TafThemeApiProvider>
        <TafFormButtons {...props} />
      </TafThemeApiProvider>,
    );

    expect(getByTestId('discardtaf')).toBeTruthy();
    expect(getByTestId('savedrafttaf')).toBeTruthy();
    expect(getByTestId('publishtaf')).toBeTruthy();

    fireEvent.click(getByTestId('discardtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('DISCARD');
    fireEvent.click(getByTestId('savedrafttaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('DRAFT');
    fireEvent.click(getByTestId('publishtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('PUBLISH');
  });

  it('should call the correct actions on buttonpress for status AMENDED', () => {
    const props = {
      tafAction: 'AMEND' as TafActions,
      onTafEditModeButtonPress: jest.fn(),
      onTafViewModeButtonPress: jest.fn(),
    };

    const { getByTestId } = render(
      <TafThemeApiProvider>
        <TafFormButtons {...props} />
      </TafThemeApiProvider>,
    );

    expect(getByTestId('discardtaf')).toBeTruthy();
    expect(getByTestId('savedrafttaf')).toBeTruthy();
    expect(getByTestId('publishtaf')).toBeTruthy();

    fireEvent.click(getByTestId('discardtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('DISCARD');
    fireEvent.click(getByTestId('savedrafttaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('DRAFT_AMEND');
    fireEvent.click(getByTestId('publishtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('AMEND');
  });

  it('should call the correct actions on buttonpress for status DRAFT_AMENDED', () => {
    const props = {
      tafAction: 'DRAFT_AMEND' as TafActions,
      onTafEditModeButtonPress: jest.fn(),
      onTafViewModeButtonPress: jest.fn(),
    };

    const { getByTestId } = render(
      <TafThemeApiProvider>
        <TafFormButtons {...props} />
      </TafThemeApiProvider>,
    );

    expect(getByTestId('discardtaf')).toBeTruthy();
    expect(getByTestId('savedrafttaf')).toBeTruthy();
    expect(getByTestId('publishtaf')).toBeTruthy();

    fireEvent.click(getByTestId('discardtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('DISCARD');
    fireEvent.click(getByTestId('savedrafttaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('DRAFT_AMEND');
    fireEvent.click(getByTestId('publishtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('AMEND');
  });

  it('should call the correct actions on buttonpress for status CORRECTED', () => {
    const props = {
      tafAction: 'CORRECT' as TafActions,
      onTafEditModeButtonPress: jest.fn(),
      onTafViewModeButtonPress: jest.fn(),
    };

    const { getByTestId } = render(
      <TafThemeApiProvider>
        <TafFormButtons {...props} />
      </TafThemeApiProvider>,
    );

    expect(getByTestId('discardtaf')).toBeTruthy();
    expect(getByTestId('savedrafttaf')).toBeTruthy();
    expect(getByTestId('publishtaf')).toBeTruthy();

    fireEvent.click(getByTestId('discardtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('DISCARD');
    fireEvent.click(getByTestId('savedrafttaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith(
      'DRAFT_CORRECT',
    );
    fireEvent.click(getByTestId('publishtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('CORRECT');
  });

  it('should call the correct actions on buttonpress for status DRAFT_CORRECTED', () => {
    const props = {
      isFormDisabled: false,
      tafAction: 'DRAFT_CORRECT' as TafActions,
      onTafEditModeButtonPress: jest.fn(),
      onTafViewModeButtonPress: jest.fn(),
    };

    const { getByTestId } = render(
      <TafThemeApiProvider>
        <TafFormButtons {...props} />
      </TafThemeApiProvider>,
    );

    expect(getByTestId('discardtaf')).toBeTruthy();
    expect(getByTestId('savedrafttaf')).toBeTruthy();
    expect(getByTestId('publishtaf')).toBeTruthy();

    fireEvent.click(getByTestId('discardtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('DISCARD');
    fireEvent.click(getByTestId('savedrafttaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith(
      'DRAFT_CORRECT',
    );
    fireEvent.click(getByTestId('publishtaf'));
    expect(props.onTafEditModeButtonPress).toHaveBeenCalledWith('CORRECT');
  });

  it('should display the correct buttons in view mode and cancel or amend a TAF', () => {
    const props = {
      isFormDisabled: true,
      canBe: [
        'CANCELLED' as TafCanbe,
        'AMENDED' as TafCanbe,
        'PUBLISHED' as TafCanbe,
      ],
      onTafEditModeButtonPress: jest.fn(),
      onTafViewModeButtonPress: jest.fn(),
    };

    const { getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <TafFormButtons {...props} />
      </TafThemeApiProvider>,
    );

    expect(getByTestId('canceltaf')).toBeTruthy();
    expect(getByTestId('amendtaf')).toBeTruthy();
    expect(queryByTestId('correcttaf')).toBeFalsy();
    expect(queryByTestId('publishtaf')).toBeFalsy();
    expect(queryByTestId('issuesButton')).toBeFalsy();

    fireEvent.click(getByTestId('canceltaf'));
    expect(props.onTafViewModeButtonPress).toHaveBeenLastCalledWith('CANCEL');
    fireEvent.click(getByTestId('amendtaf'));
    expect(props.onTafViewModeButtonPress).toHaveBeenLastCalledWith('AMEND');
    expect(props.onTafEditModeButtonPress).not.toHaveBeenCalled();
  });

  it('should display the correct buttons in view mode and correct a TAF', () => {
    const props = {
      isFormDisabled: true,
      canBe: ['CORRECTED' as TafCanbe, 'PUBLISHED' as TafCanbe],
      onTafEditModeButtonPress: jest.fn(),
      onTafViewModeButtonPress: jest.fn(),
    };

    const { getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <TafFormButtons {...props} />
      </TafThemeApiProvider>,
    );

    expect(getByTestId('correcttaf')).toBeTruthy();
    expect(queryByTestId('publishtaf')).toBeFalsy();
    expect(queryByTestId('amendtaf')).toBeFalsy();
    expect(queryByTestId('canceltaf')).toBeFalsy();
    expect(queryByTestId('issuesButton')).toBeFalsy();

    fireEvent.click(getByTestId('correcttaf'));
    expect(props.onTafViewModeButtonPress).toHaveBeenLastCalledWith('CORRECT');
    expect(props.onTafEditModeButtonPress).not.toHaveBeenCalled();
  });
});
