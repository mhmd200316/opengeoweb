/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { render } from '@testing-library/react';
import * as React from 'react';

import DragHandle, { DragHandleProps } from './DragHandle';

describe('TafForm/TafFormRow/DragHandle', () => {
  it('should render with default props', async () => {
    const { queryByTestId } = render(<DragHandle index={0} />);

    const button = queryByTestId('dragHandle-0');
    expect(button).toBeTruthy();
    expect(button.classList).toContain('handle');
    expect(getComputedStyle(button).cursor).toEqual('grab');
    expect(button.getAttribute('disabled')).toBeNull();
  });

  it('should be disabled', async () => {
    const props: DragHandleProps = {
      isDisabled: true,
      index: 1,
    };
    const { queryByTestId } = render(<DragHandle {...props} />);

    const button = queryByTestId('dragHandle-1');
    expect(button).toBeTruthy();
    expect(button.getAttribute('disabled')).toBeDefined();
  });

  it('should show the correct cursor while sorting', async () => {
    const props: DragHandleProps = {
      index: 1,
      isSorting: true,
    };
    const { queryByTestId } = render(<DragHandle {...props} />);

    const button = queryByTestId('dragHandle-1');
    expect(button).toBeTruthy();
    expect(button.getAttribute('disabled')).toBeDefined();
    expect(getComputedStyle(button).cursor).toEqual('grabbing');
  });
});
