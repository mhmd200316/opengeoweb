/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import TafFormTextField, { getDisplayValue } from './TafFormTextField';
import { TafThemeFormProvider } from '../../Providers';

describe('TafForm/TafFormRow/TafFormTextField', () => {
  describe('getDisplayValue', () => {
    it('should return a dash when disabled and no value', () => {
      expect(getDisplayValue('', true)).toEqual('-');
      expect(getDisplayValue(undefined, true)).toEqual('-');
    });

    it('should return the value when enabled or disabled and value', () => {
      expect(getDisplayValue('50', false)).toEqual('50');
      expect(getDisplayValue('50', true)).toEqual('50');
      expect(getDisplayValue('', false)).toEqual('');
    });
  });

  describe('TafFormTextField', () => {
    it('should autoComplete', () => {
      const { container } = render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
          }}
        >
          <TafFormTextField
            autoComplete={[
              {
                key: 'R',
                value: 'testing-autocomplete-string',
              },
            ]}
            name="testing"
            label="testing"
          />
        </TafThemeFormProvider>,
      );

      const input = container.querySelector('input');

      fireEvent.keyDown(input, { key: 'R' });

      expect(container.querySelector('input').getAttribute('value')).toEqual(
        'testing-autocomplete-string',
      );
    });

    it('should prevent user from typing after autocomplete', () => {
      const { container } = render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
          }}
        >
          <TafFormTextField
            autoComplete={[
              {
                key: 'R',
                value: 'testing-autocomplete-string',
              },
            ]}
            name="testing"
            label="testing"
          />
        </TafThemeFormProvider>,
      );

      const input = container.querySelector('input');

      fireEvent.keyDown(input, { key: 'R' });

      expect(container.querySelector('input').getAttribute('value')).toEqual(
        'testing-autocomplete-string',
      );

      expect(
        container.querySelector('input').getAttribute('maxLength'),
      ).toEqual('testing-autocomplete-string'.length.toString());

      // try to change
      fireEvent.keyDown(input, { key: 's' });

      expect(container.querySelector('input').getAttribute('value')).toEqual(
        'testing-autocomplete-string',
      );
    });

    it('should capitalize', async () => {
      const { container } = render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
          }}
        >
          <TafFormTextField name="testing" label="testing" />
        </TafThemeFormProvider>,
      );

      const input = container.querySelector('input');

      fireEvent.change(input, { target: { value: 'r' } });

      expect(input.value).toEqual('R');
    });

    it('should not autocomplete when input field already has a value', async () => {
      const { container } = render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
            defaultValues: {
              testing: '1',
            },
          }}
        >
          <TafFormTextField
            autoComplete={[
              {
                key: 'R',
                value: 'testing-autocomplete-string',
              },
            ]}
            name="testing"
            label="testing"
          />
        </TafThemeFormProvider>,
      );

      const input = container.querySelector('input');
      fireEvent.keyDown(input, { key: 'R' });

      expect(container.querySelector('input').getAttribute('value')).toEqual(
        '1',
      );
    });

    it('should trigger autocomplete when input field has value, but text is selected', () => {
      const { container } = render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
            defaultValues: {
              testing: 'testing',
            },
          }}
        >
          <TafFormTextField
            autoComplete={[
              {
                key: 'R',
                value: 'testing-autocomplete-string',
              },
            ]}
            name="testing"
            label="testing"
          />
        </TafThemeFormProvider>,
      );

      const input = container.querySelector('input');

      // trigger selection
      input.selectionStart = 0;
      input.selectionEnd = 7;

      fireEvent.keyDown(input, { key: 'R' });

      expect(container.querySelector('input').getAttribute('value')).toEqual(
        'testing-autocomplete-string',
      );
    });

    it('should trigger onchange when given', async () => {
      const mockOnChange = jest.fn();
      const { container } = render(
        <TafThemeFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
          }}
        >
          <TafFormTextField
            name="testing"
            label="testing"
            onChange={mockOnChange}
          />
        </TafThemeFormProvider>,
      );

      const input = container.querySelector('input');
      fireEvent.change(input, { target: { value: 'r' } });

      expect(mockOnChange).toHaveBeenCalled();
    });
  });
});
