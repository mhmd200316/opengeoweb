/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  formatWindToString,
  formatWindValues,
  invalidGustForWindP99Message,
  invalidWindDirectionMessage,
  invalidWindDirectionMinMaxMessage,
  invalidWindDirectionStepMessage,
  invalidWindGustsMessage,
  invalidWindGustSpeedBelow15Message,
  invalidWindGustSpeedCombinationMessage,
  invalidWindMessage,
  invalidWindSpeedForDirectionNonZeroMessage,
  invalidWindSpeedForDirectionZeroMessage,
  invalidWindSpeedMessage,
  invalidWindVRBWindDirectionMessage,
  parseWindToObject,
  validateWindField,
} from './wind';

describe('TafForm/TafFormRow/validations/wind', () => {
  describe('formatWindToString', () => {
    it('should format wind values without order of props affecting it', () => {
      expect(
        formatWindToString({
          direction: 180,
          speed: 10,
          unit: 'KT',
        }),
      ).toEqual('18010');
      expect(
        formatWindToString({
          speed: 10,
          direction: 180,
          unit: 'KT',
        }),
      ).toEqual('18010');
      expect(
        formatWindToString({
          direction: 180,
          speed: 10,
          gust: 50,
          unit: 'KT',
        }),
      ).toEqual('18010G50');
      expect(
        formatWindToString({
          direction: 180,
          speed: 'P99',
          unit: 'KT',
        }),
      ).toEqual('180P99');
      expect(
        formatWindToString({
          direction: 180,
          speed: 89,
          gust: 'P99',
          unit: 'KT',
        }),
      ).toEqual('18089GP99');
      expect(
        formatWindToString({
          direction: 'VRB',
          speed: 1,
          unit: 'KT',
        }),
      ).toEqual('VRB01');
      expect(
        formatWindToString({
          direction: 0,
          speed: 0,
          unit: 'KT',
        }),
      ).toEqual('00000');
      expect(
        formatWindToString({
          direction: 180,
          speed: 50,
          unit: 'KT',
          gust: '',
        }),
      ).toEqual('18050');
    });
    it('should return original value if passed something else', () => {
      expect(formatWindToString(null)).toEqual(null);
    });
  });

  describe('formatWindValues', () => {
    it('should format direction', () => {
      expect(formatWindValues('direction', 1)).toEqual('001');
      expect(formatWindValues('direction', 100)).toEqual('100');
      expect(formatWindValues('direction', 33)).toEqual('033');
      expect(formatWindValues('direction', 'VRB')).toEqual('VRB');
    });
    it('should format speed', () => {
      expect(formatWindValues('speed', 1)).toEqual('01');
      expect(formatWindValues('speed', 10)).toEqual('10');
      expect(formatWindValues('speed', 23)).toEqual('23');
      expect(formatWindValues('speed', 'P09')).toEqual('P99');
      expect(formatWindValues('speed', 'P99')).toEqual('P99');
    });
    it('should format gust', () => {
      expect(formatWindValues('gust', 1)).toEqual('G01');
      expect(formatWindValues('gust', 10)).toEqual('G10');
      expect(formatWindValues('gust', 23)).toEqual('G23');
      expect(formatWindValues('gust', 99)).toEqual('G99');
      expect(formatWindValues('gust', 'P09')).toEqual('GP99');
      expect(formatWindValues('gust', 'P99')).toEqual('GP99');
    });
    it('should return original value if passed something else', () => {
      expect(formatWindValues('dummy', 'hello')).toEqual('hello');
    });
  });

  describe('parseWindToObject', () => {
    it('should convert direction correctly (trim zeroes - parse to number or string)', () => {
      expect(parseWindToObject('00000')).toEqual({
        direction: 0,
        speed: 0,
        unit: 'KT',
      });
      expect(parseWindToObject('00100')).toEqual({
        direction: 1,
        speed: 0,
        unit: 'KT',
      });

      expect(parseWindToObject('VRB01')).toEqual({
        direction: 'VRB',
        speed: 1,
        unit: 'KT',
      });
      expect(parseWindToObject('VRB01KT')).toEqual({
        direction: 'VRB',
        speed: 1,
        unit: 'KT',
      });
      expect(parseWindToObject('00101')).toEqual({
        direction: 1,
        speed: 1,
        unit: 'KT',
      });
      expect(parseWindToObject('01001')).toEqual({
        direction: 10,
        speed: 1,
        unit: 'KT',
      });
      expect(parseWindToObject('12309')).toEqual({
        direction: 123,
        speed: 9,
        unit: 'KT',
      });
    });
    it('should convert speed correctly (parse to number or string  and accept P99)', () => {
      expect(parseWindToObject('00001')).toEqual({
        direction: 0,
        speed: 1,
        unit: 'KT',
      });
      expect(parseWindToObject('12000')).toEqual({
        direction: 120,
        speed: 0,
        unit: 'KT',
      });
      expect(parseWindToObject('18010')).toEqual({
        direction: 180,
        speed: 10,
        unit: 'KT',
      });
      expect(parseWindToObject('18099')).toEqual({
        direction: 180,
        speed: 99,
        unit: 'KT',
      });
      expect(parseWindToObject('180P99KT')).toEqual({
        direction: 180,
        speed: 'P99',
        unit: 'KT',
      });
    });
    it('should convert gust correctly (strip G, parse to number or string and accept P99) and ignore if KT', () => {
      expect(parseWindToObject('00001G15')).toEqual({
        direction: 0,
        speed: 1,
        gust: 15,
        unit: 'KT',
      });
      expect(parseWindToObject('12000G60')).toEqual({
        direction: 120,
        speed: 0,
        gust: 60,
        unit: 'KT',
      });
      expect(parseWindToObject('18045GP99')).toEqual({
        direction: 180,
        speed: 45,
        gust: 'P99',
        unit: 'KT',
      });
      expect(parseWindToObject('180P99GP99')).toEqual({
        direction: 180,
        speed: 'P99',
        gust: 'P99',
        unit: 'KT',
      });
      expect(parseWindToObject('18012')).toEqual({
        direction: 180,
        speed: 12,
        unit: 'KT',
      });
      expect(parseWindToObject('18012KT')).toEqual({
        direction: 180,
        speed: 12,
        unit: 'KT',
      });
    });
  });

  describe('validateWindField', () => {
    it('should return true for empty string', () => {
      expect(validateWindField('', '', '', '')).toEqual(true);
    });
    it('should return true for zero wind', () => {
      expect(validateWindField('00000', '', '', '')).toEqual(true);
    });

    it('should only allow numbers and chars in string', () => {
      expect(validateWindField(' dfhd7.d!', '', '', '')).toEqual(
        invalidWindMessage,
      );
      expect(validateWindField('200 20', '', '', '')).toEqual(
        invalidWindMessage,
      );
      expect(validateWindField('vrb20', '', '', '')).toEqual(
        invalidWindMessage,
      );
      expect(validateWindField('20020 ', '', '', '')).toEqual(true);
    });
    it('should return error when wind is less than 5 chars or greater than 12 chars and has other chars than VRB P KT or G', () => {
      expect(validateWindField('2', '', '', '')).toEqual(invalidWindMessage);
      expect(validateWindField('212', '', '', '')).toEqual(invalidWindMessage);
      expect(validateWindField('9522', '', '', '')).toEqual(invalidWindMessage);
      expect(validateWindField('1234567891234', '', '', '')).toEqual(
        invalidWindMessage,
      );
      expect(validateWindField('ABC02G20', '', '', '')).toEqual(
        invalidWindMessage,
      );
      expect(validateWindField('VGP02D20', '', '', '')).toEqual(
        invalidWindMessage,
      );
      expect(validateWindField('180P99GP99KT', '', '', '')).toEqual(true);
      expect(validateWindField('22020', '', '', '')).toEqual(true);
    });

    // Direction
    it('should accept only VRB as string - all other strings are invalid', () => {
      expect(validateWindField('VRB02', '', '', '')).toEqual(true);
      expect(validateWindField('VRB15G25', 'TS', '', '')).toEqual(true);
      expect(validateWindField('RVB02', '', '', '')).toEqual(
        invalidWindDirectionMessage,
      );
      expect(validateWindField('GPV02', '', '', '')).toEqual(
        invalidWindDirectionMessage,
      );
      expect(validateWindField('KTG02', '', '', '')).toEqual(
        invalidWindDirectionMessage,
      );
    });
    it('should accept numeric directions between 0 and 360 and must be rounded to the nearest step of 10', () => {
      expect(validateWindField('20020', '', '', '')).toEqual(true);
      expect(validateWindField('00000', '', '', '')).toEqual(true);
      expect(validateWindField('31010', '', '', '')).toEqual(true);
      expect(validateWindField('15010', '', '', '')).toEqual(true);
      expect(validateWindField('01010', '', '', '')).toEqual(true);
      expect(validateWindField('99920', '', '', '')).toEqual(
        invalidWindDirectionMinMaxMessage,
      );
      expect(validateWindField('36120', '', '', '')).toEqual(
        invalidWindDirectionMinMaxMessage,
      );
      expect(validateWindField('70020', '', '', '')).toEqual(
        invalidWindDirectionMinMaxMessage,
      );
      expect(validateWindField('02012', '', '', '')).toEqual(true);
      expect(validateWindField('18012', '', '', '')).toEqual(true);
      expect(validateWindField('22210', '', '', '')).toEqual(
        invalidWindDirectionStepMessage,
      );
      expect(validateWindField('00320', '', '', '')).toEqual(
        invalidWindDirectionStepMessage,
      );
      expect(validateWindField('33320', '', '', '')).toEqual(
        invalidWindDirectionStepMessage,
      );
    });

    // Speed
    it('should accept only P99 as string speed - all other speeds are invalid', () => {
      expect(validateWindField('VRBP99GP99', 'SQ', '', '')).toEqual(true);
      expect(validateWindField('02012', '', '', '')).toEqual(true);
      expect(validateWindField('19099', '', '', '')).toEqual(true);
      expect(validateWindField('00000', '', '', '')).toEqual(true);
      expect(validateWindField('1801G', '', '', '')).toEqual(
        invalidWindSpeedMessage,
      );
      expect(validateWindField('180P90', '', '', '')).toEqual(
        invalidWindSpeedMessage,
      );
      expect(validateWindField('1800G1', '', '', '')).toEqual(
        invalidWindSpeedMessage,
      );
    });

    // Gusts
    it('should accept only P99 as string gusts - all other gusts are invalid', () => {
      expect(validateWindField('VRBP99GP99', 'SQ', '', '')).toEqual(true);
      expect(validateWindField('02015GP99', '', '', '')).toEqual(true);
      expect(validateWindField('02015G35', '', '', '')).toEqual(true);
      expect(validateWindField('19099GP99', '', '', '')).toEqual(true);
      expect(validateWindField('01015G25', '', '', '')).toEqual(true);
      expect(validateWindField('180P99GP99', '', '', '')).toEqual(true);
      expect(validateWindField('18015GG1', '', '', '')).toEqual(
        invalidWindGustsMessage,
      );
      expect(validateWindField('180P99GPG9', '', '', '')).toEqual(
        invalidWindGustsMessage,
      );
      expect(validateWindField('18005G1', '', '', '')).toEqual(
        invalidWindGustsMessage,
      );
      expect(validateWindField('18005G1KT', '', '', '')).toEqual(
        invalidWindGustsMessage,
      );
    });

    // Unit
    it('should accept only KT as string units - all other units are invalid', () => {
      expect(validateWindField('22020', '', '', '')).toEqual(true);
      expect(validateWindField('22020KT', '', '', '')).toEqual(true);
      expect(validateWindField('22020G35KT', '', '', '')).toEqual(true);
      expect(validateWindField('22020GP99KT', '', '', '')).toEqual(true);
      expect(validateWindField('220P99GP99KT', '', '', '')).toEqual(true);
      expect(validateWindField('220P99KT', '', '', '')).toEqual(true);
      expect(validateWindField('22020K', '', '', '')).toEqual(
        invalidWindMessage,
      );
      expect(validateWindField('22020G200', '', '', '')).toEqual(
        invalidWindMessage,
      );
      expect(validateWindField('22020G200K', '', '', '')).toEqual(
        invalidWindMessage,
      );
      expect(validateWindField('22020G200KT', '', '', '')).toEqual(
        invalidWindMessage,
      );
    });

    // Validate direction, speed and gusts together
    it('should validate a minimum wind speed of 15 knots when gusts are given', () => {
      expect(validateWindField('22015G25', '', '', '')).toEqual(true);
      expect(validateWindField('22015GP99', '', '', '')).toEqual(true);
      expect(validateWindField('22015G24', '', '', '')).toEqual(
        invalidWindGustSpeedCombinationMessage,
      );
      expect(validateWindField('22014G24', '', '', '')).toEqual(
        invalidWindGustSpeedBelow15Message,
      );
      expect(validateWindField('22014GP99', '', '', '')).toEqual(
        invalidWindGustSpeedBelow15Message,
      );
      expect(validateWindField('22014G24', '', '', '')).toEqual(
        invalidWindGustSpeedBelow15Message,
      );
    });
    it('should validate a difference < 10 between speed and gust as invalid ', () => {
      expect(validateWindField('22020G30', '', '', '')).toEqual(true);
      expect(validateWindField('22020G80', '', '', '')).toEqual(true);
      expect(validateWindField('22099GP99', '', '', '')).toEqual(true);
      expect(validateWindField('220P99GP99', '', '', '')).toEqual(true);
      expect(validateWindField('22015G25', '', '', '')).toEqual(true);
      expect(validateWindField('22020G05', '', '', '')).toEqual(
        invalidWindGustSpeedCombinationMessage,
      );
      expect(validateWindField('22020G20KT', '', '', '')).toEqual(
        invalidWindGustSpeedCombinationMessage,
      );
      expect(validateWindField('22020G25', '', '', '')).toEqual(
        invalidWindGustSpeedCombinationMessage,
      );
      expect(validateWindField('22098G99', '', '', '')).toEqual(
        invalidWindGustSpeedCombinationMessage,
      );
      expect(validateWindField('22045G43', '', '', '')).toEqual(
        invalidWindGustSpeedCombinationMessage,
      );
      expect(validateWindField('220P99G20', '', '', '')).toEqual(
        invalidGustForWindP99Message,
      );
    });
    it('should validate the combination of direction, speed, gusts and weatherfield', () => {
      expect(validateWindField('00000', '', '', '')).toEqual(true);
      expect(validateWindField('01010', '', '', '')).toEqual(true);
      expect(validateWindField('36099', '', '', '')).toEqual(true);
      expect(validateWindField('360P99', '', '', '')).toEqual(true);
      expect(validateWindField('VRB02', '', '', '')).toEqual(true);
      expect(validateWindField('VRB03', '', '', '')).toEqual(true);
      expect(validateWindField('VRB25G75', 'TS', '', '')).toEqual(true);
      expect(validateWindField('VRB25G75', 'RA', 'SQ', '')).toEqual(true);
      expect(
        validateWindField('VRB25G75', 'RA', '-FZRADZ', '+SHGSRASN'),
      ).toEqual(true);

      expect(validateWindField('00010', '', '', '')).toEqual(
        invalidWindSpeedForDirectionZeroMessage,
      );
      expect(validateWindField('000P99', '', '', '')).toEqual(
        invalidWindSpeedForDirectionZeroMessage,
      );
      expect(validateWindField('01000', '', '', '')).toEqual(
        invalidWindSpeedForDirectionNonZeroMessage,
      );
      expect(validateWindField('VRB00', '', '', '')).toEqual(
        invalidWindSpeedForDirectionNonZeroMessage,
      );
      expect(validateWindField('VRB15', '', '', '')).toEqual(
        invalidWindVRBWindDirectionMessage,
      );
      expect(validateWindField('VRB25G75', 'RA', '-FZRADZ', '')).toEqual(
        invalidWindVRBWindDirectionMessage,
      );
      expect(validateWindField('VRB25G75', '', '', '')).toEqual(
        invalidWindVRBWindDirectionMessage,
      );
    });
  });
});
