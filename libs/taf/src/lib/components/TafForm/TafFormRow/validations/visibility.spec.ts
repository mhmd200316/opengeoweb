/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  formatVisibilityToString,
  parseVisibilityToObject,
  validateVisibilityField,
  invalidVisibilityMessage,
  invalidVisibilityRangeMessage,
  invalidVisibilityRange100mStepMessage,
  invalidVisibilityRange50mStepMessage,
  invalidVisibilityRange1000mStepMessage,
} from './visibility';

describe('TafForm/TafFormRow/validations/visibility', () => {
  describe('formatVisibilityToString', () => {
    it('should format non-cavOK visibility values by padding with zeroes', () => {
      expect(
        formatVisibilityToString({
          range: 0,
          unit: 'M',
        }),
      ).toEqual('0000');
      expect(
        formatVisibilityToString({
          range: 50,
          unit: 'M',
        }),
      ).toEqual('0050');
      expect(
        formatVisibilityToString({
          range: 500,
          unit: 'M',
        }),
      ).toEqual('0500');
      expect(
        formatVisibilityToString({
          range: 5000,
          unit: 'M',
        }),
      ).toEqual('5000');
      expect(
        formatVisibilityToString({
          range: 9999,
          unit: 'M',
        }),
      ).toEqual('9999');
    });
    it('should return CAVOK if passed true', () => {
      expect(formatVisibilityToString(true)).toEqual('CAVOK');
    });
    it('should return empty string if passed false', () => {
      expect(formatVisibilityToString(false)).toEqual('');
    });
    it('should return original value if passed something else', () => {
      expect(formatVisibilityToString(undefined)).toEqual(undefined);
    });
  });

  describe('parseVisibilityToObject', () => {
    it('should return {cavOK: true} if passed CAVOK', () => {
      expect(parseVisibilityToObject('CAVOK')).toEqual({ cavOK: true });
    });
    it('should return visibility object with range and unit if passed numbers', () => {
      expect(parseVisibilityToObject('0000')).toEqual({
        visibility: { range: 0, unit: 'M' },
      });
      expect(parseVisibilityToObject('0050')).toEqual({
        visibility: { range: 50, unit: 'M' },
      });
      expect(parseVisibilityToObject('0200')).toEqual({
        visibility: {
          range: 200,
          unit: 'M',
        },
      });
      expect(parseVisibilityToObject('2000')).toEqual({
        visibility: { range: 2000, unit: 'M' },
      });
      expect(parseVisibilityToObject('9999')).toEqual({
        visibility: { range: 9999, unit: 'M' },
      });
      expect(parseVisibilityToObject('9999 ')).toEqual({
        visibility: { range: 9999, unit: 'M' },
      });
    });
  });

  describe('validateVisibilityField', () => {
    it('should return true for empty string', () => {
      expect(validateVisibilityField('')).toEqual(true);
    });
    it('should return true for zero visibility', () => {
      expect(validateVisibilityField('0000')).toEqual(true);
    });
    it('should only allow numbers and chars in string - any trailing/leading spaces should be trimmed and allowed', () => {
      expect(validateVisibilityField(' vfoc7.k!')).toEqual(
        invalidVisibilityMessage,
      );
      expect(validateVisibilityField('200 0')).toEqual(
        invalidVisibilityMessage,
      );
      expect(validateVisibilityField('2000 ')).toEqual(true);
    });
    it('should return error when visibility is less than 4 chars or greater than 5 chars and has other chars than CAVOK', () => {
      expect(validateVisibilityField('2')).toEqual(invalidVisibilityMessage);
      expect(validateVisibilityField('212')).toEqual(invalidVisibilityMessage);
      expect(validateVisibilityField('952256')).toEqual(
        invalidVisibilityMessage,
      );
      expect(validateVisibilityField('1234567891234')).toEqual(
        invalidVisibilityMessage,
      );
      expect(validateVisibilityField('CAVUK')).toEqual(
        invalidVisibilityMessage,
      );
      expect(validateVisibilityField('090B')).toEqual(invalidVisibilityMessage);
      expect(validateVisibilityField('CAVOK')).toEqual(true);
      expect(validateVisibilityField('0500')).toEqual(true);
    });
    it('should only accept CAVOK as string', () => {
      expect(validateVisibilityField('CVAKO')).toEqual(
        invalidVisibilityMessage,
      );
      expect(validateVisibilityField('CAVO')).toEqual(invalidVisibilityMessage);
      expect(validateVisibilityField('CAVAK')).toEqual(
        invalidVisibilityMessage,
      );
      expect(validateVisibilityField('CAVOK')).toEqual(true);
    });

    // Range
    it('should only accept a 4 digit range', () => {
      expect(validateVisibilityField('00000')).toEqual(
        invalidVisibilityRangeMessage,
      );
      expect(validateVisibilityField('15000')).toEqual(
        invalidVisibilityRangeMessage,
      );
      expect(validateVisibilityField('10000')).toEqual(
        invalidVisibilityRangeMessage,
      );
      expect(validateVisibilityField('4500')).toEqual(true);
    });
    it('should check for the correct rounding between 0000 and 0800 (rounded to 50)', () => {
      expect(validateVisibilityField('0105')).toEqual(
        invalidVisibilityRange50mStepMessage,
      );
      expect(validateVisibilityField('0478')).toEqual(
        invalidVisibilityRange50mStepMessage,
      );
      expect(validateVisibilityField('0050')).toEqual(true);
      expect(validateVisibilityField('0650')).toEqual(true);
      expect(validateVisibilityField('0700')).toEqual(true);
    });
    it('should check for the correct rounding between 0800 and 5000 (rounded to 100)', () => {
      expect(validateVisibilityField('0905')).toEqual(
        invalidVisibilityRange100mStepMessage,
      );
      expect(validateVisibilityField('1050')).toEqual(
        invalidVisibilityRange100mStepMessage,
      );
      expect(validateVisibilityField('0800')).toEqual(true);
      expect(validateVisibilityField('1200')).toEqual(true);
      expect(validateVisibilityField('4500')).toEqual(true);
    });
    it('should check for the correct rounding between 5000 and 9000 (rounded to 1000) but allow 9999', () => {
      expect(validateVisibilityField('5500')).toEqual(
        invalidVisibilityRange1000mStepMessage,
      );
      expect(validateVisibilityField('7050')).toEqual(
        invalidVisibilityRange1000mStepMessage,
      );
      expect(validateVisibilityField('9100')).toEqual(
        invalidVisibilityRange1000mStepMessage,
      );
      expect(validateVisibilityField('9998')).toEqual(
        invalidVisibilityRange1000mStepMessage,
      );
      expect(validateVisibilityField('5000')).toEqual(true);
      expect(validateVisibilityField('9000')).toEqual(true);
      expect(validateVisibilityField('9999')).toEqual(true);
    });
  });
});
