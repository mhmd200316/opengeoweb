/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  invalidWeatherBRVisibility,
  invalidWeatherCAVOKMessage,
  invalidWeatherCodeMessage,
  invalidWeatherFGVisibility,
  invalidWeatherMessage,
  invalidWeatherNonEmptyNSWGroups,
  invalidWeatherNonUniqueValue,
  invalidWeatherNSWMessage,
  invalidWeatherVisibilityBelow5000,
  invalidWeatherWithPreviousEmptyWeather,
  parseWeatherToObject,
  validateSecondWeatherField,
  validateThirdWeatherField,
  validateWeatherField,
} from './weather';

describe('TafForm/TafFormRow/validations/weather', () => {
  describe('validateWeatherField', () => {
    it('should accept empty strings', () => {
      expect(validateWeatherField('', '')).toEqual(true);
      expect(validateWeatherField(' ', ' ')).toEqual(true);
    });
    it('should accept NSW if passed true or nothing', () => {
      expect(validateWeatherField('NSW', '')).toEqual(true);
      expect(validateWeatherField(' NSW', '')).toEqual(true);
      expect(validateWeatherField('NSW', '', true)).toEqual(true);
      expect(validateWeatherField('NSW', '', false)).toEqual(
        invalidWeatherNSWMessage,
      );
      expect(validateWeatherField(' NSW', '', false)).toEqual(
        invalidWeatherNSWMessage,
      );
    });

    it('should accept WeatherPhenomena', () => {
      expect(validateWeatherField('-TSSNRA', '')).toEqual(true);
      expect(validateWeatherField('-RADZPL', '')).toEqual(true);
      expect(validateWeatherField('+SNDZ', '')).toEqual(true);
      expect(validateWeatherField('SHRAGR', '')).toEqual(true);
      expect(validateWeatherField('TSSNGSRA', '')).toEqual(true);
      expect(validateWeatherField(' SHRAGR', '')).toEqual(true);
      expect(validateWeatherField('TSSNGS ', '')).toEqual(true);
    });

    it('should not accept anything else (if NSW not allowed should only mention weather code in error message)', () => {
      expect(validateWeatherField('5', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );
      expect(validateWeatherField('-RZPL', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );
      expect(validateWeatherField('FAKE INPUT', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );

      expect(validateWeatherField('SHRAGRSHRAGR', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );
      expect(validateWeatherField('TNGSRA', '', false)).toEqual(
        invalidWeatherCodeMessage,
      );
    });

    it('should not accept anything else (if NSW allowed, should mention in error message)', () => {
      expect(validateWeatherField('5', '')).toEqual(invalidWeatherMessage);
      expect(validateWeatherField('-RZPL', '')).toEqual(invalidWeatherMessage);
      expect(validateWeatherField('FAKE INPUT', '')).toEqual(
        invalidWeatherMessage,
      );
      expect(validateWeatherField('FAKE INPUT', '', true)).toEqual(
        invalidWeatherMessage,
      );
      expect(validateWeatherField('SHRAGRSHRAGR', '')).toEqual(
        invalidWeatherMessage,
      );
      expect(validateWeatherField('TNGSRA', '')).toEqual(invalidWeatherMessage);
      expect(validateWeatherField('TNGSRA', '', true)).toEqual(
        invalidWeatherMessage,
      );
    });

    it('should not accept anything if visibility is CAVOK', () => {
      expect(validateWeatherField('RA', 'CAVOK')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateWeatherField('NSW', 'CAVOK')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateWeatherField('', 'CAVOK')).toEqual(true);
      expect(validateWeatherField('RA', '')).toEqual(true);
      expect(validateWeatherField('RA', '1000')).toEqual(true);
    });

    it('should validate the combination of weather code and visibility range', () => {
      expect(validateWeatherField('FU', '5000')).toEqual(true);
      expect(validateWeatherField('DU', '4000')).toEqual(true);
      expect(validateWeatherField('FU', '6000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('DU', '6000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('HZ', '7000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('DRDU', '8000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('BLDU', '6000')).toEqual(
        invalidWeatherVisibilityBelow5000,
      );
      expect(validateWeatherField('BR', '1000')).toEqual(true);
      expect(validateWeatherField('BR', '5000')).toEqual(true);
      expect(validateWeatherField('BR', '3000')).toEqual(true);
      expect(validateWeatherField('BR', '900')).toEqual(
        invalidWeatherBRVisibility,
      );
      expect(validateWeatherField('BR', '6000')).toEqual(
        invalidWeatherBRVisibility,
      );
      expect(validateWeatherField('FG', '900')).toEqual(true);
      expect(validateWeatherField('FG', '1000')).toEqual(
        invalidWeatherFGVisibility,
      );
      expect(validateWeatherField('RA', '6000')).toEqual(true);
      expect(validateWeatherField('FU', '')).toEqual(true);
      expect(validateWeatherField('FU', '0000')).toEqual(true);
    });
  });

  describe('validateSecondWeatherField', () => {
    it('should return error for second weather field when first weather field empty', () => {
      expect(validateSecondWeatherField('TSSNRA', '', '')).toEqual(true);
      expect(validateSecondWeatherField('', '', '')).toEqual(true);
      expect(validateSecondWeatherField('', '  ', '')).toEqual(true);
      expect(validateSecondWeatherField('TSSNRA', 'RADZPL', '')).toEqual(true);
      expect(validateSecondWeatherField('TSSNRA ', 'TSSNGS ', '')).toEqual(
        true,
      );
      expect(validateSecondWeatherField('', 'RA', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
      expect(validateSecondWeatherField(' ', 'RA ', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
    });
    it('should return error for second weather field if NSW passed', () => {
      expect(validateSecondWeatherField('TSSNRA', 'NSW', '')).toEqual(
        invalidWeatherNSWMessage,
      );
    });

    it('should return error for second weather field when first weather field contains same', () => {
      expect(validateSecondWeatherField('TSSNRA', 'RADZPL', '')).toEqual(true);
      expect(validateSecondWeatherField('TSSNRA ', 'TSSNGS ', '')).toEqual(
        true,
      );
      expect(validateSecondWeatherField('TSSNRA', 'TSSNRA', '')).toEqual(
        invalidWeatherNonUniqueValue,
      );
      expect(validateSecondWeatherField('RA ', ' RA ', '')).toEqual(
        invalidWeatherNonUniqueValue,
      );
    });
    it('should return error for second weather field when first weather field contains NSW', () => {
      expect(validateSecondWeatherField('NSW', '', '')).toEqual(true);
      expect(validateSecondWeatherField('NSW ', ' ', '')).toEqual(true);
      expect(validateSecondWeatherField('NSW', 'TSSNRA', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
      expect(validateSecondWeatherField('NSW ', ' RA ', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
    });
    it('should not accept anything if visibility is CAVOK', () => {
      expect(validateSecondWeatherField('RA', 'SN', 'CAVOK')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateSecondWeatherField('', '', 'CAVOK')).toEqual(true);
      expect(validateSecondWeatherField('RA', 'SN', '')).toEqual(true);
      expect(validateSecondWeatherField('RA', 'SN', '1000')).toEqual(true);
    });
  });

  describe('validateThirdWeatherField', () => {
    it('should return error for third weather field when first or second weather field empty', () => {
      expect(
        validateThirdWeatherField('TSSNRA', 'RADZPL', 'TSSNGSRA', ''),
      ).toEqual(true);
      expect(
        validateThirdWeatherField('TSSNRA ', 'TSSNGS ', 'TSSNGSRA  ', ''),
      ).toEqual(true);
      expect(validateThirdWeatherField('', 'TSSNGS', 'RA', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
      expect(validateThirdWeatherField('TSSNGSRA', ' ', 'RA ', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
      expect(validateThirdWeatherField('', ' ', 'RA ', '')).toEqual(
        invalidWeatherWithPreviousEmptyWeather,
      );
    });
    it('should return error for thrid weather field when first/second weather field contains same', () => {
      expect(
        validateThirdWeatherField('TSSNRA', 'RADZPL', 'SHRAGR', ''),
      ).toEqual(true);
      expect(
        validateThirdWeatherField('TSSNRA ', 'TSSNGS ', 'SHRAGR   ', ''),
      ).toEqual(true);
      expect(
        validateThirdWeatherField('TSSNRA', 'TSSNRA', 'TSSNRA', ''),
      ).toEqual(invalidWeatherNonUniqueValue);
      expect(validateThirdWeatherField('RA ', ' RA ', '    RA', '')).toEqual(
        invalidWeatherNonUniqueValue,
      );
    });
    it('should return error for third weather field if NSW passed', () => {
      expect(validateThirdWeatherField('TSSNRA', 'RADZPL', 'NSW', '')).toEqual(
        invalidWeatherNSWMessage,
      );
    });
    it('should return error for third weather field when first weather field contains NSW', () => {
      expect(validateThirdWeatherField('NSW', '', '', '')).toEqual(true);
      expect(validateThirdWeatherField('NSW ', ' ', '   ', '')).toEqual(true);
      expect(validateThirdWeatherField('NSW', 'RADZPL', 'TSSNRA', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
      expect(validateThirdWeatherField('NSW ', '', ' RA ', '')).toEqual(
        invalidWeatherNonEmptyNSWGroups,
      );
    });
    it('should not accept anything if visibility is CAVOK', () => {
      expect(validateThirdWeatherField('RA', 'SN', 'DZ', 'CAVOK')).toEqual(
        invalidWeatherCAVOKMessage,
      );
      expect(validateThirdWeatherField('', '', '', 'CAVOK')).toEqual(true);
      expect(validateThirdWeatherField('RA', 'SN', 'DZ', '')).toEqual(true);
      expect(validateThirdWeatherField('RA', 'SN', 'DZ', '1000')).toEqual(true);
    });
  });

  describe('parseWeatherToObject', () => {
    it('should trim the weather strings - nothing more', () => {
      expect(parseWeatherToObject({ weather1: 'NSW' })).toEqual({
        weather1: 'NSW',
      });
      expect(parseWeatherToObject({ weather1: 'NSW    ' })).toEqual({
        weather1: 'NSW',
      });
      expect(
        parseWeatherToObject({
          weather1: 'MIFG    ',
          weather2: '',
          weather3: '',
        }),
      ).toEqual({
        weather1: 'MIFG',
      });
      expect(
        parseWeatherToObject({
          weather1: '  MIFG',
          weather2: '+SHRA  ',
          weather3: '',
        }),
      ).toEqual({
        weather1: 'MIFG',
        weather2: '+SHRA',
      });
      expect(
        parseWeatherToObject({
          weather1: '-RADZPL',
          weather2: '+SHRA',
          weather3: 'TSSNGSRA ',
        }),
      ).toEqual({
        weather1: '-RADZPL',
        weather2: '+SHRA',
        weather3: 'TSSNGSRA',
      });
    });
  });
});
