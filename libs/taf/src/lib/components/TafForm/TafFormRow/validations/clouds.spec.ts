/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  invalidCloudMessage,
  invalidCLoudTypeMessage,
  invalidCloudHeightMessage,
  invalidCloudHeightStepsMessage,
  invalidCloudCoverageMessage,
  validateFirstCloud,
  invalidHeightTypeCombinationMessage,
  invalidVerticalVisibilityMessage,
  validateSecondCloud,
  invalidCloudHeightComparedToPreviousMessage,
  invalidCloud2CoverageTypeCombinationMessage,
  invalidCloud2WithVerticalVisibilityMessage,
  validateThirdCloud,
  invalidCloud3CoverageTypeCombinationMessage,
  invalidCloudWithVerticalVisibilityMessage,
  invalidCloudWithPreviousEmptyCloud,
  invalidCloudWithNSCMessage,
  invalidCloud1Message,
  parseCloudsToObject,
  formatCloudsToString,
  validateFourthCloud,
  invalidCloudWithPreviousType,
  invalidCloudCAVOKMessage,
  invalidVerticalVisibilityHeightMessage,
  invalidVerticalVisibilityHeightFGMessage,
  invalidCloud4TypeMissingMessage,
} from './clouds';

describe('TafForm/TafFormRow/validations/clouds', () => {
  describe('parseCloudsToObject', () => {
    it('should parse cloud strings to a cloud object', () => {
      expect(
        parseCloudsToObject({
          cloud1: 'FEW001',
          cloud2: 'SCT002',
          cloud3: 'BKN003',
          cloud4: 'OVC050TCU',
        }),
      ).toEqual({
        cloud1: {
          coverage: 'FEW',
          height: 1,
        },
        cloud2: {
          coverage: 'SCT',
          height: 2,
        },
        cloud3: {
          coverage: 'BKN',
          height: 3,
        },
        cloud4: {
          coverage: 'OVC',
          height: 50,
          type: 'TCU',
        },
      });
      expect(
        parseCloudsToObject({ cloud1: 'SCT001CB', cloud2: '   ', cloud3: '' }),
      ).toEqual({
        cloud1: {
          coverage: 'SCT',
          height: 1,
          type: 'CB',
        },
      });
      expect(parseCloudsToObject({ cloud1: 'NSC' })).toEqual({
        cloud1: 'NSC',
      });
      expect(
        parseCloudsToObject({ cloud1: 'VV005', cloud2: 'OVC050TCU' }),
      ).toEqual({
        cloud1: { verticalVisibility: 5 },
        cloud2: { coverage: 'OVC', height: 50, type: 'TCU' },
      });
    });
    it('should trim all values', () => {
      expect(
        parseCloudsToObject({
          cloud1: 'FEW001 ',
          cloud2: ' SCT002',
          cloud3: 'BKN003 ',
          cloud4: 'OVC050TCU  ',
        }),
      ).toEqual({
        cloud1: {
          coverage: 'FEW',
          height: 1,
        },
        cloud2: {
          coverage: 'SCT',
          height: 2,
        },
        cloud3: {
          coverage: 'BKN',
          height: 3,
        },
        cloud4: {
          coverage: 'OVC',
          height: 50,
          type: 'TCU',
        },
      });
      expect(
        parseCloudsToObject({
          cloud1: 'SCT001CB ',
          cloud2: '   ',
          cloud3: ' ',
        }),
      ).toEqual({
        cloud1: {
          coverage: 'SCT',
          height: 1,
          type: 'CB',
        },
      });
      expect(parseCloudsToObject({ cloud1: 'NSC ' })).toEqual({
        cloud1: 'NSC',
      });
      expect(
        parseCloudsToObject({ cloud1: 'VV005  ', cloud2: ' OVC050TCU' }),
      ).toEqual({
        cloud1: { verticalVisibility: 5 },
        cloud2: { coverage: 'OVC', height: 50, type: 'TCU' },
      });
    });
  });
  describe('formatCloudToString', () => {
    it('should format cloud object to cloud strings', () => {
      expect(
        formatCloudsToString({
          cloud1: {
            coverage: 'SCT',
            height: 1,
            type: 'CB',
          },
        }),
      ).toEqual({ cloud1: 'SCT001CB' });
      expect(
        formatCloudsToString({
          cloud1: {
            coverage: 'FEW',
            height: 1,
          },
          cloud2: {
            coverage: 'SCT',
            height: 2,
          },
          cloud3: {
            coverage: 'BKN',
            height: 3,
          },
          cloud4: {
            coverage: 'OVC',
            height: 50,
            type: 'TCU',
          },
        }),
      ).toEqual({
        cloud1: 'FEW001',
        cloud2: 'SCT002',
        cloud3: 'BKN003',
        cloud4: 'OVC050TCU',
      });
      expect(formatCloudsToString({ cloud1: 'NSC' })).toEqual({
        cloud1: 'NSC',
      });
      expect(
        formatCloudsToString({
          cloud1: { verticalVisibility: 5 },
          cloud2: { coverage: 'OVC', height: 50, type: 'TCU' },
        }),
      ).toEqual({ cloud1: 'VV005', cloud2: 'OVC050TCU' });
      expect(
        formatCloudsToString({
          cloud1: { verticalVisibility: 0 },
        }),
      ).toEqual({ cloud1: 'VV000' });
    });
  });
  describe('validateFirstCloud', () => {
    it('should accept empty strings or NSC', () => {
      expect(validateFirstCloud('', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('  ', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('NSC', '', '', '', '')).toEqual(true);
    });
    it('should not accept less than 6 or more than 9 characters', () => {
      expect(validateFirstCloud('12345', '', '', '', '')).toEqual(
        invalidCloud1Message,
      );
      expect(validateFirstCloud('1234567890', '', '', '', '')).toEqual(
        invalidCloud1Message,
      );
    });
    it('should not accept invalid characters', () => {
      expect(validateFirstCloud('A,.DFG', '', '', '', '')).toEqual(
        invalidCloud1Message,
      );
    });
    it('should only accept FEW SCT BKN OVC as coverage', () => {
      expect(validateFirstCloud('FEW001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud(' FEW001 ', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('SCT001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('BKN001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('OVC001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('TSC001', '', '', '', '')).toEqual(
        invalidCloudCoverageMessage,
      );
      expect(validateFirstCloud('NSC001', '', '', '', '')).toEqual(
        invalidCloudCoverageMessage,
      );
    });
    it('should check for the correct cloud height', () => {
      expect(validateFirstCloud('FEW000', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW01F', '', '', '', '')).toEqual(
        invalidCloudHeightMessage,
      );
      expect(validateFirstCloud('FEW049', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW050CB', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW800CB', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW990TCU', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW051', '', '', '', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      expect(validateFirstCloud('FEW999CB', '', '', '', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
    });
    it('should accept only TCU, CB or an empty string as type', () => {
      expect(validateFirstCloud('FEW001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW050TCU', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW050CB', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW001CBU', '', '', '', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      expect(validateFirstCloud('FEW001TC', '', '', '', '')).toEqual(
        invalidCLoudTypeMessage,
      );
    });
    it('should check for the height and type combination', () => {
      expect(validateFirstCloud('FEW001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW049TCU', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW050CB', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW990TCU', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW050', '', '', '', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
    });
    it('should validate on vertical visibility', () => {
      // Only accept vertical visibilities of format VVXXX with XXX between 001 and 010
      // if one of the weather fields contains FG XXX should lie between 001 and 005
      expect(validateFirstCloud('VV001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('VV010', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('VV010', '', 'RA', 'DZ', '')).toEqual(true);
      expect(
        validateFirstCloud('VV007', '', 'RAPLSN', 'VCFG', 'SGRASN'),
      ).toEqual(true);

      expect(validateFirstCloud('VV', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('VV01', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('VVA01', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('VV0100', '', '', '', '')).toEqual(
        invalidVerticalVisibilityMessage,
      );
      expect(validateFirstCloud('VV011', '', '', '', '')).toEqual(
        invalidVerticalVisibilityHeightMessage,
      );
      expect(validateFirstCloud('VV000', '', '', '', '')).toEqual(
        invalidVerticalVisibilityHeightMessage,
      );
      expect(validateFirstCloud('VV011', '', 'FG', '', '')).toEqual(
        invalidVerticalVisibilityHeightFGMessage,
      );
      expect(validateFirstCloud('VV007', '', 'RA', 'FG', '')).toEqual(
        invalidVerticalVisibilityHeightFGMessage,
      );
      expect(validateFirstCloud('VV000', '', '', '', 'FG')).toEqual(
        invalidVerticalVisibilityHeightFGMessage,
      );
    });
    it('should not accept anything if visibility is CAVOK', () => {
      expect(validateFirstCloud('FEW001', 'CAVOK', '', '', '')).toEqual(
        invalidCloudCAVOKMessage,
      );
      expect(validateFirstCloud('', 'CAVOK', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW001', '', '', '', '')).toEqual(true);
      expect(validateFirstCloud('FEW001', '1000', '', '', '')).toEqual(true);
    });
  });
  describe('validateSecondCloud', () => {
    it('should accept empty strings', () => {
      expect(validateSecondCloud('', '', '')).toEqual(true);
      expect(validateSecondCloud('', '  ', '')).toEqual(true);
      expect(validateSecondCloud(' ', '  ', '')).toEqual(true);
    });
    it('should not accept NSC or VV', () => {
      expect(validateSecondCloud('FEW000', 'NSC', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateSecondCloud('FEW000', 'VV001', '')).toEqual(
        invalidCloudMessage,
      );
    });

    it('should check that cloud2 is empty when cloud1 is NSC', () => {
      expect(validateSecondCloud('NSC', '', '')).toEqual(true);
      expect(validateSecondCloud('NSC', 'SCT000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
      expect(validateSecondCloud('NSC', 'FEW000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
      expect(validateSecondCloud('NSC ', 'FEW000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
    });

    it('should check that cloud1 is not empty', () => {
      expect(validateSecondCloud('', 'SCT000', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateSecondCloud(' ', 'SCT000', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateSecondCloud('', 'FEW002', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
    });

    it('should not accept less than 6 or more than 9 characters', () => {
      expect(validateSecondCloud('FEW000', '12345', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateSecondCloud('FEW000', '1234567890', '')).toEqual(
        invalidCloudMessage,
      );
    });
    it('should not accept invalid characters', () => {
      expect(validateSecondCloud('FEW000', 'A,.DFG', '')).toEqual(
        invalidCloudMessage,
      );
    });
    it('should only accept FEW SCT BKN OVC as coverage', () => {
      expect(validateSecondCloud('FEW000', 'FEW050CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', ' SCT002 ', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'BKN002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'OVC002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'TSC002', '')).toEqual(
        invalidCloudCoverageMessage,
      );
    });
    it('should check for the correct cloud height', () => {
      expect(validateSecondCloud('FEW000', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT02F', '')).toEqual(
        invalidCloudHeightMessage,
      );
      expect(validateSecondCloud('FEW000', 'SCT049', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT050CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT800CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT990TCU', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT051', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      expect(validateSecondCloud('FEW000', 'SCT999CB', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
    });
    it('should accept only TCU, CB or an empty string as type', () => {
      expect(validateSecondCloud('FEW000', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT050TCU', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT300CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT060CBU', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      expect(validateSecondCloud('FEW000', 'SCT070TC', '')).toEqual(
        invalidCLoudTypeMessage,
      );
    });
    it('should check for the height and type combination', () => {
      expect(validateSecondCloud('FEW000', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT049TCU', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT050CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT990TCU', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT060', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
      expect(validateSecondCloud('FEW000', 'FEW090', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
    });
    it('should check for the type and coverage combination', () => {
      expect(validateSecondCloud('FEW000', 'FEW050CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'FEW050TCU', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'BKN002', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'OVC002', '')).toEqual(true);
      // In case the first cloud group contains CB or TCU it should accept FEW without a type
      expect(validateSecondCloud('FEW000CB', 'FEW002', '')).toEqual(true);
      expect(validateSecondCloud('SCT010TCU', 'FEW030', '')).toEqual(true);

      expect(validateSecondCloud('FEW000', 'FEW002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );
      expect(validateSecondCloud('SCT000', 'FEW002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );
    });

    it('should not compare cloud2 when cloud1 is invalid', () => {
      expect(validateSecondCloud('SCT0010', 'SCT000', '')).toEqual(true);
      expect(validateSecondCloud('VV', 'SCT000', '')).toEqual(true);
      expect(validateSecondCloud('FEW001', 'SCT002', 'CAVOK')).toEqual(true);
    });
    it('should check that cloud2 has no type when cloud1 cloudgroups contains type', () => {
      expect(validateSecondCloud('FEW000', 'SCT001', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'SCT001CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000', 'FEW001CB', '')).toEqual(true);
      expect(validateSecondCloud('FEW000TCU', 'FEW001', '')).toEqual(true);
      expect(validateSecondCloud('FEW000TCU', 'SCT001CB', '')).toEqual(
        invalidCloudWithPreviousType,
      );
      expect(validateSecondCloud('FEW000CB', 'FEW001CB', '')).toEqual(
        invalidCloudWithPreviousType,
      );
    });
    it('should compare the height of cloud 2 with cloud 1', () => {
      expect(validateSecondCloud('FEW001', 'SCT002', '')).toEqual(true);
      expect(validateSecondCloud('FEW001', 'SCT001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
      expect(validateSecondCloud('FEW003CB', 'FEW001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
      expect(validateSecondCloud('FEW002', 'SCT001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
    });
    it('should check that type is present when vertical visibility is given', () => {
      expect(validateSecondCloud('VV001', 'FEW050CB', '')).toEqual(true);
      expect(validateSecondCloud('VV001', '', '')).toEqual(true);
      expect(validateSecondCloud('VV001', 'SCT002', '')).toEqual(
        invalidCloud2WithVerticalVisibilityMessage,
      );
    });
  });

  describe('validateThirdCloud', () => {
    it('should accept empty strings', () => {
      expect(validateThirdCloud('', '', '', '')).toEqual(true);
      expect(validateThirdCloud('', '', '  ', '')).toEqual(true);
    });
    it('should not accept NSC or VV', () => {
      expect(validateThirdCloud('FEW000', 'SCT001', 'NSC', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'VV001', '')).toEqual(
        invalidCloudMessage,
      );
    });

    it('should not accept less than 6 or more than 9 characters', () => {
      expect(validateThirdCloud('FEW000', 'SCT001', '12345', '')).toEqual(
        invalidCloudMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', '1234567890', '')).toEqual(
        invalidCloudMessage,
      );
    });
    it('should not accept invalid characters', () => {
      expect(validateThirdCloud('FEW000', 'SCT001', 'A,.DFG', '')).toEqual(
        invalidCloudMessage,
      );
    });
    it('should only accept FEW SCT BKN OVC as coverage', () => {
      expect(validateThirdCloud('FEW001', 'SCT010', 'FEW050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW001', 'SCT010', ' SCT060TCU ', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW001', 'SCT010', 'SCT050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW001', 'SCT010', 'BKN012', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW001', 'SCT010', 'OVC012', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'TSC012', '')).toEqual(
        invalidCloudCoverageMessage,
      );
    });
    it('should check for the correct cloud height', () => {
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN02F', '')).toEqual(
        invalidCloudHeightMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN049', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN800CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN990TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN051', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN999CB', '')).toEqual(
        invalidCloudHeightStepsMessage,
      );
    });
    it('should accept only TCU, CB or an empty string as type', () => {
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN050TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN300CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN060CBU', '')).toEqual(
        invalidCLoudTypeMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN070TC', '')).toEqual(
        invalidCLoudTypeMessage,
      );
    });
    it('should check for the height and type combination', () => {
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN049', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN049CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN990TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN050', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN100', '')).toEqual(
        invalidHeightTypeCombinationMessage,
      );
    });
    it('should check for the type and coverage combination', () => {
      expect(validateThirdCloud('FEW000', 'SCT001', 'FEW050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'FEW050TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'SCT050CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'SCT050TCU', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'OVC002', '')).toEqual(
        true,
      );
      // In case the first or second cloud group contains CB or TCU
      // this cloud group should be evaluated as the second cloud and accept SCT without a type but NOT FEW without type
      expect(validateThirdCloud('FEW000CB', 'FEW002', 'SCT003', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW010', 'FEW030TCU', 'SCT040', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'SCT002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'FEW002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('FEW000CB', 'FEW001', 'FEW002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('FEW000', 'FEW001CB', 'FEW002', '')).toEqual(
        invalidCloud2CoverageTypeCombinationMessage,
      );

      expect(validateThirdCloud('FEW000', 'SCT001', 'FEW002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'SCT002', '')).toEqual(
        invalidCloud3CoverageTypeCombinationMessage,
      );
    });
    it('should not further validate cloud3 when cloud 1 or 2 is invalid', () => {
      // cloud 1 + 3 invalid
      expect(validateThirdCloud('VV', 'SCT001', 'BKN001', '')).toEqual(true);
      // cloud 2 + 3 invalid
      expect(validateThirdCloud('FEW001', 'SCT0010', 'BKN000', '')).toEqual(
        true,
      );
      // cloud 1 + 2 + 3 invalid
      expect(validateThirdCloud('FEW001', 'SCT002', 'BKN001', 'CAVOK')).toEqual(
        true,
      );
    });
    it('should check that cloud3 is empty when cloud1 and/or cloud2 is empty', () => {
      expect(validateThirdCloud('FEW001', '', '', '')).toEqual(true);
      expect(validateThirdCloud('FEW001', '', 'BKN060CB', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateThirdCloud('', '', 'BKN060CB', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
    });
    it('should check that cloud3 is empty when cloud1 is NSC', () => {
      expect(validateThirdCloud('NSC', '', '', '')).toEqual(true);
      expect(validateThirdCloud('NSC', '', 'BKN000', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
    });
    it('should check that cloud3 has no type when one of the previous cloudgroups contains type', () => {
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN040', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN030CB', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'FEW001CB', 'SCT002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000CB', 'FEW000', 'SCT002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001CB', 'BKN050TCU', '')).toEqual(
        invalidCloudWithPreviousType,
      );
      expect(validateThirdCloud('FEW000', 'SCT001CB', 'BKN040CB', '')).toEqual(
        invalidCloudWithPreviousType,
      );
      expect(
        validateThirdCloud('FEW000TCU', 'SCT001', 'BKN050TCU', ''),
      ).toEqual(invalidCloudWithPreviousType);
    });
    it('should compare the height of cloud 3 with cloud 2', () => {
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN002', '')).toEqual(
        true,
      );
      expect(validateThirdCloud('FEW000', 'SCT001', 'BKN001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
      expect(validateThirdCloud('FEW000', 'SCT002', 'BKN001', '')).toEqual(
        invalidCloudHeightComparedToPreviousMessage,
      );
    });
    it('should check that cloud3 is empty when vertical visibility is given', () => {
      expect(validateThirdCloud('VV001', '', '', '')).toEqual(true);
      expect(validateThirdCloud('VV010', 'SCT001CB', '', '')).toEqual(true);
      expect(validateThirdCloud('VV001', 'SCT050CB', 'BKN060CB', '')).toEqual(
        invalidCloudWithVerticalVisibilityMessage,
      );
    });
  });

  describe('validateFourthCloud', () => {
    it('should accept empty strings', () => {
      expect(validateFourthCloud('', '', '', '', '')).toEqual(true);
      expect(validateFourthCloud('', '', '', '  ', '')).toEqual(true);
    });
    it('should not accept NSC or VV', () => {
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'NSC', ''),
      ).toEqual(invalidCloudMessage);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'VV001', ''),
      ).toEqual(invalidCloudMessage);
    });

    it('should not accept less than 6 or more than 9 characters', () => {
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', '12345', ''),
      ).toEqual(invalidCloudMessage);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', '1234567890', ''),
      ).toEqual(invalidCloudMessage);
    });
    it('should not accept invalid characters', () => {
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'A,.DFG', ''),
      ).toEqual(invalidCloudMessage);
    });
    it('should only accept FEW SCT BKN OVC as coverage', () => {
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'FEW050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', ' SCT060TCU ', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'SCT050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'BKN200CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'OVC100TCU', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT010', 'BKN020', 'TSC120CB', ''),
      ).toEqual(invalidCloudCoverageMessage);
    });
    it('should check for the correct cloud height', () => {
      expect(
        validateFourthCloud('FEW005', 'SCT001', 'BKN002', 'BKN100CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW005', 'SCT001', 'BKN002', 'BKN02F', ''),
      ).toEqual(invalidCloudHeightMessage);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN051CB', ''),
      ).toEqual(invalidCloudHeightStepsMessage);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN999CB', ''),
      ).toEqual(invalidCloudHeightStepsMessage);
    });
    it('should check that correct type is given', () => {
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN050TCU', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN300CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN070TC', ''),
      ).toEqual(invalidCLoudTypeMessage);
    });
    it('should check for the height and type combination', () => {
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN050', ''),
      ).toEqual(invalidHeightTypeCombinationMessage);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN990TCU', ''),
      ).toEqual(true);
    });
    it('should not compare cloud4 when cloud 1, 2 or 3 is invalid', () => {
      // cloud 1 and 4 invalid
      expect(
        validateFourthCloud('VV', 'SCT001', 'BKN060CB', 'BKN060CB', ''),
      ).toEqual(true);
      // cloud 3 and 4 invalid
      expect(
        validateFourthCloud('SCT010', 'SCT020', 'BKN060', 'BKN060CB', ''),
      ).toEqual(true);
      // all clouds invalid
      expect(
        validateFourthCloud('SCT010', 'SCT020', 'BKN060', 'BKN060CB', 'CAVOK'),
      ).toEqual(true);
    });
    it('should check that cloud4 is empty when cloud 1, 2 and/or 3 is empty', () => {
      expect(validateFourthCloud('FEW000', 'SCT001', '', '', '')).toEqual(true);
      expect(
        validateFourthCloud('FEW001', 'SCT002', '', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithPreviousEmptyCloud);
      expect(validateFourthCloud('FEW001', '', '', 'BKN060CB', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(validateFourthCloud('', '', '', 'BKN060CB', '')).toEqual(
        invalidCloudWithPreviousEmptyCloud,
      );
      expect(
        validateFourthCloud('', 'SCT001', 'OVC002', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithPreviousEmptyCloud);
      expect(
        validateFourthCloud('SCT001', '', 'OVC002', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithPreviousEmptyCloud);
    });
    it('should check that cloud4 is empty when cloud1 is NSC', () => {
      expect(validateFourthCloud('NSC', '', '', '', '')).toEqual(true);
      expect(validateFourthCloud('NSC', '', '', 'BKN050CB', '')).toEqual(
        invalidCloudWithNSCMessage,
      );
    });
    it('should check that cloud4 has no type when one of the previous cloudgroups contains type', () => {
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN050', 'OVC060CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN030CB', 'BKN040', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'FEW001CB', 'SCT002', 'BKN040', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000CB', 'FEW000', 'SCT002', 'BKN040', ''),
      ).toEqual(true);
      // There is a type in one of the previous groups so fourth group needs to be evaluated as third (no FEW and SCT allowed)
      expect(
        validateFourthCloud('FEW000CB', 'FEW000', 'SCT002', 'SCT040', ''),
      ).toEqual(invalidCloud3CoverageTypeCombinationMessage);
      expect(
        validateFourthCloud('FEW000', 'FEW000TCU', 'SCT002', 'FEW040', ''),
      ).toEqual(invalidCloud3CoverageTypeCombinationMessage);

      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN050TCU', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithPreviousType);
      expect(
        validateFourthCloud('FEW000', 'SCT001CB', 'BKN040', 'BKN060TCU', ''),
      ).toEqual(invalidCloudWithPreviousType);
      expect(
        validateFourthCloud('FEW000CB', 'SCT001', 'BKN040', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithPreviousType);
    });
    it('should check that cloud4 has a type when none of the previous cloudgroups contains a type', () => {
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN050', 'OVC060CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN002', 'BKN040TCU', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN040', 'OVC045', ''),
      ).toEqual(invalidCloud4TypeMissingMessage);
    });
    it('should compare the height of cloud 4 with cloud 3', () => {
      expect(
        validateFourthCloud('FEW000', 'SCT001', 'BKN049', 'BKN050CB', ''),
      ).toEqual(true);
      expect(
        validateFourthCloud('FEW001', 'SCT020', 'BKN030TCU', 'BKN030', ''),
      ).toEqual(invalidCloudHeightComparedToPreviousMessage);
    });
    it('should check that cloud4 is empty when vertical visibility is given', () => {
      expect(validateFourthCloud('VV001', '', '', '', '')).toEqual(true);
      expect(validateFourthCloud('VV010', 'SCT001CB', '', '', '')).toEqual(
        true,
      );
      expect(
        validateFourthCloud('VV001', 'SCT050CB', 'BKN065', 'BKN060CB', ''),
      ).toEqual(invalidCloudWithVerticalVisibilityMessage);
    });
  });
});
