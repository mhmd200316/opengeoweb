/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  formatValidToString,
  invalidValidityDaysHoursEndMessage,
  invalidValidityDaysHoursStartMessage,
  invalidValidityEndTimeMessage,
  invalidValidityMinutesMessage,
  invalidValidityPeriodMessage,
  invalidValidityStartTimeMessage,
  parseValidToObject,
  validateValidField,
  validateValidChangeGroupInterField,
  invalidChangeValidInterFieldMessage,
  invalidBECMGDurationValidInterFieldMessage,
  validatePreviousChangeGroup,
  invalidPreviousChangeGroupIsBeforeMessage,
  invalidPreviousChangeGroupOrderMessage,
  invalidPreviousChangeGroupHasSameValue,
  validateOverlapWithFM,
  invalidChangeGroupOverlapsWithFM,
} from './validField';

describe('TafForm/TafFormRow/validations/validField', () => {
  describe('formatValidToString', () => {
    it('should format valid values into string if both start and end are passed', () => {
      expect(
        formatValidToString({
          start: '2020-12-07T06:00:00Z',
          end: '2020-12-08T12:00:00Z',
        }),
      ).toEqual('0706/0812');
      expect(
        formatValidToString({
          start: '2020-12-07T00:00:00Z',
          end: '2020-12-08T06:00:00Z',
        }),
      ).toEqual('0700/0806');
      expect(
        formatValidToString({
          start: '2020-12-07T18:00:00Z',
          end: '2020-12-09T00:00:00Z',
        }),
      ).toEqual('0718/0824');
      expect(
        formatValidToString({
          start: '2020-12-31T18:00:00Z',
          end: '2021-01-02T00:00:00Z',
        }),
      ).toEqual('3118/0124');
    });
    it('should format valid values into string if only start passed', () => {
      expect(
        formatValidToString({
          start: '2020-12-07T06:00:00Z',
        }),
      ).toEqual('070600');
      expect(
        formatValidToString({
          start: '2020-12-07T00:00:00Z',
        }),
      ).toEqual('070000');
    });
  });

  describe('parseValidToObject', () => {
    it('should format valid string into object with start and end if both are passed', () => {
      expect(
        parseValidToObject(
          '0706/0812',
          '2020-12-07T06:00:00Z',
          '2020-12-08T12:00:00Z',
        ),
      ).toEqual({
        start: '2020-12-07T06:00:00Z',
        end: '2020-12-08T12:00:00Z',
      });
      expect(
        parseValidToObject(
          '0700/0806',
          '2020-12-07T00:00:00Z',
          '2020-12-08T06:00:00Z',
        ),
      ).toEqual({
        start: '2020-12-07T00:00:00Z',
        end: '2020-12-08T06:00:00Z',
      });
      expect(
        parseValidToObject(
          '0718/0824',
          '2020-12-07T06:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual({
        start: '2020-12-07T18:00:00Z',
        end: '2020-12-09T00:00:00Z',
      });
      expect(
        parseValidToObject(
          '3118/0124',
          '2020-12-31T06:00:00Z',
          '2021-01-02T00:00:00Z',
        ),
      ).toEqual({
        start: '2020-12-31T18:00:00Z',
        end: '2021-01-02T00:00:00Z',
      });
      expect(
        parseValidToObject(
          '3120/0102',
          '2020-12-31T06:00:00Z',
          '2021-01-02T00:00:00Z',
        ),
      ).toEqual({
        start: '2020-12-31T20:00:00Z',
        end: '2021-01-01T02:00:00Z',
      });
    });
    it('should format valid string into object with start if only start time passed', () => {
      expect(
        parseValidToObject(
          '070600',
          '2020-12-07T06:00:00Z',
          '2020-12-08T12:00:00Z',
        ),
      ).toEqual({
        start: '2020-12-07T06:00:00Z',
      });
      expect(
        parseValidToObject(
          '070000',
          '2020-12-07T06:00:00Z',
          '2020-12-08T12:00:00Z',
        ),
      ).toEqual({
        start: '2020-12-07T00:00:00Z',
      });
      expect(
        parseValidToObject(
          '071800',
          '2020-12-07T06:00:00Z',
          '2020-12-08T12:00:00Z',
        ),
      ).toEqual({
        start: '2020-12-07T18:00:00Z',
      });
    });
  });

  describe('validateValidField', () => {
    it('should validate valid field and not accept if <6 or >9 chars or adheres to XXXX/XXXX or XXXXXX format', () => {
      expect(
        validateValidField(
          '07000',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidChangeValidInterFieldMessage);
      expect(
        validateValidField(
          '097564057392',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidChangeValidInterFieldMessage);
      expect(
        validateValidField(
          '0708-0708',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidChangeValidInterFieldMessage);
      expect(
        validateValidField(
          '0/8070',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidChangeValidInterFieldMessage);
      expect(
        validateValidField(
          '0807/0812',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '080700',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(true);
    });

    it('should validate valid start does not include 24 and valid end not 00', () => {
      expect(
        validateValidField(
          '2822/2824',
          '2020-12-28T18:00:00Z',
          '2020-12-30T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '2900/2904',
          '2020-12-28T18:00:00Z',
          '2020-12-30T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '080000',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '2824/2904',
          '2020-12-28T18:00:00Z',
          '2020-12-30T00:00:00Z',
        ),
      ).toEqual(invalidValidityDaysHoursStartMessage);
      expect(
        validateValidField(
          '2822/2900',
          '2020-12-28T18:00:00Z',
          '2020-12-30T00:00:00Z',
        ),
      ).toEqual(invalidValidityDaysHoursEndMessage);
      expect(
        validateValidField(
          '072400',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityDaysHoursStartMessage);
    });

    it('should validate to ensure only valid dates are passed', () => {
      expect(
        validateValidField(
          '601200',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityDaysHoursStartMessage);
      expect(
        validateValidField(
          '232700',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityDaysHoursStartMessage);
      expect(
        validateValidField(
          '999900',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityDaysHoursStartMessage);
      expect(
        validateValidField(
          '241599',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityMinutesMessage);
      expect(
        validateValidField(
          '3522/2304',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityDaysHoursStartMessage);
      expect(
        validateValidField(
          '1248/2304',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityDaysHoursStartMessage);
      expect(
        validateValidField(
          '0706/9999',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityDaysHoursEndMessage);
      expect(
        validateValidField(
          '0706/4500',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityDaysHoursEndMessage);
      expect(
        validateValidField(
          '0719/0812',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '072300',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(true);
    });

    it('should make sure the change group validity is within bounds of TAF validity', () => {
      expect(
        validateValidField(
          '072200',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '072200',
          '2020-12-07T22:12:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '011200',
          '2020-12-01T06:00:00Z',
          '2020-12-02T12:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '2822/2824',
          '2020-12-28T18:00:00Z',
          '2020-12-30T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '0706/0724',
          '2020-12-07T00:00:00Z',
          '2020-12-09T06:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '0700/0724',
          '2020-12-06T18:00:00Z',
          '2020-12-08T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '0711/0715',
          '2020-12-07T12:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityStartTimeMessage);
      expect(
        validateValidField(
          '0906/0912',
          '2020-12-07T12:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityStartTimeMessage);
      expect(
        validateValidField(
          '1223/1303',
          '2020-12-12T18:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(invalidValidityEndTimeMessage);
      expect(
        validateValidField(
          '1200/1203',
          '2020-12-12T18:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(invalidValidityStartTimeMessage);
      expect(
        validateValidField(
          '3113/0119',
          '2020-12-31T12:00:00Z',
          '2021-01-01T18:00:00Z',
        ),
      ).toEqual(invalidValidityEndTimeMessage);
      expect(
        validateValidField(
          '3111/0112',
          '2020-12-31T12:00:00Z',
          '2021-01-01T18:00:00Z',
        ),
      ).toEqual(invalidValidityStartTimeMessage);
      expect(
        validateValidField(
          '311100',
          '2020-12-31T12:00:00Z',
          '2021-01-01T18:00:00Z',
        ),
      ).toEqual(invalidValidityStartTimeMessage);
    });

    it('should validate end is after start (if end passed)', () => {
      expect(
        validateValidField(
          '072200',
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '011200',
          '2020-12-01T06:00:00Z',
          '2020-12-02T12:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '2822/2824',
          '2020-12-28T18:00:00Z',
          '2020-12-30T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '0706/0724',
          '2020-12-07T00:00:00Z',
          '2020-12-09T06:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '0700/0724',
          '2020-12-06T18:00:00Z',
          '2020-12-08T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidField(
          '0807/0712',
          '2020-12-07T12:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual(invalidValidityPeriodMessage);
      expect(
        validateValidField(
          '1223/1219',
          '2020-12-12T18:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(invalidValidityPeriodMessage);
      expect(
        validateValidField(
          '0112/3113',
          '2020-12-31T12:00:00Z',
          '2021-01-01T18:00:00Z',
        ),
      ).toEqual(invalidValidityPeriodMessage);
      expect(
        validateValidField(
          '3113/0112',
          '2020-12-31T12:00:00Z',
          '2021-01-01T18:00:00Z',
        ),
      ).toEqual(true);
    });
  });
  describe('validateValidChangeGroupInterField', () => {
    it('should accept empty strings', () => {
      expect(validateValidChangeGroupInterField('', '', '', '')).toEqual(true);
      expect(validateValidChangeGroupInterField('  ', ' ', ' ', ' ')).toEqual(
        true,
      );
    });
    it('should only accept DDHH/DDHH only in combination with change BECMG and TEMPO', () => {
      expect(
        validateValidChangeGroupInterField(
          '1215/1223',
          'TEMPO',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidChangeGroupInterField(
          '1215/1217',
          'BECMG',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidChangeGroupInterField(
          '1215/1223',
          'FM',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(invalidChangeValidInterFieldMessage);
    });
    it('should only accept DDHHmm only in combination with change FM', () => {
      expect(
        validateValidChangeGroupInterField(
          '121523',
          'FM',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidChangeGroupInterField(
          '121523',
          'TEMPO',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(invalidChangeValidInterFieldMessage);
      expect(
        validateValidChangeGroupInterField(
          '121523',
          'BECMG',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(invalidChangeValidInterFieldMessage);
    });
    it('should only accept validity periods of 1, 2, 3 or 4 hours for BECMG groups', () => {
      expect(
        validateValidChangeGroupInterField(
          '1215/1216',
          'BECMG',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidChangeGroupInterField(
          '1215/1217',
          'BECMG',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidChangeGroupInterField(
          '1215/1218',
          'BECMG',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidChangeGroupInterField(
          '1215/1219',
          'BECMG',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(true);
      // A longer TEMPO duration should be allowed
      expect(
        validateValidChangeGroupInterField(
          '1215/1225',
          'TEMPO',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(true);
      expect(
        validateValidChangeGroupInterField(
          '1215/1215',
          'BECMG',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(invalidBECMGDurationValidInterFieldMessage);
      expect(
        validateValidChangeGroupInterField(
          '1215/1220',
          'BECMG',
          '2020-12-12T15:00:00Z',
          '2020-12-13T00:00:00Z',
        ),
      ).toEqual(invalidBECMGDurationValidInterFieldMessage);
    });
  });

  describe('validatePreviousChangeGroup', () => {
    const tafStart = '2021-11-11T23:00:00Z';
    const tafEnd = '2021-11-12T00:00:00Z';

    it('should validate previous valid date order', () => {
      expect(
        validatePreviousChangeGroup(
          {
            change: 'BECMG',
            valid: '1523/1524',
            probability: '',
          },
          {
            change: 'FM',
            valid: '151200',
            probability: '',
          },

          tafStart,
          tafEnd,
        ),
      ).toEqual(true);

      expect(
        validatePreviousChangeGroup(
          {
            change: 'FM',
            valid: '151200',
          },
          {
            change: 'BECMG',
            valid: '1612/1614',
          },
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidPreviousChangeGroupIsBeforeMessage);
    });

    it('should validate change/probability order for same valid dates', () => {
      const changeGroupFM = {
        change: 'FM',
        valid: '152300',
      };
      const changeGroupBECMG = {
        change: 'BECMG',
        valid: '1523/1524',
      };
      const changeGroupTEMPO = {
        change: 'TEMPO',
        valid: '1523/1524',
      };
      const changeGroupPROB40 = {
        probability: 'PROB40',
        valid: '1523/1524',
        change: undefined,
      };
      const changeGroupPROB30 = {
        probability: 'PROB30',
        valid: '1523/1524',
        change: undefined,
      };
      const changeGroupPROB40TEMPO = {
        probability: 'PROB40',
        change: 'TEMPO',
        valid: '1523/1524',
      };
      const changeGroupPROB30TEMPO = {
        probability: 'PROB30',
        change: 'TEMPO',
        valid: '1523/1524',
      };
      // valid order
      expect(
        validatePreviousChangeGroup(
          changeGroupBECMG,
          changeGroupFM,
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);
      expect(
        validatePreviousChangeGroup(
          changeGroupTEMPO,
          changeGroupBECMG,
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);
      expect(
        validatePreviousChangeGroup(
          changeGroupPROB40,
          changeGroupTEMPO,
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);
      expect(
        validatePreviousChangeGroup(
          changeGroupPROB30,
          changeGroupPROB40,
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);
      expect(
        validatePreviousChangeGroup(
          changeGroupPROB40TEMPO,
          changeGroupPROB30,
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);
      expect(
        validatePreviousChangeGroup(
          changeGroupPROB30TEMPO,
          changeGroupPROB40TEMPO,
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);

      // invalid order
      expect(
        validatePreviousChangeGroup(
          changeGroupFM,
          changeGroupBECMG,
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidPreviousChangeGroupOrderMessage);
      expect(
        validatePreviousChangeGroup(
          changeGroupBECMG,
          changeGroupTEMPO,
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidPreviousChangeGroupOrderMessage);
      expect(
        validatePreviousChangeGroup(
          changeGroupTEMPO,
          changeGroupPROB40,
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidPreviousChangeGroupOrderMessage);
      expect(
        validatePreviousChangeGroup(
          changeGroupPROB40,
          changeGroupPROB30,
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidPreviousChangeGroupOrderMessage);
      expect(
        validatePreviousChangeGroup(
          changeGroupPROB30,
          changeGroupPROB40TEMPO,
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidPreviousChangeGroupOrderMessage);
      expect(
        validatePreviousChangeGroup(
          changeGroupPROB40TEMPO,
          changeGroupPROB30TEMPO,
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidPreviousChangeGroupOrderMessage);
      expect(
        validatePreviousChangeGroup(
          changeGroupFM,
          changeGroupPROB40TEMPO,
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidPreviousChangeGroupOrderMessage);
    });

    it('should return error when change/prob and valid are the same ', () => {
      expect(
        validatePreviousChangeGroup(
          {
            change: 'FM',
            valid: '152300',
          },
          {
            change: 'FM',
            valid: '1523/1524',
          },
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidPreviousChangeGroupHasSameValue);
      expect(
        validatePreviousChangeGroup(
          {
            probability: 'PROB40',
            change: 'TEMPO',
            valid: '1523/1524',
          },
          {
            probability: 'PROB40',
            change: 'TEMPO',
            valid: '1523/1524',
          },
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidPreviousChangeGroupHasSameValue);
    });

    it('should not validate if change and prob are empty', () => {
      expect(
        validatePreviousChangeGroup(
          {
            change: undefined,
            valid: '152300',
          },
          {
            change: undefined,
            valid: '1523/1524',
          },
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);

      expect(
        validatePreviousChangeGroup(
          {
            change: undefined,
            probability: undefined,
            valid: '152300',
          },
          {
            change: undefined,
            probability: undefined,
            valid: '1523/1524',
          },
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);
    });
  });

  describe('validateOverlapWithFM', () => {
    it('should not allow the validity to overlap with a following changegroup that has Change FM', () => {
      const tafStart = '2022-01-19T12:00:00Z';
      const tafEnd = '2022-01-20T18:00:00Z';
      expect(
        validateOverlapWithFM(
          {
            change: '',
            valid: '',
          },
          [
            {
              change: 'FM',
              valid: '191600',
            },
          ],
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);
      expect(
        validateOverlapWithFM(
          {
            change: 'TEMPO',
            valid: '1912/1916',
          },
          [
            {
              change: 'FM',
              valid: '191600',
            },
          ],
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);
      expect(
        validateOverlapWithFM(
          {
            change: 'TEMPO',
            valid: '1912/1916',
          },
          [
            {
              change: 'FM',
              valid: '191700',
            },
          ],
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);
      expect(
        validateOverlapWithFM(
          {
            change: 'TEMPO',
            valid: '1912/1916',
          },
          [],
          tafStart,
          tafEnd,
        ),
      ).toEqual(true);
      expect(
        validateOverlapWithFM(
          {
            change: 'TEMPO',
            valid: '1912/1916',
          },
          [
            {
              change: 'FM',
              valid: '191200',
            },
          ],
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidChangeGroupOverlapsWithFM);
      expect(
        validateOverlapWithFM(
          {
            change: 'TEMPO',
            probability: undefined,
            valid: '1912/1916',
          },
          [
            {
              change: 'FM',
              valid: '191400',
            },
          ],
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidChangeGroupOverlapsWithFM);
      expect(
        validateOverlapWithFM(
          {
            change: 'TEMPO',
            valid: '1912/1916',
          },
          [
            {
              change: ' FM ',
              valid: '191400',
            },
          ],
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidChangeGroupOverlapsWithFM);
      expect(
        validateOverlapWithFM(
          {
            change: 'BECMG',
            valid: '1912/1916',
          },
          [
            {
              change: 'TEMPO',
              valid: '1912/1916',
            },
            {
              change: 'FM',
              valid: '191200',
            },
          ],
          tafStart,
          tafEnd,
        ),
      ).toEqual(invalidChangeGroupOverlapsWithFM);
    });
  });
});
