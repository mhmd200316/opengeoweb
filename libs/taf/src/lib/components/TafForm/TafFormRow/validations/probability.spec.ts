/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  invalidProbabilityMessage,
  validateProbabilityField,
} from './probability';

describe('TafForm/TafFormRow/validations/probability', () => {
  describe('validateProbabilityField', () => {
    it('should accept empty strings', () => {
      expect(validateProbabilityField('')).toEqual(true);
      expect(validateProbabilityField('  ')).toEqual(true);
    });
    it('should accept PROB30 and PROB40', () => {
      expect(validateProbabilityField('PROB30')).toEqual(true);
      expect(validateProbabilityField('PROB40')).toEqual(true);
      expect(validateProbabilityField('  PROB30')).toEqual(true);
      expect(validateProbabilityField('PROB40  ')).toEqual(true);
    });
    it('should not accept anything else', () => {
      expect(validateProbabilityField('5')).toEqual(invalidProbabilityMessage);
      expect(validateProbabilityField('PROB 30')).toEqual(
        invalidProbabilityMessage,
      );
      expect(validateProbabilityField('PROB60')).toEqual(
        invalidProbabilityMessage,
      );
      expect(validateProbabilityField('WRONG INPUT')).toEqual(
        invalidProbabilityMessage,
      );
      expect(validateProbabilityField('30PROB')).toEqual(
        invalidProbabilityMessage,
      );
      expect(validateProbabilityField('5')).toEqual(invalidProbabilityMessage);
    });
  });
});
