/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  invalidChangeMessage,
  invalidChangeProbCombinationMessage,
  validateChangeField,
  validateChangeFieldInterField,
} from './change';

describe('TafForm/TafFormRow/validations/change', () => {
  describe('validateChangeField', () => {
    it('should accept empty strings', () => {
      expect(validateChangeField('')).toEqual(true);
      expect(validateChangeField('  ')).toEqual(true);
    });
    it('should accept FM BECMG and TEMPO', () => {
      expect(validateChangeField('TEMPO')).toEqual(true);
      expect(validateChangeField('BECMG')).toEqual(true);
      expect(validateChangeField('FM')).toEqual(true);
      expect(validateChangeField('  TEMPO')).toEqual(true);
      expect(validateChangeField('BECMG  ')).toEqual(true);
    });
    it('should not accept anything else', () => {
      expect(validateChangeField('5')).toEqual(invalidChangeMessage);
      expect(validateChangeField('FM 30')).toEqual(invalidChangeMessage);
      expect(validateChangeField('BECOMING')).toEqual(invalidChangeMessage);
      expect(validateChangeField('TEMP')).toEqual(invalidChangeMessage);
      expect(validateChangeField('F M')).toEqual(invalidChangeMessage);
    });
  });
  describe('validateChangeFieldInterField', () => {
    it('should accept empty strings', () => {
      expect(validateChangeFieldInterField('', '')).toEqual(true);
      expect(validateChangeFieldInterField('  ', ' ')).toEqual(true);
    });
    it('should only accept PROB30/PROB40 in combination with TEMPO', () => {
      expect(validateChangeFieldInterField('TEMPO', 'PROB30')).toEqual(true);
      expect(validateChangeFieldInterField('TEMPO', 'PROB40')).toEqual(true);
      expect(validateChangeFieldInterField('TEMPO', '')).toEqual(true);
      expect(validateChangeFieldInterField('', 'PROB30')).toEqual(true);
      expect(validateChangeFieldInterField('', 'PROB40')).toEqual(true);
      expect(validateChangeFieldInterField('', '')).toEqual(true);
      expect(validateChangeFieldInterField('FM', 'PROB30')).toEqual(
        invalidChangeProbCombinationMessage,
      );
      expect(validateChangeFieldInterField('FM', 'PROB40')).toEqual(
        invalidChangeProbCombinationMessage,
      );
      expect(validateChangeFieldInterField('FM', '')).toEqual(true);
      expect(validateChangeFieldInterField('BECMG', 'PROB30')).toEqual(
        invalidChangeProbCombinationMessage,
      );
      expect(validateChangeFieldInterField('BECMG', 'PROB40')).toEqual(
        invalidChangeProbCombinationMessage,
      );
      expect(validateChangeFieldInterField('BECMG', '')).toEqual(true);
    });
  });
});
