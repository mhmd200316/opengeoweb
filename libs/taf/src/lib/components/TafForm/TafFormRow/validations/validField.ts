/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import moment, { Moment } from 'moment';
import { ChangeGroupFormData, Valid } from '../../../../types';

interface ValidSections {
  day: number;
  hour: number;
  min: number;
}

export const FORMAT_DAYS_HOURS = 'DDHH';
export const FORMAT_DAYS_HOURS_MINUTES = 'DDHHmm';

// formatters
export const formatValidToString = (valid: Valid): string => {
  // Ensure end of valid period if midnight is shown as 24 and not 00
  const endHours = moment.utc(valid.end).format('kk');
  const endDate =
    endHours === '24'
      ? moment.utc(valid.end).add(-1, 'day').format('DD')
      : moment.utc(valid.end).format('DD');
  return valid.end
    ? `${moment
        .utc(valid.start)
        .format(FORMAT_DAYS_HOURS)}/${endDate}${endHours}`
    : moment.utc(valid.start).format(FORMAT_DAYS_HOURS_MINUTES);
};

const getSections = (value: string, format: string): ValidSections => {
  return {
    day: parseInt(value.substring(0, 2), 10),
    hour: parseInt(value.substring(2, 4), 10),
    min:
      format === FORMAT_DAYS_HOURS_MINUTES
        ? parseInt(value.substring(4), 10)
        : 0,
  };
};

const getValidityTimeStamp = (
  sectionType: 'start' | 'end',
  sections: ValidSections,
  startTaf: Moment,
  endTaf: Moment,
): Moment => {
  const resultMoment = startTaf.clone();

  resultMoment
    .date(sections.day)
    .hours(sections.hour)
    .minutes(sections.min)
    .seconds(0)
    .milliseconds(0);

  // if start and hour is the same but minutes != 00 for the startTaf - set the minutes to account for AMENDED tafs which do not start on the hour
  if (
    sectionType === 'start' &&
    startTaf.date() === resultMoment.date() &&
    startTaf.hours() === resultMoment.hours() &&
    startTaf.minutes() !== resultMoment.minutes()
  ) {
    resultMoment.minutes(startTaf.minutes()).seconds(startTaf.seconds());
  }

  // Only proceed if moment has not bubbled dates overflow to months and not bubbled hours overflow to days
  if (
    (resultMoment.date() === sections.day &&
      resultMoment.hours() === sections.hour &&
      resultMoment.minutes() === sections.min) ||
    (resultMoment.date() === sections.day + 1 &&
      sections.hour === 24 &&
      sections.min === 0)
  ) {
    if (endTaf.month() !== startTaf.month() && sections.day < 15) {
      resultMoment.add(1, 'months');
    }
  }

  return resultMoment;
};

// parsers
export const parseValidToObject = (
  value: string,
  tafStart: string,
  tafEnd: string,
): Valid => {
  if (!value) return null;
  // Create valid object from passed string
  const trimmedValue = value.trim();

  const tafStartMoment = moment.utc(tafStart);
  const tafEndMoment = moment.utc(tafEnd);

  const startEnd = trimmedValue.split('/');
  const format =
    trimmedValue.indexOf('/') === -1
      ? FORMAT_DAYS_HOURS_MINUTES
      : FORMAT_DAYS_HOURS;

  const startSections = getSections(startEnd[0], format);
  const start = getValidityTimeStamp(
    'start',
    startSections,
    tafStartMoment,
    tafEndMoment,
  ).format('YYYY-MM-DDTHH:mm:ss[Z]');

  // If we only have start value - return that otherwise, also get end time
  if (startEnd.length < 2) {
    return {
      start,
    };
  }
  const endSections = getSections(startEnd[1], FORMAT_DAYS_HOURS);
  const end = getValidityTimeStamp(
    'end',
    endSections,
    tafStartMoment,
    tafEndMoment,
  ).format('YYYY-MM-DDTHH:mm:ss[Z]');

  return { start, end };
};

// messages
export const invalidValidityDaysHoursStartMessage =
  'Invalid validity start period, expected a date value between 00 and 31 and an hour value between 00 and 23';
export const invalidValidityDaysHoursEndMessage =
  'Invalid validity end period, expected a date value between 00 and 31 and an hour value between 01 and 24';
export const invalidValidityMinutesMessage =
  'Invalid validity period, specified minutes should be between 00 and 59';
export const invalidValidityStartTimeMessage =
  'Invalid validity start time, change group validity must lie within the TAF validity period';
export const invalidValidityPeriodMessage =
  'Invalid validity period, validity end time has to be after validity start time';
export const invalidValidityEndTimeMessage =
  'Invalid validity end time, change group validity must lie within the TAF validity period';
export const invalidChangeValidInterFieldMessage =
  'Invalid validity period, expected DDHH/DDHH for BECMG and TEMPO or DDHHmm for FM';
export const invalidBECMGDurationValidInterFieldMessage =
  'Invalid validity period, for BECMG only validity periods with a duration of 1, 2, 3 or 4 hours are allowed';
export const invalidChangeGroupOverlapsWithFM =
  'The valid time of a change group cannot overlap with the valid time of a following FM group. This change group should end before or at the hour of the start of the FM group';

// Validations

// Ensure all individual sections are correct (DD HH mm etc)
const validateValidSections = (
  sections: ValidSections,
  isStartEnd: 'start' | 'end',
): string[] => {
  const validateMsg = [];

  if (
    isStartEnd === 'start' &&
    (sections.day < 1 ||
      sections.day > 31 ||
      sections.hour < 0 ||
      sections.hour > 23)
  ) {
    validateMsg.push(invalidValidityDaysHoursStartMessage);
  } else if (
    isStartEnd === 'end' &&
    (sections.day < 1 ||
      sections.day > 31 ||
      sections.hour < 1 ||
      sections.hour > 24)
  ) {
    validateMsg.push(invalidValidityDaysHoursEndMessage);
  }
  if (sections.min < 0 || sections.min > 59) {
    validateMsg.push(invalidValidityMinutesMessage);
  }

  return validateMsg;
};

// Validate to ensure valid dates are passed
const validateIsValidDates = (
  value: string,
  tafStart: string,
  tafEnd: string,
): string[] => {
  const tafStartMoment = moment.utc(tafStart);
  const tafEndMoment = moment.utc(tafEnd);

  const startEnd = value.split('/');
  const format =
    value.indexOf('/') === -1 ? FORMAT_DAYS_HOURS_MINUTES : FORMAT_DAYS_HOURS;

  const validateMsg = [];

  // Validate start time
  const startSections = getSections(startEnd[0], format);
  // Validate individual sections for start
  const sectionValidationStart = validateValidSections(startSections, 'start');
  // If invalid start - return with errors
  if (sectionValidationStart.length) return sectionValidationStart;

  const startTimestamp = getValidityTimeStamp(
    'start',
    startSections,
    tafStartMoment,
    tafEndMoment,
  );

  // Validate start is within the taf validity bounds
  if (startTimestamp < tafStartMoment || startTimestamp > tafEndMoment) {
    validateMsg.push(invalidValidityStartTimeMessage);
  }

  // Validate end time
  if (startEnd.length > 1) {
    const endSections = getSections(startEnd[1], FORMAT_DAYS_HOURS);

    // Validate individual sections for end
    const sectionValidationEnd = validateValidSections(endSections, 'end');
    // If invalid end - return with errors
    if (sectionValidationEnd.length) return sectionValidationEnd;

    const endTimestamp = getValidityTimeStamp(
      'end',
      endSections,
      tafStartMoment,
      tafEndMoment,
    );

    // Validate end is not before start
    if (endTimestamp < startTimestamp) {
      validateMsg.push(invalidValidityPeriodMessage);
    }

    // Validate end is within the taf validity bounds
    if (endTimestamp < tafStartMoment || endTimestamp > tafEndMoment) {
      validateMsg.push(invalidValidityEndTimeMessage);
    }
  }

  return validateMsg;
};

// Main validation function
export const validateValidField = (
  value: string,
  tafStart: string,
  tafEnd: string,
): boolean | string => {
  const trimmedValue = value.trim();
  if (!trimmedValue.length) {
    return true;
  }
  // Validate length and ensure it doesn't contain any other characters than numbers and / and has either XXXX/XXXX or XXXXXX format
  if (
    trimmedValue.length < 6 ||
    trimmedValue.length > 9 ||
    !(
      trimmedValue.match(/^[0-9]{4}\/[0-9]{4}$/) ||
      trimmedValue.match(/^[0-9]{6}$/)
    )
  ) {
    return invalidChangeValidInterFieldMessage;
  }

  // Ensure valid date(s) can be made
  const validDateErrors = validateIsValidDates(trimmedValue, tafStart, tafEnd);
  if (validDateErrors.length !== 0) {
    return validDateErrors.find((error) => typeof error === 'string');
  }

  return true;
};

export const validateValidChangeGroupInterField = (
  value: string,
  valueChange: string,
  tafStart: string,
  tafEnd: string,
): boolean | string => {
  const trimmedValue = value.trim();
  const trimmedValueChange = valueChange.trim();

  if (!trimmedValue.length) {
    return true;
  }

  // Ensure format for FM changegroups is correct DDHHmm
  if (
    (trimmedValue.length > 6 && trimmedValueChange === 'FM') ||
    (trimmedValue.length < 9 && trimmedValueChange !== 'FM')
  ) {
    return invalidChangeValidInterFieldMessage;
  }

  // Ensure BECMG changegroup has duration of 1,2,3,4 hours
  if (trimmedValueChange === 'BECMG') {
    const tafStartMoment = moment.utc(tafStart);
    const tafEndMoment = moment.utc(tafEnd);

    const startEnd = trimmedValue.split('/');
    const format =
      trimmedValue.indexOf('/') === -1
        ? FORMAT_DAYS_HOURS_MINUTES
        : FORMAT_DAYS_HOURS;

    // Validate start time
    const startSections = getSections(startEnd[0], format);
    const startTimestamp = getValidityTimeStamp(
      'start',
      startSections,
      tafStartMoment,
      tafEndMoment,
    );
    const endSections = getSections(startEnd[1], FORMAT_DAYS_HOURS);
    const endTimestamp = getValidityTimeStamp(
      'end',
      endSections,
      tafStartMoment,
      tafEndMoment,
    );
    const timeDifference = endTimestamp.diff(startTimestamp, 'hours');
    if (![1, 2, 3, 4].includes(timeDifference)) {
      return invalidBECMGDurationValidInterFieldMessage;
    }
  }

  return true;
};

// validate previous changeGroup
const validProbChangeOrder = [
  'FM',
  'BECMG',
  'TEMPO',
  'PROB40',
  'PROB30',
  'PROB40 TEMPO',
  'PROB30 TEMPO',
];
export const invalidPreviousChangeGroupIsBeforeMessage =
  'Valid time should be after previous valid time';
export const invalidPreviousChangeGroupOrderMessage = `When valid times are equal, the change/probability order should be: ${validProbChangeOrder.join(
  ', ',
)}`;
export const invalidPreviousChangeGroupHasSameValue =
  "When valid times are equal, probability/change can't have the same value as previous row value";

export const validatePreviousChangeGroup = (
  currentChangeGroup: ChangeGroupFormData,
  previousChangeGroup: ChangeGroupFormData,
  tafStart: string,
  tafEnd: string,
): boolean | string => {
  // extract the needed values from changeGroups
  const { valid, change = '', probability = '' } = currentChangeGroup;
  const {
    valid: previousValid,
    change: previousChange = '',
    probability: previousProbability = '',
  } = previousChangeGroup;

  // get the parsed values
  const validValue = parseValidToObject(valid, tafStart, tafEnd);
  const previousValidValue = parseValidToObject(
    previousValid,
    tafStart,
    tafEnd,
  );
  if (
    !validValue ||
    !previousValidValue ||
    (!change.length && !probability.length)
  ) {
    return true;
  }

  // validate is valid time before
  const currentStartTime = moment(validValue.start);
  const previousRowStartTime = moment(previousValidValue.start);

  if (currentStartTime.isBefore(previousRowStartTime)) {
    return invalidPreviousChangeGroupIsBeforeMessage;
  }

  // validate same valid time
  if (currentStartTime.isSame(previousRowStartTime)) {
    // validate change/probability order
    const currentProbChangeValue = `${probability} ${change}`.trim();
    const previousProbChangeValue =
      `${previousProbability} ${previousChange}`.trim();

    if (
      validProbChangeOrder.indexOf(currentProbChangeValue) <
      validProbChangeOrder.indexOf(previousProbChangeValue)
    ) {
      return invalidPreviousChangeGroupOrderMessage;
    }
    // validate same valid + change/probability value
    if (currentProbChangeValue === previousProbChangeValue) {
      return invalidPreviousChangeGroupHasSameValue;
    }
  }

  return true;
};

// validate overlapping validity when one of the following changegroups is FM
export const validateOverlapWithFM = (
  currentChangeGroup: ChangeGroupFormData,
  nextChangeGroups: ChangeGroupFormData[],
  tafStart: string,
  tafEnd: string,
): boolean | string => {
  const { valid } = currentChangeGroup;
  if (!valid || !nextChangeGroups.length) return true;
  const validValue = parseValidToObject(valid, tafStart, tafEnd);
  const currentRowEndTime = moment(validValue.end);

  const firstFMChangeGroup = nextChangeGroups.find(
    (nextChangeGroup) => nextChangeGroup.change.trim() === 'FM',
  );
  if (firstFMChangeGroup && firstFMChangeGroup.valid) {
    const FMValidValue = parseValidToObject(
      firstFMChangeGroup.valid,
      tafStart,
      tafEnd,
    );
    const FMStartTime = moment(FMValidValue.start);

    if (currentRowEndTime.isAfter(FMStartTime)) {
      return invalidChangeGroupOverlapsWithFM;
    }
  }
  return true;
};
