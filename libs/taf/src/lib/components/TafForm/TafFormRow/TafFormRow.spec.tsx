/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import TafFormRow from './TafFormRow';
import { TafThemeFormProvider } from '../../Providers';

describe('TafForm/TafFormRow/TafFormRow', () => {
  it('should trigger validation on cloud4 when cloud3 is updated', async () => {
    const { getAllByText, container } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
          defaultValues: {
            baseForecast: {
              cloud: {
                cloud1: 'FEW005',
                cloud2: 'SCT010',
                cloud3: 'BKN040',
                cloud4: 'BKN050CB',
              },
            },
          },
        }}
      >
        <TafFormRow />
      </TafThemeFormProvider>,
    );

    const cloudLabels = getAllByText('Cloud');

    // cloud4 shows no errors yet
    expect(cloudLabels[3].classList).not.toContain('Mui-error');

    // enter cloud3 value
    fireEvent.input(
      container.querySelector("[name='baseForecast.cloud.cloud3']"),
      {
        target: { value: 'OVC050CB' },
      },
    );

    fireEvent.blur(
      container.querySelector("[name='baseForecast.cloud.cloud3']"),
    );

    // wait for validation error on cloud4
    await waitFor(() =>
      expect(cloudLabels[3].classList).toContain('Mui-error'),
    );
  });
  it('should trigger validation on cloud 2, 3 and 4 when cloud1 is updated', async () => {
    const { getAllByText, container } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
          defaultValues: {
            baseForecast: {
              cloud: {
                cloud1: 'FEW005',
                cloud2: 'SCT010',
                cloud3: 'BKN020',
                cloud4: 'BKN050CB',
              },
            },
          },
        }}
      >
        <TafFormRow />
      </TafThemeFormProvider>,
    );
    const cloudLabels = getAllByText('Cloud');

    // no errors yet on cloud 2, 3 and 4
    expect(cloudLabels[1].classList).not.toContain('Mui-error');
    expect(cloudLabels[2].classList).not.toContain('Mui-error');
    expect(cloudLabels[3].classList).not.toContain('Mui-error');

    // remove cloud1 value
    fireEvent.input(
      container.querySelector("[name='baseForecast.cloud.cloud1']"),
      {
        target: { value: '' },
      },
    );
    fireEvent.blur(
      container.querySelector("[name='baseForecast.cloud.cloud1']"),
    );

    // wait for validation errors on cloud 2, 3 and 4
    await waitFor(() => {
      expect(cloudLabels[1].classList).toContain('Mui-error');
      expect(cloudLabels[2].classList).toContain('Mui-error');
      expect(cloudLabels[3].classList).toContain('Mui-error');
    });
  });

  it('should trigger validation on cloud4 3 and 4 when cloud2 is updated', async () => {
    const { getAllByText, container } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
          defaultValues: {
            baseForecast: {
              cloud: {
                cloud1: 'FEW005',
                cloud2: 'SCT010',
                cloud3: 'BKN020',
                cloud4: 'BKN050CB',
              },
            },
          },
        }}
      >
        <TafFormRow />
      </TafThemeFormProvider>,
    );

    const cloudLabels = getAllByText('Cloud');

    // cloud3 and 4 show no errors yet
    expect(cloudLabels[2].classList).not.toContain('Mui-error');
    expect(cloudLabels[3].classList).not.toContain('Mui-error');

    // remove cloud2 value
    fireEvent.input(
      container.querySelector("[name='baseForecast.cloud.cloud2']"),
      {
        target: { value: '' },
      },
    );
    fireEvent.blur(
      container.querySelector("[name='baseForecast.cloud.cloud2']"),
    );

    // wait for validation errors on cloud 3 and 4
    await waitFor(() => {
      expect(cloudLabels[2].classList).toContain('Mui-error');
      expect(cloudLabels[3].classList).toContain('Mui-error');
    });
  });

  it('should trigger validation on weather3 when weather2 is updated', async () => {
    const { getAllByText, container } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
          defaultValues: {
            baseForecast: {
              weather: {
                weather1: 'RA',
                weather2: 'SN',
                weather3: 'SS',
              },
            },
          },
        }}
      >
        <TafFormRow />
      </TafThemeFormProvider>,
    );

    const weatherLabels = getAllByText('Weather');

    // weather3 shows no errors yet
    expect(weatherLabels[2].classList).not.toContain('Mui-error');

    // remove weather2 value
    fireEvent.input(
      container.querySelector("[name='baseForecast.weather.weather2']"),
      {
        target: { value: '' },
      },
    );
    fireEvent.blur(
      container.querySelector("[name='baseForecast.weather.weather2']"),
    );

    // wait for validation error on weather3
    await waitFor(() => {
      expect(weatherLabels[2].classList).toContain('Mui-error');
    });

    // remove weather3 value
    fireEvent.input(
      container.querySelector("[name='baseForecast.weather.weather3']"),
      {
        target: { value: '' },
      },
    );
    fireEvent.blur(
      container.querySelector("[name='baseForecast.weather.weather3']"),
    );
    // weather3 shows no errors anymore
    await waitFor(() => {
      expect(weatherLabels[2].classList).not.toContain('Mui-error');
    });
  });

  it('should trigger validation on CHANGE and PROB if valid filled in', async () => {
    const { getAllByText, container } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
          defaultValues: {
            changeGroups: [
              {
                valid: '',
              },
            ],
          },
        }}
      >
        <TafFormRow
          isChangeGroup
          index={0}
          field={{
            valid: '',
          }}
        />
      </TafThemeFormProvider>,
    );

    const changeLabel = getAllByText('Change');
    const probLabel = getAllByText('Prob');

    // change and prob don't show errors
    expect(changeLabel[0].classList).not.toContain('Mui-error');
    expect(probLabel[0].classList).not.toContain('Mui-error');

    // add valid value
    fireEvent.input(container.querySelector("[name='changeGroups[0].valid']"), {
      target: { value: '110000' },
    });
    fireEvent.blur(container.querySelector("[name='changeGroups[0].valid']"));

    // wait for validation errors on prob and change
    await waitFor(() => {
      expect(changeLabel[0].classList).toContain('Mui-error');
      expect(probLabel[0].classList).toContain('Mui-error');
    });

    // add change value
    fireEvent.input(
      container.querySelector("[name='changeGroups[0].change']"),
      {
        target: { value: 'FM' },
      },
    );
    fireEvent.blur(container.querySelector("[name='changeGroups[0].change']"));

    // wait for validation to be gone on prob and change
    await waitFor(() => {
      expect(changeLabel[0].classList).not.toContain('Mui-error');
      expect(probLabel[0].classList).not.toContain('Mui-error');
    });
  });

  it('should be allowed to enter PROB without CHANGE', async () => {
    const { getAllByText, container } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
          defaultValues: {
            changeGroups: [
              {
                valid: '',
              },
            ],
          },
        }}
      >
        <TafFormRow
          isChangeGroup
          index={0}
          field={{
            valid: '',
          }}
        />
      </TafThemeFormProvider>,
    );

    const changeLabel = getAllByText('Change');
    const probLabel = getAllByText('Prob');

    // change and prob don't show errors
    expect(changeLabel[0].classList).not.toContain('Mui-error');
    expect(probLabel[0].classList).not.toContain('Mui-error');

    // add valid value
    fireEvent.input(container.querySelector("[name='changeGroups[0].valid']"), {
      target: { value: '110000' },
    });
    fireEvent.blur(container.querySelector("[name='changeGroups[0].valid']"));

    // wait for validation errors on prob and change
    await waitFor(() => {
      expect(changeLabel[0].classList).toContain('Mui-error');
      expect(probLabel[0].classList).toContain('Mui-error');
    });

    // add prob value
    fireEvent.input(
      container.querySelector("[name='changeGroups[0].probability']"),
      {
        target: { value: 'PROB30' },
      },
    );
    fireEvent.blur(
      container.querySelector("[name='changeGroups[0].probability']"),
    );

    // wait for validation to be gone on prob and change
    await waitFor(() => {
      expect(changeLabel[0].classList).not.toContain('Mui-error');
      expect(probLabel[0].classList).not.toContain('Mui-error');
    });
  });

  it('should allow NSW in changegroup', async () => {
    const { getAllByText, container } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
          defaultValues: {
            changeGroups: [
              {
                valid: '1112/1114',
                change: 'TEMPO',
                wind: '20020',
                visibility: '5000',
                weather: {
                  weather1: 'RA',
                },
                cloud: {
                  cloud1: 'FEW005',
                },
              },
            ],
          },
        }}
      >
        <TafFormRow
          isChangeGroup
          index={0}
          field={{
            valid: '',
          }}
        />
      </TafThemeFormProvider>,
    );

    const weatherLabels = getAllByText('Weather');

    // no errors for weather fields
    expect(weatherLabels[0].classList).not.toContain('Mui-error');
    expect(weatherLabels[1].classList).not.toContain('Mui-error');
    expect(weatherLabels[2].classList).not.toContain('Mui-error');

    // remove cloud2 value
    fireEvent.input(
      container.querySelector("[name='changeGroups[0].weather.weather1']"),
      {
        target: { value: 'NSW' },
      },
    );
    fireEvent.blur(
      container.querySelector("[name='changeGroups[0].weather.weather1']"),
    );

    // wait for no validation on weather 1
    await waitFor(() => {
      expect(weatherLabels[0].classList).not.toContain('Mui-error');
    });
  });

  it('should not allow NSW in baseforecast', async () => {
    const { getAllByText, container } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
          defaultValues: {
            baseForecast: {
              wind: '20020',
              visibility: '5000',
              weather: {
                weather1: 'RA',
              },
              cloud: {
                cloud1: 'FEW005',
              },
            },
          },
        }}
      >
        <TafFormRow />
      </TafThemeFormProvider>,
    );

    const weatherLabels = getAllByText('Weather');

    // no errors for weather fields
    expect(weatherLabels[0].classList).not.toContain('Mui-error');
    expect(weatherLabels[1].classList).not.toContain('Mui-error');
    expect(weatherLabels[2].classList).not.toContain('Mui-error');

    // remove cloud2 value
    fireEvent.input(
      container.querySelector("[name='baseForecast.weather.weather1']"),
      {
        target: { value: 'NSW' },
      },
    );
    fireEvent.blur(
      container.querySelector("[name='baseForecast.weather.weather1']"),
    );

    // wait for validation error on weather 1
    await waitFor(() => {
      expect(weatherLabels[0].classList).toContain('Mui-error');
    });
  });

  it('should autocomplete CAVOK when typing C in visibility field', async () => {
    const { container } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
        }}
      >
        <TafFormRow />
      </TafThemeFormProvider>,
    );

    const input = container.querySelector('[name="baseForecast.visibility"]');
    fireEvent.keyDown(input, { key: 'C' });

    expect(input.getAttribute('value')).toEqual('CAVOK');
  });
  it('should trigger validation on CHANGE if PROB filled in', async () => {
    const { getAllByText, container } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
          defaultValues: {
            changeGroups: [
              {
                valid: '',
                change: 'FM',
              },
            ],
          },
        }}
      >
        <TafFormRow
          isChangeGroup
          index={0}
          field={{
            valid: '',
            change: 'FM',
          }}
        />
      </TafThemeFormProvider>,
    );

    const probLabel = getAllByText('Prob');
    const changeLabel = getAllByText('Change');

    // prob and change don't show errors
    expect(probLabel[0].classList).not.toContain('Mui-error');
    expect(changeLabel[0].classList).not.toContain('Mui-error');

    // add valid value
    fireEvent.input(
      container.querySelector("[name='changeGroups[0].probability']"),
      {
        target: { value: 'PROB30' },
      },
    );
    fireEvent.blur(
      container.querySelector("[name='changeGroups[0].probability']"),
    );

    // wait for validation error change
    await waitFor(() => {
      expect(changeLabel[0].classList).toContain('Mui-error');
    });

    // change change value to TEMPO
    fireEvent.input(
      container.querySelector("[name='changeGroups[0].change']"),
      {
        target: { value: 'TEMPO' },
      },
    );
    fireEvent.blur(container.querySelector("[name='changeGroups[0].change']"));

    // wait for validation to be gone on change (as combination PROB is only allowed with TEMPO)
    await waitFor(() => {
      expect(probLabel[0].classList).not.toContain('Mui-error');
      expect(changeLabel[0].classList).not.toContain('Mui-error');
    });
  });

  it('should be able to pass drag handle', async () => {
    const TestComponent: React.FC = () => <div>TEST</div>;
    const { findByText } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
          defaultValues: {
            changeGroups: [
              {
                valid: '',
                change: 'FM',
              },
            ],
          },
        }}
      >
        <TafFormRow
          isChangeGroup
          index={0}
          field={{
            valid: '',
            change: 'FM',
          }}
          dragHandle={<TestComponent />}
        />
      </TafThemeFormProvider>,
    );

    expect(await findByText('TEST')).toBeTruthy();
  });

  it('should trigger validation on all 3 weather fields when visibility is changed', async () => {
    const { getAllByText, container } = render(
      <TafThemeFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          criteriaMode: 'all',
          defaultValues: {
            baseForecast: {
              visibility: '0900',
              weather: {
                weather1: 'BR',
                weather2: 'FG',
                weather3: 'FU',
              },
            },
          },
        }}
      >
        <TafFormRow index={0} />
      </TafThemeFormProvider>,
    );

    const weatherLabel = getAllByText('Weather');

    // weather fields don't show errors
    expect(weatherLabel[0].classList).not.toContain('Mui-error');
    expect(weatherLabel[1].classList).not.toContain('Mui-error');
    expect(weatherLabel[2].classList).not.toContain('Mui-error');

    // change visibility value
    fireEvent.input(
      container.querySelector("[name='baseForecast.visibility']"),
      {
        target: { value: '6000' },
      },
    );
    fireEvent.blur(container.querySelector("[name='baseForecast.visibility']"));

    // wait for validation errors on weather fields
    await waitFor(() => {
      expect(weatherLabel[0].classList).toContain('Mui-error');
      expect(weatherLabel[1].classList).toContain('Mui-error');
      expect(weatherLabel[2].classList).toContain('Mui-error');
    });
  });
});
