/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { SxProps, TextField, TextFieldProps, Theme } from '@mui/material';
import { getDeepProperty } from '@opengeoweb/form-fields';
import React from 'react';
import {
  useFormContext,
  Controller,
  FieldErrors,
  RegisterOptions,
} from 'react-hook-form';

const textFieldStyle = (theme: Theme): SxProps<Theme> => ({
  // input
  marginRight: '2px',
  marginBottom: '4px',
  width: 56,
  height: 46,
  input: {
    textAlign: 'center',
    fontSize: 10,
    fontWeight: 500,
    paddingLeft: 0,
    paddingRight: 0,
    '&.Mui-disabled': {
      color: 'geowebColors.typographyAndIcons.text',
      WebkitTextFillColor: theme.palette.geowebColors.typographyAndIcons.text,
    },
    // TODO: https://gitlab.com/opengeoweb/opengeoweb/-/issues/1995 disables the default autofill styling, should be a better fix
    '&:-webkit-autofill': {
      WebkitTransitionDelay: '86400s', // 1 day delay
    },
  },
  label: {
    transformOrigin: 'center top',
    right: 0,
    textAlign: 'center',
    fontSize: '10px',
    '&.Mui-disabled': {
      color: 'geowebColors.captions.captionStatus.rgba',
    },
    whiteSpace: 'nowrap',
    maxWidth: '100%',
    transform: 'translate(0px, 16px) scale(1)',
  },
  // shrink
  ' .MuiInputLabel-shrink': {
    transform: 'translate(2px, 10px) scale(0.75) !important',
    letterSpacing: '0.33px',
    textAlign: 'left',
  },
  // root
  '&': {
    '.Mui-disabled:before': {
      borderColor: 'transparent',
    },
  },
});

const getTafErrors = (name: string, errors: FieldErrors): FieldErrors => {
  const nameAsArray = name.split('.');
  if (nameAsArray[0].includes('changeGroups')) {
    if (errors.changeGroups) {
      const changeGroupNum = nameAsArray[0].slice(
        13,
        nameAsArray[0].length - 1,
      );
      const restArray = nameAsArray.slice(1);
      return getDeepProperty(restArray, errors.changeGroups[changeGroupNum]);
    }
    // No changegroup errors
    return null;
  }

  return getDeepProperty(nameAsArray, errors);
};

export const getDisplayValue = (value: string, disabled: boolean): string =>
  disabled && !value ? '-' : value;

export const BaseTextField: React.FC<TextFieldProps> = React.memo(
  (props: TextFieldProps) => {
    return (
      <TextField
        type="text"
        variant="filled"
        sx={textFieldStyle as SxProps<Theme>}
        {...props}
      />
    );
  },
  (prevProps, nextProps) => {
    return (
      prevProps.value === nextProps.value &&
      prevProps.error === nextProps.error &&
      prevProps.name === nextProps.name &&
      prevProps.disabled === nextProps.disabled
    );
  },
);

interface TafFormTextFieldProps {
  label: string;
  disabled?: boolean;
  name: string;
  onChange?: (e) => void;
  defaultValue?: string;
  formatter?: (value) => string;
  rules?: RegisterOptions;
  autoComplete?: { key: string; value: string }[];
  onBlur?: () => void;
  autoFocus?: boolean;
}

const TafFormTextField: React.FC<TafFormTextFieldProps> = ({
  label,
  disabled = false,
  name = '',
  onChange = (): void => {},
  onBlur = (): void => {},
  defaultValue = '',
  formatter = null,
  rules,
  autoComplete = [],
  autoFocus = false,
}: TafFormTextFieldProps) => {
  const { control, getValues, setValue, errors } = useFormContext();
  const inputRef = React.useRef(null);
  const fieldErrors = getTafErrors(name, errors);

  const [maxLength, setMaxLength] = React.useState(null);

  const onKeyDown = React.useCallback(
    (event: React.KeyboardEvent): void => {
      const currentValue = getValues(name).toString();
      const textSelection = currentValue.substring(
        inputRef.current.selectionStart,
        inputRef.current.selectionEnd,
      );

      if (
        (autoComplete && !currentValue.length) ||
        (textSelection.length &&
          currentValue.length &&
          textSelection === currentValue)
      ) {
        const pressedKey = event.key.toString().toUpperCase();
        const foundValue =
          autoComplete.find((el) => el.key === pressedKey) || null;

        if (foundValue) {
          setValue(name, foundValue.value);
          setMaxLength(foundValue.value.length);

          event.preventDefault();
        }
      }
    },
    [name, autoComplete, setValue, getValues],
  );

  const onChangeField = React.useCallback(
    (event, callback) => {
      const valueUppercase = event.currentTarget.value.toUpperCase();
      if (autoComplete) {
        const exist = autoComplete.find((e) => e.value === valueUppercase);
        const newMaxLength = exist ? valueUppercase.length : null;
        setMaxLength(newMaxLength);
      }
      callback(valueUppercase);
      onChange(valueUppercase);
    },
    [onChange, autoComplete],
  );

  const onBlurField = React.useCallback(
    (callback) => {
      callback();
      onBlur();
    },
    [onBlur],
  );

  const onFocusField = React.useCallback(() => {
    inputRef.current.disabled = false;
    inputRef.current?.focus();
  }, []);

  return (
    <Controller
      render={(_props): JSX.Element => {
        const displayValue = formatter
          ? formatter(_props.value)
          : getDisplayValue(_props.value, disabled);

        return (
          <BaseTextField
            label={label}
            disabled={disabled}
            name={name}
            value={displayValue}
            inputRef={inputRef}
            onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
              onChangeField(e, _props.onChange)
            }
            onKeyDown={onKeyDown}
            error={!!fieldErrors}
            onBlur={(): void => onBlurField(_props.onBlur)}
            inputProps={{ maxLength }}
            autoFocus={autoFocus}
          />
        );
      }}
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={rules}
      onFocus={onFocusField}
    />
  );
};

export default TafFormTextField;
