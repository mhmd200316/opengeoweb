/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { GridProps } from '@mui/material';
import { ArrayField } from 'react-hook-form';

export interface TafFormRowProps extends GridProps {
  isChangeGroup?: boolean;
  disabled?: boolean;
  index?: number;
  onRemoveChangeGroup?: (index: number) => void;
  onAddChangeGroupBelow?: (index: number) => void;
  onAddChangeGroupAbove?: (index: number) => void;
  onClearRow?: (index: number) => void;
  field?: Partial<ArrayField>;
  dragHandle?: React.ReactElement;
}

export interface FieldNames {
  messageType?: string;
  location?: string;
  issueDate?: string;
  probability?: string;
  change?: string;
  valid: string;
  wind: string;
  visibility: string;
  weather1: string;
  weather2: string;
  weather3: string;
  cloud1: string;
  cloud2: string;
  cloud3: string;
  cloud4: string;
}
