/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import moment from 'moment';
import { renderHook, act } from '@testing-library/react-hooks';
import { fireEvent, render, waitFor } from '@testing-library/react';
import Sortable from 'sortablejs';
import { parseValidToObject } from './TafFormRow/validations/validField';
import { parseVisibilityToObject } from './TafFormRow/validations/visibility';
import { parseWindToObject } from './TafFormRow/validations/wind';
import {
  convertTafValuesToObject,
  emptyBaseForecast,
  emptyChangegroup,
  formatFormValues,
  formatIssueTime,
  getAmendedCorrectedTafTimes,
  getConfirmationDialogButtonLabel,
  getConfirmationDialogContent,
  getConfirmationDialogTitle,
  getFieldNames,
  getIsFormDefaultDisabled,
  getMessageType,
  getTafStatusLabel,
  isTafAmendedCorrectedChanged,
  parseFormValues,
  prepareTafValues,
  shouldRetrieveTAC,
  TAC_FAILED,
  TAC_NOT_AVAILABLE,
  useDragOrder,
  useTACGenerator,
} from './utils';
import {
  fakeAmendmentTaf,
  fakeDraftAmendmentTaf,
  fakeDraftTaf,
  fakeNewTaf,
  fakePublishedTaf,
  fakePublishedTafWithChangeGroups,
} from '../../utils/mockdata/fakeTafList';
import { ChangeGroup, Taf, TafFormData } from '../../types';

describe('components/TafForm/utils', () => {
  describe('formatIssueTime', () => {
    it('should return Not issued for a taf without issuedate', () => {
      expect(formatIssueTime(undefined)).toEqual('Not issued');
    });

    it('should return DDHHmmZ for a given issuedate', () => {
      expect(formatIssueTime('2020-05-10T12:30:00Z')).toEqual('101230Z');
    });
  });

  describe('formatFormValues', () => {
    it('should format form values with weather', () => {
      const testTaf = {
        change: 'BECMG',
        weather: { weather1: 'MIFG' },
      } as ChangeGroup;

      expect(formatFormValues(testTaf)).toEqual(testTaf);
    });

    it('should format form values with wind', () => {
      const testTaf = {
        change: 'BECMG',
        wind: {
          direction: 1,
          speed: 1,
          unit: 'KT',
        },
      } as ChangeGroup;

      expect(formatFormValues(testTaf)).toEqual({
        ...testTaf,
        wind: '00101',
      });
    });
    it('should format form values with valid fields', () => {
      const testTaf = {
        change: 'BECMG',
        valid: {
          start: '2020-12-07T18:00:00Z',
          end: '2020-12-09T00:00:00Z',
        },
      } as ChangeGroup;
      expect(formatFormValues(testTaf)).toEqual({
        ...testTaf,
        valid: '0718/0824',
      });

      const testTaf2 = {
        change: 'BECMG',
        valid: {
          start: '2020-12-31T22:00:00Z',
          end: '2021-01-01T02:00:00Z',
        },
      } as ChangeGroup;
      expect(formatFormValues(testTaf2)).toEqual({
        ...testTaf2,
        valid: '3122/0102',
      });

      const testTaf3 = {
        change: 'FM',
        valid: {
          start: '2020-12-31T22:00:00Z',
        },
      } as ChangeGroup;
      expect(formatFormValues(testTaf3)).toEqual({
        ...testTaf3,
        valid: '312200',
      });

      const testTaf4 = {
        change: 'FM',
        valid: {
          start: '2021-01-01T22:00:00Z',
        },
      } as ChangeGroup;
      expect(formatFormValues(testTaf4)).toEqual({
        ...testTaf4,
        valid: '012200',
      });

      expect(
        formatFormValues({
          wind: {
            direction: 0,
            speed: 0,
            unit: 'KT',
          },
          valid: {
            start: '2021-01-01T22:00:00Z',
          },
        }),
      ).toEqual({
        wind: '00000',
        valid: '012200',
      });

      expect(
        formatFormValues({
          visibility: { range: 0, unit: 'M' },
          valid: {
            start: '2021-01-01T22:00:00Z',
          },
        }),
      ).toEqual({
        visibility: '0000',
        valid: '012200',
      });
    });

    it('should format form values with visibility and cavOK', () => {
      const testTaf = {
        change: 'BECMG',
        visibility: {
          range: 50,
          unit: 'M',
        },
      } as ChangeGroup;
      expect(formatFormValues(testTaf)).toEqual({
        ...testTaf,
        visibility: '0050',
      });
      const testTaf1 = {
        change: 'BECMG',
        cavOK: true,
      } as ChangeGroup;
      expect(formatFormValues(testTaf1)).toEqual({
        ...testTaf1,
        visibility: 'CAVOK',
      });

      const testTaf2 = {
        change: 'BECMG',
        visibility: {
          range: 50,
          unit: 'M',
        },
        cavOK: false,
      } as ChangeGroup;
      expect(formatFormValues(testTaf2)).toEqual({
        ...testTaf2,
        visibility: '0050',
      });
    });
  });

  describe('parseFormValues', () => {
    const dummyTafStart = moment.utc().format('YYYY-MM-DD');
    const dummyTafEnd = moment.utc().add(1, 'day').format('YYYY-MM-DD');

    it('should trim string values', () => {
      const testTaf = {
        change: 'BECMG ',
        probability: '  PROB30 ',
        valid: ' 070600',
      };

      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        change: 'BECMG',
        probability: 'PROB30',
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
    });

    it('should parse form values with wind', () => {
      const testTaf = {
        wind: '00101KT',
        valid: '070600',
      };

      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf,
        wind: parseWindToObject(testTaf.wind),
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });

      expect(
        parseFormValues(
          { wind: '00000', valid: '0718/0824' },
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual({
        wind: {
          direction: 0,
          speed: 0,
          unit: 'KT',
        },
        valid: {
          start: '2020-12-07T18:00:00Z',
          end: '2020-12-09T00:00:00Z',
        },
      });
    });

    it('should parse form values with visibility', () => {
      const testTaf = {
        visibility: 'CAVOK',
        valid: '070600',
      };

      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
        cavOK: true,
      });

      const testTaf2 = {
        visibility: '0400',
        valid: '070600',
      };

      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        valid: parseValidToObject(testTaf2.valid, dummyTafStart, dummyTafEnd),
        ...parseVisibilityToObject(testTaf2.visibility),
      });

      expect(
        parseFormValues(
          { visibility: '0000', valid: '0718/0824' },
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual({
        visibility: {
          range: 0,
          unit: 'M',
        },
        valid: {
          start: '2020-12-07T18:00:00Z',
          end: '2020-12-09T00:00:00Z',
        },
      });
    });

    it('should parse form values with valid field', () => {
      const testTaf = {
        change: 'BECMG',
        valid: '3122/0102',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf,
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
      const testTaf2 = {
        change: 'BECMG',
        valid: '0718/0824',
      };
      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf2,
        valid: parseValidToObject(testTaf2.valid, dummyTafStart, dummyTafEnd),
      });

      const testTaf3 = {
        change: 'FM',
        valid: '071800',
      };
      expect(parseFormValues(testTaf3, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf3,
        valid: parseValidToObject(testTaf3.valid, dummyTafStart, dummyTafEnd),
      });

      const testTaf4 = {
        change: 'FM',
        valid: '082200',
      };
      expect(parseFormValues(testTaf4, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf4,
        valid: parseValidToObject(testTaf4.valid, dummyTafStart, dummyTafEnd),
      });

      const testTaf5 = {
        change: 'FM',
        valid: '012200',
      };
      expect(parseFormValues(testTaf5, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf5,
        valid: parseValidToObject(testTaf5.valid, dummyTafStart, dummyTafEnd),
      });
    });

    it('should parse form values with weather', () => {
      const testTaf = {
        change: 'BECMG',
        weather: {
          weather1: 'SN  ',
          weather2: ' ',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf,
        weather: {
          weather1: 'SN',
        },
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });

      const testTaf2 = {
        change: 'BECMG',
        weather: {
          weather1: '-RADZPL  ',
          weather2: '  +SHRA',
          weather3: 'TSSNGSRA',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf2,
        weather: {
          weather1: '-RADZPL',
          weather2: '+SHRA',
          weather3: 'TSSNGSRA',
        },
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });

      const testTaf3 = {
        change: 'BECMG',
        weather: {
          weather1: 'SN  ',
          weather2: '',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf3, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf3,
        weather: {
          weather1: 'SN',
        },
        valid: parseValidToObject(testTaf3.valid, dummyTafStart, dummyTafEnd),
      });
    });
    it('should parse form values with cloud', () => {
      const testTaf = {
        change: 'BECMG',
        cloud: {
          cloud1: 'FEW001  ',
          cloud2: 'SCT050CB',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf,
        cloud: {
          cloud1: {
            coverage: 'FEW',
            height: 1,
          },
          cloud2: {
            coverage: 'SCT',
            height: 50,
            type: 'CB',
          },
        },
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
      const testTaf2 = {
        change: 'BECMG',
        cloud: {
          cloud1: 'FEW001  ',
          cloud2: '   ',
          cloud3: '',
          cloud4: '',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        ...testTaf2,
        cloud: {
          cloud1: {
            coverage: 'FEW',
            height: 1,
          },
        },
        valid: parseValidToObject(testTaf2.valid, dummyTafStart, dummyTafEnd),
      });

      expect(
        parseFormValues(
          { cloud: { cloud1: 'VV000' }, valid: '0718/0824' },
          '2020-12-07T18:00:00Z',
          '2020-12-09T00:00:00Z',
        ),
      ).toEqual({
        cloud: { cloud1: { verticalVisibility: 0 } },
        valid: {
          start: '2020-12-07T18:00:00Z',
          end: '2020-12-09T00:00:00Z',
        },
      });
    });
    it('should remove cloud when undefined or empty', () => {
      const testTaf = {
        change: 'BECMG',
        visibility: 'CAVOK',
        cloud: undefined,
        valid: '070600',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        change: 'BECMG',
        cavOK: true,
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
      const testTaf2 = {
        change: 'BECMG',
        visibility: 'CAVOK',
        cloud: { cloud1: '' },
        valid: '070600',
      };
      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        change: 'BECMG',
        cavOK: true,
        valid: parseValidToObject(testTaf2.valid, dummyTafStart, dummyTafEnd),
      });
    });
    it('should remove weather when all empty', () => {
      const testTaf = {
        change: 'BECMG',
        visibility: 'CAVOK',
        cloud: undefined,
        weather: {
          weather1: '',
          weather2: '',
          weather3: '',
        },
        valid: '070600',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        change: 'BECMG',
        cavOK: true,
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
    });
    it('should remove wind and visibility when left empty', () => {
      const testTaf = {
        change: 'BECMG',
        wind: '',
        visibility: '',
        valid: '070600',
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        change: 'BECMG',
        valid: parseValidToObject(testTaf.valid, dummyTafStart, dummyTafEnd),
      });
    });
    it('should remove change, probability and valid when left empty', () => {
      const testTaf = {
        change: '',
        visibility: 'CAVOK',
        probability: '',
        valid: null,
      };
      expect(parseFormValues(testTaf, dummyTafStart, dummyTafEnd)).toEqual({
        cavOK: true,
      });
      const testTaf2 = {
        change: '',
        visibility: 'CAVOK',
        probability: '',
        valid: '',
      };
      expect(parseFormValues(testTaf2, dummyTafStart, dummyTafEnd)).toEqual({
        cavOK: true,
      });
    });
  });

  describe('convertTafValuesToObject', () => {
    it('should convert a taf with only a baseforecast to a taf object', () => {
      const newTaf = {
        uuid: '5f9583fjdf0f337925',
        baseTime: '2021-01-12T18:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        validDateEnd: '2021-01-14T00:00:00Z',
        location: 'EHAM',
        status: 'NEW',
        type: 'NORMAL',
        messageType: 'ORG',
        baseForecast: {
          valid: '1218/1324',
          visibility: '1000',
          wind: '12010',
        },
      } as TafFormData;
      expect(convertTafValuesToObject(newTaf)).toEqual({
        baseForecast: {
          valid: {
            end: '2021-01-14T00:00:00Z',
            start: '2021-01-12T18:00:00Z',
          },
          visibility: {
            range: 1000,
            unit: 'M',
          },
          wind: {
            direction: 120,
            speed: 10,
            unit: 'KT',
          },
        },
        baseTime: '2021-01-12T18:00:00Z',
        location: 'EHAM',
        messageType: 'ORG',
        status: 'NEW',
        type: 'NORMAL',
        uuid: '5f9583fjdf0f337925',
        validDateEnd: '2021-01-14T00:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
      });
    });
    it('should remove changegroups and issueDate when empty', () => {
      const testTaf = {
        uuid: '5f9583fjdf0f337925',
        baseTime: '2021-01-12T18:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        validDateEnd: '2021-01-14T00:00:00Z',
        location: 'EHAM',
        status: 'NEW',
        type: 'NORMAL',
        messageType: 'ORG',
        baseForecast: {
          valid: '1218/1324',
          visibility: '1000',
          wind: '12010',
        },
        changeGroups: [{}, {}],
        issueDate: '',
      } as TafFormData;
      expect(convertTafValuesToObject(testTaf)).toEqual({
        baseForecast: {
          valid: {
            end: '2021-01-14T00:00:00Z',
            start: '2021-01-12T18:00:00Z',
          },
          visibility: {
            range: 1000,
            unit: 'M',
          },
          wind: {
            direction: 120,
            speed: 10,
            unit: 'KT',
          },
        },
        baseTime: '2021-01-12T18:00:00Z',
        location: 'EHAM',
        messageType: 'ORG',
        status: 'NEW',
        type: 'NORMAL',
        uuid: '5f9583fjdf0f337925',
        validDateEnd: '2021-01-14T00:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
      });
    });
    it('should convert a taf with changegroups and issueDate to a taf object', () => {
      const testTaf = {
        uuid: '5f9583fjdf0f337925',
        baseTime: '2021-01-12T18:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        validDateEnd: '2021-01-14T00:00:00Z',
        location: 'EHAM',
        status: 'NEW',
        type: 'NORMAL',
        messageType: 'ORG',
        baseForecast: {
          valid: '1218/1324',
          visibility: '1000',
          wind: '12010',
        },
        changeGroups: [
          {},
          { change: 'BECMG', valid: '1220/1300', visibility: '2000' },
          { change: 'FM', valid: '1300/1324', visibility: '3000' },
        ],
        issueDate: '2021-01-12T17:00:00Z',
      } as TafFormData;
      expect(convertTafValuesToObject(testTaf)).toEqual({
        baseForecast: {
          valid: {
            end: '2021-01-14T00:00:00Z',
            start: '2021-01-12T18:00:00Z',
          },
          visibility: {
            range: 1000,
            unit: 'M',
          },
          wind: {
            direction: 120,
            speed: 10,
            unit: 'KT',
          },
        },
        baseTime: '2021-01-12T18:00:00Z',
        location: 'EHAM',
        messageType: 'ORG',
        status: 'NEW',
        type: 'NORMAL',
        uuid: '5f9583fjdf0f337925',
        validDateEnd: '2021-01-14T00:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        issueDate: '2021-01-12T17:00:00Z',
        changeGroups: [
          {
            change: 'BECMG',
            valid: {
              end: '2021-01-13T00:00:00Z',
              start: '2021-01-12T20:00:00Z',
            },
            visibility: {
              range: 2000,
              unit: 'M',
            },
          },
          {
            change: 'FM',
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-13T00:00:00Z',
            },
            visibility: {
              range: 3000,
              unit: 'M',
            },
          },
        ],
      });
    });

    it('should convert a taf with an empty changegroup row to a taf object without changegroups', () => {
      const testTaf = {
        uuid: '5f9583fjdf0f337925',
        baseTime: '2021-01-12T18:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        validDateEnd: '2021-01-14T00:00:00Z',
        location: 'EHAM',
        status: 'NEW',
        type: 'NORMAL',
        messageType: 'ORG',
        baseForecast: {
          valid: '1218/1324',
          visibility: '1000',
          wind: '12010',
        },
        changeGroups: [
          {
            probability: '',
            change: '',
            valid: '',
            visibility: '',
            wind: '',
            weather: { weather1: '', weather2: '', weather3: '' },
            cloud: { cloud1: '', cloud2: '', cloud3: '', cloud4: '' },
          },
        ],
        issueDate: '2021-01-12T17:00:00Z',
      } as TafFormData;
      expect(convertTafValuesToObject(testTaf)).toEqual({
        baseForecast: {
          valid: {
            end: '2021-01-14T00:00:00Z',
            start: '2021-01-12T18:00:00Z',
          },
          visibility: {
            range: 1000,
            unit: 'M',
          },
          wind: {
            direction: 120,
            speed: 10,
            unit: 'KT',
          },
        },
        baseTime: '2021-01-12T18:00:00Z',
        location: 'EHAM',
        messageType: 'ORG',
        status: 'NEW',
        type: 'NORMAL',
        uuid: '5f9583fjdf0f337925',
        validDateEnd: '2021-01-14T00:00:00Z',
        validDateStart: '2021-01-12T18:00:00Z',
        issueDate: '2021-01-12T17:00:00Z',
      });
    });
  });

  describe('shouldRetrieveTAC', () => {
    it('should retrieve a TAC only if there is a windspeed value and visibility value', () => {
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-12T18:00:00Z',
            },
            visibility: {
              range: 1000,
              unit: 'M',
            },
            wind: {
              direction: 120,
              speed: 10,
              unit: 'KT',
            },
          },
        } as Taf),
      ).toEqual(true);
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            visibility: {
              range: 1000,
            },
          },
        } as Taf),
      ).toEqual(false);
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            wind: {
              direction: 120,
              speed: 10,
              unit: 'KT',
            },
          },
        } as Taf),
      ).toEqual(false);
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-12T18:00:00Z',
            },
            visibility: {},
            wind: {},
          },
        } as Taf),
      ).toEqual(false);
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-12T18:00:00Z',
            },
            visibility: {
              range: '',
              unit: 'M',
            },
            wind: {
              direction: '',
              speed: 10,
              unit: 'KT',
            },
          },
        } as unknown as Taf),
      ).toEqual(false);
      expect(
        shouldRetrieveTAC({
          baseForecast: {},
        } as unknown as Taf),
      ).toEqual(false);
    });
    it('should retrieve a TAC even if wind or visibility are 00000 or 0000', () => {
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-12T18:00:00Z',
            },
            visibility: {
              range: 0,
              unit: 'M',
            },
            wind: {
              direction: 0,
              speed: 0,
              unit: 'KT',
            },
          },
        } as Taf),
      ).toEqual(true);
    });
    it('should retrieve a TAC if no visibility but cavOK is used', () => {
      expect(
        shouldRetrieveTAC({
          baseForecast: {
            valid: {
              end: '2021-01-14T00:00:00Z',
              start: '2021-01-12T18:00:00Z',
            },
            cavOK: true,
            wind: {
              direction: 0,
              speed: 0,
              unit: 'KT',
            },
          },
        } as Taf),
      ).toEqual(true);
    });
  });

  describe('getFieldNames', () => {
    it('should return a object with names for baseForecast', () => {
      const fieldNames = getFieldNames(false, -1);
      expect(fieldNames.messageType).toEqual('messageType');
      expect(fieldNames.location).toEqual('location');
      expect(fieldNames.issueDate).toEqual('issueDate');
      expect(fieldNames.probability).toBeUndefined();
      expect(fieldNames.change).toBeUndefined();
      expect(fieldNames.valid).toEqual(`baseForecast.valid`);
      expect(fieldNames.wind).toEqual(`baseForecast.wind`);
      expect(fieldNames.visibility).toEqual(`baseForecast.visibility`);
      expect(fieldNames.weather1).toEqual(`baseForecast.weather.weather1`);
      expect(fieldNames.weather2).toEqual(`baseForecast.weather.weather2`);
      expect(fieldNames.weather3).toEqual(`baseForecast.weather.weather3`);
      expect(fieldNames.cloud1).toEqual(`baseForecast.cloud.cloud1`);
      expect(fieldNames.cloud2).toEqual(`baseForecast.cloud.cloud2`);
      expect(fieldNames.cloud3).toEqual(`baseForecast.cloud.cloud3`);
      expect(fieldNames.cloud4).toEqual(`baseForecast.cloud.cloud4`);
    });

    it('should return a object with names for changeGroups', () => {
      const index = 4;
      const fieldNames = getFieldNames(true, index);
      expect(fieldNames.messageType).toBeUndefined();
      expect(fieldNames.location).toBeUndefined();
      expect(fieldNames.issueDate).toBeUndefined();
      expect(fieldNames.probability).toEqual(
        `changeGroups[${index}].probability`,
      );
      expect(fieldNames.change).toEqual(`changeGroups[${index}].change`);
      expect(fieldNames.valid).toEqual(`changeGroups[${index}].valid`);
      expect(fieldNames.wind).toEqual(`changeGroups[${index}].wind`);
      expect(fieldNames.visibility).toEqual(
        `changeGroups[${index}].visibility`,
      );
      expect(fieldNames.weather1).toEqual(
        `changeGroups[${index}].weather.weather1`,
      );
      expect(fieldNames.weather2).toEqual(
        `changeGroups[${index}].weather.weather2`,
      );
      expect(fieldNames.weather3).toEqual(
        `changeGroups[${index}].weather.weather3`,
      );
      expect(fieldNames.cloud1).toEqual(`changeGroups[${index}].cloud.cloud1`);
      expect(fieldNames.cloud2).toEqual(`changeGroups[${index}].cloud.cloud2`);
      expect(fieldNames.cloud3).toEqual(`changeGroups[${index}].cloud.cloud3`);
      expect(fieldNames.cloud4).toEqual(`changeGroups[${index}].cloud.cloud4`);
    });
  });

  describe('useTACGenerator', () => {
    it('should show error message when failing', async () => {
      const request = (): Promise<{ data: string }> =>
        new Promise((_resolve, reject) =>
          reject(new Error('Error from backend')),
        );

      await act(async () => {
        const { result } = renderHook(() =>
          useTACGenerator(fakePublishedTaf.taf, request),
        );

        await waitFor(() => {
          expect(result.current[0]).toEqual(TAC_FAILED);
        });
      });
    });
    it('should not update unmounted component', async () => {
      let mockResponse = 'test data received';
      const succesCounter = jest.fn();
      const request = (): Promise<{ data: string }> =>
        new Promise((resolve) => {
          setTimeout(() => {
            resolve({ data: mockResponse });
            succesCounter();
          }, 100);
        });

      const TestComponent: React.FC = () => {
        const [tac, setTAC] = useTACGenerator(fakePublishedTaf.taf, request);

        return (
          <div>
            <p>{tac}</p>
            <button
              type="button"
              onClick={(): void => setTAC(fakePublishedTaf.taf)}
            >
              button
            </button>
          </div>
        );
      };

      const { container, unmount } = render(<TestComponent />);

      await waitFor(() => {
        expect(succesCounter).toHaveBeenCalledTimes(0);
        expect(container.querySelector('p').innerHTML).toEqual(
          'Retrieving TAC',
        );
      });
      await waitFor(() => {
        expect(succesCounter).toHaveBeenCalledTimes(1);
        expect(container.querySelector('p').innerHTML).toEqual(mockResponse);
      });

      // trigger new fetch
      const newResponseData = 'new test data';
      mockResponse = newResponseData;
      fireEvent.click(container.querySelector('button'));

      await waitFor(() => {
        expect(succesCounter).toHaveBeenCalledTimes(2);
        expect(container.querySelector('p').innerHTML).toEqual(newResponseData);
      });

      // trigger new fetch and unmount
      fireEvent.click(container.querySelector('button'));
      unmount();
      expect(succesCounter).toHaveBeenCalledTimes(2);
    });
    it('should be able to change TAF directly', async () => {
      const request = (): Promise<{ data: string }> =>
        new Promise((resolve) => resolve({ data: undefined }));

      await act(async () => {
        const { result } = renderHook(() =>
          useTACGenerator(undefined, request),
        );
        const testTAC = 'test test test test';

        await waitFor(() => {
          expect(result.current[0]).toEqual(TAC_NOT_AVAILABLE);
          result.current[2](testTAC);
        });

        await waitFor(() => {
          expect(result.current[0]).toEqual(testTAC);
        });
      });
    });
  });

  describe('useDragOrder', () => {
    it('should handle order', async () => {
      const onFormBlur = jest.fn();
      const trigger = jest.fn();
      const move = jest.fn();
      const fakeEvent = { oldIndex: 1, newIndex: 0 } as Sortable.SortableEvent;
      const { result } = renderHook(() =>
        useDragOrder(trigger, move, onFormBlur),
      );

      const { onChangeOrder, activeDraggingIndex, onStartDrag } =
        result.current;

      expect(onChangeOrder).toBeDefined();
      expect(onStartDrag).toBeDefined();
      expect(activeDraggingIndex).toBeNull();
      await act(async () => {
        onChangeOrder(fakeEvent);
      });
      expect(trigger).toHaveBeenCalled();

      expect(move).toHaveBeenCalledWith(fakeEvent.oldIndex, fakeEvent.newIndex);
      expect(onFormBlur).toHaveBeenCalled();
    });
  });

  describe('getConfirmationDialogTitle', () => {
    it('Should return Save properties when action = DISCARD', () => {
      expect(getConfirmationDialogTitle('DISCARD')).toEqual('Discard TAF');
    });
    it('Should return Save properties when action = PUBLISH', () => {
      expect(getConfirmationDialogTitle('PUBLISH')).toEqual('Publish');
    });
    it('Should return Save properties when action = AMEND', () => {
      expect(getConfirmationDialogTitle('AMEND')).toEqual(
        'This TAF is an amendment',
      );
    });
    it('Should return Save properties when action = CORRECT', () => {
      expect(getConfirmationDialogTitle('CORRECT')).toEqual(
        'This TAF is a correction',
      );
    });
  });

  describe('isTafAMDCORChanged', () => {
    it('should return false if isFormDirty = false', () => {
      expect(
        isTafAmendedCorrectedChanged(
          false,
          fakeDraftAmendmentTaf.taf,
          fakeDraftAmendmentTaf.taf,
        ),
      ).toBeFalsy();
    });
    it('should return false if no changes have been made to the taf even if isFormDirty = true', () => {
      expect(
        isTafAmendedCorrectedChanged(
          true,
          fakeDraftAmendmentTaf.taf,
          fakeDraftAmendmentTaf.taf,
        ),
      ).toBeFalsy();
    });
  });

  describe('getConfirmationDialogContent', () => {
    it('Should return correct content when action = DISCARD', () => {
      expect(
        getConfirmationDialogContent('DISCARD', () =>
          isTafAmendedCorrectedChanged(
            true,
            fakeDraftAmendmentTaf.taf,
            fakeDraftAmendmentTaf.taf,
          ),
        ),
      ).toEqual(
        'Are you sure you would like to discard this TAF? Any changes made will be lost',
      );
    });
    it('Should return correct content when action = PUBLISH', () => {
      expect(
        getConfirmationDialogContent('PUBLISH', () =>
          isTafAmendedCorrectedChanged(
            true,
            fakeDraftTaf.taf,
            fakePublishedTafWithChangeGroups.taf,
          ),
        ),
      ).toEqual('Are you sure you want to publish this TAF?');
    });
    it('Should return correct content when action = AMEND', () => {
      expect(
        getConfirmationDialogContent('AMEND', () =>
          isTafAmendedCorrectedChanged(
            true,
            fakePublishedTafWithChangeGroups.taf,
            fakeAmendmentTaf.taf,
          ),
        ),
      ).toEqual('Do you want to publish it?');
      expect(
        getConfirmationDialogContent('AMEND', () =>
          isTafAmendedCorrectedChanged(
            false,
            fakePublishedTafWithChangeGroups.taf,
            fakePublishedTafWithChangeGroups.taf,
          ),
        ),
      ).toEqual(
        'Are you sure you want to publish the TAF? You did not make any changes!',
      );
      expect(
        getConfirmationDialogContent('AMEND', () =>
          isTafAmendedCorrectedChanged(
            true,
            fakePublishedTafWithChangeGroups.taf,
            fakePublishedTafWithChangeGroups.taf,
          ),
        ),
      ).toEqual(
        'Are you sure you want to publish the TAF? You did not make any changes!',
      );
    });
    it('Should return correct content when action = CORRECT', () => {
      expect(
        getConfirmationDialogContent('CORRECT', () =>
          isTafAmendedCorrectedChanged(
            true,
            fakePublishedTafWithChangeGroups.taf,
            fakeAmendmentTaf.taf,
          ),
        ),
      ).toEqual('Do you want to publish it?');
      expect(
        getConfirmationDialogContent('CORRECT', () =>
          isTafAmendedCorrectedChanged(
            false,
            fakePublishedTafWithChangeGroups.taf,
            fakePublishedTafWithChangeGroups.taf,
          ),
        ),
      ).toEqual(
        'Are you sure you want to publish the TAF? You did not make any changes!',
      );
      expect(
        getConfirmationDialogContent('CORRECT', () =>
          isTafAmendedCorrectedChanged(
            true,
            fakePublishedTafWithChangeGroups.taf,
            fakePublishedTafWithChangeGroups.taf,
          ),
        ),
      ).toEqual(
        'Are you sure you want to publish the TAF? You did not make any changes!',
      );
    });
  });

  describe('getConfirmationDialogButtonLabel', () => {
    it('Should return correct button label for DISCARD', () => {
      expect(getConfirmationDialogButtonLabel('DISCARD')).toEqual('Discard');
    });
    it('Should return correct default button label', () => {
      expect(getConfirmationDialogButtonLabel('PUBLISH')).toEqual('Publish');
      expect(getConfirmationDialogButtonLabel('AMEND')).toEqual('Publish');
      expect(getConfirmationDialogButtonLabel('CORRECT')).toEqual('Publish');
    });
  });

  describe('getTafStatusLabel', () => {
    it('should return correct labels for button statusses', () => {
      expect(getTafStatusLabel('DRAFT_AMENDED')).toEqual('draft amended');
      expect(getTafStatusLabel('DRAFT_CORRECTED')).toEqual('draft corrected');
      expect(getTafStatusLabel('NEW')).toEqual('new');
      expect(getTafStatusLabel('PUBLISHED')).toEqual('published');
      expect(getTafStatusLabel('AMENDED')).toEqual('amended');
      expect(getTafStatusLabel('CANCELLED')).toEqual('cancelled');
      expect(getTafStatusLabel('EXPIRED')).toEqual('expired');
    });
  });

  describe('getMessageType', () => {
    it('should return the original messageType if action is not CORRECT or AMEND', () => {
      expect(getMessageType(fakePublishedTaf.taf, 'PUBLISH')).toEqual(
        fakePublishedTaf.taf.messageType,
      );
      expect(getMessageType(fakePublishedTaf.taf, 'DRAFT_AMEND')).toEqual(
        fakePublishedTaf.taf.messageType,
      );
    });
    it('should return the original messageType if action is not CORRECT or AMEND', () => {
      expect(getMessageType(fakePublishedTaf.taf, 'PUBLISH')).toEqual(
        fakePublishedTaf.taf.messageType,
      );
      expect(getMessageType(fakePublishedTaf.taf, 'DRAFT_AMEND')).toEqual(
        fakePublishedTaf.taf.messageType,
      );
    });
    it('should return the AMD if action is AMEND', () => {
      expect(getMessageType(fakePublishedTaf.taf, 'AMEND')).toEqual('AMD');
    });
    it('should return the COR if action is CORRECT', () => {
      expect(getMessageType(fakePublishedTaf.taf, 'CORRECT')).toEqual('COR');
    });
    it('should return the CNL if action is CANCEL', () => {
      expect(getMessageType(fakePublishedTaf.taf, 'CANCEL')).toEqual('CNL');
    });
  });

  describe('getAmendedCorrectedTafTimes', () => {
    it('should add previousStart and End for CORRECT and DRAFT_CORRECT', () => {
      expect(
        getAmendedCorrectedTafTimes(fakePublishedTaf.taf, 'CORRECT'),
      ).toEqual({
        ...fakePublishedTaf.taf,
        previousValidDateStart: fakePublishedTaf.taf.validDateStart,
        previousValidDateEnd: fakePublishedTaf.taf.validDateEnd,
      });
      expect(
        getAmendedCorrectedTafTimes(fakePublishedTaf.taf, 'DRAFT_CORRECT'),
      ).toEqual({
        ...fakePublishedTaf.taf,
        previousValidDateStart: fakePublishedTaf.taf.validDateStart,
        previousValidDateEnd: fakePublishedTaf.taf.validDateEnd,
      });
    });
    it('should add previousStart and End for AMEND and DRAFT_AMEND AND ALSO UPDATE VALID START', () => {
      const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
      const validObj = {
        start: now,
        end: fakePublishedTaf.taf.baseForecast.valid.end,
      };
      jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
      expect(
        getAmendedCorrectedTafTimes(fakePublishedTaf.taf, 'AMEND'),
      ).toEqual({
        ...fakePublishedTaf.taf,
        previousValidDateStart: fakePublishedTaf.taf.validDateStart,
        previousValidDateEnd: fakePublishedTaf.taf.validDateEnd,
        validDateStart: now,
        baseForecast: { ...fakePublishedTaf.taf.baseForecast, valid: validObj },
      });
      expect(
        getAmendedCorrectedTafTimes(fakePublishedTaf.taf, 'DRAFT_AMEND'),
      ).toEqual({
        ...fakePublishedTaf.taf,
        previousValidDateStart: fakePublishedTaf.taf.validDateStart,
        previousValidDateEnd: fakePublishedTaf.taf.validDateEnd,
        validDateStart: now,
        baseForecast: { ...fakePublishedTaf.taf.baseForecast, valid: validObj },
      });
    });
    it('should not update previous dates if there are already previous dates for DRAFT_AMENDMENT', () => {
      const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
      const validObj = {
        start: now,
        end: fakeDraftAmendmentTaf.taf.baseForecast.valid.end,
      };
      jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
      expect(
        getAmendedCorrectedTafTimes(fakeDraftAmendmentTaf.taf, 'DRAFT_AMEND'),
      ).toEqual({
        ...fakeDraftAmendmentTaf.taf,
        previousValidDateStart:
          fakeDraftAmendmentTaf.taf.previousValidDateStart,
        previousValidDateEnd: fakeDraftAmendmentTaf.taf.previousValidDateEnd,
        validDateStart: now,
        baseForecast: {
          ...fakeDraftAmendmentTaf.taf.baseForecast,
          valid: validObj,
        },
      });
    });
    it('should update previous dates if there are already previous dates for AMENDMENT', () => {
      const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
      const validObj = {
        start: now,
        end: fakeAmendmentTaf.taf.baseForecast.valid.end,
      };
      jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
      expect(
        getAmendedCorrectedTafTimes(fakeAmendmentTaf.taf, 'AMEND'),
      ).toEqual({
        ...fakeAmendmentTaf.taf,
        previousValidDateStart: fakeAmendmentTaf.taf.validDateStart,
        previousValidDateEnd: fakeAmendmentTaf.taf.validDateEnd,
        validDateStart: now,
        baseForecast: {
          ...fakeAmendmentTaf.taf.baseForecast,
          valid: validObj,
        },
      });
    });
  });
  describe('prepareTafValues', () => {
    it('should add an empty changeGroup', () => {
      expect(fakeNewTaf.taf.changeGroups).toBeUndefined();
      expect(
        prepareTafValues(fakeNewTaf.taf, 'DRAFT', true).changeGroups,
      ).toEqual([emptyChangegroup]);
    });

    it('should not add an empty changeGroup', () => {
      expect(fakeNewTaf.taf.changeGroups).toBeUndefined();
      expect(
        prepareTafValues(fakeNewTaf.taf, 'DRAFT', false).changeGroups,
      ).toBeUndefined();
      expect(
        prepareTafValues(fakeNewTaf.taf, 'DRAFT').changeGroups,
      ).toBeUndefined();
    });

    it('should add empty values for baseForecast keys', () => {
      const testTaf = {
        ...fakeNewTaf.taf,
        baseForecast: {},
      } as Taf;

      expect(prepareTafValues(testTaf, 'DRAFT', false).baseForecast).toEqual(
        emptyBaseForecast,
      );

      const testTaf2 = {
        ...fakeNewTaf.taf,
        baseForecast: {
          valid: {
            start: '2020-12-07T06:00:00Z',
            end: '2020-12-08T12:00:00Z',
          },
          wind: { direction: 50, speed: 5, unit: 'KT' },
          visibility: { range: 500, unit: 'M' },
        },
      } as Taf;

      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.visibility,
      ).toEqual(formatFormValues(testTaf2.baseForecast).visibility);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.valid,
      ).toEqual(formatFormValues(testTaf2.baseForecast).valid);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.wind,
      ).toEqual(formatFormValues(testTaf2.baseForecast).wind);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.weather
          .weather1,
      ).toEqual(emptyBaseForecast.weather.weather1);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.weather
          .weather2,
      ).toEqual(emptyBaseForecast.weather.weather2);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.weather
          .weather3,
      ).toEqual(emptyBaseForecast.weather.weather3);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.cloud.cloud1,
      ).toEqual(emptyBaseForecast.cloud.cloud1);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.cloud.cloud2,
      ).toEqual(emptyBaseForecast.cloud.cloud2);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.cloud.cloud3,
      ).toEqual(emptyBaseForecast.cloud.cloud3);
      expect(
        prepareTafValues(testTaf2, 'DRAFT', false).baseForecast.cloud.cloud4,
      ).toEqual(emptyBaseForecast.cloud.cloud4);

      const testTaf3 = {
        ...fakeNewTaf.taf,
        baseForecast: {
          valid: {
            start: '2020-12-07T06:00:00Z',
            end: '2020-12-08T12:00:00Z',
          },
          wind: { direction: 50, speed: 5, unit: 'KT' },
          visibility: { range: 500, unit: 'M' },
          cloud: {
            cloud1: {
              coverage: 'FEW',
              height: 1,
            },
          },
          weather: {
            weather1: 'RA',
          },
        },
      } as Taf;

      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.visibility,
      ).toEqual(formatFormValues(testTaf3.baseForecast).visibility);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.valid,
      ).toEqual(formatFormValues(testTaf3.baseForecast).valid);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.wind,
      ).toEqual(formatFormValues(testTaf3.baseForecast).wind);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.weather
          .weather1,
      ).toEqual(formatFormValues(testTaf3.baseForecast).weather.weather1);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.weather
          .weather2,
      ).toEqual(emptyBaseForecast.weather.weather2);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.weather
          .weather3,
      ).toEqual(emptyBaseForecast.weather.weather3);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.cloud.cloud1,
      ).toEqual(formatFormValues(testTaf3.baseForecast).cloud.cloud1);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.cloud.cloud2,
      ).toEqual(emptyBaseForecast.cloud.cloud2);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.cloud.cloud3,
      ).toEqual(emptyBaseForecast.cloud.cloud3);
      expect(
        prepareTafValues(testTaf3, 'DRAFT', false).baseForecast.cloud.cloud4,
      ).toEqual(emptyBaseForecast.cloud.cloud4);
    });
  });
  describe('getIsFormDefaultDisabled', () => {
    it('should return true for a published or expired taf', () => {
      expect(getIsFormDefaultDisabled('AMENDED')).toBeTruthy();
      expect(getIsFormDefaultDisabled('CANCELLED')).toBeTruthy();
      expect(getIsFormDefaultDisabled('CORRECTED')).toBeTruthy();
      expect(getIsFormDefaultDisabled('EXPIRED')).toBeTruthy();
      expect(getIsFormDefaultDisabled('PUBLISHED')).toBeTruthy();
    });

    it('should return false for a new or draft taf', () => {
      expect(getIsFormDefaultDisabled('NEW')).toBeFalsy();
      expect(getIsFormDefaultDisabled('DRAFT')).toBeFalsy();
      expect(getIsFormDefaultDisabled('DRAFT_AMENDED')).toBeFalsy();
      expect(getIsFormDefaultDisabled('DRAFT_CORRECTED')).toBeFalsy();
    });
  });
});
