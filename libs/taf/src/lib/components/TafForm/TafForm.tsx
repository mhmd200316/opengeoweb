/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Box, Grid, Typography } from '@mui/material';
import { useFieldArray, useFormContext } from 'react-hook-form';
import { ReactSortable, Sortable } from 'react-sortablejs';
import { isEqual } from 'lodash';

import { useAuthenticationContext } from '@opengeoweb/authentication';
import { ReactHookFormHiddenInput } from '@opengeoweb/form-fields';
import {
  useConfirmationDialog,
  useDraftFormHelpers,
  useIsMounted,
  usePreventBrowserClose,
} from '@opengeoweb/shared';
import TafFormRow from './TafFormRow/TafFormRow';
import {
  BaseTaf,
  TafActions,
  TafFormData,
  TafFromBackend,
  TafFromFrontEnd,
} from '../../types';
import TafFormButtons from './TafFormButtons/TafFormButtons';
import {
  convertTafValuesToObject,
  getConfirmationDialogButtonLabel,
  getConfirmationDialogContent,
  getConfirmationDialogTitle,
  useDragOrder,
  SortableField,
  isTafAmendedCorrectedChanged,
  getFieldNames,
  emptyChangegroup,
  prepareTafValues,
  getIsFormDefaultDisabled,
  fromStatusToAction,
  TAC_NOT_AVAILABLE,
} from './utils';
import DragHandle from './TafFormRow/DragHandle';
import { IssuesButton } from '../IssuesPane';
import { IssuesPanePosition } from '../IssuesPane/types';
import { useTafModuleContext } from '../TafModule/TafModuleProvider';
import { SwitchButton } from '../SwitchButton';
import { TafAvatar } from '../TafAvatar';

// prevents default functionality
const noop = (): void => {};

export interface TafFormProps {
  isDisabled?: boolean;
  setIsDisabled?: (isDisabled: boolean) => void;
  tafFromBackend: TafFromBackend;
  onFormAction?: (action: TafActions, formValues?: BaseTaf) => Promise<boolean>;
  isIssuesPaneOpen?: boolean;
  setIsIssuesPaneOpen?: (
    isOpen: boolean,
    position?: IssuesPanePosition,
  ) => void;
  onSwitchEditor?: (formValues: TafFromFrontEnd) => Promise<boolean>;
}

const TafForm: React.FC<TafFormProps> = React.memo(
  ({
    tafFromBackend,
    onFormAction = (): Promise<boolean> => null,
    isIssuesPaneOpen = false,
    setIsIssuesPaneOpen = (): void => null,
    isDisabled = false,
    setIsDisabled = (): void => null,
    onSwitchEditor = (): Promise<boolean> => null,
  }: TafFormProps) => {
    const { taf: originalTaf, editor } = tafFromBackend;
    const { auth } = useAuthenticationContext();
    const isEditor = editor === auth?.username || false;

    const isDefaultDisabled = getIsFormDefaultDisabled(originalTaf.status);

    const confirmDialog = useConfirmationDialog();
    const { toggleIsDraft, DraftFieldHelper } = useDraftFormHelpers(true);

    const {
      registerValidateForm,
      tafAction,
      setIsFormDirty,
      setTafAction,
      TAC,
      updateTAC,
      setTAC,
    } = useTafModuleContext();

    const { isMounted } = useIsMounted();
    const {
      control,
      handleSubmit,
      trigger,
      formState,
      getValues,
      setValue,
      reset,
    } = useFormContext();

    const { isDirty: isFormDirty, errors } = formState;

    usePreventBrowserClose(isFormDirty && !isDisabled);

    const hiddenInputFields = [
      'baseTime',
      'issueDate',
      'type',
      'uuid',
      'validDateStart',
      'validDateEnd',
      'status',
      'previousId',
      'previousValidDateStart',
      'previousValidDateEnd',
    ];

    const [delayedFormValues, setDelayedFormValues] = React.useState(
      getValues() as TafFormData,
    );

    const hasErrors = Object.keys(errors).length;

    const onFormBlur = (): void => {
      if (!isEqual(getValues(), delayedFormValues)) {
        setDelayedFormValues(getValues() as TafFormData);
      }
    };

    const resetTAC = (): void => {
      if (originalTaf.status !== 'NEW') {
        updateTAC(originalTaf);
      } else {
        setTAC(TAC_NOT_AVAILABLE);
      }
    };

    React.useEffect(() => {
      if (isMounted.current && isFormDirty) {
        const newTAC = hasErrors
          ? null
          : convertTafValuesToObject(delayedFormValues);

        updateTAC(newTAC);
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [delayedFormValues]);

    React.useEffect(() => {
      // prevent updating an error while typing
      if (
        !hasErrors &&
        isFormDirty &&
        document.activeElement.tagName !== 'INPUT'
      ) {
        updateTAC(convertTafValuesToObject(delayedFormValues));
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [hasErrors]);

    // Needed to be able to show a confirmation dialog on switching tabs when form is edited and not saved
    React.useEffect(() => {
      setIsFormDirty(isFormDirty);
    }, [isFormDirty, setIsFormDirty]);

    const onSubmit = (data: TafFormData): void => {
      // eslint-disable-next-line no-console
      console.log('submit:', convertTafValuesToObject(data));
    };

    const showConfirmDialog = (action: TafActions): Promise<void> =>
      confirmDialog({
        title: getConfirmationDialogTitle(action),
        description: getConfirmationDialogContent(action, () =>
          isTafAmendedCorrectedChanged(
            isFormDirty,
            originalTaf,
            convertTafValuesToObject(getValues() as TafFormData),
          ),
        ),
        confirmLabel: getConfirmationDialogButtonLabel(action),
      });

    const resetFormValues = (
      action: TafActions = undefined,
      shouldAddEmptyChangGroup: boolean,
    ): void => {
      const newTaf = prepareTafValues(
        originalTaf,
        action,
        shouldAddEmptyChangGroup,
      );
      reset(newTaf);
    };

    const resetForm = (
      action: TafActions = undefined,
      resetDisabled: boolean,
    ): void => {
      const shouldAddEmptyChangGroup =
        !(originalTaf && originalTaf.changeGroups) && !resetDisabled;

      resetFormValues(action, shouldAddEmptyChangGroup);
      setIsDisabled(resetDisabled);
      setTafAction(action);
    };

    const setIsEditor = async (checked: boolean): Promise<boolean> => {
      const editor = checked ? auth?.username : '';
      if (editor === tafFromBackend.editor) {
        // do nothing, correct editor is already set
        return true;
      }
      if (
        tafFromBackend.editor !== '' &&
        tafFromBackend.editor !== null &&
        tafFromBackend.editor !== auth?.username
      ) {
        await confirmDialog({
          title: 'Switch to editor mode',
          description:
            'This TAF is already in edit mode and important information could get lost in the switching process.',
          confirmLabel: 'Edit mode',
        });
      }

      const didSucceed = await onSwitchEditor({
        ...tafFromBackend,
        editor,
      });
      return didSucceed;
    };

    const onSwitchMode = (event: React.ChangeEvent<HTMLInputElement>): void => {
      const { checked } = event.target;
      if (!checked && isFormDirty) {
        confirmDialog({
          title: 'Whoops',
          description:
            'Are you sure you want to switch to viewer mode? Your unsaved changes will be lost.',
          confirmLabel: 'Switch mode',
        }).then(async () => {
          const didSucceed = await setIsEditor(checked);
          if (didSucceed) {
            resetForm(undefined, isDefaultDisabled);
            resetTAC();
          }
        });
      } else {
        setIsEditor(checked);
      }
    };

    const onTafEditModeButtonPress = (action: TafActions): void => {
      switch (action) {
        case 'DRAFT':
        case 'DRAFT_AMEND':
        case 'DRAFT_CORRECT':
          toggleIsDraft(true);
          handleSubmit((data: TafFormData) => {
            onFormAction(action, convertTafValuesToObject(data));
            reset(getValues());
          })();
          break;
        case 'DISCARD':
          if (isFormDirty) {
            showConfirmDialog(action).then(() => {
              resetForm(undefined, isDefaultDisabled);
              resetTAC();
            });
          } else {
            resetForm(undefined, isDefaultDisabled);
          }
          break;
        case 'AMEND':
        case 'CORRECT':
        case 'PUBLISH':
          toggleIsDraft(false);
          handleSubmit(async (data: TafFormData) => {
            await showConfirmDialog(action);
            const didSucceed = await onFormAction(
              action,
              convertTafValuesToObject(data),
            );
            if (didSucceed) {
              setIsDisabled(true);
              reset(
                prepareTafValues(convertTafValuesToObject(data), action, false),
              );
              setTafAction(null);
            }
          })();
          break;
        default:
          onFormAction(action);
          break;
      }
    };

    const onTafViewModeButtonPress = (action: TafActions): void => {
      switch (action) {
        case 'CANCEL':
          showConfirmDialog(action).then(async () => {
            await onFormAction(
              action,
              convertTafValuesToObject(prepareTafValues(originalTaf, action)),
            );
          });
          break;
        default:
          resetForm(action, false);
          break;
      }
    };

    const { fields, insert, remove, move } = useFieldArray({
      control,
      name: 'changeGroups',
    });

    const { onChangeOrder, onStartDrag, activeDraggingIndex } = useDragOrder(
      trigger,
      move,
      onFormBlur,
    );

    const onRemoverow = (index: number): void => {
      confirmDialog({
        title: 'Delete row',
        description: 'Are you sure you want to delete this row?',
        confirmLabel: 'Yes',
      }).then(() => {
        remove(index);
        trigger(); // trigger all fields validation
        onFormBlur();
      });
    };

    // TODO: https://gitlab.com/opengeoweb/opengeoweb/-/issues/726 insert (react-hook-form) does not work well with focus (only last row will get focus), so added this dirty solution. This can be fixed in a better way in react-hook-form v7
    const focusFirstInput = (rowIndex: number): void => {
      const selector =
        rowIndex === -1
          ? `[name="baseForecast.wind"]`
          : `[name="changeGroups[${rowIndex}].probability"]`;

      const nextRowFirstInput: HTMLElement = document.querySelector(selector);
      if (nextRowFirstInput) {
        nextRowFirstInput.focus();
      }
    };

    // adding rows and validation don't work well combined, with always skipping validation of fields of rows above the one the inserted row. A setTimeout prevents this issue
    const setFocusAndValidate = (rowIndex: number): NodeJS.Timeout =>
      setTimeout(() => {
        focusFirstInput(rowIndex);
        trigger(); // trigger all fields validation
      }, 0);

    const onAddRow = (rowIndex = -1, above = false): void => {
      const newRowIndex = above ? rowIndex : rowIndex + 1;
      insert(newRowIndex, emptyChangegroup, false);
      setFocusAndValidate(newRowIndex);
    };

    const onClearRow = (rowIndex = -1, isChangeGroup = false): void => {
      confirmDialog({
        title: 'Clear row',
        description: 'Are you sure you want to clear this row?',
        confirmLabel: 'Yes',
      }).then(() => {
        if (isChangeGroup) {
          remove(rowIndex);
          insert(rowIndex, emptyChangegroup, false);
        } else {
          const baseForecastFields = getFieldNames(false, -1);
          setValue(baseForecastFields.wind, '');
          setValue(baseForecastFields.visibility, '');
          setValue(baseForecastFields.weather1, '');
          setValue(baseForecastFields.weather2, '');
          setValue(baseForecastFields.weather3, '');
          setValue(baseForecastFields.cloud1, '');
          setValue(baseForecastFields.cloud2, '');
          setValue(baseForecastFields.cloud3, '');
          setValue(baseForecastFields.cloud4, '');
        }

        setFocusAndValidate(rowIndex);
      });
    };

    const [shouldValidate, setShouldValidate] = React.useState(false);

    React.useEffect(() => {
      setIsDisabled(!isEditor || isDefaultDisabled);
      const defaultValues = prepareTafValues(
        originalTaf,
        undefined,
        !isDefaultDisabled && !originalTaf.changeGroups,
      );
      // set initial form values
      reset(defaultValues);
      // set validate flag to validate after the values have been set
      setShouldValidate(true);

      // register validate method to use in parent components
      const onValidateFormPromise = (): Promise<void | BaseTaf> =>
        new Promise((resolve, reject) => {
          toggleIsDraft(true);
          handleSubmit((formData: TafFormData) => {
            resolve(convertTafValuesToObject(formData));
          })().then((formData): void => {
            if (formData === undefined) {
              reject();
            }
            resolve(formData);
          });
        });

      registerValidateForm(onValidateFormPromise);
      // set default taf action
      setTafAction(fromStatusToAction(originalTaf.status));

      resetTAC();

      return (): void => {
        reset({});
      };
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    React.useEffect(() => {
      const newValues = prepareTafValues(
        originalTaf,
        undefined,
        !isDefaultDisabled && !originalTaf.changeGroups,
      );
      // set new form values when taf values changed
      reset(newValues);
      // reset TAC when taf values changed
      resetTAC();
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [originalTaf]);

    React.useEffect(() => {
      // update disabled state when editor changed
      setIsDisabled(!isEditor || isDefaultDisabled);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [editor]);

    React.useEffect(() => {
      if (shouldValidate && !isDisabled) {
        // trigger validation on all changegroup validity times after form is enabled since they could be outside the new validity period
        const { changeGroups = [] } = originalTaf;
        const validFields = changeGroups.map(
          (_field, index) => `changeGroups[${index}].valid`,
        );

        trigger(validFields);
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [shouldValidate, isDisabled]);

    return (
      <>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            position: 'absolute',
            top: { xs: '20px', sm: '0px' },
            right: { xs: 'calc(50% -  70px)', sm: '8px' },
          }}
        >
          <SwitchButton checked={isEditor} onChange={onSwitchMode} />
          <Box sx={{ width: '16px', marginLeft: 1 }}>
            {editor && <TafAvatar editor={editor} />}
          </Box>
        </Box>
        <form
          data-testid="taf-form"
          onSubmit={handleSubmit(onSubmit)}
          onBlur={onFormBlur}
        >
          <Typography
            sx={{
              fontSize: '20px',
              fontWeight: 500,
              height: '24px',
            }}
          >
            TAC
          </Typography>
          <Typography
            sx={{ fontSize: '14px', margin: '8px 0px' }}
            data-testid="form-tac-message"
          >
            {TAC.split('\n').map((line) => (
              <Box
                component="span"
                key={line}
                data-testid="tafform-tac-message-line"
                sx={{
                  textIndent: '-0.7em',
                  borderRadius: '3px',
                  paddingRight: '0.7em',
                }}
              >
                {line}
              </Box>
            ))}
          </Typography>
          {/* baseforecast  */}
          <TafFormRow
            disabled={isDisabled}
            onAddChangeGroupBelow={onAddRow}
            onClearRow={onClearRow}
          />
          {/* changegroups */}
          <ReactSortable
            tag="div"
            list={fields as SortableField[]}
            setList={noop} // we don't use this function because we order with our own methods
            animation={200}
            onSort={onChangeOrder}
            handle=".handle"
            direction="vertical"
            // hover props
            forceFallback={true}
            onStart={(event): void => {
              onStartDrag(event);
              Sortable.ghost.style.opacity = '1';
            }}
          >
            {fields.map((item, index) => (
              <TafFormRow
                field={item}
                key={item.id}
                index={index}
                disabled={isDisabled}
                onRemoveChangeGroup={onRemoverow}
                onAddChangeGroupBelow={onAddRow}
                onAddChangeGroupAbove={(): void => onAddRow(index, true)}
                onClearRow={(): void => onClearRow(index, true)}
                isChangeGroup
                dragHandle={
                  <DragHandle
                    isDisabled={fields.length === 1 || isDisabled}
                    isSorting={activeDraggingIndex === index}
                    index={index}
                  />
                }
              />
            ))}
          </ReactSortable>

          {!isDisabled && isEditor && (
            <Grid
              container
              justifyContent="space-between"
              alignItems="center"
              sx={{
                marginLeft: 1.5,
                marginTop: 1,
              }}
            >
              <Grid item>
                <IssuesButton
                  errors={errors}
                  isIssuesPaneOpen={isIssuesPaneOpen}
                  setIsIssuesPaneOpen={setIsIssuesPaneOpen}
                />
              </Grid>
            </Grid>
          )}

          {isEditor && (
            <TafFormButtons
              isFormDisabled={isDisabled}
              tafAction={tafAction}
              canBe={tafFromBackend.canbe}
              onTafEditModeButtonPress={onTafEditModeButtonPress}
              onTafViewModeButtonPress={onTafViewModeButtonPress}
            />
          )}

          {hiddenInputFields.map((field) => (
            <ReactHookFormHiddenInput name={field} key={field} />
          ))}
          <DraftFieldHelper />
        </form>
      </>
    );
  },
  (prevProps, nextProps) => {
    // prevent re-render
    return (
      !nextProps.isDisabled &&
      prevProps.tafFromBackend !== nextProps.tafFromBackend &&
      // if editor changed it should rerender
      prevProps.tafFromBackend.editor === nextProps.tafFromBackend.editor
    );
  },
);

export default TafForm;
