/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import { Card, Theme } from '@mui/material';
import {
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
  fakeDraftTafWithSameDatesFixed,
  fakeDraftTafWithFM,
} from '../../utils/mockdata/fakeTafList';

import { TafThemeApiProvider } from '../Providers';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import TafForm from './TafForm';

export default {
  title: 'components/Taf Form',
};

interface WrapperProps {
  children: React.ReactNode;
  theme?: Theme;
}

const Wrapper: React.FC<WrapperProps> = ({ children, theme }: WrapperProps) => (
  <TafThemeApiProvider createApiFunc={createFakeApi} theme={theme}>
    <div style={{ maxWidth: '814px', caretColor: 'transparent' }}>
      <Card variant="outlined" sx={{ padding: '12px' }}>
        {children}
      </Card>
    </div>
  </TafThemeApiProvider>
);

export const NewTafForm = (): React.ReactElement => {
  return (
    <Wrapper>
      <TafForm tafFromBackend={fakeNewFixedTaf} />
    </Wrapper>
  );
};

NewTafForm.storyName = 'New Taf Form (takeSnapshot)';

export const NewTafFormDark = (): React.ReactElement => {
  return (
    <Wrapper theme={darkTheme}>
      <TafForm tafFromBackend={fakeNewFixedTaf} />
    </Wrapper>
  );
};

NewTafFormDark.storyName = 'New Taf Form dark (takeSnapshot)';

export const EditTafForm = (): React.ReactElement => {
  return (
    <Wrapper>
      <TafForm tafFromBackend={fakeDraftFixedTaf} />
    </Wrapper>
  );
};

EditTafForm.storyName = 'Edit Taf Form (takeSnapshot)';

export const EditTafFormDark = (): React.ReactElement => {
  return (
    <Wrapper theme={darkTheme}>
      <TafForm tafFromBackend={fakeDraftFixedTaf} />
    </Wrapper>
  );
};
EditTafFormDark.storyName = 'Edit Taf Form dark (takeSnapshot)';

export const ViewTafForm = (): React.ReactElement => {
  return (
    <Wrapper>
      <TafForm isDisabled tafFromBackend={fakePublishedFixedTaf} />
    </Wrapper>
  );
};

ViewTafForm.storyName = 'View Taf Form (takeSnapshot)';

export const ViewTafFormDark = (): React.ReactElement => {
  return (
    <Wrapper theme={darkTheme}>
      <TafForm isDisabled tafFromBackend={fakePublishedFixedTaf} />
    </Wrapper>
  );
};

ViewTafFormDark.storyName = 'View Taf Form dark (takeSnapshot)';

export const EditTafFormWithMultipleSameStartDates = (): React.ReactElement => {
  return (
    <Wrapper>
      <TafForm tafFromBackend={fakeDraftTafWithSameDatesFixed} />
    </Wrapper>
  );
};

EditTafFormWithMultipleSameStartDates.storyName =
  'Edit Taf Form With Multiple Same Start Dates (takeSnapshot)';

export const EditTafFormWithMultipleSameStartDatesDark =
  (): React.ReactElement => {
    return (
      <Wrapper theme={darkTheme}>
        <TafForm tafFromBackend={fakeDraftTafWithSameDatesFixed} />
      </Wrapper>
    );
  };

EditTafFormWithMultipleSameStartDatesDark.storyName =
  'Edit Taf Form With Multiple Same Start Dates dark (takeSnapshot)';

export const EditTafFormWithFMOverlap = (): React.ReactElement => {
  return (
    <Wrapper>
      <TafForm tafFromBackend={fakeDraftTafWithFM} />
    </Wrapper>
  );
};

EditTafFormWithFMOverlap.storyName =
  'Edit Taf Form With FM Overlap (takeSnapshot)';

export const EditTafFormWithFMOverlapDark = (): React.ReactElement => {
  return (
    <Wrapper theme={darkTheme}>
      <TafForm tafFromBackend={fakeDraftTafWithFM} />
    </Wrapper>
  );
};

EditTafFormWithFMOverlapDark.storyName =
  'Edit Taf Form With FM Overlap dark (takeSnapshot)';
