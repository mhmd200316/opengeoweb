/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import moment from 'moment';

import {
  fakeNewTaf,
  fakeDraftTaf,
  fakeDraftAmendmentTaf,
  fakeDraftCorrectedTaf,
  fakePublishedTafWithoutChangeGroups,
  fakeCancelledTaf,
  fakeAmendmentTaf,
  fakeCorrectedTaf,
} from '../../utils/mockdata/fakeTafList';
import { MOCK_USERNAME, TafThemeApiProvider } from '../Providers';
import {
  convertTafValuesToObject,
  prepareTafValues,
  TAC_NOT_AVAILABLE,
  TAC_RETRIEVING_DATA,
} from './utils';
import { fakeTestTac } from '../../utils/__mocks__/api';
import TafForm from './TafForm';

jest.mock('../../utils/api');

describe('components/TafForm/TafForm - test status changes', () => {
  const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
  jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());

  it('should save and publish a draft taf', async () => {
    const props = {
      tafFromBackend: { ...fakeDraftTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();

    // Wait till TAC has been retrieved
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'DRAFT'),
    );

    fireEvent.click(getByTestId('savedrafttaf'));
    expect(queryByTestId('confirmationDialog')).toBeFalsy();
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'DRAFT',
        expectedSaveResult,
      ),
    );

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedSaveResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );

    fireEvent.click(getByTestId('publishtaf'));
    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'PUBLISH'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'PUBLISH',
      expectedPublishResult,
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedPublishResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });

  it('should save a new taf as draft', async () => {
    const props = {
      tafFromBackend: { ...fakeNewTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    const expectedResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'DRAFT'),
    );

    fireEvent.click(getByTestId('savedrafttaf'));
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith('DRAFT', expectedResult),
    );

    expect(expectedResult.location).toEqual(props.tafFromBackend.taf.location);
    expect(expectedResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });

  it('should publish a new taf', async () => {
    const props = {
      tafFromBackend: { ...fakeNewTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // enter required fields
    await waitFor(() => {
      fireEvent.change(container.querySelector('[name="baseForecast.wind"'), {
        target: { value: '20020' },
      });
      fireEvent.blur(container.querySelector('[name="baseForecast.wind"'));
    });
    await waitFor(() => {
      fireEvent.change(
        container.querySelector('[name="baseForecast.visibility"'),
        {
          target: { value: 'CAVOK' },
        },
      );
      fireEvent.blur(
        container.querySelector('[name="baseForecast.visibility"'),
      );
    });

    const expectedResult = convertTafValuesToObject(
      prepareTafValues(
        {
          ...props.tafFromBackend.taf,
          baseForecast: {
            valid: props.tafFromBackend.taf.baseForecast.valid,
            cavOK: true,
            wind: { direction: 200, speed: 20, unit: 'KT' },
          },
        },
        'PUBLISH',
      ),
    );

    fireEvent.click(getByTestId('publishtaf'));
    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenCalledWith(
        'PUBLISH',
        expectedResult,
      ),
    );

    expect(expectedResult.location).toEqual(props.tafFromBackend.taf.location);
    expect(expectedResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });

  it('should save and publish a draft_corrected taf', async () => {
    const props = {
      tafFromBackend: { ...fakeDraftCorrectedTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'DRAFT'),
    );

    fireEvent.click(getByTestId('savedrafttaf'));
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenLastCalledWith(
        'DRAFT_CORRECT',
        expectedSaveResult,
      ),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'PUBLISH'),
    );

    fireEvent.click(getByTestId('publishtaf'));
    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenLastCalledWith(
        'CORRECT',
        expectedPublishResult,
      ),
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedPublishResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });

  it('should save and publish a draft_amended taf', async () => {
    const props = {
      tafFromBackend: { ...fakeDraftAmendmentTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'DRAFT'),
    );

    fireEvent.click(getByTestId('savedrafttaf'));
    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenLastCalledWith(
        'DRAFT_AMEND',
        expectedSaveResult,
      ),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'PUBLISH'),
    );

    fireEvent.click(getByTestId('publishtaf'));
    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    await waitFor(() =>
      expect(props.onFormAction).toHaveBeenLastCalledWith(
        'AMEND',
        expectedPublishResult,
      ),
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedPublishResult.messageType).toEqual(
      props.tafFromBackend.taf.messageType,
    );
  });

  it('should save a published taf as amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithoutChangeGroups,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('ORG');

    fireEvent.click(getByTestId('amendtaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('publishtaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'AMEND',
      expectedPublishResult,
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedPublishResult.messageType).toEqual('AMD');
  });

  it('should save a published taf as draft_amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithoutChangeGroups,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('ORG');

    fireEvent.click(getByTestId('amendtaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('savedrafttaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'DRAFT_AMEND',
      expectedSaveResult,
    );

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedSaveResult.messageType).toEqual('AMD');
  });

  it('should save a published taf as corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithoutChangeGroups,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('ORG');

    fireEvent.click(getByTestId('correcttaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('publishtaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'CORRECT',
      expectedPublishResult,
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedPublishResult.messageType).toEqual('COR');
  });

  it('should save a published taf as draft_corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithoutChangeGroups,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('ORG');

    fireEvent.click(getByTestId('correcttaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('savedrafttaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'DRAFT_CORRECT',
      expectedSaveResult,
    );

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedSaveResult.messageType).toEqual('COR');
  });

  it('should save a cancelled taf as amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCancelledTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('CNL');

    fireEvent.click(getByTestId('amendtaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('publishtaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'AMEND',
      expectedPublishResult,
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedPublishResult.messageType).toEqual('AMD');
  });

  it('should save a cancelled taf as draft_amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCancelledTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('CNL');

    fireEvent.click(getByTestId('amendtaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('savedrafttaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'DRAFT_AMEND',
      expectedSaveResult,
    );

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedSaveResult.messageType).toEqual('AMD');
  });

  it('should save an amended taf as amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeAmendmentTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    fireEvent.click(getByTestId('amendtaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('publishtaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'AMEND',
      expectedPublishResult,
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedPublishResult.messageType).toEqual('AMD');
  });

  it('should save an amended taf as draft_amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeAmendmentTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    fireEvent.click(getByTestId('amendtaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('savedrafttaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'DRAFT_AMEND',
      expectedSaveResult,
    );

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedSaveResult.messageType).toEqual('AMD');
  });

  it('should save a corrected taf as amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCorrectedTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('COR');

    fireEvent.click(getByTestId('amendtaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('publishtaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'AMEND',
      expectedPublishResult,
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedPublishResult.messageType).toEqual('AMD');
  });

  it('should save a corrected taf as draft_amended', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCorrectedTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('COR');

    fireEvent.click(getByTestId('amendtaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('savedrafttaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'AMEND'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'DRAFT_AMEND',
      expectedSaveResult,
    );

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedSaveResult.messageType).toEqual('AMD');
  });

  it('should save an amended taf as corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeAmendmentTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    fireEvent.click(getByTestId('correcttaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('publishtaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'CORRECT',
      expectedPublishResult,
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedPublishResult.messageType).toEqual('COR');
  });

  it('should save an amended taf as draft_corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeAmendmentTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    fireEvent.click(getByTestId('correcttaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('savedrafttaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'DRAFT_CORRECT',
      expectedSaveResult,
    );

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedSaveResult.messageType).toEqual('COR');
  });

  it('should save a corrected taf as corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCorrectedTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('COR');

    fireEvent.click(getByTestId('correcttaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('publishtaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    const expectedPublishResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'CORRECT',
      expectedPublishResult,
    );

    expect(expectedPublishResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedPublishResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedPublishResult.messageType).toEqual('COR');
  });

  it('should save a corrected taf as draft_corrected', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCorrectedTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('COR');

    fireEvent.click(getByTestId('correcttaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('COR');

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('savedrafttaf'));
    });

    // check no errors
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    const expectedSaveResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CORRECT'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'DRAFT_CORRECT',
      expectedSaveResult,
    );

    expect(expectedSaveResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedSaveResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedSaveResult.messageType).toEqual('COR');
  });

  it('should cancel a published taf', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithoutChangeGroups,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('ORG');

    fireEvent.click(getByTestId('canceltaf'));

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    const expectedCancelResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CANCEL'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'CANCEL',
      expectedCancelResult,
    );

    expect(expectedCancelResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedCancelResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedCancelResult.messageType).toEqual('CNL');
  });

  it('should cancel an amended taf', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeAmendmentTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    fireEvent.click(getByTestId('canceltaf'));

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    const expectedCancelResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CANCEL'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'CANCEL',
      expectedCancelResult,
    );

    expect(expectedCancelResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedCancelResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedCancelResult.messageType).toEqual('CNL');
  });

  it('should cancel a corrected taf', async () => {
    const props = {
      isDisabled: true,
      tafFromBackend: { ...fakeCorrectedTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('COR');

    fireEvent.click(getByTestId('canceltaf'));

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );

    const expectedCancelResult = convertTafValuesToObject(
      prepareTafValues(props.tafFromBackend.taf, 'CANCEL'),
    );

    expect(props.onFormAction).toHaveBeenCalledWith(
      'CANCEL',
      expectedCancelResult,
    );

    expect(expectedCancelResult.location).toEqual(
      props.tafFromBackend.taf.location,
    );
    expect(expectedCancelResult.type).toEqual(props.tafFromBackend.taf.type);
    expect(expectedCancelResult.messageType).toEqual('CNL');
  });
});
