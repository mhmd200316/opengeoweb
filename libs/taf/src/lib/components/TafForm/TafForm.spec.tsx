/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import moment from 'moment';
import { getInitials, showPrompt } from '@opengeoweb/shared';

import {
  fakeNewTaf,
  fakeExpiredTaf,
  fakePublishedTaf,
  fakeDraftTaf,
  fakeDraftAmendmentTaf,
  fakeDraftTafWithSameDates,
  fakePublishedTafWithChangeGroups,
  fakeDraftTafWithFM,
} from '../../utils/mockdata/fakeTafList';
import { TafFromBackend } from '../../types';
import { MOCK_USERNAME, TafThemeApiProvider } from '../Providers';
import {
  TAC_MISSING_DATA,
  TAC_NOT_AVAILABLE,
  TAC_RETRIEVING_DATA,
} from './utils';
import { WeatherPhenomena } from '../../utils/weatherPhenomena';
import { fakeTestTac } from '../../utils/__mocks__/api';
import { FORMAT_DAYS_HOURS_MINUTES } from './TafFormRow/validations/validField';
import TafForm from './TafForm';

jest.mock('../../utils/api');

describe('components/TafForm/TafForm', () => {
  it('should display the TAC for a new TAF', async () => {
    const { getByText, getByTestId, findByText } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={fakeNewTaf} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    expect(getByText('TAC')).toBeTruthy();
    expect(getByTestId('form-tac-message')).toBeTruthy();
  });

  it('should display correct TAF lifecycle', async () => {
    const { getByText, getByTestId, findByText, container } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();
    expect(getByText('TAC')).toBeTruthy();
    expect(getByTestId('form-tac-message')).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // fill in incomplete data
    await waitFor(() => {
      fireEvent.change(container.querySelector('[name="baseForecast.wind"'), {
        target: { value: '01015G35' },
      });
      fireEvent.blur(container.querySelector('[name="baseForecast.wind"'));
    });

    expect(await findByText(TAC_MISSING_DATA)).toBeTruthy();
    // fill in to complete the data
    await waitFor(() => {
      fireEvent.change(
        container.querySelector('[name="baseForecast.visibility"'),
        {
          target: { value: 'CAVOK' },
        },
      );
      fireEvent.blur(
        container.querySelector('[name="baseForecast.visibility"'),
      );
    });

    expect(await findByText(fakeTestTac)).toBeTruthy();
  });

  it('should display baseforecast for a new TAF and one empty change group', async () => {
    const { getAllByTestId, getByTestId, queryByTestId, findByText } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    expect(getAllByTestId('row-baseForecast').length).toEqual(1);
    expect(getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[1]')).toBeFalsy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(getAllByTestId('row-baseForecast').length).toEqual(1);
    expect(getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[1]')).toBeFalsy();
  });

  it('should show all inputs as disabled for an expired taf', async () => {
    const { getAllByRole, findByText, getByTestId, rerender } = render(
      <TafThemeApiProvider>
        <TafForm isDisabled tafFromBackend={fakeExpiredTaf} />
      </TafThemeApiProvider>,
    );

    // Wait till TAC has been retrieved
    expect(await findByText(fakeTestTac)).toBeTruthy();

    const allInputs = getAllByRole('textbox');
    expect(allInputs.length).toBeGreaterThan(0);
    allInputs.forEach((input) => {
      expect(input.classList).toContain('Mui-disabled');
    });

    // check editor mode
    rerender(
      <TafThemeApiProvider>
        <TafForm
          isDisabled
          tafFromBackend={{ ...fakeExpiredTaf, editor: MOCK_USERNAME }}
        />
      </TafThemeApiProvider>,
    );
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    const allInputs2 = getAllByRole('textbox');
    expect(allInputs2.length).toBeGreaterThan(0);
    allInputs2.forEach((input) => {
      expect(input.classList).toContain('Mui-disabled');
    });
  });

  it('should show only the first four inputs as disabled for a draft taf', async () => {
    const { getAllByRole, findByText, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );

    // Wait till TAC has been retrieved
    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    const allInputs = getAllByRole('textbox');
    expect(allInputs.length).toBeGreaterThan(0);
    allInputs.forEach((input, index) => {
      if (index < 4) {
        expect(input.classList).toContain('Mui-disabled');
      } else {
        expect(input.classList).not.toContain('Mui-disabled');
      }
    });
  });

  it('should show only the first four inputs as disabled for a new TAF', async () => {
    const { getAllByRole, findByText, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    const allInputs = getAllByRole('textbox');
    expect(allInputs.length).toBeGreaterThan(0);
    allInputs.forEach((input, index) => {
      if (index < 4) {
        expect(input.classList).toContain('Mui-disabled');
      } else {
        expect(input.classList).not.toContain('Mui-disabled');
      }
    });
  });

  it('should render changeGroups if passed in (and not add an empty ones to the end)', async () => {
    const { getByTestId, queryByTestId, findByText } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={fakePublishedTaf} />
      </TafThemeApiProvider>,
    );

    // Wait till TAC has been retrieved
    expect(await findByText(fakeTestTac)).toBeTruthy();

    expect(getByTestId('row-baseForecast')).toBeTruthy();
    expect(getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(getByTestId('row-changeGroups[1]')).toBeTruthy();
    // No empty ones at the end
    expect(queryByTestId('row-changeGroups[2]')).toBeFalsy();
  });

  it('should add an empty changegroup to the end if no change groups are passed in in edit mode', async () => {
    const { getByTestId, queryByTestId, findByText } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    expect(getByTestId('row-baseForecast')).toBeTruthy();
    expect(getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[1]')).toBeFalsy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(getByTestId('row-baseForecast')).toBeTruthy();
    expect(getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[1]')).toBeFalsy();
  });

  it('should add a changeGroup below or above', async () => {
    const { queryByTestId, findByText, queryByText, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // should focus on baseForecast wind
    expect(document.activeElement.getAttribute('name')).toEqual(
      'baseForecast.wind',
    );
    expect(queryByTestId('row-baseForecast')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[1]')).toBeFalsy();
    expect(queryByTestId('row-changeGroups[2]')).toBeFalsy();

    // add row via baseforecast add changegroup button
    fireEvent.click(queryByTestId('tafFormOptions[-1]'));
    fireEvent.click(queryByText('Insert 1 row below'));

    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[2]')).toBeFalsy();
      expect(queryByTestId('row-changeGroups[3]')).toBeFalsy();
      // should focus probability new added changegroup
      expect(document.activeElement.getAttribute('name')).toEqual(
        'changeGroups[0].probability',
      );
    });

    // add row on first changegroup row
    fireEvent.click(queryByTestId('tafFormOptions[0]'));
    fireEvent.click(queryByText('Insert 1 row below'));

    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[2]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[3]')).toBeFalsy();
      // should focus probability new added changegroup
      expect(document.activeElement.getAttribute('name')).toEqual(
        'changeGroups[1].probability',
      );
    });

    // add row above second changegroup row
    fireEvent.click(queryByTestId('tafFormOptions[1]'));
    fireEvent.click(queryByText('Insert 1 row above'));

    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[2]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[3]')).toBeTruthy();
      // should focus probabilty new added changegroup
      expect(document.activeElement.getAttribute('name')).toEqual(
        'changeGroups[1].probability',
      );
    });
  });

  it('should remove a changeGroup', async () => {
    const { getByTestId, queryByTestId, findByText, container, queryByText } =
      render(
        <TafThemeApiProvider>
          <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
        </TafThemeApiProvider>,
      );
    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // verify rows
    expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[1]')).toBeFalsy();

    // add rows
    const optionsButton = getByTestId('tafFormOptions[0]');

    fireEvent.click(optionsButton);
    fireEvent.click(queryByText('Insert 1 row below'));
    fireEvent.click(optionsButton);
    fireEvent.click(queryByText('Insert 1 row below'));
    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[2]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[3]')).toBeFalsy();
    });

    // set values
    const row1 = container.querySelector('[name="changeGroups[0].change"]');
    const row2 = container.querySelector('[name="changeGroups[1].change"]');
    const row3 = container.querySelector('[name="changeGroups[2].change"]');
    fireEvent.change(row1, { target: { value: 1 } });
    fireEvent.change(row2, { target: { value: 2 } });
    fireEvent.change(row3, { target: { value: 3 } });
    await waitFor(() => {
      expect(row1.getAttribute('value')).toEqual('1');
      expect(row2.getAttribute('value')).toEqual('2');
      expect(row3.getAttribute('value')).toEqual('3');
    });

    // remove middle
    await waitFor(() => {
      fireEvent.click(getByTestId('tafFormOptions[1]'));
      fireEvent.click(queryByText('Delete row'));
      fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    });
    await waitFor(() => {
      expect(
        container
          .querySelector('[name="changeGroups[0].change"]')
          .getAttribute('value'),
      ).toEqual('1');
      expect(
        container
          .querySelector('[name="changeGroups[1].change"]')
          .getAttribute('value'),
      ).toEqual('3');
      expect(queryByTestId('row-changeGroups[2]')).toBeFalsy();
    });

    // remove first
    await waitFor(() => {
      fireEvent.click(getByTestId('tafFormOptions[0]'));
      fireEvent.click(queryByText('Delete row'));
      fireEvent.click(getByTestId('confirmationDialog-confirm'));
    });

    await waitFor(() => {
      expect(
        container
          .querySelector('[name="changeGroups[0].change"]')
          .getAttribute('value'),
      ).toEqual('3');
      expect(queryByTestId('row-changeGroups[1]')).toBeFalsy();
    });

    // remove last removeable row
    await waitFor(() => {
      fireEvent.click(getByTestId('tafFormOptions[0]'));
      fireEvent.click(queryByText('Delete row'));
      fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    });
    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeFalsy();
    });
  });

  it('should clear a row', async () => {
    const { getByTestId, container, queryByText } = render(
      <TafThemeApiProvider>
        <TafForm
          tafFromBackend={{ ...fakeDraftAmendmentTaf, editor: MOCK_USERNAME }}
        />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // verify data
    await waitFor(() => {
      expect(
        container
          .querySelector('[name="changeGroups[0].change"]')
          .getAttribute('value'),
      ).toEqual(fakeDraftAmendmentTaf.taf.changeGroups[0].change);
      expect(
        container
          .querySelector('[name="baseForecast.weather.weather1"]')
          .getAttribute('value'),
      ).toEqual(fakeDraftAmendmentTaf.taf.baseForecast.weather.weather1);
    });

    // clear changegroup
    await waitFor(() => {
      fireEvent.click(getByTestId('tafFormOptions[0]'));
      fireEvent.click(queryByText('Clear row'));
      fireEvent.click(getByTestId('confirmationDialog-confirm'));
    });

    await waitFor(() => {
      expect(
        container
          .querySelector('[name="changeGroups[0].change"]')
          .getAttribute('value'),
      ).toEqual('');
    });

    // clear baseforecast
    await waitFor(() => {
      fireEvent.click(getByTestId('tafFormOptions[-1]'));
      fireEvent.click(queryByText('Clear row'));
      fireEvent.click(getByTestId('confirmationDialog-confirm'));
    });

    await waitFor(() => {
      expect(
        container
          .querySelector('[name="baseForecast.weather.weather1"]')
          .getAttribute('value'),
      ).toEqual('');
      expect(
        container
          .querySelector('[name="baseForecast.valid"]')
          .getAttribute('value'),
      ).toBeTruthy(); // should not be cleared
    });
  });

  it('should update TAC when adding and removing rows', async () => {
    const mockGetTAC = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeTestTac,
        });
      });
    });

    const { queryByTestId, queryByText, getByTestId } = render(
      <TafThemeApiProvider
        createApiFunc={(): {
          postTaf;
          getTafList;
          getTAC;
        } => {
          return {
            postTaf: jest.fn(),
            getTafList: jest.fn(),
            getTAC: mockGetTAC,
          };
        }}
      >
        <TafForm
          tafFromBackend={{ ...fakePublishedTaf, editor: MOCK_USERNAME }}
        />
      </TafThemeApiProvider>,
    );

    expect(mockGetTAC).toHaveBeenCalledTimes(2);

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // add row
    fireEvent.click(queryByTestId('tafFormOptions[-1]'));
    fireEvent.click(queryByText('Insert 1 row below'));
    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
    });

    // should update TAC when adding a row
    await waitFor(() => {
      expect(mockGetTAC).toHaveBeenCalledTimes(3);
    });

    // delete row
    await waitFor(() => {
      fireEvent.click(queryByTestId('tafFormOptions[1]'));
      fireEvent.click(queryByText('Delete row'));
      fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    });

    // should update TAC when deleting a row
    await waitFor(() => {
      expect(mockGetTAC).toHaveBeenCalledTimes(5);
    });
  });

  it('should keep field errors visible after adding rows', async () => {
    const { queryByTestId, findByText, container, queryByText, getByTestId } =
      render(
        <TafThemeApiProvider>
          <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
        </TafThemeApiProvider>,
      );

    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(queryByTestId('row-baseForecast')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[1]')).toBeFalsy();
    expect(queryByTestId('row-changeGroups[2]')).toBeFalsy();

    const windInput = container.querySelector('[name="changeGroups[0].wind"]');
    expect(windInput.parentElement.classList.contains('Mui-error')).toBeFalsy();
    // fill in invalid value
    fireEvent.change(windInput, { target: { value: '01' } });
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    await waitFor(() => fireEvent.blur(windInput));
    expect(
      windInput.parentElement.classList.contains('Mui-error'),
    ).toBeTruthy();
    expect(container.querySelectorAll('.Mui-error').length).toBeTruthy();

    // add row below baseforecast
    fireEvent.click(queryByTestId('tafFormOptions[-1]'));
    fireEvent.click(queryByText('Insert 1 row below'));

    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[2]')).toBeFalsy();
    });

    // add row at the bottom
    fireEvent.click(queryByTestId('tafFormOptions[1]'));
    fireEvent.click(queryByText('Insert 1 row below'));

    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[2]')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[3]')).toBeFalsy();
    });

    // should have correct error in field
    expect(
      container
        .querySelector('[name="changeGroups[0].wind"]')
        .parentElement.classList.contains('Mui-error'),
    ).toBeFalsy();
    expect(
      container
        .querySelector('[name="changeGroups[1].wind"]')
        .parentElement.classList.contains('Mui-error'),
    ).toBeTruthy();
    expect(
      container
        .querySelector('[name="changeGroups[2].wind"]')
        .parentElement.classList.contains('Mui-error'),
    ).toBeFalsy();
  });

  it('should should be possible to sort rows', async () => {
    const { queryByTestId, findByText, queryByText, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // not able to drag if only 1 row
    expect(queryByTestId('row-baseForecast')).toBeTruthy();
    expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(
      queryByTestId('row-changeGroups[0]')
        .querySelector('[data-testid="dragHandle-0"]')
        .getAttribute('disabled'),
    ).toBeDefined();
    expect(queryByTestId('row-changeGroups[1]')).toBeFalsy();

    // add row
    fireEvent.click(queryByTestId('tafFormOptions[-1]'));
    fireEvent.click(queryByText('Insert 1 row below'));

    await waitFor(() => {
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(
        queryByTestId('row-changeGroups[0]')
          .querySelector('[data-testid="dragHandle-0"]')
          .getAttribute('disabled'),
      ).toBeFalsy();
      expect(queryByTestId('row-changeGroups[1]')).toBeTruthy();
      expect(
        queryByTestId('row-changeGroups[1]')
          .querySelector('[data-testid="dragHandle-1"]')
          .getAttribute('disabled'),
      ).toBeFalsy();
    });

    // remove row
    await waitFor(() => {
      fireEvent.click(queryByTestId('tafFormOptions[1]'));
      fireEvent.click(queryByText('Delete row'));
      fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    });

    await waitFor(() => {
      expect(queryByTestId('row-baseForecast')).toBeTruthy();
      expect(queryByTestId('row-changeGroups[0]')).toBeTruthy();
      expect(
        queryByTestId('row-changeGroups[0]')
          .querySelector('[data-testid="dragHandle-0"]')
          .getAttribute('disabled'),
      ).toBeDefined();
      expect(queryByTestId('row-changeGroups[1]')).toBeFalsy();
    });
  });

  it('should show discard, save and publish buttons for a new taf when user is editor', async () => {
    const { getByTestId, findByText } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    await waitFor(() => {
      expect(getByTestId('discardtaf')).toBeTruthy();
      expect(getByTestId('savedrafttaf')).toBeTruthy();
      expect(getByTestId('publishtaf')).toBeTruthy();
    });
  });

  it('should show discard, save and publish buttons for a draft taf when user is editor', async () => {
    const { getByTestId, findByText } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(fakeTestTac)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    await waitFor(() => {
      expect(getByTestId('discardtaf')).toBeTruthy();
      expect(getByTestId('savedrafttaf')).toBeTruthy();
      expect(getByTestId('publishtaf')).toBeTruthy();
    });
  });

  it('should not show action buttons for expired taf', async () => {
    const { queryByTestId, findByText, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          isDisabled
          tafFromBackend={{ ...fakeExpiredTaf, editor: MOCK_USERNAME }}
        />
      </TafThemeApiProvider>,
    );

    // Wait till TAC has been retrieved
    expect(await findByText(fakeTestTac)).toBeTruthy();

    expect(queryByTestId('discardtaf')).toBeFalsy();
    expect(queryByTestId('savedrafttaf')).toBeFalsy();
    expect(queryByTestId('publishtaf')).toBeFalsy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(queryByTestId('discardtaf')).toBeFalsy();
    expect(queryByTestId('savedrafttaf')).toBeFalsy();
    expect(queryByTestId('publishtaf')).toBeFalsy();
  });

  it('should not show error when typing, but after blur', async () => {
    const { container, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          tafFromBackend={{ ...fakeExpiredTaf, editor: MOCK_USERNAME }}
          onFormAction={jest.fn()}
        />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    const windInput = container.querySelector(
      'input[name="baseForecast.wind"]',
    );

    // fill in invalid value
    fireEvent.change(windInput, { target: { value: '01' } });
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();

    await waitFor(() => fireEvent.blur(windInput));

    expect(container.querySelectorAll('.Mui-error').length).toBeTruthy();
  });

  it('should show browser prompt when making a change and reloading the page', async () => {
    const spy = jest.spyOn(window, 'addEventListener');

    const { container, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }}
          onFormAction={jest.fn()}
        />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // add wind input
    const windInput = container.querySelector(
      'input[name="baseForecast.wind"]',
    );
    fireEvent.change(windInput, { target: { value: '12010' } });

    // reload page
    fireEvent.keyDown(container, { key: 'F5', code: 'F5' });

    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('beforeunload', showPrompt);
    });
  });

  it('should not show browser prompt when not changing anything and reloading the page', async () => {
    const spy = jest.spyOn(window, 'removeEventListener');

    const { container, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          tafFromBackend={{ ...fakeDraftTaf, editor: MOCK_USERNAME }}
          onFormAction={jest.fn()}
        />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // reload page
    fireEvent.keyDown(container, { key: 'F5', code: 'F5' });

    await waitFor(() =>
      expect(spy).toHaveBeenCalledWith('beforeunload', showPrompt),
    );
  });

  it('should show the confirmation dialog when pressing Discard with some changes made', async () => {
    const props = {
      tafFromBackend: { ...fakeNewTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };
    const { getByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // add wind input
    const windInput = container.querySelector(
      'input[name="baseForecast.wind"]',
    );
    fireEvent.change(windInput, { target: { value: '12010' } });

    fireEvent.click(getByTestId('discardtaf'));
    await waitFor(() => expect(getByTestId('confirmationDialog')).toBeTruthy());
    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );
    expect(props.onFormAction).not.toHaveBeenCalled();
    // form should be reset
    expect(
      container
        .querySelector('input[name="baseForecast.wind"]')
        .getAttribute('value'),
    ).toEqual('');
  });

  it('should not show the confirmation dialog when pressing Discard without any changes made', async () => {
    const props = {
      tafFromBackend: { ...fakeNewTaf, editor: MOCK_USERNAME },
      onFormAction: jest.fn(),
    };

    const { getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    await waitFor(() => {
      fireEvent.click(getByTestId('discardtaf'));
    });

    expect(queryByTestId('confirmationDialog')).toBeFalsy();
    expect(props.onFormAction).not.toHaveBeenCalled();
  });

  it('should prevent publishing when required fields (change/valid) have not been entered', async () => {
    const tafWithMissingData = {
      ...fakeDraftTaf,
      editor: MOCK_USERNAME,
      taf: {
        ...fakeDraftTaf.taf,
        validDateStart: '2021-01-26T18:00:00Z',
        validDateEnd: '2021-01-27T23:59:00Z',
        changeGroups: [
          {
            visibility: { range: 6000, unit: 'M' },
            weather: { weather1: '+SHRA' },
            wind: { direction: 10, speed: 15, unit: 'KT', gust: 35 },
            cloud: {
              cloud1: { coverage: 'BKN', height: 40 },
            },
          },
        ],
      },
    } as TafFromBackend;

    const props = {
      tafFromBackend: tafWithMissingData,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    const publishBtn = queryByTestId('publishtaf');

    await waitFor(() => fireEvent.click(publishBtn));

    const changeInput = container.querySelector(
      '[name="changeGroups[0].change"',
    );
    const validInput = container.querySelector('[name="changeGroups[0].valid"');

    expect(
      changeInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    expect(
      validInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    expect(props.onFormAction).toHaveBeenCalledTimes(0);

    // fix errors
    await waitFor(() =>
      fireEvent.change(changeInput, { target: { value: 'BECMG' } }),
    );
    await waitFor(() =>
      fireEvent.change(validInput, { target: { value: '2623/2624' } }),
    );
    await waitFor(() => fireEvent.click(publishBtn));
    await waitFor(() =>
      fireEvent.click(queryByTestId('confirmationDialog-confirm')),
    );
    expect(props.onFormAction).toHaveBeenCalledWith(
      'PUBLISH',
      expect.anything(),
    );
  });

  it('should prevent publishing when required fields (wind, visibility, weather, cloud) have not been entered', async () => {
    const tafWithMissingData = {
      ...fakeDraftTaf,
      editor: MOCK_USERNAME,
      taf: {
        ...fakeDraftTaf.taf,
        changeGroups: [
          {
            change: 'BECMG',
            valid: {
              start: `${moment.utc().format('YYYY-MM-DD')}T23:00:00Z`,
              end: `${moment
                .utc()
                .add(1, 'day')
                .format('YYYY-MM-DD')}T00:00:00Z`,
            },
          },
        ],
      },
    } as TafFromBackend;
    const props = {
      tafFromBackend: tafWithMissingData,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    const publishBtn = queryByTestId('publishtaf');

    await waitFor(() => fireEvent.click(publishBtn));

    const windInput = container.querySelector('[name="changeGroups[0].wind"');
    const visibilityInput = container.querySelector(
      '[name="changeGroups[0].visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="changeGroups[0].weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="changeGroups[0].cloud.cloud1"',
    );

    expect(
      windInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      visibilityInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      weatherInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    expect(props.onFormAction).toHaveBeenCalledTimes(0);

    // fix errors
    await waitFor(() =>
      fireEvent.change(windInput, { target: { value: '01015G35' } }),
    );

    await waitFor(() => fireEvent.click(publishBtn));
    await waitFor(() =>
      fireEvent.click(queryByTestId('confirmationDialog-confirm')),
    );
    expect(props.onFormAction).toHaveBeenCalledWith(
      'PUBLISH',
      expect.anything(),
    );
  });

  it('should show errors on required fields when change = FM', async () => {
    const props = {
      tafFromBackend: {
        ...fakeNewTaf,
        editor: MOCK_USERNAME,
        taf: {
          ...fakeNewTaf.taf,
          changeGroups: [
            {
              change: 'FM',
              valid: {
                start: `${moment.utc().format('YYYY-MM-DD')}T23:00:00Z`,
              },
            },
          ],
        },
      } as TafFromBackend,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container, findByText } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    const publishBtn = queryByTestId('publishtaf');

    await waitFor(() => fireEvent.click(publishBtn));

    const windInput = container.querySelector('[name="changeGroups[0].wind"');
    const visibilityInput = container.querySelector(
      '[name="changeGroups[0].visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="changeGroups[0].weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="changeGroups[0].cloud.cloud1"',
    );

    expect(
      windInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      visibilityInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      weatherInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    expect(props.onFormAction).toHaveBeenCalledTimes(0);

    // update required fields
    await waitFor(() => {
      fireEvent.change(container.querySelector('[name="baseForecast.wind"'), {
        target: { value: '01015G35' },
      });
      fireEvent.blur(container.querySelector('[name="baseForecast.wind"'));
    });
    await waitFor(() => {
      fireEvent.change(
        container.querySelector('[name="baseForecast.visibility"'),
        {
          target: { value: 'CAVOK' },
        },
      );
      fireEvent.blur(
        container.querySelector('[name="baseForecast.visibility"'),
      );
    });

    expect(await findByText(TAC_MISSING_DATA)).toBeTruthy();

    // fix wind
    await waitFor(() => {
      fireEvent.change(windInput, { target: { value: '01015G35' } });
      fireEvent.blur(windInput);
    });

    await waitFor(() =>
      expect(
        windInput.closest('div').className.includes('Mui-error'),
      ).toBeFalsy(),
    );
    expect(
      visibilityInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      weatherInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    // fix visibility
    await waitFor(() => {
      fireEvent.change(visibilityInput, { target: { value: '0500' } });
      fireEvent.blur(visibilityInput);
    });
    expect(
      visibilityInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      weatherInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    // fix weather
    await waitFor(() => {
      fireEvent.change(weatherInput, { target: { value: '+SHRA' } });
      fireEvent.blur(weatherInput);
    });
    expect(
      weatherInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    // fix clouds
    await waitFor(() => {
      fireEvent.change(cloudInput, { target: { value: 'BKN040' } });
      fireEvent.blur(weatherInput);
    });
    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    // publish it as well
    await waitFor(() => fireEvent.click(publishBtn));
    await waitFor(() =>
      fireEvent.click(queryByTestId('confirmationDialog-confirm')),
    );
    expect(props.onFormAction).toHaveBeenCalledWith(
      'PUBLISH',
      expect.anything(),
    );
  });

  it('should show errors on required fields when change = FM and visibility = CAVOK', async () => {
    const props = {
      tafFromBackend: {
        ...fakeNewTaf,
        editor: MOCK_USERNAME,
        taf: {
          ...fakeNewTaf.taf,
          changeGroups: [
            {
              change: 'FM',
              cavOK: true,
              valid: {
                start: `${moment.utc().format('YYYY-MM-DD')}T23:00:00Z`,
              },
            },
          ],
        },
      } as TafFromBackend,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container, findByText } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    const publishBtn = queryByTestId('publishtaf');

    await waitFor(() => fireEvent.click(publishBtn));

    const windInput = container.querySelector('[name="changeGroups[0].wind"');
    const visibilityInput = container.querySelector(
      '[name="changeGroups[0].visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="changeGroups[0].weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="changeGroups[0].cloud.cloud1"',
    );

    // update required fields
    await waitFor(() => {
      fireEvent.change(container.querySelector('[name="baseForecast.wind"'), {
        target: { value: '01015G35' },
      });
      fireEvent.blur(container.querySelector('[name="baseForecast.wind"'));
    });
    await waitFor(() => {
      fireEvent.change(
        container.querySelector('[name="baseForecast.visibility"'),
        {
          target: { value: 'CAVOK' },
        },
      );
      fireEvent.blur(
        container.querySelector('[name="baseForecast.visibility"'),
      );
    });

    expect(await findByText(TAC_MISSING_DATA)).toBeTruthy();

    expect(
      windInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      visibilityInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      weatherInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    expect(props.onFormAction).toHaveBeenCalledTimes(0);

    // fix wind
    await waitFor(() => {
      fireEvent.change(windInput, { target: { value: '01015G35' } });
      fireEvent.blur(windInput);
    });
    expect(
      windInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    // publish it
    await waitFor(() => fireEvent.click(publishBtn));
    await waitFor(() =>
      fireEvent.click(queryByTestId('confirmationDialog-confirm')),
    );
    expect(props.onFormAction).toHaveBeenCalledWith(
      'PUBLISH',
      expect.anything(),
    );
  });

  it('should show errors on required fields when change = FM and visibility> 5000', async () => {
    const props = {
      tafFromBackend: {
        ...fakeNewTaf,
        editor: MOCK_USERNAME,
        taf: {
          ...fakeNewTaf.taf,
          changeGroups: [
            {
              change: 'FM',
              visibility: { range: 6000, unit: 'M' },
              valid: {
                start: `${moment.utc().format('YYYY-MM-DD')}T23:00:00Z`,
              },
            },
          ],
        },
      } as TafFromBackend,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    const publishBtn = queryByTestId('publishtaf');

    await waitFor(() => fireEvent.click(publishBtn));

    const windInput = container.querySelector('[name="changeGroups[0].wind"');
    const visibilityInput = container.querySelector(
      '[name="changeGroups[0].visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="changeGroups[0].weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="changeGroups[0].cloud.cloud1"',
    );

    // update required fields
    await waitFor(() => {
      fireEvent.change(container.querySelector('[name="baseForecast.wind"'), {
        target: { value: '01015G35' },
      });
      fireEvent.blur(container.querySelector('[name="baseForecast.wind"'));
    });
    await waitFor(() => {
      fireEvent.change(
        container.querySelector('[name="baseForecast.visibility"'),
        {
          target: { value: 'CAVOK' },
        },
      );
      fireEvent.blur(
        container.querySelector('[name="baseForecast.visibility"'),
      );
    });

    expect(
      windInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      visibilityInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      weatherInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    expect(props.onFormAction).toHaveBeenCalledTimes(0);

    // fix wind
    await waitFor(() => {
      fireEvent.change(windInput, { target: { value: '01015G35' } });
      fireEvent.blur(windInput);
    });

    expect(
      windInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    // fix cloud
    await waitFor(() => {
      fireEvent.change(cloudInput, { target: { value: 'FEW015' } });
      fireEvent.blur(cloudInput);
    });

    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    // publish it
    await waitFor(() => fireEvent.click(publishBtn));
    await waitFor(() =>
      fireEvent.click(queryByTestId('confirmationDialog-confirm')),
    );
    expect(props.onFormAction).toHaveBeenCalledWith(
      'PUBLISH',
      expect.anything(),
    );
  });

  it('should show errors on required fields for baseforecast if visibility = CAVOK', async () => {
    const props = {
      tafFromBackend: {
        ...fakeNewTaf,
        editor: MOCK_USERNAME,
        taf: {
          ...fakeNewTaf.taf,
          baseForecast: {
            valid: fakeNewTaf.taf.baseForecast.valid,
            cavOK: true,
          },
        },
      } as TafFromBackend,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    const publishBtn = queryByTestId('publishtaf');

    await waitFor(() => fireEvent.click(publishBtn));

    const windInput = container.querySelector('[name="baseForecast.wind"');
    const visibilityInput = container.querySelector(
      '[name="baseForecast.visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="baseForecast.weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="baseForecast.cloud.cloud1"',
    );

    expect(
      windInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      visibilityInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      weatherInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    expect(props.onFormAction).toHaveBeenCalledTimes(0);

    // fix wind
    await waitFor(() =>
      fireEvent.change(windInput, { target: { value: '01015G35' } }),
    );
    expect(
      windInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    // publish it
    await waitFor(() => fireEvent.click(publishBtn));
    await waitFor(() =>
      fireEvent.click(queryByTestId('confirmationDialog-confirm')),
    );
    expect(props.onFormAction).toHaveBeenCalledWith(
      'PUBLISH',
      expect.anything(),
    );
  });

  it('should show errors on required fields for baseforecast if visibility > 5000', async () => {
    const props = {
      tafFromBackend: {
        ...fakeNewTaf,
        editor: MOCK_USERNAME,
        taf: {
          ...fakeNewTaf.taf,
          baseForecast: {
            valid: fakeNewTaf.taf.baseForecast.valid,
            visibility: { range: 6000, unit: 'M' },
          },
        },
      } as TafFromBackend,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    const publishBtn = queryByTestId('publishtaf');

    await waitFor(() => fireEvent.click(publishBtn));

    const windInput = container.querySelector('[name="baseForecast.wind"');
    const visibilityInput = container.querySelector(
      '[name="baseForecast.visibility"',
    );
    const weatherInput = container.querySelector(
      '[name="baseForecast.weather.weather1"',
    );
    const cloudInput = container.querySelector(
      '[name="baseForecast.cloud.cloud1"',
    );

    expect(
      windInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();
    expect(
      visibilityInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      weatherInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    expect(props.onFormAction).toHaveBeenCalledTimes(0);

    // fix wind
    await waitFor(() =>
      fireEvent.change(windInput, { target: { value: '01015G35' } }),
    );
    expect(
      windInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    // fix cloud
    await waitFor(() =>
      fireEvent.change(cloudInput, { target: { value: 'FEW015' } }),
    );
    expect(
      cloudInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    // publish it
    await waitFor(() => fireEvent.click(publishBtn));
    await waitFor(() =>
      fireEvent.click(queryByTestId('confirmationDialog-confirm')),
    );
    expect(props.onFormAction).toHaveBeenCalledWith(
      'PUBLISH',
      expect.anything(),
    );
  });

  it('should prevent publishing when required fields (wind/visibility) have not been entered but saving as draft is allowed with missing fields', async () => {
    const tafWithMissingData = {
      ...fakeDraftTaf,
      editor: MOCK_USERNAME,
      taf: {
        ...fakeDraftTaf.taf,
        validDateStart: '2021-01-26T18:00:00Z',
        validDateEnd: '2021-01-27T23:59:00Z',
        baseForecast: {
          valid: {
            start: '2021-01-26T18:00:00Z',
            end: '2021-01-27T23:59:00Z',
          },
        },
      },
    } as TafFromBackend;

    const props = {
      tafFromBackend: tafWithMissingData,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    const publishBtn = queryByTestId('publishtaf');

    await waitFor(() => fireEvent.click(publishBtn));

    const windInput = container.querySelector('[name="baseForecast.wind"');
    const visibilityInput = container.querySelector(
      '[name="baseForecast.visibility"',
    );

    expect(
      windInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    expect(
      visibilityInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    expect(props.onFormAction).toHaveBeenCalledTimes(0);

    // Even with missing fields we can save a draft
    await waitFor(() => fireEvent.click(queryByTestId('savedrafttaf')));
    expect(props.onFormAction).toHaveBeenCalledWith('DRAFT', expect.anything());
  });

  it('should prevent publishing when required fields in change groups (change/valid) have not been entered but saving as draft is allowed with missing fields', async () => {
    const tafWithMissingData = {
      ...fakeDraftTaf,
      editor: MOCK_USERNAME,
      taf: {
        ...fakeDraftTaf.taf,
        validDateStart: '2021-01-26T18:00:00Z',
        validDateEnd: '2021-01-27T23:59:00Z',
        changeGroups: [
          {
            visibility: { range: 6000, unit: 'M' },
            weather: { weather1: '+SHRA' },
            wind: { direction: 10, speed: 15, unit: 'KT', gust: 35 },
            cloud: {
              cloud1: { coverage: 'BKN', height: 40 },
            },
          },
        ],
      },
    } as TafFromBackend;

    const props = {
      tafFromBackend: tafWithMissingData,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    const publishBtn = queryByTestId('publishtaf');

    await waitFor(() => fireEvent.click(publishBtn));

    const changeInput = container.querySelector(
      '[name="changeGroups[0].change"',
    );
    const validInput = container.querySelector('[name="changeGroups[0].valid"');

    expect(
      changeInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    expect(
      validInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    expect(props.onFormAction).toHaveBeenCalledTimes(0);

    // Even with missing fields we can save a draft
    await waitFor(() => fireEvent.click(queryByTestId('savedrafttaf')));
    expect(props.onFormAction).toHaveBeenCalledWith('DRAFT', expect.anything());
  });

  it('should prevent saving as draft if fields contain incorrect data', async () => {
    const tafWithMissingData = {
      ...fakeDraftTaf,
      editor: MOCK_USERNAME,
      taf: {
        ...fakeDraftTaf.taf,
        validDateStart: '2021-01-26T18:00:00Z',
        validDateEnd: '2021-01-27T23:59:00Z',
        changeGroups: [
          {
            visibility: { range: 6000, unit: 'M' },
            weather: { weather1: 'SOMEFAKEDATAWRONG' as WeatherPhenomena },
            wind: { direction: 10, speed: 15, unit: 'KT', gust: 35 },
            cloud: {
              cloud1: { coverage: 'BKN', height: 40 },
            },
          },
        ],
      },
    } as TafFromBackend;

    const props = {
      tafFromBackend: tafWithMissingData,
      onFormAction: jest.fn(),
    };
    const { queryByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    const publishBtn = queryByTestId('publishtaf');

    await waitFor(() => fireEvent.click(publishBtn));

    const weatherInput = container.querySelector(
      '[name="changeGroups[0].weather.weather1"',
    );

    expect(
      weatherInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    expect(props.onFormAction).toHaveBeenCalledTimes(0);
  });

  it('should validate next field', async () => {
    const props = {
      tafFromBackend: fakeDraftTafWithSameDates,
      onFormAction: jest.fn(),
    };
    const { findByText, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    const validInput = container.querySelector('[name="changeGroups[0].valid"');
    const nextValidInput = container.querySelector(
      '[name="changeGroups[1].valid"',
    );

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    // change input to higher date value
    await waitFor(() => {
      const newFutureDate = moment(
        fakeDraftTafWithSameDates.taf.changeGroups[0].valid.start,
      )
        .add(2, 'hours')
        .format(FORMAT_DAYS_HOURS_MINUTES);

      fireEvent.change(validInput, {
        target: { value: newFutureDate },
      });
      fireEvent.blur(validInput);
    });

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    // change input to lower date value
    await waitFor(() => {
      const newPastDate = moment(
        fakeDraftTafWithSameDates.taf.changeGroups[0].valid.start,
      )
        .subtract(2, 'hours')
        .format(FORMAT_DAYS_HOURS_MINUTES);
      fireEvent.change(validInput, {
        target: { value: newPastDate },
      });
      fireEvent.blur(validInput);
    });

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
  });

  it('should validate fields for start date after deleting a row', async () => {
    const props = {
      tafFromBackend: fakeDraftTafWithSameDates,
      onFormAction: jest.fn(),
    };
    const { findByText, container, getByTestId, queryByTestId, queryByText } =
      render(
        <TafThemeApiProvider>
          <TafForm {...props} />
        </TafThemeApiProvider>,
      );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    const validInput = container.querySelector('[name="changeGroups[0].valid"');
    const nextValidInput = container.querySelector(
      '[name="changeGroups[1].valid"',
    );

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    // change input to higher date value
    await waitFor(() => {
      const newFutureDate = moment(
        fakeDraftTafWithSameDates.taf.changeGroups[0].valid.start,
      )
        .add(2, 'hours')
        .format(FORMAT_DAYS_HOURS_MINUTES);

      fireEvent.change(validInput, {
        target: { value: newFutureDate },
      });
      fireEvent.blur(validInput);
    });

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    // remove row with higher date
    await waitFor(() => {
      fireEvent.click(queryByTestId('tafFormOptions[0]'));
      fireEvent.click(queryByText('Delete row'));
      fireEvent.click(getByTestId('confirmationDialog-confirm'));
    });

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
  });

  it('should validate next row valid field when changing valid field', async () => {
    const props = {
      tafFromBackend: fakeDraftTafWithSameDates,
      onFormAction: jest.fn(),
    };
    const { findByText, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    const changeInput = container.querySelector(
      '[name="changeGroups[3].change"',
    );
    const nextValidInput = container.querySelector(
      '[name="changeGroups[4].valid"',
    );

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    // change input to TEMPO
    await waitFor(() => {
      fireEvent.change(changeInput, {
        target: { value: 'TEMPO' },
      });
      fireEvent.blur(changeInput);
    });

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    // reset the value so the order is correct again
    await waitFor(() => {
      fireEvent.change(changeInput, {
        target: { value: '' },
      });
      fireEvent.blur(changeInput);
    });

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
  });

  it('should validate next row valid field when changing prob field', async () => {
    const props = {
      tafFromBackend: fakeDraftTafWithSameDates,
      onFormAction: jest.fn(),
    };
    const { findByText, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    const probabilityInput = container.querySelector(
      '[name="changeGroups[5].probability"',
    );
    const nextValidInput = container.querySelector(
      '[name="changeGroups[6].valid"',
    );

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();

    // change input to TEMPO
    await waitFor(() => {
      fireEvent.change(probabilityInput, {
        target: { value: 'PROB30' },
      });
      fireEvent.blur(probabilityInput);
    });

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeTruthy();

    // reset the value so the order is correct again
    await waitFor(() => {
      fireEvent.change(probabilityInput, {
        target: { value: 'PROB40' },
      });
      fireEvent.blur(probabilityInput);
    });

    expect(
      nextValidInput.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
  });

  it('should not show any errors when opening a new TAF and after using TAB', async () => {
    const { getAllByTestId, getByTestId, findByText, container } = render(
      <TafThemeApiProvider>
        <TafForm tafFromBackend={{ ...fakeNewTaf, editor: MOCK_USERNAME }} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(getAllByTestId('row-baseForecast').length).toEqual(1);
    expect(getByTestId('row-changeGroups[0]')).toBeTruthy();
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();
    // first field has focus
    expect(document.activeElement.getAttribute('name')).toEqual(
      'baseForecast.wind',
    );
    // change focus to next field
    userEvent.tab();
    // next field has focus
    await waitFor(() =>
      expect(document.activeElement.getAttribute('name')).toEqual(
        'baseForecast.visibility',
      ),
    );
    expect(container.querySelectorAll('.Mui-error').length).toBeFalsy();
  });

  it('should show an error for each invalid validity time when amending', async () => {
    // set current time to 14h
    const date = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(date).valueOf());

    const props = {
      isDisabled: true,
      tafFromBackend: {
        ...fakePublishedTafWithChangeGroups,
        editor: MOCK_USERNAME,
      },
      setIsDisabled: jest.fn(),
    };
    const { findByText, container, getByTestId, rerender } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('ORG');
    fireEvent.click(getByTestId('amendtaf'));
    expect(
      container.querySelector('[name="messageType"]').getAttribute('value'),
    ).toEqual('AMD');

    expect(props.setIsDisabled).toHaveBeenCalledWith(false);

    rerender(
      <TafThemeApiProvider>
        <TafForm {...props} isDisabled={false} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      const validInput0 = container.querySelector(
        '[name="changeGroups[0].valid"',
      );
      const validInput1 = container.querySelector(
        '[name="changeGroups[1].valid"',
      );
      const validInput2 = container.querySelector(
        '[name="changeGroups[2].valid"',
      );
      expect(
        validInput0.closest('div').className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        validInput1.closest('div').className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        validInput2.closest('div').className.includes('Mui-error'),
      ).toBeFalsy();
    });
  });

  it('should not show any error for invalid validity time when in view mode', async () => {
    // set current time to 14h
    const date = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(date).valueOf());

    const { findByText, container, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          isDisabled
          tafFromBackend={{
            ...fakePublishedTafWithChangeGroups,
            editor: MOCK_USERNAME,
          }}
        />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    const validInput0 = container.querySelector(
      '[name="changeGroups[0].valid"',
    );
    const validInput1 = container.querySelector(
      '[name="changeGroups[1].valid"',
    );
    const validInput2 = container.querySelector(
      '[name="changeGroups[2].valid"',
    );
    expect(
      validInput0.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      validInput1.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
    expect(
      validInput2.closest('div').className.includes('Mui-error'),
    ).toBeFalsy();
  });

  it('should validate next row valid field and all previous row valid fields when changing valid field', async () => {
    const props = {
      tafFromBackend: fakeDraftTafWithFM,
      onFormAction: jest.fn(),
    };
    const { findByText, container, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    const FMValidInput = container.querySelector(
      '[name="changeGroups[2].valid"',
    );
    const nextValidInput = container.querySelector(
      '[name="changeGroups[3].valid"',
    );

    const previousValidInput1 = container.querySelector(
      '[name="changeGroups[0].valid"',
    );

    const previousValidInput2 = container.querySelector(
      '[name="changeGroups[1].valid"',
    );

    await waitFor(() => {
      expect(
        previousValidInput1.closest('div').className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        previousValidInput2.closest('div').className.includes('Mui-error'),
      ).toBeTruthy();
      expect(
        FMValidInput.closest('div').className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        nextValidInput.closest('div').className.includes('Mui-error'),
      ).toBeFalsy();
    });

    // change validity of FM group
    await waitFor(() => {
      fireEvent.change(FMValidInput, {
        target: { value: '061600' },
      });
      fireEvent.blur(FMValidInput);
    });

    await waitFor(() => {
      expect(
        previousValidInput1.closest('div').className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        previousValidInput2.closest('div').className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        FMValidInput.closest('div').className.includes('Mui-error'),
      ).toBeFalsy();
      expect(
        nextValidInput.closest('div').className.includes('Mui-error'),
      ).toBeTruthy();
    });
  });

  it('should update the form values and TAC when the taf values changed', async () => {
    const { container, findByText, rerender, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm
          isDisabled
          tafFromBackend={{
            ...fakeNewTaf,
            editor: MOCK_USERNAME,
          }}
        />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();

    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="issueDate"]').getAttribute('value'),
    ).toEqual('Not issued');

    rerender(
      <TafThemeApiProvider>
        <TafForm isDisabled tafFromBackend={fakePublishedTaf} />
      </TafThemeApiProvider>,
    );

    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();

    expect(
      container.querySelector('[name="issueDate"]').getAttribute('value'),
    ).not.toEqual('Not issued');
    // make sure we are in viewer mode
    expect(getByTestId('switchMode').classList).not.toContain('Mui-checked');
  });

  it('should show the confirmation dialog when switching from editor to viewer mode with some unsaved changes and reset the form when switched', async () => {
    const props = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest.fn().mockImplementation(() => Promise.resolve(true)),
    };
    const { getByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // add weather2 input
    const weather2Input = container.querySelector(
      'input[name="baseForecast.weather.weather2"]',
    );
    fireEvent.change(weather2Input, { target: { value: 'RA' } });

    // switch to viewer mode
    fireEvent.click(
      container.querySelector('[data-testid="switchMode"] input'),
    );

    await waitFor(() => expect(getByTestId('confirmationDialog')).toBeTruthy());
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );
    expect(props.onFormAction).not.toHaveBeenCalled();
    expect(props.onSwitchEditor).toHaveBeenCalled();
    // form should be reset
    await waitFor(() =>
      expect(
        container
          .querySelector('input[name="baseForecast.weather.weather2"]')
          .getAttribute('value'),
      ).toEqual(''),
    );
  });

  it('should not reset the form when switching from editor to viewer mode failed', async () => {
    const props = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
      onSwitchEditor: jest
        .fn()
        .mockImplementation(() => Promise.resolve(false)),
    };
    const { getByTestId, container } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );

    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // add weather2 input
    const weather2Input = container.querySelector(
      'input[name="baseForecast.weather.weather2"]',
    );
    fireEvent.change(weather2Input, { target: { value: 'RA' } });

    // switch to viewer mode
    fireEvent.click(
      container.querySelector('[data-testid="switchMode"] input'),
    );

    await waitFor(() => expect(getByTestId('confirmationDialog')).toBeTruthy());
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    await waitFor(() =>
      fireEvent.click(getByTestId('confirmationDialog-confirm')),
    );
    expect(props.onFormAction).not.toHaveBeenCalled();
    expect(props.onSwitchEditor).toHaveBeenCalled();
    // form should not be reset
    await waitFor(() =>
      expect(
        container
          .querySelector('input[name="baseForecast.weather.weather2"]')
          .getAttribute('value'),
      ).toEqual('RA'),
    );
  });

  it('should show the avatar when user is editor', async () => {
    const props = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: MOCK_USERNAME,
      },
      onFormAction: jest.fn(),
    };
    const { findByText, getByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(getByTestId('avatar').textContent).toEqual(
      getInitials(props.tafFromBackend.editor),
    );
  });
  it('should not show the avatar when taf has no editor', async () => {
    const props = {
      tafFromBackend: {
        ...fakeDraftTaf,
        editor: null,
      },
      onFormAction: jest.fn(),
    };
    const { findByText, queryByTestId } = render(
      <TafThemeApiProvider>
        <TafForm {...props} />
      </TafThemeApiProvider>,
    );
    expect(await findByText(TAC_RETRIEVING_DATA)).toBeTruthy();
    expect(queryByTestId('avatar')).toBeFalsy();
  });
});
