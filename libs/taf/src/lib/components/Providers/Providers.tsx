/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { UseFormOptions } from 'react-hook-form';
import { Theme } from '@mui/material';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { ApiProvider } from '@opengeoweb/api';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import { AuthenticationProvider } from '@opengeoweb/authentication';
import { store as appStore, SnackbarWrapperConnect } from '@opengeoweb/core';
import { Store } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { createApi } from '../../utils/api';
import { TafWrapper } from '../TafWrapper';

// Used to mimic an authenticated user
export const MOCK_USERNAME = 'user.name';

export interface TafThemeApiProviderProps {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  createApiFunc?: any;
  children: React.ReactNode;
  theme?: Theme;
  store?: Store;
}

export const TafThemeApiProvider: React.FC<TafThemeApiProviderProps> = ({
  children,
  createApiFunc = null,
  theme = lightTheme,
  store = appStore,
}: TafThemeApiProviderProps) => {
  return (
    <ThemeWrapper theme={theme}>
      <AuthenticationProvider
        value={{
          isLoggedIn: true,
          auth: {
            username: MOCK_USERNAME,
            token: '1223344',
            refresh_token: '33455214',
          },
          onLogin: (): void => null,
          onSetAuth: (): void => null,
          sessionStorageProvider: null,
        }}
      >
        <ApiProvider
          createApi={createApiFunc !== null ? createApiFunc : createApi}
        >
          <Provider store={store}>
            <SnackbarWrapperConnect>
              <TafWrapper>{children}</TafWrapper>
            </SnackbarWrapperConnect>
          </Provider>
        </ApiProvider>
      </AuthenticationProvider>
    </ThemeWrapper>
  );
};

interface TafThemeFormProviderProps {
  children: React.ReactNode;
  options?: UseFormOptions;
  theme?: Theme;
}

export const TafThemeFormProvider: React.FC<TafThemeFormProviderProps> = ({
  children,
  options = {},
  theme = lightTheme,
}: TafThemeFormProviderProps) => (
  <ThemeWrapper theme={theme}>
    <ReactHookFormProvider options={options}>{children}</ReactHookFormProvider>
  </ThemeWrapper>
);
