/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { act, fireEvent, render, waitFor } from '@testing-library/react';

import TafLayout, {
  CONFIRM_DIALOG_OPTIONS,
  getSnackbarMessage,
  getTafMessagePrefix,
} from './TafLayout';
import {
  fakeAmendmentFixedTaf,
  fakeCancelledFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeDraftTaf,
  fakeFixedTafList,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
  fakeTafList,
} from '../../utils/mockdata/fakeTafList';
import { MOCK_USERNAME, TafThemeApiProvider } from '../Providers';
import { fakeTestTac, createApi } from '../../utils/__mocks__/api';

jest.mock('../../utils/api');
jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual('../../../../../api/src/index') as Record<
    string,
    unknown
  >),
}));

describe('components/TafLayout/TafLayout', () => {
  describe('getSnackbarMessage', () => {
    it('should return correct message', () => {
      expect(
        getSnackbarMessage({
          changeStatusTo: 'DRAFT',
          taf: fakeNewFixedTaf.taf,
        }),
      ).toEqual('Saved as draft');
      expect(
        getSnackbarMessage({
          changeStatusTo: 'DRAFT_AMENDED',
          taf: fakeNewFixedTaf.taf,
        }),
      ).toEqual('Saved as draft amended');
      expect(
        getSnackbarMessage({
          changeStatusTo: 'DRAFT_CORRECTED',
          taf: fakeNewFixedTaf.taf,
        }),
      ).toEqual('Saved as draft correction');
      expect(
        getSnackbarMessage({
          changeStatusTo: 'CANCELLED',
          taf: fakeNewFixedTaf.taf,
        }),
      ).toEqual(getTafMessagePrefix(fakeNewFixedTaf.taf, 'cancelled'));
      expect(
        getSnackbarMessage({
          changeStatusTo: 'AMENDED',
          taf: fakeNewFixedTaf.taf,
        }),
      ).toEqual(getTafMessagePrefix(fakeNewFixedTaf.taf, 'amended'));
      expect(
        getSnackbarMessage(
          { changeStatusTo: 'DRAFT', taf: fakeNewFixedTaf.taf },
          true,
        ),
      ).toEqual('All changes have been saved automatically');
    });
  });

  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should render with default props', async () => {
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    const { queryByTestId } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    expect(queryByTestId('lastupdated-time')).toBeFalsy();

    await waitFor(() => expect(queryByTestId('location-tabs')).toBeTruthy());
  });

  it('should not fail without a taflist', async () => {
    const props = {
      tafList: null,
      onUpdateTaf: jest.fn(),
    };

    const { queryByTestId } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => expect(queryByTestId('location-tabs')).toBeNull());
  });

  it('should open the mobile TAC Overview', async () => {
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    const { queryByTestId, queryAllByText } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => expect(queryByTestId('location-tabs')).toBeTruthy());
    expect(queryByTestId('tac-overview-mobile-button')).toBeTruthy();

    const style = window.getComputedStyle(
      queryByTestId('tac-overview-mobile-button'),
    );
    expect(style.display).toBe('inline-flex');
    expect(queryByTestId('tac-overview-mobile')).toBeFalsy();

    fireEvent.click(queryByTestId('tac-overview-mobile-button'));
    await waitFor(() =>
      expect(queryByTestId('tac-overview-mobile')).toBeTruthy(),
    );
    await waitFor(() =>
      expect(queryAllByText(fakeTestTac).length).toBeTruthy(),
    );
  });

  it('should activate selected location in TAC Overview', async () => {
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, container, queryAllByText } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(getByTestId('location-tabs').querySelectorAll('li')).toBeTruthy();
    });

    const locations = getByTestId('location-tabs').querySelectorAll('li');
    const lastLocation = locations[locations.length - 1];

    await waitFor(() => {
      fireEvent.click(lastLocation);
    });

    const activeLocation = lastLocation.querySelector('span').innerHTML;
    const activeLocations = container.querySelectorAll(
      `[data-location="${activeLocation}"]`,
    );

    expect(Element.prototype.scrollTo).toHaveBeenCalled();

    activeLocations.forEach((location) =>
      expect(location.className).toContain('active'),
    );
    await waitFor(() =>
      expect(queryAllByText(fakeTestTac).length).toBeTruthy(),
    );
  });

  it('should not ask for confirmation for switching taf location when form has been saved as draft', async () => {
    Element.prototype.scrollTo = jest.fn();

    const props = {
      tafList: fakeTafList,
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, container, queryAllByText } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(getByTestId('location-tabs').querySelectorAll('li')).toBeTruthy();
    });

    const locations = getByTestId('location-tabs').querySelectorAll('li');
    const lastLocation = locations[locations.length - 1];

    // first location is active
    expect(locations[0].classList).toContain('Mui-selected');
    expect(lastLocation.classList).not.toContain('Mui-selected');

    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // edit the selected taf
    await waitFor(() => {
      fireEvent.change(container.querySelector('[name="baseForecast.wind"'), {
        target: { value: '01015G35' },
      });
      fireEvent.blur(container.querySelector('[name="baseForecast.wind"'));
    });

    // change location without saving
    await waitFor(() => {
      fireEvent.click(lastLocation);
    });

    await waitFor(() => expect(props.onUpdateTaf).toHaveBeenCalled());

    // change timeslot
    await waitFor(() => {
      fireEvent.click(getByTestId('toptab-1'));
    });

    // active timeslot is changed
    await waitFor(() => {
      expect(getByTestId('toptab-1').classList).toContain('Mui-selected');
      expect(getByTestId('toptab-0').classList).not.toContain('Mui-selected');
    });

    await waitFor(() =>
      expect(queryAllByText(fakeTestTac).length).toBeTruthy(),
    );
  });

  it('should not ask for confirmation when switching location tabs when form is not changed', async () => {
    const props = {
      tafList: fakeTafList,
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, queryAllByText } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(getByTestId('location-tabs').querySelectorAll('li')).toBeTruthy();
    });

    const locations = getByTestId('location-tabs').querySelectorAll('li');
    const lastLocation = locations[locations.length - 1];

    // first location is active
    expect(locations[0].classList).toContain('Mui-selected');
    expect(lastLocation.classList).not.toContain('Mui-selected');

    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // change location
    await waitFor(() => {
      fireEvent.click(lastLocation);
    });

    await waitFor(() => expect(props.onUpdateTaf).not.toHaveBeenCalled());

    // active location is changed
    await waitFor(() => {
      expect(lastLocation.classList).toContain('Mui-selected');
    });

    await waitFor(() =>
      expect(queryAllByText(fakeTestTac).length).toBeTruthy(),
    );
  });

  it('should not ask for confirmation when switching location tabs when form is edited and saved', async () => {
    const props = {
      tafList: fakeTafList,
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, container, queryAllByText } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(getByTestId('location-tabs').querySelectorAll('li')).toBeTruthy();
    });

    const locations = getByTestId('location-tabs').querySelectorAll('li');
    const lastLocation = locations[locations.length - 1];

    // first location is active
    expect(locations[0].classList).toContain('Mui-selected');
    expect(lastLocation.classList).not.toContain('Mui-selected');

    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // edit the selected taf
    await waitFor(() => {
      fireEvent.change(container.querySelector('[name="baseForecast.wind"'), {
        target: { value: '20010' },
      });
      fireEvent.blur(container.querySelector('[name="baseForecast.wind"'));
    });

    fireEvent.click(getByTestId('savedrafttaf'));
    await waitFor(() =>
      expect(getByTestId('issuesButton').textContent).toEqual('0 issues'),
    );
    await waitFor(() => expect(props.onUpdateTaf).toHaveBeenCalled());

    // change location again
    await waitFor(() => {
      fireEvent.click(lastLocation);
    });

    // active location is changed
    await waitFor(() => {
      expect(lastLocation.classList).toContain('Mui-selected');
    });

    await waitFor(() =>
      expect(queryAllByText(fakeTestTac).length).toBeTruthy(),
    );
  });

  it('should update the location tabs when switching timeslots', async () => {
    const now = '2022-01-06T14:00:00Z';
    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
    Element.prototype.scrollTo = jest.fn();

    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, queryAllByText } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(getByTestId('timeslot-tabs').querySelectorAll('li')).toBeTruthy();
    });

    // first timeslot tab is active
    expect(getByTestId('toptab-0').classList).toContain('Mui-selected');
    expect(getByTestId('toptab-1').classList).not.toContain('Mui-selected');

    // change timeslot
    await waitFor(() => {
      fireEvent.click(getByTestId('toptab-1'));
    });

    // active timeslot is changed
    await waitFor(() => {
      expect(getByTestId('toptab-1').classList).toContain('Mui-selected');
      expect(getByTestId('toptab-0').classList).not.toContain('Mui-selected');
    });

    await waitFor(() => {
      expect(queryAllByText(fakeTestTac).length).toBeTruthy();
    });

    // check that current timeslot has 5 locations
    expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(5);

    // change timeslot back
    await waitFor(() => {
      fireEvent.click(getByTestId('toptab-0'));
    });

    // active timeslot is changed
    await waitFor(() => {
      expect(getByTestId('toptab-1').classList).not.toContain('Mui-selected');
      expect(getByTestId('toptab-0').classList).toContain('Mui-selected');
    });

    await waitFor(() => {
      expect(queryAllByText(fakeTestTac).length).toBeTruthy();
    });
    // check that selected timeslot has locations
    expect(
      getByTestId('location-tabs').querySelectorAll('li').length,
    ).toBeGreaterThan(0);
  });

  it('should update the location tabs when changing timeslots and new tafList is loaded', async () => {
    const now = '2022-01-06T14:00:00Z';
    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
    Element.prototype.scrollTo = jest.fn();

    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, rerender } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        1,
      ),
    );

    // first timeslot tab is active
    expect(getByTestId('toptab-0').classList).toContain('Mui-selected');
    expect(getByTestId('toptab-1').classList).not.toContain('Mui-selected');

    // change timeslot
    await waitFor(() => {
      fireEvent.click(getByTestId('toptab-1'));
    });

    // second timeslot tab is active
    expect(getByTestId('toptab-0').classList).not.toContain('Mui-selected');
    expect(getByTestId('toptab-1').classList).toContain('Mui-selected');

    rerender(
      <TafThemeApiProvider>
        <TafLayout
          tafList={[fakeNewFixedTaf]}
          onUpdateTaf={props.onUpdateTaf}
        />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        1,
      ),
    );

    // change timeslot
    await waitFor(() => {
      fireEvent.click(getByTestId('toptab-0'));
    });

    // first timeslot tab is active
    expect(getByTestId('toptab-0').classList).toContain('Mui-selected');
    expect(getByTestId('toptab-1').classList).not.toContain('Mui-selected');

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        0,
      ),
    );
  });

  it('should select the previously selected location when new tafList is loaded', async () => {
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, container, rerender } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        6,
      ),
    );

    const locations = getByTestId('location-tabs').querySelectorAll('li');
    const lastLocation = locations[locations.length - 1];

    // first location is active
    expect(locations[0].classList).toContain('Mui-selected');
    expect(lastLocation.classList).not.toContain('Mui-selected');

    // select last location
    await waitFor(() => {
      fireEvent.click(lastLocation);
    });

    expect(locations[0].classList).not.toContain('Mui-selected');
    expect(lastLocation.classList).toContain('Mui-selected');

    const locationName = lastLocation.textContent;

    rerender(
      <TafThemeApiProvider>
        <TafLayout
          tafList={[fakeCancelledFixedTaf, fakeNewFixedTaf]}
          onUpdateTaf={props.onUpdateTaf}
        />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        2,
      ),
    );

    const activeLocation = container.querySelector('li.Mui-selected');
    expect(activeLocation.textContent).toEqual(locationName);
  });

  it('should display the correct dates in headers', async () => {
    const now = '2022-01-06T14:00:00Z';
    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
    Element.prototype.scrollTo = jest.fn();

    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(getByTestId('timeslot-tabs').querySelectorAll('li')).toBeTruthy();
    });

    expect(getByTestId('toptab-0').textContent).toBe('Upcoming 18:00 06 Jan');
    expect(getByTestId('toptab-1').textContent).toBe('Current 12:00 06 Jan');
  });

  it('should toggle the issues pane via issuesButton and dialog close button', async () => {
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        1,
      ),
    );

    expect(queryByTestId('moveable-issues-pane')).toBeFalsy();

    const tafLocations = getByTestId('location-tabs').querySelectorAll('li');

    // select taf that is in edit mode
    await waitFor(() => {
      fireEvent.click(tafLocations[0]);
    });

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(getByTestId('issuesButton')).toBeTruthy();
    fireEvent.click(getByTestId('issuesButton'));

    expect(queryByTestId('moveable-issues-pane')).toBeTruthy();

    // close it via dialog close button
    fireEvent.click(getByTestId('closeBtn'));
    expect(queryByTestId('moveable-issues-pane')).toBeFalsy();

    // open again
    fireEvent.click(getByTestId('issuesButton'));
    expect(queryByTestId('moveable-issues-pane')).toBeTruthy();

    // close it via issuesButton
    fireEvent.click(getByTestId('issuesButton'));
    expect(queryByTestId('moveable-issues-pane')).toBeFalsy();
  });

  it('should toggle the issues pane and should keep being opened while changing TAFs', async () => {
    const props = {
      tafList: [
        { ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME },
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, queryByTestId } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        2,
      ),
    );

    expect(queryByTestId('moveable-issues-pane')).toBeFalsy();

    const tafLocations = getByTestId('location-tabs').querySelectorAll('li');

    // select taf that is in edit mode
    await waitFor(() => {
      fireEvent.click(tafLocations[1]);
    });

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(getByTestId('issuesButton')).toBeTruthy();
    fireEvent.click(getByTestId('issuesButton'));

    expect(queryByTestId('moveable-issues-pane')).toBeTruthy();

    // switch back to taf which is in view mode
    await waitFor(() => {
      fireEvent.click(tafLocations[0]);
    });
    expect(queryByTestId('moveable-issues-pane')).toBeTruthy();

    // close it
    fireEvent.click(getByTestId('closeBtn'));
    expect(queryByTestId('moveable-issues-pane')).toBeFalsy();
  });

  it('should reset form values when changing TAFs', async () => {
    const props = {
      tafList: [
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
        { ...fakeNewFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };

    const { getByTestId, container } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() =>
      expect(getByTestId('location-tabs').querySelectorAll('li').length).toBe(
        2,
      ),
    );

    const tafLocations = getByTestId('location-tabs').querySelectorAll('li');

    // select new taf
    await waitFor(() => {
      fireEvent.click(tafLocations[1]);
    });

    // make sure we are in editor mode
    await waitFor(() => {
      expect(getByTestId('switchMode').classList).toContain('Mui-checked');
    });

    expect(
      container.querySelector('[name="baseForecast.wind"]').parentElement
        .classList,
    ).not.toContain('Mui-error');

    // publish it
    await waitFor(() => {
      fireEvent.click(getByTestId('publishtaf'));
    });
    expect(props.onUpdateTaf).toHaveBeenCalledTimes(0);
    await waitFor(() => {
      expect(
        container.querySelector('[name="baseForecast.wind"]').parentElement
          .classList,
      ).toContain('Mui-error');
    });

    // select draft TAF
    await waitFor(() => {
      fireEvent.click(tafLocations[0]);
    });

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // publish draft
    await waitFor(() => {
      fireEvent.click(getByTestId('publishtaf'));
    });
    await waitFor(() => {
      fireEvent.click(getByTestId('confirmationDialog-confirm'));
    });
    expect(props.onUpdateTaf).toHaveBeenCalledTimes(1);

    // back to new taf
    await waitFor(() => {
      fireEvent.click(tafLocations[1]);
    });

    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(
      container.querySelector('[name="baseForecast.wind"]').parentElement
        .classList,
    ).not.toContain('Mui-error');

    // publish it
    await waitFor(() => {
      fireEvent.click(getByTestId('publishtaf'));
    });

    await waitFor(() => {
      expect(
        container.querySelector('[name="baseForecast.wind"]').parentElement
          .classList,
      ).toContain('Mui-error');
    });
    expect(props.onUpdateTaf).toHaveBeenCalledTimes(1);
  });

  it('should show the TAC export buttons only when form is in edit mode', async () => {
    const props = {
      tafList: [{ ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    const { queryByTestId, queryAllByTestId, getByTestId } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => expect(queryByTestId('location-tabs')).toBeTruthy());

    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    expect(queryAllByTestId('export-tac-button').length).toBeFalsy();
    await waitFor(() => fireEvent.click(getByTestId('correcttaf')));
    expect(queryAllByTestId('export-tac-button').length).toBeTruthy();
    await waitFor(() => fireEvent.click(getByTestId('discardtaf')));
    expect(queryAllByTestId('export-tac-button').length).toBeFalsy();
  });

  it('should be able to post a taf', async () => {
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 300);
      });
    });
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { container, getByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );
    const tafLocations = getByTestId('location-tabs').querySelectorAll('li');
    // select new taf
    await waitFor(() => {
      fireEvent.click(tafLocations[0]);
    });

    await waitFor(() => expect(getByTestId('taf-panel-active')).toBeTruthy());

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // click publish
    const publishButton = container.parentElement.querySelector(
      '[data-testid=publishtaf]',
    );
    await waitFor(() => {
      fireEvent.click(publishButton);
    });

    // click publish in the confirmation dialog
    const confirmationDialog = container.parentElement.querySelector(
      '[data-testid=confirmationDialog]',
    );
    await waitFor(() => {
      expect(confirmationDialog).toBeTruthy();
    });

    await waitFor(() => {
      fireEvent.click(
        confirmationDialog.querySelector(
          '[data-testid=confirmationDialog-confirm]',
        ),
      );
      expect(mockPostTaf).not.toHaveBeenCalled();
    });
    // confirmation dialog should be closed
    await waitFor(() => {
      expect(
        container.parentElement.querySelector(
          '[data-testid=confirmationDialog]',
        ),
      ).toBeFalsy();
    });

    // should show loading
    expect(
      container.parentElement.querySelector('[data-testid=taf-loading]'),
    ).toBeTruthy();
    expect(mockPostTaf).toHaveBeenCalled();

    jest.runOnlyPendingTimers();
    await waitFor(() => {
      expect(
        container.parentElement.querySelector('[data-testid=taf-loading]'),
      ).toBeFalsy();
      // onPostTaf callback
      expect(props.onUpdateTaf).toHaveBeenCalled();
    });
  });

  it('should show an error when posting to BE fails', async () => {
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve, reject) => {
        return setTimeout(() => {
          reject(new Error('something went wrong'));
        }, 1000);
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { container, getByTestId, queryByTestId, getByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const tafLocations = getByTestId('location-tabs').querySelectorAll('li');
    // select new taf
    await waitFor(() => {
      fireEvent.click(tafLocations[0]);
    });

    await waitFor(() => expect(getByTestId('taf-panel-active')).toBeTruthy());

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // click publish
    const publishButton = container.parentElement.querySelector(
      '[data-testid=publishtaf]',
    );
    await waitFor(() => {
      fireEvent.click(publishButton);
    });

    // click publish in the confirmation dialog
    const confirmationDialog = container.parentElement.querySelector(
      '[data-testid=confirmationDialog]',
    );
    await waitFor(() => {
      expect(confirmationDialog).toBeTruthy();
    });

    await waitFor(() => {
      fireEvent.click(
        confirmationDialog.querySelector(
          '[data-testid=confirmationDialog-confirm]',
        ),
      );
      expect(mockPostTaf).not.toHaveBeenCalled();
    });
    // confirmation dialog should be closed
    await waitFor(() => {
      expect(
        container.parentElement.querySelector(
          '[data-testid=confirmationDialog]',
        ),
      ).toBeFalsy();
    });

    // should show loading
    expect(container.querySelector('[data-testid=taf-loading]')).toBeTruthy();
    expect(mockPostTaf).toHaveBeenCalled();

    await act(async () => {
      jest.runOnlyPendingTimers();
    });

    await waitFor(() => {
      // alert should be shown
      expect(queryByTestId('taf-error')).toBeTruthy();
      // no callback should have fired
      expect(props.onUpdateTaf).not.toHaveBeenCalled();
    });

    // user should be able to close the alert
    fireEvent.click(getByText('CLOSE'));
    await waitFor(() => expect(queryByTestId('taf-error')).toBeFalsy());
  });

  it('should show an error when posting to BE fails the second time and user has clicked the first error away', async () => {
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve, reject) => {
        return setTimeout(() => {
          reject(new Error('something went wrong'));
        }, 1000);
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { container, getByTestId, queryByTestId, getByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const tafLocations = getByTestId('location-tabs').querySelectorAll('li');
    // select new taf
    await waitFor(() => {
      fireEvent.click(tafLocations[0]);
    });

    await waitFor(() => expect(getByTestId('taf-panel-active')).toBeTruthy());

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // click publish
    const publishButton = container.parentElement.querySelector(
      '[data-testid=publishtaf]',
    );
    await waitFor(() => {
      fireEvent.click(publishButton);
    });

    // click publish in the confirmation dialog
    const confirmationDialog = container.parentElement.querySelector(
      '[data-testid=confirmationDialog]',
    );
    await waitFor(() => {
      expect(confirmationDialog).toBeTruthy();
    });

    await waitFor(() => {
      fireEvent.click(
        confirmationDialog.querySelector(
          '[data-testid=confirmationDialog-confirm]',
        ),
      );
      expect(mockPostTaf).not.toHaveBeenCalled();
    });
    // confirmation dialog should be closed
    await waitFor(() => {
      expect(
        container.parentElement.querySelector(
          '[data-testid=confirmationDialog]',
        ),
      ).toBeFalsy();
    });

    // should show loading
    expect(container.querySelector('[data-testid=taf-loading]')).toBeTruthy();
    expect(mockPostTaf).toHaveBeenCalled();

    await act(async () => {
      jest.runOnlyPendingTimers();
    });

    await waitFor(() => {
      // alert should be shown
      expect(queryByTestId('taf-error')).toBeTruthy();
      // no callback should have fired
      expect(props.onUpdateTaf).not.toHaveBeenCalled();
    });

    // user should be able to close the alert
    fireEvent.click(getByText('CLOSE'));
    await waitFor(() => expect(queryByTestId('taf-error')).toBeFalsy());

    // try to save again
    await waitFor(() => {
      fireEvent.click(getByTestId('savedrafttaf'));
    });

    // should show loading
    expect(container.querySelector('[data-testid=taf-loading]')).toBeTruthy();
    expect(mockPostTaf).toHaveBeenCalled();

    await act(async () => {
      jest.runOnlyPendingTimers();
    });

    await waitFor(() => {
      // alert should be shown again
      expect(queryByTestId('taf-error')).toBeTruthy();
      // no callback should have fired
      expect(props.onUpdateTaf).not.toHaveBeenCalled();
    });
  });

  it('should show an error for a cancelled TAF, and remove it after a period of time', async () => {
    const props = {
      tafList: fakeFixedTafList,
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve, reject) => {
        return setTimeout(() => {
          reject(new Error('something went wrong'));
        }, 1000);
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { container, getByTestId, findByText, queryByTestId, findByTestId } =
      render(
        <TafThemeApiProvider createApiFunc={createFakeApi}>
          <TafLayout {...props} />
        </TafThemeApiProvider>,
      );

    await waitFor(() => expect(getByTestId('taf-panel-active')).toBeTruthy());

    // make sure we are in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // click cancel
    const cancelButton = await findByTestId('canceltaf');
    fireEvent.click(cancelButton);

    // click cancel in the confirmation dialog
    const confirmCancelButton = container.parentElement.querySelector(
      '[data-testid=confirmationDialog-confirm]',
    );

    await waitFor(() => fireEvent.click(confirmCancelButton));

    // confirmation dialog should be closed
    await waitFor(() => {
      expect(
        container.parentElement.querySelector(
          '[data-testid=confirmationDialog]',
        ),
      ).toBeFalsy();
    });

    expect(queryByTestId('taf-loading')).toBeTruthy();
    expect(mockPostTaf).toHaveBeenCalled();

    await act(async () => {
      jest.advanceTimersByTime(1000);
    });

    await waitFor(() => {
      expect(queryByTestId('taf-loading')).toBeFalsy();
      // alert should be shown
      expect(queryByTestId('taf-error')).toBeTruthy();
      // no callback should have fired
      expect(props.onUpdateTaf).not.toHaveBeenCalled();
    });

    expect(
      await findByText('Cancelling this TAF failed, please try again'),
    ).toBeTruthy();

    // we should still be in editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // fast forward 10s in time
    await act(async () => {
      jest.advanceTimersByTime(10000);
    });
    await waitFor(() => {
      // alert should be gone
      expect(queryByTestId('taf-error')).toBeFalsy();
    });
  });

  it('should show an error when posting to BE fails', async () => {
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve, reject) => {
        return setTimeout(() => {
          reject(new Error('something went wrong'));
        }, 1000);
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { container, getByTestId, queryByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const tafLocations = getByTestId('location-tabs').querySelectorAll('li');
    // select new taf
    await waitFor(() => {
      fireEvent.click(tafLocations[0]);
    });

    await waitFor(() => expect(getByTestId('taf-panel-active')).toBeTruthy());

    // check editor mode
    expect(getByTestId('switchMode').classList).toContain('Mui-checked');

    // click publish
    const publishButton = container.parentElement.querySelector(
      '[data-testid=publishtaf]',
    );
    await waitFor(() => {
      fireEvent.click(publishButton);
    });

    // click publish in the confirmation dialog
    await waitFor(() => {
      expect(
        container.parentElement.querySelector(
          '[data-testid=confirmationDialog]',
        ),
      ).toBeTruthy();
    });

    await waitFor(() => {
      fireEvent.click(
        container.parentElement.querySelector(
          '[data-testid=confirmationDialog-confirm]',
        ),
      );
      expect(mockPostTaf).not.toHaveBeenCalled();
    });
    // confirmation dialog should be closed
    await waitFor(() => {
      expect(
        container.parentElement.querySelector(
          '[data-testid=confirmationDialog]',
        ),
      ).toBeFalsy();
    });

    // should show loading
    expect(container.querySelector('[data-testid=taf-loading]')).toBeTruthy();
    expect(mockPostTaf).toHaveBeenCalled();

    await act(async () => {
      jest.runOnlyPendingTimers();
    });

    await waitFor(() => {
      // alert should be shown
      expect(queryByTestId('taf-error')).toBeTruthy();
      // no callback should have fired
      expect(props.onUpdateTaf).not.toHaveBeenCalled();
    });
  });

  it('should export a TAF from the TAC overview into the edit form', async () => {
    Element.prototype.scrollTo = jest.fn();

    const props = {
      tafList: [
        fakeAmendmentFixedTaf,
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
        fakePublishedFixedTaf,
      ],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const mockGetTafList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: fakeFixedTafList });
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
      getTafList: mockGetTafList,
    });
    const { queryByTestId, queryAllByTestId, container, findByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => expect(queryByTestId('location-tabs')).toBeTruthy());
    expect(queryAllByTestId('export-tac-button').length).toBeFalsy();

    await waitFor(() => {
      fireEvent.click(queryAllByTestId('status-draft')[0]);
    });

    // check editor mode
    expect(queryByTestId('switchMode').classList).toContain('Mui-checked');

    expect(queryAllByTestId('export-tac-button').length).toBeTruthy();

    const oldWind = container
      .querySelector('[name="baseForecast.wind"]')
      .getAttribute('value');
    expect(oldWind).toEqual('18010');

    await waitFor(() => {
      fireEvent.click(queryAllByTestId('export-tac-button')[2]);
    });

    const newWind = container
      .querySelector('[name="baseForecast.wind"]')
      .getAttribute('value');
    expect(newWind).not.toEqual(oldWind);
    expect(newWind).toEqual('05005');

    // validations should be triggered
    await waitFor(() => {
      expect(
        container.querySelector('[name="changeGroups[0].valid"]').parentElement
          .classList,
      ).toContain('Mui-error');
    });

    // change timeslot without saving
    await waitFor(() => {
      fireEvent.click(queryByTestId('toptab-1'));
    });

    // confirmation should show
    expect(await findByText(CONFIRM_DIALOG_OPTIONS.description)).toBeTruthy();
  });

  it('should export a TAF from the TAC overview into the edit form and not show errors on empty weather and cloud fields', async () => {
    Element.prototype.scrollTo = jest.fn();

    const props = {
      tafList: [
        fakeAmendmentFixedTaf,
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
        fakePublishedFixedTaf,
      ],
      onUpdateTaf: jest.fn(),
    };

    const { queryByTestId, queryAllByTestId, container } = render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => expect(queryByTestId('location-tabs')).toBeTruthy());
    expect(queryAllByTestId('export-tac-button').length).toBeFalsy();

    await waitFor(() => {
      fireEvent.click(queryAllByTestId('status-draft')[0]);
    });
    // check editor mode
    expect(queryByTestId('switchMode').classList).toContain('Mui-checked');

    expect(queryAllByTestId('export-tac-button').length).toBeTruthy();

    const oldWeather = container
      .querySelector('[name="baseForecast.weather.weather1"]')
      .getAttribute('value');
    expect(oldWeather).toEqual('SN');

    const oldCloud = container
      .querySelector('[name="baseForecast.cloud.cloud1"]')
      .getAttribute('value');
    expect(oldCloud).toEqual('BKN035');

    await waitFor(() => {
      fireEvent.click(queryAllByTestId('export-tac-button')[2]);
    });
    const newWeather = container
      .querySelector('[name="baseForecast.weather.weather1"]')
      .getAttribute('value');
    expect(newWeather).not.toEqual(oldWeather);
    expect(newWeather).toEqual('');

    const newCloud = container
      .querySelector('[name="baseForecast.cloud.cloud1"]')
      .getAttribute('value');
    expect(newCloud).not.toEqual(oldCloud);
    expect(newCloud).toEqual('');

    // validations should be triggered
    await waitFor(() => {
      expect(
        container.querySelector('[name="changeGroups[0].valid"]').parentElement
          .classList,
      ).toContain('Mui-error');
    });

    // there should be no errors on weather and cloud
    expect(
      container.querySelector('[name="baseForecast.weather.weather1"]')
        .parentElement.classList,
    ).not.toContain('Mui-error');

    expect(
      container.querySelector('[name="baseForecast.cloud.cloud1"]')
        .parentElement.classList,
    ).not.toContain('Mui-error');
  });

  it('should save a taf as draft when switching tabs', async () => {
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { queryByTestId, container, findByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => expect(queryByTestId('location-tabs')).toBeTruthy());

    // make sure we are in editor mode
    expect(queryByTestId('switchMode').classList).toContain('Mui-checked');

    // change form value
    await waitFor(() => {
      fireEvent.change(container.querySelector('[name="baseForecast.wind"'), {
        target: { value: '01015G35' },
      });
      fireEvent.blur(container.querySelector('[name="baseForecast.wind"'));
    });
    expect(mockPostTaf).not.toHaveBeenCalled();

    // change tabs
    await waitFor(() => {
      fireEvent.click(queryByTestId('toptab-1'));
    });

    expect(mockPostTaf).toHaveBeenCalled();

    // test notification
    expect(queryByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await findByText(getSnackbarMessage(fakeDraftAmendmentFixedTaf, true)),
    ).toBeTruthy();
  });

  it('should clear the alert banner when switching locations', async () => {
    const props = {
      tafList: [
        fakeAmendmentFixedTaf,
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve, reject) => {
        reject(new Error('something went wrong'));
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { queryByTestId, queryAllByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => expect(queryByTestId('location-tabs')).toBeTruthy());

    // open first draft taf
    fireEvent.click(queryAllByTestId('status-draft')[0]);

    // check editor mode
    expect(queryByTestId('switchMode').classList).toContain('Mui-checked');

    // try to save
    await waitFor(() => {
      fireEvent.click(queryByTestId('savedrafttaf'));
    });

    await waitFor(() => {
      // alert should be shown
      expect(queryByTestId('taf-error')).toBeTruthy();
      // no callback should have fired
      expect(props.onUpdateTaf).not.toHaveBeenCalled();
    });

    expect(queryByTestId('snackbarComponent')).toBeFalsy();

    // change location
    const locations = queryByTestId('location-tabs').querySelectorAll('li');
    const lastLocation = locations[locations.length - 1];
    await waitFor(() => {
      fireEvent.click(lastLocation);
    });

    await waitFor(() => {
      // alert should be gone
      expect(queryByTestId('taf-error')).toBeFalsy();
    });
  });

  it('should not save a taf as draft when switching tabs when form is not dirty', async () => {
    const props = {
      tafList: [
        fakeAmendmentFixedTaf,
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { queryByTestId, queryAllByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => expect(queryByTestId('location-tabs')).toBeTruthy());
    // open first draft taf
    fireEvent.click(queryAllByTestId('status-draft')[0]);
    // make sure we are in editor mode
    expect(queryByTestId('switchMode').classList).toContain('Mui-checked');

    // change tabs
    await waitFor(() => {
      fireEvent.click(queryByTestId('toptab-1'));
    });
    expect(mockPostTaf).not.toHaveBeenCalled();
    expect(queryByTestId('snackbarComponent')).toBeFalsy();
  });

  it('should not save a taf as draft when switching tabs when form has errors', async () => {
    const props = {
      tafList: [
        fakeAmendmentFixedTaf,
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { queryByTestId, container, findByText, queryAllByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => expect(queryByTestId('location-tabs')).toBeTruthy());
    // open first draft taf
    fireEvent.click(queryAllByTestId('status-draft')[0]);
    // make sure we are in editor mode
    expect(queryByTestId('switchMode').classList).toContain('Mui-checked');

    // change form value
    await waitFor(() => {
      fireEvent.change(container.querySelector('[name="baseForecast.wind"'), {
        target: { value: 'faultyvalue' },
      });
      fireEvent.blur(container.querySelector('[name="baseForecast.wind"'));
    });
    expect(mockPostTaf).not.toHaveBeenCalled();

    // change tabs
    await waitFor(() => {
      fireEvent.click(queryByTestId('toptab-1'));
    });

    expect(mockPostTaf).not.toHaveBeenCalled();

    // confirmation should show
    expect(await findByText(CONFIRM_DIALOG_OPTIONS.description)).toBeTruthy();

    // click confirm
    await waitFor(() => {
      fireEvent.click(
        container.parentElement.querySelector(
          '[data-testid=confirmationDialog-confirm]',
        ),
      );
    });
    // tab should switch and form not saved
    await waitFor(() => {
      expect(queryByTestId('toptab-1').classList).toContain('Mui-selected');
      expect(queryByTestId('toptab-0').classList).not.toContain('Mui-selected');
    });
    expect(mockPostTaf).not.toHaveBeenCalled();
    expect(queryByTestId('snackbarComponent')).toBeFalsy();
  });

  it('should clear the alert banner when switching locations when automatic saving failed and user decides to exit without saving', async () => {
    const props = {
      tafList: [
        fakeAmendmentFixedTaf,
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve, reject) => {
        reject(new Error('something went wrong'));
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { queryByTestId, container, findByText, queryAllByTestId } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => expect(queryByTestId('location-tabs')).toBeTruthy());

    // open first draft taf
    fireEvent.click(queryAllByTestId('status-draft')[0]);

    // check editor mode
    expect(queryByTestId('switchMode').classList).toContain('Mui-checked');

    // change form value
    await waitFor(() => {
      fireEvent.change(container.querySelector('[name="baseForecast.wind"'), {
        target: { value: '20020' },
      });
      fireEvent.blur(container.querySelector('[name="baseForecast.wind"'));
    });
    expect(mockPostTaf).not.toHaveBeenCalled();

    // change location
    const locations = queryByTestId('location-tabs').querySelectorAll('li');
    const lastLocation = locations[locations.length - 1];
    await waitFor(() => {
      fireEvent.click(lastLocation);
    });

    await waitFor(() => {
      expect(mockPostTaf).toHaveBeenCalled();
      // alert should be shown
      expect(queryByTestId('taf-error')).toBeTruthy();
      // no callback should have fired
      expect(props.onUpdateTaf).not.toHaveBeenCalled();
    });

    // confirmation should show
    expect(await findByText(CONFIRM_DIALOG_OPTIONS.description)).toBeTruthy();

    // click confirm
    await waitFor(() => {
      fireEvent.click(
        container.parentElement.querySelector(
          '[data-testid=confirmationDialog-confirm]',
        ),
      );
    });

    // active location is changed
    await waitFor(() => {
      expect(lastLocation.classList).toContain('Mui-selected');
    });

    // alert should be gone
    expect(queryByTestId('taf-error')).toBeFalsy();
  });

  it('should show snackbar for saving a taf as draft', async () => {
    const props = {
      tafList: [{ ...fakeDraftTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { queryByTestId, findByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(queryByTestId('savedrafttaf'));
    });

    // test notification
    expect(queryByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await findByText(
        getSnackbarMessage({
          ...fakeDraftTaf,
          changeStatusTo: 'DRAFT',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for saving a taf as draft amended', async () => {
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { queryByTestId, findByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(queryByTestId('savedrafttaf'));
    });

    // test notification
    expect(queryByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await findByText(
        getSnackbarMessage({
          ...fakeDraftAmendmentFixedTaf,
          changeStatusTo: 'DRAFT_AMENDED',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for publish a taf', async () => {
    const props = {
      tafList: [{ ...fakeDraftFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { queryByTestId, findByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(queryByTestId('publishtaf'));
    });

    await waitFor(() => {
      fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    });

    // test notification
    expect(queryByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await findByText(
        getSnackbarMessage({
          ...fakeDraftFixedTaf,
          changeStatusTo: 'PUBLISHED',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for correct a taf', async () => {
    const props = {
      tafList: [{ ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { queryByTestId, findByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(queryByTestId('correcttaf'));
    });

    await waitFor(() => {
      fireEvent.click(queryByTestId('publishtaf'));
    });

    await waitFor(() => {
      fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    });

    // test notification
    expect(queryByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await findByText(
        getSnackbarMessage({
          taf: { ...fakeAmendmentFixedTaf.taf, messageType: 'COR' },
          changeStatusTo: 'CORRECTED',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for cancelled a taf', async () => {
    const props = {
      tafList: [{ ...fakePublishedFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { queryByTestId, findByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(queryByTestId('canceltaf'));
    });

    await waitFor(() => {
      fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    });

    // test notification
    expect(queryByTestId('snackbarComponent')).toBeTruthy();

    expect(
      await findByText(
        getSnackbarMessage({
          taf: { ...fakePublishedFixedTaf.taf, messageType: 'CNL' },
          changeStatusTo: 'CANCELLED',
        }),
      ),
    ).toBeTruthy();
  });

  it('should show snackbar for amend a taf', async () => {
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };
    const mockPostTaf = jest.fn(() => {
      return new Promise<void>((resolve) => {
        resolve();
      });
    });
    const createFakeApi = (): unknown => ({
      ...createApi(),
      postTaf: mockPostTaf,
    });
    const { queryByTestId, findByText } = render(
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      fireEvent.click(queryByTestId('publishtaf'));
    });

    await waitFor(() => {
      fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    });

    // test notification
    expect(queryByTestId('snackbarComponent')).toBeTruthy();
    expect(
      await findByText(
        getSnackbarMessage({
          ...fakeDraftAmendmentFixedTaf,
          changeStatusTo: 'AMENDED',
        }),
      ),
    ).toBeTruthy();
  });
});
