/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Box, Card, Grid } from '@mui/material';
import { useConfirmationDialog } from '@opengeoweb/shared';
import { useApiContext } from '@opengeoweb/api';
import { useDispatch } from 'react-redux';
import { snackbarActions } from '@opengeoweb/core';
import moment from 'moment';
import { TacOverview, TacOverviewMobile } from './TacOverview';
import { TopTabs } from './TopTabs';
import { TafFromBackend, TafFromFrontEnd, TimeSlot, Taf } from '../../types';
import { sortTafTimeSlots } from './utils';
import { LocationTabs } from './LocationTabs';
import { TafPanel } from './TafPanel';
import { MODAL_DIALOG_ELEMENT, useIssuesPane } from '../IssuesPane/IssuesPane';
import IssuesPaneWrapper from '../IssuesPane/IssuesPaneWrapper';
import {
  fromActionToStatus,
  isWarning,
  TafError,
  usePostTafRequest,
} from './TafPanel/TafPanel';
import { useTafModuleContext } from '../TafModule/TafModuleProvider';
import { tafActionToDraftAction } from '../TafForm/TafFormButtons/TafFormButtons';
import { LastUpdateTime } from './LastUpdateTime';
import { TafApi } from '../../utils/api';
import { formatBaseTime } from './TopTabs/TopTabs';

export const CONFIRM_DIALOG_OPTIONS = {
  title: 'Whoops',
  description:
    "It's not possible to autosave this TAF due to the presence of issue(s). Are you sure you want to close this TAF? Any changes made will be lost.",
  confirmLabel: 'Exit without saving',
};

export const getTafMessagePrefix = (
  { location, validDateStart, messageType }: Taf,
  newStatus: string,
): string => {
  const formattedDate = formatBaseTime(moment.utc(validDateStart));
  return `TAF ${location} ${formattedDate} ${messageType} was ${newStatus} successfully`;
};

export const getSnackbarMessage = (
  tafFromFrontEnd: TafFromFrontEnd,
  isAutoSaved = false,
): string => {
  const { changeStatusTo, taf } = tafFromFrontEnd;
  if (isAutoSaved) {
    return 'All changes have been saved automatically';
  }
  switch (changeStatusTo) {
    case 'DRAFT':
      return 'Saved as draft';
    case 'DRAFT_AMENDED':
      return 'Saved as draft amended';
    case 'DRAFT_CORRECTED':
      return 'Saved as draft correction';
    case 'PUBLISHED':
      return getTafMessagePrefix(taf, 'published');
    case 'CANCELLED':
      return getTafMessagePrefix(taf, 'cancelled');
    case 'CORRECTED':
      return getTafMessagePrefix(taf, 'corrected');
    case 'AMENDED':
      return getTafMessagePrefix(taf, 'amended');
    default:
      return getTafMessagePrefix(taf, 'saved');
  }
};

interface TafLayoutProps {
  tafList: TafFromBackend[];
  onUpdateTaf: () => void;
  lastUpdateTime?: string;
}

const TafLayout: React.FC<TafLayoutProps> = ({
  tafList = [],
  onUpdateTaf = (): void => {},
  lastUpdateTime,
}: TafLayoutProps) => {
  const { api } = useApiContext<TafApi>();
  const dispatch = useDispatch();
  const confirmDialog = useConfirmationDialog();
  const {
    isIssuesPaneOpen,
    onToggleIssuesPane,
    onCloseIssuesPane,
    startPosition,
  } = useIssuesPane();
  const {
    isLoading,
    doRequest: postTafRequest,
    error: postError,
  } = usePostTafRequest(api.postTaf);
  const { doRequest: patchTafRequest, error: patchError } = usePostTafRequest(
    api.patchTaf,
  );
  const { onValidateForm, tafAction, isFormDirty } = useTafModuleContext();
  const [{ upcoming, current, expired }, updateSortedTafs] = React.useState(
    sortTafTimeSlots(tafList),
  );
  const [lastCurrentTaf] = current.slice(-1);

  const [timeSlot, setTimeSlot] = React.useState<TimeSlot>('UPCOMING');
  const [locations, setLocations] = React.useState<TafFromBackend[]>(upcoming);
  const [activeTafIndex, setActiveTafIndex] = React.useState<number>(0);
  const [isFormDisabled, setIsFormDisabled] = React.useState<boolean>(false);
  const [error, setError] = React.useState<TafError>();

  const showSnackbar = React.useCallback(
    (message) => {
      dispatch(snackbarActions.openSnackbar({ snackbarContent: { message } }));
    },
    [dispatch],
  );

  const postTaf = async (
    data: TafFromFrontEnd,
    isAutoSaved = false,
  ): Promise<void> => {
    try {
      setError(null);
      await postTafRequest(data);
      showSnackbar(getSnackbarMessage(data, isAutoSaved));
      // callback
      onUpdateTaf();
    } catch (error) {
      if (isWarning(error)) {
        onUpdateTaf();
      }
      throw error;
    }
  };

  const patchTaf = async (data: TafFromFrontEnd): Promise<boolean> => {
    try {
      setError(null);
      await patchTafRequest(data);

      // callback
      onUpdateTaf();
      return true;
    } catch (error) {
      return false;
    }
  };

  async function onChangeTab<T>(
    action: (param: T) => void,
    value: T,
  ): Promise<void> {
    if (isFormDirty) {
      try {
        const taf = await onValidateForm();
        await postTaf(
          {
            taf,
            changeStatusTo: fromActionToStatus(
              tafActionToDraftAction(tafAction),
            ),
          } as TafFromFrontEnd,
          true,
        );
        action(value);
      } catch (error) {
        confirmDialog(CONFIRM_DIALOG_OPTIONS).then(() => {
          action(value);
          setError(null);
        });
      }
    } else {
      action(value);
      setError(null);
    }
  }

  const onChangeLocationTab = (index: number): void => {
    onChangeTab(setActiveTafIndex, index);
  };

  const onChangeTimeSlot = (newTimeSlot: TimeSlot): void => {
    onChangeTab(changeTimeSlot, newTimeSlot);
  };

  const changeTimeSlot = (newTimeSlot: TimeSlot): void => {
    setTimeSlot(newTimeSlot);
    setLocations(newTimeSlot === 'ACTIVE' ? current : upcoming);
    setActiveTafIndex(0);
  };

  React.useEffect(() => {
    const newSortedTafs = sortTafTimeSlots(tafList);
    const updatedLocations =
      timeSlot === 'ACTIVE' ? newSortedTafs.current : newSortedTafs.upcoming;
    updateSortedTafs(sortTafTimeSlots(tafList));
    setLocations(updatedLocations);

    // update active index
    const newActiveIndex = updatedLocations.findIndex(
      (location) =>
        location?.taf?.previousId === locations[activeTafIndex]?.taf?.uuid,
    );
    const newIndex = newActiveIndex !== -1 ? newActiveIndex : activeTafIndex;
    setActiveTafIndex(newIndex);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tafList, setLocations]);

  React.useEffect(() => {
    setError(postError);

    const cancelErrorTimeout =
      postError && postError.type === 'CANCELLED'
        ? setTimeout(() => {
            setError(null);
          }, 10000)
        : null;
    return (): void => {
      if (cancelErrorTimeout !== null) {
        clearTimeout(cancelErrorTimeout);
      }
    };
  }, [postError]);

  React.useEffect(() => {
    setError(patchError);
  }, [patchError]);

  if (!tafList) {
    return null;
  }
  const activeTaf = locations[activeTafIndex];

  return (
    <>
      <Box
        id={MODAL_DIALOG_ELEMENT}
        component="div"
        sx={{
          width: '100%',
          height: '100%',
          position: 'absolute',
          overflow: 'hidden',
          pointerEvents: 'none',
        }}
      />
      <Box sx={{ height: '100%', maxHeight: '100%' }}>
        <Box
          sx={{
            minWidth: 320,
            maxWidth: 1272,
            margin: 'auto',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <Grid container spacing={2} sx={{ paddingLeft: 1, paddingRight: 1 }}>
            <Grid item xs={12} sm={9}>
              <TopTabs
                tafFromBackend={lastCurrentTaf}
                timeSlot={timeSlot}
                onChangeTimeSlot={onChangeTimeSlot}
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              {lastUpdateTime && (
                <LastUpdateTime
                  lastUpdateTime={lastUpdateTime}
                  onUpdateTaf={onUpdateTaf}
                />
              )}
            </Grid>
          </Grid>

          <Grid container flexGrow={1} overflow="hidden">
            <Grid
              container
              spacing={2}
              sx={{
                height: '100%',
                paddingLeft: 1,
                paddingRight: 1,
                paddingTop: 2,
                overflow: 'auto',
              }}
            >
              <Grid item xs={3} sm={1}>
                <LocationTabs
                  tafList={locations}
                  activeIndex={activeTafIndex}
                  onChangeTab={onChangeLocationTab}
                  timeSlot={timeSlot}
                />
              </Grid>
              <Grid item xs>
                <Card
                  variant="outlined"
                  sx={{
                    marginBottom: '12px',
                    position: 'relative',
                    minHeight: locations.length * 62 + 32,
                  }}
                >
                  <TacOverviewMobile
                    upcomingTafs={upcoming}
                    currentTafs={current}
                    expiredTafs={expired}
                    activeTaf={activeTaf}
                  />
                  {locations.map((tafFromBackend, index) => (
                    <TafPanel
                      key={tafFromBackend.taf.uuid}
                      activeTafIndex={activeTafIndex}
                      index={index}
                      tafFromBackend={tafFromBackend}
                      isIssuesPaneOpen={isIssuesPaneOpen}
                      setIsIssuesPaneOpen={onToggleIssuesPane}
                      isFormDisabled={isFormDisabled}
                      setIsFormDisabled={setIsFormDisabled}
                      postTaf={postTaf}
                      patchTaf={patchTaf}
                      isLoading={isLoading}
                      error={error}
                    />
                  ))}
                </Card>
              </Grid>
              <Grid
                item
                xs={0}
                sm={3}
                height={{ xs: 0, sm: '100%' }}
                width={{ xs: 0, sm: '100%' }}
              >
                <TacOverview
                  upcomingTafs={upcoming}
                  currentTafs={current}
                  expiredTafs={expired}
                  activeTaf={activeTaf}
                  isFormDisabled={isFormDisabled}
                />
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <IssuesPaneWrapper
        handleClose={onCloseIssuesPane}
        isOpen={isIssuesPaneOpen}
        startPosition={startPosition}
      />
    </>
  );
};

export default TafLayout;
