/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Paper } from '@mui/material';
import { darkTheme } from '@opengeoweb/theme';
import { TafThemeApiProvider } from '../Providers';
import TafLayout from './TafLayout';
import {
  fakeFixedTafList,
  fakeTafList,
} from '../../utils/mockdata/fakeTafList';
import { TafFromBackend } from '../../types';
import { createApi as createFakeApi } from '../../utils/fakeApi';

export default { title: 'components/Taf Layout' };

interface TafLayoutComponentProps {
  tafList?: TafFromBackend[];
  lastUpdateTime?: string;
}

const TafLayoutComponent = ({
  tafList = fakeTafList,
  lastUpdateTime,
}: TafLayoutComponentProps): React.ReactElement => {
  return (
    <Paper sx={{ maxHeight: '100%', height: '100vh', overflow: 'auto' }}>
      <TafLayout
        tafList={tafList}
        onUpdateTaf={(): void => {}}
        lastUpdateTime={lastUpdateTime}
      />
    </Paper>
  );
};

export const DemoTafLayoutLightTheme = (): React.ReactElement => {
  return (
    <TafThemeApiProvider createApiFunc={createFakeApi}>
      <TafLayoutComponent />
    </TafThemeApiProvider>
  );
};

export const DemoTafLayoutDarkTheme = (): React.ReactElement => {
  return (
    <TafThemeApiProvider theme={darkTheme} createApiFunc={createFakeApi}>
      <TafLayoutComponent />
    </TafThemeApiProvider>
  );
};

export const TafLayoutLightSnap = (): React.ReactElement => {
  return (
    <TafThemeApiProvider createApiFunc={createFakeApi}>
      <TafLayoutComponent tafList={fakeFixedTafList} lastUpdateTime="11:12" />
    </TafThemeApiProvider>
  );
};

export const TafLayoutDarkSnap = (): React.ReactElement => {
  return (
    <TafThemeApiProvider theme={darkTheme} createApiFunc={createFakeApi}>
      <TafLayoutComponent tafList={fakeFixedTafList} lastUpdateTime="11:12" />
    </TafThemeApiProvider>
  );
};

TafLayoutLightSnap.storyName = 'TafLayout light (takeSnapshot)';
TafLayoutDarkSnap.storyName = 'TafLayout dark (takeSnapshot)';
