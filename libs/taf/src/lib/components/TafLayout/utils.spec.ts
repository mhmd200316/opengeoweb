/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Taf, TafFormData, TafFromBackend } from '../../types';
import {
  getBaseTime,
  prepareTafExportValues,
  sortTafsOnLocation,
  sortTafTimeSlots,
} from './utils';

import {
  fakeAmendmentFixedTaf,
  fakeCancelledFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeExpiredTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
} from '../../utils/mockdata/fakeTafList';
import { emptyBaseForecast } from '../TafForm/utils';
import { WeatherPhenomena } from '../../utils/weatherPhenomena';

describe('components/TafLayout/utils', () => {
  describe('sortTafsOnLocation', () => {
    it('should sort tafList based on location', () => {
      const expextedResult = [
        { taf: { location: 'EHAM' } },
        { taf: { location: 'EHRD' } },
        { taf: { location: 'EHLE' } },
        { taf: { location: 'EHLE' } },
        { taf: { location: 'EHGG' } },
        { taf: { location: 'EHGG' } },
        { taf: { location: 'EHBK' } },
        { taf: { location: 'TNCB' } },
      ];
      expect(
        sortTafsOnLocation([
          { taf: { location: 'EHLE' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHLE' } },
          { taf: { location: 'TNCB' } },
          { taf: { location: 'EHRD' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHAM' } },
          { taf: { location: 'EHBK' } },
        ] as TafFromBackend[]),
      ).toEqual(expextedResult);
      expect(
        sortTafsOnLocation([
          { taf: { location: 'EHLE' } },
          { taf: { location: 'TNCB' } },
          { taf: { location: 'EHRD' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHLE' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHAM' } },
          { taf: { location: 'EHBK' } },
        ] as TafFromBackend[]),
      ).toEqual(expextedResult);
      expect(
        sortTafsOnLocation([
          { taf: { location: 'TNCB' } },
          { taf: { location: 'EHRD' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHAM' } },
          { taf: { location: 'EHBK' } },
          { taf: { location: 'EHLE' } },
          { taf: { location: 'EHGG' } },
          { taf: { location: 'EHLE' } },
        ] as TafFromBackend[]),
      ).toEqual(expextedResult);
    });
  });

  describe('getBaseTime', () => {
    it('should get current and nextbasetime of taf', () => {
      const result = getBaseTime(
        fakeNewFixedTaf.taf.baseTime,
        fakeNewFixedTaf.taf.location,
      );
      expect(result.currentBaseTime.format()).toEqual('2022-01-06T12:00:00Z');
      expect(result.nextBaseTime.format()).toEqual('2022-01-06T18:00:00Z');

      const result2 = getBaseTime(
        fakeDraftFixedTaf.taf.baseTime,
        fakeDraftFixedTaf.taf.location,
      );
      expect(result2.currentBaseTime.format()).toEqual('2022-01-06T18:00:00Z');
      expect(result2.nextBaseTime.format()).toEqual('2022-01-07T00:00:00Z');

      const result3 = getBaseTime(
        fakePublishedFixedTaf.taf.baseTime,
        fakePublishedFixedTaf.taf.location,
      );
      expect(result3.currentBaseTime.format()).toEqual('2022-01-06T12:00:00Z');
      expect(result3.nextBaseTime.format()).toEqual('2022-01-06T18:00:00Z');
    });
  });

  describe('sortTafTimeSlots', () => {
    it('should sort tafList into current, upcoming and expired tafs', () => {
      const tafList = [
        fakeNewFixedTaf,
        fakeDraftFixedTaf,
        fakePublishedFixedTaf,
        fakeDraftAmendmentFixedTaf,
        fakeAmendmentFixedTaf,
        fakeExpiredTaf,
        fakeCancelledFixedTaf,
      ];

      const result = sortTafTimeSlots(tafList);
      // dates are fixed, so current is empty
      expect(result.current).toEqual([]);
      expect(result.upcoming).toEqual([
        fakeAmendmentFixedTaf,
        fakeDraftAmendmentFixedTaf,
        fakeDraftFixedTaf,
        fakePublishedFixedTaf,
        fakeNewFixedTaf,
        fakeCancelledFixedTaf,
      ]);

      expect(result.expired).toEqual([fakeExpiredTaf]);
    });
  });

  describe('prepareTafExportValues', () => {
    it('should copy only editable form values to export', () => {
      const currentTaf: TafFormData = {
        type: 'NORMAL',
        messageType: 'ORG',
        status: 'NEW',
        uuid: 'testTafExport',
        baseTime: `2022-01-06T12:00:00Z`,
        validDateStart: `2022-01-06T12:00:00Z`,
        validDateEnd: `2022-01-07T18:00:00Z`,
        location: 'EHBK',
        baseForecast: { valid: '0612/0718' },
        changeGroups: [],
      };
      const tafToCopy = fakeExpiredTaf.taf;
      const result = prepareTafExportValues(tafToCopy, currentTaf);
      expect(result).toEqual({
        ...currentTaf,
        baseForecast: {
          ...emptyBaseForecast,
          valid: currentTaf.baseForecast.valid,
          cloud: {
            cloud1: 'FEW015',
            cloud2: 'SCT025',
            cloud3: 'BKN030',
            cloud4: 'OVC050CB',
          },
          visibility: '0500',
          weather: {
            weather1: 'BCFG',
            weather2: 'SNDZ',
            weather3: '',
          },
          wind: '05005',
        },
        changeGroups: [
          {
            change: 'BECMG',
            valid: '0709/0712',
            visibility: '1000',
            weather: {
              weather1: 'BR',
            },
          },
          {
            change: 'FM',
            cloud: {
              cloud1: 'FEW015',
              cloud2: 'BKN030',
            },
            valid: '071500',
            visibility: '6000',
            weather: {
              weather1: 'DZ',
            },
          },
        ],
      });
    });

    it('should return an empty changeGroups array when the taf to copy does not have changeGroups', () => {
      const currentTaf: TafFormData = {
        type: 'NORMAL',
        messageType: 'ORG',
        status: 'AMENDED',
        uuid: 'testTafExport',
        baseTime: `2022-01-06T12:00:00Z`,
        validDateStart: `2022-01-06T12:00:00Z`,
        validDateEnd: `2022-01-07T18:00:00Z`,
        location: 'EHBK',
        baseForecast: {
          valid: '0612/0614',
          wind: '10010',
          visibility: '1000',
          weather: {
            weather1: 'RA',
          },
          cloud: {
            cloud1: 'FEW001',
          },
        },
        changeGroups: [
          {
            change: 'BECMG',
            valid: '0612/0718',
            visibility: '1000',
            weather: { weather1: 'BR' },
          },
        ],
      };

      const tafToCopy: Taf = {
        type: 'NORMAL',
        messageType: 'AMD',
        status: 'DRAFT',
        uuid: 'copyTaf',
        issueDate: '2020-12-07T06:00:00Z',
        baseTime: '2020-12-07T06:00:00Z',
        validDateStart: '2020-12-07T06:00:00Z',
        validDateEnd: '2020-12-08T12:00:00Z',
        location: 'EHAM',
        baseForecast: {
          valid: {
            start: '2020-12-07T06:00:00Z',
            end: '2020-12-08T12:00:00Z',
          },
          wind: { direction: 300, speed: 30, unit: 'KT' },
          visibility: { range: 2000, unit: 'M' },
        },
      };

      const result = prepareTafExportValues(tafToCopy, currentTaf);
      expect(result).toEqual({
        ...currentTaf,
        baseForecast: {
          ...emptyBaseForecast,
          valid: currentTaf.baseForecast.valid,
          wind: '30030',
          visibility: '2000',
        },
        changeGroups: [],
      });
    });

    it('should return empty values for empty baseForecast weather and cloud fields', () => {
      const currentTaf: TafFormData = {
        type: 'NORMAL',
        messageType: 'ORG',
        status: 'AMENDED',
        uuid: 'testTafExport',
        baseTime: `2022-01-06T12:00:00Z`,
        validDateStart: `2022-01-06T12:00:00Z`,
        validDateEnd: `2022-01-07T18:00:00Z`,
        location: 'EHBK',
        baseForecast: {
          valid: '0612/0614',
          wind: '10010',
          visibility: '1000',
          weather: {
            weather1: 'RA',
            weather2: 'SS',
            weather3: 'SN',
          },
          cloud: {
            cloud1: 'FEW001',
            cloud2: 'BKN002',
            cloud3: 'BKN005',
            cloud4: 'BKN006CB',
          },
        },
        changeGroups: [
          {
            change: 'BECMG',
            valid: '0612/0718',
            visibility: '1000',
            weather: {
              weather1: 'RA',
              weather2: 'SS',
              weather3: 'SN',
            },
            cloud: {
              cloud1: 'FEW001',
              cloud2: 'BKN002',
              cloud3: 'BKN005',
              cloud4: 'BKN006CB',
            },
          },
        ],
      };

      const tafToCopy: Taf = {
        type: 'NORMAL',
        messageType: 'AMD',
        status: 'DRAFT',
        uuid: 'copyTaf',
        issueDate: '2020-12-07T06:00:00Z',
        baseTime: '2020-12-07T06:00:00Z',
        validDateStart: '2020-12-07T06:00:00Z',
        validDateEnd: '2020-12-08T12:00:00Z',
        location: 'EHAM',
        baseForecast: {
          valid: {
            start: '2020-12-07T06:00:00Z',
            end: '2020-12-08T12:00:00Z',
          },
          wind: { direction: 300, speed: 30, unit: 'KT' },
          visibility: { range: 2000, unit: 'M' },
          weather: {
            weather1: 'SN' as WeatherPhenomena,
          },
          cloud: {
            cloud1: {
              coverage: 'BKN',
              height: 1,
            },
          },
        },
        changeGroups: [
          {
            change: 'BECMG',
            valid: {
              start: '2020-12-07T06:00:00Z',
              end: '2020-12-07T08:00:00Z',
            },
            visibility: { range: 1000, unit: 'M' },
            weather: {
              weather1: 'SN' as WeatherPhenomena,
            },
            cloud: {
              cloud1: {
                coverage: 'BKN',
                height: 1,
              },
            },
          },
        ],
      };

      const result = prepareTafExportValues(tafToCopy, currentTaf);
      expect(result).toEqual({
        ...currentTaf,
        baseForecast: {
          valid: currentTaf.baseForecast.valid,
          wind: '30030',
          visibility: '2000',
          weather: {
            ...emptyBaseForecast.weather,
            weather1: 'SN',
          },
          cloud: {
            ...emptyBaseForecast.cloud,
            cloud1: 'BKN001',
          },
        },
        changeGroups: [
          {
            change: 'BECMG',
            cloud: {
              cloud1: 'BKN001',
            },
            valid: '0706/0708',
            visibility: '1000',
            weather: {
              weather1: 'SN',
            },
          },
        ],
      });
    });
  });
});
