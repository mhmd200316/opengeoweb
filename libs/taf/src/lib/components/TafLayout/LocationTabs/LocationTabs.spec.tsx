/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render } from '@testing-library/react';

import { getInitials } from '@opengeoweb/shared';
import LocationTabs from './LocationTabs';
import { TafThemeApiProvider } from '../../Providers';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import { TimeSlot } from '../../../types';
import { sortTafsOnLocation } from '../utils';

describe('components/TafLayout/LocationTabs', () => {
  it('should display a list of tafs', async () => {
    const tafList = [
      fakeNewFixedTaf,
      fakeDraftFixedTaf,
      fakePublishedFixedTaf,
      fakePublishedTaf,
    ];
    const props = {
      tafList,
      activeIndex: 0,
      timeSlot: 'UPCOMING' as TimeSlot,
      onChangeTab: jest.fn(),
    };
    const { getByRole } = render(
      <TafThemeApiProvider>
        <LocationTabs {...props} />
      </TafThemeApiProvider>,
    );

    const menu = getByRole('menu');
    const locationList = menu.querySelectorAll('li');
    expect(locationList).toHaveLength(tafList.length);

    locationList.forEach((location, index) => {
      if (tafList[index].editor) {
        // If taf has an editor the Avatar should have correct label
        expect(
          location.querySelector('[data-testid="avatar"]').textContent,
        ).toEqual(getInitials(tafList[index].editor));
      }
      if (!tafList[index].editor) {
        // If taf does not have an editor the avatar should not be shown
        expect(location.querySelector('[data-testid="avatar"]')).toBeFalsy();
      }
      // Location should have correct label
      expect(
        location.querySelector('[data-testid="taf-location"]').textContent,
      ).toEqual(tafList[index].taf.location);
    });

    // should be able to click
    fireEvent.click(locationList[1]);
    expect(props.onChangeTab).toHaveBeenCalledWith(1);
  });

  it('should display a list of tafs in UPCOMING timeslot', async () => {
    const tafList = [fakeNewFixedTaf, fakeDraftFixedTaf, fakePublishedFixedTaf];
    const { getByRole } = render(
      <TafThemeApiProvider>
        <LocationTabs
          tafList={sortTafsOnLocation(tafList)}
          activeIndex={0}
          timeSlot="UPCOMING"
        />
      </TafThemeApiProvider>,
    );

    const menu = getByRole('menu');
    const locationList = menu.querySelectorAll('li');

    // should have correct icons
    expect(
      locationList[0].querySelector('[data-testid="status-draft"]'),
    ).toBeTruthy();
    expect(
      locationList[1].querySelector('[data-testid="status-upcoming"]'),
    ).toBeTruthy();
    expect(
      locationList[2].querySelector('[data-testid="status-draft"]'),
    ).toBeFalsy();
    expect(
      locationList[2].querySelector('[data-testid="status-upcoming"]'),
    ).toBeFalsy();
  });

  it('should display a list of tafs in ACTIVE timeslot', async () => {
    const tafList = [fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf];
    const { getByRole } = render(
      <TafThemeApiProvider>
        <LocationTabs tafList={tafList} activeIndex={0} timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );

    const menu = getByRole('menu');
    const locationList = menu.querySelectorAll('li');

    // should have correct icons
    expect(
      locationList[0].querySelector('[data-testid="status-draft"]'),
    ).toBeTruthy();
    expect(
      locationList[1].querySelector('[data-testid="status-active"]'),
    ).toBeTruthy();
  });
});
