/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { Tab, Tabs } from '@mui/material';
import { Moment } from 'moment';
import React from 'react';
import { TafFromBackend, TimeSlot } from '../../../types';
import { getBaseTime, muiTabA11yProps } from '../utils';

type TimeSlotTab = {
  title: string;
  timeSlot: TimeSlot;
};

interface TopTabsProps {
  timeSlot: TimeSlot;
  tafFromBackend: TafFromBackend;
  onChangeTimeSlot?: (timeSlot: TimeSlot) => void;
}

export const formatBaseTime = (time: Moment): string =>
  time.format('HH:mm DD MMM');

const TopTabs: React.FC<TopTabsProps> = ({
  timeSlot,
  onChangeTimeSlot = (): void => {},
  tafFromBackend = {
    taf: {
      baseTime: '2022-01-06T12:00:00Z',
      location: 'EHAM',
    },
  },
}) => {
  if (!tafFromBackend) {
    return null;
  }

  const activeTab = timeSlot === 'UPCOMING' ? 0 : 1;
  const { currentBaseTime, nextBaseTime } = getBaseTime(
    tafFromBackend.taf.baseTime,
    tafFromBackend.taf.location,
  );

  const tabs: TimeSlotTab[] = [
    {
      title: `Upcoming ${formatBaseTime(nextBaseTime)}`,
      timeSlot: 'UPCOMING',
    },
    {
      title: `Current ${formatBaseTime(currentBaseTime)}`,
      timeSlot: 'ACTIVE',
    },
  ];

  const onChangeTab = (
    _event: React.SyntheticEvent,
    newValue: number,
  ): void => {
    onChangeTimeSlot(tabs[newValue].timeSlot);
  };

  return (
    <Tabs
      value={activeTab}
      onChange={onChangeTab}
      aria-label="timeslot-tabs"
      data-testid="timeslot-tabs"
      variant="fullWidth"
    >
      {tabs.map((tab, index) => (
        <Tab
          key={tab.title}
          label={tab.title}
          data-testid={`toptab-${index}`}
          sx={{ fontSize: 16 }}
          {...muiTabA11yProps(index)}
        />
      ))}
    </Tabs>
  );
};

export default TopTabs;
