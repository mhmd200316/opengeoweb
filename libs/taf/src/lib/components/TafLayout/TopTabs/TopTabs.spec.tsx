/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render } from '@testing-library/react';

import TopTabs from './TopTabs';

import { TimeSlot } from '../../../types';
import { fakeNewFixedTaf } from '../../../utils/mockdata/fakeTafList';

describe('components/TafLayout/TopTabs/TopTabs', () => {
  it('should render top tabs', async () => {
    const props = {
      timeSlot: 'UPCOMING' as TimeSlot,
      tafFromBackend: fakeNewFixedTaf,
      onChangeTimeSlot: jest.fn(),
    };

    const { findAllByRole } = render(<TopTabs {...props} />);

    const tabs = await findAllByRole('tab');
    expect(tabs[0].className).toContain('Mui-selected');
    expect(tabs[1].className).not.toContain('Mui-selected');

    fireEvent.click(tabs[1]);

    expect(props.onChangeTimeSlot).toHaveBeenCalledWith('ACTIVE');
  });

  it('should render top tabs with timeslot ACTIVE as active', async () => {
    const props = {
      timeSlot: 'ACTIVE' as TimeSlot,
      tafFromBackend: fakeNewFixedTaf,
      onChangeTimeSlot: jest.fn(),
    };

    const { findAllByRole } = render(<TopTabs {...props} />);

    const tabs = await findAllByRole('tab');
    expect(tabs[0].className).not.toContain('Mui-selected');
    expect(tabs[1].className).toContain('Mui-selected');

    fireEvent.click(tabs[0]);

    expect(props.onChangeTimeSlot).toHaveBeenCalledWith('UPCOMING');
  });
});
