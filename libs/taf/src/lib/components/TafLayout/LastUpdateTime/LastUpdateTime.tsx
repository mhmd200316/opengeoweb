/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Box, Tooltip, Typography } from '@mui/material';
import { ToolButton } from '@opengeoweb/shared';
import { Cached } from '@opengeoweb/theme';

export const getLastUpdateTitle = (time: string): string =>
  `Last updated ${time}`;

interface LastUpdateTimeProps {
  onUpdateTaf: () => void;
  lastUpdateTime?: string;
}

const LastUpdateTime: React.FC<LastUpdateTimeProps> = ({
  onUpdateTaf = (): void => {},
  lastUpdateTime,
}: LastUpdateTimeProps) => {
  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        height: '100%',
        justifyContent: 'flex-end',
      }}
    >
      <Typography
        sx={{
          color: 'geowebColors.captions.captionStatus.rgba',
          fontSize: '10px',
          lineHeight: '1.6',
          letterSpacing: '0.33px',
        }}
        data-testid="lastupdated-time"
      >
        {getLastUpdateTitle(lastUpdateTime)}
      </Typography>
      <Tooltip title="Click to refresh">
        <ToolButton
          data-testid="updateTafBtn"
          onClick={onUpdateTaf}
          sx={{ marginRight: 1, marginLeft: 1 }}
        >
          <Cached />
        </ToolButton>
      </Tooltip>
    </Box>
  );
};

export default LastUpdateTime;
