/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Card, Grid } from '@mui/material';
import { darkTheme } from '@opengeoweb/theme';
import { TafThemeApiProvider } from '../../Providers';
import TafPanel, { TafError } from './TafPanel';
import { fakeDraftFixedTaf } from '../../../utils/mockdata/fakeTafList';
import { TafFromBackend } from '../../../types';
import { createApi as createFakeApi } from '../../../utils/fakeApi';

export default { title: 'components/Taf Layout/TafPanel' };

const mockSaveError: TafError = {
  type: 'SERVER',
  title: 'An error has occurred while saving, please try again',
  message: 'Status change not allowed',
};

const mockTakeOverWarning: TafError = {
  type: 'WARNING',
  title: 'You are no longer the editor for this TAF',
};

interface TafPanelComponentProps {
  tafFromBackend?: TafFromBackend;
  disabled?: boolean;
  error?: TafError;
}

const TafPanelComponent = ({
  tafFromBackend = fakeDraftFixedTaf,
  disabled = false,
  error,
}: TafPanelComponentProps): React.ReactElement => {
  return (
    <Grid
      container
      sx={{
        position: 'absolute',
        width: '832px',
      }}
    >
      <Grid item xs={12}>
        <Card>
          <TafPanel
            tafFromBackend={tafFromBackend}
            activeTafIndex={0}
            index={0}
            isFormDisabled={disabled}
            error={error}
          />
        </Card>
      </Grid>
    </Grid>
  );
};

export const TafPanelSaveErrorLightSnap = (): React.ReactElement => {
  return (
    <TafThemeApiProvider createApiFunc={createFakeApi}>
      <TafPanelComponent error={mockSaveError} />
    </TafThemeApiProvider>
  );
};

export const TafPanelSaveErrorDarkSnap = (): React.ReactElement => {
  return (
    <TafThemeApiProvider theme={darkTheme} createApiFunc={createFakeApi}>
      <TafPanelComponent error={mockSaveError} />
    </TafThemeApiProvider>
  );
};

TafPanelSaveErrorLightSnap.storyName =
  'TafPanel save error light (takeSnapshot)';
TafPanelSaveErrorDarkSnap.storyName = 'TafPanel save error dark (takeSnapshot)';

export const TafPanelTakeOverErrorLightSnap = (): React.ReactElement => {
  return (
    <TafThemeApiProvider createApiFunc={createFakeApi}>
      <TafPanelComponent
        tafFromBackend={{ ...fakeDraftFixedTaf, editor: 'other.user' }}
        error={mockTakeOverWarning}
        disabled
      />
    </TafThemeApiProvider>
  );
};

export const TafPanelTakeOverErrorDarkSnap = (): React.ReactElement => {
  return (
    <TafThemeApiProvider theme={darkTheme} createApiFunc={createFakeApi}>
      <TafPanelComponent
        tafFromBackend={{ ...fakeDraftFixedTaf, editor: 'other.user' }}
        error={mockTakeOverWarning}
        disabled
      />
    </TafThemeApiProvider>
  );
};

TafPanelTakeOverErrorLightSnap.storyName =
  'TafPanel take over error light (takeSnapshot)';
TafPanelTakeOverErrorDarkSnap.storyName =
  'TafPanel take over error dark (takeSnapshot)';
