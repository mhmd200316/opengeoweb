/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, waitFor } from '@testing-library/react';

import {
  fakeDraftFixedTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';

import TafPanel from './TafPanel';
import { TafThemeApiProvider } from '../../Providers';

describe('components/TafLayout/TafPanel/TafPanel', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should show correct view mode of form', async () => {
    const props = {
      tafFromBackend: fakePublishedFixedTaf,
      activeTafIndex: 0,
      index: 0,
      postTaf: jest.fn(),
      isFormDisabled: true,
    };
    const { container } = render(
      <TafThemeApiProvider>
        <TafPanel {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(
        container
          .querySelector('[name="baseForecast.wind"]')
          .hasAttribute('disabled'),
      ).toBeTruthy();
    });
  });

  it('should show correct edit mode of form', async () => {
    const props = {
      tafFromBackend: fakeDraftFixedTaf,
      activeTafIndex: 0,
      index: 0,
      postTaf: jest.fn(),
    };
    const { container } = render(
      <TafThemeApiProvider>
        <TafPanel {...props} />
      </TafThemeApiProvider>,
    );

    await waitFor(() => {
      expect(
        container
          .querySelector('[name="baseForecast.wind"]')
          .hasAttribute('disabled'),
      ).toBeFalsy();
    });
  });
});
