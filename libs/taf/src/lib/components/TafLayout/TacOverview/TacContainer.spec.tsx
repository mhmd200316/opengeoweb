/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, waitFor } from '@testing-library/react';

import TacContainer from './TacContainer';
import { TafThemeApiProvider } from '../../Providers';
import { getLocationSetting } from '../utils';
import {
  fakeExpiredTaf,
  fakeNewTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import { fakeTestTac } from '../../../utils/__mocks__/api';
import { TAC_NOT_AVAILABLE } from '../../TafForm/utils';
import { createApi } from '../../../utils/fakeApi';
import { TafApi } from '../../../utils/api';

jest.mock('../../../utils/api');

describe('components/TafLayout/TacOverview/TacContainer', () => {
  it('should display a TacContainer for a new taf without export button', async () => {
    const { container, getByTestId, findByText, queryByTestId } = render(
      <TafThemeApiProvider>
        <TacContainer taf={fakeNewTaf.taf} />
      </TafThemeApiProvider>,
    );

    expect(
      container.querySelector(`[data-location="${fakeNewTaf.taf.location}"]`)
        .className,
    ).not.toContain('active');

    expect(getByTestId('tafLocation').innerHTML).toEqual(
      fakeNewTaf.taf.location,
    );
    expect(getByTestId('tafLocationLabel').innerHTML).toEqual(
      getLocationSetting(fakeNewTaf.taf.location).label,
    );
    expect(await findByText(TAC_NOT_AVAILABLE)).toBeTruthy();
    expect(queryByTestId('export-tac-button')).toBeFalsy();
  });

  it('should display a TacContainer for an expired taf with correct icon and export button', async () => {
    const { getByTestId, findByText } = render(
      <TafThemeApiProvider>
        <TacContainer taf={fakeExpiredTaf.taf} />
      </TafThemeApiProvider>,
    );

    expect(getByTestId('tafLocation').innerHTML).toEqual(
      fakeExpiredTaf.taf.location,
    );
    expect(getByTestId('tafLocationLabel').innerHTML).toEqual(
      getLocationSetting(fakeExpiredTaf.taf.location).label,
    );
    expect(await findByText(fakeTestTac)).toBeTruthy();
    expect(getByTestId('status-expired')).toBeTruthy();
    expect(getByTestId('export-tac-button')).toBeTruthy();
  });

  it('should display an active TacContainer', async () => {
    const { container } = render(
      <TafThemeApiProvider>
        <TacContainer taf={fakeExpiredTaf.taf} isActive />
      </TafThemeApiProvider>,
    );
    await waitFor(() => {
      expect(
        container.querySelector(
          `[data-location="${fakeExpiredTaf.taf.location}"]`,
        ).className,
      ).toContain('active');
    });
  });

  it('should fetch a new TAC when active taf changes', async () => {
    const props = {
      taf: fakeExpiredTaf.taf,
      isActive: false,
    };

    const mockGetTac = jest.fn(() => {
      return new Promise<{ data: string }>((resolve) => {
        resolve({ data: fakeTestTac });
      });
    });

    const createFakeApi = (): TafApi => ({
      ...createApi(),
      getTAC: mockGetTac,
    });

    const Wrapper: React.FC<{ children: React.ReactNode }> = ({ children }) => (
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        {children}
      </TafThemeApiProvider>
    );

    const { rerender } = render(
      <Wrapper>
        <TacContainer {...props} />
      </Wrapper>,
    );

    await waitFor(() => {
      expect(mockGetTac).toHaveBeenCalledTimes(1);
    });

    const newProps = {
      taf: fakePublishedFixedTaf.taf,
      isActive: true,
    };

    await waitFor(() =>
      rerender(
        <Wrapper>
          <TacContainer {...newProps} />
        </Wrapper>,
      ),
    );
    expect(mockGetTac).toHaveBeenCalledTimes(2);
  });
  it('should not display the export button when form is disabled', async () => {
    const { findByText, queryByTestId } = render(
      <TafThemeApiProvider>
        <TacContainer taf={fakePublishedFixedTaf.taf} isFormDisabled />
      </TafThemeApiProvider>,
    );
    expect(await findByText(fakeTestTac)).toBeTruthy();
    expect(queryByTestId('export-tac-button')).toBeFalsy();
  });
});
