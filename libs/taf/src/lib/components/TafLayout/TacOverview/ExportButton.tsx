/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Tooltip } from '@mui/material';
import { ToolButton } from '@opengeoweb/shared';
import { Export } from '@opengeoweb/theme';

export const TOOLTIP_TITLE = 'Import TAC';

interface ExportButtonProps {
  onClick: () => void;
  isHidden: boolean;
}

const ExportButton: React.FC<ExportButtonProps> = ({
  onClick,
  isHidden,
}: ExportButtonProps) => {
  return isHidden ? null : (
    <Tooltip title={TOOLTIP_TITLE} placement="left">
      <ToolButton onClick={onClick} data-testid="export-tac-button">
        <Export />
      </ToolButton>
    </Tooltip>
  );
};

export default ExportButton;
