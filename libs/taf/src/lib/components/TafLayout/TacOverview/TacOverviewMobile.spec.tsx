/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import TacOverviewMobile from './TacOverviewMobile';
import { TafThemeApiProvider } from '../../Providers';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import { fakeTestTac } from '../../../utils/__mocks__/api';

jest.mock('../../../utils/api');

describe('components/TafLayout/TacOverviewMobile', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should open and close the mobile TAC Overview', async () => {
    Element.prototype.scrollTo = jest.fn();
    const upcomingTafs = [
      fakeNewFixedTaf,
      fakeDraftFixedTaf,
      fakePublishedFixedTaf,
    ];
    const currentTafs = [fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf];
    const expiredTafs = upcomingTafs;
    const props = {
      upcomingTafs,
      currentTafs,
      expiredTafs,
    };
    const { getByTestId, findAllByText, queryByTestId, getAllByText } = render(
      <TafThemeApiProvider>
        <TacOverviewMobile {...props} />
      </TafThemeApiProvider>,
    );

    expect(queryByTestId('tac-overview-mobile')).toBeFalsy();
    fireEvent.click(getByTestId('tac-overview-mobile-button'));

    expect(queryByTestId('tac-overview-mobile')).toBeTruthy();
    expect(getAllByText('TAC Overview').length).toBe(1);

    expect(await findAllByText(fakeTestTac)).toBeTruthy();

    expect(getByTestId('tab-panel-0').children).toHaveLength(
      upcomingTafs.length,
    );
    expect(getByTestId('tab-panel-1').children).toHaveLength(0);
    jest.runOnlyPendingTimers();

    await waitFor(() =>
      expect(Element.prototype.scrollTo).toHaveBeenCalledTimes(1),
    );

    fireEvent.click(getByTestId('CloseIcon'));
    await waitFor(() =>
      expect(queryByTestId('tac-overview-mobile')).toBeFalsy(),
    );
  });
});
