/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import ExportButton, { TOOLTIP_TITLE } from './ExportButton';

describe('components/TafLayout/TacOverview/ExportButton', () => {
  it('should not show button when hidden', () => {
    const props = {
      onClick: jest.fn(),
      isHidden: true,
    };
    const { queryByTestId } = render(<ExportButton {...props} />);
    expect(queryByTestId('export-tac-button')).toBeFalsy();
  });
  it('should show button and handle onClick when clicked', () => {
    const props = {
      onClick: jest.fn(),
      isHidden: false,
    };
    const { queryByTestId } = render(<ExportButton {...props} />);
    expect(queryByTestId('export-tac-button')).toBeTruthy();
    fireEvent.click(queryByTestId('export-tac-button'));
    expect(props.onClick).toHaveBeenCalled();
  });

  it('should show and hide tooltip', async () => {
    const props = {
      onClick: jest.fn(),
      isHidden: false,
    };
    const { queryByTestId, queryByText } = render(<ExportButton {...props} />);
    expect(queryByTestId('export-tac-button')).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(queryByText(TOOLTIP_TITLE)).toBeFalsy();

    fireEvent.mouseOver(queryByTestId('export-tac-button'));
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(queryByText(TOOLTIP_TITLE)).toBeTruthy();

    fireEvent.mouseOut(queryByTestId('export-tac-button'));

    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeFalsy();
      expect(queryByText(TOOLTIP_TITLE)).toBeFalsy();
    });
  });
});
