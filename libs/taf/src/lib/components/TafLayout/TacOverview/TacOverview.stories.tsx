/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import { Box } from '@mui/material';
import { TafThemeApiProvider } from '../../Providers';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import TacOverview from './TacOverview';
import { fakeTafTac, createApi as createFakeApi } from '../../../utils/fakeApi';

export default { title: 'components/Taf Layout/TacOverview' };

const TacOverviewDemo = (): React.ReactElement => {
  return (
    <Box sx={{ padding: '10px' }}>
      <TacOverview
        upcomingTafs={[
          fakeDraftFixedTaf,
          fakePublishedFixedTaf,
          fakeNewFixedTaf,
        ]}
        currentTafs={[fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf]}
        expiredTafs={[]}
        activeTaf={fakePublishedFixedTaf}
      />
    </Box>
  );
};

export const TacOverviewWithError = (): React.ReactElement => {
  return (
    <div style={{ width: '300px', height: '460px' }}>
      <TafThemeApiProvider>
        <TacOverviewDemo />
      </TafThemeApiProvider>
    </div>
  );
};

const tacEHLE =
  'TAF EHLE 2912/3018 06007KT 7000 BKN006\nBECMG 2914/2917 9999 FEW025\nTEMPO 3006/3010 BKN012=';
const tacEHAM =
  'TAF EHAM 2906/3012 07007KT 7000 BKN006\nBECMG 2908/2911 9999 FEW025\nBECMG 2915/2918 04012KT\nBECMG 2921/2924 03005KT\nTEMPO 3006/3010 BKN012\nBECMG 3009/3012 04011KT=';
const tacEHRD =
  'TAF EHRD 2906/3012 07007KT 0600 FG VV000\nBECMG 2906/2908 3000 BR BKN003\nBECMG 2908/2911 9999 NSW FEW025\nBECMG 2915/2918 04012KT\nBECMG 3000/3003 03005KT=';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const fakeGetTac = (): any => {
  return {
    getTAC: (): Promise<{ data: string }> => {
      return new Promise((resolve) => {
        const randomNumber = Math.floor(Math.random() * 4) + 1;
        switch (randomNumber) {
          case 1:
            return resolve({
              data: tacEHAM,
            });
          case 2:
            return resolve({
              data: tacEHRD,
            });
          case 3:
            return resolve({
              data: tacEHLE,
            });
          default:
            return resolve({
              data: fakeTafTac,
            });
        }
      });
    },
  };
};

export const TacOverviewWithDifferentTAC = (): React.ReactElement => {
  return (
    <div style={{ width: '300px', height: '460px' }}>
      <TafThemeApiProvider createApiFunc={fakeGetTac}>
        <TacOverviewDemo />
      </TafThemeApiProvider>
    </div>
  );
};

export const TacOverviewLightTheme = (): React.ReactElement => {
  return (
    <div style={{ width: '300px', height: '460px' }}>
      <TafThemeApiProvider createApiFunc={createFakeApi}>
        <TacOverviewDemo />
      </TafThemeApiProvider>
    </div>
  );
};
TacOverviewLightTheme.storyName = 'Tac overview light (takeSnapshot)';

export const TacOverviewDarkTheme = (): React.ReactElement => {
  return (
    <div style={{ width: '300px', height: '460px' }}>
      <TafThemeApiProvider theme={darkTheme} createApiFunc={createFakeApi}>
        <TacOverviewDemo />
      </TafThemeApiProvider>
    </div>
  );
};
TacOverviewDarkTheme.storyName = 'Tac overview dark (takeSnapshot)';
