/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { Box } from '@mui/material';
import {
  calculateDialogSizeAndPosition,
  ToolContainerDraggable,
} from '@opengeoweb/shared';
import {
  LayerManagerConnect,
  MultiMapDimensionSelectConnect,
  uiActions,
} from '@opengeoweb/core';
import MetInfoWrapper from './components/MetInfoWrapper/MetInfoWrapper';
import { StoryWrapperFakeApi } from './utils/testUtils';

export default { title: 'Demo Airmet' };

const AirmetDialogDemoComponent = (): React.ReactElement => {
  const dispatch = useDispatch();
  const { width, height, top, left } = calculateDialogSizeAndPosition();

  React.useEffect(() => {
    dispatch(
      uiActions.setToggleOpenDialog({
        setOpen: false,
        type: 'legend',
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div style={{ height: '100vh' }}>
      <ToolContainerDraggable
        onClose={(): void => {}}
        title="AIRMET list"
        startPosition={{ top, left }}
        startSize={{ width, height }}
        minWidth={600}
        minHeight={300}
      >
        <Box
          sx={{
            padding: 3,
            backgroundColor: '#f1f1f1',
          }}
        >
          <MetInfoWrapper productType="airmet" />
        </Box>
      </ToolContainerDraggable>
      <LayerManagerConnect bounds="parent" showTitle />
      <MultiMapDimensionSelectConnect />
    </div>
  );
};

export const AirmetDialogDemo = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <AirmetDialogDemoComponent />
    </StoryWrapperFakeApi>
  );
};
