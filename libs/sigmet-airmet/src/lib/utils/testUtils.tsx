/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import { ApiProvider, CreateApi } from '@opengeoweb/api';
import { Provider } from 'react-redux';
import { Store } from '@reduxjs/toolkit';
import { AxiosError, AxiosResponseHeaders } from 'axios';
import { DynamicModuleLoader } from 'redux-dynamic-modules';
import { coreModuleConfig } from '@opengeoweb/core';
import { store as defaultStore } from './store';
import { createApi as createFakeApi } from './fakeApi';

interface StoryWrapperProps {
  children: React.ReactNode;
}

interface TestWrapperProps extends StoryWrapperProps {
  store?: Store;
  createApi?: CreateApi;
}

export const TestWrapper: React.FC<TestWrapperProps> = ({
  children,
  store,
  createApi = null,
}: TestWrapperProps) => {
  return (
    <Provider store={store || defaultStore}>
      <DynamicModuleLoader modules={coreModuleConfig}>
        <ThemeWrapperOldTheme>
          <ApiProvider createApi={createApi || createFakeApi}>
            {children}
          </ApiProvider>
        </ThemeWrapperOldTheme>
      </DynamicModuleLoader>
    </Provider>
  );
};

export const StoryWrapperFakeApi: React.FC<StoryWrapperProps> = ({
  children,
}: StoryWrapperProps) => {
  return (
    <Provider store={defaultStore}>
      <DynamicModuleLoader modules={coreModuleConfig}>
        <ThemeWrapperOldTheme>
          <ApiProvider createApi={createFakeApi}>{children}</ApiProvider>
        </ThemeWrapperOldTheme>
      </DynamicModuleLoader>
    </Provider>
  );
};

interface SnapshotStoryWrapperProps {
  children: React.ReactElement;
}

export const SnapshotStoryWrapper: React.FC<SnapshotStoryWrapperProps> = ({
  children,
}) => (
  <div style={{ width: '600px', padding: '10px', position: 'relative' }}>
    {children}
  </div>
);

export const fakeBackendError: AxiosError = {
  isAxiosError: true,
  config: undefined,
  toJSON: undefined,
  name: 'API error',
  message: '',
  response: {
    data: 'Unable to store data',
    status: 400,
    statusText: '',
    config: undefined,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

export const fakeBackendNestedError: AxiosError<{ message: string }> = {
  isAxiosError: true,
  config: undefined,
  toJSON: undefined,
  name: 'API error',
  message: '',
  response: {
    data: { message: 'Unable to store nested data' },
    status: 400,
    statusText: '',
    config: undefined,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

export const fakeBackendDifferentError: AxiosError = {
  isAxiosError: true,
  config: undefined,
  toJSON: undefined,
  name: 'API error',
  message: '',
  response: {
    data: { errorMessage: { innerErrorMessage: 'Unable to store data' } },
    status: 400,
    statusText: '',
    config: undefined,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

export const StoryWrapperFakeApiWithErrors: React.FC<StoryWrapperProps> = ({
  children,
}: StoryWrapperProps) => {
  return (
    <Provider store={defaultStore}>
      <DynamicModuleLoader modules={coreModuleConfig}>
        <ThemeWrapperOldTheme>
          <ApiProvider
            createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
            any => {
              return {
                // dummy calls
                postSigmet: (): Promise<void> => {
                  return Promise.reject(fakeBackendError);
                },
                postAirmet: (): Promise<void> => {
                  return Promise.reject(fakeBackendError);
                },
              };
            }}
          >
            {children}
          </ApiProvider>
        </ThemeWrapperOldTheme>
      </DynamicModuleLoader>
    </Provider>
  );
};
