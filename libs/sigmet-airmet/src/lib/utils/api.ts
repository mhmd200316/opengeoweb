/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { CreateApi, createApiInstance, Credentials } from '@opengeoweb/api';
import { AxiosInstance } from 'axios';
import {
  Airmet,
  AirmetFromBackend,
  AirmetFromFrontend,
  Sigmet,
  SigmetFromBackend,
  SigmetFromFrontend,
} from '../types';

export type SigmetAirmetApi = {
  // sigmet
  getSigmetList: () => Promise<{ data: SigmetFromBackend[] }>;
  postSigmet: (formData: SigmetFromFrontend) => Promise<void>;
  getSigmetTAC: (sigmetData: Sigmet) => Promise<{ data: string }>;
  // airmet
  getAirmetList: () => Promise<{ data: AirmetFromBackend[] }>;
  postAirmet: (formData: AirmetFromFrontend) => Promise<void>;
  getAirmetTAC: (sigmetData: Airmet) => Promise<{ data: string }>;
};

const getApiRoutes = (axiosInstance: AxiosInstance): SigmetAirmetApi => ({
  getSigmetList: (): Promise<{ data: SigmetFromBackend[] }> => {
    return axiosInstance.get('/sigmetlist');
  },
  postSigmet: (formData: SigmetFromFrontend): Promise<void> => {
    return axiosInstance.post('/sigmet', { ...formData });
  },
  getSigmetTAC: (sigmetData: Sigmet): Promise<{ data: string }> => {
    return axiosInstance.post('/sigmet2tac', { ...sigmetData });
  },
  getAirmetList: (): Promise<{ data: AirmetFromBackend[] }> => {
    return axiosInstance.get('/airmetlist');
  },
  postAirmet: (formData: AirmetFromFrontend): Promise<void> => {
    return axiosInstance.post('/airmet', { ...formData });
  },
  getAirmetTAC: (sigmetData: Airmet): Promise<{ data: string }> => {
    return axiosInstance.post('/airmet2tac', { ...sigmetData });
  },
});

export const createApi: CreateApi = (
  url: string,
  appUrl: string,
  auth: Credentials,
  setAuth: (cred: Credentials) => void,
  authTokenUrl: string,
  authClientId: string,
): SigmetAirmetApi => {
  const axiosInstance = createApiInstance(
    url,
    appUrl,
    auth,
    setAuth,
    authTokenUrl,
    authClientId,
  );

  return getApiRoutes(axiosInstance);
};
