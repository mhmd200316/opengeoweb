/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { createFakeApiInstance } from '@opengeoweb/api';
import { AxiosInstance } from 'axios';
import {
  Airmet,
  AirmetFromBackend,
  AirmetFromFrontend,
  Sigmet,
  SigmetFromBackend,
  SigmetFromFrontend,
} from '../types';
import { SigmetAirmetApi } from './api';
import { fakeAirmetList } from './mockdata/fakeAirmetList';
import { fakeSigmetList } from './mockdata/fakeSigmetList';

export const fakeSigmetTAC =
  'EHAA SIGMET 1 VALID 121249/121449 EHDB- EHAA AMSTERDAM FIR FRQ TS OBS ENTIRE FIR FL010 STNR WKN';

export const fakeAirmetTAC =
  'EHAA AIRMET -1 VALID 310700/311100 EHDB- EHAA AMSTERDAM FIR ISOL CB OBS ENTIRE FIR FL020 STNR NC';

const getApiRoutes = (axiosInstance: AxiosInstance): SigmetAirmetApi => ({
  getSigmetList: (): Promise<{ data: SigmetFromBackend[] }> => {
    return axiosInstance.get('/sigmetlist').then(() => ({
      data: fakeSigmetList,
    }));
  },
  getSigmetTAC: (formData: Sigmet): Promise<{ data: string }> => {
    return axiosInstance.post('/sigmet2tac', formData).then(() => ({
      data: fakeSigmetTAC,
    }));
  },
  getAirmetTAC: (formdata: Airmet): Promise<{ data: string }> => {
    return axiosInstance.post('/airmet2tac', formdata).then(() => ({
      data: fakeAirmetTAC,
    }));
  },
  postSigmet: (postSigmet: SigmetFromFrontend): Promise<void> => {
    // eslint-disable-next-line no-console
    console.log(
      'form has been posted with following values:',
      postSigmet.sigmet,
    );
    return axiosInstance.post('/sigmet', postSigmet);
  },
  getAirmetList: (): Promise<{ data: AirmetFromBackend[] }> => {
    return axiosInstance.get('/airmetlist').then(() => ({
      data: fakeAirmetList,
    }));
  },
  postAirmet: (postAirmet: AirmetFromFrontend): Promise<void> => {
    // eslint-disable-next-line no-console
    console.log(
      'form has been posted with following values:',
      postAirmet.airmet,
    );
    return axiosInstance.post('/airmet', postAirmet);
  },
});

export const createApi = (): SigmetAirmetApi => {
  return getApiRoutes(createFakeApiInstance());
};
