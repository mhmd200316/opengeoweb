/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as utils from '@opengeoweb/api';
import { createFakeApiInstance } from '@opengeoweb/api';
import { createApi } from './api';

jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual(
    '../../../../api/src/lib/components/ApiContext/utils',
  ) as Record<string, unknown>),
}));

describe('src/utils/api', () => {
  const fakeAxiosInstance = createFakeApiInstance();

  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
      );
      expect(api.getSigmetList).toBeTruthy();
      expect(api.postSigmet).toBeTruthy();
      expect(api.getSigmetTAC).toBeTruthy();
      expect(api.getAirmetTAC).toBeTruthy();
      expect(api.getAirmetList).toBeTruthy();
      expect(api.postAirmet).toBeTruthy();
    });

    it('should call with the right params for getSigmetList', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
      );

      await api.getSigmetList();
      expect(spy).toHaveBeenCalledWith('/sigmetlist');
    });
    it('should call with the right params for postSigmet', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
      );

      const params = { sigmetId: 12 };
      await api.postSigmet(params);
      expect(spy).toHaveBeenCalledWith('/sigmet', {
        ...params,
      });
    });
    it('should call with the right params for getSigmetTAC', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
      );

      const params = { sigmetId: 12 };
      await api.getSigmetTAC(params);
      expect(spy).toHaveBeenCalledWith('/sigmet2tac', {
        ...params,
      });
    });
    it('should call with the right params for getAirmetTAC', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
      );

      const params = { sigmetId: 12 };
      await api.getAirmetTAC(params);
      expect(spy).toHaveBeenCalledWith('/airmet2tac', {
        ...params,
      });
    });
    it('should call with the right params for getAirmetList', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
      );

      await api.getAirmetList();
      expect(spy).toHaveBeenCalledWith('/airmetlist');
    });
  });
  it('should call with the right params for postAirmet', async () => {
    jest
      .spyOn(utils, 'createApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');
    const api = createApi(
      'fakeURL',
      'fakeUrl',
      {
        username: 'Michael Jackson',
        token: '1223344',
        refresh_token: '33455214',
      },
      jest.fn(),
      'anotherFakeUrl',
    );

    const params = { airmetId: 12 };
    await api.postAirmet(params);
    expect(spy).toHaveBeenCalledWith('/airmet', {
      ...params,
    });
  });
});
