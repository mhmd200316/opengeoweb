/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Typography, Button } from '@mui/material';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import ContentDialog from './ContentDialog';

export default { title: 'components/Content Dialog' };

export const ContentDialogDemo = (): React.ReactElement => {
  const [open, setOpen] = React.useState(false);
  const handleToggleDialog = (): void => {
    setOpen(!open);
  };

  return (
    <ThemeWrapperOldTheme>
      <Button variant="outlined" color="secondary" onClick={handleToggleDialog}>
        Open dialog
      </Button>
      <ContentDialog
        open={open}
        toggleDialogStatus={handleToggleDialog}
        title="Example"
        options={
          <div>
            <Button color="secondary">Yes</Button>
            <Button color="secondary">No</Button>
            <Button color="secondary">Maybe</Button>
          </div>
        }
      >
        <Typography variant="subtitle1">Hello World</Typography>
        <Typography variant="body1">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
          tincidunt feugiat est nec porttitor. Sed quis nulla commodo, fringilla
          mi fringilla, hendrerit justo. Curabitur et felis non dolor congue
          tincidunt. Nunc elementum scelerisque dictum. Sed blandit malesuada
          sem quis rhoncus. Lorem ipsum dolor sit amet, consectetur adipiscing
          elit. Nullam tincidunt tellus et sagittis dapibus. Cras sed vehicula
          quam, volutpat lobortis massa. Etiam quis elementum orci. Ut iaculis
          posuere bibendum. Nam eu sapien et metus sagittis sagittis id et dui.
          Integer vitae lacinia nulla, et laoreet libero. Duis sit amet quam
          luctus, dapibus odio luctus, pharetra metus. Quisque accumsan, lectus
          nec pharetra mollis, neque neque malesuada quam, dictum dapibus purus
          metus in quam. Vestibulum facilisis ornare tortor, quis accumsan nibh
          rutrum vel. Mauris rhoncus est id eros volutpat maximus. Nunc leo
          enim, semper et neque nec, pellentesque faucibus lorem. Sed pretium
          condimentum ipsum, id commodo purus viverra sit amet. Integer
          elementum velit non elit pellentesque lacinia. Nullam accumsan
          efficitur turpis, eget suscipit purus placerat sed. Ut vestibulum
          tempus vehicula. Quisque eu lectus sit amet libero feugiat
          ullamcorper. Aenean egestas, nulla vel dapibus porta, dolor purus
          pellentesque nisi, vitae iaculis dui nunc accumsan metus. Donec
          efficitur viverra nibh at porttitor. Pellentesque diam metus, mollis
          in placerat nec, auctor vitae est. Sed condimentum efficitur risus id
          vehicula. Nam luctus lacus nec urna varius dignissim.
        </Typography>
      </ContentDialog>
    </ThemeWrapperOldTheme>
  );
};

export const OpenDialogDemo = (): React.ReactElement => {
  return (
    <ThemeWrapperOldTheme>
      <ContentDialog
        open
        toggleDialogStatus={(): void => {
          /* Do nothing */
        }}
        title="Example"
        options={<Button color="secondary">Maybe</Button>}
      >
        <Typography variant="h5">Hello World</Typography>
      </ContentDialog>
    </ThemeWrapperOldTheme>
  );
};
