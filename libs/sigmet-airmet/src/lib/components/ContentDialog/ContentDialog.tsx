/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  DialogProps,
  Typography,
  Dialog,
  DialogContent,
  Button,
  Grid,
  DialogTitle,
} from '@mui/material';
import { ChevronLeft } from '@opengeoweb/theme';

const styles = {
  dialogHeader: {
    boxShadow: '0 2px 2px 0 rgba(0, 0, 0, 0.12), 0 0 2px 0 rgba(0, 0, 0, 0.14)',
    zIndex: 1,
    padding: '4px 20px',
  },
  dialogTitle: {
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center',
  },
  dialogChildren: {
    display: 'flex',
    padding: '20px',
    minHeight: 200,
    minWidth: 400,
    height: '100%',
    paddingTop: '20px!important',
  },
};

interface ContentDialogProps extends DialogProps {
  open: boolean;
  toggleDialogStatus: () => void;
  title: string;
  children?: React.ReactNode;
  options?: React.ReactNode;
  alertBanner?: React.ReactNode;
  onClose?: () => void;
}

const ContentDialog: React.FC<ContentDialogProps> = ({
  open,
  toggleDialogStatus,
  title,
  children,
  options,
  onClose,
  alertBanner = null,
  ...other
}: ContentDialogProps) => {
  const handleClose = (): void => {
    if (onClose) {
      onClose();
    } else {
      toggleDialogStatus();
    }
  };

  return (
    <Dialog
      data-testid="contentDialog"
      open={open}
      onClose={handleClose}
      maxWidth={other.maxWidth ? other.maxWidth : 'lg'}
      {...other}
    >
      <DialogTitle sx={styles.dialogHeader}>
        <Grid container alignItems="center">
          <Grid item xs>
            <Button
              onClick={handleClose}
              color="secondary"
              size="medium"
              startIcon={<ChevronLeft color="secondary" />}
              data-testid="contentdialog-close"
            >
              <Typography variant="subtitle1" color="secondary">
                BACK
              </Typography>
            </Button>
          </Grid>
          <Grid item xs sx={styles.dialogTitle}>
            <Typography data-testid="dialogTitle" variant="h6">
              {title}
            </Typography>
          </Grid>
          <Grid item xs>
            <Grid container>{options}</Grid>
          </Grid>
        </Grid>
      </DialogTitle>
      {alertBanner !== null && alertBanner}
      <DialogContent sx={styles.dialogChildren}>{children}</DialogContent>
    </Dialog>
  );
};

export default ContentDialog;
