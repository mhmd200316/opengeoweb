/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import moment from 'moment';

import { fakeSigmetList } from '../../utils/mockdata/fakeSigmetList';
import ProductListRow, { getActiveStatus } from './ProductListRow';
import { TestWrapper } from '../../utils/testUtils';
import { fakeAirmetTAC, fakeSigmetTAC } from '../../utils/fakeApi';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';
import { Airmet } from '../../types';
import { noTAC } from '../ProductForms/ProductFormTac';

jest.mock('../../utils/api');

describe('components/ProductList/ProductListRow', () => {
  it('should render sigmet row successfully', async () => {
    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[0].sigmet}
          productType="sigmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeSigmetTAC)).toBeTruthy());
  });

  it('should render airmet row successfully', async () => {
    const { queryByText, getByTestId } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[0].airmet}
          productType="airmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeTruthy());
  });

  it('should show the check icon when validity start is in the past and sigmet is not expired', async () => {
    const { queryByText, getByTestId } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[2].sigmet}
          productType="sigmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeSigmetTAC)).toBeTruthy());

    expect(getByTestId('status-active')).toBeTruthy();
  });

  it('should show the check icon when validity start is equal to current time and airmet is not expired', async () => {
    const { queryByText, getByTestId } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[2].airmet}
          productType="airmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeTruthy());

    expect(getByTestId('status-active')).toBeTruthy();
  });

  it('should show the clock icon when validity start and end are in the future and sigmet status is published', async () => {
    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[1].sigmet}
          productType="sigmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeSigmetTAC)).toBeTruthy());

    expect(getByTestId('status-inactive')).toBeTruthy();
  });

  it('should show no icon when sigmet status is expired', async () => {
    const { queryByText, getByTestId, queryByTestId } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[4].sigmet}
          productType="sigmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeSigmetTAC)).toBeTruthy());

    expect(queryByTestId('status-active')).toBeFalsy();
    expect(queryByTestId('status-inactive')).toBeFalsy();
  });

  it('should not show any status icon when airmet status is draft', async () => {
    const { queryByTestId, queryByText, getByTestId } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[0].airmet}
          productType="airmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeTruthy());

    expect(queryByTestId('status-active')).toBeFalsy();
    expect(queryByTestId('status-inactive')).toBeFalsy();
  });

  it('should not show any status icon when sigmet is expired', async () => {
    const { queryByTestId, getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[4].sigmet}
          productType="sigmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeSigmetTAC)).toBeTruthy());

    expect(queryByTestId('status-active')).toBeFalsy();
    expect(queryByTestId('status-inactive')).toBeFalsy();
  });

  it('should show "not published" for a draft sigmet', async () => {
    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[0].sigmet}
          productType="sigmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeSigmetTAC)).toBeTruthy());

    expect(getByTestId('issueTime').textContent).toEqual('(Not published)');
  });

  it('should show the correct airmet issue date for a published airmet', async () => {
    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[1].airmet}
          productType="airmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeTruthy());

    expect(getByTestId('issueTime').textContent).toEqual(
      moment
        .utc(fakeSigmetList[1].sigmet.issueDate)
        .format('DD MMM YYYY, HH:mm UTC'),
    );
  });

  it('should show the correct Sigmet valid date (same day for start and end)', async () => {
    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[1].sigmet}
          productType="sigmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeSigmetTAC)).toBeTruthy());

    expect(getByTestId('validTime').textContent).toEqual(
      `${moment
        .utc(fakeSigmetList[1].sigmet.validDateStart)
        .format('DD MMM YYYY, HH:mm')} - ${moment
        .utc(fakeSigmetList[1].sigmet.validDateEnd)
        .format('HH:mm UTC')}`,
    );
  });

  it('should show the correct Airmet valid date (different day for start and end)', async () => {
    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[6].airmet}
          productType="airmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeTruthy());

    expect(getByTestId('validTime').textContent).toEqual(
      `${moment
        .utc(fakeSigmetList[6].sigmet.validDateStart)
        .format('DD MMM YYYY, HH:mm')} - ${moment
        .utc(fakeSigmetList[6].sigmet.validDateEnd)
        .format('DD MMM YYYY, HH:mm UTC')}`,
    );
  });

  it('should show - when validity start or end is empty and no TAC should be retrieved', async () => {
    const { validDateStart, validDateEnd, ...product } =
      fakeAirmetList[2].airmet;

    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ProductListRow product={product as Airmet} productType="airmet" />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));

    // Should show Missing data: no TAC can be generated
    await waitFor(() => expect(queryByText(noTAC)).toBeTruthy());

    const validTime = getByTestId('validTime');
    expect(validTime.innerHTML).toEqual('-');
  });

  it('should show the correct cancel tag for a cancel airmet', async () => {
    const { queryByText, getByTestId } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeAirmetList[3].airmet}
          productType="airmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => {
      expect(queryByText(fakeAirmetTAC)).toBeTruthy();
      expect(queryByText('cancels 113')).toBeTruthy();
    });
  });

  it('should show the correct tag for a published sigmet', async () => {
    const { queryByText, getByTestId } = render(
      <TestWrapper>
        <ProductListRow
          product={fakeSigmetList[2].sigmet}
          productType="sigmet"
        />
      </TestWrapper>,
    );

    fireEvent.mouseOver(getByTestId('productlistrow-tacicon'));
    await waitFor(() => {
      expect(queryByText(fakeSigmetTAC)).toBeTruthy();
      expect(queryByText('published')).toBeTruthy();
    });
  });

  describe('getActiveStatus', () => {
    it('should return null if status is expired', async () => {
      expect(getActiveStatus(fakeSigmetList[4].sigmet)).toBe(null);
    });
  });
});
