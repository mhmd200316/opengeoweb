/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import ProductList from './ProductList';
import { fakeSigmetList } from '../../utils/mockdata/fakeSigmetList';
import { TestWrapper } from '../../utils/testUtils';
import { ProductType } from '../../types';
import { fakeAirmetTAC, fakeSigmetTAC } from '../../utils/fakeApi';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';

jest.mock('../../utils/api');

describe('ProductList', () => {
  it('should create a new sigmet', async () => {
    const props = {
      productType: 'sigmet' as ProductType,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: false,
      error: null,
    };
    const { getByTestId, findAllByText, getAllByTestId, findByText } = render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await findAllByText('Issue time');

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await findByText(fakeSigmetTAC);

    fireEvent.click(getByTestId('productListCreateButton'));
    expect(props.onClickNewProduct).toHaveBeenCalled();
    expect(props.onClickProductRow).not.toHaveBeenCalled();
  });

  it('should create a new airmet', async () => {
    const props = {
      productType: 'airmet' as ProductType,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeAirmetList,
      isLoading: false,
      error: null,
    };
    const { getByTestId, findAllByText, getAllByTestId, findByText } = render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await findAllByText('Issue time');

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await findByText(fakeAirmetTAC);

    fireEvent.click(getByTestId('productListCreateButton'));
    expect(props.onClickNewProduct).toHaveBeenCalled();
    expect(props.onClickProductRow).not.toHaveBeenCalled();
  });

  it('should open an existing sigmet', async () => {
    const props = {
      productType: 'sigmet' as ProductType,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: false,
      error: null,
    };
    const { findAllByText, getAllByTestId, findByText } = render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    const issueTimes = await findAllByText('Issue time');

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await findByText(fakeSigmetTAC);

    fireEvent.click(issueTimes[0]);
    expect(props.onClickNewProduct).not.toHaveBeenCalled();
    expect(props.onClickProductRow).toHaveBeenCalled();
  });

  it('should show a row for each sigmet in the list', async () => {
    const props = {
      productType: 'sigmet' as ProductType,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: false,
      error: null,
    };
    const { getAllByTestId, queryByTestId, findAllByText, findByText } = render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );
    // Wait until sigmetlist has loaded
    await findAllByText('Issue time');

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await findByText(fakeSigmetTAC);

    expect(getAllByTestId('productListItem').length).toEqual(
      props.productList.length,
    );
    expect(queryByTestId('productList-skeleton')).toBeFalsy();
    expect(queryByTestId('productList-alert')).toBeFalsy();
  });

  it('should show a row for each airmet in the list', async () => {
    const props = {
      productType: 'airmet' as ProductType,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeAirmetList,
      isLoading: false,
      error: null,
    };
    const { getAllByTestId, queryByTestId, findAllByText, findByText } = render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );
    // Wait until airmetlist has loaded
    await findAllByText('Issue time');

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await findByText(fakeAirmetTAC);

    expect(getAllByTestId('productListItem').length).toEqual(
      props.productList.length,
    );
    expect(queryByTestId('productList-skeleton')).toBeFalsy();
    expect(queryByTestId('productList-alert')).toBeFalsy();
  });

  it('should show an error when present', async () => {
    const props = {
      productType: 'sigmet' as ProductType,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: false,
      error: new Error('test sigmet list error'),
    };
    const {
      getByTestId,
      queryByTestId,
      findAllByText,
      getAllByTestId,
      findByText,
    } = render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await findAllByText('Issue time');

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await findByText(fakeSigmetTAC);

    expect(getByTestId('productList-alert').textContent).toEqual(
      `An error has occurred while retrieving the list, please try again${props.error.message}`,
    );
    expect(queryByTestId('productList-skeleton')).toBeFalsy();
  });

  it('should show a skeleton when loading', async () => {
    const props = {
      productType: 'sigmet' as ProductType,
      onClickProductRow: jest.fn(),
      onClickNewProduct: jest.fn(),
      productList: fakeSigmetList,
      isLoading: true,
      error: null,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <ProductList {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(getByTestId('productList-skeleton')).toBeTruthy();
      expect(queryByTestId('productList-alert')).toBeFalsy();
    });
  });
});
