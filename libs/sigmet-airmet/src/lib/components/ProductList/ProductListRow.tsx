/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React, { ReactElement } from 'react';
import {
  Button,
  Card,
  CardContent,
  Grid,
  Tooltip,
  Typography,
} from '@mui/material';
import moment from 'moment';

import { useApiContext } from '@opengeoweb/api';
import { StatusTag, useIsMounted } from '@opengeoweb/shared';
import { Edit, Success, Clock, Info } from '@opengeoweb/theme';
import {
  CancelSigmet,
  isInstanceOfCancelSigmet,
  SigmetPhenomena,
  Sigmet,
  ProductType,
  CancelAirmet,
  Airmet,
  AirmetPhenomena,
  isInstanceOfSigmetOrAirmet,
  isInstanceOfCancelSigmetOrAirmet,
} from '../../types';
import { noTAC, shouldRetrieveTAC } from '../ProductForms/ProductFormTac';
import { SigmetAirmetApi } from '../../utils/api';

const styles = {
  cardWrapper: {
    width: '100%',
  },
  cardContent: {
    width: '100%',
    padding: '14px',
    '&:last-child': {
      paddingBottom: '14px',
    },
  },
  activeStatus: {
    color: '#72bb23',
    paddingLeft: '6px',
    paddingTop: '2px',
  },
  font: {
    fontWeight: 'normal',
  },
};

export const getActiveStatus = (
  product: Sigmet | CancelSigmet | Airmet | CancelAirmet,
): ReactElement => {
  // do not return an icon if product is expired
  if (moment(product.validDateEnd) < moment.utc(new Date())) {
    return null;
  }

  const active = moment(product.validDateStart) <= moment.utc(new Date());
  return active ? (
    <Success data-testid="status-active" sx={styles.activeStatus} />
  ) : (
    <Clock data-testid="status-inactive" sx={styles.activeStatus} />
  );
};

const getStatusTagList = (
  product: CancelSigmet | Sigmet | Airmet | CancelAirmet,
  cancelsSeqId = null,
): ReactElement => {
  if (cancelsSeqId !== null) {
    return <StatusTag content={`cancels ${cancelsSeqId}`} color="red" />;
  }
  if (product.status === 'CANCELLED') {
    return <StatusTag content={`cancelled ${product.sequence}`} color="red" />;
  }
  if (moment(product.validDateEnd) < moment.utc(new Date())) {
    return <StatusTag content="expired" color="grey" />;
  }

  return (
    <StatusTag
      content={product.status.toLowerCase()}
      color={product.status === 'PUBLISHED' ? 'green' : 'grey'}
    />
  );
};

const getPhenomenon = (
  productType: ProductType,
  phenomenon: string,
): SigmetPhenomena | AirmetPhenomena => {
  if (productType === 'sigmet') {
    return SigmetPhenomena[phenomenon];
  }
  return AirmetPhenomena[phenomenon];
};

const getCancelSeqId = (product: CancelSigmet | CancelAirmet): string => {
  if (isInstanceOfCancelSigmet(product)) {
    return product.cancelsSigmetSequenceId;
  }
  return product.cancelsAirmetSequenceId;
};

const formatValidTime = (
  startDate: string,
  endDate: string,
  format: string,
): string => {
  if (!startDate || !endDate) {
    return '-';
  }
  return `${moment.utc(startDate).format('DD MMM YYYY, HH:mm')} - ${moment
    .utc(endDate)
    .format(format)} UTC`;
};

export interface ProductListRowProps {
  product: CancelSigmet | Sigmet | Airmet | CancelAirmet;
  productType: ProductType;
}

const ProductListRow: React.FC<ProductListRowProps> = ({
  product,
  productType,
}: ProductListRowProps) => {
  const { api } = useApiContext<SigmetAirmetApi>();
  const { isMounted } = useIsMounted();
  const [TAC, setTAC] = React.useState('');
  const apiCall =
    productType === 'sigmet' ? api.getSigmetTAC : api.getAirmetTAC;

  React.useEffect(() => {
    const retrieveTAC = (productToPost): void => {
      apiCall(productToPost)
        .then((result) => {
          if (isMounted.current) {
            setTAC(result.data);
          }
        })
        .catch(() => {});
    };
    if (shouldRetrieveTAC(product)) {
      retrieveTAC(product);
    } else if (isMounted.current) {
      setTAC(noTAC);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [product, apiCall]);

  const cancelsSeqId = isInstanceOfCancelSigmetOrAirmet(product)
    ? getCancelSeqId(product)
    : null;

  const phenomenon =
    isInstanceOfSigmetOrAirmet(product) && product.phenomenon
      ? product.phenomenon
      : null;

  const validTimeFormat =
    moment.utc(product.validDateStart).format('DD MMM YYYY') ===
    moment.utc(product.validDateEnd).format('DD MMM YYYY')
      ? 'HH:mm'
      : 'DD MMM YYYY, HH:mm';

  const validTime = formatValidTime(
    product.validDateStart,
    product.validDateEnd,
    validTimeFormat,
  );

  return (
    <Card elevation={0} variant="outlined" sx={styles.cardWrapper}>
      <CardContent sx={styles.cardContent}>
        <Grid container alignItems="center" spacing={1}>
          <Tooltip
            title={
              phenomenon !== null ? getPhenomenon(productType, phenomenon) : ''
            }
            placement="bottom-start"
          >
            <Grid item xs={3} sm={3} md={1}>
              {phenomenon !== null && (
                <Typography variant="overline">
                  {productType.toUpperCase()}
                </Typography>
              )}
              <Typography variant="subtitle2" noWrap sx={styles.font}>
                {phenomenon !== null ? phenomenon : 'Cancel'}
              </Typography>
            </Grid>
          </Tooltip>
          <Grid item xs={3} sm={2} md={1}>
            <Tooltip
              data-testid="productlistrow-tactooltip"
              title={<div style={{ whiteSpace: 'pre-line' }}>{TAC}</div>}
              placement="bottom-start"
            >
              <span>
                <Info data-testid="productlistrow-tacicon" color="secondary" />
              </span>
            </Tooltip>
          </Grid>
          <Grid item xs={9} sm={10} md={3} lg={2}>
            <Typography variant="overline">Issue time</Typography>
            <Typography
              variant="subtitle2"
              sx={styles.font}
              data-testid="issueTime"
            >
              {product.issueDate
                ? `${moment
                    .utc(product.issueDate)
                    .format('DD MMM YYYY, HH:mm')} UTC`
                : '(Not published)'}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={8} md={5} lg={6}>
            <Typography variant="overline">Valid time</Typography>
            <Typography
              variant="subtitle2"
              sx={styles.font}
              data-testid="validTime"
            >
              {validTime}
            </Typography>
          </Grid>
          <Grid item xs={6} sm={4} md={2} container>
            {product.status === 'DRAFT' ? (
              <Button
                color="secondary"
                size="small"
                startIcon={<Edit color="secondary" />}
              >
                Draft
              </Button>
            ) : (
              getStatusTagList(product, cancelsSeqId)
            )}
            {product.status === 'PUBLISHED' &&
              cancelsSeqId === null &&
              getActiveStatus(product)}
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default ProductListRow;
