/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Paper } from '@mui/material';
import ProductList from './ProductList';
import {
  fakeSigmetList,
  fakeStaticSigmetList,
} from '../../utils/mockdata/fakeSigmetList';
import { StoryWrapperFakeApi } from '../../utils/testUtils';
import {
  fakeAirmetList,
  fakeStaticAirmetList,
} from '../../utils/mockdata/fakeAirmetList';
import { AirmetFromBackend, SigmetFromBackend } from '../../types';

export default {
  title: 'components/Product List',
};

interface ListProps {
  productList: SigmetFromBackend[] | AirmetFromBackend[];
}

const SigmetListComponent: React.FC<ListProps> = ({
  productList,
}: ListProps) => {
  return (
    <StoryWrapperFakeApi>
      <Paper
        style={{
          position: 'absolute',
          top: '30px',
          left: '30px',
          width: '80%',
          backgroundColor: '#f1f1f1',
          padding: '24px',
        }}
      >
        <ProductList
          productList={productList}
          productType="sigmet"
          isLoading={false}
          error={null}
          onClickProductRow={(): void => {}}
          onClickNewProduct={(): void => {}}
        />
      </Paper>
    </StoryWrapperFakeApi>
  );
};

export const SigmetListDemo = (): React.ReactElement => {
  return <SigmetListComponent productList={fakeSigmetList} />;
};

export const SigmetListDemoSnapshot = (): React.ReactElement => {
  return <SigmetListComponent productList={fakeStaticSigmetList} />;
};

SigmetListDemoSnapshot.storyName = 'Sigmet List Demo (takeSnapshot)';

const AirmetListComponent: React.FC<ListProps> = ({
  productList,
}: ListProps) => {
  return (
    <StoryWrapperFakeApi>
      <Paper
        style={{
          position: 'absolute',
          top: '30px',
          left: '30px',
          width: '80%',
          backgroundColor: '#f1f1f1',
          padding: '24px',
        }}
      >
        <ProductList
          productList={productList}
          productType="airmet"
          isLoading={false}
          error={null}
          onClickProductRow={(): void => {}}
          onClickNewProduct={(): void => {}}
        />
      </Paper>
    </StoryWrapperFakeApi>
  );
};

export const AirmetListDemo = (): React.ReactElement => {
  return <AirmetListComponent productList={fakeAirmetList} />;
};

export const AirmetListDemoSnapshot = (): React.ReactElement => {
  return <AirmetListComponent productList={fakeStaticAirmetList} />;
};

AirmetListDemoSnapshot.storyName = 'Airmet List Demo (takeSnapshot)';
