/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Button, Grid, List, ListItem, Skeleton } from '@mui/material';

import { AlertBanner } from '@opengeoweb/shared';
import ProductListRow from './ProductListRow';
import { AirmetFromBackend, ProductType, SigmetFromBackend } from '../../types';

interface ProductListProps {
  productType: ProductType;
  productList: SigmetFromBackend[] | AirmetFromBackend[];
  isLoading: boolean;
  error: Error;
  onClickProductRow: (product: SigmetFromBackend | AirmetFromBackend) => void;
  onClickNewProduct: () => void;
}

const ProductList: React.FC<ProductListProps> = ({
  productType,
  productList,
  isLoading,
  error,
  onClickProductRow,
  onClickNewProduct,
}: ProductListProps) => {
  return (
    <Grid container direction="column" alignItems="stretch">
      <Grid container item justifyContent="flex-end">
        <Button
          variant="contained"
          color="secondary"
          data-testid="productListCreateButton"
          onClick={onClickNewProduct}
        >
          Create a new {productType.toUpperCase()}
        </Button>
      </Grid>
      <Grid item sx={{ width: '100%', minHeight: '600px' }}>
        {error && (
          <AlertBanner
            severity="error"
            title="An error has occurred while retrieving the list, please try again"
            info={error.message ? error.message : ''}
            dataTestId="productList-alert"
          />
        )}
        {productList && (
          <List>
            {(productList as Array<SigmetFromBackend | AirmetFromBackend>).map(
              (productListItem, index) => {
                const product = productListItem[productType];
                return (
                  <ListItem
                    data-testid="productListItem"
                    button
                    onClick={(): void => {
                      onClickProductRow(productListItem);
                    }}
                    key={
                      product.sequence && product.issueDate
                        ? `${product.sequence}-${product.issueDate}`
                        : `draft-${index}`
                    }
                    sx={{ padding: '2px' }}
                  >
                    <ProductListRow
                      product={product}
                      productType={productType}
                    />
                  </ListItem>
                );
              },
            )}
          </List>
        )}
        {isLoading && (
          <Skeleton
            data-testid="productList-skeleton"
            variant="rectangular"
            height="660px"
          />
        )}
      </Grid>
    </Grid>
  );
};

export default ProductList;
