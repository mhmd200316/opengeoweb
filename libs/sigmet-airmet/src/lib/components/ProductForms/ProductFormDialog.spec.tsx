/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import * as utils from '@opengeoweb/api';

import moment from 'moment';
import { createFakeApiInstance } from '@opengeoweb/api';
import { prepareProductValues, ProductFormDialog } from './ProductFormDialog';
import {
  TestWrapper,
  fakeBackendError,
  fakeBackendNestedError,
  fakeBackendDifferentError,
} from '../../utils/testUtils';
import { fakeSigmetList } from '../../utils/mockdata/fakeSigmetList';
import { createApi, fakeAirmetTAC } from '../../utils/fakeApi';
import {
  Airmet,
  ProductType,
  Sigmet,
  SigmetFromBackend,
  SigmetPhenomena,
} from '../../types';
import {
  airmetNoValidStartEnd,
  fakeAirmetList,
  fakeDraftAirmetCloud,
} from '../../utils/mockdata/fakeAirmetList';
import { VALID_FROM_TIME_DELAY_IN_MINUTES } from './ProductFormFields/ValidFrom';
import { getDefaultHoursValid } from './ProductFormFields/ValidUntil';
import { SigmetAirmetApi } from '../../utils/api';

describe('components/SigmetForm/ProductFormDialog', () => {
  beforeEach(() => {
    jest.spyOn(console, 'log').mockImplementation();
  });

  it('should display the correct buttons for a new sigmet', () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet' as ProductType,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(getByTestId('productform-dialog')).toBeTruthy();
    expect(getByTestId('productform-dialog-draft')).toBeTruthy();
    expect(getByTestId('productform-dialog-discard')).toBeTruthy();
    expect(getByTestId('productform-dialog-publish')).toBeTruthy();
    expect(queryByTestId('productform-dialog-cancel')).toBeFalsy();
  });

  it('should display the correct buttons for a new airmet', () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'airmet' as ProductType,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(getByTestId('productform-dialog')).toBeTruthy();
    expect(getByTestId('productform-dialog-draft')).toBeTruthy();
    expect(getByTestId('productform-dialog-discard')).toBeTruthy();
    expect(getByTestId('productform-dialog-publish')).toBeTruthy();
    expect(queryByTestId('productform-dialog-cancel')).toBeFalsy();
  });

  it('should cancel a sigmet for VA_CLD', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[1],
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId, queryByText, getByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel'));
    expect(getByTestId('confirmationDialog-title').textContent).toEqual(
      'Cancel SIGMET',
    );

    fireEvent.click(queryByText('EBBU'));
    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/sigmet', {
        changeStatusTo: 'CANCELLED',
        sigmet: expect.any(Object),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should cancel a sigmet for a phenomenon other than VA_CLD', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[9],
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel'));
    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/sigmet', {
        changeStatusTo: 'CANCELLED',
        sigmet: expect.any(Object),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should cancel an airmet for any phenomenon', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[1],
      productType: 'airmet' as ProductType,
    };

    const { queryByTestId, getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel'));
    expect(
      queryByText('Are you sure you want to cancel this AIRMET?'),
    ).toBeTruthy();
    expect(getByTestId('confirmationDialog-title').textContent).toEqual(
      'Cancel AIRMET',
    );

    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/airmet', {
        changeStatusTo: 'CANCELLED',
        airmet: expect.any(Object),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should show an error message when saving fails', async () => {
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postAirmet: (): Promise<void> =>
        Promise.reject(new Error('Network error')),
    });

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[1],
      productType: 'airmet' as ProductType,
    };

    const { queryByTestId, queryByText, getByTestId } = render(
      <TestWrapper createApi={fakeApi}>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel'));
    expect(
      queryByText('Are you sure you want to cancel this AIRMET?'),
    ).toBeTruthy();
    expect(getByTestId('confirmationDialog-title').textContent).toEqual(
      'Cancel AIRMET',
    );

    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(
        queryByText('An error has occurred while saving, please try again'),
      ).toBeTruthy();
    });
    expect(props.toggleDialogStatus).not.toHaveBeenCalled();
  });

  it('should show an extra error line when saving fails on the backend', async () => {
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: (): Promise<void> => Promise.reject(fakeBackendError),
    });

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[1],
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId, queryByText } = render(
      <TestWrapper createApi={fakeApi}>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel'));
    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(queryByText(fakeBackendError.response.data)).toBeTruthy();
    });
    expect(props.toggleDialogStatus).not.toHaveBeenCalled();
  });

  it('should show an extra error line when saving fails on the backend and AxiosError message is nested', async () => {
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: (): Promise<void> => Promise.reject(fakeBackendNestedError),
    });

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[1],
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId, queryByText } = render(
      <TestWrapper createApi={fakeApi}>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel'));
    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(
        queryByText(fakeBackendNestedError.response.data.message),
      ).toBeTruthy();
    });
    expect(props.toggleDialogStatus).not.toHaveBeenCalled();
  });

  it('should show no extra error line when saving fails on the backend and a different error structure is received', async () => {
    const fakeApi = (): SigmetAirmetApi => ({
      ...createApi(),
      postSigmet: (): Promise<void> =>
        Promise.reject(fakeBackendDifferentError),
    });

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[1],
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId, queryByText } = render(
      <TestWrapper createApi={fakeApi}>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-cancel'));
    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(
        queryByText('An error has occurred while saving, please try again'),
      ).toBeTruthy();
      expect(queryByText('Unable to store data')).toBeFalsy();
    });
    expect(props.toggleDialogStatus).not.toHaveBeenCalled();
  });

  it('should discard a new sigmet without doing a post request', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId, getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-discard'));
    expect(
      queryByText(
        'Are you sure you would like to discard this SIGMET? Its properties will be lost',
      ),
    ).toBeTruthy();
    expect(getByTestId('confirmationDialog-title').textContent).toEqual(
      'Discard SIGMET',
    );

    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
    expect(spy).toHaveBeenCalledTimes(0);
  });

  it('should discard a draft sigmet', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[0],
      productType: 'airmet' as ProductType,
    };

    const { queryByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-discard'));
    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/airmet', {
        changeStatusTo: 'DISCARDED',
        airmet: expect.any(Object),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should save a draft sigmet without asking for confirmation', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[0],
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-draft'));
    expect(queryByTestId('confirmationDialog-confirm')).toBeFalsy();
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/sigmet', {
        changeStatusTo: 'DRAFT',
        sigmet: expect.any(Object),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should publish a valid sigmet', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[0],
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId, queryByText, getByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-publish'));
    await waitFor(() =>
      expect(queryByTestId('confirmationDialog-confirm')).toBeTruthy(),
    );
    expect(
      queryByText('Are you sure you want to publish this SIGMET?'),
    ).toBeTruthy();
    expect(getByTestId('confirmationDialog-title').textContent).toEqual(
      'Publish',
    );

    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/sigmet', {
        changeStatusTo: 'PUBLISHED',
        sigmet: expect.any(Object),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should publish a valid sigmet with VA_CLD', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const productListItem: SigmetFromBackend = {
      ...fakeSigmetList[0],
      sigmet: {
        ...fakeSigmetList[0].sigmet,
        phenomenon: 'VA_CLD' as SigmetPhenomena,
        vaSigmetVolcanoName: 'EYJAFJALLAJOKULL',
        vaSigmetVolcanoCoordinates: { latitude: 63.62, longitude: -19.61 },
      },
    };

    const [testLatitude, testLongitude] = [59, 23, -23.76];

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem,
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId, queryByText, getByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.change(
      queryByTestId('vaSigmetVolcanoCoordinates.latitude').querySelector(
        'input',
      ),
      { target: { value: testLatitude.toString() } },
    );

    fireEvent.change(
      queryByTestId('vaSigmetVolcanoCoordinates.longitude').querySelector(
        'input',
      ),
      { target: { value: testLongitude.toString() } },
    );

    fireEvent.click(queryByTestId('productform-dialog-publish'));
    await waitFor(() =>
      expect(queryByTestId('confirmationDialog-confirm')).toBeTruthy(),
    );
    expect(
      queryByText('Are you sure you want to publish this SIGMET?'),
    ).toBeTruthy();
    expect(getByTestId('confirmationDialog-title').textContent).toEqual(
      'Publish',
    );

    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/sigmet', {
        changeStatusTo: 'PUBLISHED',
        sigmet: expect.objectContaining({
          vaSigmetVolcanoCoordinates: {
            latitude: testLatitude,
            longitude: testLongitude,
          },
        }),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should publish a valid sigmet with VA_CLD without coordinates', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const productListItem: SigmetFromBackend = {
      ...fakeSigmetList[0],
      sigmet: {
        ...fakeSigmetList[0].sigmet,
      },
    };

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem,
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId, queryByText, findByText, getByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role="button"]'),
    );
    const menuItem = await findByText('Volcanic ash cloud');
    fireEvent.click(menuItem);

    fireEvent.click(queryByTestId('productform-dialog-publish'));
    await waitFor(() =>
      expect(queryByTestId('confirmationDialog-confirm')).toBeTruthy(),
    );
    expect(
      queryByText('Are you sure you want to publish this SIGMET?'),
    ).toBeTruthy();
    expect(getByTestId('confirmationDialog-title').textContent).toEqual(
      'Publish',
    );

    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/sigmet', {
        changeStatusTo: 'PUBLISHED',
        sigmet: expect.objectContaining({
          phenomenon: 'VA_CLD',
        }),
      });
      expect(spy).toHaveBeenCalledWith('/sigmet', {
        changeStatusTo: 'PUBLISHED',
        sigmet: expect.not.objectContaining({
          vaSigmetVolcanoCoordinates: {
            latitude: NaN,
            longitude: NaN,
          },
        }),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should publish a valid airmet', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[0],
      productType: 'airmet' as ProductType,
    };

    const { queryByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('productform-dialog-publish'));
    await waitFor(() =>
      expect(queryByTestId('confirmationDialog-confirm')).toBeTruthy(),
    );

    expect(
      queryByText('Are you sure you want to publish this AIRMET?'),
    ).toBeTruthy();
    expect(queryByTestId('confirmationDialog-title').textContent).toEqual(
      'Publish',
    );

    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/airmet', {
        changeStatusTo: 'PUBLISHED',
        airmet: expect.any(Object),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should save a draft sigmet after making changes, closing the form and choosing to save', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[0],
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('levels-TOPS'));
    fireEvent.click(queryByTestId('contentdialog-close'));
    fireEvent.click(queryByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/sigmet', {
        changeStatusTo: 'DRAFT',
        sigmet: expect.any(Object),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should not save a draft sigmet after making changes, closing the form and choosing to discard', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[0],
      productType: 'sigmet' as ProductType,
    };

    const { queryByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    fireEvent.click(queryByTestId('levels-TOPS'));
    fireEvent.click(queryByTestId('contentdialog-close'));
    fireEvent.click(queryByTestId('confirmationDialog-cancel'));
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
    expect(spy).not.toHaveBeenCalledWith('/sigmet', {
      changeStatusTo: 'DRAFT',
      sigmet: expect.any(Object),
    });
  });

  it('should not ask for confirmation when clicking the back button and no changes have been made', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[0],
      productType: 'airmet' as ProductType,
    };

    const { queryByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());

    fireEvent.click(queryByTestId('contentdialog-close'));
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
    expect(spy).not.toHaveBeenCalledWith('/airmet', {
      changeStatusTo: 'DRAFT',
      airmet: expect.any(Object),
    });
  });
  it('should show the correct dialog title when opening  a new airmet', () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: null,
      productType: 'airmet' as ProductType,
    };

    const { getByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(getByTestId('dialogTitle').textContent).toEqual('New AIRMET');
  });
  it('should show the correct dialog title when opening  a new sigmet', () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: null,
      productType: 'sigmet' as ProductType,
    };

    const { getByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(getByTestId('dialogTitle').textContent).toEqual('New SIGMET');
  });
  it('should show the correct dialog title when opening a draft airmet', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[0],
      productType: 'airmet' as ProductType,
    };

    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Isolated thunderstorm(s) with hail - saved as draft',
    );
  });
  it('should show the correct dialog title when opening a published sigmet', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[1],
      productType: 'sigmet' as ProductType,
    };

    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'SIGMET Volcanic ash cloud - Published',
    );
  });
  it('should show the correct dialog title when opening a cancelled sigmet', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeSigmetList[5],
      productType: 'sigmet' as ProductType,
    };

    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'SIGMET Obscured thunderstorm(s) - Cancelled',
    );
  });
  it('should show the correct dialog title when opening a cancel airmet', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[3],
      productType: 'airmet' as ProductType,
    };

    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Cancels 113',
    );
  });
  it('should show the correct dialog title when opening an expired airmet', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeAirmetList[4],
      productType: 'airmet' as ProductType,
    };

    const { getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeFalsy());
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Overcast cloud - Expired',
    );
  });

  it('should change the CloudLevelInfoMode accordingly when the checkbox Above is clicked', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeDraftAirmetCloud,
      productType: 'airmet' as ProductType,
    };

    const { queryByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const cloudLevelAbove = queryByTestId('cloudLevels-Above');
    fireEvent.click(cloudLevelAbove);

    fireEvent.click(queryByTestId('productform-dialog-draft'));
    expect(queryByTestId('confirmationDialog-confirm')).toBeFalsy();
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/airmet', {
        changeStatusTo: 'DRAFT',
        airmet: expect.objectContaining({
          cloudLevelInfoMode: 'BETW',
          cloudLowerLevel: (fakeDraftAirmetCloud.airmet as Airmet)
            .cloudLowerLevel,
          cloudLevel: (fakeDraftAirmetCloud.airmet as Airmet).cloudLevel,
        }),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should change the CloudLevelInfoMode accordingly when the checkbox SFC is clicked', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createFakeApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: fakeDraftAirmetCloud,
      productType: 'airmet' as ProductType,
    };

    const { queryByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const cloudLevelSFC = queryByTestId('cloudLevels-SFC');
    fireEvent.click(cloudLevelSFC);

    fireEvent.click(queryByTestId('productform-dialog-draft'));
    expect(queryByTestId('confirmationDialog-confirm')).toBeFalsy();
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith('/airmet', {
        changeStatusTo: 'DRAFT',
        airmet: expect.objectContaining({
          cloudLevelInfoMode: 'BETW_SFC_ABV',
          cloudLevel: (fakeDraftAirmetCloud.airmet as Airmet).cloudLevel,
        }),
      });
      expect(spy).toHaveBeenCalledWith('/airmet', {
        changeStatusTo: 'DRAFT',
        airmet: expect.not.objectContaining({
          cloudLowerLevel: (fakeDraftAirmetCloud.airmet as Airmet)
            .cloudLowerLevel,
        }),
      });
    });
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should show the TAC even if no valid start/end in list item as they get defaulted in', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productListItem: airmetNoValidStartEnd,
      productType: 'airmet' as ProductType,
    };

    const { queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider>
          <ProductFormDialog {...props} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeTruthy());
  });

  describe('prepareProductValues', () => {
    it('should not add/change validDateStart and End if already present or if it is a cancel sigmet/airmet', () => {
      expect(prepareProductValues(fakeSigmetList[0].sigmet, 'sigmet')).toEqual(
        fakeSigmetList[0].sigmet,
      );
      expect(prepareProductValues(fakeSigmetList[3].sigmet, 'sigmet')).toEqual(
        fakeSigmetList[3].sigmet,
      );
    });
    it('should add validDateStart and End if not present', () => {
      const sigmetMissingStartEnd = {
        uuid: 'someuniqueidprescibedbyBE',
        phenomenon: 'OBSC_TS' as SigmetPhenomena,
        sequence: 'A01',
        firName: 'AMSTERDAM FIR',
        locationIndicatorATSU: 'EHAA',
        locationIndicatorATSR: 'EHAA',
        locationIndicatorMWO: 'EHDB',
        isObservationOrForecast: 'OBS',
        movementType: 'FORECAST_POSITION',
        change: 'WKN',
        type: 'NORMAL',
        status: 'DRAFT',
        levelInfoMode: 'AT',
      } as Sigmet;

      expect(prepareProductValues(sigmetMissingStartEnd, 'sigmet')).toEqual({
        ...sigmetMissingStartEnd,
        validDateStart: moment
          .utc()
          .add(VALID_FROM_TIME_DELAY_IN_MINUTES, 'minutes')
          .format('YYYY-MM-DDTHH:mm:ss[Z]'),
        validDateEnd: moment
          .utc()
          .add(getDefaultHoursValid('sigmet'), 'hour')
          .add(VALID_FROM_TIME_DELAY_IN_MINUTES, 'minutes')
          .format('YYYY-MM-DDTHH:mm:ss[Z]'),
      });
      expect(prepareProductValues(fakeSigmetList[3].sigmet, 'sigmet')).toEqual(
        fakeSigmetList[3].sigmet,
      );
    });
  });
});
