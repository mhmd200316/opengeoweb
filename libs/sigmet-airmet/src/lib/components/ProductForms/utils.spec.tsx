/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { fireEvent, render } from '@testing-library/react';
import { act, renderHook } from '@testing-library/react-hooks';

import moment from 'moment';

import {
  LevelUnits,
  isInstanceOfCancelSigmet,
  isInstanceOfSigmet,
  SigmetPhenomena,
  ProductCanbe,
  ProductStatusDescription,
  Sigmet,
  CancelSigmet,
  AirmetPhenomena,
  isInstanceOfAirmet,
  isInstanceOfCancelAirmet,
  WindUnit,
  MovementUnit,
} from '../../types';
import {
  isLevelLower,
  getDialogtitle,
  getProductFormMode,
  getProductFormLifecycleButtons,
  createSigmetPostParameter,
  useDelayedFormValues,
  getConfirmationDialogTitle,
  getConfirmationDialogContent,
  getConfirmationDialogButtonLabel,
  getConfirmationDialogCancelLabel,
  getHoursBeforeValidity,
  getMaxHoursOfValidity,
  getMaxLevelValue,
  useProductSettings,
  getLevelInFeet,
  getProductIssueDate,
  createAirmetPostParameter,
  getMinLevelValue,
  getMaxWindSpeedValue,
  getMinWindSpeedValue,
  getAllowedUnits,
  rewindGeometry,
  removeEmptyNumberFieldValues,
  hasValue,
} from './utils';
import {
  fakeNewSigmet,
  fakeSigmetList,
} from '../../utils/mockdata/fakeSigmetList';
import {
  fakeAirmetList,
  fakeNewAirmet,
} from '../../utils/mockdata/fakeAirmetList';

describe('components/SigmetForm/utils', () => {
  const config = {
    location_indicator_mwo: 'EHDB',
    active_firs: ['EHAA'],
    firareas: {
      EHAA: {
        firname: 'AMSTERDAM FIR',
        location_indicator_atsr: 'EHAA',
        location_indicator_atsu: 'EHAA',
        hoursbeforevalidity: 5,
        tc_hoursbeforevalidity: 12,
        va_hoursbeforevalidity: 20,
        maxhoursofvalidity: 5,
        va_maxhoursofvalidity: 12,
        tc_maxhoursofvalidity: 20,
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KT'],
          },
        ],
      },
    },
  };
  const configMultipleFIR = {
    location_indicator_mwo: 'EHDB',
    firareas: {
      EHAA: {
        firname: 'AMSTERDAM FIR',
        location_indicator_atsr: 'EHAA',
        location_indicator_atsu: 'EHAA',
        areapreset: 'NL_FIR',
        maxhoursofvalidity: 4,
        hoursbeforevalidity: 4,
        tc_maxhoursofvalidity: 6,
        tc_hoursbeforevalidity: 12,
        va_maxhoursofvalidity: 6,
        va_hoursbeforevalidity: 12,
        adjacent_firs: ['EKDK', 'EDWW', 'EDGG', 'EBBU', 'EGTT', 'EGPX'],
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'M'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KMH'],
          },
        ],
      },
      EBBU: {
        firname: 'Some OTHER FIR',
        location_indicator_atsr: 'EBBU',
        location_indicator_atsu: 'EBBU',
        areapreset: 'OTHER_FIR',
        maxhoursofvalidity: 6,
        hoursbeforevalidity: 7,
        tc_maxhoursofvalidity: 12,
        tc_hoursbeforevalidity: 13,
        va_maxhoursofvalidity: 2,
        va_hoursbeforevalidity: 1,
        adjacent_firs: ['HIP', 'KNEE', 'ALPHA', 'BRAVO'],
        units: [
          {
            unit_type: 'level_unit',
            allowed_units: ['FT', 'FL'],
          },
          {
            unit_type: 'movement_unit',
            allowed_units: ['KT'],
          },
        ],
      },
    },
    active_firs: ['EHAA', 'EBBU'],
  };
  describe('getHoursBeforeValidity', () => {
    it('should return default hours before validity from the config', () => {
      expect(getHoursBeforeValidity('HVY_DS', 'EHAA', config)).toBe(
        config.firareas.EHAA.hoursbeforevalidity,
      );
    });
    it('should return vc hours before validity from the config', () => {
      expect(getHoursBeforeValidity('VA_CLD', 'EHAA', config)).toBe(
        config.firareas.EHAA.va_hoursbeforevalidity,
      );
    });
    it('should return tc hours before validity from the config', () => {
      expect(getHoursBeforeValidity('TC', 'EHAA', config)).toBe(
        config.firareas.EHAA.tc_hoursbeforevalidity,
      );
    });
    it('should return default 4 hours when there is no fir', () => {
      expect(getHoursBeforeValidity('HVY_DS', undefined, config)).toBe(4);
    });
    it('should return default hours from the correct FIR when it has multiple FIRs', () => {
      expect(getHoursBeforeValidity('HVY_DS', 'EBBU', configMultipleFIR)).toBe(
        configMultipleFIR.firareas.EBBU.hoursbeforevalidity,
      );
    });
  });

  describe('getMaxHoursOfValidity', () => {
    it('should return default max hours of validity from the config', () => {
      expect(getMaxHoursOfValidity('HVY_DS', 'EHAA', config)).toBe(
        config.firareas.EHAA.maxhoursofvalidity,
      );
    });
    it('should return vc max hours of validity from the config', () => {
      expect(getMaxHoursOfValidity('VA_CLD', 'EHAA', config)).toBe(
        config.firareas.EHAA.va_maxhoursofvalidity,
      );
    });
    it('should return tc max hours of validity from the config', () => {
      expect(getMaxHoursOfValidity('TC', 'EHAA', config)).toBe(
        config.firareas.EHAA.tc_maxhoursofvalidity,
      );
    });
    it('should return default 4 hours if there is no fir', () => {
      expect(getMaxHoursOfValidity('TC', undefined, config)).toBe(4);
    });
    it('should return default hours from the correct FIR when it has multiple FIRs', () => {
      expect(getMaxHoursOfValidity('HVY_DS', 'EBBU', configMultipleFIR)).toBe(
        configMultipleFIR.firareas.EBBU.maxhoursofvalidity,
      );
    });
  });

  describe('getMaxLevelValue', () => {
    it('should return 100 as max level for FL for airmet and 650 for sigmet', () => {
      expect(getMaxLevelValue('FL' as LevelUnits, 'sigmet')).toBe(650);
      expect(getMaxLevelValue('FL' as LevelUnits, 'airmet')).toBe(100);
    });
    it('should return 4900 as max level for ft', () => {
      expect(getMaxLevelValue('FT' as LevelUnits, 'sigmet')).toBe(4900);
      expect(getMaxLevelValue('FT' as LevelUnits, 'airmet')).toBe(4900);
    });
    it('should return 9999 as max level for m', () => {
      expect(getMaxLevelValue('M' as LevelUnits, 'sigmet')).toBe(9999);
      expect(getMaxLevelValue('M' as LevelUnits, 'airmet')).toBe(9999);
    });
    it('should return 4900 as max level for any other level unit', () => {
      expect(getMaxLevelValue('random' as LevelUnits, 'sigmet')).toBe(4900);
      expect(getMaxLevelValue('random' as LevelUnits, 'airmet')).toBe(4900);
    });
  });

  describe('getMinLevelValue', () => {
    it('should return 50 as min level for FL', () => {
      expect(getMinLevelValue('FL' as LevelUnits)).toBe(50);
    });
    it('should return 100 as min level for ft', () => {
      expect(getMinLevelValue('FT' as LevelUnits)).toBe(100);
    });
    it('should return 1 as min level for m', () => {
      expect(getMinLevelValue('M' as LevelUnits)).toBe(1);
    });
    it('should return 1 as min level for any other level unit', () => {
      expect(getMinLevelValue('randomunit' as LevelUnits)).toBe(100);
    });
  });

  describe('getMaxWindSpeedValue', () => {
    it('should return 199 as max level for kt', () => {
      expect(getMaxWindSpeedValue('KT' as WindUnit)).toBe(199);
    });
    it('should return 99 as max level for mps', () => {
      expect(getMaxWindSpeedValue('MPS' as WindUnit)).toBe(99);
    });
    it('should return 199 as max level for any other level unit', () => {
      expect(getMaxWindSpeedValue('random' as WindUnit)).toBe(199);
    });
  });

  describe('getMinWindSpeedValue', () => {
    it('should return 31 as min level for kt', () => {
      expect(getMinWindSpeedValue('KT' as WindUnit)).toBe(31);
    });
    it('should return 0 as min level for mps', () => {
      expect(getMinWindSpeedValue('MPS' as WindUnit)).toBe(0);
    });
    it('should return 31 as min level for any other level unit', () => {
      expect(getMinWindSpeedValue('random' as WindUnit)).toBe(31);
    });
  });

  describe('useSigmetSettings', () => {
    it('should construct the right objects from the passed in configuration', async () => {
      const { result } = renderHook(() => useProductSettings(config));
      expect(result.current).toEqual({
        defaultATSR: config.firareas.EHAA.location_indicator_atsr,
        optionsFIR: [
          {
            ATSR: config.firareas.EHAA.location_indicator_atsr,
            FIR: config.firareas.EHAA.firname,
          },
        ],
        allFIRS: config.firareas,
        allowedUnits: { EHAA: config.firareas.EHAA.units },
      });
    });
    it('should construct the right objects from the passed in configuration if multiple FIRs passed', async () => {
      const { result } = renderHook(() =>
        useProductSettings(configMultipleFIR),
      );

      expect(result.current).toEqual({
        defaultATSR: configMultipleFIR.firareas.EHAA.location_indicator_atsr,
        optionsFIR: [
          {
            ATSR: configMultipleFIR.firareas.EHAA.location_indicator_atsr,
            FIR: configMultipleFIR.firareas.EHAA.firname,
          },
          {
            ATSR: configMultipleFIR.firareas.EBBU.location_indicator_atsr,
            FIR: configMultipleFIR.firareas.EBBU.firname,
          },
        ],
        allFIRS: configMultipleFIR.firareas,
        allowedUnits: {
          EHAA: configMultipleFIR.firareas.EHAA.units,
          EBBU: configMultipleFIR.firareas.EBBU.units,
        },
      });
    });
  });

  describe('getLevelInFeet', () => {
    it('should convert meters to feet correctly', () => {
      expect(getLevelInFeet(15.24, 'M' as LevelUnits)).toBe(50);
    });
    it('should convert flight level to feet correctly', () => {
      expect(getLevelInFeet(5, 'FL' as LevelUnits)).toBe(500);
    });
    it('should return same value if passed in value is already in feet', () => {
      expect(getLevelInFeet(100, 'FT' as LevelUnits)).toBe(100);
    });
  });

  describe('isLevelLower', () => {
    it('should return true when first parameter is lower than second parameter and both units are the same', () => {
      expect(
        isLevelLower(1, 'M' as LevelUnits, 2, 'M' as LevelUnits),
      ).toBeTruthy();

      expect(
        isLevelLower(1, 'FT' as LevelUnits, 2, 'FT' as LevelUnits),
      ).toBeTruthy();
      expect(
        isLevelLower(1, 'FL' as LevelUnits, 2, 'FL' as LevelUnits),
      ).toBeTruthy();
    });

    it('should return false when first parameter is higher than or equal to second parameter and both units are the same', () => {
      expect(
        isLevelLower(2, 'M' as LevelUnits, 1, 'M' as LevelUnits),
      ).toBeFalsy();
      expect(
        isLevelLower(2, 'FT' as LevelUnits, 1, 'FT' as LevelUnits),
      ).toBeFalsy();
      expect(
        isLevelLower(2, 'FL' as LevelUnits, 1, 'FL' as LevelUnits),
      ).toBeFalsy();
      expect(
        isLevelLower(1, 'M' as LevelUnits, 1, 'M' as LevelUnits),
      ).toBeFalsy();
      expect(
        isLevelLower(2, 'FT' as LevelUnits, 2, 'FT' as LevelUnits),
      ).toBeFalsy();
      expect(
        isLevelLower(2, 'FL' as LevelUnits, 2, 'FL' as LevelUnits),
      ).toBeFalsy();
    });

    it('should do a coorect conversion to determine if lower when units differ', () => {
      // meters and ft
      expect(
        isLevelLower(10, 'M' as LevelUnits, 33, 'FT' as LevelUnits),
      ).toBeTruthy();
      expect(
        isLevelLower(10, 'M' as LevelUnits, 32, 'FT' as LevelUnits),
      ).toBeFalsy();

      // m and FL
      expect(
        isLevelLower(30, 'M' as LevelUnits, 1, 'FL' as LevelUnits),
      ).toBeTruthy();
      expect(
        isLevelLower(31, 'M' as LevelUnits, 1, 'FL' as LevelUnits),
      ).toBeFalsy();

      // ft and FL
      expect(
        isLevelLower(1, 'FL' as LevelUnits, 101, 'FT' as LevelUnits),
      ).toBeTruthy();
      expect(
        isLevelLower(1, 'FL' as LevelUnits, 99, 'FT' as LevelUnits),
      ).toBeFalsy();
    });
  });

  describe('getProductFormMode', () => {
    it('should return new if passed canbe of [DRAFTED, DISCARDED, PUBLISHED] and sigmet = null (passed when new)', () => {
      const canBe = ['DRAFTED', 'DISCARDED', 'PUBLISHED'] as ProductCanbe[];
      expect(getProductFormMode(canBe, null)).toBe('new');
    });
    it('should return edit if passed canbe of [DRAFTED, DISCARDED, PUBLISHED] (passed when draft)', () => {
      const canBe = ['DRAFTED', 'DISCARDED', 'PUBLISHED'] as ProductCanbe[];
      expect(getProductFormMode(canBe, fakeSigmetList[0])).toBe('edit');
    });
    it('should return view if passed canbe of [CANCELLED] (passed when published sigmet)', () => {
      const canBe = ['CANCELLED'] as ProductCanbe[];
      expect(getProductFormMode(canBe, fakeSigmetList[2])).toBe('view');
    });
    it('should return view if passed canbe of [] (passed when cancel/cancelled or expired sigmet)', () => {
      const canBe = [] as ProductCanbe[];
      expect(getProductFormMode(canBe, fakeSigmetList[6])).toBe('view');
    });
  });

  describe('getDialogtitle', () => {
    it('should return --New SIGMET-- or --New AIRMET-- if null is passed', () => {
      expect(getDialogtitle(null, 'sigmet')).toBe('New SIGMET');
      expect(getDialogtitle(null, 'airmet')).toBe('New AIRMET');
    });

    it('should return --SIGMET {phenomenon} - saved as draft-- if sigmet with status draft is passed', () => {
      const { sigmet } = fakeSigmetList[0];
      expect(getDialogtitle(sigmet, 'sigmet')).toBe(
        `SIGMET ${
          isInstanceOfSigmet(sigmet) && SigmetPhenomena[sigmet.phenomenon]
        } - saved as draft`,
      );
    });
    it('should return --AIRMET {phenomenon} - saved as draft-- if airmet with status draft is passed', () => {
      const { airmet } = fakeAirmetList[0];
      expect(getDialogtitle(airmet, 'airmet')).toBe(
        `AIRMET ${
          isInstanceOfAirmet(airmet) && AirmetPhenomena[airmet.phenomenon]
        } - saved as draft`,
      );
    });

    it('should return --SIGMET {phenomenon} - {status description}-- if sigmet with status published/expired/cancelled is passed', () => {
      // for published sigmet
      const { sigmet } = fakeSigmetList[2];
      expect(getDialogtitle(sigmet, 'sigmet')).toBe(
        `SIGMET ${
          isInstanceOfSigmet(sigmet) && SigmetPhenomena[sigmet.phenomenon]
        } - ${ProductStatusDescription[sigmet.status]}`,
      );

      // for expired sigmet
      const { sigmet: sigmet2 } = fakeSigmetList[6];
      expect(getDialogtitle(sigmet2, 'sigmet')).toBe(
        `SIGMET ${
          isInstanceOfSigmet(sigmet2) && SigmetPhenomena[sigmet2.phenomenon]
        } - ${ProductStatusDescription[sigmet2.status]}`,
      );

      // For cancelled sigmet
      const { sigmet: sigmet3 } = fakeSigmetList[5];
      expect(getDialogtitle(sigmet3, 'sigmet')).toBe(
        `SIGMET ${
          isInstanceOfSigmet(sigmet3) && SigmetPhenomena[sigmet3.phenomenon]
        } - ${ProductStatusDescription[sigmet3.status]}`,
      );
    });

    it('should return --Cancels SIGMET {seqnum}-- if cancel sigmet is passed', () => {
      const { sigmet } = fakeSigmetList[3];
      expect(getDialogtitle(sigmet, 'sigmet')).toBe(
        `SIGMET Cancels ${
          isInstanceOfCancelSigmet(sigmet) && sigmet.cancelsSigmetSequenceId
        }`,
      );
    });

    it('should return --Cancels AIRMET {seqnum}-- if cancel airmet is passed', () => {
      const { airmet } = fakeAirmetList[3];
      expect(getDialogtitle(airmet, 'airmet')).toBe(
        `AIRMET Cancels ${
          isInstanceOfCancelAirmet(airmet) && airmet.cancelsAirmetSequenceId
        }`,
      );
    });
  });

  describe('getProductFormLifecycleButtons', () => {
    it('should trigger discard', () => {
      const props = {
        canBe: ['DISCARDED'],
        onButtonPress: jest.fn(),
      };
      const { getByTestId } = render(
        getProductFormLifecycleButtons(props.canBe, props.onButtonPress),
      );

      fireEvent.click(getByTestId('productform-dialog-discard'));
      expect(props.onButtonPress).toHaveBeenCalledWith(props.canBe[0]);
    });

    it('should trigger saving as draft', () => {
      const props = {
        canBe: ['DRAFTED'],
        onButtonPress: jest.fn(),
      };
      const { getByTestId } = render(
        getProductFormLifecycleButtons(props.canBe, props.onButtonPress),
      );

      fireEvent.click(getByTestId('productform-dialog-draft'));
      expect(props.onButtonPress).toHaveBeenCalledWith('DRAFT');
    });

    it('should trigger cancel', () => {
      const props = {
        canBe: ['CANCELLED'],
        onButtonPress: jest.fn(),
      };
      const { getByTestId } = render(
        getProductFormLifecycleButtons(props.canBe, props.onButtonPress),
      );

      fireEvent.click(getByTestId('productform-dialog-cancel'));
      expect(props.onButtonPress).toHaveBeenCalledWith(props.canBe[0]);
    });

    it('should trigger publish', () => {
      const props = {
        canBe: ['PUBLISHED'],
        onButtonPress: jest.fn(),
      };
      const { getByTestId } = render(
        getProductFormLifecycleButtons(props.canBe, props.onButtonPress),
      );

      fireEvent.click(getByTestId('productform-dialog-publish'));
      expect(props.onButtonPress).toHaveBeenCalledWith(props.canBe[0]);
    });
  });

  describe('hasValue', () => {
    it('should return true when not null or NaN', () => {
      expect(hasValue(NaN)).toBeFalsy();
      expect(hasValue(null)).toBeFalsy();
      expect(hasValue(1)).toBeTruthy();
      expect(hasValue(0)).toBeTruthy();
      expect(hasValue(1.02)).toBeTruthy();
    });
  });

  describe('removeEmptyNumberFieldValues', () => {
    it('should remove empty numeric field values', () => {
      const { level, ...fakeSigmet } = fakeSigmetList[0].sigmet as Sigmet;
      const product = {
        fakeSigmet,
        vaSigmetVolcanoCoordinates: { latitude: NaN, longitude: NaN },
        level: {
          unit: LevelUnits.FL,
          value: NaN,
        },
      } as unknown as Sigmet;

      expect(removeEmptyNumberFieldValues(product)).toEqual({
        fakeSigmet,
      });
    });
  });

  describe('createSigmetPostParameter', () => {
    it('should not have a uuid when posting a new sigmet', () => {
      expect(fakeNewSigmet.uuid).toBeFalsy();
      expect(createSigmetPostParameter(fakeNewSigmet, 'DRAFT', null)).toEqual({
        changeStatusTo: 'DRAFT',
        sigmet: fakeNewSigmet,
      });
    });
    it('should have a uuid when posting an existing sigmet', () => {
      const updatedSigmet = {
        ...fakeSigmetList[0].sigmet,
        uuid: null,
        change: 'NC' as Sigmet['change'],
      };
      expect(
        createSigmetPostParameter(
          updatedSigmet,
          'DRAFT',
          fakeSigmetList[0].sigmet.uuid,
        ),
      ).toEqual({
        changeStatusTo: 'DRAFT',
        sigmet: { ...updatedSigmet, uuid: fakeSigmetList[0].sigmet.uuid },
      });
    });

    it('should not have post volcanic coordinate fields if non passed in or if empty', () => {
      // not passed
      expect(
        createSigmetPostParameter(fakeSigmetList[0].sigmet, 'DRAFT', null),
      ).toMatchObject({
        changeStatusTo: 'DRAFT',
        sigmet: {
          ...fakeSigmetList[0].sigmet,
          uuid: fakeSigmetList[0].sigmet.uuid,
        },
      });

      // empty
      const updatedSigmet = {
        ...fakeSigmetList[0].sigmet,
        vaSigmetVolcanoCoordinates: { latitude: NaN, longitude: NaN },
      };
      expect(
        createSigmetPostParameter(
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          updatedSigmet,
          'DRAFT',
          fakeSigmetList[0].sigmet.uuid,
        ),
      ).toEqual({
        changeStatusTo: 'DRAFT',
        sigmet: fakeSigmetList[0].sigmet,
      });
    });

    it('should post volcanic coordinate fields if non-empty', () => {
      expect(
        createSigmetPostParameter(fakeSigmetList[1].sigmet, 'DRAFT', null),
      ).toMatchObject({
        changeStatusTo: 'DRAFT',
        sigmet: fakeSigmetList[1].sigmet,
      });
    });
  });

  describe('createAirmetPostParameter', () => {
    it('should not have a uuid when posting a new airmet', () => {
      expect(fakeNewAirmet.uuid).toBeFalsy();
      expect(createAirmetPostParameter(fakeNewAirmet, 'DRAFT', null)).toEqual({
        changeStatusTo: 'DRAFT',
        airmet: fakeNewAirmet,
      });
    });
    it('should have a uuid when posting an existing airmet', () => {
      const updatedAirmet = {
        ...fakeAirmetList[0].airmet,
        uuid: null,
        change: 'NC' as Sigmet['change'],
      };
      expect(
        createAirmetPostParameter(
          updatedAirmet,
          'DRAFT',
          fakeAirmetList[0].airmet.uuid,
        ),
      ).toEqual({
        changeStatusTo: 'DRAFT',
        airmet: { ...updatedAirmet, uuid: fakeAirmetList[0].airmet.uuid },
      });
    });
  });

  describe('useDelayedFormValues', () => {
    beforeEach(() => {
      jest.useFakeTimers();
      jest.spyOn(global, 'clearTimeout');
      jest.spyOn(global, 'setTimeout');
    });
    afterEach(() => {
      jest.clearAllTimers();
      jest.useRealTimers();
    });
    it('should set intial values', () => {
      const expectedData = fakeSigmetList[0].sigmet;
      const { getValues, register, delay } = {
        getValues: (): unknown => expectedData,
        register: jest.fn(),
        delay: 500,
      };

      const { result } = renderHook(() =>
        useDelayedFormValues(getValues, register, delay),
      );

      expect(result.current[0]).toEqual(expectedData);

      result.current[1]();
      expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
    });
    it('should return callable function to trigger update form values and update the form values after timeout', async () => {
      const expectedData = fakeSigmetList[0].sigmet;
      const expectedData2 = fakeSigmetList[1].sigmet;
      const getExpectedData = jest
        .fn()
        .mockImplementationOnce(() => expectedData)
        .mockImplementationOnce(() => expectedData)
        .mockImplementationOnce(() => expectedData2);
      const { getValues, register, delay } = {
        getValues: (): unknown => getExpectedData,
        register: jest.fn(),
        delay: 500,
      };
      const hook = renderHook(() =>
        useDelayedFormValues(getValues, register, delay),
      );
      expect(hook.result.current[0]).toEqual(expectedData);

      hook.result.current[1]();
      expect(clearTimeout).toHaveBeenCalled();
      expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);

      act(() => {
        jest.runOnlyPendingTimers();
      });

      act(() => {
        hook.rerender();
      });
      expect(hook.result.current[0]).toEqual(expectedData2);
    });
    it('should update formvalues if getValues is changed', async () => {
      const expectedData = fakeSigmetList[0].sigmet;
      const { getValues, register, delay } = {
        getValues: (): unknown => expectedData,
        register: jest.fn(),
        delay: 500,
      };

      const hook = renderHook(
        ({ getValues }) => useDelayedFormValues(getValues, register, delay),
        {
          initialProps: { getValues },
        },
      );

      expect(hook.result.current[0]).toEqual(expectedData);

      const expectedData2 = fakeSigmetList[1].sigmet;
      const getValues2 = (): unknown => expectedData2;
      act(() => {
        hook.rerender({ getValues: getValues2 });
      });
      expect(hook.result.current[0]).toEqual(expectedData2);
    });
  });

  describe('getConfirmationDialogTitle', () => {
    it('Should return Save properties when action = CLOSED', () => {
      expect(getConfirmationDialogTitle('CLOSED', 'sigmet')).toEqual(
        'Close SIGMET',
      );
      expect(getConfirmationDialogTitle('CLOSED', 'airmet')).toEqual(
        'Close AIRMET',
      );
    });
    it('Should return Save properties when action = DISCARDED', () => {
      expect(getConfirmationDialogTitle('DISCARDED', 'sigmet')).toEqual(
        'Discard SIGMET',
      );
      expect(getConfirmationDialogTitle('DISCARDED', 'airmet')).toEqual(
        'Discard AIRMET',
      );
    });
    it('Should return Save properties when action = PUBLISHED', () => {
      expect(getConfirmationDialogTitle('PUBLISHED', 'sigmet')).toEqual(
        'Publish',
      );
      expect(getConfirmationDialogTitle('PUBLISHED', 'airmet')).toEqual(
        'Publish',
      );
    });
    it('Should return Save properties when action = CANCELLED', () => {
      expect(getConfirmationDialogTitle('CANCELLED', 'sigmet')).toEqual(
        'Cancel SIGMET',
      );
      expect(getConfirmationDialogTitle('CANCELLED', 'airmet')).toEqual(
        'Cancel AIRMET',
      );
    });
  });

  describe('getConfirmationDialogContent', () => {
    it('Should return correct content when action = CLOSED', () => {
      expect(getConfirmationDialogContent('CLOSED', 'sigmet')).toEqual(
        'Do you want to save any changes made?',
      );
      expect(getConfirmationDialogContent('CLOSED', 'airmet')).toEqual(
        'Do you want to save any changes made?',
      );
    });
    it('Should return correct content when action = DISCARDED', () => {
      expect(getConfirmationDialogContent('DISCARDED', 'sigmet')).toEqual(
        'Are you sure you would like to discard this SIGMET? Its properties will be lost',
      );
      expect(getConfirmationDialogContent('DISCARDED', 'airmet')).toEqual(
        'Are you sure you would like to discard this AIRMET? Its properties will be lost',
      );
    });
    it('Should return correct content when action = PUBLISHED', () => {
      expect(getConfirmationDialogContent('PUBLISHED', 'sigmet')).toEqual(
        'Are you sure you want to publish this SIGMET?',
      );
      expect(getConfirmationDialogContent('PUBLISHED', 'airmet')).toEqual(
        'Are you sure you want to publish this AIRMET?',
      );
    });
    it('Should return correct content when action = CANCELLED', () => {
      expect(getConfirmationDialogContent('CANCELLED', 'airmet')).toEqual(
        'Are you sure you want to cancel this AIRMET?',
      );
      expect(getConfirmationDialogContent('CANCELLED', 'sigmet')).toEqual(
        'Are you sure you want to cancel this SIGMET?',
      );
    });
  });
  describe('getConfirmationDialogButtonLabel', () => {
    it('Should return correct button label when action = DISCARDED', () => {
      expect(getConfirmationDialogButtonLabel('DISCARDED', 'sigmet')).toEqual(
        'Discard SIGMET',
      );
      expect(getConfirmationDialogButtonLabel('DISCARDED', 'airmet')).toEqual(
        'Discard AIRMET',
      );
    });
    it('Should return correct button label when action = PUBLISHED', () => {
      expect(getConfirmationDialogButtonLabel('PUBLISHED', 'sigmet')).toEqual(
        'Publish',
      );
      expect(getConfirmationDialogButtonLabel('PUBLISHED', 'airmet')).toEqual(
        'Publish',
      );
    });
    it('Should return correct button label when action = CANCELLED', () => {
      expect(getConfirmationDialogButtonLabel('CANCELLED', 'sigmet')).toEqual(
        'Cancel',
      );
      expect(getConfirmationDialogButtonLabel('CANCELLED', 'airmet')).toEqual(
        'Cancel',
      );
    });
    it('Should return correct button label when other action', () => {
      expect(getConfirmationDialogButtonLabel('DRAFT', 'sigmet')).toEqual(
        'Save and close',
      );
      expect(getConfirmationDialogButtonLabel('DRAFT', 'airmet')).toEqual(
        'Save and close',
      );
    });
  });

  describe('getConfirmationDialogCancelLabel', () => {
    it('Should return correct button label when action = ClOSED', () => {
      expect(getConfirmationDialogCancelLabel('CLOSED')).toEqual(
        'Discard and close',
      );
    });

    it('Should return undefined', () => {
      expect(getConfirmationDialogCancelLabel('DRAFT')).toBeUndefined();
    });
  });

  describe('getProductIssueDate', () => {
    it('should return no issue date if not published yet', () => {
      expect(getProductIssueDate(fakeNewSigmet, null)).toEqual(
        '(Not published)',
      );
      expect(
        getProductIssueDate(fakeSigmetList[0].sigmet as Sigmet, null),
      ).toEqual('(Not published)');
    });
    it('should return issue date if published and expired', () => {
      expect(
        getProductIssueDate(fakeSigmetList[1].sigmet as Sigmet, null),
      ).toEqual(
        `${moment
          .utc(fakeSigmetList[1].sigmet.issueDate)
          .format('YYYY/MM/DD HH:mm')} UTC`,
      );
      expect(
        getProductIssueDate(fakeSigmetList[4].sigmet as Sigmet, null),
      ).toEqual(
        `${moment
          .utc(fakeSigmetList[1].sigmet.issueDate)
          .format('YYYY/MM/DD HH:mm')} UTC`,
      );
    });
    it('should return issue date if cancel sigmet', () => {
      expect(
        getProductIssueDate(null, fakeSigmetList[3].sigmet as CancelSigmet),
      ).toEqual(
        `${moment
          .utc(fakeSigmetList[3].sigmet.issueDate)
          .format('YYYY/MM/DD HH:mm')} UTC`,
      );
    });
  });

  describe('getAllowedUnits', () => {
    const FIRAllowedUnits = {
      EHAA: [
        {
          unit_type: 'movement_unit',
          allowed_units: ['KT'],
        },
      ],
      EBBU: [
        {
          unit_type: 'movement_unit',
          allowed_units: ['KT', 'RANDOMUNIT'],
        },
        {
          unit_type: 'level_unit',
          allowed_units: ['FT', 'FL'],
        },
      ],
    };
    it('should return all units as defined in the passed type if no fir selected or no allowed units passed for selected fir or if fir not present in options', () => {
      expect(
        getAllowedUnits(
          undefined,
          FIRAllowedUnits,
          'movement_unit',
          MovementUnit,
        ),
      ).toBe(MovementUnit);
      expect(getAllowedUnits('EHAA', {}, 'movement_unit', MovementUnit)).toBe(
        MovementUnit,
      );
      expect(
        getAllowedUnits('EGTT', FIRAllowedUnits, 'movement_unit', MovementUnit),
      ).toBe(MovementUnit);
    });
    it('should return all units as defined in the passed type if no fir no unit selection provided for that FIR', () => {
      expect(
        getAllowedUnits('EHAA', FIRAllowedUnits, 'level_unit', LevelUnits),
      ).toBe(LevelUnits);
    });
    it('should return units that are a cross section of the provided type and the allowed units if allowed units provided for selected fir (units not available in the types should be ignored)', () => {
      expect(
        getAllowedUnits('EHAA', FIRAllowedUnits, 'movement_unit', MovementUnit),
      ).toStrictEqual({ KT: 'kt' });
      expect(
        getAllowedUnits('EBBU', FIRAllowedUnits, 'level_unit', LevelUnits),
      ).toStrictEqual({ FT: 'ft', FL: 'FL' });
      // It should ignore RANDOMUNIT as this is not defined in types
      expect(
        getAllowedUnits('EBBU', FIRAllowedUnits, 'movement_unit', MovementUnit),
      ).toStrictEqual({ KT: 'kt' });
    });
  });

  describe('rewindGeometry', () => {
    const clockwisePoly = JSON.parse(
      '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"stroke":"#f24a00","stroke-width":1.5,"stroke-opacity":1,"fill":"#f24a00","fill-opacity":0.25,"selectionType":"poly"},"geometry":{"type":"Polygon","coordinates":[[[3.9727570104176655,52.798401500118025],[3.345035685215513,53.65515457833232],[3.9727570104176655,54.741996695611284],[5.906138692040295,54.33411924173588],[5.680159014967519,53.13111016712073],[3.9727570104176655,52.798401500118025]]]}}]}',
    );
    const counterClockwisePoly = JSON.parse(
      '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"stroke":"#f24a00","stroke-width":1.5,"stroke-opacity":1,"fill":"#f24a00","fill-opacity":0.25,"selectionType":"poly"},"geometry":{"type":"Polygon","coordinates":[[[3.9727570104176655,52.798401500118025],[5.680159014967519,53.13111016712073],[5.906138692040295,54.33411924173588],[3.9727570104176655,54.741996695611284],[3.345035685215513,53.65515457833232],[3.9727570104176655,52.798401500118025]]]}}]}',
    );
    const singlePoint = JSON.parse(
      '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{"stroke":"#f24a00","stroke-width":1.5,"stroke-opacity":1,"fill":"#f24a00","fill-opacity":0.25,"selectionType":"point"},"geometry":{"type":"Point","coordinates":[4.262040673801408,54.168394541491786]}}]}',
    );

    it('should rewind a clockwise Polygon', () => {
      expect(rewindGeometry(clockwisePoly)).toEqual(counterClockwisePoly);
    });
    it('should not rewind counterclockwise Polygon', () => {
      expect(rewindGeometry(counterClockwisePoly)).toEqual(
        counterClockwisePoly,
      );
    });
    it('should not affect point', () => {
      expect(rewindGeometry(singlePoint)).toEqual(singlePoint);
    });
  });
});
