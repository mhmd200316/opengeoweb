/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';

import { useFormContext } from 'react-hook-form';
import { Backdrop, CircularProgress } from '@mui/material';
import {
  useConfirmationDialog,
  usePreventBrowserClose,
  AlertBanner,
  useDraftFormHelpers,
  isAxiosError,
  getAxiosErrorMessage,
} from '@opengeoweb/shared';

import { useApiContext } from '@opengeoweb/api';

import { AxiosError } from 'axios';
import { ContentDialog } from '../ContentDialog';
import {
  CancelSigmet,
  isInstanceOfCancelSigmet,
  SigmetPhenomena,
  Sigmet,
  ProductActions,
  FormMode,
  CancelAirmet,
  Airmet,
  ProductType,
  isInstanceOfCancelSigmetOrAirmet,
  isInstanceOfCancelAirmet,
  SigmetFromFrontend,
  AirmetFromFrontend,
} from '../../types';
import { AirmetForm, SigmetForm } from '.';
import {
  createSigmetPostParameter,
  getDialogtitle,
  getProductFormLifecycleButtons,
  getConfirmationDialogButtonLabel,
  getConfirmationDialogContent,
  getConfirmationDialogTitle,
  getConfirmationDialogCancelLabel,
  createAirmetPostParameter,
} from './utils';

import { CancelSigmetVolcanicMovements } from './SigmetForm/SigmetCancelVolcanicMovements';
import { SigmetAirmetApi } from '../../utils/api';

const isVAPhenomenon = (
  product: Sigmet | CancelSigmet | Airmet | CancelAirmet,
  action: ProductActions,
): boolean => {
  return (
    action === 'CANCELLED' &&
    product !== null &&
    !isInstanceOfCancelSigmetOrAirmet(product) &&
    product.phenomenon === ('VA_CLD' as SigmetPhenomena)
  );
};

const prepareCancelSigmet = (sigmet: Sigmet, movement: string): Sigmet => {
  // If movement UNKNOWN, don't send to BE
  if (movement === 'UNKNOWN') {
    return sigmet;
  }
  return {
    ...sigmet,
    vaSigmetMoveToFIR: movement,
  };
};

interface ProductFormDialogContentProps {
  isOpen: boolean;
  toggleDialogStatus: () => void;
  mode: FormMode;
  productType: ProductType;
  productCanBe: string[];
  product: Sigmet | CancelSigmet | Airmet | CancelAirmet;
}

export const ProductFormDialogContent: React.FC<ProductFormDialogContentProps> =
  ({
    isOpen,
    toggleDialogStatus,
    mode,
    productType,
    productCanBe,
    product,
  }: ProductFormDialogContentProps) => {
    const { api } = useApiContext<SigmetAirmetApi>();
    const apiCall = productType === 'sigmet' ? api.postSigmet : api.postAirmet;
    const { handleSubmit, reset, formState, watch } = useFormContext();
    const { isDirty } = formState;
    const { toggleIsDraft, DraftFieldHelper } = useDraftFormHelpers();
    usePreventBrowserClose(isDirty);

    const confirmationDialog = useConfirmationDialog();

    const [showLoader, setLoader] = React.useState(false);
    const [errorStoreMessage, setErrorStoreMessage] = React.useState(false);
    const [errorStoreMessageText, setErrorStoreMessageText] =
      React.useState('');

    React.useEffect(() => {
      // set the values of the newly selected product
      reset(product !== null ? { ...product } : {});
      return (): void => {
        // reset form on unmount
        reset({});
      };
    }, [reset, product]);

    const postProduct = (_productToPost, newStatus): void => {
      // remove hidden helper fields
      const { IS_DRAFT, ...productToPost } = _productToPost;
      const uuid = product !== null && product.uuid ? product.uuid : null;
      const postProductParam =
        productType === 'sigmet'
          ? createSigmetPostParameter(productToPost, newStatus, uuid)
          : createAirmetPostParameter(productToPost, newStatus, uuid);

      setLoader(true);
      setErrorStoreMessage(false);
      setErrorStoreMessageText('');

      apiCall(postProductParam as SigmetFromFrontend & AirmetFromFrontend)
        .then(() => {
          onToggleDialog();
        })
        .catch((error: AxiosError | Error) => {
          setErrorStoreMessage(true);
          if (isAxiosError(error)) {
            setErrorStoreMessageText(getAxiosErrorMessage(error as AxiosError));
          }
          setLoader(false);
        });
    };

    const onToggleDialog = (): void => {
      setLoader(false);
      toggleDialogStatus();
    };

    const showConfirmDialog = (action: ProductActions): Promise<void> =>
      confirmationDialog({
        title: getConfirmationDialogTitle(action, productType),
        description: getConfirmationDialogContent(action, productType),
        confirmLabel: getConfirmationDialogButtonLabel(action, productType),
        cancelLabel: getConfirmationDialogCancelLabel(action),
        catchOnCancel: action === 'CLOSED',
        content: isVAPhenomenon(product, action) ? (
          <CancelSigmetVolcanicMovements sigmet={product as Sigmet} />
        ) : null,
      });

    const onProductButtonPress = (action: ProductActions): void => {
      switch (action) {
        case 'CLOSED':
          if (mode === 'view' || !isDirty) {
            onToggleDialog();
          } else {
            toggleIsDraft(true);
            showConfirmDialog(action)
              .then(() => {
                handleSubmit((formValues) => {
                  postProduct(formValues, 'DRAFT');
                })();
              })
              .catch((reason) => {
                if (reason === 'CANCELLED') {
                  // close without save
                  onToggleDialog();
                }
              });
          }
          break;
        case 'CANCELLED':
          showConfirmDialog(action).then(() => {
            // if volcanic sigmet - add to which FIR it's moving
            if (isVAPhenomenon(product, action)) {
              postProduct(
                prepareCancelSigmet(
                  product as Sigmet,
                  watch('vaSigmetMoveToFIR'),
                ),
                action,
              );
            } else {
              postProduct(product, action);
            }
          });
          break;

        case 'DISCARDED':
          showConfirmDialog(action).then(() => {
            // If we're discarding a new SIGMET/AIRMET - just close the dialog
            if (mode === 'new') {
              onToggleDialog();
            } else {
              // If we're discarding an existing SIGMET/AIRMET - make call to BE
              postProduct(product, action);
            }
          });
          break;
        default:
          toggleIsDraft(action === 'DRAFT');
          // validate form
          handleSubmit((formValues) => {
            if (action === 'DRAFT') {
              // NO MODAL, SAVE DIRECTLY
              postProduct(formValues, action);
            } else {
              // YES MODAL
              showConfirmDialog(action).then(() => {
                postProduct(formValues, action);
              });
            }
          })();
          break;
      }
    };

    const title = getDialogtitle(product, productType);

    return (
      <ContentDialog
        open={isOpen}
        toggleDialogStatus={(): void => onProductButtonPress('CLOSED')}
        data-testid="productform-dialog"
        title={title}
        fullWidth
        maxWidth="xl"
        disableEscapeKeyDown
        onClose={(): void => onProductButtonPress('CLOSED')}
        alertBanner={
          errorStoreMessage ? (
            <AlertBanner
              severity="error"
              title="An error has occurred while saving, please try again"
              info={errorStoreMessageText}
            />
          ) : null
        }
        options={getProductFormLifecycleButtons(
          productCanBe,
          onProductButtonPress,
        )}
        style={{ zIndex: 1000 }}
      >
        {productType === 'sigmet' ? (
          <SigmetForm
            mode={mode}
            isCancelSigmet={
              product !== null && isInstanceOfCancelSigmet(product)
            }
            initialSigmet={
              product !== null && !isInstanceOfCancelSigmet(product)
                ? (product as Sigmet)
                : null
            }
            initialCancelSigmet={
              product !== null && isInstanceOfCancelSigmet(product)
                ? product
                : null
            }
          />
        ) : (
          <AirmetForm
            mode={mode}
            isCancelAirmet={
              product !== null && isInstanceOfCancelAirmet(product)
            }
            initialAirmet={
              product !== null && !isInstanceOfCancelAirmet(product)
                ? (product as Airmet)
                : null
            }
            initialCancelAirmet={
              product !== null && isInstanceOfCancelAirmet(product)
                ? product
                : null
            }
          />
        )}

        <DraftFieldHelper />

        <Backdrop
          data-testid="loader"
          open={showLoader}
          sx={{
            zIndex: 1500,
            color: '#fff',
          }}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </ContentDialog>
    );
  };

export default ProductFormDialogContent;
