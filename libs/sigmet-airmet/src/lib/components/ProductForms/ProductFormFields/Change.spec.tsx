/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import Change from './Change';

describe('components/ProductForms/ProductFormFields/Change', () => {
  it('should be possible to select a change', async () => {
    const { getByTestId } = render(
      <ReactHookFormProvider>
        <Change isDisabled={false} isReadOnly={false} />
      </ReactHookFormProvider>,
    );
    const field = getByTestId('change-WKN');

    await waitFor(() => {
      expect(field.querySelector('.Mui-checked')).toBeFalsy();
    });

    fireEvent.click(field);
    await waitFor(() => {
      expect(field.querySelector('.Mui-checked')).toBeTruthy();
    });
  });
  it('should show change as disabled', async () => {
    const { getByTestId } = render(
      <ReactHookFormProvider>
        <Change isDisabled isReadOnly={false} />
      </ReactHookFormProvider>,
    );

    await waitFor(() => {
      expect(getByTestId('change-WKN').getAttribute('class')).toContain(
        'Mui-disabled',
      );
      expect(getByTestId('change-NC').getAttribute('class')).toContain(
        'Mui-disabled',
      );
      expect(getByTestId('change-INTSF').getAttribute('class')).toContain(
        'Mui-disabled',
      );
    });
  });
  it('should show change as readonly', () => {
    const { queryByTestId } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            change: 'WKN',
          },
        }}
      >
        <Change isDisabled isReadOnly />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('change-WKN')).toBeTruthy();
    expect(queryByTestId('change-WKN').getAttribute('class')).toContain(
      'Mui-disabled',
    );
    expect(queryByTestId('change-NC')).toBeFalsy();
    expect(queryByTestId('change-INTSF')).toBeFalsy();
  });
});
