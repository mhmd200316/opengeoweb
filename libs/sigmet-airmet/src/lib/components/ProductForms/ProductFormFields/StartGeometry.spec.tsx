/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Button } from '@mui/material';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { useFormContext } from 'react-hook-form';
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';

import StartGeometry, {
  maxFeaturePointsMessage,
  maximum6PointsMessage,
  multiIntersectionsMessage,
  noIntersectionMessage,
} from './StartGeometry';
import { getFir } from '../../../utils/getFir';

describe('components/ProductForms/ProductFormFields/StartGeometry', () => {
  it('should not show startGeometry when readonly', () => {
    const { queryByTestId } = render(
      <ReactHookFormProvider>
        <StartGeometry
          isReadOnly={true}
          geoJSONs={{ start: null, end: null }}
          drawMode={{ start: null, end: null }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('startGeometry')).toBeNull();
  });
  it('should show startGeometry when not readonly', () => {
    const { queryByTestId } = render(
      <ReactHookFormProvider>
        <StartGeometry
          geoJSONs={{ start: null, end: null }}
          drawMode={{ start: null, end: null }}
          setDrawModeType={(): void => {}}
        />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('startGeometry')).toBeTruthy();
  });
  it('should set the drawMode type', () => {
    const mockSetDrawModeType = jest.fn();
    const { container } = render(
      <ReactHookFormProvider>
        <StartGeometry
          geoJSONs={{ start: null, end: null }}
          drawMode={{ start: null, end: null }}
          setDrawModeType={mockSetDrawModeType}
        />
      </ReactHookFormProvider>,
    );

    fireEvent.click(
      container.querySelector('[data-testid="drawtools-point"] input'),
    );
    expect(mockSetDrawModeType).toHaveBeenCalledWith('POINT', 'start');
  });

  it('should show an error message for maximum 6 points when drawing has more points and is not equal to the FIR', async () => {
    const start = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [1.4359186342468666, 54.09406326927267],
                [2.219743723443398, 50.857775603890964],
                [4.434901584216204, 52.276920619051204],
                [5.116488618300145, 53.20532566834704],
                [5.252806025116932, 54.23372984146369],
                [4.571218991032993, 54.669618903123215],
                [3.242124274569308, 54.669618903123215],
                [1.4359186342468666, 54.09406326927267],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionStart = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.8285861239569896, 54.53143],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.986539573126695, 52.70658863800587],
                [3.412521, 52.526431],
                [5.184647, 52.877491],
                [5.457282, 53.652036],
                [4.877933, 54.53143],
                [2.8285861239569896, 54.53143],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <StartGeometry
            geoJSONs={{ start: null, end: null }}
            drawMode={{ start: null, end: null }}
            setDrawModeType={jest.fn()}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { queryByTestId, getByText, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            startGeometry: start,
            startGeometryIntersect: intersectionStart,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('startGeometry')).toBeTruthy();
    fireEvent.click(getByText('Validate'));
    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(startGeometryHelperTextSelector).textContent,
    ).toEqual(maximum6PointsMessage);
  });
  it('should not show an error message for maximum 6 points when FIR is selected', async () => {
    const start = getFir();
    const intersectionStart = getFir();

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <StartGeometry
            geoJSONs={{ start: null, end: null }}
            drawMode={{ start: null, end: null }}
            setDrawModeType={jest.fn()}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { queryByTestId, getByText, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            startGeometry: start,
            startGeometryIntersect: intersectionStart,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('startGeometry')).toBeTruthy();
    fireEvent.click(getByText('Validate'));
    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeFalsy(),
    );
  });
  it('should not show an error message for maximum 6 points when drawing has 6 points', async () => {
    const start = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [1.4359186342468666, 54.09406326927267],
                [2.219743723443398, 50.857775603890964],
                [4.434901584216204, 52.276920619051204],
                [5.116488618300145, 53.20532566834704],
                [4.571218991032993, 54.669618903123215],
                [3.242124274569308, 54.669618903123215],
                [1.4359186342468666, 54.09406326927267],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionStart = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.8285861239569896, 54.53143],
                [2.761908, 54.379261],
                [3.15576, 52.913554],
                [2.986539573126695, 52.70658863800587],
                [3.412521, 52.526431],
                [5.457282, 53.652036],
                [4.877933, 54.53143],
                [2.8285861239569896, 54.53143],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <StartGeometry
            geoJSONs={{ start: null, end: null }}
            drawMode={{ start: null, end: null }}
            setDrawModeType={jest.fn()}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { queryByTestId, getByText, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            startGeometry: start,
            startGeometryIntersect: intersectionStart,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('startGeometry')).toBeTruthy();
    fireEvent.click(getByText('Validate'));
    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeFalsy(),
    );
  });
  it('should show an error message for drawing inside the FIR when the drawing has more than 6 points but has no intersection with the FIR ', async () => {
    const start = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [8.899296657466017, 54.37292536640029],
                [8.69482054724084, 52.19343690011639],
                [10.56918489097167, 51.41375109471964],
                [13.056977565378059, 51.41375109471964],
                [15.272135426150864, 52.3810545583907],
                [14.181596171616562, 54.57096052166186],
                [11.830120904026963, 55.15933041390611],
                [8.899296657466017, 54.37292536640029],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionStart = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [[]],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <StartGeometry
            geoJSONs={{ start: null, end: null }}
            drawMode={{ start: null, end: null }}
            setDrawModeType={jest.fn()}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { queryByTestId, getByText, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            startGeometry: start,
            startGeometryIntersect: intersectionStart,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('startGeometry')).toBeTruthy();
    fireEvent.click(getByText('Validate'));
    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(startGeometryHelperTextSelector).textContent,
    ).toEqual(noIntersectionMessage);
  });

  it('should show a warning message if there are multiple intersections with the FIR', async () => {
    const start = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [2.763574121746228, 55.7517694491503],
                [7.577660215570578, 54.441809771698],
                [4.554179315180896, 56.77870860348328],
                [2.763574121746228, 55.7517694491503],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionStart = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'MultiPolygon',
            coordinates: [
              [
                [
                  [4.365729269907419, 55.315807175634454],
                  [3.299427839634416, 55.605958027800156],
                  [3.368817, 55.764314],
                  [4.331914, 55.332644],
                  [4.365729269907419, 55.315807175634454],
                ],
              ],
              [
                [
                  [6.500001602574285, 54.735051191917506],
                  [5.526314849017847, 55.0000007017522],
                  [6.500002, 55.000002],
                  [6.500001602574285, 54.735051191917506],
                ],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const { queryByTestId, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {},
        }}
      >
        <StartGeometry
          geoJSONs={{ start, intersectionStart, end: null }}
          drawMode={{ start: null, end: null }}
          setDrawModeType={jest.fn()}
        />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('startGeometry')).toBeTruthy();
    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(startGeometryHelperTextSelector).textContent,
    ).toEqual(multiIntersectionsMessage);
  });

  it('should show an error message if more than two polygons are drawn', async () => {
    const start = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#f24a00',
            'stroke-width': 1.5,
            'stroke-opacity': 1,
            fill: '#f24a00',
            'fill-opacity': 0.25,
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [3.996575273929873, 56.338767599638814],
                [5.3666880610038135, 56.951914773869866],
                [5.844634382076118, 55.57175436782723],
                [3.996575273929873, 56.338767599638814],
              ],
              [
                [4.5382477711451505, 53.0637004930805],
                [5.334824972932328, 53.87919160339488],
                [6.099539086648015, 52.446617738370804],
                [4.5382477711451505, 53.0637004930805],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionStart = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#f24a00',
            'stroke-width': 1.5,
            'stroke-opacity': 1,
            fill: '#f24a00',
            'fill-opacity': 0.5,
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [3.996575273929873, 56.338767599638814],
                [5.3666880610038135, 56.951914773869866],
                [5.844634382076118, 55.57175436782723],
                [3.996575273929873, 56.338767599638814],
              ],
              [
                [4.5382477711451505, 53.0637004930805],
                [5.334824972932328, 53.87919160339488],
                [6.099539086648015, 52.446617738370804],
                [4.5382477711451505, 53.0637004930805],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <StartGeometry
            geoJSONs={{ start: null, end: null }}
            drawMode={{ start: null, end: null }}
            setDrawModeType={jest.fn()}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { queryByTestId, getByText, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            startGeometry: start,
            startGeometryIntersect: intersectionStart,
          },
        }}
      >
        <Wrapper />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('startGeometry')).toBeTruthy();
    fireEvent.click(getByText('Validate'));
    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(startGeometryHelperTextSelector).textContent,
    ).toEqual('Only one start position drawing is allowed');
  });

  it('should show a warning when intersection has more than 6 points', async () => {
    const start = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [3.2647803798922106, 52.51276033513897],
                [3.2647803798922106, 50.35440809900966],
                [8.37325616160738, 50.35440809900966],
                [8.37325616160738, 52.51276033513897],
                [3.2647803798922106, 52.51276033513897],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;
    const intersectionStart = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            selectionType: 'poly',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [3.26478, 52.51276],
                [3.26478, 51.37972798677809],
                [3.370001, 51.369722],
                [3.362223, 51.320002],
                [3.36389, 51.313608],
                [3.373613, 51.309999],
                [3.952501, 51.214441],
                [4.397501, 51.452776],
                [5.078611, 51.391665],
                [5.848333, 51.139444],
                [5.651667, 50.824717],
                [6.011797, 50.757273],
                [5.934168, 51.036386],
                [6.222223, 51.361666],
                [5.94639, 51.811663],
                [6.405001, 51.830828],
                [7.053095, 52.237764],
                [7.031389, 52.268885],
                [7.063612, 52.346109],
                [7.065557, 52.385828],
                [7.082588115904894, 52.51276],
                [3.26478, 52.51276],
              ],
            ],
          },
        },
      ],
    } as GeoJSON.FeatureCollection;

    const { queryByTestId, container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {},
        }}
      >
        <StartGeometry
          geoJSONs={{ start, intersectionStart, end: null }}
          drawMode={{ start: null, end: null }}
          setDrawModeType={jest.fn()}
        />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('startGeometry')).toBeTruthy();
    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(startGeometryHelperTextSelector).textContent,
    ).toEqual(maxFeaturePointsMessage);
  });
});
