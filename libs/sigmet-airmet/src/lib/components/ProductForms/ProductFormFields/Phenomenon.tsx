/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, Typography, MenuItem } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormSelect } from '@opengeoweb/form-fields';

import { styles } from '../ProductForm.styles';
import { AirmetPhenomena, ProductType, SigmetPhenomena } from '../../../types';

interface PhenomenonProps {
  productType: ProductType;
  isDisabled: boolean;
  onChange?: () => void;
}

const Phenomenon: React.FC<PhenomenonProps> = ({
  productType,
  isDisabled,
  onChange = (): void => null,
}: PhenomenonProps) => {
  const { getValues, setValue, trigger } = useFormContext();
  const phenomenaList =
    productType === 'sigmet' ? SigmetPhenomena : AirmetPhenomena;

  return (
    <Grid item container spacing={2}>
      <Grid item xs={4} container justifyContent="flex-end">
        <Typography variant="subtitle1" sx={styles.label}>
          What
        </Typography>
      </Grid>
      <Grid item xs={8} container justifyContent="flex-start">
        <ReactHookFormSelect
          name="phenomenon"
          label="Select phenomenon"
          rules={{ required: true }}
          disabled={isDisabled}
          data-testid="phenomenon"
          onChange={(): Promise<boolean> | void => {
            if (getValues('movementType') === 'NO_VA_EXP') {
              setValue('movementType', null);
            }
            if (getValues('phenomenon') !== 'SFC_VIS') {
              setValue('visibilityValue', null);
              setValue('visibilityCause', null);
            }
            if (getValues('phenomenon') !== 'SFC_WIND') {
              setValue('windDirection', null);
              setValue('windUnit', null);
              setValue('windSpeed', null);
            }
            onChange();
            return trigger([
              ...(getValues('validDateStart') ? ['validDateStart'] : []),
              ...(getValues('validDateEnd') ? ['validDateEnd'] : []),
            ]);
          }}
          autoFocus
        >
          {Object.keys(phenomenaList).map((key) => (
            <MenuItem value={key} key={key}>
              {phenomenaList[key]}
            </MenuItem>
          ))}
        </ReactHookFormSelect>
      </Grid>
    </Grid>
  );
};

export default Phenomenon;
