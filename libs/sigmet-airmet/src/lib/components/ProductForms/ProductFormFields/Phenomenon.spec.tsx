/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';

import Phenomenon from './Phenomenon';
import { SigmetPhenomena } from '../../../types';

describe('components/ProductForms/ProductFormFields/Phenomenon', () => {
  it('should be possible to select a sigmet phenomenon', async () => {
    const { container, findByText, getByRole } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'EMBD_TS',
          },
        }}
      >
        <Phenomenon productType="sigmet" isDisabled={false} />
      </ReactHookFormProvider>,
    );
    fireEvent.mouseDown(getByRole('button'));

    const menuItem = await findByText('Radioactive cloud');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(
        container.querySelector('[name=phenomenon]').previousElementSibling
          .textContent,
      ).toEqual('Radioactive cloud');
    });
  });

  it('should be possible to select an airmet phenomenon', async () => {
    const { container, findByText, getByRole } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'BKN_CLD',
          },
        }}
      >
        <Phenomenon productType="airmet" isDisabled={false} />
      </ReactHookFormProvider>,
    );
    fireEvent.mouseDown(getByRole('button'));

    const menuItem = await findByText('Surface wind');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(
        container.querySelector('[name=phenomenon]').previousElementSibling
          .textContent,
      ).toEqual('Surface wind');
    });
  });

  it('should show the phenomenon as disabled', () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            phenomenon: 'EMBD_TS',
          },
        }}
      >
        <Phenomenon productType="sigmet" isDisabled />
      </ReactHookFormProvider>,
    );
    const phenomenon = container.querySelector('[name=phenomenon]')
      .previousElementSibling;
    expect(phenomenon.textContent).toEqual(SigmetPhenomena['EMBD_TS']);
    expect(phenomenon.getAttribute('class')).toContain('Mui-disabled');
  });
});
