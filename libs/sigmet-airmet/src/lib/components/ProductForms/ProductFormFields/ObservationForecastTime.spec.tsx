/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import ObservationForecastTime, {
  ForecastTimeValidation,
} from './ObservationForecastTime';

describe('components/ProductForms/ProductFormFields/ObservationForecastTime', () => {
  it('should not be possible to set a date later then validDateStart when isObservationOrForecast = OBS', async () => {
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            isObservationOrForecast: 'OBS',
            validDateStart: '2020-11-17T13:03+00:00',
          },
        }}
      >
        <ObservationForecastTime
          isDisabled={false}
          isReadOnly={false}
          helperText=""
        />
      </ReactHookFormProvider>,
    );

    const field = container.querySelector('[name="observationOrForecastTime"]');

    expect(queryByText('Observation time cannot be in the future')).toBeFalsy();

    fireEvent.change(field, { target: { value: '2120-11-20T13:03+00:00' } });

    await waitFor(() => {
      expect(container.querySelector('.Mui-error')).toBeTruthy();
      expect(
        queryByText('Observation time cannot be in the future'),
      ).toBeTruthy();
    });

    fireEvent.change(field, { target: { value: '2020-11-16T13:03+00:00' } });

    await waitFor(() => {
      expect(container.querySelector('.Mui-error')).toBeFalsy();
      expect(
        queryByText('Observation time cannot be in the future'),
      ).toBeFalsy();
    });
  });

  it('should not be possible to set a date ouside the valid time when isObservationOrForecast = FCST', async () => {
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            isObservationOrForecast: 'FCST',
            validDateStart: '2020-11-17T13:03+00:00',
            validDateEnd: '2020-11-17T14:03+00:00',
          },
        }}
      >
        <ObservationForecastTime
          isDisabled={false}
          isReadOnly={false}
          helperText=""
        />
      </ReactHookFormProvider>,
    );

    const field = container.querySelector('[name="observationOrForecastTime"]');

    expect(queryByText(ForecastTimeValidation)).toBeFalsy();

    fireEvent.change(field, { target: { value: '2020-11-16T13:03+00:00' } });

    await waitFor(() => {
      expect(container.querySelector('.Mui-error')).toBeTruthy();
      expect(queryByText(ForecastTimeValidation)).toBeTruthy();
    });

    fireEvent.change(field, { target: { value: '2020-11-18T13:03+00:00' } });

    await waitFor(() => {
      expect(container.querySelector('.Mui-error')).toBeTruthy();
      expect(queryByText(ForecastTimeValidation)).toBeTruthy();
    });

    fireEvent.change(field, { target: { value: '2020-11-17T13:33+00:00' } });

    await waitFor(() => {
      expect(container.querySelector('.Mui-error')).toBeFalsy();
      expect(queryByText(ForecastTimeValidation)).toBeFalsy();
    });
  });

  it('should show the correct readOnly and disabled input', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            observationOrForecastTime: '2020-11-17T13:03+00:00',
          },
        }}
      >
        <ObservationForecastTime isDisabled isReadOnly helperText="" />
      </ReactHookFormProvider>,
    );

    const input = container.querySelector('[name="observationOrForecastTime"]');

    await waitFor(() => {
      expect(input).toBeTruthy();
      expect(
        (input.parentNode as Element).getAttribute('disabled'),
      ).toBeDefined();
    });
  });

  it('should hide the input when readOnly and no value', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            observationOrForecastTime: null,
          },
        }}
      >
        <ObservationForecastTime isDisabled={false} isReadOnly helperText="" />
      </ReactHookFormProvider>,
    );

    await waitFor(() => {
      expect(
        container.querySelector('[name="observationOrForecastTime"]'),
      ).toBeFalsy();
    });
  });

  it('should handle a helperText', async () => {
    const { queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            observationOrForecastTime: null,
          },
        }}
      >
        <ObservationForecastTime
          isDisabled={false}
          isReadOnly={false}
          helperText="test-text"
        />
      </ReactHookFormProvider>,
    );

    await waitFor(() => {
      expect(queryByText('test-text')).toBeTruthy();
    });
  });
});
