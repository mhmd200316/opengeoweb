/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { Grid } from '@mui/material';
import { ReactHookFormRadioGroup } from '@opengeoweb/form-fields';

import { useDraftFormHelpers } from '@opengeoweb/shared';
import { RadioButtonAndLabel } from './RadioButtonAndLabel';

interface ObservationForecastProps {
  isDisabled: boolean;
  isReadOnly: boolean;
}

const ObservationForecast: React.FC<ObservationForecastProps> = ({
  isDisabled,
  isReadOnly,
}: ObservationForecastProps) => {
  const { watch, trigger, getValues } = useFormContext();

  const { isRequired } = useDraftFormHelpers();

  return (
    <Grid item container spacing={2}>
      <Grid item xs={4} container justifyContent="flex-end" />
      <Grid item xs={8} container justifyContent="flex-start">
        <ReactHookFormRadioGroup
          name="isObservationOrForecast"
          disabled={isDisabled}
          rules={{
            validate: {
              isRequired,
            },
          }}
          onChange={(): void => {
            if (getValues('observationOrForecastTime')) {
              trigger('observationOrForecastTime');
            }
          }}
        >
          {(!isReadOnly || watch('isObservationOrForecast') === 'OBS') && (
            <RadioButtonAndLabel
              value="OBS"
              label="Observed"
              disabled={isDisabled}
              data-testid="isObservationOrForecast-OBS"
            />
          )}
          {(!isReadOnly || watch('isObservationOrForecast') === 'FCST') && (
            <RadioButtonAndLabel
              value="FCST"
              label="Forecast"
              disabled={isDisabled}
              data-testid="isObservationOrForecast-FCST"
            />
          )}
        </ReactHookFormRadioGroup>
      </Grid>
    </Grid>
  );
};

export default ObservationForecast;
