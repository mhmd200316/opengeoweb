/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { Grid, Typography } from '@mui/material';
import moment from 'moment';
import {
  ReactHookFormDateTime,
  isValidDate,
  isXHoursBefore,
} from '@opengeoweb/form-fields';

import { useDraftFormHelpers } from '@opengeoweb/shared';
import { styles } from '../ProductForm.styles';
import { getHoursBeforeValidity } from '../utils';
import sigmetConfig from '../../../utils/sigmetConfig.json';
import airmetConfig from '../../../utils/airmetConfig.json';
import { ProductType } from '../../../types';

export const VALID_FROM_TIME_DELAY_IN_MINUTES = 30;

interface ValidFromProps {
  productType: ProductType;
  isDisabled: boolean;
  onChange?: () => void;
}

const ValidFrom: React.FC<ValidFromProps> = ({
  productType,
  isDisabled,
  onChange = (): void => null,
}: ValidFromProps) => {
  const { getValues, trigger } = useFormContext();
  const { isRequired } = useDraftFormHelpers();

  const config = productType === 'sigmet' ? sigmetConfig : airmetConfig;

  return (
    <Grid item container spacing={2}>
      <Grid item xs={4} container justifyContent="flex-end">
        <Typography variant="subtitle1" sx={styles.label}>
          Valid from
        </Typography>
      </Grid>
      <Grid item xs={8} container justifyContent="flex-start">
        <ReactHookFormDateTime
          disablePast
          name="validDateStart"
          defaultNullValue={moment
            .utc()
            .add(VALID_FROM_TIME_DELAY_IN_MINUTES, 'minutes')
            .format('YYYY-MM-DDTHH:mm:ss[Z]')}
          rules={{
            validate: {
              isRequired,
              isValidDate,
              // The validity start can not be before the current time
              isValueBeforeCurrentTime: (value): boolean | string =>
                isXHoursBefore(value, moment.utc().format(), 0) ||
                'Valid from time cannot be before current time',
              // The current time can be no more than 4 hours before the validity start (12 for VA/TC SIGMET)
              isCurrentTimeXHoursBeforeValue: (value): boolean | string =>
                isXHoursBefore(
                  moment.utc().format(),
                  value,
                  getHoursBeforeValidity(
                    getValues('phenomenon'),
                    getValues('locationIndicatorATSR'),
                    config,
                  ),
                ) ||
                `Valid from time can be no more than ${getHoursBeforeValidity(
                  getValues('phenomenon'),
                  getValues('locationIndicatorATSR'),
                  config,
                )} hours after current time`,
            },
          }}
          onChange={(): void => {
            if (getValues('validDateEnd')) {
              trigger('validDateEnd');
            }
            if (getValues('observationOrForecastTime')) {
              trigger('observationOrForecastTime');
            }
            onChange();
          }}
          disabled={isDisabled}
          data-testid="valid-from"
        />
      </Grid>
      <Grid item xs={6} />
    </Grid>
  );
};

export default ValidFrom;
