/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import { Button } from '@mui/material';
import { useFormContext } from 'react-hook-form';

import SelectFIR from './SelectFIR';
import * as utils from '../utils';

describe('components/ProductForms/ProductFormFields/SelectFIR', () => {
  it('should be possible to select a FIR', async () => {
    const { container, findAllByText, getByRole } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            firName: 'ROME FIR',
          },
        }}
      >
        <SelectFIR productType="sigmet" isDisabled={false} />
      </ReactHookFormProvider>,
    );
    fireEvent.mouseDown(getByRole('button'));

    const menuItem = await findAllByText('AMSTERDAM FIR');
    fireEvent.click(menuItem[0]);

    await waitFor(() => {
      expect(
        container.querySelector('[name=locationIndicatorATSR]')
          .previousElementSibling.textContent,
      ).toEqual('AMSTERDAM FIR');
    });
  });

  it('should show the selectFIR as disabled', () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            firName: 'AMSTERDAM FIR',
          },
        }}
      >
        <SelectFIR productType="sigmet" isDisabled />
      </ReactHookFormProvider>,
    );
    const fir = container.querySelector(
      '[name=locationIndicatorATSR]',
    ).previousElementSibling;
    expect(fir.textContent).toEqual('AMSTERDAM FIR');
    expect(fir.getAttribute('class')).toContain('Mui-disabled');
  });

  it('should also update the hidden fields when changing the FIR', async () => {
    const mockConfig = {
      location_indicator_mwo: 'EHDB',
      active_firs: ['EHAA', 'EBBU'],
      firareas: {
        EHAA: {
          firname: 'AMSTERDAM FIR',
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          maxhoursofvalidity: 4,
          hoursbeforevalidity: 4,
        },
        EBBU: {
          firname: 'BRUSSEL FIR',
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          maxhoursofvalidity: 4,
          hoursbeforevalidity: 4,
        },
      },
    };

    jest
      .spyOn(utils, 'useProductSettings')
      .mockReturnValue(utils.useProductSettings(mockConfig));

    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      const [result, setResult] = React.useState(null);
      return (
        <>
          <SelectFIR productType="sigmet" isDisabled={false} />
          <Button
            onClick={(): void => {
              handleSubmit((formvalues) => setResult(formvalues))();
            }}
          >
            Validate
          </Button>
          {result && <div data-testid="result">{JSON.stringify(result)}</div>}
        </>
      );
    };

    const { container, findByText, getByText, getByTestId } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    // check the default values
    fireEvent.click(getByText('Validate'));
    await waitFor(() => {
      expect(getByTestId('result').textContent).toEqual(
        '{"locationIndicatorATSR":"EHAA","locationIndicatorATSU":"EHAA","locationIndicatorMWO":"EHDB","firName":"AMSTERDAM FIR","firGeometry":{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[5,55],[4.331914,55.332644],[3.368817,55.764314],[2.761908,54.379261],[3.15576,52.913554],[2.000002,51.500002],[3.370001,51.369722],[3.370527,51.36867],[3.362223,51.320002],[3.36389,51.313608],[3.373613,51.309999],[3.952501,51.214441],[4.397501,51.452776],[5.078611,51.391665],[5.848333,51.139444],[5.651667,50.824717],[6.011797,50.757273],[5.934168,51.036386],[6.222223,51.361666],[5.94639,51.811663],[6.405001,51.830828],[7.053095,52.237764],[7.031389,52.268885],[7.063612,52.346109],[7.065557,52.385828],[7.133055,52.888887],[7.14218,52.898244],[7.191667,53.3],[6.5,53.666667],[6.500002,55.000002],[5,55]]]},"properties":{"centlong":4.98042633,"REGION":"EUR","StateName":"Netherlands","FIRname":"AMSTERDAM FIR","StateCode":"NLD","centlat":52.8618788,"ICAOCODE":"EHAA"}}]}}',
      );
    });

    fireEvent.mouseDown(getByText('AMSTERDAM FIR'));

    const menuItem = await findByText('BRUSSEL FIR');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(
        container
          .querySelector('[name=locationIndicatorATSR]')
          .getAttribute('value'),
      ).toEqual('EBBU');
    });

    // check that all the form fields are updated
    fireEvent.click(getByText('Validate'));
    await waitFor(() => {
      expect(getByTestId('result').textContent).toEqual(
        '{"locationIndicatorATSR":"EBBU","locationIndicatorATSU":"EBBU","locationIndicatorMWO":"EHDB","firName":"BRUSSEL FIR","firGeometry":{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[5,55],[4.331914,55.332644],[3.368817,55.764314],[2.761908,54.379261],[3.15576,52.913554],[2.000002,51.500002],[3.370001,51.369722],[3.370527,51.36867],[3.362223,51.320002],[3.36389,51.313608],[3.373613,51.309999],[3.952501,51.214441],[4.397501,51.452776],[5.078611,51.391665],[5.848333,51.139444],[5.651667,50.824717],[6.011797,50.757273],[5.934168,51.036386],[6.222223,51.361666],[5.94639,51.811663],[6.405001,51.830828],[7.053095,52.237764],[7.031389,52.268885],[7.063612,52.346109],[7.065557,52.385828],[7.133055,52.888887],[7.14218,52.898244],[7.191667,53.3],[6.5,53.666667],[6.500002,55.000002],[5,55]]]},"properties":{"centlong":4.98042633,"REGION":"EUR","StateName":"Netherlands","FIRname":"AMSTERDAM FIR","StateCode":"NLD","centlat":52.8618788,"ICAOCODE":"EHAA"}}]}}',
      );
    });
  });
});
