/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';

import Type from './Type';

describe('components/ProductForms/ProductFormFields/Type', () => {
  it('should be possible to select a type', async () => {
    const { getByTestId } = render(
      <ReactHookFormProvider>
        <Type isDisabled={false} isReadOnly={false} />
      </ReactHookFormProvider>,
    );
    const fieldTest = getByTestId('type-TEST');

    await waitFor(() => {
      expect(
        getByTestId('type-NORMAL').querySelector('.Mui-checked'),
      ).toBeTruthy(); // default value
      expect(fieldTest.querySelector('.Mui-checked')).toBeFalsy();
    });

    fireEvent.click(fieldTest);
    await waitFor(() => {
      expect(fieldTest.querySelector('.Mui-checked')).toBeTruthy();
    });
  });
  it('should show type as disabled', async () => {
    const { getByTestId } = render(
      <ReactHookFormProvider>
        <Type isDisabled isReadOnly={false} />
      </ReactHookFormProvider>,
    );

    await waitFor(() => {
      expect(getByTestId('type-NORMAL').getAttribute('class')).toContain(
        'Mui-disabled',
      );
      expect(getByTestId('type-TEST').getAttribute('class')).toContain(
        'Mui-disabled',
      );
      expect(getByTestId('type-EXERCISE').getAttribute('class')).toContain(
        'Mui-disabled',
      );
    });
  });
  it('should show type as readonly', () => {
    const { queryByTestId } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            type: 'TEST',
          },
        }}
      >
        <Type isDisabled isReadOnly />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('type-TEST')).toBeTruthy();
    expect(queryByTestId('type-TEST').getAttribute('class')).toContain(
      'Mui-disabled',
    );
    expect(queryByTestId('type-NORMAL')).toBeFalsy();
    expect(queryByTestId('type-EXERCISE')).toBeFalsy();
  });
});
