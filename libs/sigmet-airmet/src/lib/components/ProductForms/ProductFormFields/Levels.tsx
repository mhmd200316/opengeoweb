/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  Grid,
  Typography,
  MenuItem,
  FormControlLabel,
  Checkbox,
} from '@mui/material';
import { useFormContext } from 'react-hook-form';
import {
  ReactHookFormRadioGroup,
  ReactHookFormSelect,
  ReactHookFormNumberField,
  isValidMax,
  isInteger,
  isValidMin,
  isEmpty,
} from '@opengeoweb/form-fields';

import { useDraftFormHelpers } from '@opengeoweb/shared';
import { styles } from '../ProductForm.styles';
import { LevelUnits, ProductConfig, ProductType } from '../../../types';
import {
  getAllowedUnits,
  getMaxLevelValue,
  getMinLevelValue,
  isLevelLower,
  useProductSettings,
} from '../utils';
import { RadioButtonAndLabel } from './RadioButtonAndLabel';
import sigmetConfig from '../../../utils/sigmetConfig.json';
import airmetConfig from '../../../utils/airmetConfig.json';

export const invalidLevelsTill4900FTStepsMessage =
  'Levels must be rounded to the nearest 100ft';
export const invalidLevelsFrom50FLStepsMessage =
  'Levels must be rounded to the nearest 5';

export const validateLevels = (
  value: string,
  unit: string,
): boolean | string => {
  if (isEmpty(value)) {
    return true;
  }
  // Parse to integer to check for steps
  const intLevel = parseInt(value, 10);
  if (LevelUnits[unit] === LevelUnits.FT) {
    return intLevel % 100 === 0 || invalidLevelsTill4900FTStepsMessage;
  }
  if (LevelUnits[unit] === LevelUnits.FL) {
    return intLevel % 5 === 0 || invalidLevelsFrom50FLStepsMessage;
  }
  // For meters the allowed step is 1, so always valid
  return true;
};

export const invalidLevelUnitCombinationMessage = 'Invalid unit combination';

export const validateLevelUnitCombinations = (
  levelUnit: string,
  lowerLevelUnit: string,
): boolean | string => {
  // If different units and lower level not SFC, only FL is allowed as upper level
  if (
    lowerLevelUnit !== 'SFC' &&
    levelUnit !== lowerLevelUnit &&
    LevelUnits[levelUnit] !== LevelUnits.FL
  ) {
    return invalidLevelUnitCombinationMessage;
  }

  return true;
};

interface LevelsProps {
  isDisabled: boolean;
  isReadOnly: boolean;
  onChange?: () => void;
  productType: ProductType;
}

const Levels: React.FC<LevelsProps> = ({
  isDisabled,
  isReadOnly,
  onChange = (): void => null,
  productType,
}: LevelsProps) => {
  const { watch, setValue, trigger, getValues, reset, clearErrors } =
    useFormContext();
  const { isRequired } = useDraftFormHelpers();

  const config = productType === 'sigmet' ? sigmetConfig : airmetConfig;
  const { allowedUnits } = useProductSettings(config as ProductConfig);

  const selectedFIR = watch('locationIndicatorATSR');

  // Get allowed level units based on selected FIR - if no FIR selected, allow all default units
  const allowedLevelUnitsForFir = getAllowedUnits(
    selectedFIR,
    allowedUnits,
    'level_unit',
    LevelUnits,
  );

  return (
    <Grid item container spacing={2} sx={styles.containerItem}>
      <Grid item xs={4} container justifyContent="flex-end">
        <Typography variant="subtitle1" sx={styles.label}>
          Levels
        </Typography>
      </Grid>
      <Grid item xs={8} container justifyContent="flex-start">
        <Grid item xs={12}>
          <ReactHookFormRadioGroup
            name="levelInfoMode"
            rules={{ validate: { isRequired } }}
            disabled={isDisabled}
            onChange={(): void => {
              // Reset default value for the level value to '' so when it rerenders it uses this empty default value
              const currentValues = getValues();
              reset(
                {
                  ...currentValues,
                  level: { unit: 'FL' as LevelUnits, value: '' },
                },
                {
                  errors: true, // errors will not be reset
                  dirtyFields: true, // dirtyFields will not be reset
                  isDirty: true, // dirty will not be reset
                  isSubmitted: false,
                  touched: false,
                  isValid: false,
                },
              );
              // Clear out errors for the levels
              clearErrors(['level', 'lowerLevel']);
            }}
          >
            {/** AT */}
            {(!isReadOnly ||
              watch('levelInfoMode') === 'AT' ||
              watch('levelInfoMode') === 'TOPS') && (
              <RadioButtonAndLabel
                value="AT"
                label="At"
                checked={
                  watch('levelInfoMode') === 'AT' ||
                  watch('levelInfoMode') === 'TOPS'
                }
                data-testid="levels-AT"
                disabled={isDisabled}
              />
            )}
            {watch('levelInfoMode') === 'AT' ||
            watch('levelInfoMode') === 'TOPS' ? (
              <Grid item xs={12} container>
                <Grid item xs={3}>
                  {(!isReadOnly || watch('levelInfoMode') === 'TOPS') && (
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={watch('levelInfoMode') === 'TOPS'}
                          onChange={(): void => {
                            setValue(
                              'levelInfoMode',
                              getValues('levelInfoMode') === 'AT'
                                ? 'TOPS'
                                : 'AT',
                              { shouldDirty: true },
                            );
                          }}
                          color="secondary"
                          disabled={isDisabled}
                        />
                      }
                      data-testid="levels-TOPS"
                      label="Tops"
                    />
                  )}
                </Grid>
                <Grid item xs={3}>
                  <ReactHookFormSelect
                    name="level.unit"
                    label="Unit"
                    rules={{ validate: { isRequired } }}
                    sx={styles.unit}
                    disabled={isDisabled}
                    defaultValue={'FL' as LevelUnits}
                    onChange={(): void => {
                      if (
                        getValues('level.value') ||
                        getValues('level.value') === 0
                      ) {
                        trigger('level.value');
                      }
                      onChange();
                    }}
                  >
                    {Object.keys(allowedLevelUnitsForFir).map((key) => (
                      <MenuItem value={key} key={key}>
                        {allowedLevelUnitsForFir[key]}
                      </MenuItem>
                    ))}
                  </ReactHookFormSelect>
                </Grid>
                <Grid item xs={6}>
                  <ReactHookFormNumberField
                    name="level.value"
                    data-testid="level.value"
                    label="Level"
                    rules={{
                      validate: {
                        isRequired,
                        isInteger,
                        max: (value): boolean | string =>
                          // The max level depends on the unit
                          isValidMax(
                            value,
                            getMaxLevelValue(
                              getValues('level.unit'),
                              productType,
                            ),
                          ) ||
                          `The maximum level in ${
                            LevelUnits[getValues('level.unit')]
                          } is ${getMaxLevelValue(
                            getValues('level.unit'),
                            productType,
                          )}`,

                        min: (value): boolean | string =>
                          // The min level depends on the unit
                          isValidMin(
                            value,
                            getMinLevelValue(getValues('level.unit')),
                          ) ||
                          `The minimum level in ${
                            LevelUnits[getValues('level.unit')]
                          } is ${getMinLevelValue(getValues('level.unit'))}`,

                        validateLevels: (value): boolean | string =>
                          // level step depends on the unit
                          validateLevels(value, getValues('level.unit')),
                      },
                    }}
                    disabled={isDisabled}
                    autoFocus
                  />
                </Grid>
              </Grid>
            ) : null}

            {/** BETWEEN */}
            {(!isReadOnly ||
              watch('levelInfoMode') === 'BETW' ||
              watch('levelInfoMode') === 'BETW_SFC') && (
              <RadioButtonAndLabel
                value="BETW"
                label="Between"
                checked={
                  watch('levelInfoMode') === 'BETW' ||
                  watch('levelInfoMode') === 'BETW_SFC'
                }
                data-testid="levels-BETW"
                disabled={isDisabled}
              />
            )}
            {watch('levelInfoMode') === 'BETW' ||
            watch('levelInfoMode') === 'BETW_SFC' ? (
              <>
                <Grid item xs={12} container>
                  <Grid item xs={3} />
                  <Grid item xs={3}>
                    <ReactHookFormSelect
                      name="level.unit"
                      label="Unit"
                      rules={{
                        validate: {
                          isRequired,
                          validateLevelUnitCombinations: (
                            value,
                          ): boolean | string =>
                            // level step depends on the unit
                            validateLevelUnitCombinations(
                              value,
                              watch('levelInfoMode') === 'BETW_SFC'
                                ? 'SFC'
                                : getValues('lowerLevel.unit'),
                            ) === true,
                        },
                      }}
                      sx={styles.unit}
                      disabled={isDisabled}
                      defaultValue={'FL' as LevelUnits}
                      onChange={(): void => {
                        trigger([
                          ...(getValues('level.value') ||
                          getValues('level.value') === 0
                            ? ['level.value']
                            : []),
                          ...(getValues('lowerLevel.value') ||
                          getValues('lowerLevel.value') === 0
                            ? ['lowerLevel.value']
                            : []),
                          'lowerLevel.unit',
                        ]);

                        onChange();
                      }}
                    >
                      {Object.keys(allowedLevelUnitsForFir).map((key) => (
                        <MenuItem value={key} key={key}>
                          {allowedLevelUnitsForFir[key]}
                        </MenuItem>
                      ))}
                    </ReactHookFormSelect>
                  </Grid>
                  <Grid item xs={6}>
                    <ReactHookFormNumberField
                      name="level.value"
                      label="Upper level"
                      sx={styles.levelField}
                      rules={{
                        validate: {
                          isRequired,
                          isInteger,
                          max: (value): boolean | string =>
                            // The max level depends on the unit
                            isValidMax(
                              value,
                              getMaxLevelValue(
                                getValues('level.unit'),
                                productType,
                              ),
                            ) ||
                            `The maximum level in ${
                              LevelUnits[getValues('level.unit')]
                            } is ${getMaxLevelValue(
                              getValues('level.unit'),
                              productType,
                            )}`,

                          min: (value): boolean | string =>
                            // The min level depends on the unit
                            isValidMin(
                              value,
                              getMinLevelValue(getValues('level.unit')),
                            ) ||
                            `The minimum level in ${
                              LevelUnits[getValues('level.unit')]
                            } is ${getMinLevelValue(getValues('level.unit'))}`,

                          validateLevels: (value): boolean | string =>
                            // level step depends on the unit
                            validateLevels(value, getValues('level.unit')),
                        },
                      }}
                      disabled={isDisabled}
                      onChange={(): Promise<boolean> => {
                        return (
                          (getValues('lowerLevel.value') ||
                            getValues('lowerLevel.value') === 0) &&
                          trigger('lowerLevel.value')
                        );
                      }}
                      autoFocus
                    />
                  </Grid>
                </Grid>
                <Grid item xs={12} container>
                  <Grid item xs={3}>
                    {(!isReadOnly || watch('levelInfoMode') === 'BETW_SFC') && (
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={watch('levelInfoMode') === 'BETW_SFC'}
                            onChange={(): void => {
                              getValues('level.unit') && trigger('level.unit');
                              setValue(
                                'levelInfoMode',
                                getValues('levelInfoMode') === 'BETW'
                                  ? 'BETW_SFC'
                                  : 'BETW',
                                { shouldDirty: true },
                              );
                            }}
                            name="checkedF"
                            color="secondary"
                            disabled={isDisabled}
                          />
                        }
                        data-testid="levels-SFC"
                        label="SFC"
                      />
                    )}
                  </Grid>
                  {watch('levelInfoMode') !== 'BETW_SFC' && (
                    <>
                      <Grid item xs={3}>
                        <ReactHookFormSelect
                          name="lowerLevel.unit"
                          label="Unit"
                          rules={{
                            validate: {
                              isRequired,
                              validateLevelUnitCombinations: (
                                value,
                              ): boolean | string =>
                                // level step depends on the unit
                                validateLevelUnitCombinations(
                                  getValues('level.unit'),
                                  value,
                                ),
                            },
                          }}
                          sx={styles.unit}
                          disabled={isDisabled}
                          defaultValue={
                            getValues('level.unit') || ('FL' as LevelUnits)
                          }
                          onChange={(): void => {
                            if (
                              getValues('lowerLevel.value') ||
                              getValues('lowerLevel.value') === 0
                            ) {
                              trigger('lowerLevel.value');
                            }
                            trigger('level.unit');
                            onChange();
                          }}
                        >
                          {Object.keys(allowedLevelUnitsForFir).map((key) => (
                            <MenuItem value={key} key={key}>
                              {allowedLevelUnitsForFir[key]}
                            </MenuItem>
                          ))}
                        </ReactHookFormSelect>
                      </Grid>
                      <Grid item xs={6}>
                        <ReactHookFormNumberField
                          name="lowerLevel.value"
                          label="Lower level"
                          rules={{
                            validate: {
                              isRequired,
                              isInteger,
                              max: (value): boolean | string =>
                                // The max level depends on the unit
                                isValidMax(
                                  value,
                                  getMaxLevelValue(
                                    getValues('lowerLevel.unit'),
                                    productType,
                                  ),
                                ) ||
                                `The maximum level in ${
                                  LevelUnits[getValues('lowerLevel.unit')]
                                } is ${getMaxLevelValue(
                                  getValues('lowerLevel.unit'),
                                  productType,
                                )}`,

                              min: (value): boolean | string =>
                                // The min level depends on the unit
                                isValidMin(
                                  value,
                                  getMinLevelValue(
                                    getValues('lowerLevel.unit'),
                                  ),
                                ) ||
                                `The minimum level in ${
                                  LevelUnits[getValues('lowerLevel.unit')]
                                } is ${getMinLevelValue(
                                  getValues('lowerLevel.unit'),
                                )}`,

                              isLevelLower: (value): boolean | string =>
                                // The lower level needs to be smaller than upper level
                                isLevelLower(
                                  value,
                                  getValues('lowerLevel.unit'),
                                  getValues('level.value'),
                                  getValues('level.unit'),
                                ),

                              validateLevels: (value): boolean | string =>
                                // level step depends on the unit
                                validateLevels(
                                  value,
                                  getValues('lowerLevel.unit'),
                                ),
                            },
                          }}
                          disabled={isDisabled}
                        />
                      </Grid>
                    </>
                  )}
                </Grid>
              </>
            ) : null}

            {/** ABOVE */}
            {(!isReadOnly ||
              watch('levelInfoMode') === 'ABV' ||
              watch('levelInfoMode') === 'TOPS_ABV') && (
              <RadioButtonAndLabel
                value="ABV"
                label="Above"
                disabled={isDisabled}
                checked={
                  watch('levelInfoMode') === 'ABV' ||
                  watch('levelInfoMode') === 'TOPS_ABV'
                }
                data-testid="levels-ABV"
              />
            )}
            {watch('levelInfoMode') === 'ABV' ||
            watch('levelInfoMode') === 'TOPS_ABV' ? (
              <Grid item xs={12} container>
                <Grid item xs={3}>
                  {(!isReadOnly || watch('levelInfoMode') === 'TOPS_ABV') && (
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={watch('levelInfoMode') === 'TOPS_ABV'}
                          onChange={(): void => {
                            setValue(
                              'levelInfoMode',
                              getValues('levelInfoMode') === 'ABV'
                                ? 'TOPS_ABV'
                                : 'ABV',
                              { shouldDirty: true },
                            );
                          }}
                          disabled={isDisabled}
                        />
                      }
                      label="Tops"
                      data-testid="levels-TOPS_ABV"
                    />
                  )}
                </Grid>
                <Grid item xs={3}>
                  <ReactHookFormSelect
                    name="level.unit"
                    label="Unit"
                    rules={{ validate: { isRequired } }}
                    sx={styles.unit}
                    disabled={isDisabled}
                    defaultValue={'FL' as LevelUnits}
                    onChange={(): void => {
                      if (
                        getValues('level.value') ||
                        getValues('level.value') === 0
                      ) {
                        trigger('level.value');
                      }
                      onChange();
                    }}
                  >
                    {Object.keys(allowedLevelUnitsForFir).map((key) => (
                      <MenuItem value={key} key={key}>
                        {allowedLevelUnitsForFir[key]}
                      </MenuItem>
                    ))}
                  </ReactHookFormSelect>
                </Grid>
                <Grid item xs={6}>
                  <ReactHookFormNumberField
                    name="level.value"
                    label="Level"
                    rules={{
                      validate: {
                        isRequired,
                        isInteger,
                        max: (value): boolean | string =>
                          // The max level depends on the unit
                          isValidMax(
                            value,
                            getMaxLevelValue(
                              getValues('level.unit'),
                              productType,
                            ),
                          ) ||
                          `The maximum level in ${
                            LevelUnits[getValues('level.unit')]
                          } is ${getMaxLevelValue(
                            getValues('level.unit'),
                            productType,
                          )}`,
                        min: (value): boolean | string =>
                          // The min level depends on the unit
                          isValidMin(
                            value,
                            getMinLevelValue(getValues('level.unit')),
                          ) ||
                          `The minimum level in ${
                            LevelUnits[getValues('level.unit')]
                          } is ${getMinLevelValue(getValues('level.unit'))}`,
                        validateLevels: (value): boolean | string =>
                          // level step depends on the unit
                          validateLevels(value, getValues('level.unit')),
                      },
                    }}
                    disabled={isDisabled}
                    autoFocus
                  />
                </Grid>
              </Grid>
            ) : null}
          </ReactHookFormRadioGroup>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Levels;
