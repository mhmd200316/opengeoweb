/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import Levels, {
  invalidLevelsFrom50FLStepsMessage,
  invalidLevelsTill4900FTStepsMessage,
  invalidLevelUnitCombinationMessage,
  validateLevels,
  validateLevelUnitCombinations,
} from './Levels';
import { LevelUnits } from '../../../types';
import { getMaxLevelValue, getMinLevelValue } from '../utils';

describe('components/ProductForms/ProductFormFields/Levels', () => {
  describe('AT section', () => {
    it('should show the correct input fields when selecting level AT and TOPS', async () => {
      const { queryByTestId, container } = render(
        <ReactHookFormProvider>
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelFieldAt = queryByTestId('levels-AT');

      const fields = [
        '[data-testid="levels-TOPS"]',
        '[name="level.unit"]',
        '[name="level.value"]',
      ];

      // AT not selected
      expect(levelFieldAt.querySelector('.Mui-checked')).toBeFalsy();
      fields.forEach((selector) =>
        expect(container.querySelector(selector)).toBeNull(),
      );

      // select AT
      fireEvent.click(levelFieldAt);

      await waitFor(() => {
        expect(levelFieldAt.querySelector('.Mui-checked')).toBeTruthy();
        fields.forEach((selector) =>
          expect(container.querySelector(selector)).toBeTruthy(),
        );
        // should autoFocus
        expect(container.querySelector(fields[2])).toEqual(
          document.activeElement,
        );
      });

      // select TOPS
      const levelTopField = queryByTestId('levels-TOPS');
      fireEvent.click(levelTopField);
      await waitFor(() =>
        expect(levelTopField.querySelector('.Mui-checked')).toBeTruthy(),
      );
    });

    it('should show the input fields as disabled with correct values', () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '95',
          unit: 'FL' as LevelUnits,
        },
      };
      const { queryByTestId, container } = render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelAt = queryByTestId('levels-AT');
      const levelUnit = container.querySelector('[name="level.unit"]');
      const levelValue = container.querySelector('[name="level.value"]');

      expect(levelAt.querySelector('.Mui-disabled')).toBeTruthy();
      expect(levelAt.querySelector('input').getAttribute('value')).toEqual(
        testValues.levelInfoMode,
      );

      expect(
        levelUnit.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelUnit.getAttribute('value')).toEqual(testValues.level.unit);

      expect(
        levelValue.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);
    });

    it('should show the input fields as readOnly with correct values', () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '95',
          unit: 'FL' as LevelUnits,
        },
      };
      const { queryByTestId, container } = render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled isReadOnly productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelAt = queryByTestId('levels-AT');
      const levelUnit = container.querySelector('[name="level.unit"]');
      const levelValue = container.querySelector('[name="level.value"]');

      expect(levelAt.querySelector('.Mui-disabled')).toBeTruthy();
      expect(levelAt.querySelector('input').getAttribute('value')).toEqual(
        testValues.levelInfoMode,
      );

      expect(
        levelUnit.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();

      expect(levelUnit.getAttribute('value')).toEqual(testValues.level.unit);

      expect(
        levelValue.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);

      // test other level fields are hidden
      expect(queryByTestId('levels-BETW')).toBeFalsy();
      expect(queryByTestId('levels-ABV')).toBeFalsy();
    });

    it('should show an error message when level exceeds max', async () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );

      const levelValueField = container.querySelector('[name="level.value"]');

      fireEvent.change(levelValueField, { target: { value: 1500 } });

      await waitFor(() => {
        expect(
          queryByText(
            `The maximum level in FL is ${getMaxLevelValue('FL', 'sigmet')}`,
          ),
        ).toBeTruthy();
        expect(
          levelValueField.parentElement.classList.contains('Mui-error'),
        ).toBeTruthy();
      });

      const levelUnitField = container.querySelector('[name="level.unit"]');
      fireEvent.change(levelUnitField, {
        target: { value: 'FT' as LevelUnits },
      });

      await waitFor(() => {
        expect(
          levelValueField.parentElement.classList.contains('Mui-error'),
        ).toBeFalsy();
      });
    });

    it('should show an error message when the level value is too low', async () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="airmet" />
        </ReactHookFormProvider>,
      );

      const levelInput = container.querySelector('[name="level.value"]');

      fireEvent.change(levelInput, { target: { value: 0 } });

      const levelHelperTextSelector = '[data-testid="level.value"]';

      await waitFor(() =>
        expect(
          container.querySelector(levelHelperTextSelector).nextElementSibling,
        ).toBeTruthy(),
      );

      expect(
        queryByText(`The minimum level in FL is ${getMinLevelValue('FL')}`),
      ).toBeTruthy();
    });

    it('should not show error message when decimal level is entered but should automatically convert to integer', async () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '',
          unit: 'FT' as LevelUnits,
        },
      };
      const { container } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );

      const levelInput = container.querySelector('[name="level.value"]');

      fireEvent.change(levelInput, { target: { value: '600.0' } });
      /* wait for the value to be converted */
      await waitFor(() =>
        expect(levelInput.getAttribute('value')).toEqual('600'),
      );

      const levelHelperTextSelector = '[data-testid="level.value"]';

      await waitFor(() =>
        expect(
          container.querySelector(levelHelperTextSelector).nextElementSibling,
        ).toBeFalsy(),
      );
    });
  });

  describe('BETW section', () => {
    it('should show the correct input fields when selecting level BETW and BETW_SFC', async () => {
      const { queryByTestId, container } = render(
        <ReactHookFormProvider>
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelFieldBew = queryByTestId('levels-BETW');
      const fields = [
        '[data-testid="levels-SFC"]',
        '[name="level.unit"]',
        '[name="level.value"]',
        '[name="lowerLevel.value"]',
        '[name="lowerLevel.unit"]',
      ];
      // BETW not selected
      expect(levelFieldBew.querySelector('.Mui-checked')).toBeFalsy();
      fields.forEach((selector) =>
        expect(container.querySelector(selector)).toBeNull(),
      );
      // select BETW
      fireEvent.click(levelFieldBew);

      await waitFor(() => {
        expect(levelFieldBew.querySelector('.Mui-checked')).toBeTruthy();
        fields.forEach((selector) =>
          expect(container.querySelector(selector)).toBeTruthy(),
        );
        // should autoFocus
        expect(container.querySelector(fields[2])).toEqual(
          document.activeElement,
        );
      });
      // select BETW_SFC
      const levelTopField = queryByTestId('levels-SFC');
      fireEvent.click(levelTopField);
      await waitFor(() =>
        expect(levelTopField.querySelector('.Mui-checked')).toBeTruthy(),
      );
    });
    it('should show the input fields as disabled with correct values', () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: '75',
          unit: 'FL' as LevelUnits,
        },
        lowerLevel: {
          value: '500',
          unit: 'FL' as LevelUnits,
        },
      };
      const { queryByTestId, container } = render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelAt = queryByTestId('levels-BETW');
      const levelUnit = container.querySelector('[name="level.unit"]');
      const levelValue = container.querySelector('[name="level.value"]');
      const lowerLevelUnit = container.querySelector(
        '[name="lowerLevel.unit"]',
      );
      const lowerLevelValue = container.querySelector(
        '[name="lowerLevel.value"]',
      );
      expect(levelAt.querySelector('.Mui-disabled')).toBeTruthy();
      expect(levelAt.querySelector('input').getAttribute('value')).toEqual(
        testValues.levelInfoMode,
      );
      expect(
        levelUnit.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelUnit.getAttribute('value')).toEqual(testValues.level.unit);
      expect(
        levelValue.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);
      expect(
        lowerLevelUnit.parentNode.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(lowerLevelUnit.getAttribute('value')).toEqual(
        testValues.lowerLevel.unit,
      );
      expect(
        lowerLevelValue.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(lowerLevelValue.getAttribute('value')).toEqual(
        testValues.lowerLevel.value,
      );
    });
    it('should show the input fields as readOnly with correct values', () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: '95',
          unit: 'FL' as LevelUnits,
        },
        lowerLevel: {
          value: '4200',
          unit: 'FT' as LevelUnits,
        },
      };
      const { queryByTestId, container } = render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled isReadOnly productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelAt = queryByTestId('levels-BETW');
      const levelUnit = container.querySelector('[name="level.unit"]');
      const levelValue = container.querySelector('[name="level.value"]');
      const lowerLevelUnit = container.querySelector(
        '[name="lowerLevel.unit"]',
      );
      const lowerLevelValue = container.querySelector(
        '[name="lowerLevel.value"]',
      );
      expect(levelAt.querySelector('.Mui-disabled')).toBeTruthy();
      expect(levelAt.querySelector('input').getAttribute('value')).toEqual(
        testValues.levelInfoMode,
      );
      expect(levelUnit.parentNode.querySelector('.Mui-disabled')).toBeTruthy();
      expect(levelUnit.getAttribute('value')).toEqual(testValues.level.unit);
      expect(
        levelValue.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);
      expect(
        lowerLevelUnit.parentNode.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(lowerLevelUnit.getAttribute('value')).toEqual(
        testValues.lowerLevel.unit,
      );
      expect(
        lowerLevelValue.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(lowerLevelValue.getAttribute('value')).toEqual(
        testValues.lowerLevel.value,
      );
      // test other level fields are hidden
      expect(queryByTestId('levels-AT')).toBeFalsy();
      expect(queryByTestId('levels-ABV')).toBeFalsy();
    });
    it('should show error when switching to a upperlevel unit with a lower max value', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 1000,
          unit: 'FT' as LevelUnits,
        },
        lowerLevel: {
          value: 150,
          unit: 'FT' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const upperLevelUnit = container.querySelector('[name="level.unit"]');
      await waitFor(() => {
        expect(
          container
            .querySelector('[name="level.value"]')
            .parentElement.classList.contains('Mui-error'),
        ).toBeFalsy();
      });
      fireEvent.change(upperLevelUnit, { target: { value: LevelUnits.FL } });
      await waitFor(() => {
        expect(
          queryByText(
            `The maximum level in FL is ${getMaxLevelValue('FL', 'sigmet')}`,
          ),
        ).toBeTruthy();
        expect(
          container
            .querySelector('[name="level.value"]')
            .parentElement.classList.contains('Mui-error'),
        ).toBeTruthy();
      });
    });
    it('should show error when lowerlevel value is set higher than upper level value', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 200,
          unit: 'FT' as LevelUnits,
        },
        lowerLevel: {
          value: 100,
          unit: 'FT' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const lowerLevelValueField = container.querySelector(
        '[name="lowerLevel.value"]',
      );
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      });
      fireEvent.change(lowerLevelValueField, { target: { value: 4900 } });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
        expect(
          queryByText('The lower level has to be below the upper level'),
        ).toBeTruthy();
      });
      fireEvent.change(lowerLevelValueField, { target: { value: 100 } });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
        expect(
          queryByText('The lower level has to be below the upper level'),
        ).toBeFalsy();
      });
    });
    it('should show error when upperlevel value is set below lowerlevel value', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 400,
          unit: 'FT' as LevelUnits,
        },
        lowerLevel: {
          value: 200,
          unit: 'FT' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const upperLevelValueField = container.querySelector(
        '[name="level.value"]',
      );
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      });
      fireEvent.change(upperLevelValueField, { target: { value: 100 } });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
        expect(
          queryByText('The lower level has to be below the upper level'),
        ).toBeTruthy();
      });
      fireEvent.change(upperLevelValueField, { target: { value: 300 } });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
        expect(
          queryByText('The lower level has to be below the upper level'),
        ).toBeFalsy();
      });
    });
    it('should show error when switching to a lowerlevel unit with a lower max value', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 100,
          unit: 'FL' as LevelUnits,
        },
        lowerLevel: {
          value: 900,
          unit: 'FT' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const lowerLevelUnit = container.querySelector(
        '[name="lowerLevel.unit"]',
      );
      await waitFor(() => {
        expect(
          container
            .querySelector('[name="lowerLevel.value"]')
            .parentElement.classList.contains('Mui-error'),
        ).toBeFalsy();
      });
      fireEvent.change(lowerLevelUnit, { target: { value: LevelUnits.FL } });
      await waitFor(() => {
        expect(
          queryByText(
            `The maximum level in FL is ${getMaxLevelValue('FL', 'sigmet')}`,
          ),
        ).toBeTruthy();
        expect(
          container
            .querySelector('[name="lowerLevel.value"]')
            .parentElement.classList.contains('Mui-error'),
        ).toBeTruthy();
      });
    });
  });

  describe('ABOVE section', () => {
    it('should show the correct input fields when selecting level ABV and TOPS_ABV', async () => {
      const { queryByTestId, container } = render(
        <ReactHookFormProvider>
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelFieldAbv = queryByTestId('levels-ABV');
      const fields = [
        '[data-testid="levels-TOPS_ABV"]',
        '[name="level.unit"]',
        '[name="level.value"]',
      ];
      // ABV not selected
      expect(levelFieldAbv.querySelector('.Mui-checked')).toBeFalsy();
      fields.forEach((selector) =>
        expect(container.querySelector(selector)).toBeNull(),
      );
      // select ABV
      fireEvent.click(levelFieldAbv);

      await waitFor(() => {
        expect(levelFieldAbv.querySelector('.Mui-checked')).toBeTruthy();
        fields.forEach((selector) =>
          expect(container.querySelector(selector)).toBeTruthy(),
        );
        // should autoFocus
        expect(container.querySelector(fields[2])).toEqual(
          document.activeElement,
        );
      });
      // select TOPS
      const levelTopField = queryByTestId('levels-TOPS_ABV');
      fireEvent.click(levelTopField);
      await waitFor(() =>
        expect(levelTopField.querySelector('.Mui-checked')).toBeTruthy(),
      );
    });
    it('should show the input fields as disabled with correct values', () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '50',
          unit: 'FL' as LevelUnits,
        },
      };
      const { queryByTestId, container } = render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelAt = queryByTestId('levels-ABV');
      const levelUnit = container.querySelector('[name="level.unit"]');
      const levelValue = container.querySelector('[name="level.value"]');
      expect(levelAt.querySelector('.Mui-disabled')).toBeTruthy();
      expect(levelAt.querySelector('input').getAttribute('value')).toEqual(
        testValues.levelInfoMode,
      );
      expect(
        levelUnit.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelUnit.getAttribute('value')).toEqual(testValues.level.unit);
      expect(
        levelValue.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);
    });
    it('should show the input fields as readOnly with correct values', () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '50',
          unit: 'FL' as LevelUnits,
        },
      };
      const { queryByTestId, container } = render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled isReadOnly productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelAt = queryByTestId('levels-ABV');
      const levelUnit = container.querySelector('[name="level.unit"]');
      const levelValue = container.querySelector('[name="level.value"]');
      expect(levelAt.querySelector('.Mui-disabled')).toBeTruthy();
      expect(levelAt.querySelector('input').getAttribute('value')).toEqual(
        testValues.levelInfoMode,
      );
      expect(
        levelUnit.parentElement.querySelector('.Mui-disabled'),
      ).toBeTruthy();
      expect(levelUnit.getAttribute('value')).toEqual(testValues.level.unit);
      expect(levelValue.parentNode.querySelector('.Mui-disabled')).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);
      // test other level fields are hidden
      expect(queryByTestId('levels-AT')).toBeFalsy();
      expect(queryByTestId('levels-BETW')).toBeFalsy();
    });
    it('should show an error message when level exceeds max (650 for sigmet FL)', async () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelValueField = container.querySelector('[name="level.value"]');
      fireEvent.change(levelValueField, { target: { value: 700 } });
      await waitFor(() => {
        expect(
          queryByText(
            `The maximum level in FL is ${getMaxLevelValue('FL', 'sigmet')}`,
          ),
        ).toBeTruthy();
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      });
      const levelUnitField = container.querySelector('[name="level.unit"]');
      fireEvent.change(levelUnitField, {
        target: { value: 'FT' as LevelUnits },
      });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      });
    });
    it('should show an error message when level exceeds max (100 for airmet FL)', async () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="airmet" />
        </ReactHookFormProvider>,
      );
      const levelValueField = container.querySelector('[name="level.value"]');
      fireEvent.change(levelValueField, { target: { value: 300 } });
      await waitFor(() => {
        expect(
          queryByText(
            `The maximum level in FL is ${getMaxLevelValue('FL', 'airmet')}`,
          ),
        ).toBeTruthy();
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      });
      const levelUnitField = container.querySelector('[name="level.unit"]');
      fireEvent.change(levelUnitField, {
        target: { value: 'FT' as LevelUnits },
      });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      });
    });
    it('should show an error message when level exceeds max (9999 for M)', async () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '',
          unit: 'M' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="airmet" />
        </ReactHookFormProvider>,
      );
      const levelValueField = container.querySelector('[name="level.value"]');
      fireEvent.change(levelValueField, { target: { value: 16789 } });
      await waitFor(() => {
        expect(
          queryByText(
            `The maximum level in m is ${getMaxLevelValue('M', 'airmet')}`,
          ),
        ).toBeTruthy();
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(1);
      });
      fireEvent.change(levelValueField, { target: { value: 655 } });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
      });
    });
    it('should show an error message when the level value is too low', async () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelInput = container.querySelector('[name="level.value"]');
      fireEvent.change(levelInput, { target: { value: 0 } });
      const levelHelperTextSelector = '[name="level.value"]';
      await waitFor(() =>
        expect(
          container.querySelector(levelHelperTextSelector).parentElement
            .parentElement.nextElementSibling,
        ).toBeTruthy(),
      );
      expect(
        queryByText(`The minimum level in FL is ${getMinLevelValue('FL')}`),
      ).toBeTruthy();
    });
    it('should show not show an error message when entering a decimal number but it directly to integer', async () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '',
          unit: 'M' as LevelUnits,
        },
      };
      const { container } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelInput = container.querySelector('[name="level.value"]');
      fireEvent.change(levelInput, { target: { value: '15.0' } });
      /* wait for the value to be converted to integer */
      await waitFor(() => {
        expect(levelInput.getAttribute('value')).toEqual('15');
      });
      const levelHelperTextSelector = '[name="level.value"]';
      await waitFor(() =>
        expect(
          container.querySelector(levelHelperTextSelector).parentElement
            .parentElement.nextElementSibling,
        ).toBeFalsy(),
      );
    });

    it('should clear out level value when switching between AT and AVOBE', async () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '50',
          unit: 'FL' as LevelUnits,
        },
      };
      const { container, queryByTestId } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const levelFieldAt = queryByTestId('levels-AT');
      const levelValue = container.querySelector('[name="level.value"]');

      // AT not selected
      expect(levelFieldAt.querySelector('.Mui-checked')).toBeFalsy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);

      // select AT - expect value to have been cleared out for level
      fireEvent.click(levelFieldAt);
      await waitFor(() =>
        expect(levelFieldAt.querySelector('.Mui-checked')).toBeTruthy(),
      );
      // Select newly rendered level field
      const levelValue2 = container.querySelector('[name="level.value"]');
      await waitFor(() =>
        expect(levelValue2.getAttribute('value')).toEqual(''),
      );
    });
    it('should show error when lowerlevel unit not allowed with upper level unit', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 200,
          unit: 'FL' as LevelUnits,
        },
        lowerLevel: {
          value: 100,
          unit: 'FL' as LevelUnits,
        },
      };
      const { container, queryByText } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );
      const lowerLevelUnitField = container.querySelector(
        '[name="lowerLevel.unit"]',
      );

      // Upper FL lower FT is allowed
      fireEvent.change(lowerLevelUnitField, { target: { value: 'FT' } });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
        expect(queryByText(invalidLevelUnitCombinationMessage)).toBeFalsy();
      });

      // Upper FL lower FL is allowed
      fireEvent.change(lowerLevelUnitField, { target: { value: 'FL' } });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
        expect(queryByText(invalidLevelUnitCombinationMessage)).toBeFalsy();
      });

      // Upper FT lower FL not allowed
      const levelUnitField = container.querySelector('[name="level.unit"]');
      fireEvent.change(levelUnitField, { target: { value: 'FT' } });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(3);
        expect(queryByText(invalidLevelUnitCombinationMessage)).toBeTruthy();
      });

      // Upper FL lower FL allowed
      fireEvent.change(levelUnitField, { target: { value: 'FL' } });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
        expect(queryByText(invalidLevelUnitCombinationMessage)).toBeFalsy();
      });
    });

    it('should show no error for unit if SFC selected', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 200,
          unit: 'FL' as LevelUnits,
        },
        lowerLevel: {
          value: 100,
          unit: 'FL' as LevelUnits,
        },
      };
      const { container, queryByText, queryByTestId } = render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels isDisabled={false} isReadOnly={false} productType="sigmet" />
        </ReactHookFormProvider>,
      );

      // Upper FT lower FL not allowed
      const levelUnitField = container.querySelector('[name="level.unit"]');
      fireEvent.change(levelUnitField, { target: { value: 'FT' } });
      await waitFor(() => {
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(3);
        expect(queryByText(invalidLevelUnitCombinationMessage)).toBeTruthy();
      });

      const levelSFCField = queryByTestId('levels-SFC');
      fireEvent.click(levelSFCField);
      await waitFor(() => {
        expect(levelSFCField.querySelector('.Mui-checked')).toBeTruthy();
        expect(container.querySelectorAll('p.Mui-error')).toHaveLength(0);
        expect(queryByText(invalidLevelUnitCombinationMessage)).toBeFalsy();
      });
    });
  });

  describe('validateLevels', () => {
    it('should validate the steps correctly for FL (steps of 5)', () => {
      expect(validateLevels('65', 'FL' as LevelUnits)).toBe(true);
      expect(validateLevels('100', 'FL' as LevelUnits)).toBe(true);
      expect(validateLevels('78', 'FL' as LevelUnits)).toBe(
        invalidLevelsFrom50FLStepsMessage,
      );
    });
    it('should validate the steps correctly for M (steps of 1)', () => {
      expect(validateLevels('60', 'M' as LevelUnits)).toBe(true);
      expect(validateLevels('1568', 'M' as LevelUnits)).toBe(true);
      expect(validateLevels('9999', 'M' as LevelUnits)).toBe(true);
    });
    it('should validate the steps correctly for FT (steps of 100))', () => {
      expect(validateLevels('200', 'FT' as LevelUnits)).toBe(true);
      expect(validateLevels('3200', 'FT' as LevelUnits)).toBe(true);
      expect(validateLevels('450', 'FT' as LevelUnits)).toBe(
        invalidLevelsTill4900FTStepsMessage,
      );
    });

    describe('validateLevelUnitCombinations', () => {
      it('should validate correctly allowed combinations of units', () => {
        expect(validateLevelUnitCombinations('FL', 'FL')).toBe(true);
        expect(validateLevelUnitCombinations('FT', 'FT')).toBe(true);
        expect(validateLevelUnitCombinations('M', 'M')).toBe(true);

        expect(validateLevelUnitCombinations('FL', 'M')).toBe(true);
        expect(validateLevelUnitCombinations('FL', 'FT')).toBe(true);

        expect(validateLevelUnitCombinations('FT', 'M')).toBe(
          invalidLevelUnitCombinationMessage,
        );
        expect(validateLevelUnitCombinations('FT', 'FL')).toBe(
          invalidLevelUnitCombinationMessage,
        );
        expect(validateLevelUnitCombinations('M', 'FT')).toBe(
          invalidLevelUnitCombinationMessage,
        );
        expect(validateLevelUnitCombinations('M', 'FL')).toBe(
          invalidLevelUnitCombinationMessage,
        );
      });
      it('should validate correctly combination with SFC', () => {
        expect(validateLevelUnitCombinations('FL', 'SFC')).toBe(true);
        expect(validateLevelUnitCombinations('FT', 'SFC')).toBe(true);
        expect(validateLevelUnitCombinations('M', 'SFC')).toBe(true);
      });
    });
  });
});
