/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import VolcanicFields from './VolcanicFields';

describe('components/ProductForms/ProductFormFields/VolcanicFields', () => {
  it('should not show any errors when entering a valid name and coordinates', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: '',
            vaSigmetVolcanoCoordinates: {
              latitude: '',
              longitude: '',
            },
          },
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const volcanoName = container.querySelector('[name=vaSigmetVolcanoName]');
    const latitude = container.querySelector(
      "[name='vaSigmetVolcanoCoordinates.latitude']",
    );
    const longitude = container.querySelector(
      "[name='vaSigmetVolcanoCoordinates.longitude']",
    );

    fireEvent.change(volcanoName, { target: { value: 'Etna' } });
    fireEvent.change(latitude, { target: { value: '5' } });
    fireEvent.change(longitude, { target: { value: '-5' } });

    await waitFor(() =>
      expect(container.querySelector('[class*=Mui-error]')).toBeFalsy(),
    );
  });

  it('should show the helpertext', () => {
    const { getAllByText } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: '',
            vaSigmetVolcanoCoordinates: {
              latitude: '',
              longitude: '',
            },
          },
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    expect(getAllByText('Optional').length).toEqual(3);
  });

  it('should show the volcanic fields as disabled', () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: 'ETNA',
            vaSigmetVolcanoCoordinates: {
              latitude: '63.62',
              longitude: '-19.61',
            },
          },
        }}
      >
        <VolcanicFields isDisabled isReadOnly={false} helperText="Optional" />
      </ReactHookFormProvider>,
    );
    const volcanoName = container.querySelector('[name=vaSigmetVolcanoName]');
    expect(volcanoName.getAttribute('value')).toEqual('ETNA');
    expect(volcanoName.getAttribute('class')).toContain('Mui-disabled');

    const latitude = container.querySelector(
      "[name='vaSigmetVolcanoCoordinates.latitude']",
    );
    expect(latitude.getAttribute('value')).toEqual('63.62');
    expect(latitude.getAttribute('class')).toContain('Mui-disabled');

    const longitude = container.querySelector(
      "[name='vaSigmetVolcanoCoordinates.longitude']",
    );
    expect(longitude.getAttribute('value')).toEqual('-19.61');
    expect(longitude.getAttribute('class')).toContain('Mui-disabled');
  });

  it('should show the volcanic fields when the fields are filled in and form is readonly', async () => {
    const { findByText } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: 'ETNA',
            vaSigmetVolcanoCoordinates: {
              latitude: '63.62',
              longitude: '-19.61',
            },
          },
        }}
      >
        <VolcanicFields isDisabled isReadOnly helperText="Optional" />
      </ReactHookFormProvider>,
    );

    expect(await findByText('Volcano name')).toBeTruthy();
    expect(await findByText('Latitude')).toBeTruthy();
    expect(await findByText('Longitude')).toBeTruthy();
  });

  it('should not show the volcanic fields when they are not filled in and form is readonly', () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: '',
            vaSigmetVolcanoCoordinates: {
              latitude: '',
              longitude: '',
            },
          },
        }}
      >
        <VolcanicFields isDisabled isReadOnly helperText="Optional" />
      </ReactHookFormProvider>,
    );

    expect(container.querySelector("[label='Volcano name']")).toBeFalsy();
    expect(container.querySelector("[label='Latitude']")).toBeFalsy();
    expect(container.querySelector("[label='Longitude']")).toBeFalsy();
  });

  it('should show the volcano name in uppercase', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: '',
            vaSigmetVolcanoCoordinates: {
              latitude: '',
              longitude: '',
            },
          },
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const volcanoName = container.querySelector('[name=vaSigmetVolcanoName]');
    // fireEvent of simulating user typing the volcano name in lowercase
    fireEvent.change(volcanoName, { target: { value: 'Etna' } });
    // expected result with volcano name in uppercase
    await waitFor(() =>
      expect(volcanoName.getAttribute('value')).toEqual('ETNA'),
    );
  });

  it('should show an error message for longitude when only latitude is filled in', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: 'ETNA',
            vaSigmetVolcanoCoordinates: {
              latitude: '',
              longitude: '',
            },
          },
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const latitudeInput = container.querySelector(
      '[name="vaSigmetVolcanoCoordinates.latitude"]',
    );

    fireEvent.change(latitudeInput, { target: { value: 5 } });

    const longitudeHelperTextSelector =
      '[data-testid="vaSigmetVolcanoCoordinates.longitude"]';

    await waitFor(() =>
      expect(
        container.querySelector(longitudeHelperTextSelector).nextElementSibling,
      ).toBeTruthy(),
    );

    expect(
      container.querySelector(longitudeHelperTextSelector).nextElementSibling
        .textContent,
    ).toEqual('Please enter both coordinates');
  });

  it('should show an error message for latitude when only longitude is filled in', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: 'ETNA',
            vaSigmetVolcanoCoordinates: {
              latitude: '',
              longitude: '',
            },
          },
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const longitudeInput = container.querySelector(
      '[name="vaSigmetVolcanoCoordinates.longitude"]',
    );

    fireEvent.change(longitudeInput, { target: { value: -5 } });

    const latitudeHelperTextSelector =
      '[data-testid="vaSigmetVolcanoCoordinates.latitude"]';

    await waitFor(() =>
      expect(
        container.querySelector(latitudeHelperTextSelector).nextElementSibling,
      ).toBeTruthy(),
    );

    expect(
      container.querySelector(latitudeHelperTextSelector).nextElementSibling
        .textContent,
    ).toEqual('Please enter both coordinates');
  });

  it('should show an error message for latitude when entering an invalid value', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const latitudeInput = container.querySelector(
      '[name="vaSigmetVolcanoCoordinates.latitude"]',
    );

    fireEvent.change(latitudeInput, { target: { value: 91 } });

    const latitudeHelperTextSelector =
      '[data-testid="vaSigmetVolcanoCoordinates.latitude"]';

    await waitFor(() =>
      expect(
        container.querySelector(latitudeHelperTextSelector).nextElementSibling,
      ).toBeTruthy(),
    );

    expect(
      container.querySelector(latitudeHelperTextSelector).nextElementSibling
        .textContent,
    ).toEqual(
      'Invalid latitude, expected number with maximum two decimal degrees between -90.00 and 90.00',
    );
  });

  it('should show an error message for longitude when entering an invalid value', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const longitudeInput = container.querySelector(
      '[name="vaSigmetVolcanoCoordinates.longitude"]',
    );

    fireEvent.change(longitudeInput, { target: { value: 181 } });

    const longitudeHelperTextSelector =
      '[data-testid="vaSigmetVolcanoCoordinates.longitude"]';

    await waitFor(() =>
      expect(
        container.querySelector(longitudeHelperTextSelector).nextElementSibling,
      ).toBeTruthy(),
    );

    expect(
      container.querySelector(longitudeHelperTextSelector).nextElementSibling
        .textContent,
    ).toEqual(
      'Invalid longitude, expected number with maximum two decimal degrees between -180.00 and 180.00',
    );
  });

  it('should be allowed to enter coordinates with decimals', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const latitudeInput = container.querySelector(
      '[name="vaSigmetVolcanoCoordinates.latitude"]',
    );
    const longitudeInput = container.querySelector(
      '[name="vaSigmetVolcanoCoordinates.longitude"]',
    );

    fireEvent.change(latitudeInput, { target: { value: 53.48 } });
    fireEvent.change(longitudeInput, { target: { value: 5.21 } });

    const latitudeHelperTextSelector =
      '[data-testid="vaSigmetVolcanoCoordinates.latitude"]';
    const longitudeHelperTextSelector =
      '[data-testid="vaSigmetVolcanoCoordinates.longitude"]';

    await waitFor(() => {
      expect(
        container.querySelector(longitudeHelperTextSelector).nextElementSibling,
      ).toBeFalsy();
      expect(
        container.querySelector(latitudeHelperTextSelector).nextElementSibling,
      ).toBeFalsy();
    });
  });

  it('should be not allowed to enter coordinates with a comma as decimal separator', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const latitudeInput = container.querySelector(
      '[name="vaSigmetVolcanoCoordinates.latitude"]',
    );
    const longitudeInput = container.querySelector(
      '[name="vaSigmetVolcanoCoordinates.longitude"]',
    );

    fireEvent.change(latitudeInput, { target: { value: '53,48' } });

    const latitudeHelperTextSelector =
      '[data-testid="vaSigmetVolcanoCoordinates.latitude"]';

    await waitFor(() =>
      expect(
        container.querySelector(latitudeHelperTextSelector).nextElementSibling,
      ).toBeTruthy(),
    );

    expect(
      container.querySelector(latitudeHelperTextSelector).nextElementSibling
        .textContent,
    ).toEqual('Please use a dot (.) as decimal separator');

    fireEvent.change(longitudeInput, { target: { value: '5,21' } });

    const longitudeHelperTextSelector =
      '[data-testid="vaSigmetVolcanoCoordinates.longitude"]';

    await waitFor(() =>
      expect(
        container.querySelector(longitudeHelperTextSelector).nextElementSibling
          .textContent,
      ).toEqual('Please use a dot (.) as decimal separator'),
    );
  });
});
