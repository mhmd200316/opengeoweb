/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
  errorMessages,
} from '@opengeoweb/form-fields';
import SurfaceVisibility, {
  invalidSurfaceVisibilityFrom750StepsMessage,
  invalidSurfaceVisibilityTill750StepsMessage,
  validateSurfaceVisibility,
} from './SurfaceVisibility';

describe('components/ProductForms/ProductFormFields/SurfaceVisibilty', () => {
  it('should not show any errors when entering valid visibility values', async () => {
    const onChangeSpy = jest.fn();
    const { container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            visibilityValue: '',
            visibilityUnit: 'm',
            visibilityCause: '',
          },
        }}
      >
        <SurfaceVisibility isDisabled={false} onChange={onChangeSpy} />
      </ReactHookFormProvider>,
    );

    const visibilityValue = container.querySelector('[name=visibilityValue]');
    const visibilityCause = container.querySelector("[name='visibilityCause']");

    fireEvent.change(visibilityValue, { target: { value: 2000 } });
    fireEvent.change(visibilityCause, { target: { value: 'RA' } });
    expect(onChangeSpy).toHaveBeenCalled();
    await waitFor(() =>
      expect(container.querySelector('[class*=Mui-error]')).toBeFalsy(),
    );
  });
  it('should show an error message when invalid visibility value is entered', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            visibilityValue: '',
            visibilityUnit: 'm',
            visibilityCause: '',
          },
        }}
      >
        <SurfaceVisibility isDisabled={false} onChange={jest.fn()} />
      </ReactHookFormProvider>,
    );

    const visibilityValue = container.querySelector('[name="visibilityValue"]');
    fireEvent.change(visibilityValue, { target: { value: '-1' } });

    const visibilityHelperTextSelector =
      '[data-testid="surfaceVisibility-visibilityValue"]';
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector).nextElementSibling
          .textContent,
      ).toEqual(errorMessages.minVisibility),
    );

    fireEvent.change(visibilityValue, { target: { value: '1000' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector)
          .nextElementSibling,
      ).toBeFalsy(),
    );

    fireEvent.change(visibilityValue, { target: { value: '5000' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector).nextElementSibling
          .textContent,
      ).toEqual(errorMessages.maxVisibility),
    );
    // add valid visibility
    fireEvent.change(visibilityValue, { target: { value: '0100' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector)
          .nextElementSibling,
      ).toBeFalsy(),
    );
    // add valid faulty visibility
    fireEvent.change(visibilityValue, { target: { value: '950' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector).nextElementSibling
          .textContent,
      ).toEqual(invalidSurfaceVisibilityFrom750StepsMessage),
    );

    // add valid visibility
    fireEvent.change(visibilityValue, { target: { value: '0100' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector)
          .nextElementSibling,
      ).toBeFalsy(),
    );
    // add valid faulty visibility
    fireEvent.change(visibilityValue, { target: { value: '0220' } });
    await waitFor(() =>
      expect(
        container.querySelector(visibilityHelperTextSelector).nextElementSibling
          .textContent,
      ).toEqual(invalidSurfaceVisibilityTill750StepsMessage),
    );
  });

  describe('validateSurfaceVisibility', () => {
    it('should validate passed visibility correctly', () => {
      expect(validateSurfaceVisibility('0000')).toBe(true);
      expect(validateSurfaceVisibility('0')).toBe(true);
      expect(validateSurfaceVisibility('50')).toBe(true);
      expect(validateSurfaceVisibility('0700')).toBe(true);
      expect(validateSurfaceVisibility('600')).toBe(true);
      expect(validateSurfaceVisibility('750')).toBe(true);
      expect(validateSurfaceVisibility('9900')).toBe(true);
      expect(validateSurfaceVisibility('1000')).toBe(true);
      expect(validateSurfaceVisibility('10')).toBe(
        invalidSurfaceVisibilityTill750StepsMessage,
      );
      expect(validateSurfaceVisibility('010')).toBe(
        invalidSurfaceVisibilityTill750StepsMessage,
      );
      expect(validateSurfaceVisibility('0010')).toBe(
        invalidSurfaceVisibilityTill750StepsMessage,
      );
      expect(validateSurfaceVisibility('950')).toBe(
        invalidSurfaceVisibilityFrom750StepsMessage,
      );
      expect(validateSurfaceVisibility('1450')).toBe(
        invalidSurfaceVisibilityFrom750StepsMessage,
      );
      expect(validateSurfaceVisibility('5079')).toBe(
        invalidSurfaceVisibilityFrom750StepsMessage,
      );
      expect(validateSurfaceVisibility('')).toBeTruthy();
    });
  });
});
