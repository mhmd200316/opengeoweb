/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { Grid, Typography } from '@mui/material';
import moment from 'moment';
import {
  ReactHookFormDateTime,
  isBefore,
  isBetween,
  isValidDate,
} from '@opengeoweb/form-fields';

import { styles } from '../ProductForm.styles';

export const ForecastTimeValidation =
  'Forecast time should lie between the valid start and valid end time';

interface ObservationForecastTimeProps {
  isDisabled: boolean;
  isReadOnly: boolean;
  helperText: string;
  onChange?: () => void;
}

const ObservationForecastTime: React.FC<ObservationForecastTimeProps> = ({
  isDisabled,
  isReadOnly,
  helperText,
  onChange = (): void => null,
}: ObservationForecastTimeProps) => {
  const { watch } = useFormContext();

  return (
    (!isReadOnly || watch('observationOrForecastTime') !== null) && (
      <Grid item container spacing={2}>
        <Grid item xs={4} container justifyContent="flex-end">
          <Typography variant="subtitle1" sx={styles.label}>
            At
          </Typography>
        </Grid>
        <Grid item xs={8} container justifyContent="flex-start">
          <ReactHookFormDateTime
            disablePast={watch('isObservationOrForecast') === 'FCST'}
            disableFuture={watch('isObservationOrForecast') === 'OBS'}
            name="observationOrForecastTime"
            rules={{
              validate: {
                isValidDate,

                isBefore: (val): boolean | string =>
                  watch('isObservationOrForecast') === 'OBS' && val
                    ? isBefore(val, moment.utc().format()) ||
                      'Observation time cannot be in the future'
                    : true,

                isBetween: (val): boolean | string =>
                  watch('isObservationOrForecast') === 'FCST' && val
                    ? isBetween(
                        val,
                        watch('validDateStart'),
                        watch('validDateEnd'),
                      ) || ForecastTimeValidation
                    : true,
              },
            }}
            disabled={isDisabled}
            helperText={helperText}
            onChange={onChange}
          />
        </Grid>
      </Grid>
    )
  );
};

export default ObservationForecastTime;
