/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, Typography } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormRadioGroup } from '@opengeoweb/form-fields';

import { useDraftFormHelpers } from '@opengeoweb/shared';
import { styles } from '../ProductForm.styles';
import { RadioButtonAndLabel } from './RadioButtonAndLabel';

interface TypeProps {
  isReadOnly: boolean;
  isDisabled: boolean;
}

const Type: React.FC<TypeProps> = ({ isReadOnly, isDisabled }: TypeProps) => {
  const { watch } = useFormContext();
  const { isRequired } = useDraftFormHelpers();

  return (
    <Grid item container spacing={2} sx={styles.containerItem}>
      <Grid item xs={4} container justifyContent="flex-end">
        <Typography variant="subtitle1" sx={styles.label}>
          Type
        </Typography>
      </Grid>
      <Grid item xs={8} container justifyContent="flex-start">
        <Grid item xs={12}>
          <ReactHookFormRadioGroup
            name="type"
            rules={{ validate: { isRequired } }}
            disabled={isDisabled}
            defaultValue="NORMAL"
          >
            {(!isReadOnly || watch('type') === 'NORMAL') && (
              <RadioButtonAndLabel
                value="NORMAL"
                label="Normal"
                disabled={isDisabled}
                data-testid="type-NORMAL"
              />
            )}
            {(!isReadOnly || watch('type') === 'TEST') && (
              <RadioButtonAndLabel
                value="TEST"
                label="Test"
                disabled={isDisabled}
                data-testid="type-TEST"
              />
            )}
            {(!isReadOnly || watch('type') === 'EXERCISE') && (
              <RadioButtonAndLabel
                value="EXERCISE"
                label="Exercise"
                disabled={isDisabled}
                data-testid="type-EXERCISE"
              />
            )}
          </ReactHookFormRadioGroup>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Type;
