/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { Grid, Typography } from '@mui/material';
import { ReactHookFormRadioGroup } from '@opengeoweb/form-fields';
import { useDraftFormHelpers } from '@opengeoweb/shared';
import { styles } from '../ProductForm.styles';
import { RadioButtonAndLabel } from './RadioButtonAndLabel';

interface ChangeProps {
  isDisabled: boolean;
  isReadOnly: boolean;
}

const Change: React.FC<ChangeProps> = ({
  isDisabled,
  isReadOnly,
}: ChangeProps) => {
  const { watch } = useFormContext();
  const { isRequired } = useDraftFormHelpers();

  return (
    <Grid item container spacing={2} sx={styles.containerItem}>
      <Grid item xs={4} container justifyContent="flex-end">
        <Typography variant="subtitle1" sx={styles.label}>
          Change
        </Typography>
      </Grid>
      <Grid item xs={8} container justifyContent="flex-start">
        <Grid item xs={12}>
          <ReactHookFormRadioGroup
            name="change"
            rules={{ validate: { isRequired } }}
            disabled={isDisabled}
          >
            {(!isReadOnly || watch('change') === 'WKN') && (
              <RadioButtonAndLabel
                value="WKN"
                label="Weakening"
                disabled={isDisabled}
                data-testid="change-WKN"
              />
            )}
            {(!isReadOnly || watch('change') === 'NC') && (
              <RadioButtonAndLabel
                value="NC"
                label="No change"
                disabled={isDisabled}
                data-testid="change-NC"
              />
            )}
            {(!isReadOnly || watch('change') === 'INTSF') && (
              <RadioButtonAndLabel
                value="INTSF"
                label="Intensifying"
                disabled={isDisabled}
                data-testid="change-INTSF"
              />
            )}
          </ReactHookFormRadioGroup>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Change;
