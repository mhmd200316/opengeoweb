/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, MenuItem } from '@mui/material';
import {
  isEmpty,
  isInteger,
  isValidMax,
  isValidMin,
  ReactHookFormSelect,
  ReactHookFormNumberField,
} from '@opengeoweb/form-fields';
import { useFormContext } from 'react-hook-form';
import { useDraftFormHelpers } from '@opengeoweb/shared';
import { ProductConfig, WindUnit } from '../../../types';
import { styles } from '../ProductForm.styles';
import {
  getAllowedUnits,
  getMaxWindSpeedValue,
  getMinWindSpeedValue,
  useProductSettings,
} from '../utils';
import airmetConfig from '../../../utils/airmetConfig.json';

export const invalidSurfaceWindDirectionStepsMessage =
  'Direction must be rounded to the nearest 10 deg.';

export const validateSurfaceWindDirection = (
  value: string,
): boolean | string => {
  if (isEmpty(value)) return true;
  // Parse to integer to check for steps
  const intVisibility = parseInt(value, 10);
  return intVisibility % 10 === 0 || invalidSurfaceWindDirectionStepsMessage;
};

interface SurfaceWindProps {
  isDisabled: boolean;
  onChange: () => void;
}

const SurfaceWind: React.FC<SurfaceWindProps> = ({
  isDisabled,
  onChange = (): void => null,
}: SurfaceWindProps) => {
  const { trigger, getValues, watch } = useFormContext();
  const { isRequired } = useDraftFormHelpers();

  const { allowedUnits } = useProductSettings(airmetConfig as ProductConfig);
  const selectedFIR = watch('locationIndicatorATSR');
  // Get allowed speed units based on selected FIR - if no FIR selected, allow all units
  const allowedWindUnitsForFir = getAllowedUnits(
    selectedFIR,
    allowedUnits,
    'surfacewind_unit',
    WindUnit,
  );

  return (
    <Grid item container spacing={2}>
      <Grid item xs={4} container justifyContent="flex-end" />
      <Grid item xs={8} container justifyContent="flex-start">
        <Grid item xs={12} sx={styles.surfaceWind}>
          <ReactHookFormNumberField
            name="windDirection"
            label="Set wind direction"
            helperText="Value in degrees"
            rules={{
              min: {
                value: 10,
                message: 'Direction should be between 10 and 360 deg.',
              },
              max: {
                value: 360,
                message: 'Direction should be between 10 and 360 deg.',
              },
              validate: {
                isRequired,
                validateSurfaceWindDirection,
              },
            }}
            disabled={isDisabled}
            data-testid="surfaceWind-windDirection"
          />
        </Grid>
        <Grid item xs={12} container>
          <Grid item xs={5}>
            <ReactHookFormSelect
              name="windUnit"
              label="Unit"
              rules={{ validate: { isRequired } }}
              sx={styles.unit}
              disabled={isDisabled}
              defaultValue={'KT' as WindUnit}
              data-testid="surfaceWind-windUnit"
              onChange={(): void => {
                if (getValues('windSpeed') || getValues('windSpeed') === 0) {
                  trigger('windSpeed');
                }
                onChange();
              }}
            >
              {Object.keys(allowedWindUnitsForFir).map((key) => (
                <MenuItem value={key} key={key}>
                  {allowedWindUnitsForFir[key]}
                </MenuItem>
              ))}
            </ReactHookFormSelect>
          </Grid>
          <Grid item xs={7}>
            <ReactHookFormNumberField
              name="windSpeed"
              label="Set speed"
              rules={{
                validate: {
                  isRequired,
                  isInteger,
                  min: (value): boolean | string =>
                    // The min level depends on the unit
                    isValidMin(
                      value,
                      getMinWindSpeedValue(getValues('windUnit')),
                    ) ||
                    `The minimum wind speed in ${
                      WindUnit[getValues('windUnit')]
                    } is ${getMinWindSpeedValue(getValues('windUnit'))}`,
                  max: (value): boolean | string =>
                    // The max level depends on the unit
                    isValidMax(
                      value,
                      getMaxWindSpeedValue(getValues('windUnit')),
                    ) ||
                    `The maximum wind speed in ${
                      WindUnit[getValues('windUnit')]
                    } is ${getMaxWindSpeedValue(getValues('windUnit'))}`,
                },
              }}
              disabled={isDisabled}
              data-testid="surfaceWind-windSpeed"
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default SurfaceWind;
