/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import moment from 'moment';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import ValidFrom, { VALID_FROM_TIME_DELAY_IN_MINUTES } from './ValidFrom';

describe('components/ProductForms/ProductFormFields/ValidFrom', () => {
  it('should show current time +30mins if no date passed in', async () => {
    const timeToShow = moment
      .utc()
      .add(VALID_FROM_TIME_DELAY_IN_MINUTES, 'minutes');
    const { container } = render(
      <ReactHookFormProvider>
        <ValidFrom productType="sigmet" isDisabled={false} />
      </ReactHookFormProvider>,
    );

    const field = container.querySelector('[name="validDateStart"]');
    await waitFor(() => {
      expect(field.getAttribute('value')).toEqual(
        timeToShow.format('YYYY/MM/DD HH:mm'),
      );
    });
  });

  it('should show passed in date', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2020-11-17T13:03Z',
          },
        }}
      >
        <ValidFrom productType="sigmet" isDisabled={false} />
      </ReactHookFormProvider>,
    );

    const field = container.querySelector('[name="validDateStart"]');

    await waitFor(() => {
      expect(field.getAttribute('value')).toEqual('2020/11/17 13:03');
    });
  });

  it('should be possible to set a date', async () => {
    const time = moment.utc().add(VALID_FROM_TIME_DELAY_IN_MINUTES, 'minutes');
    const { container } = render(
      <ReactHookFormProvider>
        <ValidFrom productType="sigmet" isDisabled={false} />
      </ReactHookFormProvider>,
    );

    const field = container.querySelector('[name="validDateStart"]');

    await waitFor(() => {
      expect(field.getAttribute('value')).toEqual(
        time.format('YYYY/MM/DD HH:mm'),
      );
    });

    fireEvent.change(field, { target: { value: '2020-11-17T13:03+00:00' } });

    await waitFor(() => {
      expect(field.getAttribute('value')).toEqual('2020/11/17 13:03');
    });
  });

  it('should show correct error', async () => {
    const { container, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2020-11-17T13:03Z',
          },
        }}
      >
        <ValidFrom productType="sigmet" isDisabled={false} />
      </ReactHookFormProvider>,
    );

    const field = container.querySelector('[name="validDateStart"]');

    await waitFor(() => {
      expect(field.getAttribute('value')).toEqual('2020/11/17 13:03');
    });

    // invalid date
    fireEvent.change(field, { target: { value: 1 } });

    await waitFor(() => {
      expect(queryByText('Not a valid date')).toBeTruthy();
    });

    // in past
    fireEvent.change(field, { target: { value: '2000-11-17T13:03+00:00' } });

    await waitFor(() => {
      expect(
        queryByText('Valid from time cannot be before current time'),
      ).toBeTruthy();
    });

    // before validity start
    fireEvent.change(field, { target: { value: '2100-11-17T13:03+00:00' } });
    await waitFor(() => {
      expect(
        queryByText(
          'Valid from time can be no more than 4 hours after current time',
        ),
      ).toBeTruthy();
    });
  });

  it('should show the correct disabled input', async () => {
    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            validDateStart: '2000-11-17T13:03+00:00',
          },
        }}
      >
        <ValidFrom productType="sigmet" isDisabled />
      </ReactHookFormProvider>,
    );

    const input = container.querySelector('[name="validDateStart"]');

    await waitFor(() => {
      expect(input).toBeTruthy();
      expect(input.getAttribute('disabled')).toBeDefined();
    });
  });
});
