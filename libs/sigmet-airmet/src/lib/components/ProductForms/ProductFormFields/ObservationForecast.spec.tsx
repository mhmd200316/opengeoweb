/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import ObservationForecast from './ObservationForecast';

describe('components/ProductForms/ProductFormFields/ObservationForecast', () => {
  it('should be possible to select a observation or forecast', async () => {
    const { getByTestId } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            isObservationOrForecast: 'FCST',
          },
        }}
      >
        <ObservationForecast isDisabled={false} isReadOnly={false} />
      </ReactHookFormProvider>,
    );

    const obsField = getByTestId('isObservationOrForecast-OBS');
    const fcstField = getByTestId('isObservationOrForecast-FCST');

    await waitFor(() => {
      expect(obsField.querySelector('.Mui-checked')).toBeFalsy();
      expect(fcstField.querySelector('.Mui-checked')).toBeTruthy();
    });

    fireEvent.click(obsField);

    await waitFor(() => {
      expect(obsField.querySelector('.Mui-checked')).toBeTruthy();
      expect(fcstField.querySelector('.Mui-checked')).toBeFalsy();
    });
  });

  it('should show the observation or forecast as disabled', () => {
    const { getByTestId } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            isObservationOrForecast: 'FCST',
          },
        }}
      >
        <ObservationForecast isDisabled isReadOnly={false} />
      </ReactHookFormProvider>,
    );
    const obsField = getByTestId('isObservationOrForecast-OBS');
    const fcstField = getByTestId('isObservationOrForecast-FCST');

    expect(obsField.getAttribute('class')).toContain('Mui-disabled');
    expect(fcstField.getAttribute('class')).toContain('Mui-disabled');
  });

  it('should show the observation or forecast as readonly', () => {
    const { queryByTestId } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            isObservationOrForecast: 'FCST',
          },
        }}
      >
        <ObservationForecast isDisabled isReadOnly />
      </ReactHookFormProvider>,
    );
    const obsField = queryByTestId('isObservationOrForecast-OBS');
    const fcstField = queryByTestId('isObservationOrForecast-FCST');

    expect(obsField).toBeNull();
    expect(fcstField.getAttribute('class')).toContain('Mui-disabled');
  });
});
