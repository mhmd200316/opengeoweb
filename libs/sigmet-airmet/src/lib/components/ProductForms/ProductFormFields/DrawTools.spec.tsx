/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { findByText, fireEvent, render } from '@testing-library/react';

import DrawTools from './DrawTools';
import { MapDrawMode, StartOrEndDrawing } from '../../../types';

describe('components/ProductForms/ProductFormFields/DrawTools', () => {
  it('expect the selected drawMode to be highlighted', () => {
    const props = {
      type: StartOrEndDrawing.start,
      drawMode: MapDrawMode.BOX,
      setDrawMode: jest.fn(),
    };

    const { getByTestId } = render(<DrawTools {...props} />);

    expect(getByTestId('drawtools-box').getAttribute('class')).toContain(
      'Mui-checked',
    );
    expect(getByTestId('drawtools-point').getAttribute('class')).not.toContain(
      'Mui-checked',
    );
    expect(
      getByTestId('drawtools-polygon').getAttribute('class'),
    ).not.toContain('Mui-checked');
    expect(getByTestId('drawtools-fir').getAttribute('class')).not.toContain(
      'Mui-checked',
    );
  });

  it('should show the correct tooltips using the type passed in: start', async () => {
    const props = {
      type: StartOrEndDrawing.start,
      drawMode: MapDrawMode.BOX,
      setDrawMode: jest.fn(),
    };

    const { getByTestId, container } = render(<DrawTools {...props} />);

    fireEvent.mouseOver(getByTestId('drawtools-point'));
    // Wait until tooltip appears
    const tooltippoint = await findByText(
      container.parentElement,
      'Drop a point as start position',
    );
    expect(tooltippoint.textContent).toBeTruthy();

    fireEvent.mouseOver(getByTestId('drawtools-box'));
    // Wait until tooltip appears
    const tooltipbox = await findByText(
      container.parentElement,
      'Draw a box as start position',
    );
    expect(tooltipbox.textContent).toBeTruthy();

    fireEvent.mouseOver(getByTestId('drawtools-polygon'));
    // Wait until tooltip appears
    const tooltippolygon = await findByText(
      container.parentElement,
      'Draw a shape as start position',
    );
    expect(tooltippolygon.textContent).toBeTruthy();

    fireEvent.mouseOver(getByTestId('drawtools-fir'));
    // Wait until tooltip appears
    const tooltipfir = await findByText(
      container.parentElement,
      'Set FIR as start position',
    );
    expect(tooltipfir.textContent).toBeTruthy();

    fireEvent.mouseOver(getByTestId('drawtools-delete'));
    // Wait until tooltip appears
    const tooltipdelete = await findByText(
      container.parentElement,
      'Remove geometry',
    );
    expect(tooltipdelete.textContent).toBeTruthy();
  });

  it('should show the correct tooltips using the type passed in: end', async () => {
    const props = {
      type: StartOrEndDrawing.end,
      drawMode: MapDrawMode.BOX,
      setDrawMode: jest.fn(),
    };

    const { getByTestId, container } = render(<DrawTools {...props} />);

    fireEvent.mouseOver(getByTestId('drawtools-point'));
    // Wait until tooltip appears
    const tooltippoint = await findByText(
      container.parentElement,
      'Drop a point as end position',
    );
    expect(tooltippoint.textContent).toBeTruthy();

    fireEvent.mouseOver(getByTestId('drawtools-box'));
    // Wait until tooltip appears
    const tooltipbox = await findByText(
      container.parentElement,
      'Draw a box as end position',
    );
    expect(tooltipbox.textContent).toBeTruthy();

    fireEvent.mouseOver(getByTestId('drawtools-polygon'));
    // Wait until tooltip appears
    const tooltippolygon = await findByText(
      container.parentElement,
      'Draw a shape as end position',
    );
    expect(tooltippolygon.textContent).toBeTruthy();

    fireEvent.mouseOver(getByTestId('drawtools-fir'));
    // Wait until tooltip appears
    const tooltipfir = await findByText(
      container.parentElement,
      'Set FIR as end position',
    );
    expect(tooltipfir.textContent).toBeTruthy();

    fireEvent.mouseOver(getByTestId('drawtools-delete'));
    // Wait until tooltip appears
    const tooltipdelete = await findByText(
      container.parentElement,
      'Remove geometry',
    );
    expect(tooltipdelete.textContent).toBeTruthy();
  });

  it('expect when clicking one of the buttons - setDrawMode is called with the right drawMode as parameter', async () => {
    const props = {
      type: StartOrEndDrawing.start,
      drawMode: null,
      setDrawMode: jest.fn(),
    };

    const { getByTestId } = render(<DrawTools {...props} />);

    const deleteButton = getByTestId('drawtools').querySelector(
      '[data-testid="drawtools-delete"] input',
    );
    fireEvent.click(deleteButton);
    expect(props.setDrawMode).toHaveBeenLastCalledWith(MapDrawMode.DELETE);

    const pointButton = getByTestId('drawtools').querySelector(
      '[data-testid="drawtools-point"] input',
    );
    fireEvent.click(pointButton);
    expect(props.setDrawMode).toHaveBeenLastCalledWith(MapDrawMode.POINT);

    const polygonButton = getByTestId('drawtools').querySelector(
      '[data-testid="drawtools-polygon"] input',
    );
    fireEvent.click(polygonButton);
    expect(props.setDrawMode).toHaveBeenLastCalledWith(MapDrawMode.POLYGON);

    const firButton = getByTestId('drawtools').querySelector(
      '[data-testid="drawtools-fir"] input',
    );
    fireEvent.click(firButton);
    expect(props.setDrawMode).toHaveBeenLastCalledWith(MapDrawMode.FIR);

    const boxButton = getByTestId('drawtools').querySelector(
      '[data-testid="drawtools-box"] input',
    );
    fireEvent.click(boxButton);
    expect(props.setDrawMode).toHaveBeenLastCalledWith(MapDrawMode.BOX);
  });
});
