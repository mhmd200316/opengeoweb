/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import {
  ReactHookFormTextField,
  ReactHookFormNumberField,
  isLatitude,
  isLongitude,
  isNonOrBothCoordinates,
  containsNoCommas,
} from '@opengeoweb/form-fields';
import { styles } from '../ProductForm.styles';

interface VolcanicFieldsProps {
  isDisabled: boolean;
  isReadOnly: boolean;
  helperText: string;
}

const VolcanicFields: React.FC<VolcanicFieldsProps> = ({
  isDisabled,
  isReadOnly,
  helperText,
}: VolcanicFieldsProps) => {
  const { watch, trigger, getValues } = useFormContext();

  return (
    <Grid item container spacing={2}>
      <Grid item xs={4} container justifyContent="flex-end" />
      <Grid item xs={8} container justifyContent="flex-start">
        {(!isReadOnly || !!watch('vaSigmetVolcanoName')) && (
          <Grid item xs={12} sx={styles.volcanicField}>
            <ReactHookFormTextField
              name="vaSigmetVolcanoName"
              label="Volcano name"
              rules={{ required: false }}
              disabled={isDisabled}
              helperText={helperText}
              upperCase={true}
            />
          </Grid>
        )}
        {(!isReadOnly || !!watch('vaSigmetVolcanoCoordinates.latitude')) && (
          <Grid item xs={6}>
            <ReactHookFormNumberField
              name="vaSigmetVolcanoCoordinates.latitude"
              data-testid="vaSigmetVolcanoCoordinates.latitude"
              label="Latitude"
              inputMode="decimal"
              rules={{
                required: false,
                validate: {
                  containsNoCommas,
                  isLatitude,
                  isNonOrBothCoordinates: (value): boolean | string =>
                    isNonOrBothCoordinates(
                      value,
                      getValues('vaSigmetVolcanoCoordinates.longitude'),
                    ),
                },
              }}
              sx={styles.latitude}
              disabled={isDisabled}
              helperText={helperText}
              onChange={(): Promise<boolean> =>
                trigger('vaSigmetVolcanoCoordinates.longitude')
              }
            />
          </Grid>
        )}
        {(!isReadOnly || !!watch('vaSigmetVolcanoCoordinates.longitude')) && (
          <Grid item xs={6}>
            <ReactHookFormNumberField
              name="vaSigmetVolcanoCoordinates.longitude"
              data-testid="vaSigmetVolcanoCoordinates.longitude"
              label="Longitude"
              inputMode="decimal"
              rules={{
                required: false,
                validate: {
                  containsNoCommas,
                  isLongitude,
                  isNonOrBothCoordinates: (value): boolean | string =>
                    isNonOrBothCoordinates(
                      value,
                      getValues('vaSigmetVolcanoCoordinates.latitude'),
                    ),
                },
              }}
              disabled={isDisabled}
              helperText={helperText}
              onChange={(): Promise<boolean> =>
                trigger('vaSigmetVolcanoCoordinates.latitude')
              }
            />
          </Grid>
        )}
      </Grid>
    </Grid>
  );
};

export default VolcanicFields;
