/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, Typography, MenuItem } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import {
  ReactHookFormSelect,
  ReactHookFormHiddenInput,
} from '@opengeoweb/form-fields';

import { useDraftFormHelpers } from '@opengeoweb/shared';
import { styles } from '../ProductForm.styles';
import sigmetConfig from '../../../utils/sigmetConfig.json';
import airmetConfig from '../../../utils/airmetConfig.json';
import { useProductSettings } from '../utils';
import { ProductType, ProductConfig } from '../../../types';
import { getFir } from '../../../utils/getFir';

interface SelectFIRProps {
  productType: ProductType;
  isDisabled: boolean;
}

const SelectFIR: React.FC<SelectFIRProps> = ({
  productType,
  isDisabled,
}: SelectFIRProps) => {
  const { setValue } = useFormContext();
  const { isRequired } = useDraftFormHelpers();

  const config = productType === 'sigmet' ? sigmetConfig : airmetConfig;
  const { defaultATSR, optionsFIR, allFIRS } = useProductSettings(
    config as ProductConfig,
  );
  return (
    <Grid item container spacing={2} sx={styles.containerItem}>
      <Grid item xs={4} container justifyContent="flex-end">
        <Typography variant="subtitle1" sx={styles.label}>
          Where
        </Typography>
      </Grid>
      <Grid item xs={8} container justifyContent="flex-start">
        <ReactHookFormSelect
          name="locationIndicatorATSR"
          label="Select FIR"
          rules={{ validate: { isRequired } }}
          disabled={isDisabled}
          defaultValue={defaultATSR}
          onChange={(
            changeEvent: React.ChangeEvent<HTMLInputElement>,
          ): void => {
            setValue(
              'locationIndicatorATSU',
              allFIRS[changeEvent.target.value].location_indicator_atsu,
            );
            setValue('firName', allFIRS[changeEvent.target.value].firname);
          }}
        >
          {optionsFIR.map((fir) => (
            <MenuItem key={`${fir.ATSR}-${fir.FIR}`} value={fir.ATSR}>
              {fir.FIR}
            </MenuItem>
          ))}
        </ReactHookFormSelect>
        <ReactHookFormHiddenInput
          name="locationIndicatorATSU"
          defaultValue={allFIRS[defaultATSR].location_indicator_atsu}
        />
        <ReactHookFormHiddenInput
          name="locationIndicatorMWO"
          defaultValue={config.location_indicator_mwo}
        />
        <ReactHookFormHiddenInput
          name="firName"
          defaultValue={allFIRS[defaultATSR].firname}
        />
        {/* TODO: Make the firGeometry dependend on the fir selected for locationIndicatorATSR. Also update the fir layer shown on the map: https://gitlab.com/opengeoweb/opengeoweb/-/issues/655 */}
        <ReactHookFormHiddenInput name="firGeometry" defaultValue={getFir()} />
      </Grid>
    </Grid>
  );
};

export default SelectFIR;
