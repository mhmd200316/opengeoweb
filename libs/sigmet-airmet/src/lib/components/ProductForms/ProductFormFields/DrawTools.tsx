/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, Radio, Tooltip } from '@mui/material';
import {
  DrawRegion,
  DrawPolygon,
  DrawFIRLand,
  Delete,
  Location,
} from '@opengeoweb/theme';
import { MapDrawMode, StartOrEndDrawing } from '../../../types';

const styles = {
  drawButtons: {
    color: '#0075a9',
    borderRadius: '5px',
    border: 'solid 1px #0075a9',
    padding: '2px',
  },
  drawButtonsSelected: {
    color: 'white',
    backgroundColor: '#0075a9',
    borderRadius: '5px',
    border: 'solid 1px #0075a9',
    padding: '2px',
  },
};

interface DrawToolsProps {
  type?: StartOrEndDrawing | '';
  drawMode: MapDrawMode;
  setDrawMode: (newMapDrawMode: MapDrawMode) => void;
}

const DrawTools: React.FC<DrawToolsProps> = ({
  type = '',
  drawMode,
  setDrawMode,
}: DrawToolsProps) => {
  return (
    <Grid
      item
      xs={12}
      container
      justifyContent="space-between"
      data-testid="drawtools"
    >
      <Grid item>
        <Tooltip title={`Drop a point as ${type} position`}>
          <Radio
            icon={<Location sx={styles.drawButtons} />}
            value
            checkedIcon={<Location sx={styles.drawButtonsSelected} />}
            checked={drawMode === MapDrawMode.POINT}
            onChange={(): void => {
              setDrawMode(MapDrawMode.POINT);
            }}
            data-testid="drawtools-point"
          />
        </Tooltip>
      </Grid>
      <Grid item>
        <Tooltip title={`Draw a box as ${type} position`}>
          <Radio
            icon={<DrawRegion sx={styles.drawButtons} />}
            checkedIcon={<DrawRegion sx={styles.drawButtonsSelected} />}
            checked={drawMode === MapDrawMode.BOX}
            onChange={(): void => {
              setDrawMode(MapDrawMode.BOX);
            }}
            data-testid="drawtools-box"
          />
        </Tooltip>
      </Grid>
      <Grid item>
        <Tooltip title={`Draw a shape as ${type} position`}>
          <Radio
            icon={<DrawPolygon sx={styles.drawButtons} />}
            checkedIcon={<DrawPolygon sx={styles.drawButtonsSelected} />}
            checked={drawMode === MapDrawMode.POLYGON}
            onChange={(): void => {
              setDrawMode(MapDrawMode.POLYGON);
            }}
            data-testid="drawtools-polygon"
          />
        </Tooltip>
      </Grid>
      <Grid item>
        <Tooltip title={`Set FIR as ${type} position`}>
          <Radio
            icon={<DrawFIRLand sx={styles.drawButtons} />}
            checkedIcon={<DrawFIRLand sx={styles.drawButtonsSelected} />}
            checked={drawMode === MapDrawMode.FIR}
            onChange={(): void => {
              setDrawMode(MapDrawMode.FIR);
            }}
            data-testid="drawtools-fir"
          />
        </Tooltip>
      </Grid>
      <Grid item>
        <Tooltip title="Remove geometry">
          <Radio
            icon={<Delete sx={styles.drawButtons} />}
            checkedIcon={<Delete sx={styles.drawButtonsSelected} />}
            checked={drawMode === MapDrawMode.DELETE}
            onChange={(): void => {
              setDrawMode(MapDrawMode.DELETE);
            }}
            data-testid="drawtools-delete"
          />
        </Tooltip>
      </Grid>
    </Grid>
  );
};

export default DrawTools;
