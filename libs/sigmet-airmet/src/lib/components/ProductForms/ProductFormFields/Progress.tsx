/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, Typography, MenuItem, FormHelperText } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import {
  isValidGeoJsonCoordinates,
  isMaximumOneDrawing,
  hasMaxFeaturePoints,
  hasIntersectionWithFIR,
  isInteger,
  ReactHookFormSelect,
  ReactHookFormRadioGroup,
  ReactHookFormNumberField,
  ReactHookFormHiddenInput,
  isValidMax,
  isValidMin,
  isEmpty,
  hasMulitpleIntersections,
} from '@opengeoweb/form-fields';

import { useDraftFormHelpers } from '@opengeoweb/shared';
import DrawTools from './DrawTools';
import {
  Direction,
  MovementUnit,
  MapDrawMode,
  StartOrEndDrawing,
  SetMapDrawModes,
  MapGeoJSONS,
  ProductType,
  ProductConfig,
} from '../../../types';
import { styles } from '../ProductForm.styles';
import { RadioButtonAndLabel } from './RadioButtonAndLabel';
import { getAllowedUnits, useProductSettings } from '../utils';
import sigmetConfig from '../../../utils/sigmetConfig.json';
import airmetConfig from '../../../utils/airmetConfig.json';
import { isFir } from '../../../utils/getFir';
import {
  exitDrawModeMessage,
  maxFeaturePointsMessage,
  multiIntersectionsMessage,
} from './StartGeometry';

export const movementStepValMessageKT =
  'Speed should be rounded to the nearest 5kt';
export const movementStepValMessageKMH =
  'Speed should be rounded to the nearest 10kmh';
export const noIntersectionMessage =
  'The end position needs to be (partly) inside the FIR';
export const maximum6PointsMessage =
  'The end position drawing allows a maximum of 6 individual points';

export const validateMovementSteps = (
  value: string,
  unit: string,
): boolean | string => {
  if (isEmpty(value)) return true;
  // Parse to integer to check for steps
  const intLevel = parseInt(value, 10);
  if (MovementUnit[unit] === MovementUnit.KT) {
    return intLevel % 5 === 0 || movementStepValMessageKT;
  }
  if (MovementUnit[unit] === MovementUnit.KMH) {
    return intLevel % 10 === 0 || movementStepValMessageKMH;
  }
  return 'Invalid unit';
};

export const getMaxMovementSpeedValue = (unit: string): number => {
  switch (MovementUnit[unit]) {
    case MovementUnit.KT:
      return 150;
    case MovementUnit.KMH:
      return 99;
    default:
      return 150;
  }
};

export const getMinMovementSpeedValue = (unit: string): number => {
  switch (MovementUnit[unit]) {
    case MovementUnit.KT:
      return 5;
    case MovementUnit.KMH:
      return 10;
    default:
      return 5;
  }
};

interface ProgressProps {
  productType: ProductType;
  drawMode?: SetMapDrawModes;
  isDisabled?: boolean;
  isReadOnly?: boolean;
  geoJSONs?: MapGeoJSONS;
  setDrawModeType?: (
    mapDrawMode: MapDrawMode,
    startOrEnd: StartOrEndDrawing,
  ) => void;
  onChange?: () => void;
}

const Progress: React.FC<ProgressProps> = ({
  productType,
  drawMode,
  isDisabled = false,
  isReadOnly = false,
  geoJSONs,
  setDrawModeType,
  onChange = (): void => null,
}: ProgressProps) => {
  const { watch, getValues, trigger, errors } = useFormContext();
  const { isRequired, isDraft } = useDraftFormHelpers();

  const config = productType === 'sigmet' ? sigmetConfig : airmetConfig;
  const { allowedUnits } = useProductSettings(config as ProductConfig);

  const movementType = watch('movementType');
  const selectedFIR = watch('locationIndicatorATSR');

  // Get allowed movement units based on selected FIR - if no FIR selected, allow all units
  const allowedMovementUnitsForFir = getAllowedUnits(
    selectedFIR,
    allowedUnits,
    'movement_unit',
    MovementUnit,
  );

  return (
    <Grid item container spacing={2} sx={styles.containerItem}>
      <Grid item xs={4} container justifyContent="flex-end">
        <Typography variant="subtitle1" sx={styles.label}>
          Progress
        </Typography>
      </Grid>
      <Grid item xs={8} container justifyContent="flex-start">
        <Grid item xs={12}>
          <ReactHookFormRadioGroup
            name="movementType"
            data-testid="movementType-group"
            rules={{ validate: { isRequired } }}
            onChange={(): void => {
              return setDrawModeType
                ? setDrawModeType(MapDrawMode.DELETE, StartOrEndDrawing.end)
                : null;
            }}
          >
            {watch('phenomenon') === 'VA_CLD' &&
              (!isReadOnly || movementType === 'NO_VA_EXP') && (
                <RadioButtonAndLabel
                  value="NO_VA_EXP"
                  label="No volcanic ash expected"
                  disabled={isDisabled}
                  data-testid="movementType-NO_VA_EXP"
                />
              )}

            {(!isReadOnly || movementType === 'STATIONARY') && (
              <RadioButtonAndLabel
                value="STATIONARY"
                label="Stationary"
                disabled={isDisabled}
                data-testid="movementType-STATIONARY"
              />
            )}
            {(!isReadOnly || movementType === 'MOVEMENT') && (
              <RadioButtonAndLabel
                value="MOVEMENT"
                label="Movement"
                disabled={isDisabled}
                data-testid="movementType-MOVEMENT"
              />
            )}
            {movementType === 'MOVEMENT' && (
              <>
                <Grid item xs={12}>
                  <ReactHookFormSelect
                    name="movementDirection"
                    label="Set direction"
                    rules={{ validate: isRequired }}
                    sx={styles.movement}
                    disabled={isDisabled}
                    data-testid="movement-movementDirection"
                    onChange={(event): void => {
                      event.stopPropagation();
                      onChange();
                    }}
                    autoFocus
                  >
                    {Object.keys(Direction).map((key) => (
                      <MenuItem value={key} key={key}>
                        {Direction[key]}
                      </MenuItem>
                    ))}
                  </ReactHookFormSelect>
                </Grid>
                <Grid item xs={12} container>
                  <Grid item xs={5}>
                    <ReactHookFormSelect
                      name="movementUnit"
                      label="Unit"
                      rules={{ validate: isRequired }}
                      sx={styles.unit}
                      disabled={isDisabled}
                      defaultValue={'KT' as MovementUnit}
                      data-testid="movement-movementUnit"
                      onChange={(event): void => {
                        event.stopPropagation();
                        if (
                          getValues('movementSpeed') ||
                          getValues('movementSpeed') === 0
                        ) {
                          trigger('movementSpeed');
                        }

                        onChange();
                      }}
                    >
                      {Object.keys(allowedMovementUnitsForFir).map((key) => (
                        <MenuItem value={key} key={key}>
                          {MovementUnit[key]}
                        </MenuItem>
                      ))}
                    </ReactHookFormSelect>
                  </Grid>
                  <Grid item xs={7}>
                    <ReactHookFormNumberField
                      name="movementSpeed"
                      label="Step speed"
                      rules={{
                        validate: {
                          isRequired,
                          isInteger,
                          min: (value): boolean | string =>
                            // The max level depends on the unit
                            isValidMin(
                              value,
                              getMinMovementSpeedValue(
                                getValues('movementUnit'),
                              ),
                            ) ||
                            `The minimum level in ${
                              MovementUnit[getValues('movementUnit')]
                            } is ${getMinMovementSpeedValue(
                              getValues('movementUnit'),
                            )}`,
                          max: (value): boolean | string =>
                            // The max level depends on the unit
                            isValidMax(
                              value,
                              getMaxMovementSpeedValue(
                                getValues('movementUnit'),
                              ),
                            ) ||
                            `The maximum level in ${
                              MovementUnit[getValues('movementUnit')]
                            } is ${getMaxMovementSpeedValue(
                              getValues('movementUnit'),
                            )}`,
                          validateMovementSteps: (value): boolean | string =>
                            // movement step depends on the unit
                            validateMovementSteps(
                              value,
                              getValues('movementUnit'),
                            ),
                        },
                      }}
                      disabled={isDisabled}
                      data-testid="movement-movementSpeed"
                      onChange={(event): void =>
                        event && event.stopPropagation()
                      }
                    />
                  </Grid>
                </Grid>
              </>
            )}

            {productType === 'sigmet' &&
              (!isReadOnly || movementType === 'FORECAST_POSITION') && (
                <RadioButtonAndLabel
                  value="FORECAST_POSITION"
                  label="End position"
                  disabled={isDisabled}
                  data-testid="movementType-FORECAST_POSITION"
                />
              )}
          </ReactHookFormRadioGroup>
          {productType === 'sigmet' && movementType === 'FORECAST_POSITION' && (
            <Grid
              item
              xs={12}
              data-testid="endGeometry"
              sx={styles.drawSection}
            >
              {!isReadOnly && (
                <>
                  <DrawTools
                    data-testid="endGeometry-drawTools"
                    type={StartOrEndDrawing.end}
                    drawMode={drawMode[StartOrEndDrawing.end]}
                    setDrawMode={(mapDrawMode: MapDrawMode): void => {
                      setDrawModeType(mapDrawMode, StartOrEndDrawing.end);
                    }}
                  />
                  {drawMode[StartOrEndDrawing.end] && (
                    <FormHelperText
                      variant="filled"
                      sx={styles.quitDrawModeMessage}
                    >
                      {exitDrawModeMessage}
                    </FormHelperText>
                  )}
                  {!!errors.endGeometry && (
                    <FormHelperText error variant="filled">
                      {errors.endGeometry.message}
                    </FormHelperText>
                  )}
                  {
                    /* non-blocking warnings */
                    hasMaxFeaturePoints(geoJSONs.intersectionEnd) &&
                      !drawMode[StartOrEndDrawing.end] &&
                      !errors.endGeometry &&
                      !isFir(geoJSONs.end) && (
                        <FormHelperText variant="filled">
                          {maxFeaturePointsMessage}
                        </FormHelperText>
                      )
                  }
                  {hasMulitpleIntersections(geoJSONs.intersectionEnd) &&
                    !drawMode[StartOrEndDrawing.end] &&
                    !errors.endGeometry &&
                    !isFir(geoJSONs.end) && (
                      <FormHelperText variant="filled">
                        {multiIntersectionsMessage}
                      </FormHelperText>
                    )}
                </>
              )}
              <ReactHookFormHiddenInput
                name="endGeometry"
                rules={{
                  validate: {
                    maximumOneDrawing: (
                      value: GeoJSON.FeatureCollection,
                    ): boolean | string =>
                      isMaximumOneDrawing(value) ||
                      'Only one end position drawing is allowed',
                    coordinatesNotEmpty: (
                      value: GeoJSON.FeatureCollection,
                    ): boolean | string =>
                      getValues('movementType') === 'FORECAST_POSITION' &&
                      !isDraft()
                        ? isValidGeoJsonCoordinates(value) ||
                          'An end position drawing is required when selecting End position'
                        : true,
                    intersectWithFIR: (
                      value: GeoJSON.FeatureCollection,
                    ): boolean | string =>
                      hasIntersectionWithFIR(
                        value,
                        getValues('endGeometryIntersect'),
                      ) || noIntersectionMessage,
                    hasMaxFeaturePoints: (
                      value: GeoJSON.FeatureCollection,
                    ): boolean | string =>
                      isFir(value)
                        ? true
                        : !hasMaxFeaturePoints(value) || maximum6PointsMessage,
                  },
                }}
              />
              <ReactHookFormHiddenInput name="endGeometryIntersect" />
            </Grid>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Progress;
