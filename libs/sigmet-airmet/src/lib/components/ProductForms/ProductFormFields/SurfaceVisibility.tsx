/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, MenuItem } from '@mui/material';
import {
  ReactHookFormSelect,
  ReactHookFormNumberField,
  errorMessages,
  isEmpty,
} from '@opengeoweb/form-fields';
import { useDraftFormHelpers } from '@opengeoweb/shared';
import { VisibilityCause } from '../../../types';
import { styles } from '../ProductForm.styles';

export const invalidSurfaceVisibilityTill750StepsMessage =
  'A visibility below 750m must be rounded to the nearest 50m';
export const invalidSurfaceVisibilityFrom750StepsMessage =
  'A visibility above 750m must be rounded to the nearest 100m';

export const validateSurfaceVisibility = (value: string): boolean | string => {
  if (isEmpty(value)) return true;
  // Parse to integer to check for steps
  const intVisibility = parseInt(value, 10);
  if (intVisibility > 750) {
    return (
      intVisibility % 100 === 0 || invalidSurfaceVisibilityFrom750StepsMessage
    );
  }
  return (
    intVisibility % 50 === 0 || invalidSurfaceVisibilityTill750StepsMessage
  );
};

interface SurfaceVisibilityProps {
  isDisabled: boolean;
  onChange: () => void;
}

const SurfaceVisibility: React.FC<SurfaceVisibilityProps> = ({
  isDisabled,
  onChange = (): void => null,
}: SurfaceVisibilityProps) => {
  const { isRequired } = useDraftFormHelpers();

  return (
    <Grid item container spacing={2}>
      <Grid item xs={4} container justifyContent="flex-end" />
      <Grid item xs={8} container justifyContent="flex-start">
        <Grid item xs={12} sx={styles.surfaceVisibility}>
          <ReactHookFormNumberField
            name="visibilityValue"
            data-testid="surfaceVisibility-visibilityValue"
            label="Select visibility"
            helperText="Value in meters"
            rules={{
              min: {
                value: 0,
                message: errorMessages.minVisibility,
              },
              max: {
                value: 4900,
                message: errorMessages.maxVisibility,
              },
              validate: {
                isRequired,
                validateSurfaceVisibility,
              },
            }}
            disabled={isDisabled}
          />
        </Grid>
        <Grid item xs={12}>
          <ReactHookFormSelect
            name="visibilityCause"
            label="Select cause"
            rules={{ validate: { isRequired } }}
            disabled={isDisabled}
            data-testid="surfaceVisibility-visibilityCause"
            onChange={(event): void => {
              event.stopPropagation();
              onChange();
            }}
          >
            {Object.keys(VisibilityCause).map((key) => (
              <MenuItem value={key} key={key}>
                {VisibilityCause[key]}
              </MenuItem>
            ))}
          </ReactHookFormSelect>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default SurfaceVisibility;
