/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  Checkbox,
  FormControlLabel,
  Grid,
  MenuItem,
  Typography,
} from '@mui/material';
import {
  isEmpty,
  isInteger,
  isValidMax,
  isValidMin,
  ReactHookFormHiddenInput,
  ReactHookFormSelect,
  ReactHookFormNumberField,
} from '@opengeoweb/form-fields';
import { useFormContext } from 'react-hook-form';
import { useDraftFormHelpers } from '@opengeoweb/shared';
import { CloudLevelUnits, ProductConfig } from '../../../types';
import { styles } from '../ProductForm.styles';
import {
  getAllowedUnits,
  getMaxCloudLevelValue,
  getMaxCloudLowerLevelValue,
  getMinCloudLevelValue,
  getMinCloudLowerLevelValue,
  isLevelLower,
  useProductSettings,
} from '../utils';
import airmetConfig from '../../../utils/airmetConfig.json';

export const invalidStepsForFeetUnitMessage =
  'A level must be rounded to the nearest 100 ft';
export const invalidStepsForMetersUnitBelow2970Message =
  'A level must be rounded to the nearest 30 m';
export const invalidStepsForMetersUnitAbove2970Message =
  'A level must be rounded to the nearest 300 m';
export const invalidUnitMessage = 'Level unit not defined';

export const validateRoundedStep = (
  value: string,
  unit: string,
): boolean | string => {
  if (isEmpty(value)) return true;
  if (CloudLevelUnits[unit] === CloudLevelUnits.FT) {
    return parseInt(value, 10) % 100 === 0
      ? true
      : invalidStepsForFeetUnitMessage;
  }
  if (CloudLevelUnits[unit] === CloudLevelUnits.M) {
    if (value <= '2970') {
      return parseInt(value, 10) % 30 === 0
        ? true
        : invalidStepsForMetersUnitBelow2970Message;
    }

    return parseInt(value, 10) % 300 === 0
      ? true
      : invalidStepsForMetersUnitAbove2970Message;
  }
  return invalidUnitMessage;
};

interface CloudLevelsProps {
  isDisabled: boolean;
  isReadOnly: boolean;
  onChange?: () => void;
}

const CloudLevels: React.FC<CloudLevelsProps> = ({
  isDisabled,
  isReadOnly,
  onChange = (): void => null,
}: CloudLevelsProps) => {
  const { watch, setValue, trigger, getValues } = useFormContext();
  const { isRequired } = useDraftFormHelpers();

  const { allowedUnits } = useProductSettings(airmetConfig as ProductConfig);

  const selectedFIR = watch('locationIndicatorATSR');

  // Get allowed cloud level units based on selected FIR - if no FIR selected, allow all default units
  const allowedCloudLevelUnitsForFir = getAllowedUnits(
    selectedFIR,
    allowedUnits,
    'cloud_level_unit',
    CloudLevelUnits,
  );

  return (
    <Grid item container spacing={2} sx={styles.containerItem}>
      <Grid item xs={4} container justifyContent="flex-end">
        <Typography variant="subtitle1" sx={styles.label}>
          Levels
        </Typography>
      </Grid>
      <Grid item xs={8} container justifyContent="flex-start">
        <Grid item xs={12}>
          <ReactHookFormHiddenInput
            name="cloudLevelInfoMode"
            defaultValue="BETW"
          />
          <Grid item xs={12} container>
            <Grid item xs={4}>
              {(!isReadOnly ||
                watch('cloudLevelInfoMode') === 'BETW_ABV' ||
                watch('cloudLevelInfoMode') === 'BETW_SFC_ABV') && (
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={
                        watch('cloudLevelInfoMode') === 'BETW_ABV' ||
                        watch('cloudLevelInfoMode') === 'BETW_SFC_ABV'
                      }
                      onChange={(): void => {
                        switch (getValues('cloudLevelInfoMode')) {
                          case 'BETW':
                            setValue('cloudLevelInfoMode', 'BETW_ABV', {
                              shouldDirty: true,
                            });
                            break;
                          case 'BETW_ABV':
                            setValue('cloudLevelInfoMode', 'BETW', {
                              shouldDirty: true,
                            });
                            break;
                          case 'BETW_SFC':
                            setValue('cloudLevelInfoMode', 'BETW_SFC_ABV', {
                              shouldDirty: true,
                            });
                            break;
                          case 'BETW_SFC_ABV':
                            setValue('cloudLevelInfoMode', 'BETW_SFC', {
                              shouldDirty: true,
                            });
                            break;
                          default:
                            break;
                        }
                      }}
                      name="checkedAbove"
                      color="secondary"
                      disabled={isDisabled}
                    />
                  }
                  data-testid="cloudLevels-Above"
                  label="Above"
                />
              )}
            </Grid>
            <Grid item xs={3}>
              <ReactHookFormSelect
                name="cloudLevel.unit"
                label="Unit"
                data-testid="cloudLevel-unit"
                rules={{ validate: { isRequired } }}
                sx={styles.unit}
                disabled={isDisabled}
                defaultValue={'FT' as CloudLevelUnits}
                onChange={(): void => {
                  if (
                    getValues('cloudLevel.value') ||
                    getValues('cloudLevel.value') === 0
                  ) {
                    trigger('cloudLevel.value');
                  }
                  // Unit change is reflected in the Lower Cloud Level
                  setValue(
                    'cloudLowerLevel.unit',
                    getValues('cloudLevel.unit'),
                  );
                  if (
                    getValues('cloudLowerLevel.value') ||
                    getValues('cloudLowerLevel.value') === 0
                  ) {
                    trigger('cloudLowerLevel.value');
                  }
                  onChange();
                }}
              >
                {Object.keys(allowedCloudLevelUnitsForFir).map((key) => (
                  <MenuItem value={key} key={key}>
                    {allowedCloudLevelUnitsForFir[key]}
                  </MenuItem>
                ))}
              </ReactHookFormSelect>
            </Grid>
            <Grid item xs={5}>
              <ReactHookFormNumberField
                name="cloudLevel.value"
                label="Upper level"
                data-testid="cloudLevel-value"
                sx={styles.levelField}
                rules={{
                  validate: {
                    isRequired,
                    isInteger,
                    min: (value): boolean | string =>
                      // The min level depends on the unit
                      isValidMin(
                        value,
                        getMinCloudLevelValue(getValues('cloudLevel.unit')),
                      ) ||
                      `The minimum level in ${
                        CloudLevelUnits[getValues('cloudLevel.unit')]
                      } is ${getMinCloudLevelValue(
                        getValues('cloudLevel.unit'),
                      )}`,
                    max: (value): boolean | string =>
                      // The max level depends on the unit
                      isValidMax(
                        value,
                        getMaxCloudLevelValue(getValues('cloudLevel.unit')),
                      ) ||
                      `The maximum level in ${
                        CloudLevelUnits[getValues('cloudLevel.unit')]
                      } is ${getMaxCloudLevelValue(
                        getValues('cloudLevel.unit'),
                      )}`,
                    step: (value): boolean | string =>
                      validateRoundedStep(value, getValues('cloudLevel.unit')),
                  },
                }}
                disabled={isDisabled}
                onChange={(): Promise<boolean> => {
                  return (
                    (getValues('cloudLowerLevel.value') ||
                      getValues('cloudLowerLevel.value') === 0) &&
                    trigger('cloudLowerLevel.value')
                  );
                }}
              />
            </Grid>
          </Grid>
          <Grid item xs={12} container>
            <Grid item xs={4}>
              {(!isReadOnly ||
                watch('cloudLevelInfoMode') === 'BETW_SFC' ||
                watch('cloudLevelInfoMode') === 'BETW_SFC_ABV') && (
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={
                        watch('cloudLevelInfoMode') === 'BETW_SFC' ||
                        watch('cloudLevelInfoMode') === 'BETW_SFC_ABV'
                      }
                      onChange={(): void => {
                        switch (getValues('cloudLevelInfoMode')) {
                          case 'BETW':
                            setValue('cloudLevelInfoMode', 'BETW_SFC', {
                              shouldDirty: true,
                            });
                            break;
                          case 'BETW_ABV':
                            setValue('cloudLevelInfoMode', 'BETW_SFC_ABV', {
                              shouldDirty: true,
                            });
                            break;
                          case 'BETW_SFC':
                            setValue('cloudLevelInfoMode', 'BETW', {
                              shouldDirty: true,
                            });
                            break;
                          case 'BETW_SFC_ABV':
                            setValue('cloudLevelInfoMode', 'BETW_ABV', {
                              shouldDirty: true,
                            });
                            break;
                          default:
                            break;
                        }
                      }}
                      name="checkedSFC"
                      color="secondary"
                      disabled={isDisabled}
                    />
                  }
                  data-testid="cloudLevels-SFC"
                  label="SFC"
                />
              )}
            </Grid>
            {watch('cloudLevelInfoMode') !== 'BETW_SFC' &&
              watch('cloudLevelInfoMode') !== 'BETW_SFC_ABV' && (
                <>
                  <Grid item xs={3}>
                    <ReactHookFormSelect
                      name="cloudLowerLevel.unit"
                      label="Unit"
                      data-testid="cloudLowerLevel-unit"
                      rules={{ validate: { isRequired } }}
                      sx={styles.unit}
                      disabled={isDisabled}
                      defaultValue={
                        getValues('cloudLevel.unit')
                          ? getValues('cloudLevel.unit')
                          : ('FT' as CloudLevelUnits)
                      }
                      onChange={(): void => {
                        if (
                          getValues('cloudLowerLevel.value') ||
                          getValues('cloudLowerLevel.value') === 0
                        ) {
                          trigger('cloudLowerLevel.value');
                        }
                        // Unit change is reflected in the Upper Cloud Level
                        setValue(
                          'cloudLevel.unit',
                          getValues('cloudLowerLevel.unit'),
                        );
                        if (
                          getValues('cloudLevel.value') ||
                          getValues('cloudLevel.value') === 0
                        ) {
                          trigger('cloudLevel.value');
                        }
                        onChange();
                      }}
                    >
                      {Object.keys(allowedCloudLevelUnitsForFir).map((key) => (
                        <MenuItem value={key} key={key}>
                          {allowedCloudLevelUnitsForFir[key]}
                        </MenuItem>
                      ))}
                    </ReactHookFormSelect>
                  </Grid>
                  <Grid item xs={5}>
                    <ReactHookFormNumberField
                      name="cloudLowerLevel.value"
                      label="Lower level"
                      data-testid="cloudLowerLevel-value"
                      rules={{
                        validate: {
                          isRequired,
                          isInteger,
                          min: (value): boolean | string =>
                            // The min level depends on the unit
                            isValidMin(
                              value,
                              getMinCloudLowerLevelValue(
                                getValues('cloudLowerLevel.unit'),
                              ),
                            ) ||
                            `The minimum level in ${
                              CloudLevelUnits[getValues('cloudLowerLevel.unit')]
                            } is ${getMinCloudLowerLevelValue(
                              getValues('cloudLowerLevel.unit'),
                            )}`,
                          max: (value): boolean | string =>
                            // The max level depends on the unit
                            isValidMax(
                              value,
                              getMaxCloudLowerLevelValue(
                                getValues('cloudLowerLevel.unit'),
                              ),
                            ) ||
                            `The maximum level in ${
                              CloudLevelUnits[getValues('cloudLowerLevel.unit')]
                            } is ${getMaxCloudLowerLevelValue(
                              getValues('cloudLowerLevel.unit'),
                            )}`,
                          isLevelLower: (value): boolean | string =>
                            // The lower level needs to be smaller than upper level
                            isLevelLower(
                              value,
                              getValues('cloudLowerLevel.unit'),
                              getValues('cloudLevel.value'),
                              getValues('cloudLevel.unit'),
                            ),
                          step: (value): boolean | string =>
                            validateRoundedStep(
                              value,
                              getValues('cloudLowerLevel.unit'),
                            ),
                        },
                      }}
                      disabled={isDisabled}
                    />
                  </Grid>
                </>
              )}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default CloudLevels;
