/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { Grid, Typography } from '@mui/material';
import moment from 'moment';
import {
  ReactHookFormDateTime,
  isAfter,
  isValidDate,
  isXHoursAfter,
} from '@opengeoweb/form-fields';

import { useDraftFormHelpers } from '@opengeoweb/shared';
import { styles } from '../ProductForm.styles';
import sigmetConfig from '../../../utils/sigmetConfig.json';
import airmetConfig from '../../../utils/airmetConfig.json';
import { getMaxHoursOfValidity } from '../utils';
import { ProductType } from '../../../types';
import { VALID_FROM_TIME_DELAY_IN_MINUTES } from './ValidFrom';

interface ValidUntilProps {
  productType: ProductType;
  isDisabled: boolean;
  onChange?: () => void;
}

export const DEFAULT_VALID_TIME_SIGMET = 3;
export const DEFAULT_VALID_TIME_AIRMET = 1;

export const getDefaultHoursValid = (productType: ProductType): number => {
  return productType === 'sigmet'
    ? DEFAULT_VALID_TIME_SIGMET
    : DEFAULT_VALID_TIME_AIRMET;
};

const ValidUntil: React.FC<ValidUntilProps> = ({
  productType,
  isDisabled,
  onChange = (): void => null,
}: ValidUntilProps) => {
  const { getValues, trigger } = useFormContext();
  const { isRequired } = useDraftFormHelpers();

  const config = productType === 'sigmet' ? sigmetConfig : airmetConfig;
  const defaultHoursValid = getDefaultHoursValid(productType);

  return (
    <Grid item container spacing={2}>
      <Grid item xs={4} container justifyContent="flex-end">
        <Typography variant="subtitle1" sx={styles.label}>
          Valid until
        </Typography>
      </Grid>
      <Grid item xs={8} container justifyContent="flex-start">
        <ReactHookFormDateTime
          disablePast
          name="validDateEnd"
          defaultNullValue={moment
            .utc()
            .add(defaultHoursValid, 'hour')
            .add(VALID_FROM_TIME_DELAY_IN_MINUTES, 'minutes')
            .format('YYYY-MM-DDTHH:mm:ss[Z]')}
          rules={{
            validate: {
              isRequired,
              isValidDate,
              // Validity end has to be after validity start
              isAfter: (value): boolean | string =>
                isAfter(value, getValues('validDateStart')) ||
                'Valid until time has to be after Valid from time',
              // Validity end can be no more than 4 hours after validity start (6 for VA/TC SIGMET)
              isXHoursAfter: (value): boolean | string =>
                isXHoursAfter(
                  value,
                  getValues('validDateStart'),
                  getMaxHoursOfValidity(
                    getValues('phenomenon'),
                    getValues('locationIndicatorATSR'),
                    config,
                  ),
                ) ||
                `Valid until time can be no more than ${getMaxHoursOfValidity(
                  getValues('phenomenon'),
                  getValues('locationIndicatorATSR'),
                  config,
                )} hours after Valid from time`,
            },
          }}
          disabled={isDisabled}
          onChange={(): void => {
            if (getValues('validDateStart')) {
              trigger('validDateStart');
            }
            if (getValues('observationOrForecastTime')) {
              trigger('observationOrForecastTime');
            }
            onChange();
          }}
        />
      </Grid>
      <Grid item xs={6} />
    </Grid>
  );
};

export default ValidUntil;
