/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import { CreateApi } from '@opengeoweb/api';
import { ConfirmationServiceProvider } from '@opengeoweb/shared';

import { ProductFormDialogContent } from './ProductFormDialogContent';
import { Airmet, FormMode, ProductType, Sigmet } from '../../types';
import {
  fakeSigmetList,
  fakeVolcanicCancelSigmetNoMoveTo,
  fakeVolcanicCancelSigmetWithMoveTo,
} from '../../utils/mockdata/fakeSigmetList';
import { noTAC } from './ProductFormTac';
import { TestWrapper } from '../../utils/testUtils';
import {
  fakeAirmetList,
  fakeDraftAirmet,
} from '../../utils/mockdata/fakeAirmetList';
import { fakeSigmetTAC } from '../../utils/fakeApi';

jest.mock('../../utils/api');

describe('components/SigmetForm/ProductFormDialogContent', () => {
  beforeEach(() => {
    jest.spyOn(console, 'log').mockImplementation();
  });
  interface WrapperProps {
    children: React.ReactNode;
    createApi?: CreateApi;
  }

  const Wrapper: React.FC<WrapperProps> = ({
    children,
    createApi = null,
  }: WrapperProps) => (
    <TestWrapper createApi={createApi}>
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <ConfirmationServiceProvider>{children}</ConfirmationServiceProvider>
      </ReactHookFormProvider>
    </TestWrapper>
  );

  it('should display only the Cancel button for a published sigmet', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet' as ProductType,
      productCanBe: ['CANCELLED'],
      product: fakeSigmetList[1].sigmet,
      mode: 'edit' as FormMode,
    };

    const { getByTestId, queryByTestId, queryByText } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(getByTestId('productform-dialog-cancel')).toBeTruthy();
    expect(queryByTestId('productform-dialog-draft')).toBeFalsy();
    expect(queryByTestId('productform-dialog-discard')).toBeFalsy();
    expect(queryByTestId('productform-dialog-publish')).toBeFalsy();
  });

  it('should not show the "volcanic ash cloud move" to if not received from the BE', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet' as ProductType,
      productCanBe: ['CANCELLED'],
      product: fakeVolcanicCancelSigmetNoMoveTo.sigmet,
      mode: 'edit' as FormMode,
    };

    const { getByTestId, queryByText, queryByTestId } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(getByTestId('productform-dialog-cancel')).toBeTruthy();
    await waitFor(() => expect(queryByTestId('vaSigmetMoveToFIR')).toBeFalsy());
  });

  it('should show the "volcanic ash cloud move to" if received from the BE', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet' as ProductType,
      productCanBe: ['CANCELLED'],
      product: fakeVolcanicCancelSigmetWithMoveTo.sigmet,
      mode: 'edit' as FormMode,
    };

    const { getByTestId, queryByText } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(getByTestId('productform-dialog-cancel')).toBeTruthy();
    expect(getByTestId('vaSigmetMoveToFIR')).toBeTruthy();
  });

  it('should show errors when pressing publish on an invalid SIGMET', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet' as ProductType,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null,
      mode: 'new' as FormMode,
    };

    const { getByTestId, getByText, queryByText } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    expect(queryByText('A start position drawing is required')).toBeFalsy();
    fireEvent.click(getByTestId('productform-dialog-publish'));
    await waitFor(() =>
      expect(getByText('A start position drawing is required')).toBeTruthy(),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(0),
    );
  });

  it('should be able to save a SIGMET as draft with only phenomenon as value', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet' as ProductType,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: {
        phenomenon: 'EMBD_TS',
      } as unknown as Sigmet,
      mode: 'new' as FormMode,
    };

    const { getByTestId, queryAllByText } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    // test publish is disabled
    fireEvent.click(getByTestId('productform-dialog-publish'));
    await waitFor(() =>
      expect(queryAllByText('This field is required')).toBeTruthy(),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(0),
    );

    // save as draft
    fireEvent.click(getByTestId('productform-dialog-draft'));
    await waitFor(() =>
      expect(queryAllByText('This field is required')).toHaveLength(0),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1),
    );
  });

  it('should show the confirmation dialog when pressing Publish on a valid SIGMET', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet' as ProductType,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: fakeSigmetList[0].sigmet,
      mode: 'edit' as FormMode,
    };

    const { getByTestId, queryByText } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    fireEvent.click(getByTestId('productform-dialog-publish'));
    await waitFor(() => expect(getByTestId('confirmationDialog')).toBeTruthy());
  });

  it('should show the confirmation dialog when pressing Discard', async () => {
    const props = {
      isOpen: true,
      productType: 'airmet' as ProductType,
      toggleDialogStatus: jest.fn(),
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null,
      mode: 'new' as FormMode,
    };

    const { getByTestId } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    fireEvent.click(getByTestId('productform-dialog-discard'));
    await waitFor(() => expect(getByTestId('confirmationDialog')).toBeTruthy());
  });

  it('should not show the confirmation dialog when pressing Save on a valid airmet', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'airmet' as ProductType,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: fakeDraftAirmet,
      mode: 'edit' as FormMode,
    };

    const { getByTestId, queryByTestId, queryByText } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    fireEvent.click(getByTestId('productform-dialog-draft'));
    await waitFor(() => {
      expect(queryByTestId('confirmationDialog')).toBeFalsy();
      expect(queryByTestId('loader').style.opacity).toEqual('1');
    });

    await waitFor(() => {
      expect(queryByTestId('loader').style.opacity).toEqual('0');
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1);
    });
  });

  it('should show the confirmation dialog when pressing Back and form changes have been made', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet' as ProductType,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null,
      mode: 'new' as FormMode,
    };

    const { getByTestId, queryByTestId } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    const obsField = getByTestId('isObservationOrForecast-OBS');
    // trigger form change so form will set isDirty flag
    fireEvent.click(obsField);
    // wait on observation value to be set
    await waitFor(() => {
      expect(obsField.querySelector('.Mui-checked')).toBeTruthy();
    });
    // close modal
    fireEvent.click(getByTestId('contentdialog-close'));
    await waitFor(() => expect(getByTestId('confirmationDialog')).toBeTruthy());
    // close and re-open to make sure it doesn't reappear
    fireEvent.click(getByTestId('confirmationDialog-cancel'));
    await waitFor(() =>
      expect(queryByTestId('confirmationDialog')).toBeFalsy(),
    );
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should not show the confirmation dialog when pressing Back and no form changes have been made', async () => {
    const props = {
      isOpen: true,
      productType: 'sigmet' as ProductType,
      toggleDialogStatus: jest.fn(),
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null,
      mode: 'new' as FormMode,
    };

    const { queryByTestId } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    fireEvent.click(queryByTestId('contentdialog-close'));
    await waitFor(() =>
      expect(queryByTestId('confirmationDialog')).toBeFalsy(),
    );
    await waitFor(() => expect(props.toggleDialogStatus).toHaveBeenCalled());
  });

  it('should show the confirmation dialog when pressing Cancel', async () => {
    const props = {
      isOpen: true,
      productType: 'airmet' as ProductType,
      toggleDialogStatus: jest.fn(),
      productCanBe: ['CANCELLED'],
      product: fakeAirmetList[1].airmet,
      mode: 'view' as FormMode,
    };

    const { getByTestId, queryByText } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    fireEvent.click(getByTestId('productform-dialog-cancel'));
    await waitFor(() => expect(getByTestId('confirmationDialog')).toBeTruthy());
  });

  it('should show the confirmation dialog when pressing Publish after changing movementType from forecast position to stationary', async () => {
    const props = {
      isOpen: true,
      productType: 'sigmet' as ProductType,
      toggleDialogStatus: jest.fn(),
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: fakeSigmetList[0].sigmet,
      mode: 'edit' as FormMode,
    };

    const { getByTestId, queryByText, queryByTestId } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    // check that forecast position is checked
    const forecastPosition = getByTestId('movementType-FORECAST_POSITION');
    expect(forecastPosition.querySelector('.Mui-checked')).toBeTruthy();
    expect(queryByTestId('endGeometry')).toBeTruthy();

    // change movementType to stationary
    const stationary = getByTestId('movementType-STATIONARY');
    fireEvent.click(stationary);
    await waitFor(() => {
      expect(stationary.querySelector('.Mui-checked')).toBeTruthy();
      expect(queryByTestId('endGeometry')).toBeFalsy();
    });

    // publish
    fireEvent.click(getByTestId('productform-dialog-publish'));
    await waitFor(() => expect(getByTestId('confirmationDialog')).toBeTruthy());
  });

  it('should not contain the movement forecast position option for an airmet', async () => {
    const props = {
      isOpen: true,
      productType: 'airmet' as ProductType,
      toggleDialogStatus: jest.fn(),
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: fakeAirmetList[0].airmet,
      mode: 'edit' as FormMode,
    };

    const { getByTestId, queryByTestId } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    // check that forecast position is checked
    expect(getByTestId('movementType-MOVEMENT')).toBeTruthy();
    expect(getByTestId('movementType-STATIONARY')).toBeTruthy();
    expect(queryByTestId('movementType-FORECAST_POSITION')).toBeFalsy();
  });

  it('should be able to save a Airmet as draft with only phenomenon as value', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'airmet' as ProductType,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: {
        phenomenon: 'ISOL_TS',
      } as unknown as Airmet,
      mode: 'new' as FormMode,
    };

    const { getByTestId, queryAllByText } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    // test publish is disabled
    fireEvent.click(getByTestId('productform-dialog-publish'));
    await waitFor(() =>
      expect(queryAllByText('This field is required')).toBeTruthy(),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(0),
    );

    // save as draft
    fireEvent.click(getByTestId('productform-dialog-draft'));
    await waitFor(() =>
      expect(queryAllByText('This field is required')).toHaveLength(0),
    );
    await waitFor(() =>
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1),
    );
  });

  it('should save a SIGMET with only phenomenon as draft when pressing BACK and choosing Save and Close', async () => {
    jest.useFakeTimers();

    const mockGetSigmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: fakeSigmetList });
      });
    });

    const mockGetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeSigmetTAC,
        });
      });
    });

    const mockPostSigmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 3000);
      });
    });

    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'sigmet' as ProductType,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null,
      mode: 'new' as FormMode,
    };

    const { getByTestId, findByText, container, queryByTestId } = render(
      <Wrapper
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            getSigmetList: mockGetSigmetList,
            postSigmet: mockPostSigmet,
            getSigmetTAC: mockGetTac,
          };
        }}
      >
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    // choose phenomenon
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role=button]'),
    );
    const menuItem = await findByText('Severe icing');
    fireEvent.click(menuItem);
    // wait on phenomenon value to be set
    await waitFor(() => {
      expect(container.querySelector('li[data-value=SEV_ICE]')).toBeFalsy();
      expect(
        getByTestId('phenomenon').querySelector('input').previousElementSibling
          .textContent,
      ).toEqual('Severe icing');
      expect(getByTestId('phenomenon').querySelector('input').value).toEqual(
        'SEV_ICE',
      );
    });
    // press BACK button
    fireEvent.click(getByTestId('contentdialog-close'));
    await waitFor(() => expect(getByTestId('confirmationDialog')).toBeTruthy());
    // click save and close
    fireEvent.click(getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(queryByTestId('confirmationDialog')).toBeFalsy();
    });
    await waitFor(() => {
      expect(queryByTestId('loader').style.opacity).toEqual('1');
    });
    await waitFor(() => {
      expect(mockPostSigmet).toHaveBeenCalledTimes(1);
    });
    jest.runOnlyPendingTimers();
    await waitFor(() => {
      expect(queryByTestId('loader').style.opacity).toEqual('0');
    });
    await waitFor(() => {
      expect(props.toggleDialogStatus).toHaveBeenCalledTimes(1);
    });
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not save an AIRMET without a phenomenon when pressing BACK and choosing Save and Close', async () => {
    const props = {
      isOpen: true,
      toggleDialogStatus: jest.fn(),
      productType: 'airmet' as ProductType,
      productCanBe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
      product: null,
      mode: 'new' as FormMode,
    };

    const { getByTestId, queryAllByText } = render(
      <Wrapper>
        <ProductFormDialogContent {...props} />
      </Wrapper>,
    );

    const obsField = getByTestId('isObservationOrForecast-OBS');
    // trigger form change so form will set isDirty flag
    fireEvent.click(obsField);
    // wait on observation value to be set
    await waitFor(() => {
      expect(obsField.querySelector('.Mui-checked')).toBeTruthy();
    });
    // press BACK button
    fireEvent.click(getByTestId('contentdialog-close'));
    await waitFor(() => expect(getByTestId('confirmationDialog')).toBeTruthy());
    // click save and close
    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    await waitFor(() =>
      expect(queryAllByText('This field is required')).toBeTruthy(),
    );
    expect(props.toggleDialogStatus).not.toHaveBeenCalled();
  });
});
