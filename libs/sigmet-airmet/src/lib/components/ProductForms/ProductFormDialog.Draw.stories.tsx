/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  fakeDataBox,
  fakeDataPoly,
  fakeSigmetList,
  fakeSigmetMultiPolygon,
} from '../../utils/mockdata/fakeSigmetList';
import { ProductFormDialog } from './ProductFormDialog';
import { StoryWrapperFakeApi } from '../../utils/testUtils';

export default {
  title: 'components/ProductFormDialog/Sigmet/Map Draw Demos',
};

export const NewSigmetDemo = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productType="sigmet"
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const DraftSigmetFirPolygonDemo = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productType="sigmet"
        productListItem={fakeSigmetList[0]}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const DraftSigmetBoxPointDemo = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productType="sigmet"
        productListItem={fakeDataBox}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const DraftSigmetPolygonPolygonDemo = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productType="sigmet"
        productListItem={fakeDataPoly}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const DraftSigmetMultiIntersectionsDemo = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productType="sigmet"
        productListItem={fakeSigmetMultiPolygon}
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};
