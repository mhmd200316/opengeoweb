/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Button, Grid } from '@mui/material';
import moment from 'moment';
import {
  booleanClockwise,
  rewind,
  polygonToLineString,
  Polygon,
  Feature,
} from '@turf/turf';
import { FeatureCollection } from 'geojson';
import produce from 'immer';
import { useIsMounted } from '@opengeoweb/shared';
import {
  LevelUnits,
  ATSRLocation,
  ICAOLocation,
  ProductStatus,
  CancelSigmet,
  Sigmet,
  ProductStatusDescription,
  SigmetPhenomena,
  isInstanceOfCancelSigmet,
  ProductCanbe,
  SigmetFromBackend,
  SigmetFromFrontend,
  ProductActions,
  Airmet,
  CancelAirmet,
  ProductConfig,
  Firareas,
  FIRConfigSigmet,
  AirmetFromBackend,
  AirmetFromFrontend,
  ProductType,
  isInstanceOfCancelSigmetOrAirmet,
  AirmetPhenomena,
  CloudLevelUnits,
  WindUnit,
  FirAllowedUnits,
} from '../../types';

export const getHoursBeforeValidity = (
  phenomenon: string,
  fir: string,
  config: ProductConfig,
): number => {
  if (!fir) return 4;
  const {
    hoursbeforevalidity,
    /* eslint-disable @typescript-eslint/naming-convention */
    va_hoursbeforevalidity,
    tc_hoursbeforevalidity,
  } = config.firareas[fir] as FIRConfigSigmet;

  switch (phenomenon) {
    // Tropical Cyclone
    case 'TC':
      return tc_hoursbeforevalidity;
    // Volcanic ash cloud
    case 'VA_CLD':
      return va_hoursbeforevalidity;
    default:
      return hoursbeforevalidity;
  }
};

export const getMaxHoursOfValidity = (
  phenomenon: string,
  fir: string,
  config: ProductConfig,
): number => {
  if (!fir) return 4;
  const {
    maxhoursofvalidity,
    /* eslint-disable @typescript-eslint/naming-convention */
    va_maxhoursofvalidity,
    tc_maxhoursofvalidity,
  } = config.firareas[fir] as FIRConfigSigmet;

  switch (phenomenon) {
    // Tropical Cyclone
    case 'TC':
      return tc_maxhoursofvalidity;
    // Volcanic ash cloud
    case 'VA_CLD':
      return va_maxhoursofvalidity;
    default:
      return maxhoursofvalidity;
  }
};

export const getMinCloudLevelValue = (unit: string): number => {
  switch (CloudLevelUnits[unit]) {
    case CloudLevelUnits.FT:
      return 100;
    case CloudLevelUnits.M:
      return 30;
    default:
      return 100;
  }
};

export const getMinCloudLowerLevelValue = (unit: string): number => {
  switch (CloudLevelUnits[unit]) {
    case CloudLevelUnits.FT:
      return 100;
    case CloudLevelUnits.M:
      return 30;
    default:
      return 100;
  }
};

export const getMaxCloudLevelValue = (unit: string): number => {
  switch (CloudLevelUnits[unit]) {
    case CloudLevelUnits.FT:
      return 9900;
    case CloudLevelUnits.M:
      return 9900;
    default:
      return 9900;
  }
};

export const getMaxCloudLowerLevelValue = (unit: string): number => {
  switch (CloudLevelUnits[unit]) {
    case CloudLevelUnits.FT:
      return 900;
    case CloudLevelUnits.M:
      return 300;
    default:
      return 900;
  }
};

export const getMaxLevelValue = (
  unit: string,
  productType: ProductType,
): number => {
  switch (LevelUnits[unit]) {
    case LevelUnits.FL:
      // For FL airmet max is 100, for sigmet 650
      if (productType === 'airmet') {
        return 100;
      }
      return 650;
    case LevelUnits.FT:
      return 4900;
    case LevelUnits.M:
      return 9999;
    default:
      return 4900;
  }
};

export const getMinLevelValue = (unit: string): number => {
  switch (LevelUnits[unit]) {
    case LevelUnits.FL:
      return 50;
    case LevelUnits.FT:
      return 100;
    case LevelUnits.M:
      return 1;
    default:
      return 100;
  }
};

export const getMaxWindSpeedValue = (unit: string): number => {
  switch (WindUnit[unit]) {
    case WindUnit.KT:
      return 199;
    case WindUnit.MPS:
      return 99;
    default:
      return 199;
  }
};

export const getMinWindSpeedValue = (unit: string): number => {
  switch (WindUnit[unit]) {
    case WindUnit.KT:
      return 31;
    case WindUnit.MPS:
      return 0;
    default:
      return 31;
  }
};

export const useProductSettings = (
  productConfig: ProductConfig,
): {
  defaultATSR: ATSRLocation;
  optionsFIR: { ATSR: ATSRLocation; FIR: ICAOLocation }[];
  allFIRS: Firareas;
  allowedUnits: FirAllowedUnits;
} => {
  const firData = productConfig.firareas;

  const optionsFIR = Object.keys(firData).map((fir) => ({
    ATSR: firData[fir].location_indicator_atsr,
    FIR: firData[fir].firname,
  }));

  const allowedUnits = Object.keys(firData).reduce((list, key) => {
    if (firData[key].units) {
      return {
        ...list,
        [key]: firData[key].units,
      };
    }
    return list;
  }, {});

  const allFIRS = productConfig.firareas;

  return {
    defaultATSR: optionsFIR[0].ATSR,
    optionsFIR,
    allFIRS,
    allowedUnits,
  };
};

export const getLevelInFeet = (level: number, unit: string): number => {
  if (LevelUnits[unit] === 'm') return level / 0.3048;
  if (LevelUnits[unit] === 'FL') return level * 100;
  return level;
};

export const isLevelLower = (
  ownValue: number | string,
  ownUnit: LevelUnits,
  otherValue: number,
  otherUnit: LevelUnits,
): boolean => {
  if (typeof ownValue === 'string' && !ownValue.length) return true;
  if (ownUnit === otherUnit) return ownValue < otherValue;

  // not same units - convert both to feet and compare
  const ownValueFeet = getLevelInFeet(ownValue as number, ownUnit);
  const otherValueFeet = getLevelInFeet(otherValue, otherUnit);

  return ownValueFeet < otherValueFeet;
};

export const getDialogtitle = (
  product: Sigmet | CancelSigmet | Airmet | CancelAirmet,
  productType: ProductType,
): string => {
  const productName = productType.toUpperCase();

  if (product === null) {
    return `New ${productName}`;
  }
  if (!isInstanceOfCancelSigmetOrAirmet(product)) {
    const phenomenon =
      productType === 'sigmet'
        ? SigmetPhenomena[product.phenomenon]
        : AirmetPhenomena[product.phenomenon];
    return `${productName} ${phenomenon} - ${
      product.status === 'DRAFT'
        ? 'saved as draft'
        : ProductStatusDescription[product.status]
    }`;
  }
  if (isInstanceOfCancelSigmet(product)) {
    return `${productName} Cancels ${product.cancelsSigmetSequenceId}`;
  }
  return `${productName} Cancels ${product.cancelsAirmetSequenceId}`;
};

export const getProductFormMode = (
  canBeOptions: ProductCanbe[],
  productListItem: SigmetFromBackend | AirmetFromBackend,
): string => {
  if (productListItem === null) return 'new';

  return canBeOptions.includes('PUBLISHED') ? 'edit' : 'view';
};

export const getProductFormLifecycleButtons = (
  canBe: string[],
  onButtonPress: (newStatus: ProductStatus | 'DISCARDED') => void,
): React.ReactElement => {
  return (
    <Grid container spacing={1} justifyContent="flex-end">
      {canBe.includes('DISCARDED') && (
        <Grid item>
          <Button
            color="secondary"
            onClick={(): void => {
              onButtonPress('DISCARDED');
            }}
            data-testid="productform-dialog-discard"
          >
            Discard
          </Button>
        </Grid>
      )}
      {canBe.includes('DRAFTED') && (
        <Grid item>
          <Button
            color="secondary"
            variant="outlined"
            onClick={(): void => {
              onButtonPress('DRAFT');
            }}
            data-testid="productform-dialog-draft"
          >
            Save
          </Button>
        </Grid>
      )}
      {canBe.includes('CANCELLED') && (
        <Grid item>
          <Button
            color="secondary"
            variant="contained"
            onClick={(): void => {
              onButtonPress('CANCELLED');
            }}
            data-testid="productform-dialog-cancel"
          >
            Cancel
          </Button>
        </Grid>
      )}
      {canBe.includes('PUBLISHED') && (
        <Grid item>
          <Button
            color="secondary"
            variant="contained"
            onClick={(): void => {
              onButtonPress('PUBLISHED');
            }}
            data-testid="productform-dialog-publish"
          >
            Publish
          </Button>
        </Grid>
      )}
    </Grid>
  );
};

type Product = Sigmet | Airmet | CancelSigmet | CancelAirmet;
export const hasValue = (value: number): boolean =>
  value !== null && value.toString() !== 'NaN';

export const removeEmptyNumberFieldValues = <T extends Product>(
  product: T,
): T =>
  Object.keys(product).reduce((list, key) => {
    const value = product[key];
    // check if nested object has value
    if (typeof value === 'object' && value !== null && value !== undefined) {
      const hasNaNValue = Object.keys(value).reduce(
        (total, valKey) => !hasValue(value[valKey]) || total,
        false,
      );

      return {
        ...list,
        ...(!hasNaNValue && {
          [key]: value,
        }),
      };
    }

    return {
      ...list,
      ...(hasValue(value) && {
        [key]: value,
      }),
    };
  }, {} as T);

export const createSigmetPostParameter = (
  sigmetToPost: Sigmet | CancelSigmet,
  newStatus: ProductStatus,
  previousUuid: string,
): SigmetFromFrontend => {
  const sigmetIncludingUuid =
    previousUuid !== null
      ? { ...sigmetToPost, uuid: previousUuid }
      : sigmetToPost;

  // Do not send empty numbered fields to the BE to prevent errors
  const sigmet = removeEmptyNumberFieldValues(sigmetIncludingUuid);

  return {
    changeStatusTo: newStatus,
    sigmet,
  };
};

export const createAirmetPostParameter = (
  airmetToPost: Airmet | CancelAirmet,
  newStatus: ProductStatus,
  previousUuid: string,
): AirmetFromFrontend => {
  const airmetIncludingUuid =
    previousUuid !== null
      ? { ...airmetToPost, uuid: previousUuid }
      : airmetToPost;

  // Do not send empty numbered fields to the BE to prevent errors
  const airmet = removeEmptyNumberFieldValues(airmetIncludingUuid);

  return {
    changeStatusTo: newStatus,
    airmet,
  };
};

export const useDelayedFormValues = (
  getValues: () => void,
  register: () => void,
  delay = 1000,
): [unknown, () => void] => {
  const { isMounted } = useIsMounted();
  const formValueTimer = React.useRef(null);
  const [formValues, setFormValues] = React.useState(getValues());

  React.useEffect(() => {
    if (!isMounted.current) return;
    setFormValues(getValues());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [register, getValues]);

  const updateFormValueTimer = (): void => {
    if (!isMounted.current) return;
    setFormValues(getValues());
  };

  const onChangeForm = (): void => {
    clearTimeout(formValueTimer.current);
    formValueTimer.current = setTimeout(updateFormValueTimer, delay);
  };

  return [formValues, onChangeForm];
};

export const getConfirmationDialogTitle = (
  action: ProductActions,
  productType: ProductType,
): string => {
  const product = productType.toUpperCase();
  if (action === 'CLOSED') return `Close ${product}`;
  if (action === 'DISCARDED') return `Discard ${product}`;
  if (action === 'PUBLISHED') return 'Publish';
  return `Cancel ${product}`;
};

export const getConfirmationDialogContent = (
  action: ProductActions,
  productType: ProductType,
): string => {
  const product = productType.toUpperCase();
  if (action === 'DISCARDED')
    return `Are you sure you would like to discard this ${product}? Its properties will be lost`;
  if (action === 'CLOSED') return 'Do you want to save any changes made?';
  if (action === 'PUBLISHED')
    return `Are you sure you want to publish this ${product}?`;
  return `Are you sure you want to cancel this ${product}?`;
};

export const getConfirmationDialogButtonLabel = (
  action: ProductActions,
  productType: ProductType,
): string => {
  const product = productType.toUpperCase();
  switch (action) {
    case 'DISCARDED':
      return `Discard ${product}`;
    case 'PUBLISHED':
      return 'Publish';
    case 'CANCELLED':
      return 'Cancel';
    default:
      return 'Save and close';
  }
};

export const getConfirmationDialogCancelLabel = (
  action: ProductActions,
): string => {
  switch (action) {
    case 'CLOSED':
      return 'Discard and close';
    default:
      return undefined;
  }
};

export const getProductIssueDate = (
  initialProduct: Sigmet | Airmet,
  initialCancelProduct: CancelSigmet | CancelAirmet,
): string => {
  if (
    initialProduct !== null &&
    (initialProduct.status === 'PUBLISHED' ||
      initialProduct.status === 'EXPIRED') &&
    initialProduct.issueDate
  ) {
    return `${moment
      .utc(initialProduct.issueDate)
      .format('YYYY/MM/DD HH:mm')} UTC`;
  }

  if (initialCancelProduct !== null && initialCancelProduct.issueDate) {
    return `${moment
      .utc(initialCancelProduct.issueDate)
      .format('YYYY/MM/DD HH:mm')} UTC`;
  }
  return '(Not published)';
};
const extractUnitsToShow = (
  allowedUnits: string[],
  unitType: Record<string, string>,
): Record<string, string> => {
  return Object.keys(allowedUnits).reduce((list, key) => {
    const unit = allowedUnits[key];
    if (unitType[unit]) {
      return {
        ...list,
        [unit]: unitType[unit],
      };
    }
    return list;
  }, {});
};

export const getAllowedUnits = (
  selectedFIR: string,
  allowedUnits: FirAllowedUnits,
  config_type: string,
  unitType: Record<string, string>,
): Record<string, string> => {
  if (selectedFIR !== undefined && allowedUnits[selectedFIR]) {
    // For selected FIR, find the corresponding unit_type. If not specified in the config, return as defined in type
    const allowedMovementUnits = allowedUnits[selectedFIR].find(
      (unitConfig) => unitConfig.unit_type === config_type,
    );
    return allowedMovementUnits
      ? extractUnitsToShow(allowedMovementUnits.allowed_units, unitType)
      : unitType;
  }
  // If no FIR selected - return all units as defined in the type
  return unitType;
};

export const rewindGeometry = (
  geoJSON: FeatureCollection,
): FeatureCollection => {
  return produce(geoJSON, (geoJSONDraft) => {
    if (
      geoJSONDraft &&
      geoJSONDraft.features[0].geometry.type === 'Polygon' &&
      geoJSONDraft.features[0].geometry.coordinates[0].length > 1
    ) {
      const { geometry } = geoJSON.features[0] as Feature<Polygon>;
      const lineString = polygonToLineString(geometry);
      if (booleanClockwise(lineString.geometry.coordinates)) {
        // eslint-disable-next-line no-param-reassign
        geoJSONDraft.features[0].geometry = rewind(
          geoJSONDraft.features[0].geometry,
        );
      }
    }
  });
};
