/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';

import { render, waitFor } from '@testing-library/react';
import { renderHook, act } from '@testing-library/react-hooks';

import ProductFormTAC, {
  noTAC,
  shouldRetrieveTAC,
  useTAC,
} from './ProductFormTac';
import { fakeSigmetList } from '../../utils/mockdata/fakeSigmetList';
import { TestWrapper } from '../../utils/testUtils';
import { Airmet, Sigmet } from '../../types';
import {
  fakeAirmetList,
  fakeCancelAirmet,
} from '../../utils/mockdata/fakeAirmetList';

jest.mock('../../utils/api');

describe('components/ProductForms/ProductFormTac', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('shows the returned TAC once TAC is returned from BE', async () => {
    const props = {
      tac: noTAC,
    };

    const { getByTestId } = render(
      <TestWrapper>
        <ProductFormTAC {...props} />
      </TestWrapper>,
    );
    expect(getByTestId('productform-tac-message').textContent).toEqual(
      'Missing data: no TAC can be generated',
    );
  });

  it('should retreive TAC and refetch TAC', async () => {
    const testSigmet = fakeSigmetList[0].sigmet as Sigmet;
    // test with fakeRequest; mimic the backend with only returning phenomenon
    const fakeTACRequest = (data: Sigmet): Promise<{ data: string }> =>
      new Promise((resolve) => resolve({ data: data.phenomenon }));

    await act(async () => {
      const { result } = renderHook(() => useTAC(testSigmet, fakeTACRequest));

      await waitFor(() => expect(result.current[0]).toEqual(noTAC));

      // // update
      const newResponse = 'test';

      await result.current[1]({ ...testSigmet, phenomenon: newResponse });
      // fix for lodash debounce test problem https://github.com/facebook/jest/issues/3465
      jest.runOnlyPendingTimers();
      setTimeout(() => {
        expect(result.current[0]).toEqual(newResponse);
      }, 1000);
    });
  });

  it('should not retreive TAC when missing values in Sigmet', async () => {
    const { validDateStart, phenomenon, ...testSigmet } = fakeSigmetList[0]
      .sigmet as Sigmet;

    // test with fakeRequest; mimic the backend with only returning phenomenon
    const fakeTACRequest = (data: Sigmet): Promise<{ data: string }> =>
      new Promise((resolve) => resolve({ data: data.phenomenon }));

    await act(async () => {
      const { result } = await renderHook(() =>
        useTAC(testSigmet as Sigmet, fakeTACRequest),
      );

      await waitFor(() => expect(result.current[0]).toEqual(noTAC));
      // update with invalid value
      const newResponse = null;
      await result.current[1]({ ...testSigmet, phenomenon: newResponse });

      await waitFor(() => expect(result.current[0]).toEqual(noTAC));
    });
  });

  it('should show error message when error occurs', async () => {
    const testSigmet = fakeSigmetList[0].sigmet as Sigmet;

    const fakeTACRequest = (): Promise<{ data: string }> =>
      new Promise((_, reject) => reject(new Error('server error')));

    await act(async () => {
      const { result } = renderHook(() =>
        useTAC(testSigmet as Sigmet, fakeTACRequest),
      );

      await waitFor(() => expect(result.current[0]).toEqual(noTAC));
    });
  });
  describe('components/ProductForms/ProductFormTac', () => {
    it('should return true always for a cancel sigmet', async () => {
      expect(shouldRetrieveTAC(fakeCancelAirmet)).toBe(true);
      expect(shouldRetrieveTAC(fakeSigmetList[3].sigmet)).toBe(true);
    });
    // Minimally required: phenomenon, isObservationOrForecast, validDateStart, validDateEnd, startGeometry, startGeometryIntersect, movementType, change
    it('should return true for sigmet/airmet if all minimal required fields are present', async () => {
      expect(shouldRetrieveTAC(fakeAirmetList[0].airmet)).toBe(true);
      expect(shouldRetrieveTAC(fakeSigmetList[0].sigmet)).toBe(true);
      expect(shouldRetrieveTAC(fakeAirmetList[1].airmet)).toBe(true);
      expect(shouldRetrieveTAC(fakeSigmetList[1].sigmet)).toBe(true);
      expect(shouldRetrieveTAC(fakeAirmetList[4].airmet)).toBe(true);
      expect(shouldRetrieveTAC(fakeSigmetList[4].sigmet)).toBe(true);
      const {
        phenomenon: unusedPhenom,
        ...incompleteAirmet
      } = fakeAirmetList[0].airmet as Airmet;
      expect(shouldRetrieveTAC(incompleteAirmet as Airmet)).toBe(false);
      const { validDateStart, ...incompleteSigmet } = fakeSigmetList[0]
        .sigmet as Sigmet;
      expect(shouldRetrieveTAC(incompleteSigmet as Sigmet)).toBe(false);
      const {
        isObservationOrForecast,
        ...incompleteAirmet2
      } = fakeAirmetList[1].airmet as Airmet;
      expect(shouldRetrieveTAC(incompleteAirmet2 as Airmet)).toBe(false);
      const { validDateEnd, ...incompleteSigmet2 } = fakeSigmetList[1]
        .sigmet as Sigmet;
      expect(shouldRetrieveTAC(incompleteSigmet2 as Sigmet)).toBe(false);
      const { change, ...incompleteAirmet3 } = fakeAirmetList[1]
        .airmet as Airmet;
      expect(shouldRetrieveTAC(incompleteAirmet3 as Airmet)).toBe(false);
      const { movementType, ...incompleteSigmet3 } = fakeSigmetList[1]
        .sigmet as Sigmet;
      expect(shouldRetrieveTAC(incompleteSigmet3 as Sigmet)).toBe(false);
    });
  });
});
