/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

export const styles = {
  productForm: {
    overflowY: 'scroll',
    overflowX: 'hidden',
    height: '100%',
    width: '100%',
  },
  label: {
    paddingTop: '15px',
  },
  body: {
    paddingTop: '17px',
  },
  containerItem: {
    paddingTop: '8px',
  },
  volcanicField: {
    marginBottom: 2,
  },
  surfaceVisibility: {
    marginBottom: 2,
  },
  surfaceWind: {
    marginBottom: 2,
  },
  levelField: {
    marginBottom: 2,
  },
  movement: {
    marginBottom: 2,
  },
  latitude: {
    width: '97%',
  },
  unit: {
    width: '95%',
  },
  quitDrawModeMessage: { paddingBottom: '10px', opacity: '1' },
  drawSection: { padding: '10px 0px' },
};
