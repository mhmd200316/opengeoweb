/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import moment from 'moment';
import {
  ProductCanbe,
  FormMode,
  SigmetFromBackend,
  ProductType,
  AirmetFromBackend,
  CancelSigmet,
  CancelAirmet,
  Sigmet,
  Airmet,
  isInstanceOfCancelSigmetOrAirmet,
} from '../../types';
import { ProductFormDialogContent } from './ProductFormDialogContent';
import { getProductFormMode } from './utils';
import { VALID_FROM_TIME_DELAY_IN_MINUTES } from './ProductFormFields/ValidFrom';
import { getDefaultHoursValid } from './ProductFormFields/ValidUntil';

export const prepareProductValues = (
  product: Sigmet | Airmet | CancelSigmet | CancelAirmet,
  productType: ProductType,
): Sigmet | Airmet | CancelSigmet | CancelAirmet => {
  // Ensure there is a valid start/end date
  if (
    !isInstanceOfCancelSigmetOrAirmet(product) &&
    (!product.validDateStart || !product.validDateEnd)
  ) {
    const defaultHoursValid = getDefaultHoursValid(productType);
    return {
      validDateStart: moment
        .utc()
        .add(VALID_FROM_TIME_DELAY_IN_MINUTES, 'minutes')
        .format('YYYY-MM-DDTHH:mm:ss[Z]'),
      validDateEnd: moment
        .utc()
        .add(defaultHoursValid, 'hour')
        .add(VALID_FROM_TIME_DELAY_IN_MINUTES, 'minutes')
        .format('YYYY-MM-DDTHH:mm:ss[Z]'),
      ...product,
    };
  }
  return product;
};

interface ProductFormDialogProps {
  isOpen: boolean;
  productType: ProductType;
  toggleDialogStatus: () => void;
  productListItem?: SigmetFromBackend | AirmetFromBackend;
}

export const ProductFormDialog: React.FC<ProductFormDialogProps> = ({
  isOpen,
  productType,
  toggleDialogStatus,
  productListItem = null,
}: ProductFormDialogProps) => {
  // Show the right options for a new sigmet/airmet not coming from the BE
  const productCanBe =
    productListItem !== null
      ? productListItem.canbe
      : (['DRAFTED', 'DISCARDED', 'PUBLISHED'] as ProductCanbe[]);

  // Determine mode of form based on canbe properties
  const mode = getProductFormMode(productCanBe, productListItem) as FormMode;

  if (!isOpen) {
    return null;
  }

  return (
    <ReactHookFormProvider options={defaultFormOptions}>
      <ConfirmationServiceProvider>
        <ProductFormDialogContent
          isOpen={isOpen}
          toggleDialogStatus={toggleDialogStatus}
          mode={mode}
          productCanBe={productCanBe}
          product={
            productListItem !== null
              ? prepareProductValues(productListItem[productType], productType)
              : null
          }
          productType={productType}
        />
      </ConfirmationServiceProvider>
    </ReactHookFormProvider>
  );
};
