/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';
import * as React from 'react';
import {
  CancelSigmet,
  LevelUnits,
  Sigmet,
  SigmetPhenomena,
} from '../../../types';
import { getFir } from '../../../utils/getFir';
import {
  fakeSigmetList,
  fakeVolcanicCancelSigmetWithMoveTo,
} from '../../../utils/mockdata/fakeSigmetList';

import {
  SnapshotStoryWrapper,
  StoryWrapperFakeApi,
} from '../../../utils/testUtils';
import SigmetForm from './SigmetForm';

export default {
  title: 'components/SigmetForm',
};

export const NewSigmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <StoryWrapperFakeApi>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              validDateStart: '2022-01-01T12:00Z',
              validDateEnd: '2022-01-01T13:00Z',
            },
          }}
        >
          <SigmetForm mode="new" showMap={false} />
        </ReactHookFormProvider>
      </StoryWrapperFakeApi>
    </SnapshotStoryWrapper>
  );
};

NewSigmet.storyName = 'New Sigmet (takeSnapshot)';

const VASigmet = {
  uuid: 'someuniqueidprescibedbyBE3',
  phenomenon: 'VA_CLD' as SigmetPhenomena,
  sequence: 'A02V',
  issueDate: '2022-01-02T12:35Z',
  validDateStart: '2022-01-02T11:35Z',
  validDateEnd: '2022-01-02T12:35Z',
  firName: 'AMSTERDAM FIR',
  locationIndicatorATSU: 'EHAA',
  locationIndicatorATSR: 'EHAA',
  locationIndicatorMWO: 'EHDB',
  isObservationOrForecast: 'OBS',
  observationOrForecastTime: '2022-01-02T12:35Z',
  movementType: 'NO_VA_EXP',
  change: 'WKN',
  type: 'NORMAL',
  status: 'PUBLISHED',
  levelInfoMode: 'BETW',
  vaSigmetVolcanoName: 'EYJAFJALLAJOKULL',
  vaSigmetVolcanoCoordinates: { latitude: 63.62, longitude: -19.61 },
  level: {
    value: 1500,
    unit: 'FT' as LevelUnits,
  },
  lowerLevel: {
    value: 1000,
    unit: 'FT' as LevelUnits,
  },
  firGeometry: getFir(),
  startGeometry: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {
          selectionType: 'poly',
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [3.7513346922388284, 52.93209131750574],
              [4.890169687103436, 52.26807619123409],
              [3.250247294498403, 51.960365357854286],
              [3.7513346922388284, 52.93209131750574],
            ],
          ],
        },
      },
    ],
  },
  startGeometryIntersect: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {
          selectionType: 'poly',
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [3.7513346922388284, 52.93209131750574],
              [4.890169687103436, 52.26807619123409],
              [3.250247294498403, 51.960365357854286],
              [3.7513346922388284, 52.93209131750574],
            ],
          ],
        },
      },
    ],
  },
};

export const EditSigmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <StoryWrapperFakeApi>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...VASigmet,
            },
          }}
        >
          <SigmetForm
            mode="edit"
            showMap={false}
            initialSigmet={VASigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </StoryWrapperFakeApi>
    </SnapshotStoryWrapper>
  );
};

EditSigmet.storyName = 'Edit Sigmet (takeSnapshot)';

export const ViewSigmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <StoryWrapperFakeApi>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[7].sigmet,
            },
          }}
        >
          <SigmetForm
            mode="view"
            showMap={false}
            initialSigmet={fakeSigmetList[7].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </StoryWrapperFakeApi>
    </SnapshotStoryWrapper>
  );
};

ViewSigmet.storyName = 'View Sigmet (takeSnapshot)';

export const CancelVASigmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <StoryWrapperFakeApi>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeVolcanicCancelSigmetWithMoveTo.sigmet,
            },
          }}
        >
          <SigmetForm
            mode="view"
            showMap={false}
            isCancelSigmet={true}
            initialCancelSigmet={
              fakeVolcanicCancelSigmetWithMoveTo.sigmet as CancelSigmet
            }
          />
        </ReactHookFormProvider>
      </StoryWrapperFakeApi>
    </SnapshotStoryWrapper>
  );
};

CancelVASigmet.storyName = 'Cancel Sigmet (takeSnapshot)';
