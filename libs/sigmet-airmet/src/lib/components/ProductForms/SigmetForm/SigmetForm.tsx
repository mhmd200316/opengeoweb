/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, Typography } from '@mui/material';
import { useFormContext } from 'react-hook-form';

import {
  ReactHookFormHiddenInput,
  isGeometryDirty,
} from '@opengeoweb/form-fields';
import { useApiContext } from '@opengeoweb/api';
import { produce } from 'immer';
import { MapViewGeoJson } from '../../MapViewGeoJson';
import {
  CancelSigmet,
  MapDrawMode,
  MapGeoJSONS,
  Sigmet,
  FormMode,
  StartOrEndDrawing,
} from '../../../types';
import { createInterSections, useDrawMode } from '../../MapViewGeoJson/utils';
import { styles } from '../ProductForm.styles';
import {
  Phenomenon,
  VolcanicFields,
  ObservationForecast,
  Change,
  Progress,
  Type,
  SelectFIR,
  ObservationForecastTime,
  ValidFrom,
  ValidUntil,
  StartGeometry,
  Levels,
} from '../ProductFormFields';
import ProductFormTAC, { useTAC } from '../ProductFormTac';
import { getProductIssueDate, rewindGeometry } from '../utils';
import { SigmetAirmetApi } from '../../../utils/api';

export interface SigmetFormProps {
  mode: FormMode;
  isCancelSigmet?: boolean;
  initialSigmet?: Sigmet;
  initialCancelSigmet?: CancelSigmet;
  showMap?: boolean;
}

const SigmetForm: React.FC<SigmetFormProps> = ({
  mode,
  isCancelSigmet = false,
  initialSigmet = null,
  initialCancelSigmet = null,
  showMap = true,
}: SigmetFormProps) => {
  const { api } = useApiContext<SigmetAirmetApi>();
  const { watch, setValue, handleSubmit, getValues } = useFormContext();
  const [isDisabled, setIsDisabled] = React.useState(mode === 'view');
  const isReadOnly = mode === 'view';
  const helperText = isDisabled ? '' : 'Optional';
  const [tac, updateTac] = useTAC(
    initialSigmet !== null ? initialSigmet : initialCancelSigmet,
    api.getSigmetTAC,
  );
  const onChangeForm = (): void => {
    updateTac(getValues());
  };
  // TODO: [Loes Cornelis]: fix this whole mess of if statements for isCancelSigmet and hidden fields if disabled throughout the form
  const initialGeometry =
    initialSigmet && !isCancelSigmet
      ? {
          start: initialSigmet.startGeometry,
          end: initialSigmet.endGeometry,
          intersectionStart: initialSigmet.startGeometryIntersect,
          intersectionEnd: initialSigmet.endGeometryIntersect,
        }
      : null;
  const {
    drawMode,
    setDrawMode,
    layerDrawModes,
    geoJSONs,
    setLayerGeoJSONs,
    setDrawModeType,
  } = useDrawMode(initialGeometry);

  const updateStartDrawing = (mapGeoJSONS?: MapGeoJSONS): void => {
    const baseGeoJSON = mapGeoJSONS || geoJSONs;
    const newGeoJSON = produce(baseGeoJSON, (draftGeoJSON) => {
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.start = rewindGeometry(baseGeoJSON.start);
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.intersectionStart = rewindGeometry(
        baseGeoJSON.intersectionStart,
      );
    });

    setValue('startGeometry', newGeoJSON.start, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(watch('startGeometry'), newGeoJSON.start),
    });
    setValue('startGeometryIntersect', newGeoJSON.intersectionStart, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(
        watch('startGeometryIntersect'),
        newGeoJSON.intersectionStart,
      ),
    });
    onChangeForm();
  };

  const updateEndDrawing = (mapGeoJSONS?: MapGeoJSONS): void => {
    const baseGeoJSON = mapGeoJSONS || geoJSONs;
    const newGeoJSON = produce(baseGeoJSON, (draftGeoJSON) => {
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.end = rewindGeometry(baseGeoJSON.end);
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.intersectionEnd = rewindGeometry(
        baseGeoJSON.intersectionEnd,
      );
    });

    setValue('endGeometry', newGeoJSON.end, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(watch('endGeometry'), newGeoJSON.end),
    });

    setValue('endGeometryIntersect', newGeoJSON.intersectionEnd, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(
        watch('endGeometryIntersect'),
        newGeoJSON.intersectionEnd,
      ),
    });
    onChangeForm();
  };

  React.useEffect(() => {
    const isInDrawMode = drawMode.start !== null || drawMode.end !== null;
    if (!isReadOnly && isInDrawMode !== isDisabled) {
      setIsDisabled(isInDrawMode);
    }
  }, [drawMode.start, drawMode.end, isReadOnly, isDisabled]);

  const renderForm = (): React.ReactElement => (
    <>
      <Grid container direction="column" spacing={2}>
        {/* Phenomenon */}
        {!isCancelSigmet && (
          <Phenomenon
            productType="sigmet"
            isDisabled={isDisabled}
            onChange={onChangeForm}
          />
        )}
        {/* Observed/Forecast */}
        {!isCancelSigmet && (
          <ObservationForecast
            isDisabled={isDisabled}
            isReadOnly={isReadOnly}
          />
        )}
        {/* At */}
        {!isCancelSigmet && (
          <ObservationForecastTime
            isDisabled={isDisabled}
            isReadOnly={isReadOnly}
            helperText={helperText}
            onChange={onChangeForm}
          />
        )}
        {/* Volcanic fields */}
        {!isCancelSigmet && watch('phenomenon') === 'VA_CLD' && (
          <VolcanicFields
            isDisabled={isDisabled}
            isReadOnly={isReadOnly}
            helperText={helperText}
          />
        )}
        {/* Valid from */}
        <ValidFrom
          productType="sigmet"
          isDisabled={isDisabled}
          onChange={onChangeForm}
        />
        {/* Valid until */}
        <ValidUntil
          productType="sigmet"
          isDisabled={isDisabled}
          onChange={onChangeForm}
        />
      </Grid>
      {/* Where */}
      <SelectFIR productType="sigmet" isDisabled={isDisabled} />
      {/* Draw */}
      <StartGeometry
        isReadOnly={isReadOnly}
        drawMode={drawMode}
        geoJSONs={geoJSONs}
        setDrawModeType={(
          mapDrawMode: MapDrawMode,
          StartOrEndDrawing: StartOrEndDrawing,
        ): void => {
          setDrawModeType(mapDrawMode, StartOrEndDrawing, updateStartDrawing);
        }}
      />
      {/* Levels */}
      {!isCancelSigmet && (
        <Levels
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          onChange={onChangeForm}
          productType="sigmet"
        />
      )}
      {/* Progress */}
      {!isCancelSigmet && (
        <Progress
          productType="sigmet"
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          drawMode={drawMode}
          geoJSONs={geoJSONs}
          setDrawModeType={(
            mapDrawMode: MapDrawMode,
            StartOrEndDrawing: StartOrEndDrawing,
          ): void => {
            setDrawModeType(mapDrawMode, StartOrEndDrawing, updateEndDrawing);
          }}
          onChange={onChangeForm}
        />
      )}
      {/* Change */}
      {!isCancelSigmet && (
        <Change isDisabled={isDisabled} isReadOnly={isReadOnly} />
      )}
      {/* Volcanic ash cloud moving to: */}
      {isCancelSigmet && watch('vaSigmetMoveToFIR') && (
        <Grid item container spacing={2}>
          <Grid item xs={4} container justifyContent="flex-end" />
          <Grid item xs={8} container justifyContent="flex-start">
            <Grid item xs={12}>
              <Typography
                variant="body2"
                sx={styles.body}
                data-testid="vaSigmetMoveToFIR"
              >
                Volcanic Cloud is moving in the direction of:{' '}
                <b> {watch('vaSigmetMoveToFIR')}</b>
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      )}
      {/* Issued at */}
      <Grid item container spacing={2} sx={styles.containerItem}>
        <Grid item xs={4} container justifyContent="flex-end">
          <Typography variant="subtitle1" sx={styles.label}>
            Issued at
          </Typography>
        </Grid>
        <Grid item xs={8} container justifyContent="flex-start">
          <Grid item xs={12}>
            <Typography variant="body2" sx={styles.body}>
              {getProductIssueDate(initialSigmet, initialCancelSigmet)}
            </Typography>
          </Grid>
        </Grid>
        {/* TAC */}
        <Grid item container spacing={2}>
          <Grid item xs={4} container justifyContent="flex-end">
            <Typography variant="subtitle1" sx={styles.label}>
              TAC
            </Typography>
          </Grid>
          <Grid item xs={8} container justifyContent="flex-start">
            <Grid item xs={12}>
              <ProductFormTAC tac={tac} />
            </Grid>
          </Grid>
        </Grid>
        {/* Hidden sequence number field needed for the TAC generation */}
        <ReactHookFormHiddenInput name="sequence" defaultValue="-1" />
        {
          /* Add hidden fields for TAC generation in case of cancel sigmet */
          isCancelSigmet && (
            <>
              <ReactHookFormHiddenInput name="cancelsSigmetSequenceId" />
              <ReactHookFormHiddenInput name="validDateEndOfSigmetToCancel" />
              <ReactHookFormHiddenInput name="validDateStartOfSigmetToCancel" />
            </>
          )
        }
      </Grid>
      {/* Type */}
      <Type isDisabled={isDisabled} isReadOnly={isReadOnly} />
    </>
  );
  if (!showMap) {
    return renderForm();
  }

  return (
    <form
      onChange={onChangeForm}
      onSubmit={handleSubmit(() => null)}
      style={{ width: '100%' }}
    >
      <Grid container style={{ height: '100%' }}>
        <Grid item xs={8}>
          <MapViewGeoJson
            geoJSONs={geoJSONs}
            setLayerGeoJSONs={(geoJSONs): void =>
              setLayerGeoJSONs(createInterSections(geoJSONs))
            }
            layerDrawModes={layerDrawModes}
            drawMode={drawMode}
            setDrawMode={setDrawMode}
            exitDrawModeCallbackStart={updateStartDrawing}
            exitDrawModeCallbackEnd={updateEndDrawing}
          />
        </Grid>
        <Grid item xs={4} sx={styles.productForm}>
          {renderForm()}
        </Grid>
      </Grid>
    </form>
  );
};

export default SigmetForm;
