/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';

import {
  StoryWrapperFakeApi,
  StoryWrapperFakeApiWithErrors,
} from '../../utils/testUtils';
import { ProductFormDialog } from './ProductFormDialog';

export default {
  title: 'components/ProductFormDialog/Airmet',
};

export const NewAirmet = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={null}
        productType="airmet"
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const ViewAirmet = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeAirmetList[2]}
        productType="airmet"
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const ViewAirmetSurfaceVisibility = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeAirmetList[1]}
        productType="airmet"
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const ViewAirmetSurfaceWind = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeAirmetList[5]}
        productType="airmet"
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const ViewBrokenCloud = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeAirmetList[7]}
        productType="airmet"
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const ViewOvercastCloud = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeAirmetList[4]}
        productType="airmet"
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const EditAirmet = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeAirmetList[0]}
        productType="airmet"
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const CancelAirmet = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApi>
      <ProductFormDialog
        productListItem={fakeAirmetList[3]}
        productType="airmet"
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApi>
  );
};

export const BackendErrorMsgOnSaveOrPublish = (): React.ReactElement => {
  return (
    <StoryWrapperFakeApiWithErrors>
      <ProductFormDialog
        productListItem={fakeAirmetList[0]}
        productType="airmet"
        isOpen
        toggleDialogStatus={(): void => {}}
      />
    </StoryWrapperFakeApiWithErrors>
  );
};
