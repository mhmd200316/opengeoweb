/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, Typography } from '@mui/material';
import { useFormContext } from 'react-hook-form';

import {
  ReactHookFormHiddenInput,
  isGeometryDirty,
} from '@opengeoweb/form-fields';
import { useApiContext } from '@opengeoweb/api';
import { produce } from 'immer';
import { MapViewGeoJson } from '../../MapViewGeoJson';
import {
  MapDrawMode,
  MapGeoJSONS,
  FormMode,
  StartOrEndDrawing,
  Airmet,
  CancelAirmet,
} from '../../../types';
import { createInterSections, useDrawMode } from '../../MapViewGeoJson/utils';
import { styles } from '../ProductForm.styles';
import {
  Phenomenon,
  ObservationForecast,
  Change,
  Progress,
  Type,
  SelectFIR,
  ObservationForecastTime,
  ValidFrom,
  ValidUntil,
  StartGeometry,
  Levels,
  SurfaceVisibility,
  SurfaceWind,
  CloudLevels,
} from '../ProductFormFields';
import ProductFormTAC, { useTAC } from '../ProductFormTac';
import { getProductIssueDate, rewindGeometry } from '../utils';
import { SigmetAirmetApi } from '../../../utils/api';

export interface AirmetFormProps {
  mode: FormMode;
  isCancelAirmet?: boolean;
  initialAirmet?: Airmet;
  initialCancelAirmet?: CancelAirmet;
  showMap?: boolean;
}

const AirmetForm: React.FC<AirmetFormProps> = ({
  mode,
  isCancelAirmet = false,
  initialAirmet = null,
  initialCancelAirmet = null,
  showMap = true,
}: AirmetFormProps) => {
  const { api } = useApiContext<SigmetAirmetApi>();
  const { watch, setValue, handleSubmit, getValues } = useFormContext();

  const phenomenon = watch('phenomenon');
  const [isDisabled, setIsDisabled] = React.useState(mode === 'view');
  const isReadOnly = mode === 'view';
  const helperText = isDisabled ? '' : 'Optional';
  const [tac, updateTac] = useTAC(
    initialAirmet !== null ? initialAirmet : initialCancelAirmet,
    api.getAirmetTAC,
  );
  const onChangeForm = (): void => {
    updateTac(getValues());
  };

  const initialGeometry =
    initialAirmet && !isCancelAirmet
      ? {
          start: initialAirmet.startGeometry,
          intersectionStart: initialAirmet.startGeometryIntersect,
        }
      : null;
  const {
    drawMode,
    setDrawMode,
    layerDrawModes,
    geoJSONs,
    setLayerGeoJSONs,
    setDrawModeType,
  } = useDrawMode(initialGeometry);

  const updateStartDrawing = (mapGeoJSONS?: MapGeoJSONS): void => {
    const baseGeoJSON = mapGeoJSONS || geoJSONs;
    const newGeoJSON = produce(baseGeoJSON, (draftGeoJSON) => {
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.start = rewindGeometry(baseGeoJSON.start);
      // eslint-disable-next-line no-param-reassign
      draftGeoJSON.intersectionStart = rewindGeometry(
        baseGeoJSON.intersectionStart,
      );
    });

    setValue('startGeometry', newGeoJSON.start, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(watch('startGeometry'), newGeoJSON.start),
    });
    setValue('startGeometryIntersect', newGeoJSON.intersectionStart, {
      shouldValidate: true,
      shouldDirty: isGeometryDirty(
        watch('startGeometryIntersect'),
        newGeoJSON.intersectionStart,
      ),
    });
    onChangeForm();
  };

  React.useEffect(() => {
    const isInDrawMode = drawMode.start !== null;
    if (!isReadOnly && isInDrawMode !== isDisabled) {
      setIsDisabled(isInDrawMode);
    }
  }, [drawMode.start, isReadOnly, isDisabled]);

  const renderForm = (): React.ReactElement => (
    <>
      <Grid container direction="column" spacing={2}>
        {/* Phenomenon */}
        {!isCancelAirmet && (
          <Phenomenon
            productType="airmet"
            isDisabled={isDisabled}
            onChange={onChangeForm}
          />
        )}
        {/* Surface Visibility */}
        {!isCancelAirmet && phenomenon === 'SFC_VIS' && (
          <SurfaceVisibility isDisabled={isDisabled} onChange={onChangeForm} />
        )}
        {/* Surface Wind */}
        {!isCancelAirmet && phenomenon === 'SFC_WIND' && (
          <SurfaceWind isDisabled={isDisabled} onChange={onChangeForm} />
        )}
        {/* Observed/Forecast */}
        {!isCancelAirmet && (
          <ObservationForecast
            isDisabled={isDisabled}
            isReadOnly={isReadOnly}
          />
        )}
        {/* At */}
        {!isCancelAirmet && (
          <ObservationForecastTime
            isDisabled={isDisabled}
            isReadOnly={isReadOnly}
            helperText={helperText}
            onChange={onChangeForm}
          />
        )}
        {/* Valid from */}
        <ValidFrom
          productType="airmet"
          isDisabled={isDisabled}
          onChange={onChangeForm}
        />
        {/* Valid until */}
        <ValidUntil
          productType="airmet"
          isDisabled={isDisabled}
          onChange={onChangeForm}
        />
      </Grid>
      {/* Where */}
      <SelectFIR productType="airmet" isDisabled={isDisabled} />
      {/* Draw */}
      <StartGeometry
        isReadOnly={isReadOnly}
        drawMode={drawMode}
        geoJSONs={geoJSONs}
        setDrawModeType={(
          mapDrawMode: MapDrawMode,
          StartOrEndDrawing: StartOrEndDrawing,
        ): void => {
          setDrawModeType(mapDrawMode, StartOrEndDrawing, updateStartDrawing);
        }}
      />
      {/* Levels */}
      {!isCancelAirmet &&
        phenomenon !== 'SFC_WIND' &&
        phenomenon !== 'SFC_VIS' &&
        phenomenon !== 'BKN_CLD' &&
        phenomenon !== 'OVC_CLD' && (
          <Levels
            isDisabled={isDisabled}
            isReadOnly={isReadOnly}
            onChange={onChangeForm}
            productType="airmet"
          />
        )}
      {/* Cloud Levels */}
      {!isCancelAirmet &&
        (phenomenon === 'BKN_CLD' || phenomenon === 'OVC_CLD') && (
          <CloudLevels
            isDisabled={isDisabled}
            isReadOnly={isReadOnly}
            onChange={onChangeForm}
          />
        )}
      {/* Progress */}
      {!isCancelAirmet && (
        <Progress
          productType="airmet"
          isDisabled={isDisabled}
          isReadOnly={isReadOnly}
          onChange={onChangeForm}
        />
      )}
      {/* Change */}
      {!isCancelAirmet && (
        <Change isDisabled={isDisabled} isReadOnly={isReadOnly} />
      )}
      {/* Issued at */}
      <Grid item container spacing={2} sx={styles.containerItem}>
        <Grid item xs={4} container justifyContent="flex-end">
          <Typography variant="subtitle1" sx={styles.label}>
            Issued at
          </Typography>
        </Grid>
        <Grid item xs={8} container justifyContent="flex-start">
          <Grid item xs={12}>
            <Typography variant="body2" sx={styles.body}>
              {getProductIssueDate(initialAirmet, initialCancelAirmet)}
            </Typography>
          </Grid>
        </Grid>
        {/* TAC */}
        <Grid item container spacing={2}>
          <Grid item xs={4} container justifyContent="flex-end">
            <Typography variant="subtitle1" sx={styles.label}>
              TAC
            </Typography>
          </Grid>
          <Grid item xs={8} container justifyContent="flex-start">
            <Grid item xs={12}>
              <ProductFormTAC tac={tac} />
            </Grid>
          </Grid>
        </Grid>
        {/* Hidden sequence number field needed for the TAC generation */}
        <ReactHookFormHiddenInput name="sequence" defaultValue="-1" />
        {
          /* Add hidden fields for TAC generation in case of cancel airmet */
          isCancelAirmet && (
            <>
              <ReactHookFormHiddenInput name="cancelsAirmetSequenceId" />
              <ReactHookFormHiddenInput name="validDateEndOfAirmetToCancel" />
              <ReactHookFormHiddenInput name="validDateStartOfAirmetToCancel" />
            </>
          )
        }
      </Grid>
      {/* Type */}
      <Type isDisabled={isDisabled} isReadOnly={isReadOnly} />
    </>
  );

  if (!showMap) {
    return renderForm();
  }

  return (
    <form
      onChange={onChangeForm}
      onSubmit={handleSubmit(() => null)}
      style={{ width: '100%' }}
    >
      <Grid container style={{ height: '100%' }}>
        <Grid item xs={8}>
          <MapViewGeoJson
            geoJSONs={geoJSONs}
            setLayerGeoJSONs={(geoJSONs): void =>
              setLayerGeoJSONs(createInterSections(geoJSONs))
            }
            layerDrawModes={layerDrawModes}
            drawMode={drawMode}
            setDrawMode={setDrawMode}
            exitDrawModeCallbackStart={updateStartDrawing}
          />
        </Grid>
        <Grid item xs={4} sx={styles.productForm}>
          {renderForm()}
        </Grid>
      </Grid>
    </form>
  );
};

export default AirmetForm;
