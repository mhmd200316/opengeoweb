/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';
import * as React from 'react';
import { Airmet, CancelAirmet } from '../../../types';
import { fakeAirmetList } from '../../../utils/mockdata/fakeAirmetList';

import {
  SnapshotStoryWrapper,
  StoryWrapperFakeApi,
} from '../../../utils/testUtils';
import AirmetForm from './AirmetForm';

export default {
  title: 'components/AirmetForm',
};

export const NewAirmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <StoryWrapperFakeApi>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              validDateStart: '2022-01-01T12:00Z',
              validDateEnd: '2022-01-01T13:00Z',
            },
          }}
        >
          <AirmetForm mode="new" showMap={false} />
        </ReactHookFormProvider>
      </StoryWrapperFakeApi>
    </SnapshotStoryWrapper>
  );
};

NewAirmet.storyName = 'New Airmet (takeSnapshot)';

export const EditAirmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <StoryWrapperFakeApi>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeAirmetList[5].airmet,
              validDateStart: '2020-09-17T11:00Z',
              validDateEnd: '2020-09-17T12:00Z',
              observationOrForecastTime: '2020-09-17T12:00Z',
            },
          }}
        >
          <AirmetForm
            mode="edit"
            showMap={false}
            initialAirmet={fakeAirmetList[5].airmet as Airmet}
          />
        </ReactHookFormProvider>
      </StoryWrapperFakeApi>
    </SnapshotStoryWrapper>
  );
};

EditAirmet.storyName = 'Edit Airmet (takeSnapshot)';

export const ViewAirmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <StoryWrapperFakeApi>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeAirmetList[1].airmet,
              validDateStart: '2020-09-17T11:00Z',
              validDateEnd: '2020-09-17T12:00Z',
              observationOrForecastTime: '2020-09-17T12:00Z',
            },
          }}
        >
          <AirmetForm
            mode="view"
            showMap={false}
            initialAirmet={fakeAirmetList[1].airmet as Airmet}
          />
        </ReactHookFormProvider>
      </StoryWrapperFakeApi>
    </SnapshotStoryWrapper>
  );
};

ViewAirmet.storyName = 'View Airmet (takeSnapshot)';

export const CancelledAirmet = (): React.ReactElement => {
  return (
    <SnapshotStoryWrapper>
      <StoryWrapperFakeApi>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeAirmetList[3].airmet,
              validDateStart: '2020-09-17T11:00Z',
              validDateEnd: '2020-09-17T12:00Z',
              observationOrForecastTime: '2020-09-17T12:00Z',
            },
          }}
        >
          <AirmetForm
            mode="view"
            showMap={false}
            isCancelAirmet={true}
            initialCancelAirmet={fakeAirmetList[3].airmet as CancelAirmet}
          />
        </ReactHookFormProvider>
      </StoryWrapperFakeApi>
    </SnapshotStoryWrapper>
  );
};

CancelledAirmet.storyName = 'Cancel Airmet (takeSnapshot)';
