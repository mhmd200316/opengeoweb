/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import moment from 'moment';

import AirmetForm from './AirmetForm';
import { noTAC } from '../ProductFormTac';
import { TestWrapper } from '../../../utils/testUtils';
import {
  fakeCancelAirmet,
  fakeDraftAirmet,
  fakePublishedAirmet,
  fakeAirmetList,
} from '../../../utils/mockdata/fakeAirmetList';
import { Airmet, AirmetPhenomena } from '../../../types';
import { ForecastTimeValidation } from '../ProductFormFields/ObservationForecastTime';
import { exitDrawModeMessage } from '../ProductFormFields/StartGeometry';

jest.mock('../../../utils/api');

describe('components/ProductForms/AirmetForm/AirmetForm', () => {
  it('should render successfully', async () => {
    const { container, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeDraftAirmet },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakeDraftAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());
    expect(
      container.querySelector('[name=phenomenon]').previousElementSibling
        .textContent,
    ).toContain(AirmetPhenomena[fakeDraftAirmet.phenomenon]);
  });
  it('should show no issue date for a new airmet', async () => {
    const { getByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {},
          }}
        >
          <AirmetForm mode="new" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(getByText('(Not published)')).toBeTruthy();
  });

  it('should show no issue date for a draft airmet', async () => {
    const { getByText, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeDraftAirmet },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakeDraftAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(getByText('(Not published)')).toBeTruthy();
  });

  it('should show issue date for a published airmet', async () => {
    const { getByText, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakePublishedAirmet },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakePublishedAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(
      getByText(
        `${moment
          .utc(fakePublishedAirmet.issueDate)
          .format('YYYY/MM/DD HH:mm')} UTC`,
      ),
    ).toBeTruthy();
  });

  it('should show issue date for a cancel airmet', async () => {
    const { getByText, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeCancelAirmet },
          }}
        >
          <AirmetForm
            mode="edit"
            isCancelAirmet
            initialCancelAirmet={fakeCancelAirmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(
      getByText(
        `${moment
          .utc(fakeCancelAirmet.issueDate)
          .format('YYYY/MM/DD HH:mm')} UTC`,
      ),
    ).toBeTruthy();
  });

  it('should show an error message when the start position drawing is removed', async () => {
    const { container, getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeDraftAirmet },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakeDraftAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    const deleteButtonStartGeometry = getByTestId(
      'startGeometry',
    ).querySelector('[data-testid="drawtools-delete"] input');

    fireEvent.click(deleteButtonStartGeometry);

    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(startGeometryHelperTextSelector).textContent,
    ).toEqual('A start position drawing is required');
  });

  it('should remove the error message for start position when selecting fir', async () => {
    const { container, getByTestId, queryByText } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeDraftAirmet },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakeDraftAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    const deleteButtonStartGeometry = getByTestId(
      'startGeometry',
    ).querySelector('[data-testid="drawtools-delete"] input');
    fireEvent.click(deleteButtonStartGeometry);

    const startGeometryHelperTextSelector = '[data-testid="startGeometry"] p';
    await waitFor(() =>
      expect(
        container.querySelector(startGeometryHelperTextSelector),
      ).toBeTruthy(),
    );
    expect(
      container.querySelector(startGeometryHelperTextSelector).textContent,
    ).toEqual('A start position drawing is required');

    const firButtonStartGeometry = getByTestId('startGeometry').querySelector(
      '[data-testid="drawtools-fir"] input',
    );
    fireEvent.click(firButtonStartGeometry);

    await waitFor(() =>
      expect(queryByText('A start position drawing is required')).toBeFalsy(),
    );
  });

  it('should show a helper text when entering draw mode', async () => {
    const { getByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider options={defaultFormOptions}>
          <AirmetForm mode="new" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const startGeometryContainer = getByTestId('startGeometry');

    const drawBtn = startGeometryContainer.querySelector(
      '[data-testid="drawtools-polygon"] input',
    );
    fireEvent.click(drawBtn);

    await waitFor(() =>
      expect(startGeometryContainer.textContent).toContain(exitDrawModeMessage),
    );
  });

  it('should disable form when entering draw mode', async () => {
    const { container, getByTestId } = render(
      <TestWrapper>
        <ReactHookFormProvider options={defaultFormOptions}>
          <AirmetForm mode="new" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const drawBtn = container.querySelector(
      '[data-testid="drawtools-polygon"] input',
    );
    fireEvent.click(drawBtn);

    const inputWrapper = getByTestId('isObservationOrForecast-OBS');

    await waitFor(() =>
      expect(
        inputWrapper.querySelector('input').hasAttribute('disabled'),
      ).toBeTruthy(),
    );

    // enable form again by switching mode
    const deleteBtn = container.querySelector(
      '[data-testid="drawtools-delete"] input',
    );
    fireEvent.click(deleteBtn);

    await waitFor(() =>
      expect(
        inputWrapper.querySelector('input').hasAttribute('disabled'),
      ).toBeFalsy(),
    );
  });

  it('should show an error message when choosing forecast and forecast time is before valid from time', async () => {
    const { queryByText, getByTestId, container } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeDraftAirmet,
            },
          }}
        >
          <AirmetForm mode="edit" initialAirmet={fakeDraftAirmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const obsField = getByTestId('isObservationOrForecast-OBS');
    const fcstField = getByTestId('isObservationOrForecast-FCST');

    fireEvent.click(fcstField);

    await waitFor(() => {
      expect(obsField.querySelector('.Mui-checked')).toBeFalsy();
      expect(fcstField.querySelector('.Mui-checked')).toBeTruthy();
    });
    await waitFor(() => {
      expect(queryByText(ForecastTimeValidation)).toBeTruthy();
      expect(
        container
          .querySelector('[name="observationOrForecastTime"]')
          .parentElement.classList.contains('Mui-error'),
      ).toBeTruthy();
    });
  });
  it('should show the visibility value and cause and not show Levels field in case of SFC_VIS phenomena', async () => {
    const { container } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmetList[1].airmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmetList[1].airmet as Airmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() =>
      expect(container.querySelector('[name=visibilityValue]')).toBeTruthy(),
    );
    await waitFor(() =>
      expect(container.querySelector('[name=visibilityCause]')).toBeTruthy(),
    );
    await waitFor(() =>
      expect(container.querySelector('[name=levelInfoMode]')).toBeFalsy(),
    );
  });
  it('should show the wind speed, unit and direction and not show Levels field in case of SFC_WIND phenomena', async () => {
    const { container } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmetList[5].airmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmetList[5].airmet as Airmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() =>
      expect(container.querySelector('[name=windDirection]')).toBeTruthy(),
    );
    await waitFor(() =>
      expect(container.querySelector('[name=windUnit]')).toBeTruthy(),
    );
    await waitFor(() =>
      expect(container.querySelector('[name=windSpeed]')).toBeTruthy(),
    );
    await waitFor(() =>
      expect(container.querySelector('[name=levelInfoMode]')).toBeFalsy(),
    );
  });
  it('should show the cloud Levels fields and not show Levels fields in case of BKN_CLD phenomena', () => {
    const { container } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmetList[7].airmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmetList[7].airmet as Airmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(container.querySelector('[name=checkedAbove]')).toBeTruthy();
    expect(container.querySelector('[name="cloudLevel.unit"]')).toBeTruthy();
    expect(container.querySelector('[name="cloudLevel.value"]')).toBeTruthy();
    expect(container.querySelector('[name=checkedSFC]')).toBeTruthy();
    expect(
      container.querySelector('[name="cloudLowerLevel.unit"]'),
    ).toBeTruthy();
    expect(
      container.querySelector('[name="cloudLowerLevel.value"]'),
    ).toBeTruthy();
    expect(container.querySelector('[name=levelInfoMode]')).toBeFalsy();
  });
  it('should show the cloud Levels fields and not show Levels fields in case of OVC_CLD phenomena', () => {
    const { container } = render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeAirmetList[4].airmet },
          }}
        >
          <AirmetForm
            mode="edit"
            initialAirmet={fakeAirmetList[4].airmet as Airmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(container.querySelector('[name=checkedAbove]')).toBeTruthy();
    expect(container.querySelector('[name="cloudLevel.unit"]')).toBeTruthy();
    expect(container.querySelector('[name="cloudLevel.value"]')).toBeTruthy();
    expect(container.querySelector('[name=checkedSFC]')).toBeTruthy();
    expect(
      container.querySelector('[name="cloudLowerLevel.unit"]'),
    ).toBeFalsy();
    expect(
      container.querySelector('[name="cloudLowerLevel.value"]'),
    ).toBeFalsy();
    expect(container.querySelector('[name=levelInfoMode]')).toBeFalsy();
  });
});
