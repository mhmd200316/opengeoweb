/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { act, renderHook } from '@testing-library/react-hooks';
import { simplePolygonGeoJSONHalfOfNL } from './constants';
import { intersectGeoJSONS, useDrawMode } from './utils';
import { getFir } from '../../utils/getFir';
import { MapDrawMode, StartOrEndDrawing } from '../../types';

describe('components/MapViewGeoJSON/utils/intersectGeoJSONS', () => {
  it('should run intersectGeoJSONS successfully', () => {
    const intersection = intersectGeoJSONS(
      simplePolygonGeoJSONHalfOfNL,
      getFir(),
    );
    expect(intersection).toBeTruthy();
    /* Resulting intersection should have 13 points */
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(intersection.features[0].geometry.coordinates[0].length).toBe(13);
  });
});

describe('components/MapViewGeoJSON/utils/useDrawMode', () => {
  it('should call start callback function if passed for FIR', async () => {
    const fakeCallback = jest.fn();

    const { result } = renderHook(() => useDrawMode());
    const { setDrawModeType } = result.current;

    await act(async () => {
      setDrawModeType(
        'FIR' as MapDrawMode,
        'start' as StartOrEndDrawing,
        fakeCallback,
      );
    });

    expect(fakeCallback).toHaveBeenCalled();
  });
  it('should not call start callback function if passed for POINT', async () => {
    const fakeCallback = jest.fn();

    const { result } = renderHook(() => useDrawMode());
    const { setDrawModeType } = result.current;

    await act(async () => {
      setDrawModeType(
        'POINT' as MapDrawMode,
        'start' as StartOrEndDrawing,
        fakeCallback,
      );
    });

    expect(fakeCallback).not.toHaveBeenCalled();
  });
  it('should call end callback function if passed for DELETE', async () => {
    const fakeCallback = jest.fn();

    const { result } = renderHook(() => useDrawMode());
    const { setDrawModeType } = result.current;

    await act(async () => {
      setDrawModeType(
        'DELETE' as MapDrawMode,
        'end' as StartOrEndDrawing,
        fakeCallback,
      );
    });

    expect(fakeCallback).toHaveBeenCalled();
  });
  it('should not end callback function if passed for BOX', async () => {
    const fakeCallback = jest.fn();

    const { result } = renderHook(() => useDrawMode());
    const { setDrawModeType } = result.current;

    await act(async () => {
      setDrawModeType(
        'BOX' as MapDrawMode,
        'end' as StartOrEndDrawing,
        fakeCallback,
      );
    });

    expect(fakeCallback).not.toHaveBeenCalled();
  });
});
