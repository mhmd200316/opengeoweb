/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import produce from 'immer';

import MapViewGeoJson from './MapViewGeoJson';
import {
  featurePropsFIR,
  simplePolygonGeoJSON,
  simplePolygonGeoJSONHalfOfNL,
} from './constants';
import { getFir } from '../../utils/getFir';
import { intersectGeoJSONS } from './utils';
import { StoryWrapperFakeApi } from '../../utils/testUtils';

export default {
  title: 'components/Mapview',
};

export const mapViewGeoJson: React.FC = () => {
  return (
    <div style={{ height: '100vh' }}>
      <StoryWrapperFakeApi>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
          }}
        />
      </StoryWrapperFakeApi>
    </div>
  );
};

export const mapViewAmsterdamFir: React.FC = () => {
  const firArea = produce(getFir(), (draft) => {
    draft.features[0].properties = {
      ...draft.features[0].properties,
      ...featurePropsFIR,
    };
  });
  return (
    <div style={{ height: '100vh' }}>
      <StoryWrapperFakeApi>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
            fir: firArea,
          }}
        />
      </StoryWrapperFakeApi>
    </div>
  );
};

export const mapViewIntersect: React.FC = () => {
  const firArea = produce(getFir(), (draft) => {
    draft.features[0].properties = {
      ...draft.features[0].properties,
      ...featurePropsFIR,
    };
  });
  return (
    <div style={{ height: '100vh' }}>
      <StoryWrapperFakeApi>
        <MapViewGeoJson
          geoJSONs={{
            start: produce(simplePolygonGeoJSONHalfOfNL, (draft) => {
              draft.features[0].properties['fill-opacity'] = 0;
              draft.features[0].properties['stroke-width'] = 5;
            }),
            intersectionStart: intersectGeoJSONS(
              simplePolygonGeoJSONHalfOfNL,
              firArea,
            ),
            fir: firArea,
          }}
        />
      </StoryWrapperFakeApi>
    </div>
  );
};
