/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import * as React from 'react';
import {
  MapViewConnect,
  generateMapId,
  MapViewLayer,
  mapActions,
  generateLayerId,
  mapSelectors,
  mapTypes,
  AppStore,
  TimeSliderConnect,
  SyncGroups,
  generateTimesliderId,
  LayerManagerMapButtonConnect,
  MultiDimensionSelectMapButtonsConnect,
  LegendMapButtonConnect,
  publicLayers,
  MapControls,
  LegendConnect,
} from '@opengeoweb/core';

import produce from 'immer';
import { FeatureCollection, Polygon } from 'geojson';
import { srsAndBboxDefault } from './constants';
import { MapDrawMode, MapGeoJSONS, SetMapDrawModes } from '../../types';
import * as KNMIlayers from '../../utils/KNMIlayers';

export interface MapViewGeoJsonProps {
  geoJSONs: MapGeoJSONS;
  setLayerGeoJSONs?: (geoJSONs: MapGeoJSONS) => void;
  layerDrawModes?: SetMapDrawModes;
  drawMode?: SetMapDrawModes;
  setDrawMode?: (drawMode: SetMapDrawModes) => void;
  exitDrawModeCallbackStart?: () => void;
  exitDrawModeCallbackEnd?: () => void;
}

export const allowOneGeoJSONFeature = (
  mode: MapDrawMode,
  geojson: FeatureCollection<Polygon>,
): FeatureCollection<Polygon> => {
  switch (mode) {
    case MapDrawMode.BOX:
    case MapDrawMode.POLYGON:
      return geojson.features[0].geometry.coordinates.length > 1
        ? produce(geojson, (draft) => {
            draft.features[0].geometry.coordinates = [
              draft.features[0].geometry.coordinates[1],
            ];
          })
        : geojson;
    default:
      return geojson;
  }
};

const MapViewGeoJson: React.FC<MapViewGeoJsonProps> = ({
  geoJSONs,
  setLayerGeoJSONs = (): void => {},
  layerDrawModes = { start: null, end: null },
  drawMode = { start: null, end: null },
  setDrawMode = (): void => {},
  exitDrawModeCallbackStart = (): void => {},
  exitDrawModeCallbackEnd = (): void => {},
}: MapViewGeoJsonProps) => {
  const dispatch = useDispatch();

  const firstMap = useSelector(
    (store: AppStore) => mapSelectors.getFirstMap(store),
    shallowEqual,
  );
  const firstMapId = firstMap && firstMap.id ? firstMap.id : null;

  const mapLayers = useSelector(
    (store: AppStore) => mapSelectors.getMapLayers(store, firstMapId),
    shallowEqual,
  );
  const mapDimensions = useSelector(
    (store: AppStore) => mapSelectors.getMapDimensions(store, firstMapId),
    shallowEqual,
  );

  const mapId = React.useRef(`sigmet-${generateMapId()}`).current;
  const timesliderId = React.useRef(`sigmet-${generateTimesliderId()}`).current;

  const syncGroupAddGroup = React.useCallback(
    (payload: SyncGroups.types.SyncGroupAddGroupPayload): void => {
      dispatch(SyncGroups.actions.syncGroupAddGroup(payload));
    },
    [dispatch],
  );

  const syncGroupAddTarget = React.useCallback(
    (payload: SyncGroups.types.SyncGroupAddTargetPayload): void => {
      dispatch(SyncGroups.actions.syncGroupAddTarget(payload));
    },
    [dispatch],
  );

  React.useEffect(() => {
    if (mapLayers && mapLayers.length) {
      // use maplayers, but change layerId and current mapId
      const layers = mapLayers.map((layer: mapTypes.Layer) => ({
        ...layer,
        id: generateLayerId(),
        mapId,
      }));
      dispatch(
        mapActions.setLayers({
          mapId,
          layers,
        }),
      );
    }

    if (mapDimensions && mapDimensions.length) {
      dispatch(
        mapActions.mapChangeDimension({
          mapId,
          dimension: mapDimensions[0],
          origin: 'MapViewGeoJson',
        }),
      );
    }

    dispatch(
      mapActions.setBaseLayers({
        mapId,
        layers: [
          { ...publicLayers.baseLayerGrey, id: `baselayer-${mapId}` },
          { ...publicLayers.overLayer, id: `countryborders-${mapId}` },
          { ...KNMIlayers.northSeaPlatforms, id: `northseaplatforms-${mapId}` },
          { ...KNMIlayers.majorAirportsEHAA, id: `majorairportsehaa-${mapId}` },
        ],
      }),
    );

    dispatch(
      mapActions.setBbox({
        mapId,
        bbox: srsAndBboxDefault.bbox,
        srs: srsAndBboxDefault.srs,
      }),
    );
    syncGroupAddGroup({
      groupId: 'Time_SigmetAirmet',
      title: 'Group 1 for sigmet/airmet map',
      type: SyncGroups.constants.SYNCGROUPS_TYPE_SETTIME,
    });

    syncGroupAddTarget({ groupId: 'Time_SigmetAirmet', targetId: mapId });
    syncGroupAddTarget({
      groupId: 'Time_SigmetAirmet',
      targetId: timesliderId,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div style={{ position: 'relative', height: '99%' }} data-testid="MapView">
      <MapControls>
        <LayerManagerMapButtonConnect source="module" mapId={mapId} />
        <LegendMapButtonConnect source="module" mapId={mapId} />
        <MultiDimensionSelectMapButtonsConnect source="module" mapId={mapId} />
      </MapControls>

      <MapViewConnect mapId={mapId}>
        <MapViewLayer
          id={`geojsonlayer-fir-${mapId}`}
          isInEditMode={false}
          geojson={geoJSONs.fir}
        />
        <MapViewLayer
          id={`geojsonlayer-end-${mapId}`}
          isInEditMode={drawMode.end !== null && layerDrawModes.end !== null}
          geojson={geoJSONs.end}
          drawMode={layerDrawModes.end}
          updateGeojson={(geojson): void => {
            const end = allowOneGeoJSONFeature(layerDrawModes.end, geojson);
            setLayerGeoJSONs({ ...geoJSONs, end });
          }}
          exitDrawModeCallback={(): void => {
            setDrawMode({ start: null, end: null });
            exitDrawModeCallbackEnd();
          }}
          featureNrToEdit={0}
        />
        <MapViewLayer
          id={`geojsonlayer-start-${mapId}`}
          isInEditMode={
            drawMode.start !== null && layerDrawModes.start !== null
          }
          geojson={geoJSONs.start}
          drawMode={layerDrawModes.start}
          updateGeojson={(geojson): void => {
            const start = allowOneGeoJSONFeature(layerDrawModes.start, geojson);
            setLayerGeoJSONs({ ...geoJSONs, start });
          }}
          exitDrawModeCallback={(): void => {
            setDrawMode({ start: null, end: null });
            exitDrawModeCallbackStart();
          }}
          featureNrToEdit={0}
        />
        <MapViewLayer
          id={`geojsonlayer-intersection-end-${mapId}`}
          isInEditMode={false}
          geojson={geoJSONs.intersectionEnd}
        />
        <MapViewLayer
          id={`geojsonlayer-intersection-start-${mapId}`}
          isInEditMode={false}
          geojson={geoJSONs.intersectionStart}
        />
      </MapViewConnect>
      <div
        style={{
          position: 'absolute',
          left: '0',
          bottom: '0',
          zIndex: 1000,
          width: '100%',
        }}
      >
        <TimeSliderConnect sourceId={timesliderId} mapId={mapId} />
      </div>
      <LegendConnect showMapId mapId={mapId} />
    </div>
  );
};

export default MapViewGeoJson;
