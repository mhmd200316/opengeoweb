/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import produce from 'immer';
import * as turf from '@turf/turf';
import { getFir } from '../../utils/getFir';

import {
  featurePropsEnd,
  featurePropsFIR,
  featurePropsIntersectionEnd,
  featurePropsIntersectionStart,
  featurePropsStart,
  featurePropsUnStyled,
  geoJSONTemplate,
} from './constants';
import {
  SetMapDrawModes,
  MapGeoJSONS,
  MapDrawMode,
  StartOrEndDrawing,
} from '../../types';

const getDrawModeFromSelectionType = (selectionType: string): MapDrawMode => {
  switch (selectionType) {
    case 'point':
      return MapDrawMode.POINT;
    case 'box':
      return MapDrawMode.BOX;
    case 'fir':
      return MapDrawMode.FIR;
    case 'poly':
    default:
      return MapDrawMode.POLYGON;
  }
};

/**
 * Adds properties to the first geojson feature based on the given property object.
 * It only extends or changes the properties which are defined in styleConfig,
 * all other properties in the geojson are left unchanged.
 * @param geojson
 * @param featureProperties
 */
const addFeatureProperties = (
  geojson: GeoJSON.FeatureCollection,
  featureProperties,
): GeoJSON.FeatureCollection => {
  if (!geojson) return null;
  return produce(geojson, (draft) => {
    if (
      draft.features &&
      draft.features.length > 0 &&
      draft.features[0].properties
    ) {
      Object.keys(featureProperties).forEach((key) => {
        draft.features[0].properties[key] = featureProperties[key];
      });
    }
  });
};

export const useDrawMode = (
  initialGeometry = null,
): {
  geoJSONs: MapGeoJSONS;
  setLayerGeoJSONs: (geoJSONs: MapGeoJSONS) => void;
  layerDrawModes: SetMapDrawModes;
  setLayersDrawModes: (layerDrawModes: SetMapDrawModes) => void;
  drawMode: SetMapDrawModes;
  setDrawMode: (drawMode: SetMapDrawModes) => void;
  setDrawModeType: (
    mapDrawMode: MapDrawMode,
    startOrEnd: StartOrEndDrawing,
    updateCallback?: (mapGeoJSONS: MapGeoJSONS) => void,
  ) => void;
} => {
  const getInitialLayerDrawModes = (startorEnd: 'start' | 'end'): MapDrawMode =>
    initialGeometry && initialGeometry[startorEnd]
      ? getDrawModeFromSelectionType(
          initialGeometry[startorEnd].features[0].properties.selectionType,
        )
      : null;

  /* For the radio buttons */
  const [drawMode, setDrawMode] = React.useState<SetMapDrawModes>({
    start: null,
    end: null,
  });

  /* Drawmode for the layers */
  const [layerDrawModes, setLayersDrawModes] = React.useState<SetMapDrawModes>({
    start: getInitialLayerDrawModes('start'),
    end: getInitialLayerDrawModes('end'),
  });

  /* GeoJSONs for the layers */
  const [geoJSONs, setLayerGeoJSONs] = React.useState<MapGeoJSONS>(() => {
    return {
      start: addFeatureProperties(
        initialGeometry && initialGeometry.start,
        featurePropsStart,
      ),
      end: addFeatureProperties(
        initialGeometry && initialGeometry.end,
        featurePropsEnd,
      ),
      intersectionStart: addFeatureProperties(
        initialGeometry && initialGeometry.intersectionStart,
        featurePropsIntersectionStart,
      ),
      intersectionEnd: addFeatureProperties(
        initialGeometry && initialGeometry.intersectionEnd,
        featurePropsIntersectionEnd,
      ),
      fir: addFeatureProperties(getFir(), featurePropsFIR),
    };
  });

  /**
   *
   * @param mapDrawMode Can be 'POINT' | 'BOX' | 'POLYGON' |  'FIR' | 'DELETE'
   * @param startOrEnd Can be 'start' | 'end'
   */
  const setDrawModeType = (
    mapDrawMode: MapDrawMode,
    startOrEnd: StartOrEndDrawing,
    updateCallback?: (mapGeoJSONS: MapGeoJSONS) => void,
  ): void => {
    const newGeoJSONDrawMode = { ...layerDrawModes };

    const featureStyleProperties =
      startOrEnd === StartOrEndDrawing.start
        ? featurePropsStart
        : featurePropsEnd;

    /* Switch on the mapdrawmode type for either start or end */
    switch (mapDrawMode) {
      /* Set draw modes point, box, polygon for either start or end layer */
      case MapDrawMode.POINT:
      case MapDrawMode.BOX:
      case MapDrawMode.POLYGON: {
        if (newGeoJSONDrawMode[startOrEnd] !== mapDrawMode) {
          /* Set for either start or end GeoJSON; key is the same as in the MapDrawMode ENUM */
          const cleanGeoJSON = produce<MapGeoJSONS>(geoJSONs, (draft) => {
            draft[startOrEnd] = addFeatureProperties(
              geoJSONTemplate[mapDrawMode][startOrEnd],
              featureStyleProperties,
            );
          });
          setLayerGeoJSONs(createInterSections(cleanGeoJSON));
        }

        newGeoJSONDrawMode[startOrEnd] = mapDrawMode;

        setLayersDrawModes(newGeoJSONDrawMode);
        setDrawMode(
          startOrEnd === StartOrEndDrawing.start
            ? { start: mapDrawMode, end: null }
            : { start: null, end: mapDrawMode },
        );
        break;
      }
      /* Set complete fir for either start or end layer */
      case MapDrawMode.FIR: {
        newGeoJSONDrawMode[startOrEnd] = mapDrawMode;
        const cleanGeoJSON = produce<MapGeoJSONS>(geoJSONs, (draft) => {
          draft[startOrEnd] = addFeatureProperties(getFir(), {
            ...featureStyleProperties,
            selectionType: 'fir',
          });
        });
        setDrawMode({ start: null, end: null });
        setLayersDrawModes(newGeoJSONDrawMode);
        const intersections = createInterSections(cleanGeoJSON);
        setLayerGeoJSONs(intersections);
        if (updateCallback) updateCallback(intersections);
        break;
      }
      /* Clear the map for either start or end layer */
      case MapDrawMode.DELETE: {
        const newDrawMode = { ...drawMode };
        newDrawMode[startOrEnd] = null;
        setDrawMode(newDrawMode);
        switch (newGeoJSONDrawMode[startOrEnd]) {
          case MapDrawMode.POINT:
          case MapDrawMode.BOX: {
            const cleanGeoJSON = produce<MapGeoJSONS>(geoJSONs, (draft) => {
              draft[startOrEnd] = addFeatureProperties(
                geoJSONTemplate[newGeoJSONDrawMode[startOrEnd]][startOrEnd],
                featureStyleProperties,
              );
            });

            const intersections = createInterSections(cleanGeoJSON);
            setLayerGeoJSONs(intersections);
            if (updateCallback) updateCallback(intersections);
            break;
          }
          default: {
            const cleanGeoJSON = produce<MapGeoJSONS>(geoJSONs, (draft) => {
              draft[startOrEnd] = addFeatureProperties(
                geoJSONTemplate['POLYGON' as MapDrawMode][startOrEnd],
                featureStyleProperties,
              );
            });
            const intersections = createInterSections(cleanGeoJSON);
            setLayerGeoJSONs(intersections);
            if (updateCallback) updateCallback(intersections);
            break;
          }
        }
        break;
      }
      default:
        break;
    }
  };

  return {
    drawMode,
    setDrawMode,
    layerDrawModes,
    setLayersDrawModes,
    geoJSONs,
    setLayerGeoJSONs,
    setDrawModeType,
  };
};

/**
 * Returns the intersection of two features. In case of a polygon, only the first feature is used.
 * @param a Feature A
 * @param b Feature B
 * @returns The intersection of the two features.
 */
export const intersectGeoJSONS = (
  a: GeoJSON.FeatureCollection,
  b: GeoJSON.FeatureCollection,
): GeoJSON.FeatureCollection => {
  const featureA = turf.feature(a.features[0].geometry as turf.Polygon);
  const featureB = turf.feature(b.features[0].geometry as turf.Polygon);
  const options = { tolerance: 0.001, highQuality: true };
  const simplifiedA = turf.simplify(featureA, options);
  const simplifiedB = turf.simplify(featureB, options);
  const intersection = turf.intersect(simplifiedA, simplifiedB);

  return addFeatureProperties(
    {
      type: 'FeatureCollection',
      features:
        intersection === null
          ? [
              {
                type: 'Feature',
                properties: {
                  selectionType: 'poly',
                },
                geometry: {
                  type: 'Polygon',
                  coordinates: [[]],
                },
              },
            ]
          : [intersection],
    },
    featurePropsUnStyled,
  );
};

/**
 * Adds the intersectionStart and intersectionEnd properties to the GeoJSONS structure
 * @param geoJSONs
 * @returns GeoJSONS extend with intersections
 */

export const createInterSections = (geoJSONs: MapGeoJSONS): MapGeoJSONS => {
  const intersections = produce(geoJSONs, (draft) => {
    try {
      draft.intersectionStart = addFeatureProperties(
        intersectGeoJSONS(geoJSONs.start, geoJSONs.fir),
        featurePropsIntersectionStart,
      );
    } catch (e) {
      draft.intersectionStart = addFeatureProperties(
        geoJSONs.start,
        featurePropsIntersectionStart,
      );
    }

    try {
      draft.intersectionEnd = addFeatureProperties(
        intersectGeoJSONS(geoJSONs.end, geoJSONs.fir),
        featurePropsIntersectionEnd,
      );
    } catch (e) {
      draft.intersectionEnd = addFeatureProperties(
        geoJSONs.end,
        featurePropsIntersectionEnd,
      );
    }
  });
  return intersections;
};
