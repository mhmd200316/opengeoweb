/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Polygon, FeatureCollection } from 'geojson';
import {
  coreModuleConfig,
  layerActions,
  mapActions,
  testLayers,
} from '@opengeoweb/core';
import { DynamicModuleLoader } from 'redux-dynamic-modules';
import MapViewGeoJson, { allowOneGeoJSONFeature } from './MapViewGeoJson';
import { simplePolygonGeoJSON } from './constants';
import { TestWrapper } from '../../utils/testUtils';
import { MapDrawMode } from '../../types';

describe('allowOneGeoJSONFeature', () => {
  it('should allow one GeoJSON feature', () => {
    const dummyFeature = {
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1],
                [0, 0],
              ],
            ],
          },
        },
      ],
      type: 'FeatureCollection',
    } as FeatureCollection<Polygon>;

    const result = allowOneGeoJSONFeature(MapDrawMode.BOX, dummyFeature)
      .features[0] as unknown as Polygon;

    expect(result.coordinates).toEqual(
      (dummyFeature.features[0] as unknown as Polygon).coordinates,
    );
  });

  it('should return one GeoJSON feature', () => {
    const dummyFeature = {
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1],
                [0, 0],
              ],
              [
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1],
                [0, 0],
              ],
            ],
          },
        },
      ],
      type: 'FeatureCollection',
    } as FeatureCollection<Polygon>;
    const result = allowOneGeoJSONFeature(MapDrawMode.BOX, dummyFeature)
      .features[0] as unknown as Polygon;
    expect(result.coordinates).toEqual(
      (dummyFeature.features[0] as unknown as Polygon).coordinates,
    );
  });
});

describe('MapViewGeoJson', () => {
  it('should render successfully with default layers', () => {
    const { queryAllByTestId } = render(
      <TestWrapper>
        <DynamicModuleLoader modules={coreModuleConfig}>
          <MapViewGeoJson
            geoJSONs={{
              start: simplePolygonGeoJSON,
              end: simplePolygonGeoJSON,
            }}
          />
        </DynamicModuleLoader>
      </TestWrapper>,
    );
    const layerList = queryAllByTestId('mapViewLayer');
    expect(layerList[0].textContent).toContain(testLayers.baseLayerGrey.name);
    expect(layerList[1].textContent).toContain('countryborders');
    expect(layerList[2].textContent).toContain('northseaplatforms');
    expect(layerList[3].textContent).toContain('majorairportsehaa');
    expect(layerList[4].textContent).toContain('geojsonlayer-fir');
  });

  it('should render successfully with fir', () => {
    const { queryAllByTestId } = render(
      <TestWrapper>
        <DynamicModuleLoader modules={coreModuleConfig}>
          <MapViewGeoJson
            geoJSONs={{
              start: simplePolygonGeoJSON,
              end: simplePolygonGeoJSON,
              fir: simplePolygonGeoJSON,
            }}
          />
        </DynamicModuleLoader>
      </TestWrapper>,
    );
    const layerList = queryAllByTestId('mapViewLayer');
    expect(layerList[4].textContent).toContain('geojsonlayer-fir');
    expect(layerList[5].textContent).toContain('geojsonlayer-end');
    expect(layerList[6].textContent).toContain('geojsonlayer-start');
    expect(layerList[7].textContent).toContain('geojsonlayer-intersection-end');
    expect(layerList[8].textContent).toContain(
      'geojsonlayer-intersection-start',
    );
  });

  it('should set layers of main map if they exist', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);

    store.addModules = jest.fn();

    render(
      <TestWrapper store={store}>
        <DynamicModuleLoader modules={coreModuleConfig}>
          <MapViewGeoJson
            geoJSONs={{
              start: simplePolygonGeoJSON,
              end: simplePolygonGeoJSON,
              fir: simplePolygonGeoJSON,
            }}
          />
        </DynamicModuleLoader>
      </TestWrapper>,
    );
    expect(
      store
        .getActions()
        .find((action) => action.type === layerActions.setLayers.type),
    ).toBeTruthy();
  });

  it('should set mapDimensions of the main map if they exist', () => {
    const dimensions = [{ currentTime: new Date().toString(), name: 'time' }];
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
            dimensions,
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);

    store.addModules = jest.fn();

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
            fir: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );

    const expectedAction = store
      .getActions()
      .find((action) => action.type === mapActions.mapChangeDimension.type);
    expect(expectedAction).toBeTruthy();
    expect(expectedAction.payload.dimension).toEqual(dimensions[0]);
  });

  it('should contain the layermanager, legend and multidimensionselect buttons', async () => {
    const dimensions = [{ currentValue: '9000', name: 'elevation' }];
    const mapId = 'main-map';
    const layerId = 'layerid_1';

    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            id: mapId,
            mapLayers: [layerId],
            dimensions,
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          [layerId]: {
            mapId,
            id: layerId,
            dimensions: [
              {
                name: 'elevation',
                currentValue: '9000',
                units: 'meters',
                values: '1000,5000,9000',
                synced: false,
              },
            ],
          },
        },
        allIds: [layerId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();

    const { getByTestId } = render(
      <TestWrapper store={store}>
        <MapViewGeoJson
          geoJSONs={{
            start: simplePolygonGeoJSON,
            end: simplePolygonGeoJSON,
          }}
        />
      </TestWrapper>,
    );
    expect(getByTestId('layerManagerButton')).toBeTruthy();
    expect(getByTestId('open-Legend')).toBeTruthy();

    // cannot check multidimensionselect button because it only shows up after dimension is set, checking the action instead
    const dimensionAction = store
      .getActions()
      .find((action) => action.type === mapActions.mapChangeDimension.type);
    expect(dimensionAction).toBeTruthy();
    expect(dimensionAction.payload.dimension).toEqual(dimensions[0]);
  });
});
