/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { useApi, useApiContext } from '@opengeoweb/api';
import { usePoller } from '@opengeoweb/shared';
import React from 'react';

import { AirmetFromBackend, ProductType, SigmetFromBackend } from '../../types';
import { SigmetAirmetApi } from '../../utils/api';
import { ProductFormDialog } from '../ProductForms/ProductFormDialog';
import { ProductList } from '../ProductList';

export interface MetInfoWrapperProps {
  productType: ProductType;
}

/**
 * Shows the list of Sigmets or Airmets and allows to create, edit and view Sigmets/Airmets.
 * Needs to be wrapped in the ApiProvider.
 * Pass in either 'sigmet' or 'airmet' as prop to determine the mode
 * @param {string} productType productType: string - either 'sigmet' or 'airmet'. Determines mode in which wrapper is loaded
 *
 * @example
 * ```<ApiProvider baseURL="http://test.com" appURL="http://app.com" auth={auth} onSetAuth={onSetAuth} createApi={api.createApi} authTokenUrl="url/tokenrefresh"> <MetInfoWrapper productType="sigmet"/> </ApiProvider>```
 */
const MetInfoWrapper: React.FC<MetInfoWrapperProps> = ({
  productType,
}: MetInfoWrapperProps) => {
  const { api } = useApiContext<SigmetAirmetApi>();

  const [isFormDialogOpen, setIsFormDialogOpen] = React.useState(false);
  const [selectedProduct, setSelectedProduct] = React.useState(null);

  // Retrieve list of products
  const {
    error: errorList,
    isLoading: isLoadingList,
    result: productList,
    fetchApiData: fetchNewProductList,
  } = useApi(productType === 'sigmet' ? api.getSigmetList : api.getAirmetList);
  const getNewProductList = (): void => {
    fetchNewProductList({});
  };

  usePoller([productList, errorList], getNewProductList, 60000);

  React.useEffect(() => {
    if (selectedProduct) {
      toggleDialogOpen();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedProduct]);

  const toggleDialogOpen = (): void => {
    // Refresh list on closing dialog
    if (isFormDialogOpen === true) getNewProductList();
    setIsFormDialogOpen(!isFormDialogOpen);
  };

  const handleProductRowClick = (
    product: SigmetFromBackend | AirmetFromBackend,
  ): void => {
    if (selectedProduct === product) {
      toggleDialogOpen();
    } else {
      setSelectedProduct(product);
    }
  };

  const handleNewProductClick = (): void => {
    setSelectedProduct(null);
    toggleDialogOpen();
  };
  return (
    <>
      <ProductList
        productList={productList || []}
        productType={productType}
        isLoading={isLoadingList}
        error={errorList}
        onClickProductRow={handleProductRowClick}
        onClickNewProduct={handleNewProductClick}
      />
      <ProductFormDialog
        productType={productType}
        isOpen={isFormDialogOpen}
        toggleDialogStatus={toggleDialogOpen}
        productListItem={selectedProduct}
      />
    </>
  );
};

export default MetInfoWrapper;
