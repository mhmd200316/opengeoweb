/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import * as utils from '@opengeoweb/api';
import MetInfoWrapper from './MetInfoWrapper';
import { noTAC } from '../ProductForms/ProductFormTac';
import { TestWrapper } from '../../utils/testUtils';
import { fakeAirmetTAC, fakeSigmetTAC } from '../../utils/fakeApi';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';
import { fakeSigmetList } from '../../utils/mockdata/fakeSigmetList';
import { Airmet, Sigmet } from '../../types';

jest.mock('../../utils/api');
jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual('../../../../api/src/index') as Record<
    string,
    unknown
  >),
}));

describe('components/MetInfoWrapper/MetInfoWrapper', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('should load the correct list based on the productType passed in (airmet)', async () => {
    const { findByText, findAllByText, getAllByTestId } = render(
      <TestWrapper>
        <MetInfoWrapper productType="airmet" />
      </TestWrapper>,
    );

    // Wait until airmetlist has loaded
    await findAllByText('Issue time');

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await findByText(fakeAirmetTAC);

    expect(await findByText('Create a new AIRMET')).toBeTruthy();
  });

  it('should load the correct list based on the productType passed in (sigmet)', async () => {
    const { findByText, findAllByText, getAllByTestId } = render(
      <TestWrapper>
        <MetInfoWrapper productType="sigmet" />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await findAllByText('Issue time');

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await findByText(fakeSigmetTAC);

    expect(await findByText('Create a new SIGMET')).toBeTruthy();
  });

  it('should open the dialog for a new sigmet when creating a new sigmet', async () => {
    const {
      getByTestId,
      queryByTestId,
      findAllByText,
      getAllByTestId,
      findByText,
    } = render(
      <TestWrapper>
        <MetInfoWrapper productType="sigmet" />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await findAllByText('Issue time');

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await findByText(fakeSigmetTAC);

    expect(queryByTestId('productform-dialog')).toBeFalsy();
    fireEvent.click(getByTestId('productListCreateButton'));
    expect(getByTestId('productform-dialog')).toBeTruthy();
    expect(getByTestId('dialogTitle').textContent).toEqual('New SIGMET');
  });

  it('should open the dialog for a new airmet when creating a new airmet, save it and open a new empty airmet', async () => {
    const mockGetAirmetList = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({ data: fakeAirmetList });
      });
    });

    const mockGetTac = jest.fn(() => {
      return new Promise((resolve) => {
        resolve({
          data: fakeAirmetTAC,
        });
      });
    });

    const mockPostAirmet = jest.fn(() => {
      return new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve();
        }, 3000);
      });
    });

    const {
      getByTestId,
      queryByTestId,
      queryByText,
      getAllByTestId,
      queryAllByTestId,
      findByText,
      container,
    } = render(
      <TestWrapper
        createApi={(): // eslint-disable-next-line @typescript-eslint/no-explicit-any
        any => {
          return {
            // dummy calls
            getAirmetList: mockGetAirmetList,
            postAirmet: mockPostAirmet,
            getAirmetTAC: mockGetTac,
          };
        }}
      >
        <MetInfoWrapper productType="airmet" />
      </TestWrapper>,
    );

    // Wait until airmetlist has loaded
    await waitFor(() => {
      expect(getAllByTestId('issueTime').length).toBeGreaterThan(0);
      expect(mockGetAirmetList).toHaveBeenCalledTimes(1);
    });

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeTruthy());

    expect(queryByTestId('productform-dialog')).toBeFalsy();
    fireEvent.click(getByTestId('productListCreateButton'));
    expect(getByTestId('productform-dialog')).toBeTruthy();
    expect(getByTestId('dialogTitle').textContent).toEqual('New AIRMET');
    expect(getByTestId('phenomenon').querySelector('input').value).toEqual('');

    // change phenomenon
    fireEvent.mouseDown(
      getByTestId('phenomenon').querySelector('[role="button"]'),
    );

    const menuItem = await findByText('Surface wind');
    fireEvent.click(menuItem);
    await waitFor(() => {
      expect(container.querySelector('li[data-value=SFC_WIND]')).toBeFalsy();
      expect(
        getByTestId('phenomenon').querySelector('input').previousElementSibling
          .textContent,
      ).toEqual('Surface wind');
      expect(getByTestId('phenomenon').querySelector('input').value).toEqual(
        'SFC_WIND',
      );
    });

    // save as draft and close dialog
    fireEvent.click(getByTestId('productform-dialog-draft'));

    // spinner should be shown
    await waitFor(() => {
      expect(getAllByTestId('loader')).toBeTruthy();
    });

    // post action should be called
    expect(mockPostAirmet).toHaveBeenCalledWith({
      changeStatusTo: 'DRAFT',
      airmet: expect.any(Object),
    });

    jest.advanceTimersByTime(3000);

    await waitFor(() => {
      // spinner should be gone
      expect(queryAllByTestId('loader')).toHaveLength(0);
      // form should be closed
      expect(queryByTestId('productform-dialog')).toBeFalsy();
      // list should be updated
      expect(mockGetAirmetList).toHaveBeenCalledTimes(2);
    });

    // Open again the new dialog and verify the values are empty
    fireEvent.click(getByTestId('productListCreateButton'));
    await waitFor(() => {
      expect(getByTestId('productform-dialog')).toBeTruthy();
      expect(getByTestId('dialogTitle').textContent).toEqual('New AIRMET');
      expect(getByTestId('phenomenon').querySelector('input').value).toEqual(
        '',
      );
    });
  });

  it('should open the dialog for the correct airmet when clicking a row', async () => {
    const { getAllByTestId, getByTestId, queryByTestId, queryByText } = render(
      <TestWrapper>
        <MetInfoWrapper productType="airmet" />
      </TestWrapper>,
    );

    // Wait until airmetlist has loaded
    await waitFor(() =>
      expect(getAllByTestId('issueTime').length).toBeGreaterThan(0),
    );

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await waitFor(() => expect(queryByText(fakeAirmetTAC)).toBeTruthy());

    expect(queryByTestId('productform-dialog')).toBeFalsy();
    await waitFor(() => expect(getAllByTestId('productListItem')).toBeTruthy());

    fireEvent.click(getAllByTestId('productListItem')[0]);
    await waitFor(() => {
      expect(queryByText(noTAC)).toBeFalsy();
      expect(getByTestId('productform-dialog')).toBeTruthy();
      expect(getByTestId('dialogTitle').textContent).toEqual(
        'AIRMET Isolated thunderstorm(s) with hail - saved as draft',
      );
    });
  });

  it('should open the dialog for the correct sigmet when clicking a second time on the same row', async () => {
    const {
      getAllByTestId,
      getByTestId,
      queryByTestId,
      queryByText,
      findAllByText,
      findByText,
    } = render(
      <TestWrapper>
        <MetInfoWrapper productType="sigmet" />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await findAllByText('Issue time');

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await findByText(fakeSigmetTAC);

    expect(queryByTestId('productform-dialog')).toBeFalsy();

    await waitFor(() => expect(getAllByTestId('productListItem')).toBeTruthy());
    // click on the first row
    fireEvent.click(getAllByTestId('productListItem')[0]);
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(getByTestId('phenomenon').querySelector('input').value).toEqual(
      (fakeSigmetList[0].sigmet as Sigmet).phenomenon,
    );

    expect(getByTestId('productform-dialog')).toBeTruthy();
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'SIGMET Obscured thunderstorm(s) - saved as draft',
    );
    // close the dialog
    fireEvent.click(getByTestId('contentdialog-close'));
    await waitFor(() =>
      expect(queryByTestId('productform-dialog')).toBeFalsy(),
    );
    // click again on the same row
    fireEvent.click(getAllByTestId('productListItem')[0]);
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(getByTestId('productform-dialog')).toBeTruthy();
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'SIGMET Obscured thunderstorm(s) - saved as draft',
    );
    expect(getByTestId('phenomenon').querySelector('input').value).toEqual(
      (fakeSigmetList[0].sigmet as Sigmet).phenomenon,
    );
    // close it again and check confirmation dialog does not show
    fireEvent.click(getByTestId('contentdialog-close'));
    await waitFor(() =>
      expect(queryByTestId('productform-dialog')).toBeFalsy(),
    );
  });

  it('should open the dialog for the correct airmet when clicking a second time on the same row', async () => {
    const {
      getAllByTestId,
      getByTestId,
      queryByTestId,
      queryByText,
      findAllByText,
      findByText,
    } = render(
      <TestWrapper>
        <MetInfoWrapper productType="airmet" />
      </TestWrapper>,
    );

    // Wait until sigmetlist has loaded
    await findAllByText('Issue time');

    // Wait until the TAC has loaded
    fireEvent.mouseOver(getAllByTestId('productlistrow-tacicon')[0]);
    await findByText(fakeAirmetTAC);

    expect(queryByTestId('productform-dialog')).toBeFalsy();

    await waitFor(() => expect(getAllByTestId('productListItem')).toBeTruthy());
    // click on the first row
    fireEvent.click(getAllByTestId('productListItem')[0]);
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(getByTestId('phenomenon').querySelector('input').value).toEqual(
      (fakeAirmetList[0].airmet as Airmet).phenomenon,
    );

    expect(getByTestId('productform-dialog')).toBeTruthy();
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Isolated thunderstorm(s) with hail - saved as draft',
    );
    // close the dialog
    fireEvent.click(getByTestId('contentdialog-close'));
    await waitFor(() =>
      expect(queryByTestId('productform-dialog')).toBeFalsy(),
    );
    // click again on the same row
    fireEvent.click(getAllByTestId('productListItem')[0]);
    await waitFor(() => expect(queryByText(noTAC)).toBeFalsy());

    expect(getByTestId('productform-dialog')).toBeTruthy();
    expect(getByTestId('dialogTitle').textContent).toEqual(
      'AIRMET Isolated thunderstorm(s) with hail - saved as draft',
    );
    expect(getByTestId('phenomenon').querySelector('input').value).toEqual(
      (fakeAirmetList[0].airmet as Airmet).phenomenon,
    );
  });

  describe('ensure polling works correctly', () => {
    beforeEach(() => {
      jest.useFakeTimers();
    });
    afterEach(() => {
      jest.clearAllTimers();
      jest.useRealTimers();
    });
    it('should refetch the list periodically if there is an error', async () => {
      const fetchNewData = jest.fn();
      jest.spyOn(utils, 'useApi').mockReturnValueOnce({
        isLoading: false,
        error: new Error('error'),
        result: null,
        fetchApiData: fetchNewData,
      });

      const { getByTestId } = render(
        <TestWrapper>
          <MetInfoWrapper productType="sigmet" />
        </TestWrapper>,
      );

      expect(getByTestId('productList-alert')).toBeTruthy();
      jest.runOnlyPendingTimers();

      // Test new data has been fetched
      await waitFor(() => expect(fetchNewData).toHaveBeenCalled());

      jest.runOnlyPendingTimers();
      expect(fetchNewData).toHaveBeenCalledTimes(2);
    });

    it('should not call periodically if list still loading', async () => {
      const fetchNewData = jest.fn();
      jest.spyOn(utils, 'useApi').mockReturnValueOnce({
        isLoading: true,
        error: null,
        result: null,
        fetchApiData: fetchNewData,
      });

      const { getByTestId } = render(
        <TestWrapper>
          <MetInfoWrapper productType="sigmet" />
        </TestWrapper>,
      );

      expect(getByTestId('productList-skeleton')).toBeTruthy();
      jest.runOnlyPendingTimers();

      // Test new data has been fetched
      expect(fetchNewData).not.toHaveBeenCalled();

      jest.runOnlyPendingTimers();
      expect(fetchNewData).not.toHaveBeenCalled();
    });
    it('should refetch the list periodically if there is a list loaded', async () => {
      const fetchNewData = jest.fn();
      jest.spyOn(utils, 'useApi').mockReturnValueOnce({
        isLoading: false,
        error: null,
        result: fakeAirmetList,
        fetchApiData: fetchNewData,
      });

      const { getAllByTestId } = render(
        <TestWrapper>
          <MetInfoWrapper productType="airmet" />
        </TestWrapper>,
      );
      // Wait until airmetlist has loaded
      await waitFor(() =>
        expect(getAllByTestId('productListItem')).toBeTruthy(),
      );
      jest.runOnlyPendingTimers();

      // Test new data has been fetched
      await waitFor(() => expect(fetchNewData).toHaveBeenCalledTimes(1));

      jest.runOnlyPendingTimers();
      await waitFor(() => expect(fetchNewData).toHaveBeenCalledTimes(2));
    });
  });
});
