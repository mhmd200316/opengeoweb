![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/libs/sigmet-airmet/package.json)
![coverage](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg?job=test-sigmet-airmet)

# sigmet-airmet

React component library with Sigmet and Airmet components for the opengeoweb project.
This library was generated with [Nx](https://nx.dev).

## Installation

```
npm install @opengeoweb/sigmet-airmet
```

## Use

You can use any component exported from sigmet-airmet by importing them, for example:

```
import { MetInfoWrapper } from '@opengeoweb/sigmet-airmet'
```

### API

Documentation of the API can be found on Swagger (make sure you're connected to the KNMI network).
ADD API DOCUMENTATION

The front-end uses the [axios](https://github.com/axios/axios) library to fetch data from the api. Importing components from the sigmet-airmet lib should be wrapped by the ApiProvider component with a specified baseURL. Example:

```
import { ApiProvider } from '@opengeoweb/api';
import { MetInfoWrapper } from '@opengeoweb/sigmet-airmet';

const myAuth = {
  username: 'string',
  token: 'string',
  refresh_token: 'string',
};

const Component = () => {
  const [auth, onSetAuth] = React.useState(myAuth);

  return (
      <ApiProvider
        baseURL="http://test.com"
        appURL="http://app.com"
        auth={auth}
        onSetAuth={onSetAuth}
        createApi={createApi}
        authTokenUrl="url/tokenrefresh"
      >
        <MetInfoWrapper productType='sigmet' />
      </ApiProvider>
  )}
```

## Documentation

https://opengeoweb.gitlab.io/opengeoweb/docs/sigmet-airmet/
