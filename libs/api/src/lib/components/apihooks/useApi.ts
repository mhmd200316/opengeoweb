/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import axios from 'axios';
import { useIsMounted } from '@opengeoweb/shared';
import { useDebounce } from './useDebounce';
/*
  Summary. Accepts a promise base apiCall with params, and returns an object with result
  
  @param {Promise} apiCall: api request Promise
  @param {object} params: params for api request   
  @return {isLoading: boolean, error: Error, results: any} 

  @example: 
   const { isLoading, error, result } = useApi(api.getList, {
    id: 'xxxxx',
  });
*/
export interface ApiHookProps {
  isLoading: boolean;
  error: Error;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  result: any;
  fetchApiData?: (params) => Promise<void>;
  clearResults?: () => void;
}

const generateRandomId = (): string =>
  `-${Math.random().toString(36).substr(2, 9)}`;

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const useApi = (apiCall, params = null): ApiHookProps => {
  const { isMounted } = useIsMounted();
  const [id] = React.useState(generateRandomId());
  const [isLoading, setIsloading] = React.useState(true);
  const [error, setError] = React.useState(null);
  const [result, setResult] = React.useState(null);
  const debouncedParams = useDebounce(JSON.stringify(params), 300);

  const fetchApiData = async (_params = null): Promise<void> => {
    try {
      setIsloading(true);
      setError(null);
      const response = await apiCall(_params, id);
      if (isMounted.current) {
        setResult(
          Array.isArray(response) ? response.map((d) => d.data) : response.data,
        );

        setIsloading(false);
        setError(null);
      }
    } catch (newError) {
      if (isMounted.current) {
        if (axios.isCancel(newError)) {
          setIsloading(false);
        } else {
          setError(newError);
          setIsloading(false);
        }
      }
    }
  };

  React.useEffect(
    () => {
      fetchApiData(params);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [debouncedParams],
  );

  const clearResults = (): void => {
    setIsloading(true);
    setResult(null);
  };

  return { isLoading, error, result, clearResults, fetchApiData };
};
