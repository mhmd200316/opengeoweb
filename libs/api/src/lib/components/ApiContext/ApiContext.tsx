/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { CreateApi, Credentials } from './types';

const ApiContext = React.createContext({
  api: null,
});

interface ApiContextState<ApiType> {
  api: ApiType;
}

interface ApiProviderProps {
  children: React.ReactNode;
  baseURL?: string;
  appURL?: string;
  auth?: Credentials;
  onSetAuth?: (cred: Credentials) => void;
  authTokenUrl?: string;
  createApi: CreateApi;
  authClientId?: string;
}

export const ApiProvider: React.FC<ApiProviderProps> = ({
  children,
  baseURL,
  appURL,
  auth,
  onSetAuth,
  authTokenUrl,
  authClientId,
  createApi,
}: ApiProviderProps) => {
  const api = createApi(
    baseURL,
    appURL,
    auth,
    onSetAuth,
    authTokenUrl,
    authClientId,
  );
  return (
    <ApiContext.Provider
      value={{
        api,
      }}
    >
      {children}
    </ApiContext.Provider>
  );
};

export function useApiContext<ApiType>(): ApiContextState<ApiType> {
  const context = React.useContext(ApiContext);
  return context;
}

export default ApiContext;
