/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import axios, { AxiosInstance } from 'axios';
import { Credentials } from './types';

const DEFAULT_TIMEOUT = 15000;

export const createApiInstance = (
  url: string,
  appUrl: string,
  auth: Credentials,
  setAuth: (cred: Credentials) => void,
  authTokenUrl: string,
  authClientId: string,
  timeout = DEFAULT_TIMEOUT,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): any => {
  const axiosInstance = axios.create({
    baseURL: url,
    headers: {},
    timeout,
  });

  // Request interceptor for API calls
  axiosInstance.interceptors.request.use(
    async (config) => {
      const newConfig = {
        ...config,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded', // TODO temporary default Content-Type
          ...config.headers, // use header settings from config parameters
          Authorization: `Bearer ${auth.token}`,
          Accept: 'application/json',
        },
      };
      return newConfig;
    },
    (error) => {
      Promise.reject(error);
    },
  );

  const refreshAccessToken = (): Promise<string> => {
    // Refresh token request with a new axios instance
    // without request interceptor
    const tokenAxiosInstance = axios.create({
      headers: {},
      timeout,
    });
    const refreshPayload = {
      refresh_token: auth.refresh_token,
      redirect_uri: `${appUrl}/code`,
      grant_type: 'refresh_token',
      client_id: authClientId,
    };
    /* Send data in the "application/x-www-form-urlencoded" format.
      If only JSON is supported, use Axios' default content type ("application/x-www-form-urlencoded"). */
    const useDefaultContentType = authTokenUrl.includes('amazonaws.com');
    const data = useDefaultContentType
      ? refreshPayload
      : new URLSearchParams(refreshPayload);

    const config = {
      headers: {
        'Content-Type': useDefaultContentType
          ? 'application/json'
          : 'application/x-www-form-urlencoded',
      },
    };
    return tokenAxiosInstance.post(authTokenUrl, data, config);
  };

  // Response interceptor for API calls
  axiosInstance.interceptors.response.use(
    (response) => response,
    async (error) => {
      const originalRequest = error.config;
      if (
        error.response &&
        error.response.status &&
        (error.response.status === 401 || error.response.status === 403) &&
        !originalRequest.inRetry
      ) {
        originalRequest.inRetry = true;
        const refreshedToken = await refreshAccessToken();

        // eslint-disable-next-line no-param-reassign
        auth.token = refreshedToken['data']['body']['access_token'];
        setAuth(auth);
        return axiosInstance(originalRequest);
      }
      return Promise.reject(error);
    },
  );

  return axiosInstance;
};

export const fakeApiRequest = (signal = null): Promise<void> =>
  new Promise((resolve, reject) => {
    const timer = setTimeout(() => {
      resolve();
    }, 300);
    signal?.signal.addEventListener('abort', () => {
      clearTimeout(timer);
      reject(new Error('canceled'));
    });
  });

export const createFakeApiInstance = (): AxiosInstance =>
  ({
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars,
    get: (_url: string, _params?: unknown): Promise<void> => fakeApiRequest(),
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars,
    put: (_url, _params?): Promise<void> => fakeApiRequest(),
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars,
    post: (_url, _params?): Promise<void> => fakeApiRequest(),
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
    delete: (_url, _params?): Promise<void> => fakeApiRequest(),
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars, @typescript-eslint/explicit-module-boundary-types
    patch: (_url, _params?, _signal?): Promise<void> => fakeApiRequest(_signal),
  } as AxiosInstance);
