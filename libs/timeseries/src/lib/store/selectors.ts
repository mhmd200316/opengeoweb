/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Plot, PlotPreset } from '../components/TimeSeries/types';
import { TimeSeriesModuleState } from './types';

export const getPlotState = (
  state: TimeSeriesModuleState,
): PlotPreset | undefined => {
  return state.timeSeries?.plotPreset;
};

export const getMapId = (state: TimeSeriesModuleState): string | undefined =>
  state.timeSeries?.plotPreset.mapId;

export const getPlotWithParameters = (
  state: TimeSeriesModuleState,
  selectPlotId: string,
): Plot | undefined => {
  if (!state.timeSeries?.plotPreset || !selectPlotId) return undefined;

  const { plotPreset } = state.timeSeries;
  const plot = plotPreset.plots.find((plot) => plot.plotId === selectPlotId);
  if (!plot) return undefined;

  const parameters = plotPreset.parameters.filter(
    (parameter) => parameter.plotId === selectPlotId,
  );

  return { ...plot, parameters };
};
