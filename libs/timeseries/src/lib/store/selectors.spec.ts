/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { TimeSeriesModuleState } from './types';
import { getMapId, getPlotState, getPlotWithParameters } from './selectors';
import { Parameter } from '../components/TimeSeries/types';

describe('store/ui/selectors', () => {
  const mapId = 'mapId';
  const plotId1 = 'plotId1';
  const service0 = {
    id: 'service0',
    serviceUrl: 'serviceUrl',
  };
  const parameter1: Parameter = {
    plotId: plotId1,
    unit: '°C',
    propertyName: 'Temperature',
    plotType: 'line',
    serviceId: service0.id,
  };
  const parameter2: Parameter = {
    plotId: plotId1,
    unit: '°C',
    propertyName: 'DewPoint',
    plotType: 'line',
    serviceId: service0.id,
  };
  const plot1 = {
    title: 'Plot 1',
    plotId: plotId1,
  };

  const mockStore: TimeSeriesModuleState = {
    timeSeries: {
      plotPreset: {
        mapId,
        plots: [
          plot1,
          {
            title: 'Plot 2',
            plotId: 'plotId2',
          },
        ],
        parameters: [
          parameter1,
          parameter2,
          {
            plotId: 'plotId2',
            unit: '%',
            propertyName: 'Humidity',
            plotType: 'line',
            serviceId: service0.id,
          },
        ],
        services: [service0],
      },
    },
  };

  const emptyMockStore: TimeSeriesModuleState = {
    timeSeries: undefined,
  };
  describe('getPlotState', () => {
    it('should return plots', () => {
      expect(getPlotState(mockStore)).toEqual(mockStore.timeSeries.plotPreset);
    });
    it('should return undefined if no plots', () => {
      expect(getPlotState(emptyMockStore)).toEqual(undefined);
    });
    describe('getMapId', () => {
      it('should return mapId', () => {
        expect(getMapId(mockStore)).toEqual(mapId);
      });
      it('should return undefined if no state', () => {
        expect(getMapId(emptyMockStore)).toEqual(undefined);
      });
    });

    describe('getPlotWithParameters', () => {
      it('should return plot with parameters', () => {
        const plot = getPlotWithParameters(mockStore, plotId1);
        expect(plot).toEqual({
          ...plot1,
          parameters: [parameter1, parameter2],
        });
      });
      it('should return undefined if no state', () => {
        expect(getPlotWithParameters(emptyMockStore, plotId1)).toEqual(
          undefined,
        );
      });
      it('should return undefined if no plot id', () => {
        expect(getPlotWithParameters(mockStore, '')).toEqual(undefined);
      });
      it('should return undefined if plot id is not in store', () => {
        expect(getPlotWithParameters(mockStore, 'plotIdNotInStore')).toEqual(
          undefined,
        );
      });
    });
  });
});
