/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import produce from 'immer';
import { Parameter, PlotPreset } from '../components/TimeSeries/types';
import { TimeSeriesStoreType, UpdateParameterPayload } from './types';

export const initialState: TimeSeriesStoreType = {
  plotPreset: undefined,
};

const slice = createSlice({
  initialState,
  name: 'timeseries',
  reducers: {
    registerPlotPreset: (draft, action: PayloadAction<PlotPreset>) => {
      const plotPresetWithParameterIds = produce(
        action.payload,
        (draftPreset) => {
          draftPreset.parameters.forEach((draftParameter) => {
            draftParameter.id = generateParameterId();
          });
        },
      );
      draft.plotPreset = plotPresetWithParameterIds;
    },
    deletePlot: (draft, action: PayloadAction<string>) => {
      draft.plotPreset.plots = draft.plotPreset.plots.filter((plot) => {
        return plot.plotId !== action.payload;
      });
      draft.plotPreset.parameters = draft.plotPreset.parameters.filter(
        (parameter) => {
          return parameter.plotId !== action.payload;
        },
      );
    },
    togglePlot: (draft, action: PayloadAction<string>) => {
      draft.plotPreset.plots = draft.plotPreset.plots.map((plot) => {
        if (plot.plotId === action.payload) {
          const plotIsEnabled = plot.enabled !== false;
          return { ...plot, enabled: !plotIsEnabled };
        }
        return plot;
      });
    },
    addPlot: (draft, action: PayloadAction<{ title: string }>) => {
      draft.plotPreset.plots.push({
        title: action.payload.title,
        plotId: generateTimeSeriesId(),
      });
    },
    deleteParameter: (draft, action: PayloadAction<{ id: string }>) => {
      draft.plotPreset.parameters = draft.plotPreset.parameters.filter(
        (parameter) => {
          return parameter.id !== action.payload.id;
        },
      );
    },
    addParameter: (draft, action: PayloadAction<Parameter>) => {
      draft.plotPreset.parameters.push({
        ...action.payload,
        id: generateParameterId(),
      });
    },
    toggleParameter: (draft, action: PayloadAction<{ id: string }>) => {
      draft.plotPreset.parameters = draft.plotPreset.parameters.map(
        (parameter) => {
          if (parameter.id === action.payload.id) {
            const parameterIsEnabled = parameter.enabled !== false;
            return { ...parameter, enabled: !parameterIsEnabled };
          }
          return parameter;
        },
      );
    },
    updateParameter: (draft, action: PayloadAction<UpdateParameterPayload>) => {
      const { id, color, plotType } = action.payload.parameter;
      draft.plotPreset.parameters = draft.plotPreset.parameters.map(
        (parameter): Parameter => {
          if (parameter.id === id) {
            return { ...parameter, color, plotType };
          }
          return parameter;
        },
      );
    },
  },
});

export const { reducer, actions } = slice;

let generatedTimeseriesIds = 0;
const generateTimeSeriesId = (): string => {
  generatedTimeseriesIds += 1;
  return `timeseriesid_${generatedTimeseriesIds}`;
};

let generatedParameterIds = 0;
const generateParameterId = (): string => {
  generatedParameterIds += 1;
  return `timeseriesid_${generatedParameterIds}`;
};
