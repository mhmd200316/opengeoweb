/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import produce from 'immer';
import { initialState, reducer } from './reducer';
import { Parameter, PlotPreset } from '../components/TimeSeries/types';
import { actions } from '.';
import { TimeSeriesStoreType } from './types';

const {
  deletePlot,
  registerPlotPreset,
  togglePlot,
  deleteParameter,
  addParameter,
  toggleParameter,
  addPlot,
  updateParameter,
} = actions;

describe('store/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  const service = {
    id: 'fmi',
    serviceUrl:
      'https://for.weather.fmibeta.com/sofp/collections/hirlam_timeseries/items?f=json&limit=100',
  };
  const plot1 = { plotId: 'plotId1', title: 'Plot 1' };
  const plot2 = { plotId: 'plotId2', title: 'Plot 2' };
  const parameter1: Parameter = {
    id: 'parameter1',
    plotId: plot1.plotId,
    unit: '°C',
    propertyName: 'Temperature',
    plotType: 'line',
    serviceId: service.id,
  };
  const plotPreset: PlotPreset = {
    mapId: 'TimeseriesMap',
    plots: [plot1, plot2],
    parameters: [
      parameter1,
      {
        id: 'parameter2',
        plotId: plot2.plotId,
        unit: 'mm',
        propertyName: 'Precipitation1h',
        plotType: 'bar',
        serviceId: service.id,
      },
    ],
    services: [service],
  };

  const mockTimeSeriesState: TimeSeriesStoreType = {
    plotPreset,
  };

  it('should register plot', () => {
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset.parameters.forEach((draftParameter) => {
        draftParameter.id = expect.any(String);
      });
    });
    expect(reducer(undefined, registerPlotPreset(plotPreset))).toEqual(
      expectedState,
    );
  });

  it('should delete plot', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset: {
        mapId: 'TimeseriesMap',
        plots: [plot1],
        parameters: [parameter1],
        services: [service],
      },
    };
    expect(reducer({ plotPreset }, deletePlot(plot2.plotId))).toEqual(
      expectedState,
    );
  });
  it('should toggle plot', () => {
    const initialState: TimeSeriesStoreType = {
      plotPreset,
    };

    const state = reducer(initialState, togglePlot('plotId2'));
    const expectedState1 = produce(initialState, (draft) => {
      draft.plotPreset.plots[1].enabled = false;
    });
    expect(state).toEqual(expectedState1);

    const expectedState2 = produce(expectedState1, (draft) => {
      draft.plotPreset.plots[1].enabled = true;
    });
    expect(reducer(state, togglePlot('plotId2'))).toEqual(expectedState2);
  });

  it('should delete plot parameter', () => {
    const idToDelete = 'id1';
    const initialPlotPreset = produce(plotPreset, (draft) => {
      draft.parameters.push({
        id: idToDelete,
        plotId: plot1.plotId,
        unit: 'unit1',
        propertyName: 'prop1',
        plotType: 'line',
        serviceId: service.id,
      });
    });
    expect(
      reducer(
        {
          plotPreset: initialPlotPreset,
        },
        deleteParameter({ id: idToDelete }),
      ),
    ).toEqual({
      plotPreset,
    });
  });

  it('should add parameter', () => {
    const newParameter: Parameter = {
      id: expect.any(String),
      plotId: 'plotId1',
      unit: '°C',
      propertyName: 'DewPoint',
      plotType: 'line',
      serviceId: service.id,
    };
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset.parameters.push(newParameter);
    });

    expect(reducer(mockTimeSeriesState, addParameter(newParameter))).toEqual(
      expectedState,
    );
  });
  it('should toggle parameter', () => {
    const initialState: TimeSeriesStoreType = {
      plotPreset,
    };

    const idToToggle = plotPreset.parameters[1].id;

    const state1 = reducer(initialState, toggleParameter({ id: idToToggle }));
    const expectedState1 = produce(initialState, (draft) => {
      draft.plotPreset.parameters[1].enabled = false;
    });
    expect(state1).toEqual(expectedState1);

    const state2 = reducer(
      state1,
      toggleParameter({
        id: idToToggle,
      }),
    );
    const expectedState2 = produce(expectedState1, (draft) => {
      draft.plotPreset.parameters[1].enabled = true;
    });
    expect(state2).toEqual(expectedState2);
  });

  it('should add a plot', () => {
    const titleToAdd = 'Plot_2';
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset.plots.push({
        title: titleToAdd,
        plotId: expect.any(String),
      });
    });
    expect(
      reducer(mockTimeSeriesState, addPlot({ title: titleToAdd })),
    ).toEqual(expectedState);
  });

  it('should update color and type of parameter', () => {
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset.parameters[0].color = 'blue';
      draft.plotPreset.parameters[0].plotType = 'bar';
    });
    const parameterToUpdate = plotPreset.parameters[0];
    expect(
      reducer(
        mockTimeSeriesState,
        updateParameter({
          parameter: {
            ...parameterToUpdate,
            color: 'blue',
            plotType: 'bar',
          },
        }),
      ),
    ).toEqual(expectedState);
  });
});
