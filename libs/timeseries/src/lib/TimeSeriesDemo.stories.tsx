/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { MapViewConnect } from '@opengeoweb/core';
import { useDefaultMapSettings } from './storybookUtils/defaultStorySettings';
import screenPresets from './storybookUtils/screenPresets.json';
import { TimeSeriesThemeStoreProvider } from './storybookUtils/Providers';
import { store } from './storybookUtils/store';
import { TimeSeriesConnect } from './components/TimeSeries/TimeSeriesConnect';
import { PlotPreset } from './components/TimeSeries/types';
import { TimeSeriesManagerConnect } from './components/TimeSeriesManager/TimeSeriesManagerConnect';
import { ToggleThemeButton } from './storybookUtils/ToggleThemeButton';
import timeSeriesPresetLocations from './storybookUtils/timeSeriesPresetLocations.json';

const MapWithTimeSeriesManager: React.FC<{ mapId: string }> = ({ mapId }) => {
  useDefaultMapSettings({
    mapId,
  });

  const plotPreset = screenPresets[0].views.byId.Timeseries.initialProps
    .plotPreset as PlotPreset;

  return (
    <div>
      <ToggleThemeButton />
      <TimeSeriesManagerConnect />
      <div style={{ display: 'flex' }}>
        <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
          <MapViewConnect mapId={mapId} />
        </div>
        <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
          <TimeSeriesConnect
            plotPreset={{ ...plotPreset, mapId }}
            timeSeriesPresetLocations={timeSeriesPresetLocations}
          />
        </div>
      </div>
    </div>
  );
};

export const TimeSeriesDemo: React.FC = () => {
  return (
    <TimeSeriesThemeStoreProvider store={store}>
      <MapWithTimeSeriesManager mapId="mapid_1" />
    </TimeSeriesThemeStoreProvider>
  );
};

export default { title: 'application/demo' };
