/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { uiActions } from '@opengeoweb/core';
import { renderHook } from '@testing-library/react-hooks';
import React from 'react';
import configureStore from 'redux-mock-store';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { useSetupDialog } from './useSetupDialog';

describe('hooks/useSetupDialog/useSetupDialog', () => {
  it('should register the dialog and return dialog order', async () => {
    const mockStore = configureStore();
    const store = mockStore({});
    store.addModules = jest.fn();

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const wrapper = ({ children }) => (
      <TimeSeriesThemeStoreProvider store={store}>
        {children}
      </TimeSeriesThemeStoreProvider>
    );

    const { result } = renderHook(() => useSetupDialog('timeSeriesManager'), {
      wrapper,
    });

    const expectedRegisterDialogAction = uiActions.registerDialog({
      type: 'timeSeriesManager',
      setOpen: false,
    });

    expect(store.getActions()).toEqual([expectedRegisterDialogAction]);

    expect(result.current.dialogOrder).toEqual(0);

    result.current.setDialogOrder();
    const expectedSetUiOrderAction = uiActions.orderDialog({
      type: 'timeSeriesManager',
    });
    expect(store.getActions()).toEqual([
      expectedRegisterDialogAction,
      expectedSetUiOrderAction,
    ]);
  });
  it('should close the dialog when map is not present anymore', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: { byId: { otherMapId: {} }, allIds: ['otherMapId'] },
      ui: {
        order: ['timeSeriesManager'],
        dialogs: {
          timeSeriesManager: {
            activeMapId: 'mapId123',
            isOpen: true,
            type: 'timeSeriesManager',
            source: 'app',
          },
        },
      },
    });
    store.addModules = jest.fn();

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const wrapper = ({ children }) => (
      <TimeSeriesThemeStoreProvider store={store}>
        {children}
      </TimeSeriesThemeStoreProvider>
    );
    renderHook(() => useSetupDialog('timeSeriesManager'), {
      wrapper,
    });

    expect(store.getActions()[0]).toEqual(
      uiActions.setToggleOpenDialog({
        setOpen: false,
        type: 'timeSeriesManager',
      }),
    );
  });
});
