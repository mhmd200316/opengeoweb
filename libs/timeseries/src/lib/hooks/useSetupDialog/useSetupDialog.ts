/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  mapSelectors,
  uiActions,
  uiSelectors,
  uiTypes,
} from '@opengeoweb/core';

export const useSetupDialog = (
  dialogType: uiTypes.DialogType,
): {
  setDialogOrder: () => void;
  dialogOrder: number;
} => {
  const dispatch = useDispatch();

  const onCloseDialog = useCallback((): void => {
    dispatch(
      uiActions.setToggleOpenDialog({
        type: dialogType,
        setOpen: false,
      }),
    );
  }, [dialogType, dispatch]);

  const mapId = useSelector((store) =>
    uiSelectors.getDialogMapId(store, dialogType),
  );

  const isMapPresent = useSelector((store) =>
    mapSelectors.getIsMapPresent(store, mapId),
  );

  // Check to ensure the currently active map is still present on screen - if not, close the dialog
  useEffect(() => {
    if (mapId !== '' && !isMapPresent) {
      onCloseDialog();
    }
  }, [mapId, isMapPresent, onCloseDialog]);

  const registerDialog = (): void => {
    dispatch(
      uiActions.registerDialog({
        type: dialogType,
        setOpen: false,
      }),
    );
  };

  const unregisterDialog = (): void => {
    dispatch(
      uiActions.unregisterDialog({
        type: dialogType,
      }),
    );
  };

  // Register this dialog in the store
  useEffect(() => {
    registerDialog();
    return (): void => {
      unregisterDialog();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const setDialogOrder = (): void => {
    dispatch(
      uiActions.orderDialog({
        type: dialogType,
      }),
    );
  };

  const dialogOrder: number = useSelector((store) =>
    uiSelectors.getDialogOrder(store, dialogType),
  );

  return { setDialogOrder, dialogOrder };
};
