/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { useDispatch } from 'react-redux';

import {
  Paper,
  Card,
  CardContent,
  Typography,
  Select,
  MenuItem,
  SelectChangeEvent,
} from '@mui/material';
import { mapActions } from '@opengeoweb/core';
import defaultTimeseriesPreset from '../../storybookUtils/screenPresets.json';
import {
  componentsLookUp,
  InitialProps,
  SupportedComponentTypes,
} from './componentsLookUp';
import { store } from '../../storybookUtils/store';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import timeSeriesPresetLocations from '../../storybookUtils/timeSeriesPresetLocations.json';

export default {
  title: 'components/ComponentsLookUp',
};

const initialProps = defaultTimeseriesPreset[0].views.byId.Timeseries
  .initialProps as InitialProps;
const componentTypesWithInitialPresetProps: Record<string, InitialProps> = {
  TimeSeries: initialProps,
};

const availableComponentTypes = Object.keys(
  componentTypesWithInitialPresetProps,
).map((key) => key);

const DEFAULT_COMPONENT_TYPE = 'TimeSeries';

interface SelectComponentTypeProps {
  selectedComponentType: SupportedComponentTypes;
  onChangeComponentType: (preset: SupportedComponentTypes) => void;
}

const SelectComponentType: React.FC<SelectComponentTypeProps> = ({
  selectedComponentType,
  onChangeComponentType,
}: SelectComponentTypeProps) => {
  const onChangeSelect = (event: SelectChangeEvent): void => {
    onChangeComponentType(event.target.value as SupportedComponentTypes);
  };

  return (
    <Paper style={{ position: 'absolute', right: 50, top: 40, zIndex: 51 }}>
      <Card>
        <CardContent>
          <Typography variant="subtitle1">ComponentType:</Typography>
          <Select value={selectedComponentType} onChange={onChangeSelect}>
            {availableComponentTypes.map((componentType) => (
              <MenuItem key={componentType} value={componentType}>
                {componentType}
              </MenuItem>
            ))}
          </Select>
        </CardContent>
      </Card>
    </Paper>
  );
};

// Demo of componentsLookUp. This only shows the layout, button logic is not working
const Demo: React.FC = () => {
  const dispatch = useDispatch();
  const [selectedComponentType, onChangeComponentType] =
    React.useState<SupportedComponentTypes>(DEFAULT_COMPONENT_TYPE);
  const initialProps =
    componentTypesWithInitialPresetProps[selectedComponentType];

  React.useEffect(() => {
    dispatch(mapActions.registerMap({ mapId: initialProps.plotPreset.mapId }));
    dispatch(
      mapActions.setMapPinLocation({
        mapId: initialProps.plotPreset.mapId,
        mapPinLocation: { lat: 61.2, lon: 5.6 },
      }),
    );
  }, [dispatch, initialProps.plotPreset.mapId]);

  return (
    <div style={{ width: '100vw', height: '100vh' }}>
      <SelectComponentType
        selectedComponentType={selectedComponentType}
        onChangeComponentType={onChangeComponentType}
      />
      {componentsLookUp({
        componentType: selectedComponentType,
        initialProps,
        id: selectedComponentType,
        timeSeriesPresetLocations,
      })}
    </div>
  );
};

export const ComponentsLookUp: React.FC = () => (
  <TimeSeriesThemeStoreProvider store={store}>
    <Demo />
  </TimeSeriesThemeStoreProvider>
);
