/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { componentsLookUp } from './componentsLookUp';

describe('components/ComponentsLookUp', () => {
  it('should return the TimeSeries component', () => {
    const payload = {
      title: 'title',
      componentType: 'TimeSeries' as const,
      id: 'viewid-1',
      initialProps: {
        plotPreset: {
          mapId: 'map-1',
          plots: [],
          services: [],
          parameters: [],
        },
      },
    };
    const component = componentsLookUp(payload);
    expect(component).toBeDefined();
    expect(component.props['data-testid']).toEqual('timeseriesPlot');
  });

  it('should return the null if not recognized', () => {
    const payload = {
      title: 'title',
      componentType: undefined,
      id: 'viewid-1',
      initialProps: undefined,
    };
    const component = componentsLookUp(payload);
    expect(component).toBeNull();
  });
});
