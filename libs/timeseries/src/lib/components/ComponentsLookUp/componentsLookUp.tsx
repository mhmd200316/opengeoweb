/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { TimeSeriesConnect } from '../TimeSeries/TimeSeriesConnect';
import { PlotPreset, TimeSeriesPresetLocation } from '../TimeSeries/types';

interface InitialTimeseriesProps {
  plotPreset: PlotPreset;
  predefinedLocations?: TimeSeriesPresetLocation[];
}

export type SupportedComponentTypes = 'TimeSeries';

export type InitialProps = InitialTimeseriesProps;

export interface ComponentsLookUpPayload {
  componentType: SupportedComponentTypes;
  id: string;
  title?: string;
  initialProps: InitialProps;
  timeSeriesPresetLocations?: TimeSeriesPresetLocation[];
}

/**
 * The lookup table is for registering your own components with the screenmanager.
 * @param payload
 * @returns
 */
export const componentsLookUp = ({
  componentType,
  initialProps,
  timeSeriesPresetLocations,
}: ComponentsLookUpPayload): React.ReactElement => {
  switch (componentType) {
    case 'TimeSeries': {
      return (
        <TimeSeriesConnect
          plotPreset={initialProps.plotPreset}
          data-testid="timeseriesPlot"
          timeSeriesPresetLocations={timeSeriesPresetLocations}
        />
      );
    }
    default:
      return null;
  }
};
