/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Add } from '@opengeoweb/theme';
import { ManagerButton } from '@opengeoweb/shared';
import { PlotPreset } from '../TimeSeries/types';

interface AddPlotButtonProps {
  handleClick: (title: string) => void;
  plotState: PlotPreset;
}

export const AddPlotButton: React.FC<AddPlotButtonProps> = ({
  handleClick,
  plotState,
}) => {
  const onClick = (): void => {
    const titleRegex = /\d+/;
    const plotIndex: number = plotState.plots.length
      ? +plotState.plots[plotState.plots.length - 1].title.match(titleRegex)
      : 0;
    handleClick(`Plot ${plotIndex + 1}`);
  };

  return (
    <ManagerButton
      tooltipTitle="Add new plot"
      onClick={onClick}
      icon={<Add />}
      testId="timeSeriesAddPlotButton"
    />
  );
};
