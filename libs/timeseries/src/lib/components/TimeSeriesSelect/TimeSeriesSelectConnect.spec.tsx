/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { last } from 'lodash';
import { uiActions } from '@opengeoweb/core';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { TimeSeriesSelectConnect } from './TimeSeriesSelectConnect';
import { TimeSeriesModuleState } from '../../store/types';
import { actions } from '../../store';

describe('components/TimeSeriesSelect/TimeSeriesSelectConnect', () => {
  const mockMapId = 'mockMapId';
  const coreStore = {
    webmap: { byId: { mockMapId: {} }, allIds: [mockMapId] },
    ui: {
      order: ['timeSeriesSelect'],
      dialogs: {
        timeSeriesSelect: {
          activeMapId: mockMapId,
          isOpen: true,
          type: 'timeSeriesSelect',
          source: 'app',
        },
      },
    },
  };

  it('should close the dialog if plot is not in store anymore', () => {
    const mockStore = configureStore();

    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [{ plotId: 'plotId1', title: 'title' }],
          services: [],
          parameters: [],
        },
      },
    };

    const store = mockStore({ ...coreStore, ...timeSeriesStore });
    store.addModules = jest.fn();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId="plotIdNotInStore" />
      </TimeSeriesThemeStoreProvider>,
    );
    expect(store.getActions()).toContainEqual(
      uiActions.setToggleOpenDialog({
        setOpen: false,
        type: 'timeSeriesSelect',
      }),
    );
  });

  it('should handle adding and removing parameters', () => {
    const mockStore = configureStore();
    const plotId = 'plotId1';
    const parameterId = 'parameter1';
    const service = {
      id: 'fmi',
      serviceUrl: 'serviceUrl',
    };
    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [
            {
              title: 'Plot 1',
              plotId,
            },
          ],
          parameters: [
            {
              id: parameterId,
              plotId,
              plotType: 'line',
              propertyName: 'Temperature',
              unit: '°C',
              serviceId: service.id,
            },
          ],
          services: [service],
        },
      },
    };
    const store = mockStore({
      ...coreStore,
      ...timeSeriesStore,
    });
    store.addModules = jest.fn();
    const { getByRole, getAllByRole } = render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId={plotId} />
      </TimeSeriesThemeStoreProvider>,
    );

    const removeTemperatureButton = getByRole('button', {
      name: 'Remove',
    });
    fireEvent.click(removeTemperatureButton);
    const deleteParameterAction = actions.deleteParameter({
      id: parameterId,
    });
    expect(last(store.getActions())).toEqual(deleteParameterAction);

    const addButtons = getAllByRole('button', { name: 'Add' });
    fireEvent.click(addButtons[0]);
    const addParameterAction = actions.addParameter({
      plotId: 'plotId1',
      plotType: 'line',
      propertyName: 'Pressure',
      unit: 'hPa',
      serviceId: service.id,
    });
    expect(last(store.getActions())).toEqual(addParameterAction);
  });
});
