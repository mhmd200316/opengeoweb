/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { uiActions, uiSelectors } from '@opengeoweb/core';
import { TimeSeriesSelect } from './TimeSeriesSelect';
import { Parameter, Plot } from '../TimeSeries/types';
import { getPlotWithParameters } from '../../store/selectors';
import { useSetupDialog } from '../../hooks/useSetupDialog/useSetupDialog';
import { actions } from '../../store';

const DIALOG_TYPE = 'timeSeriesSelect';

export const TimeSeriesSelectConnect: React.FC<{ selectPlotId: string }> = ({
  selectPlotId,
}) => {
  const { setDialogOrder, dialogOrder } = useSetupDialog(DIALOG_TYPE);

  const dispatch = useDispatch();

  const isOpenInStore = useSelector((store) =>
    uiSelectors.getisDialogOpen(store, DIALOG_TYPE),
  );

  const plot: Plot | undefined = useSelector((state) =>
    getPlotWithParameters(state, selectPlotId),
  );

  const onClose = React.useCallback(
    () =>
      dispatch(
        uiActions.setToggleOpenDialog({
          type: DIALOG_TYPE,
          setOpen: false,
        }),
      ),
    [dispatch],
  );

  // If select dialog for a plot is open but the plot is deleted, close the dialog
  React.useEffect(() => {
    if (isOpenInStore && !plot) {
      onClose();
    }
  }, [plot, isOpenInStore, onClose]);

  const handleAddOrRemoveClick = (
    parameter: Parameter,
    plotHasParameter: boolean,
  ): void => {
    if (plotHasParameter) {
      dispatch(actions.deleteParameter({ id: parameter.id }));
    } else {
      dispatch(actions.addParameter(parameter));
    }
  };

  return plot ? (
    <TimeSeriesSelect
      isOpen={isOpenInStore}
      onClose={onClose}
      onMouseDown={setDialogOrder}
      order={dialogOrder}
      selectPlot={plot}
      handleAddOrRemoveClick={handleAddOrRemoveClick}
    />
  ) : null;
};
