/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { darkTheme, lightTheme } from '@opengeoweb/theme';
import configureStore from 'redux-mock-store';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { PlotPreset } from '../TimeSeries/types';
import { TimeSeriesManager } from './TimeSeriesManager';

const TimeSeriesManagerStory: React.FC = () => {
  return (
    <TimeSeriesManager
      isOpen={true}
      onClose={(): void => {}}
      onMouseDown={(): void => {}}
      order={1}
      addPlot={(): void => {}}
      deleteParameter={(): void => {}}
      deletePlot={(): void => {}}
      plotState={plotPreset}
      setSelectPlotId={(): void => {}}
      toggleParameter={(): void => {}}
      togglePlot={(): void => {}}
      updateParameter={(): void => {}}
    />
  );
};

const plotPreset: PlotPreset = {
  mapId: 'TimeseriesMap',
  plots: [
    {
      title: 'Plot 1',
      plotId: 'Plot_1',
    },
    {
      title: 'Plot 2',
      plotId: 'Plot_2',
    },
  ],
  parameters: [
    {
      plotId: 'Plot_1',
      unit: '°C',
      propertyName: 'Temperature',
      plotType: 'line',
      serviceId: 'fmi',
    },
    {
      plotId: 'Plot_2',
      unit: 'mm',
      propertyName: 'Precipitation1h',
      plotType: 'bar',
      serviceId: 'fmi',
    },
  ],
  services: [
    {
      id: 'fmi',
      serviceUrl:
        'https://for.weather.fmibeta.com/sofp/collections/hirlam_timeseries/items?f=json&limit=100',
    },
  ],
};

const mockStore = configureStore();
const store = mockStore();
store.addModules = (): void => {};

export const TimeSeriesManagerDemoLightTheme = (): React.ReactElement => (
  <TimeSeriesThemeStoreProvider store={store} theme={lightTheme}>
    <TimeSeriesManagerStory />
  </TimeSeriesThemeStoreProvider>
);

export const TimeSeriesManagerDemoDarkTheme = (): React.ReactElement => (
  <TimeSeriesThemeStoreProvider store={store} theme={darkTheme}>
    <TimeSeriesManagerStory />
  </TimeSeriesThemeStoreProvider>
);

TimeSeriesManagerDemoLightTheme.storyName =
  'TimeSeriesManager light theme (takeSnapshot)';
TimeSeriesManagerDemoDarkTheme.storyName =
  'TimeSeriesManager dark theme (takeSnapshot)';

export default { title: 'components/TimeSeriesManager' };
