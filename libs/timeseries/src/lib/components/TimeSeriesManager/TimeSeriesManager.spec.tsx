/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { fireEvent, render } from '@testing-library/react';

import React from 'react';
import configureStore from 'redux-mock-store';
import { TimeSeriesManager } from './TimeSeriesManager';
import { Parameter, PlotPreset } from '../TimeSeries/types';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';

describe('src/components/TimeSeriesManager/TimeSeriesManager', () => {
  it('should render rows, delete row, toggle row visibility, change color and type of parameter', () => {
    const service = {
      id: 'fmi',
      serviceUrl:
        'https://for.weather.fmibeta.com/sofp/collections/hirlam_timeseries/items?f=json&limit=100',
    };
    const parameter1: Parameter = {
      id: 'parameter1',
      plotId: 'Plot_1',
      unit: '°C',
      propertyName: 'Temperature',
      plotType: 'line',
      serviceId: service.id,
    };

    const plotPreset: PlotPreset = {
      mapId: 'TimeseriesMap',
      plots: [
        {
          title: 'Plot 1',
          plotId: 'Plot_1',
        },
        {
          title: 'Plot 2',
          plotId: 'Plot_2',
        },
      ],
      parameters: [
        parameter1,
        {
          plotId: 'Plot_2',
          unit: 'mm',
          propertyName: 'Precipitation1h',
          plotType: 'bar',
          serviceId: service.id,
        },
      ],
      services: [service],
    };
    const addPlot = jest.fn();
    const deletePlot = jest.fn();
    const deleteParameter = jest.fn();
    const togglePlot = jest.fn();
    const setSelectPlotId = jest.fn();
    const toggleParameter = jest.fn();
    const updateParameter = jest.fn();

    const mockStore = configureStore();
    const store = mockStore();
    store.addModules = jest.fn();

    const { getByText, getAllByTestId, getByTestId, getByRole } = render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManager
          isOpen={true}
          onClose={jest.fn()}
          onMouseDown={jest.fn()}
          order={1}
          plotState={plotPreset}
          addPlot={addPlot}
          deletePlot={deletePlot}
          togglePlot={togglePlot}
          deleteParameter={deleteParameter}
          toggleParameter={toggleParameter}
          setSelectPlotId={setSelectPlotId}
          updateParameter={updateParameter}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(getByText('Plot 1')).toBeTruthy();
    expect(getByText('Temperature')).toBeTruthy();
    expect(getAllByTestId('timeSeriesSelectButton')[0]).toBeTruthy();
    expect(getByTestId('timeSeriesAddPlotButton')).toBeTruthy();

    expect(getByText('Plot 2')).toBeTruthy();
    expect(getByText('Precipitation1h')).toBeTruthy();

    fireEvent.click(getAllByTestId('timeSeriesAddPlotButton')[0]);
    expect(addPlot).toBeCalledTimes(1);

    fireEvent.click(getAllByTestId('deleteButton')[0]);
    expect(deletePlot).toBeCalledWith('Plot_1');

    fireEvent.click(getAllByTestId('deleteButton')[1]);
    expect(deleteParameter).toBeCalledWith(parameter1.id);

    const toggleButtons = getAllByTestId('enableButton');
    fireEvent.click(toggleButtons[0]);
    expect(togglePlot).toBeCalledWith('Plot_1');

    fireEvent.click(toggleButtons[1]);
    expect(toggleParameter).toBeCalledWith(parameter1.id);

    fireEvent.click(getAllByTestId('timeSeriesSelectButton')[0]);
    expect(setSelectPlotId).toBeCalledWith('Plot_1');

    fireEvent.mouseDown(getByRole('button', { name: /line/i }));
    fireEvent.click(getByRole('option', { name: /bar/i }));
    expect(updateParameter).toBeCalledWith({
      ...parameter1,
      plotType: 'bar',
    });

    fireEvent.mouseDown(getByRole('button', { name: /orange/i }));
    fireEvent.click(getByRole('option', { name: /yellow/i }));
    expect(updateParameter).toBeCalledWith({
      ...parameter1,
      color: 'yellow',
    });
  });
});
