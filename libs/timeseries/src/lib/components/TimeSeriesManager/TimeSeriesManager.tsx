/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC } from 'react';
import { Box, Grid } from '@mui/material';
import { groupBy } from 'lodash';
import { ToolContainerDraggable } from '@opengeoweb/shared';
import { Parameter, Plot, PlotPreset } from '../TimeSeries/types';
import { PlotRows } from './PlotRows';
import {
  PLOT_WIDTH,
  BUTTON_WIDTH,
  PARAMETER_WIDTH,
  COLOR_WIDTH,
  TYPE_WIDTH,
} from './TimeSeriesManagerUtils';
import { AddPlotButton } from '../TimeSeriesAddPlot/TimeSeriesAddPlot';

export const styles = {
  headerText: {
    fontSize: 12,
    marginTop: 1.25,
    opacity: 0.67,
  },
};

export const TimeSeriesManager: FC<{
  onClose: () => void;
  onMouseDown: () => void;
  isOpen: boolean;
  order: number;
  plotState: PlotPreset;
  addPlot: (title: string) => void;
  deletePlot: (id: string) => void;
  togglePlot: (id: string) => void;
  deleteParameter: (id: string) => void;
  toggleParameter: (id: string) => void;
  setSelectPlotId: (id: string) => void;
  updateParameter: (parameter: Parameter) => void;
}> = ({
  onClose,
  isOpen,
  onMouseDown,
  order,
  plotState,
  addPlot,
  deletePlot,
  togglePlot,
  deleteParameter,
  toggleParameter,
  setSelectPlotId,
  updateParameter,
}) => {
  const parametersGroupedByPlot = groupBy(
    plotState.parameters,
    (parameter) => parameter.plotId,
  );

  const plotHierarchy: Plot[] = plotState.plots.map((plot) => {
    const plotParameters = parametersGroupedByPlot[plot.plotId];
    return { ...plot, parameters: plotParameters };
  });

  return (
    <ToolContainerDraggable
      title="Timeseries manager"
      startSize={{ width: 720, height: 300 }}
      minWidth={530}
      minHeight={126}
      startPosition={{ top: 96, left: 50 }}
      isOpen={isOpen}
      onMouseDown={onMouseDown}
      order={order}
      onClose={onClose}
    >
      <Box sx={{ padding: '0 6px' }}>
        <Grid container sx={{ height: 32 }}>
          <Grid item sx={{ width: BUTTON_WIDTH }}>
            <AddPlotButton handleClick={addPlot} plotState={plotState} />
          </Grid>
          <Grid item sx={{ width: BUTTON_WIDTH }} />
          <Grid item sx={[styles.headerText, { width: PLOT_WIDTH }]}>
            Plot
          </Grid>
          <Grid item sx={[styles.headerText, { width: PARAMETER_WIDTH }]}>
            Parameter
          </Grid>
          <Grid item sx={[styles.headerText, { width: COLOR_WIDTH }]}>
            Color
          </Grid>
          <Grid item sx={[styles.headerText, { width: TYPE_WIDTH }]}>
            Type
          </Grid>
        </Grid>
        <PlotRows
          plotHierarchy={plotHierarchy}
          deletePlot={deletePlot}
          togglePlot={togglePlot}
          deleteParameter={deleteParameter}
          toggleParameter={toggleParameter}
          setSelectPlotId={setSelectPlotId}
          updateParameter={updateParameter}
        />
      </Box>
    </ToolContainerDraggable>
  );
};
