/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { uiActions } from '@opengeoweb/core';
import TimeSeriesManagerMapButtonConnect, {
  TOOLTIP_TITLE,
} from './TimeSeriesManagerMapButtonConnect';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';

describe('src/components/TimeSeriesManager/TimeSeriesManagerMapButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked', () => {
    const mockStore = configureStore();
    const mockState = {
      timeSeriesManager: {
        type: 'timeSeriesManager',
        activeMapId: 'map1',
        isOpen: false,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerMapButtonConnect {...props} />
      </TimeSeriesThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('timeSeriesManagerButton')).toBeTruthy();

    // close the dialog
    fireEvent.click(getByTestId('timeSeriesManagerButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'timeSeriesManager',
        mapId: props.mapId,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action to close dialog when dialog is already opened for corresponding map', () => {
    const mockStore = configureStore();
    const mockState = {
      dialogs: {
        timeSeriesManager: {
          type: 'timeSeriesManager',
          activeMapId: 'mapId_123',
          isOpen: true,
          source: 'app',
        },
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerMapButtonConnect {...props} />
      </TimeSeriesThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('timeSeriesManagerButton')).toBeTruthy();

    // close the dialog
    fireEvent.click(getByTestId('timeSeriesManagerButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'timeSeriesManager',
        mapId: props.mapId,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should show and hide tooltip', async () => {
    const mockStore = configureStore();
    const mockState = {
      dialogs: {
        timeSeriesManager: {
          type: 'timeSeriesManager',
          activeMapId: 'mapId_123',
          isOpen: true,
          source: 'app',
        },
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId, queryByText } = render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesManagerMapButtonConnect {...props} />
      </TimeSeriesThemeStoreProvider>,
    );
    expect(queryByText(TOOLTIP_TITLE)).toBeFalsy();

    // button should be present
    const button = getByTestId('timeSeriesManagerButton');
    expect(button).toBeTruthy();
    fireEvent.mouseOver(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(queryByText(TOOLTIP_TITLE)).toBeTruthy();

    fireEvent.mouseOut(button);
    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeFalsy();
      expect(queryByText(TOOLTIP_TITLE)).toBeFalsy();
    });
  });
});
