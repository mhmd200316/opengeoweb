/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { uiActions, uiSelectors } from '@opengeoweb/core';
import { actions } from '../../store';
import { Parameter, PlotPreset } from '../TimeSeries/types';
import { TimeSeriesManager } from './TimeSeriesManager';
import { getPlotState } from '../../store/selectors';
import { TimeSeriesSelectConnect } from '../TimeSeriesSelect/TimeSeriesSelectConnect';
import { useSetupDialog } from '../../hooks/useSetupDialog/useSetupDialog';

export const DIALOG_TYPE_MANAGER = 'timeSeriesManager';

export const TimeSeriesManagerConnect: FC = () => {
  const { setDialogOrder, dialogOrder } = useSetupDialog(DIALOG_TYPE_MANAGER);

  const [selectPlotId, setSelectPlotId] = useState('');
  const dispatch = useDispatch();

  const isOpen = useSelector((store) =>
    uiSelectors.getisDialogOpen(store, DIALOG_TYPE_MANAGER),
  );

  const plotState: PlotPreset | undefined = useSelector(getPlotState);

  const onClose = useCallback((): void => {
    dispatch(
      uiActions.setToggleOpenDialog({
        type: DIALOG_TYPE_MANAGER,
        setOpen: false,
      }),
    );
  }, [dispatch]);

  const deletePlot = (id: string): void => {
    dispatch(actions.deletePlot(id));
  };
  const addPlot = (title: string): void => {
    dispatch(actions.addPlot({ title }));
  };
  const deleteParameter = (id: string): void => {
    dispatch(actions.deleteParameter({ id }));
  };
  const togglePlot = (id: string): void => {
    dispatch(actions.togglePlot(id));
  };
  const toggleParameter = (id: string): void => {
    dispatch(actions.toggleParameter({ id }));
  };
  const updateParameter = (parameter: Parameter): void => {
    dispatch(actions.updateParameter({ parameter }));
  };
  return (
    <>
      {plotState && (
        <TimeSeriesManager
          isOpen={isOpen}
          onClose={onClose}
          onMouseDown={setDialogOrder}
          order={dialogOrder}
          plotState={plotState}
          addPlot={addPlot}
          deletePlot={deletePlot}
          togglePlot={togglePlot}
          deleteParameter={deleteParameter}
          toggleParameter={toggleParameter}
          setSelectPlotId={setSelectPlotId}
          updateParameter={updateParameter}
        />
      )}
      <TimeSeriesSelectConnect selectPlotId={selectPlotId} />
    </>
  );
};
