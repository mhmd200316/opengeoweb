/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  AppStore,
  generateLayerId,
  layerActions,
  mapActions,
  mapSelectors,
} from '@opengeoweb/core';
import { Box, Stack, Tooltip } from '@mui/material';
import { Location } from '@opengeoweb/theme';
import { ToolButton, usePrevious } from '@opengeoweb/shared';
import { Feature, FeatureCollection, Point } from 'geojson';
import { isEqual } from 'lodash';
import produce from 'immer';
import { PlotPreset, PointerLocation, TimeSeriesPresetLocation } from './types';
import { TimeSeriesView } from './TimeSeriesView';
import { actions } from '../../store';
import { LocationSelect } from './LocationSelect';
import { getPlotState } from '../../store/selectors';
import TimeSeriesManagerMapButtonConnect from '../TimeSeriesManager/TimeSeriesManagerMapButtonConnect';

export const TOOLTIP_TITLE_ENABLED = 'Lock location';
export const TOOLTIP_TITLE_DISABLED = 'Unlock location';

interface Props {
  plotPreset: PlotPreset;
  timeSeriesPresetLocations: TimeSeriesPresetLocation[];
}

export const TimeSeriesConnect: React.FC<Props> = ({
  plotPreset,
  timeSeriesPresetLocations,
}) => {
  const dispatch = useDispatch();

  const { mapId } = plotPreset;
  const mapPinLocation: PointerLocation | undefined = useSelector(
    (store: AppStore) => mapSelectors.getPinLocation(store, mapId),
  );
  const [timeSeriesLocation, setTimeSeriesLocation] = useState<
    PointerLocation | undefined
  >(mapPinLocation);

  const [selectedLocation, setSelectedLocation] = React.useState('');

  const disableMapPin: boolean = useSelector((store: AppStore) =>
    mapSelectors.getDisableMapPin(store, mapId),
  );

  const isMapPresent = useSelector((store) =>
    mapSelectors.getIsMapPresent(store, mapId),
  );

  const plotPresetState: PlotPreset | undefined = useSelector(getPlotState);
  const geojson = createGeojson(timeSeriesPresetLocations);
  const geojsonLayerId = useRef(generateLayerId());
  useEffect(() => {
    dispatch(actions.registerPlotPreset(plotPreset));

    dispatch(
      mapActions.addLayer({
        mapId,
        layer: { geojson },
        layerId: geojsonLayerId.current,
        origin: 'TimeSeriesConnect',
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const toggleDisableMapPin = (): void => {
    dispatch(
      mapActions.setDisableMapPin({
        mapId,
        disableMapPin: !disableMapPin,
      }),
    );
  };

  const setLayerGeojson = (geojson: FeatureCollection): void => {
    dispatch(
      layerActions.layerChangeGeojson({
        geojson,
        layerId: geojsonLayerId.current,
      }),
    );
  };

  const setMapPinLocation = (location: PointerLocation): void => {
    dispatch(
      mapActions.setMapPinLocation({
        mapId,
        mapPinLocation: location,
      }),
    );
  };

  const selectedFeatureIndex = useSelector((store: AppStore) =>
    mapSelectors.getSelectedFeatureIndex(store, mapId),
  );
  useHandleClickOnMap(
    selectedFeatureIndex,
    mapPinLocation,
    setMapPinLocation,
    setTimeSeriesLocation,
    setSelectedLocation,
    geojson,
    disableMapPin,
    setLayerGeojson,
  );

  const setSelectedFeatureIndex = (selectedFeatureIndex: number): void => {
    dispatch(
      mapActions.setSelectedFeature({
        mapId,
        selectedFeatureIndex,
      }),
    );
  };

  return (
    <Stack
      sx={{
        padding: '20px',
        width: '100%',
        backgroundColor: 'geowebColors.background.surface',
      }}
    >
      <Stack direction="row" spacing={1}>
        <Stack spacing={1}>
          <TimeSeriesManagerMapButtonConnect mapId={mapId} />
          <Tooltip
            title={
              disableMapPin ? TOOLTIP_TITLE_DISABLED : TOOLTIP_TITLE_ENABLED
            }
            placement="right"
          >
            <ToolButton
              onClick={(): void => toggleDisableMapPin()}
              data-testid="toggleLockLocationButton"
              active={!disableMapPin}
            >
              <Location />
            </ToolButton>
          </Tooltip>
        </Stack>
        <LocationSelect
          selectedLocation={selectedLocation}
          isMapPresent={isMapPresent}
          mapPinLocation={timeSeriesLocation}
          disableMapPin={disableMapPin}
          geojson={geojson}
          setSelectedFeatureIndex={setSelectedFeatureIndex}
          setTimeSeriesLocation={setTimeSeriesLocation}
          setSelectedLocation={setSelectedLocation}
          setMapPinLocation={setMapPinLocation}
        />
      </Stack>
      {timeSeriesLocation && plotPresetState && (
        <Box>
          <TimeSeriesView
            plotPreset={plotPresetState}
            selectedLocation={timeSeriesLocation}
          />
        </Box>
      )}
    </Stack>
  );
};

const createGeojson = (
  timeSeriesPresetLocations: TimeSeriesPresetLocation[],
): FeatureCollection<Point> => {
  const features = timeSeriesPresetLocations.map(
    ({ name, lat, lon }): Feature<Point> => {
      return {
        type: 'Feature',
        properties: { name, drawFunction: circleDrawFunction },
        geometry: {
          type: 'Point',
          coordinates: [lon, lat],
        },
      };
    },
  );

  return {
    type: 'FeatureCollection',
    features,
  };
};

const addDrawFunctionToGeojson = (
  geojson: FeatureCollection,
  selectedFeatureIndex?: number,
): FeatureCollection => {
  const geojsonWithDraw = produce(geojson, (draft) => {
    /* Set default drawfunction for each feature */
    draft.features.forEach((feature) => {
      // eslint-disable-next-line no-param-reassign
      feature.properties.drawFunction = circleDrawFunction;
    });

    /* Change drawfunction for selected feature */
    if (selectedFeatureIndex !== undefined) {
      draft.features[selectedFeatureIndex].properties.drawFunction = (
        args,
      ): void => {
        circleDrawFunction(args, '#F00', 12);
      };
    }
  });
  return geojsonWithDraw;
};

export const circleDrawFunction = (
  args,
  fillColor: string,
  radius: number,
): void => {
  const { context: ctx, coord } = args;
  ctx.strokeStyle = '#000';
  ctx.fillStyle = fillColor || '#88F';
  ctx.lineWidth = '1px';
  ctx.beginPath();
  ctx.arc(coord.x, coord.y, radius || 10, 0, 2 * Math.PI);
  ctx.fill();
  ctx.stroke();
  ctx.fillStyle = '#000';
  ctx.beginPath();
  ctx.arc(coord.x, coord.y, 2, 0, 2 * Math.PI);
  ctx.fill();
};

export const useHandleClickOnMap = (
  selectedFeatureIndex: number | undefined,
  mapPinLocation: PointerLocation,
  setMapPinLocation: (location: PointerLocation) => void,
  setTimeSeriesLocation: (location: PointerLocation) => void,
  setSelectedLocation: (location: string) => void,
  geojson: FeatureCollection<Point>,
  disableMapPin: boolean,
  setLayerGeojson: (geojson: FeatureCollection) => void,
): void => {
  const previousMapPinLocation = usePrevious(mapPinLocation);

  // Handle click on map
  useEffect(() => {
    if (
      mapPinLocation === undefined ||
      disableMapPin ||
      // Avoid infinite loop
      isEqual(previousMapPinLocation, mapPinLocation)
    ) {
      return;
    }

    // Handle click on point
    if (selectedFeatureIndex !== undefined) {
      const selectedFeature = geojson.features[selectedFeatureIndex];
      const [lon, lat] = selectedFeature.geometry.coordinates;
      const location = { lon, lat };

      setMapPinLocation(location);
      setTimeSeriesLocation(location);
      setSelectedLocation(selectedFeature.properties.name);
      setLayerGeojson(addDrawFunctionToGeojson(geojson, selectedFeatureIndex));
    }

    // Handle click outside of point
    else if (selectedFeatureIndex === undefined) {
      setTimeSeriesLocation(mapPinLocation);
      setLayerGeojson(geojson);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedFeatureIndex, mapPinLocation]);
};
