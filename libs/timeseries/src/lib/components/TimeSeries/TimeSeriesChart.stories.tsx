/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { ThemeWrapper } from '@opengeoweb/theme';
import { TimeSeriesChart } from './TimeSeriesChart';
import { plotsWithData } from './storyUtils/chartSeriesExampleData';
import { ToggleThemeButton } from '../../storybookUtils/ToggleThemeButton';

export default { title: 'components/TimeSeriesChart' };

const ThemeDemo: React.FC = () => {
  return (
    <div style={{ width: '100vw', height: '100vh' }}>
      <ToggleThemeButton />
      <TimeSeriesChart plotsWithData={plotsWithData} />
    </div>
  );
};

export const ThemeChartDemo: React.FC = () => (
  <ThemeWrapper>
    <ThemeDemo />
  </ThemeWrapper>
);

export const StaticChartDemo = (): React.ReactElement => (
  <div style={{ width: '800px', height: '500px' }}>
    <TimeSeriesChart plotsWithData={plotsWithData} />
  </div>
);

StaticChartDemo.storyName = 'Static Chart Demo (takeSnapshot)';
