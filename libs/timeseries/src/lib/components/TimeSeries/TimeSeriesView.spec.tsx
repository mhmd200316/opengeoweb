/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import configureStore from 'redux-mock-store';
import produce from 'immer';
import moment from 'moment';
import { TimeSeriesThemeProvider } from '../../storybookUtils/Providers';
import * as api from './api';
import {
  getParametersWithData,
  getUtcTime,
  TimeSeriesView,
  useGetPlotsWithData,
} from './TimeSeriesView';
import {
  ParameterWithData,
  Service,
  ParameterApiData,
  Plot,
  PlotPreset,
  Parameter,
} from './types';

describe('components/TimeSeries/TimeSeriesView', () => {
  const PLOT_ID_1 = 'PLOT_ID_1';
  const LINE = 'line';
  const PROPERTY_NAME_1 = 'PROPERTY_NAME_1';
  const PROPERTY_NAME_2 = 'PROPERTY_NAME_2';
  const UNIT = 'unit';
  const SERVICE_URL = 'SERVICE_URL';

  const service1: Service = {
    id: 'fmi',
    serviceUrl: SERVICE_URL,
  };

  const parameter1: Parameter = {
    plotId: PLOT_ID_1,
    plotType: LINE,
    propertyName: PROPERTY_NAME_1,
    unit: UNIT,
    serviceId: service1.id,
  };

  const parameter2: Parameter = {
    ...parameter1,
    propertyName: PROPERTY_NAME_2,
  };

  const TIME_1 = '20220113T120000';
  const TIME_2 = '20220113T130000';
  const RESULT = ['1', '2'];
  const parameterApiData1: ParameterApiData = {
    numberReturned: 1,
    timeStamp: 'timeStamp',
    type: 'type',
    features: [
      {
        type: 'type',
        geometry: {
          coordinates: [1.0, 2.0],
          type: 'type',
        },
        properties: {
          observedPropertyName: PROPERTY_NAME_1,
          result: RESULT,
          timestep: [TIME_1, TIME_2],
        },
      },
    ],
  };

  const expectedParameterWithData1: ParameterWithData = {
    ...parameter1,
    timestep: [
      moment(TIME_1, 'YYYYMMDDHH').toDate(),
      moment(TIME_2, 'YYYYMMDDHH').toDate(),
    ],
    value: RESULT.map((el) => Number(el)),
  };

  const mapPinLocation = { lat: 1.0, lon: 2.0 };

  const plot: Plot = {
    plotId: PLOT_ID_1,
    title: 'Title',
  };

  const plotPreset1: PlotPreset = {
    mapId: 'mapId',
    plots: [plot],
    services: [service1],
    parameters: [parameter1, parameter2],
  };

  describe('TimeSeriesView', () => {
    it('should render', async () => {
      const spy = jest
        .spyOn(api, 'fetchParameterApiData')
        .mockResolvedValue(parameterApiData1);

      const mockStore = configureStore();
      const store = mockStore();
      store.addModules = jest.fn();

      const { findByTestId, rerender } = render(
        <TimeSeriesView
          selectedLocation={mapPinLocation}
          plotPreset={plotPreset1}
        />,
        {
          wrapper: ({ children }) => (
            <TimeSeriesThemeProvider>{children}</TimeSeriesThemeProvider>
          ),
        },
      );
      expect(await findByTestId('TimeSeriesChart')).toBeTruthy();

      expect(spy).toHaveBeenCalledTimes(2);

      // Should not rerender if lat lon did not change
      rerender(
        <TimeSeriesView
          selectedLocation={{ ...mapPinLocation }}
          plotPreset={plotPreset1}
        />,
      );

      expect(spy).toHaveBeenCalledTimes(2);
    });
  });

  describe('useUpdateTimeSeriesData', () => {
    it('should update data correctly', async () => {
      const spy = jest
        .spyOn(api, 'fetchParameterApiData')
        .mockResolvedValue(parameterApiData1);

      let location = mapPinLocation;
      let plotPreset = plotPreset1;
      const { rerender, waitForNextUpdate } = renderHook(() =>
        useGetPlotsWithData(plotPreset, location),
      );

      expect(spy).toHaveBeenCalledTimes(2);

      plotPreset = { ...plotPreset1, parameters: [parameter1] };
      rerender();
      await waitForNextUpdate();

      expect(spy).toHaveBeenCalledTimes(3);

      location = { ...mapPinLocation };
      rerender();
      await waitForNextUpdate();

      expect(spy).toHaveBeenCalledTimes(4);
    });
    it('should remove hidden plots and parameters', async () => {
      const spy = jest
        .spyOn(api, 'fetchParameterApiData')
        .mockResolvedValue(parameterApiData1);

      const location = mapPinLocation;
      let plotPreset = plotPreset1;
      const { rerender, waitForNextUpdate, result } = renderHook(() =>
        useGetPlotsWithData(plotPreset, location),
      );

      expect(spy).toHaveBeenCalledTimes(2);
      await waitForNextUpdate();
      expect(result.current).toHaveLength(1);

      const plotPresetWithHiddenParameter = produce(plotPreset1, (draft) => {
        draft.parameters[0].enabled = false;
      });
      plotPreset = plotPresetWithHiddenParameter;
      rerender();
      expect(spy).toHaveBeenCalledTimes(3);
      await waitForNextUpdate();
      expect(result.current).toHaveLength(1);

      plotPreset = produce(plotPresetWithHiddenParameter, (draft) => {
        draft.plots[0].enabled = false;
      });
      rerender();
      expect(spy).toHaveBeenCalledTimes(3);
      expect(result.current).toEqual(null);
    });
  });

  describe('getParametersWithData', () => {
    it('should get parameter with data from one service', async () => {
      const spy = jest
        .spyOn(api, 'fetchParameterApiData')
        .mockResolvedValue(parameterApiData1);

      const parametersWithData = await getParametersWithData(
        { ...plotPreset1, parameters: [parameter1] },
        mapPinLocation,
      );

      expect(spy).toHaveBeenCalledTimes(1);

      expect(parametersWithData).toEqual([expectedParameterWithData1]);
    });

    it('should get parameters with data from two services', async () => {
      const parameterApiData2 = produce(parameterApiData1, (draft) => {
        draft.features[0].properties.observedPropertyName = PROPERTY_NAME_2;
      });

      const spy = jest
        .spyOn(api, 'fetchParameterApiData')
        .mockResolvedValue(parameterApiData1)
        .mockResolvedValue(parameterApiData2);

      const parametersWithData = await getParametersWithData(
        plotPreset1,
        mapPinLocation,
      );

      expect(spy).toHaveBeenCalledTimes(2);

      const expectedParameterWithData2 = produce(
        expectedParameterWithData1,
        (draft) => {
          draft.plotId = PLOT_ID_1;
          draft.propertyName = PROPERTY_NAME_2;
        },
      );

      expect(parametersWithData).toEqual([
        expectedParameterWithData1,
        expectedParameterWithData2,
      ]);
    });
    it('should get parameters with data from two services but one api call fails', async () => {
      const spy = jest
        .spyOn(api, 'fetchParameterApiData')
        .mockResolvedValueOnce(parameterApiData1)
        .mockResolvedValueOnce(null);

      const parametersWithData = await getParametersWithData(
        plotPreset1,
        mapPinLocation,
      );

      expect(spy).toHaveBeenCalledTimes(2);

      expect(parametersWithData).toEqual([expectedParameterWithData1]);
    });
  });

  describe('getUtcTime', () => {
    it('should convert time string to utc time', () => {
      const apiTime = '20220113T120000';
      const utcTime = getUtcTime(apiTime);

      expect(utcTime.toISOString()).toEqual('2022-01-13T12:00:00.000Z');
    });
  });
});
