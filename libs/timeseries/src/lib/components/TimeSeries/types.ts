/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
export interface Geometry {
  type: string;
  coordinates: number[];
}

export interface Properties {
  timestep: Date[];
  propertyName: string;
  values: number[];
}

// properties provided by https://for.weather.fmibeta.com/sofp
export interface ObservationProperties {
  timestep: string[];
  observedPropertyName: string;
  result: string[];
}

export interface Feature {
  type: string;
  geometry: Geometry;
  properties: ObservationProperties;
}

export interface ParameterApiData {
  type: string;
  features: Feature[];
  timeStamp: string;
  numberReturned: number;
}

export interface PlotPreset {
  mapId: string;
  plots: Plot[];
  services: Service[];
  parameters: Parameter[];
}

export interface Plot {
  title: string;
  plotId: string;
  enabled?: boolean;
  parameters?: Parameter[];
}

export interface PlotWithData extends Plot {
  parametersWithData: ParameterWithData[];
}

export interface Service {
  id: string;
  serviceUrl: string;
}

export type PlotType = 'bar' | 'line';

export interface Parameter {
  id?: string;
  plotId: string;
  unit: string;
  propertyName: string;
  plotType: PlotType;
  enabled?: boolean;
  color?: string;
  serviceId: string;
}

export interface ParameterWithData extends Parameter {
  timestep: Date[];
  value: number[];
}

export interface PointerLocation {
  lon: number;
  lat: number;
}

export interface TimeSeriesPresetLocation {
  lat: number;
  lon: number;
  name: string;
}
