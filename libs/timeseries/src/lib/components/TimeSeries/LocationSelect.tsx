/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { useEffect } from 'react';
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { FeatureCollection, Point } from 'geojson';
import { PointerLocation } from './types';

export const LocationSelect: React.FC<{
  selectedLocation: string;
  mapPinLocation: PointerLocation;
  disableMapPin: boolean;
  geojson: FeatureCollection<Point>;
  isMapPresent: boolean;
  setSelectedLocation: (location: string) => void;
  setMapPinLocation: (mapLocation: PointerLocation) => void;
  setSelectedFeatureIndex: (selectedFeatureIndex: number) => void;
  setTimeSeriesLocation: (mapLocation: PointerLocation) => void;
}> = ({
  selectedLocation,
  mapPinLocation,
  disableMapPin,
  geojson,
  isMapPresent,
  setSelectedLocation,
  setMapPinLocation,
  setTimeSeriesLocation,
  setSelectedFeatureIndex,
}) => {
  // Reset select if user clicks on map
  useEffect(() => {
    if (selectedLocation === '' || disableMapPin) {
      return;
    }

    const [lon, lat] = geojson.features.find(
      (feature) => feature.properties.name === selectedLocation,
    ).geometry.coordinates;

    // Avoid resetting select if mapPinLocation is still on same lat lon as selected location
    if (
      mapPinLocation &&
      lat === mapPinLocation.lat &&
      lon === mapPinLocation.lon
    ) {
      return;
    }
    setSelectedLocation('');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [mapPinLocation]);

  const handleChange = (event: SelectChangeEvent<string>): void => {
    const newSelectedLocation = event.target.value;
    setSelectedLocation(newSelectedLocation);

    const selectedFeatureIndex = geojson.features.findIndex(
      (feature) => feature.properties.name === newSelectedLocation,
    );
    setSelectedFeatureIndex(selectedFeatureIndex);

    const [lon, lat] =
      geojson.features[selectedFeatureIndex].geometry.coordinates;
    const location = { lat, lon };

    isMapPresent
      ? setMapPinLocation(location)
      : setTimeSeriesLocation(location);
  };

  return (
    <FormControl disabled={disableMapPin}>
      <InputLabel id="time-series-location-select-label">
        Select Location
      </InputLabel>
      <Select
        labelId="time-series-location-select-label"
        value={selectedLocation}
        label="Select Location"
        style={{ width: '200px' }}
        onChange={handleChange}
      >
        {geojson.features.map((feature) => {
          const { name } = feature.properties;
          return (
            <MenuItem key={name} value={name}>
              {name}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
};
