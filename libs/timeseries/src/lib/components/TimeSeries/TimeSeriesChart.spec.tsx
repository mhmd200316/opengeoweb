/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { render } from '@testing-library/react';
import React from 'react';
import 'jest-canvas-mock';
import { TimeSeriesChart } from './TimeSeriesChart';

describe('components/Timeseries/TimeSeriesChart', () => {
  it('should render a canvas element', () => {
    const { container } = render(
      <TimeSeriesChart
        plotsWithData={[
          {
            plotId: 'plot_1',
            title: 'test-series',
            parametersWithData: [
              {
                unit: 'hPa',
                plotId: 'plot_1',
                propertyName: 'pressure',
                plotType: 'line',
                timestep: [
                  new Date(Date.parse('2021-11-03T09:00:00Z')),
                  new Date(Date.parse('2021-11-03T10:00:00Z')),
                  new Date(Date.parse('2021-11-03T11:00:00Z')),
                ],
                value: [1, 2, 3],
                serviceId: 'fmi',
              },
            ],
          },
        ]}
      />,
    );
    const canvas = container.querySelector('canvas');
    expect(canvas).toBeInstanceOf(HTMLElement);
  });
});
