/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { XAXisComponentOption, YAXisComponentOption } from 'echarts';
import produce from 'immer';
import {
  dateFormatter,
  Y_AXIS_OFFSET,
  getGrid,
  getOption,
  getSeries,
  getTooltipTimeLabel,
  getXAxis,
  getYAxis,
  PlotChart,
} from './chartUtils';
import { ParameterWithData, Plot } from './types';

describe('components/TimeSeries/chartUtils', () => {
  const date1 = new Date('2022-05-16T07:00:00.000Z');
  const date2 = new Date('2022-05-16T08:00:00.000Z');
  const color1 = 'red';
  const mockPlot: Plot = {
    plotId: 'id0',
    title: 'title0',
  };

  const mockParameterWithData: ParameterWithData = {
    plotId: 'id0',
    plotType: 'line',
    propertyName: 'propertyName',
    timestep: [date1, date2],
    unit: 'unit',
    value: [1, 2],
    color: color1,
    serviceId: 'fmi',
  };

  const plotChart: PlotChart = {
    ...mockPlot,
    parametersByUnit: {
      [mockParameterWithData.unit]: [mockParameterWithData],
    },
    countPreviousUnits: 0,
  };

  describe('getYAxis', () => {
    it('should get y axis', () => {
      const yAxis = getYAxis([plotChart]);

      const expectedYAxis: YAXisComponentOption[] = [
        {
          gridIndex: 0,
          name: 'title0',
          type: 'value',
          axisLabel: {
            formatter: `{value} unit`,
          },
          axisLine: {
            show: true,
          },
          offset: 0,
          position: 'left',
          min: 1,
          max: 2,
        },
      ];
      expect(yAxis).toEqual(expectedYAxis);
    });
  });
  it('should get multiple y axis', () => {
    const plotChart2 = produce(plotChart, (draft) => {
      draft.parametersByUnit['unit2'] = [
        mockParameterWithData,
        mockParameterWithData,
      ];
      draft.parametersByUnit['unit3'] = [mockParameterWithData];
    });

    const yAxis = getYAxis([plotChart2]);

    const expectedYAxis: YAXisComponentOption[] = [
      {
        gridIndex: 0,
        name: 'title0',
        type: 'value',
        axisLabel: {
          formatter: `{value} unit`,
        },
        axisLine: {
          show: true,
        },
        offset: 0,
        position: 'left',
        min: 1,
        max: 2,
      },
      {
        axisLabel: {
          formatter: `{value} unit2`,
        },
        axisLine: {
          show: true,
        },
        gridIndex: 0,
        max: 2,
        min: 1,
        offset: 0,
        position: 'right',
        type: 'value',
      },
      {
        axisLabel: {
          formatter: `{value} unit3`,
        },
        axisLine: {
          show: true,
        },
        gridIndex: 0,
        max: 2,
        min: 1,
        offset: Y_AXIS_OFFSET,
        position: 'left',
        type: 'value',
      },
    ];
    expect(yAxis).toEqual(expectedYAxis);
  });

  describe('getXAxis', () => {
    it('should get x axis', () => {
      const expectedXAxis: XAXisComponentOption[] = [
        {
          gridIndex: 0,
          type: 'category',
          axisLabel: {
            formatter: dateFormatter,
          },
        },
      ];
      expect(getXAxis([plotChart])).toEqual(expectedXAxis);
    });
  });

  describe('getSeries()', () => {
    it('should convert parameter to series', () => {
      const series = getSeries([plotChart]);

      expect(series).toEqual([
        {
          data: [
            [date1, 1],
            [date2, 2],
          ],
          itemStyle: { color: color1 },
          lineStyle: { color: color1 },
          name: 'propertyName',
          type: 'line',
          xAxisIndex: 0,
          yAxisIndex: 0,
        },
      ]);
    });
  });

  describe('getGrid', () => {
    it('should get grid for plot with one parameter', () => {
      const grid = getGrid([plotChart]);

      expect(grid).toEqual([
        {
          height: '100px',
          left: '80px',
          right: '80px',
          top: '50px',
        },
      ]);
    });

    it('should get grid for plot with two parameter with different units', () => {
      const plotChart2 = produce(plotChart, (draft) => {
        draft.parametersByUnit['unit2'] = [
          {
            ...mockParameterWithData,
            unit: 'unit2',
          },
        ];
      });
      const grid = getGrid([plotChart2]);

      expect(grid).toEqual([
        {
          height: '100px',
          left: '80px',
          right: '80px',
          top: '50px',
        },
      ]);
    });

    it('should get grid for two plots with parameters with same unit', () => {
      const plotChart2: PlotChart = {
        ...plotChart,
        plotId: 'plotId2',
      };
      const grid = getGrid([plotChart, plotChart2]);

      expect(grid).toEqual([
        {
          height: '100px',
          left: '80px',
          right: '80px',
          top: '50px',
        },
        {
          height: '100px',
          left: '80px',
          right: '80px',
          top: '200px',
        },
      ]);
    });
  });

  describe('getOption()', () => {
    it('should get option', () => {
      const option = getOption([
        {
          ...mockPlot,
          parametersWithData: [
            mockParameterWithData,
            { ...mockParameterWithData, propertyName: 'propertyName1' },
          ],
        },
      ]);
      expect(option.xAxis).toHaveLength(1);
      expect(option.yAxis).toHaveLength(1);
      expect(option.series).toHaveLength(2);
      expect(option.grid).toHaveLength(1);
      expect(option.tooltip).toEqual({
        trigger: 'axis',
        formatter: expect.any(Function),
      });
      expect(option.dataZoom).toEqual([
        {
          show: true,
          realtime: true,
          xAxisIndex: [0, 1],
          labelFormatter: expect.any(Function),
        },
      ]);
    });
  });

  describe('dateFormatter', () => {
    it('should format date', () => {
      const date = new Date('2022-01-13T12:00:00.000Z');
      const formattedDate = dateFormatter(date.toISOString());

      expect(formattedDate).toEqual('Thu 12:00');
    });
  });

  describe('getTooltipTimeLabel', () => {
    it('should format tooltip date', () => {
      const date = new Date('2022-01-13T12:00:00.000Z');
      const formattedDate = getTooltipTimeLabel(date);

      expect(formattedDate).toEqual('Thu 13 Jan 12:00');
    });
  });
});
