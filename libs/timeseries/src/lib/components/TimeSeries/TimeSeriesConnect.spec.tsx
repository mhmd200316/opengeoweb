/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import { layerActions, mapActions, mapUtils } from '@opengeoweb/core';
import configureStore from 'redux-mock-store';
import { renderHook } from '@testing-library/react-hooks';
import { FeatureCollection, Point } from 'geojson';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import {
  circleDrawFunction,
  TimeSeriesConnect,
  TOOLTIP_TITLE_DISABLED,
  TOOLTIP_TITLE_ENABLED,
  useHandleClickOnMap,
} from './TimeSeriesConnect';
import screenPresets from '../../storybookUtils/screenPresets.json';
import { PlotPreset } from './types';
import { actions } from '../../store';

describe('src/components/TimeSeries/TimeSeriesConnect', () => {
  it('should register plot and set map pin location', async () => {
    const mockStore = configureStore();
    const mapId = 'TimeseriesMap';
    const mockMap = mapUtils.createMap({ id: 'TimeseriesMap' });
    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: mockMap,
        },
        allIds: [mapId],
      },
    });
    store.addModules = jest.fn();

    const plotPreset = screenPresets[0].views.byId.Timeseries.initialProps
      .plotPreset as PlotPreset;

    const lat = 60.192059;
    const lon = 24.945831;

    const { getByLabelText, getByText } = render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect
          plotPreset={plotPreset}
          timeSeriesPresetLocations={[
            {
              lat,
              lon,
              name: 'Helsinki',
            },
          ]}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    const registerPlotAction = actions.registerPlotPreset(plotPreset);

    const geojson: FeatureCollection = {
      features: [
        {
          geometry: {
            coordinates: [lon, lat],
            type: 'Point',
          },
          properties: {
            drawFunction: circleDrawFunction,
            name: 'Helsinki',
          },
          type: 'Feature',
        },
      ],
      type: 'FeatureCollection',
    };

    const registerLayerAction = layerActions.addLayer({
      layer: { geojson },
      layerId: 'layerid_41',
      mapId: 'TimeseriesMap',
      origin: 'TimeSeriesConnect',
    });

    const expectedInitialActions = [registerPlotAction, registerLayerAction];
    expect(store.getActions()).toEqual(expectedInitialActions);

    const selectLocation = getByLabelText('Select Location');
    fireEvent.mouseDown(selectLocation);

    fireEvent.click(getByText('Helsinki'));

    const setSelectedFeatureIndexAction = mapActions.setSelectedFeature({
      mapId: 'TimeseriesMap',
      selectedFeatureIndex: 0,
    });
    const setMapPinAction = mapActions.setMapPinLocation({
      mapId: 'TimeseriesMap',
      mapPinLocation: { lon, lat },
    });
    expect(store.getActions()).toEqual([
      ...expectedInitialActions,
      setSelectedFeatureIndexAction,
      setMapPinAction,
    ]);
  });

  it('should show correct tooltip for enabled mappin', async () => {
    const mockStore = configureStore();
    const mapId = 'TimeseriesMap';
    const mockMap = mapUtils.createMap({ id: 'TimeseriesMap' });
    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: { ...mockMap, disableMapPin: false },
        },
        allIds: [mapId],
      },
    });
    store.addModules = jest.fn();

    const lat = 60.192059;
    const lon = 24.945831;
    const plotPreset = screenPresets[0].views.byId.Timeseries.initialProps
      .plotPreset as PlotPreset;

    const props = {
      lat,
      lon,
      plotPreset,
      timeSeriesPresetLocations: [
        {
          lat,
          lon,
          name: 'Helsinki',
        },
      ],
    };

    const { getByTestId, queryByText } = render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect {...props} />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(queryByText(TOOLTIP_TITLE_ENABLED)).toBeFalsy();

    const button = getByTestId('toggleLockLocationButton');
    expect(button).toBeTruthy();
    fireEvent.mouseOver(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(queryByText(TOOLTIP_TITLE_ENABLED)).toBeTruthy();

    fireEvent.mouseOut(button);
    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeFalsy();
      expect(queryByText(TOOLTIP_TITLE_ENABLED)).toBeFalsy();
    });
  });

  it('should show correct tooltip for disabled mappin', async () => {
    const mockStore = configureStore();
    const mapId = 'TimeseriesMap';
    const mockMap = mapUtils.createMap({ id: 'TimeseriesMap' });
    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            disableMapPin: true,
          },
        },
        allIds: [mapId],
      },
    });
    store.addModules = jest.fn();

    const lat = 60.192059;
    const lon = 24.945831;
    const plotPreset = screenPresets[0].views.byId.Timeseries.initialProps
      .plotPreset as PlotPreset;

    const props = {
      lat,
      lon,
      plotPreset,
      timeSeriesPresetLocations: [
        {
          lat,
          lon,
          name: 'Helsinki',
        },
      ],
    };
    const { getByTestId, queryByText } = render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect {...props} />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(queryByText(TOOLTIP_TITLE_DISABLED)).toBeFalsy();

    const button = getByTestId('toggleLockLocationButton');
    expect(button).toBeTruthy();
    fireEvent.mouseOver(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(queryByText(TOOLTIP_TITLE_DISABLED)).toBeTruthy();

    fireEvent.mouseOut(button);
    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeFalsy();
      expect(queryByText(TOOLTIP_TITLE_DISABLED)).toBeFalsy();
    });
  });

  describe('useHandleClickOnMap', () => {
    const geojson: FeatureCollection<Point> = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: { name: 'name' },
          geometry: {
            type: 'Point',
            coordinates: [1, 2],
          },
        },
      ],
    };
    it('should handle click on map', () => {
      let selectedFeatureIndex: number;
      let mapPinLocation = { lat: 1, lon: 1 };

      const setMapPinLocation = jest.fn();
      const setTimeSeriesLocation = jest.fn();
      const setSelectedLocation = jest.fn();
      const disableMapPin = false;
      const setLayerGeojson = jest.fn();
      const { rerender } = renderHook(() =>
        useHandleClickOnMap(
          selectedFeatureIndex,
          mapPinLocation,
          setMapPinLocation,
          setTimeSeriesLocation,
          setSelectedLocation,
          geojson,
          disableMapPin,
          setLayerGeojson,
        ),
      );

      expect(setTimeSeriesLocation).toBeCalledTimes(1);
      expect(setLayerGeojson).toBeCalledTimes(1);
      expect(setMapPinLocation).not.toBeCalled();
      expect(setSelectedLocation).not.toBeCalled();

      // user clicks on point
      selectedFeatureIndex = 0;
      mapPinLocation = { lat: 2, lon: 2 };
      rerender();

      expect(setTimeSeriesLocation).toBeCalledTimes(2);
      expect(setLayerGeojson).toBeCalledTimes(2);
      expect(setMapPinLocation).toBeCalledTimes(1);
      expect(setSelectedLocation).toBeCalledTimes(1);

      // user clicks on same point but a little to the side
      mapPinLocation = { lat: 3, lon: 3 };
      rerender();

      expect(setTimeSeriesLocation).toBeCalledTimes(3);
      expect(setLayerGeojson).toBeCalledTimes(3);
      expect(setMapPinLocation).toBeCalledTimes(2);
      expect(setSelectedLocation).toBeCalledTimes(2);

      // user clicks outside point
      selectedFeatureIndex = undefined;
      mapPinLocation = { lat: 4, lon: 4 };
      rerender();

      expect(setTimeSeriesLocation).toBeCalledTimes(4);
      expect(setLayerGeojson).toBeCalledTimes(4);
      expect(setMapPinLocation).toBeCalledTimes(2);
      expect(setSelectedLocation).toBeCalledTimes(2);
    });

    it('should not change location if map pin is disabled', () => {
      const selectedFeatureIndex = undefined;
      const setMapPinLocation = jest.fn();
      const setSelectedLocation = jest.fn();
      const setLastMapPinLocation = jest.fn();

      let mapPinLocation = { lat: 1, lon: 1 };
      let disableMapPin = true;

      const setLayerGeojson = jest.fn();
      const { rerender } = renderHook(() =>
        useHandleClickOnMap(
          selectedFeatureIndex,
          mapPinLocation,
          setMapPinLocation,
          setLastMapPinLocation,
          setSelectedLocation,
          geojson,
          disableMapPin,
          setLayerGeojson,
        ),
      );

      expect(setLastMapPinLocation).not.toBeCalled();

      // Enabling map pin should not cause update
      disableMapPin = false;
      rerender();
      expect(setLastMapPinLocation).not.toBeCalled();

      // Click on map after enabling map pin causes update
      mapPinLocation = { lat: 2, lon: 2 };
      rerender();
      expect(setLastMapPinLocation).toBeCalledTimes(1);
    });
  });
});
