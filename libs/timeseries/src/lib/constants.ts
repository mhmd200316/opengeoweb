/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

export const COLOR_MAP = {
  Pressure: 'thistle',
  GeopHeight: 'red',
  Temperature: 'orange',
  DewPoint: 'yellow',
  Humidity: 'olive',
  WindDirection: 'rosybrown',
  WindSpeedMS: 'fuchsia',
  PrecipitationAmount: 'salmon',
  TotalCloudCover: 'green',
  MiddleAndLowCloudCover: 'navy',
  LandSeaMask: 'blue',
  WeatherSymbol3: 'tan',
  Precipitation1h: 'teal',
  WindGust: 'silver',
  PrecipitationForm2: 'gray',
  WindGust2: 'chocolate',
};

export const COLOR_NAME_TO_HEX_MAP = {
  thistle: '#d8bfd8',
  red: '#ff0000',
  orange: '#ffa500',
  yellow: '#ffff00',
  olive: '#808000',
  rosybrown: '#bc8f8f',
  fuchsia: '#ff00ff',
  salmon: '#fa8072',
  green: '#008000',
  navy: '#000080',
  blue: '#0000ff',
  tan: '#d2b48c',
  teal: '#008080',
  silver: '#c0c0c0',
  gray: '#808080',
  chocolate: '#d2691e',
};
