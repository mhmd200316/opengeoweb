/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { addColors } from './addColors';
import { mockGeoJson } from './mockGeoJson';

describe('lib/cap/src/lib/utils/addColors', () => {
  describe('addColors', () => {
    it('shoud have default values of empty or zero', () => {
      expect(mockGeoJson.features[0].properties.fill === '');
      expect(mockGeoJson.features[0].properties.stroke === '');
      expect(mockGeoJson.features[0].properties['stroke-width'] === 0);
      expect(mockGeoJson.features[0].properties['stroke-opacity'] === 0);
    });

    it('should return red color for Extreme severity', () => {
      mockGeoJson.features[0].properties.details.severity = 'Extreme';
      const editedGeoJson = addColors(mockGeoJson);
      expect(editedGeoJson.features[0].properties.stroke === 'rgb(255, 9, 9)');
      expect(
        editedGeoJson.features[0].properties.fill === 'rgba(255, 9, 9, 0.10)',
      );
      expect(editedGeoJson.features[0].properties['stroke-opacity'] === 1);
      expect(editedGeoJson.features[0].properties['stroke-width'] === 2);
    });

    it('should return orange color for Severe severity', () => {
      mockGeoJson.features[0].properties.details.severity = 'Severe';
      const editedGeoJson = addColors(mockGeoJson);
      expect(
        editedGeoJson.features[0].properties.stroke === 'rgb(255, 165, 0)',
      );
      expect(
        editedGeoJson.features[0].properties.fill === 'rgba(255, 165, 0, 0.10)',
      );
      expect(editedGeoJson.features[0].properties['stroke-opacity'] === 1);
      expect(editedGeoJson.features[0].properties['stroke-width'] === 2);
    });

    it('should return yellow color for Moderate severity', () => {
      mockGeoJson.features[0].properties.details.severity = 'Moderate';
      const editedGeoJson = addColors(mockGeoJson);
      expect(
        editedGeoJson.features[0].properties.stroke === 'rgb(248, 248, 0)',
      );
      expect(
        editedGeoJson.features[0].properties.fill === 'rgba(248, 248, 0, 0.10)',
      );
      expect(editedGeoJson.features[0].properties['stroke-opacity'] === 1);
      expect(editedGeoJson.features[0].properties['stroke-width'] === 2);
    });

    it('should return green color for Minor severity', () => {
      mockGeoJson.features[0].properties.details.severity = 'Minor';
      const editedGeoJson = addColors(mockGeoJson);
      expect(
        editedGeoJson.features[0].properties.stroke === 'rgb(106, 248, 108)',
      );
      expect(
        editedGeoJson.features[0].properties.fill ===
          'rgba(106, 248, 108, 0.10)',
      );
      expect(editedGeoJson.features[0].properties['stroke-opacity'] === 1);
      expect(editedGeoJson.features[0].properties['stroke-width'] === 2);
    });
  });
});
