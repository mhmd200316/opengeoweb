/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import axios from 'axios';
import cloneDeep from 'lodash.clonedeep';

// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import {
  generateMapId,
  MapViewLayer,
  MapView,
  generateLayerId,
  MapWarningProperties,
  FeatureEvent,
} from '@opengeoweb/core';

import { baseLayer } from './publicLayers';
import { CapWarnings } from '../types';
import { addColors } from '../../utils/addColors';

interface MapViewCapProps {
  bbox?: { left: number; bottom: number; right: number; top: number };
  srs?: string;
  geoJson?: CapWarnings;
}

export const MapViewCap: React.FC<MapViewCapProps> = ({
  srs,
  bbox,
  geoJson,
}) => {
  const [selectedFeatureEvent, setSelectedFeatureEvent] =
    React.useState<FeatureEvent>(null);
  const [currentJson, setCurrentJson] = React.useState<CapWarnings | null>(
    null,
  );
  const [geoJsonNoHighlight, setGeoJsonNoHighlight] =
    React.useState<CapWarnings>();

  // This is used for testing purposes, so that when writing test, geojson can be passed through props
  React.useEffect(() => {
    if (geoJson) {
      setCurrentJson(geoJson);
      setGeoJsonNoHighlight(cloneDeep(geoJson));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    const fetchData = async (): Promise<CapWarnings> => {
      const response = await axios.get(
        'https://cap.opengeoweb.com/allalerts?url=https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
      );
      const capDataWithColors = addColors(response.data);
      setCurrentJson(capDataWithColors);
      setGeoJsonNoHighlight(cloneDeep(capDataWithColors));
      return null;
    };

    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const highLightGeoJson = (
    capData: CapWarnings,
    selectedFeature: GeoJSON.Feature,
  ): CapWarnings => {
    const foundFeature = capData.features.find(
      (feature) =>
        feature.properties.details.identifier ===
        selectedFeature.properties.details.identifier,
    );
    if (foundFeature) {
      const fill = foundFeature.properties.fill as string;
      const newFill = fill.replace('0.10', '0.25');
      foundFeature.properties.fill = newFill;
      foundFeature.properties['stroke-width'] = 3;
    }
    return capData;
  };

  const onPolyClick = (featureResult: FeatureEvent): void => {
    if (!featureResult) {
      // When clicked outside of polygon, reset styles
      setCurrentJson(cloneDeep(geoJsonNoHighlight));
      setSelectedFeatureEvent(featureResult);
    }

    // Set highlight styles for polygons
    setCurrentJson(
      highLightGeoJson(cloneDeep(geoJsonNoHighlight), featureResult.feature),
    );
    setSelectedFeatureEvent(featureResult);
  };

  return (
    <div data-testid="mapViewCap" style={{ height: '100%', width: '100%' }}>
      <MapView
        data-testid="mapView"
        mapId={generateMapId()}
        srs={srs}
        bbox={bbox}
      >
        <MapViewLayer data-testid="backroundMapLayer" {...baseLayer} />
        <MapViewLayer
          data-testid="polygonLayer"
          id={generateLayerId()}
          geojson={currentJson}
          onClickFeature={onPolyClick}
        />
        {selectedFeatureEvent && (
          <MapWarningProperties
            data-testid="mapWarnings"
            selectedFeatureProperties={
              selectedFeatureEvent.feature.properties.details
            }
          />
        )}
      </MapView>
    </div>
  );
};
