/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Card } from '@mui/material';
import React from 'react';
import { MapViewCap } from './MapViewCap';
import { mockGeoJson } from '../../utils/mockGeoJson';

export default { title: 'components/MapViewCap' };

export const MapViewCapDemo: React.FC = () => {
  const bbox = {
    left: -2324980.5498391856,
    bottom: 5890854.775012179,
    right: 6393377.702660825,
    top: 11652109.058827976,
  };

  const srs = 'EPSG:3857';

  return (
    <Card sx={{ height: '100vh', width: '100%', padding: '20px' }}>
      <MapViewCap geoJson={mockGeoJson} bbox={bbox} srs={srs} />
    </Card>
  );
};
