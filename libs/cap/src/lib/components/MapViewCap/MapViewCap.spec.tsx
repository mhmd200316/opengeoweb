/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { MapViewCap } from './MapViewCap';
import { mockGeoJson } from '../../utils/mockGeoJson';

describe('components/MapViewCap/MapViewCap', () => {
  const srs = 'EPSG:3857';
  const bbox = {
    left: -2324980.5498391856,
    bottom: 5890854.775012179,
    right: 6393377.702660825,
    top: 11652109.058827976,
  };

  const props = {
    srs,
    bbox,
    geoJson: mockGeoJson,
  };

  it('should render successfully', async () => {
    const { container } = render(<MapViewCap {...props} />);
    expect(container).toBeTruthy();
  });

  it('should render a feature (polygon) layer on top of map using demo geoJSON data', async () => {
    const { queryByTestId, queryAllByTestId } = render(
      <MapViewCap {...props} />,
    );

    expect(queryByTestId('backroundMapLayer')).toBeDefined();
    expect(queryByTestId('polygonLayer')).toBeDefined();

    const onePieceOfGeoJSONDemoData = 'Forest fire warning';
    expect(queryAllByTestId('mapViewLayer').length === 2).toBeTruthy(); // Two layers of type 'mapViewLayer' exist: one for backround and the other for polygon feature data
    expect(
      queryAllByTestId('mapViewLayer')[0].textContent.includes(
        onePieceOfGeoJSONDemoData,
      ),
    ).toBeFalsy(); // Ground map layer obviously does not contain any geoJSON demo data
    expect(
      queryAllByTestId('mapViewLayer')[1].textContent.includes(
        onePieceOfGeoJSONDemoData,
      ),
    ).toBeTruthy(); // Feature (polygon) layer contains geoJSON demo data
  });

  it('should *not* show map warning properties component on top of map when no click has yet occurred', async () => {
    const { queryByTestId } = render(<MapViewCap {...props} />);

    // These layers should be defined at all times ...
    expect(queryByTestId('mapViewCap')).toBeDefined();
    expect(queryByTestId('mapView')).toBeDefined();
    expect(queryByTestId('backroundMapLayer')).toBeDefined();
    expect(queryByTestId('polygonLayer')).toBeDefined();

    // ... but component for map warning properties should not be found there as no click has occurred
    expect(queryByTestId('mapWarnings')).toBeNull();
  });

  it('should *not* show any map warnings component on top of map when clicked outside of any feature (polygon)', async () => {
    // NOTE!
    // This test relies on the fact we know the simple map layer demo geoJSON data set in use -
    // where on the map to click to get desired things happen visually.
    //
    // This test also uses a fixed mouse click test position.
    // Changing the demo data (e.g. polygon shapes and positions) only but not adjusting this test position accordingly would ruin this test.

    const { container, queryByTestId } = render(<MapViewCap {...props} />);

    // Test clicking on the upper part of the container that we no is free from any polygons.
    const testXPosOutside = Math.round(0.3 * container.clientWidth);
    const testYPosOutside = Math.round(0.3 * container.clientHeight);
    fireEvent.mouseUp(container, {
      clientX: testXPosOutside,
      client: testYPosOutside,
    });

    // After click, at least these layers should be defined ...
    expect(queryByTestId('mapViewCap')).toBeDefined();
    expect(queryByTestId('mapView')).toBeDefined();
    expect(queryByTestId('backroundMapLayer')).toBeDefined();
    expect(queryByTestId('polygonLayer')).toBeDefined();

    // ... while the component for map warning properties should not be found there as no click was hit on any polygon
    expect(queryByTestId('mapWarnings')).toBeNull();
    expect(container.querySelector('[data-testid=mapWarnings]')).toBeNull();
  });

  it('should make map warnings component pop up on top of map only when some feature (polygon) on map is clicked', async () => {
    // NOTE!
    // This test relies on the fact we know the simple map layer demo geoJSON data set in use -
    // where on the map to click to get desired things happen visually.
    //
    // This test also uses a fixed mouse click test position.
    // Changing the demo data (e.g. polygon shapes and positions) only but not adjusting this test position accordingly would ruin this test.

    const { container, queryByTestId } = render(<MapViewCap {...props} />);

    // Test clicking on a position we know hits a certain polygon.
    const testXPosInside = Math.round(0.7 * container.clientWidth);
    const testYPosInside = Math.round(0.5 * container.clientHeight);
    fireEvent.mouseUp(container, {
      clientX: testXPosInside,
      clientY: testYPosInside,
    });

    // After click, at least these layers should be defined ...
    expect(queryByTestId('mapViewCap')).toBeDefined();
    expect(queryByTestId('mapView')).toBeDefined();
    expect(queryByTestId('backroundMapLayer')).toBeDefined();
    expect(queryByTestId('polygonLayer')).toBeDefined();

    // ... and map warnings component should appear on map
    expect(queryByTestId('mapWarnings')).toBeDefined();
    expect(container.querySelector('[data-testid=mapWarnings]')).toBeDefined();
  });
});
