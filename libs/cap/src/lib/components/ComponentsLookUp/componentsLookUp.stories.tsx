/* eslint-disable no-param-reassign */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import axios from 'axios';
import { Box, Card, Grid } from '@mui/material';
import { MapViewCap } from '../MapViewCap';
import { WarningListCap } from '../WarningListCap';
import { addColors } from '../../utils/addColors';

import { CapWarnings } from '../types';

export default { title: 'components/ComponentsLookUp' };

export const ComponentsLookUp: React.FC = () => {
  const bbox = {
    left: -2324980.5498391856,
    bottom: 5890854.775012179,
    right: 6393377.702660825,
    top: 11652109.058827976,
  };
  const srs = 'EPSG:3857';

  const [geoJson, setCapData] = React.useState<CapWarnings | null>(null);

  React.useEffect(() => {
    const fetchData = async (): Promise<any> => {
      const response = await axios.get(
        'https://cap.opengeoweb.com/allalerts?url=https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
      );
      const capDataWithColors = addColors(response.data);
      setCapData(capDataWithColors);
    };

    fetchData();
  }, []);

  return (
    <Grid container sx={{ width: '100%', height: '100%' }}>
      <Grid container item xs={6}>
        <Card
          sx={{
            height: '100vh',
            width: '100%',
            padding: '20px',
          }}
        >
          <MapViewCap geoJson={geoJson} bbox={bbox} srs={srs} />
        </Card>
      </Grid>
      <Grid container item xs={6}>
        <Box sx={{ width: '100%', height: '100%' }}>
          <WarningListCap geoJson={geoJson} />
        </Box>
      </Grid>
    </Grid>
  );
};
