/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';

import { MapViewCap } from '../MapViewCap';
import { WarningListCap } from '../WarningListCap';
import { CapPresets } from '../types';

export type SupportedComponentTypes = 'CapWarningList' | 'CapWarningMap';

interface InitialProps {
  bbox?: {
    left: number;
    bottom: number;
    right: number;
    top: number;
  };
  srs?: string;
}

export interface ComponentsLookUpPayload {
  capWarningPresets: CapPresets;
  componentType: SupportedComponentTypes;
  id: string;
  initialProps: InitialProps;
}

/**
 * The lookup table is for registering your own components with the screenmanager.
 * @param payload
 * @returns
 */

export const ComponentsLookUp = ({
  componentType,
  initialProps,
}: ComponentsLookUpPayload): React.ReactElement => {
  // TODO: implement polling when backend is ready
  // TODO: fetch new data using feedLanguage if previous response indicates there are new warnings

  // NOTE: Fetching data inside this component might not work, as using React Hooks causes Invalid Hook Call -errors in tests.

  switch (componentType) {
    case 'CapWarningMap': {
      return <MapViewCap bbox={initialProps.bbox} srs={initialProps.srs} />;
    }
    case 'CapWarningList': {
      return <WarningListCap />;
    }
    default:
      return null;
  }
};
