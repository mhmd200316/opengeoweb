/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { ComponentsLookUp } from './componentsLookUp';
import { mockGeoJson } from '../../utils/mockGeoJson';

describe('components/ComponentsLookUp', () => {
  it('should return the CapWarningMap Component', () => {
    const payload = {
      componentType: 'CapWarningMap' as const,
      id: 'viewid-1',
      initialProps: {
        bbox: {
          left: 58703.6377,
          bottom: 6408480.4514,
          right: 3967387.5161,
          top: 11520588.9031,
        },
        srs: 'EPSG:3857',
        geoJson: mockGeoJson,
      },
      capWarningPresets: {
        backendUrl: {
          feed: 'https://cap.opengeoweb.com/feed?url=',
          feedLinks: 'https://cap.opengeoweb.com/feedlinks?url=',
          singleAlert: 'https://cap.opengeoweb.com/alert?url=',
          allAlerts: 'https://cap.opengeoweb.com/allalerts?url=',
        },
        pollInterval: '300',
        feeds: [
          {
            feedAddress: 'https://api.met.no/weatherapi/metalerts/1.1',
            feedType: 'rss',
            languagePriority: 'no',
          },
        ],
      },
    };

    const component = ComponentsLookUp(payload);
    expect(component).toBeDefined();
  });

  it('should return Cap Map Component', () => {
    const payload = {
      componentType: 'CapWarningList' as const,
      id: 'viewid-1',
      initialProps: {},
      capWarningPresets: {
        backendUrl: {
          feed: 'https://cap.opengeoweb.com/feed?url=',
          feedLinks: 'https://cap.opengeoweb.com/feedlinks?url=',
          singleAlert: 'https://cap.opengeoweb.com/alert?url=',
          allAlerts: 'https://cap.opengeoweb.com/allalerts?url=',
        },
        pollInterval: '300',
        feeds: [
          {
            feedAddress: 'https://api.met.no/weatherapi/metalerts/1.1',
            feedType: 'rss',
            languagePriority: 'no',
          },
        ],
      },
    };

    const component = ComponentsLookUp(payload);
    expect(component).toBeDefined();
  });

  it('should return null if not recognized', () => {
    const payload = {
      title: 'title',
      componentType: undefined,
      id: 'viewid-1',
      initialProps: undefined,
      capWarningPresets: {
        backendUrl: {
          feed: 'https://cap.opengeoweb.com/feed?url=',
          feedLinks: 'https://cap.opengeoweb.com/feedlinks?url=',
          singleAlert: 'https://cap.opengeoweb.com/alert?url=',
          allAlerts: 'https://cap.opengeoweb.com/allalerts?url=',
        },
        pollInterval: '300',
        feeds: [
          {
            feedAddress: 'https://api.met.no/weatherapi/metalerts/1.1',
            feedType: 'rss',
            languagePriority: 'no',
          },
        ],
      },
    };

    const component = ComponentsLookUp(payload);
    expect(component).toBeNull();
  });
});
