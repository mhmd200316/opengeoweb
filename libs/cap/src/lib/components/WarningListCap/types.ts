/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

export interface CapData {
  alert: {
    identifier: string;
    sender: string;
    sent: string;
    status: string;
    msgType: string;
    scope: string;
    code: string[];
    references: string;
    info: Info[];
  };
}

interface Info {
  language: string;
  category: string;
  event: string;
  urgency: string;
  severity: string;
  certainty: string;
  eventCode: {
    valueName: string;
    value: string;
  };
  onset: string;
  expires: string;
  senderName: string;
  headline: string;
  description: string;
  web: string;
  contact: string;
  parameter: Parameter[];
  area: Area | Area[];
}

interface Parameter {
  valueName: string;
  value: number | string;
}

interface Area {
  areaDesc: string;
  polygon: string | string[];
  geocode: {
    valueName: string;
    value: string | number;
  };
}
