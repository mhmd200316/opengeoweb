/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { WarningListRowCap } from './WarningListRowCap';

export default { title: 'components/WarningListCap' };

const severeFlood = {
  event: 'Flood',
  severity: 'Minor',
  expires: '2022-05-13',
};

const highFlood = {
  event: 'FLood',
  severity: 'Moderate',
  expires: '2022-05-14',
};

const moderateFlood = {
  event: 'Flood',
  severity: 'Severe',
  expires: '2022-05-15',
};

const noFlood = {
  event: 'FLood',
  severity: 'Extreme',
  expires: '2022-05-16',
};

export const WarningListRowCapDemo: React.FC = () => (
  <>
    <WarningListRowCap {...severeFlood} />
    <WarningListRowCap {...highFlood} />
    <WarningListRowCap {...moderateFlood} />
    <WarningListRowCap {...noFlood} />
  </>
);
