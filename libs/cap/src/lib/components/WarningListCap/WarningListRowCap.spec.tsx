/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';
import { WarningListRowCap } from './WarningListRowCap';

describe('components/WarningListCap/WarningListCap', () => {
  const testProps = {
    event: 'High winds',
    severity: 'Moderate',
    expires: '2022-05-16',
  };

  it('should render properties successfully', async () => {
    const { findByText } = render(<WarningListRowCap {...testProps} />);
    expect(await findByText('High winds')).toBeTruthy();
    expect(await findByText('Moderate')).toBeTruthy();
    expect(await findByText('16 May 2022, 00:00 UTC')).toBeTruthy();
  });
});
