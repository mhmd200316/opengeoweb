/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import axios from 'axios';
import { Box, Card, CircularProgress } from '@mui/material';

import { WarningListRowCap } from './WarningListRowCap';
import { CapWarnings, CustomFeature } from '../types';
import { addColors } from '../../utils/addColors';

interface WarningListCapProps {
  geoJson?: CapWarnings;
}

export const WarningListCap: React.FC<WarningListCapProps> = ({ geoJson }) => {
  const [capFeatures, setCapFeatures] = React.useState<CustomFeature[]>([]);

  React.useEffect(() => {
    if (geoJson) {
      setCapFeatures(geoJson.features);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    const fetchData = async (): Promise<CapWarnings> => {
      const response = await axios.get(
        'https://cap.opengeoweb.com/allalerts?url=https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
      );
      const capDataWithColors = addColors(response.data);
      setCapFeatures(capDataWithColors.features);
      return null;
    };

    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Box sx={{ width: '100%', height: '100%', display: 'flex' }}>
      <Card
        sx={{
          justifyContent: 'center',
          alignItems: 'center',
          overflow: 'auto',
          height: '95%',
          width: '100%',
          padding: '10px',
          margin: '20px',
          marginBottom: '10px',
          position: 'relative',
        }}
        data-testid="warningList"
      >
        {capFeatures.length === 0 ? (
          <Box
            sx={{
              position: 'absolute',
              left: '50%',
              top: '20%',
              zIndex: '100',
            }}
          >
            <CircularProgress />
          </Box>
        ) : null}
        {capFeatures.map((object) => (
          <WarningListRowCap
            key={object.properties.details.identifier}
            event={object.properties.details.languages[2].event}
            severity={object.properties.details.severity}
            expires={object.properties.details.expires}
          />
        ))}
      </Card>
    </Box>
  );
};
