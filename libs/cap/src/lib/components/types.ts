/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

export interface CapPresets {
  backendUrl: Url;
  pollInterval: string;
  feeds: Feed[];
}

interface Url {
  feed: string;
  feedLinks: string;
  singleAlert: string;
  allAlerts: string;
}

interface Feed {
  feedAddress: string;
  feedType: string;
  languagePriority: string;
}

export interface CapWarnings extends GeoJSON.FeatureCollection {
  type: 'FeatureCollection';
  features: Array<CustomFeature>;
}

export interface CustomFeature {
  type: 'Feature';
  geometry: GeoJSON.Geometry;
  id?: string | number;
  properties: CustomProperties;
}

interface CustomProperties {
  fill?: string;
  stroke?: string;
  'stroke-width'?: number;
  'stroke-opacity'?: number;
  details: {
    identifier: string;
    areaDesc: string;
    senderName: string;
    severity: string;
    certainty: string;
    onset: string;
    expires: string;
    languages: Language[];
  };
}

interface Language {
  language: string;
  event: string;
  senderName: string;
  headline: string;
  description: string;
}
