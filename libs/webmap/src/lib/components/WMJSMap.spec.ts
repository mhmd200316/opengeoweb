/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { WMBBOX, WMLayer } from '.';
import { defaultReduxLayerRadarKNMI } from '../utils/testUtils';
import WMJSMap, {
  detectLeftButton,
  detectRightButton,
  getErrorsToDisplay,
} from './WMJSMap';
import * as WMJSTools from './WMJSTools';

describe('components/WMJSMap', () => {
  it('should initialise with correct elements', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    // load layer
    expect(
      map.loadingDiv.className.includes('WMJSDivBuffer-loading'),
    ).toBeTruthy();
    expect(baseElement.innerHTML).toContain('class="WMJSDivBuffer-loading"');
    // main div
    expect(baseElement.innerHTML).toContain(`id="${map.baseDiv.id}"`);
    // zoombox
    expect(baseElement.innerHTML).toContain('class="wmjs-zoombox"');
    // boundingbox
    expect(baseElement.innerHTML).toContain('class="wmjs-boundingbox"');
  });

  it('should attach events', () => {
    const mockBaseElement = document.createElement('div');
    mockBaseElement.addEventListener = jest.fn();
    mockBaseElement.appendChild = jest.fn();

    const map = new WMJSMap(mockBaseElement);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    map.baseDiv = mockBaseElement;

    map.attachEvents();

    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'mousedown',
      map.mouseDownEvent,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'mouseup',
      map.mouseUpEvent,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'mousemove',
      map.mouseMoveEvent,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'contextmenu',
      map.onContextMenu,
    );
    expect(mockBaseElement.addEventListener).toHaveBeenCalledWith(
      'wheel',
      map.mouseWheelEvent,
    );
  });

  it('should deattach events', () => {
    const mockBaseElement = document.createElement('div');
    mockBaseElement.removeEventListener = jest.fn();
    mockBaseElement.appendChild = jest.fn();

    const map = new WMJSMap(mockBaseElement);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    map.baseDiv = mockBaseElement;

    map.detachEvents();

    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'mousedown',
      map.mouseDownEvent,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'mouseup',
      map.mouseUpEvent,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'mousemove',
      map.mouseMoveEvent,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'contextmenu',
      map.onContextMenu,
    );
    expect(mockBaseElement.removeEventListener).toHaveBeenCalledWith(
      'wheel',
      map.mouseWheelEvent,
    );
  });

  it('should set correct cursors when start panning map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);

    map._mapPanStart(0, 0);

    expect(getComputedStyle(map.baseDiv).cursor).toEqual('move');
  });

  it('should set correct cursors when end panning map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);

    map._mapPanEnd(0, 0);

    expect(getComputedStyle(map.baseDiv).cursor).toEqual('default');
  });

  it('should set correct cursors when start zooming map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);
    map.mapZooming = 1;

    map._mapZoom();

    expect(getComputedStyle(map.baseDiv).cursor).toEqual('crosshair');

    // not allowed
    map.mouseX = 0;
    map.mouseDownX = 1;
    map.mouseY = 0;
    map.mouseDownY = 1;

    map._mapZoom();
    expect(getComputedStyle(map.baseDiv).cursor).toEqual('not-allowed');
  });

  it('should set correct cursors when end zooming map', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);
    map.mapZooming = 1;

    map._mapZoomEnd();

    expect(getComputedStyle(map.baseDiv).cursor).toEqual('default');
  });

  it('should handle fly to bbox', () => {
    const mockBaseElement = document
      .createElement('div')
      .appendChild(document.createElement('div'));
    const map = new WMJSMap(mockBaseElement);

    const spy = {
      flyZoomToBBOXStartZoom: jest.fn(),
      flyZoomToBBOXStop: jest.fn(),
      flyZoomToBBOXFly: {
        setBBOX: jest.fn(),
      },
    };

    expect(map.mouseX).toEqual(0);
    expect(map.mouseY).toEqual(0);
    const [newPageX, newPageY] = [100, 240];

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    map.wMFlyToBBox = spy;
    map.mouseWheelEvent({ pageX: newPageX, pageY: newPageY } as WheelEvent);

    expect(map.mouseX).toEqual(newPageX);
    expect(map.mouseY).toEqual(newPageY);

    expect(spy.flyZoomToBBOXStartZoom).toHaveBeenCalledWith(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      map.mouseWheelEventBBOXCurrent,
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      map.mouseWheelEventBBOXNew,
    );
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    map._mapPanStart();
    expect(spy.flyZoomToBBOXStop).toHaveBeenCalled();

    map.setBBOX(0, 0, 0, 0);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(spy.flyZoomToBBOXFly.setBBOX).toHaveBeenCalledWith(map.bbox);
  });

  it('should set mouse coordinates', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    expect(map.mouseX).toEqual(0);
    expect(map.mouseY).toEqual(0);
    const [newX, newY] = [100, 240];

    map.setMouseCoordinates(newX, newY);

    expect(map.mouseX).toEqual(newX);
    expect(map.mouseY).toEqual(newY);
  });

  it('should handle mouse movement', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    expect(map.mouseX).toEqual(0);
    expect(map.mouseY).toEqual(0);
    const [newX, newY] = [100, 240];

    map.mouseMove(newX, newY, new MouseEvent('move'));

    expect(map.mouseX).toEqual(newX);
    expect(map.mouseY).toEqual(newY);
  });

  it('should handle panning with percentage', () => {
    jest.useFakeTimers();

    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    expect(map.drawnBBOX).toMatchObject({
      bottom: -90,
      left: -180,
      right: 180,
      top: 90,
    });

    map.mapPanPercentage(0, 0);

    jest.runOnlyPendingTimers();

    const expectedBbox = {
      bottom: -180,
      left: -180,
      right: 180,
      top: 180,
    };
    // The map bbox is adjusted
    expect(map.drawnBBOX).toMatchObject(expectedBbox);

    map.mapPanPercentage(0.1, 0.1);

    // Bbox is not updated instantly because of debounce
    expect(map.drawnBBOX).toMatchObject(expectedBbox);

    jest.runOnlyPendingTimers();

    expect(map.drawnBBOX).toMatchObject({
      bottom: -216,
      left: -216,
      right: 144,
      top: 144,
    });

    jest.useRealTimers();
  });

  it('should get correct coordinates for document', () => {
    const baseElement = document.createElement('div');
    const parentElement = document.createElement('div');
    parentElement.appendChild(baseElement);
    const map = new WMJSMap(baseElement);

    expect(map.getMouseCoordinatesForDocument(new MouseEvent('click'))).toEqual(
      {
        x: 0,
        y: 0,
      },
    );

    expect(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      map.getMouseCoordinatesForDocument({ pageX: 10, pageY: 10 }),
    ).toEqual({
      x: 10,
      y: 10,
    });

    expect(
      map.getMouseCoordinatesForDocument(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            {
              screenX: 0,
              screenY: 0,
            },
          ],
        }),
      ),
    ).toEqual({
      x: 0,
      y: 0,
    });

    expect(
      map.getMouseCoordinatesForDocument(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            { screenX: 10, screenY: 10 },
          ],
        }),
      ),
    ).toEqual({
      x: 10,
      y: 10,
    });

    // mock offset
    Object.assign(parentElement, {
      ...parentElement,
      getBoundingClientRect: () => ({
        left: 100,
        top: 100,
      }),
    });

    expect(map.getMouseCoordinatesForDocument(new MouseEvent('click'))).toEqual(
      {
        x: -100,
        y: -100,
      },
    );

    const getMouseXCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseXCoordinate');
    const getMouseYCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseYCoordinate');
    const testEvent = new MouseEvent('click');
    map.getMouseCoordinatesForDocument(testEvent);
    expect(getMouseXCoordinateSpy).toHaveBeenCalledWith(testEvent);
    expect(getMouseYCoordinateSpy).toHaveBeenCalledWith(testEvent);
  });

  it('should get correct coordinates for element', () => {
    const baseElement = document.createElement('div');
    const parentElement = document.createElement('div');
    parentElement.appendChild(baseElement);

    const map = new WMJSMap(baseElement);

    expect(map.getMouseCoordinatesForElement(new MouseEvent('click'))).toEqual({
      x: 0,
      y: 0,
    });

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(map.getMouseCoordinatesForElement({ pageX: 10, pageY: 10 })).toEqual(
      {
        x: 10,
        y: 10,
      },
    );

    expect(
      map.getMouseCoordinatesForElement(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            {
              screenX: 0,
              screenY: 0,
            },
          ],
        }),
      ),
    ).toEqual({
      x: 0,
      y: 0,
    });

    expect(
      map.getMouseCoordinatesForElement(
        new TouchEvent('touchstart', {
          changedTouches: [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            {
              screenX: 10,
              screenY: 10,
            },
          ],
        }),
      ),
    ).toEqual({
      x: 10,
      y: 10,
    });

    // mock offset
    Object.assign(parentElement, {
      ...parentElement,
      getBoundingClientRect: () => ({
        left: 100,
        top: 100,
      }),
    });
    expect(map.getMouseCoordinatesForElement(new MouseEvent('click'))).toEqual({
      x: -100,
      y: -100,
    });

    const getMouseXCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseXCoordinate');
    const getMouseYCoordinateSpy = jest.spyOn(WMJSTools, 'getMouseYCoordinate');
    const testEvent = new MouseEvent('click');
    map.getMouseCoordinatesForElement(testEvent);
    expect(getMouseXCoordinateSpy).toHaveBeenCalledWith(testEvent);
    expect(getMouseYCoordinateSpy).toHaveBeenCalledWith(testEvent);
  });

  it('should handle layers', () => {
    const baseElement = document.createElement('div');

    const map = new WMJSMap(baseElement);
    const testLayer1 = new WMLayer({
      ...defaultReduxLayerRadarKNMI,
      id: 'wmjsmap-testlayer-1',
      enabled: true,
    });

    const testLayer2 = new WMLayer({
      ...defaultReduxLayerRadarKNMI,
      id: 'wmjsmap-testlayer-2',
      enabled: true,
    });

    map.addLayer(testLayer1);
    map.addLayer(testLayer2);
    expect(map.getLayers()).toEqual([testLayer2, testLayer1]);

    map.swapLayers(testLayer1, testLayer2);
    expect(map.getLayers()).toEqual([testLayer1, testLayer2]);

    map.removeAllLayers();
    expect(map.getLayers()).toEqual([]);
  });

  it('should set projection', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);
    const srs = 'EPSG:3857';
    const bbox = { left: -180, bottom: -90, right: 180, top: 90 };
    const wmbbox = new WMBBOX(bbox.left, bbox.bottom, bbox.right, bbox.top);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const defaultBboxSpy = jest.spyOn(map.defaultBBOX, 'setBBOX');
    map.setProjection(srs, wmbbox);

    expect(defaultBboxSpy).toHaveBeenCalledWith(wmbbox);
    expect(defaultBboxSpy).toHaveBeenCalledTimes(1);

    expect(map.getProjection()).toEqual({
      srs,
      bbox: expect.objectContaining(bbox),
    });

    // should not update defaultBBOX if srs has not changed
    map.setProjection(srs, new WMBBOX(-100, -100, 100, 100));
    expect(defaultBboxSpy).toHaveBeenCalledTimes(1);

    // should update defaultBBOX if srs has changed
    map.setProjection('EPSG:4326', new WMBBOX(100, 100, -100, -100));
    expect(defaultBboxSpy).toHaveBeenCalledTimes(2);
  });

  it('should set size when passing numbers', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    const width = 234;
    const height = 567;

    map.setSize(width, height);

    expect(map.getWidth()).toEqual(width);
    expect(map.getHeight()).toEqual(height);
  });

  it('should set size when passing strings', () => {
    const baseElement = document.createElement('div');
    const map = new WMJSMap(baseElement);

    const width = 234;
    const height = 567;

    map.setSize(width, height);

    expect(map.getWidth()).toEqual(width);
    expect(map.getHeight()).toEqual(height);
  });

  it('should detect left and right button clicks', () => {
    const mockEventClickLeft = new MouseEvent('click', { button: 0 });
    const mockEventClickLeft2 = new MouseEvent('click', { buttons: 1 });
    const mockEventClickMiddle = new MouseEvent('click', { button: 1 });
    const mockEventClickMiddle2 = new MouseEvent('click', { buttons: 4 });
    const mockEventClickRight = new MouseEvent('click', { button: 2 });
    const mockEventClickRight2 = new MouseEvent('click', { buttons: 2 });

    expect(detectLeftButton(mockEventClickLeft)).toBeTruthy();
    expect(detectLeftButton(mockEventClickLeft2)).toBeTruthy();
    expect(detectLeftButton(mockEventClickMiddle)).toBeFalsy();
    expect(detectLeftButton(mockEventClickMiddle2)).toBeFalsy();
    expect(detectLeftButton(mockEventClickRight)).toBeFalsy();
    expect(detectLeftButton(mockEventClickRight2)).toBeFalsy();

    expect(detectRightButton(mockEventClickLeft)).toBeFalsy();
    expect(detectRightButton(mockEventClickLeft2)).toBeFalsy();
    expect(detectRightButton(mockEventClickMiddle)).toBeFalsy();
    expect(detectRightButton(mockEventClickMiddle2)).toBeFalsy();
    expect(detectRightButton(mockEventClickRight)).toBeTruthy();
    expect(detectRightButton(mockEventClickRight2)).toBeTruthy();
  });

  describe('getErrorsToDisplay', () => {
    it('should only return errors for enabled layers', () => {
      const baseElement = document.createElement('div');

      const map = new WMJSMap(baseElement);
      const testLayer1 = new WMLayer({
        ...defaultReduxLayerRadarKNMI,
        id: 'wmjsmap-testlayer-1',
        enabled: true,
      });

      const testLayer2 = new WMLayer({
        ...defaultReduxLayerRadarKNMI,
        id: 'wmjsmap-testlayer-2',
        enabled: false,
        name: 'RADNL_OPER_R___25PCPRR_L3_KNMI-2',
      });

      map.addLayer(testLayer1);
      map.addLayer(testLayer2);

      const canvasErrors = [
        {
          linkedInfo: {
            layer: {
              id: 'wmjsmap-testlayer-1',
              name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
              title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
              service: 'https://testservice',
            } as WMLayer,
            message: 'Time too early',
          },
        },
        {
          linkedInfo: {
            layer: {
              id: 'wmjsmap-testlayer-2',
              name: 'RADNL_OPER_R___25PCPRR_L3_KNMI-2',
              title: 'RADNL_OPER_R___25PCPRR_L3_KNMI-2',
              service: 'https://testservice',
            } as WMLayer,
            message: 'Time too early',
          },
        },
      ];

      expect(
        getErrorsToDisplay(canvasErrors, map.getLayerByServiceAndName),
      ).toStrictEqual([
        `Layer with title ${testLayer1.title} failed to load, ${canvasErrors[0].linkedInfo.message}`,
      ]);
    });
  });
});
