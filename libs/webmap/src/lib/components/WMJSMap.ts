/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */
import { debounce } from 'throttle-debounce';
import WMImageStore, {
  getMapImageStore,
  WMImageEventType,
} from './WMImageStore';
import {
  isDefined,
  preventdefaultEvent,
  URLEncode,
  WMJScheckURL,
  getMouseXCoordinate,
  getMouseYCoordinate,
  getMapDimURL,
  DebugType,
  debug,
} from './WMJSTools';
import WMBBOX from './WMBBOX';
import WMProjection from './WMProjection';
import WMListener from './WMListener';
import WMTimer from './WMTimer';
import WMCanvasBuffer from './WMCanvasBuffer';
import WMAnimate from './WMAnimate';
import WMTileRenderer from './WMTileRenderer';
import { drawLegend, getLegendImageStore } from './WMLegend';
import { proj4 } from './WMJSExternalDependencies';
import WMJSDimension from './WMJSDimension';
import {
  WMDateOutSideRange,
  WMDateTooEarlyString,
  WMDateTooLateString,
  WMSVersion,
  WMProj4Defs,
  bgImageStoreLength,
} from './WMConstants';
import MapPin from './WMMapPin';
import WMLayer from './WMLayer';
import { LinkedInfo } from './types';
import { buildWMSGetMapRequest, getBBOXandProjString } from './WMJSMapHelpers';
import WMFlyToBBox from './WMFlyToBBox';

const version = '3.0.1';

/**
 * Set base URL of several sources used wihtin webmapjs
 */

/* Global vars */
let WebMapJSMapNo = 0;
const WebMapJSMapInstanceIds = [];

const bgMapImageStore = new WMImageStore(bgImageStoreLength, {
  id: 'bgMapImageStore',
});

/* GetFeature info handling */
function GetFeatureInfoObject(layer, data): void {
  this.layer = layer;
  this.data = data;
}

export const detectLeftButton = (evt: MouseEvent): boolean => {
  if (evt.buttons) {
    return evt.buttons === 1;
  }
  return evt.button === 0;
};

export const detectRightButton = (evt: MouseEvent): boolean => {
  if (evt.buttons) {
    return evt.buttons === 2;
  }
  return evt.button === 2;
};

export const getErrorsToDisplay = (
  canvasErrors: LinkedInfo[],
  getLayerByServiceAndName: (
    layerservice: string,
    layername: string,
  ) => WMLayer,
): string[] => {
  return canvasErrors.reduce((list, error): string[] => {
    if (error.linkedInfo && error.linkedInfo.layer) {
      const layer = getLayerByServiceAndName(
        error.linkedInfo.layer.service,
        error.linkedInfo.layer.name,
      );
      // Only show if layer is present and enabled
      if (layer && layer.enabled) {
        const message = error.linkedInfo.message
          ? `, ${error.linkedInfo.message}`
          : '';

        return list.concat(
          `Layer with title ${error.linkedInfo.layer.title} failed to load${message}`,
        );
      }
    } else {
      return list.concat('Layer failed to load.');
    }
    return list;
  }, []);
};

interface DivBBox extends HTMLElement {
  bbox?: WMBBOX;
  displayed?: boolean;
}

/**
/**
  * WMJSMap class
  */
export default class WMJSMap {
  mapPin: MapPin;

  mapdimensions: WMJSDimension[];

  stopAnimating: () => void;

  public showLayerInfo: boolean;

  public isAnimating: boolean;

  public animationDelay: number;

  public animationList;

  // TODO: (Sander de Snaijer 2020-06-30) make this private after fixing public access of other classs to this property
  public _animationList;

  public currentAnimationStep: number;

  public setAnimationDelay: (delay) => void;

  public isAnimatingLoopRunning: boolean;

  public checkAnimation: () => void;

  public animateBusy: boolean;

  public animationTimer;

  public mouseHoverAnimationBox: boolean;

  public loadingDiv: HTMLElement;

  public baseDiv: HTMLElement;

  public mapZooming: 0 | 1;

  public mouseX: number;

  public mouseDownX: number;

  public mouseY: number;

  public mouseDownY: number;

  private WebMapJSMapVersion: string;

  private mainElement: HTMLElement;

  private srs: string;

  private resizeBBOX: WMBBOX;

  private defaultBBOX: WMBBOX;

  private width: number;

  private height: number;

  private layers: WMLayer[];

  private baseLayers: WMLayer[];

  private numBaseLayers: number;

  private _map: WMJSMap;

  private renderer: string;

  private layersBusy: number | boolean;

  private mapBusy: boolean;

  private hasGeneratedId: boolean;

  private divZoomBox: HTMLElement;

  private divBoundingBox: DivBBox;

  private divDimInfo: HTMLElement;

  private _displayLegendInMap: boolean;

  private bbox: WMBBOX;

  public updateBBOX: WMBBOX;

  private loadedBBOX: WMBBOX;
  public drawnBBOX: WMBBOX;

  private divBuffer: WMCanvasBuffer;

  private isDestroyed: boolean;

  private mapHeader: {
    height: number;
    fill: {
      color: string;
      opacity: number;
    };
    hover: {
      color: string;
      opacity: number;
    };
    selected: {
      color: string;
      opacity: number;
    };
    hoverSelected: {
      color: string;
      opacity: number;
    };
    cursorSet: boolean;
    prevCursor: string;
    hovering: boolean;
  };

  private currentCursor: string;

  private mapIsActivated: boolean;

  private isMapHeaderEnabled: boolean;

  private showScaleBarInMap: boolean;

  private callBack: WMListener;

  private initialized: number;

  private suspendDrawing: boolean;

  private activeLayer: WMLayer;

  private MaxUndos: number;

  private NrOfUndos: number;

  private UndoPointer: number;

  private DoUndo: number;

  private DoRedo: number;

  private WMProjectionUndo: WMProjection[];

  private WMProjectionTempUndo: WMProjection[];

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private gfiDialogList: any[];

  private setTimeOffsetValue: string;

  private setMessageValue: string;

  private canvasErrors: LinkedInfo[];

  private resizeWidth: number;

  private resizeHeight: number;

  private _resizeTimerBusy: boolean;

  private _resizeTimer: WMTimer;

  private wmAnimate: WMAnimate;

  private wmTileRenderer: WMTileRenderer;

  private loadingDivTimer: WMTimer;

  private mouseWheelBusy: number;

  private wMFlyToBBox: WMFlyToBBox;

  private mouseWheelEventBBOXCurrent: WMBBOX;

  private mouseWheelEventBBOXNew: WMBBOX;

  private mouseUpX: number;

  private mouseUpY: number;

  private mouseDragging: number;

  private controlsBusy: boolean;

  private mouseDownPressed: number;

  private mapMode: string;

  private numGetFeatureInfoRequests: number;

  private getFeatureInfoResult: typeof GetFeatureInfoObject[];

  private oldMapMode: string;

  private InValidMouseAction: number;

  private resizingBBOXCursor: string;

  private resizingBBOXEnabled: string;

  private mouseGeoCoordXY: { x: number; y: number };

  private mouseUpdateCoordinates: { x: number; y: number };

  private mapPanning: number;

  private mapPanStartGeoCoords: { x: number; y: number };

  private proj4: { srs?: string; projection?: string };

  private previousMouseButtonState: string;

  private longlat: string;

  private tileRenderSettings: unknown;

  private drawPending: boolean;

  private needsRedraw: boolean;

  private buildLayerDimsBusy: boolean;

  constructor(_element: HTMLElement) {
    this.WebMapJSMapVersion = version;
    this.mainElement = _element;
    this.baseDiv = undefined;
    this.srs = undefined;
    this.resizeBBOX = new WMBBOX();
    this.defaultBBOX = new WMBBOX();
    this.width = 2;
    this.height = 2;
    this.layers = [];
    this.mapdimensions = []; // Array of Dimension;
    this.baseLayers = [];
    this.numBaseLayers = 0;
    this._map = this;
    this.renderer = 'WMCanvasBuffer';
    this.layersBusy = 0;
    this.mapBusy = false;
    this.hasGeneratedId = false;
    this.divZoomBox = document.createElement('div');
    this.divBoundingBox = document.createElement('div');
    this.divDimInfo = document.createElement('div');
    this._displayLegendInMap = true;
    this.bbox = new WMBBOX(); // Boundingbox that will be used for map loading
    this.updateBBOX = new WMBBOX(); // Boundingbox to move map without loading anything
    this.drawnBBOX = new WMBBOX(); // Boundingbox that is used when map is drawn

    this.divBuffer = undefined;

    this.isDestroyed = false;

    this.mapHeader = {
      height: 0,
      fill: {
        color: '#EEE',
        opacity: 0.4,
      },
      hover: {
        color: '#017daf',
        opacity: 0.9,
      },
      selected: {
        color: '#017daf',
        opacity: 1.0,
      },
      hoverSelected: {
        color: '#017daf',
        opacity: 1.0,
      },
      cursorSet: false,
      prevCursor: 'default',
      hovering: false,
    };

    this.currentCursor = 'default';
    this.mapIsActivated = false;
    this.isMapHeaderEnabled = false;
    this.showScaleBarInMap = true;
    this.mouseHoverAnimationBox = false;
    this.callBack = new WMListener();

    this.initialized = 0;
    this.suspendDrawing = false;
    this.activeLayer = undefined;
    /* Undo: */
    this.MaxUndos = 3;
    this.NrOfUndos = 0;
    this.UndoPointer = 0;
    this.DoUndo = 0;
    this.DoRedo = 0;

    this.WMProjectionUndo = new Array(this.MaxUndos);
    this.WMProjectionTempUndo = new Array(this.MaxUndos);
    for (let j = 0; j < this.MaxUndos; j += 1) {
      this.WMProjectionUndo[j] = new WMProjection();
      this.WMProjectionTempUndo[j] = new WMProjection();
    }

    /* Contains the event values for when the mouse was pressed down (used for checking the shiftKey); */
    this.gfiDialogList = [];
    this.setTimeOffsetValue = '';
    this.setMessageValue = '';
    this.canvasErrors = [];

    this.resizeWidth = -1;
    this.resizeHeight = -1;
    this._resizeTimerBusy = false;
    this._resizeTimer = new WMTimer();
    this.wmAnimate = undefined;
    this.loadingDivTimer = new WMTimer();

    this.mouseWheelBusy = 0;
    this.wMFlyToBBox = new WMFlyToBBox(this);

    this.mouseWheelEventBBOXCurrent = new WMBBOX();
    this.mouseWheelEventBBOXNew = new WMBBOX();
    this.mouseX = 0;
    this.mouseY = 0;
    this.mouseDownX = -10000;
    this.mouseDownY = -10000;
    this.mouseUpX = 10000;
    this.mouseUpY = 10000;
    this.mouseDragging = 0;
    this.controlsBusy = false;
    this.mouseDownPressed = 0;
    this.mapMode = 'pan'; /* pan,zoom,zoomout,info */

    this.numGetFeatureInfoRequests = 0;
    this.getFeatureInfoResult = [];
    this.oldMapMode = undefined;
    this.InValidMouseAction = 0;
    this.resizingBBOXCursor = '';
    this.resizingBBOXEnabled = '';
    this.mouseGeoCoordXY = undefined;
    this.mouseUpdateCoordinates = undefined;
    this.mapPanning = 0;
    this.mapPanStartGeoCoords = undefined;
    this.mapZooming = 0;
    this.proj4 = {}; /* proj4 remembers current projection */
    this.proj4.srs = 'empty';
    this.proj4.projection = undefined;
    this.longlat = 'EPSG:4326';
    if (proj4) proj4.defs(WMProj4Defs);
    this.previousMouseButtonState = 'up';

    /* Binds */
    this.setWMTileRendererTileSettings =
      this.setWMTileRendererTileSettings.bind(this);
    this.makeComponentId = this.makeComponentId.bind(this);

    this.setMessage = this.setMessage.bind(this);
    this.setTimeOffset = this.setTimeOffset.bind(this);
    this.init = this.init.bind(this);
    this.rebuildMapDimensions = this.rebuildMapDimensions.bind(this);
    this.getLayerByServiceAndName = this.getLayerByServiceAndName.bind(this);
    this.getLayers = this.getLayers.bind(this);
    this.setLayer = this.setLayer.bind(this);
    this.setActive = this.setActive.bind(this);
    this.setActiveLayer = this.setActiveLayer.bind(this);
    this.calculateNumBaseLayers = this.calculateNumBaseLayers.bind(this);
    this.enableLayer = this.enableLayer.bind(this);
    this.disableLayer = this.disableLayer.bind(this);
    this.toggleLayer = this.toggleLayer.bind(this);
    this.displayLayer = this.displayLayer.bind(this);
    this._getLayerIndex = this._getLayerIndex.bind(this);
    this.removeAllLayers = this.removeAllLayers.bind(this);
    this.deleteLayer = this.deleteLayer.bind(this);
    this.moveLayerDown = this.moveLayerDown.bind(this);
    this.swapLayers = this.swapLayers.bind(this);
    this.moveLayerUp = this.moveLayerUp.bind(this);
    this.addLayer = this.addLayer.bind(this);
    this.getActiveLayer = this.getActiveLayer.bind(this);
    this.setProjection = this.setProjection.bind(this);
    this.getBBOX = this.getBBOX.bind(this);
    this.getMapPin = this.getMapPin.bind(this);
    this.getProjection = this.getProjection.bind(this);
    this.getSize = this.getSize.bind(this);
    this.getWidth = this.getWidth.bind(this);
    this.getHeight = this.getHeight.bind(this);
    this.repositionLegendGraphic = this.repositionLegendGraphic.bind(this);
    this.setSize = this.setSize.bind(this);
    this._setSize = this._setSize.bind(this);
    this.abort = this.abort.bind(this);
    this.isBusy = this.isBusy.bind(this);
    this._makeInfoHTML = this._makeInfoHTML.bind(this);
    this.showScaleBar = this.showScaleBar.bind(this);
    this.hideScaleBar = this.hideScaleBar.bind(this);
    this.display = this.display.bind(this);
    this.draw = this.draw.bind(this);
    this._draw = this._draw.bind(this);
    this._drawAndLoad = this._drawAndLoad.bind(this);
    this._drawReady = this._drawReady.bind(this);
    this._onLayersReadyCallbackFunction =
      this._onLayersReadyCallbackFunction.bind(this);
    this._onMapReadyCallbackFunction =
      this._onMapReadyCallbackFunction.bind(this);
    this._onResumeSuspendCallbackFunction =
      this._onResumeSuspendCallbackFunction.bind(this);
    this._animFrameRedraw = this._animFrameRedraw.bind(this);
    this.getWMSRequests = this.getWMSRequests.bind(this);
    this._pdraw = this._pdraw.bind(this);
    this._updateBoundingBox = this._updateBoundingBox.bind(this);
    this.redrawBuffer = this.redrawBuffer.bind(this);
    this.addBaseLayers = this.addBaseLayers.bind(this);
    this.setBaseLayers = this.setBaseLayers.bind(this);
    this.getBaseLayers = this.getBaseLayers.bind(this);
    this.getNumLayers = this.getNumLayers.bind(this);
    this.getBaseElement = this.getBaseElement.bind(this);
    this.mouseWheelEvent = this.mouseWheelEvent.bind(this);
    this.destroy = this.destroy.bind(this);
    this.detachEvents = this.detachEvents.bind(this);
    this.attachEvents = this.attachEvents.bind(this);
    this._buildLayerDims = this._buildLayerDims.bind(this);
    this.getMapMode = this.getMapMode.bind(this);
    this.getWMSGetFeatureInfoRequestURL =
      this.getWMSGetFeatureInfoRequestURL.bind(this);

    this.setMapModeGetInfo = this.setMapModeGetInfo.bind(this);
    this.setMapModeZoomBoxIn = this.setMapModeZoomBoxIn.bind(this);
    this.setMapModeZoomOut = this.setMapModeZoomOut.bind(this);
    this.setMapModePan = this.setMapModePan.bind(this);
    this.setMapModePoint = this.setMapModePoint.bind(this);
    this.setMapModeNone = this.setMapModeNone.bind(this);
    this.getMouseCoordinatesForDocument =
      this.getMouseCoordinatesForDocument.bind(this);
    this.getMouseCoordinatesForElement =
      this.getMouseCoordinatesForElement.bind(this);
    this.mouseDown = this.mouseDown.bind(this);

    this._checkInvalidMouseAction = this._checkInvalidMouseAction.bind(this);
    this.updateMouseCursorCoordinates =
      this.updateMouseCursorCoordinates.bind(this);
    this.mouseDownEvent = this.mouseDownEvent.bind(this);
    this.mouseMoveEvent = this.mouseMoveEvent.bind(this);
    this.mouseUpEvent = this.mouseUpEvent.bind(this);
    this.mouseMove = this.mouseMove.bind(this);
    this.mouseUp = this.mouseUp.bind(this);
    this._mouseDragStart = this._mouseDragStart.bind(this);
    this.mouseDrag = this.mouseDrag.bind(this);
    this.mouseDragEnd = this.mouseDragEnd.bind(this);
    this._mapPanStart = this._mapPanStart.bind(this);
    this._mapPan = this._mapPan.bind(this);
    this._mapPanEnd = this._mapPanEnd.bind(this);
    this.mapPanPercentage = this.mapPanPercentage.bind(this);
    this._mapPanPercentageEnd = debounce(500, this._mapPanPercentageEnd);
    this._mapZoomStart = this._mapZoomStart.bind(this);
    this._mapZoom = this._mapZoom.bind(this);
    this._mapZoomEnd = this._mapZoomEnd.bind(this);
    this.setCursor = this.setCursor.bind(this);
    this.getId = this.getId.bind(this);
    this.zoomTo = this.zoomTo.bind(this);
    this.pixelCoordinatesToXY = this.pixelCoordinatesToXY.bind(this);
    this.getGeoCoordFromPixelCoord = this.getGeoCoordFromPixelCoord.bind(this);
    this.getProj4 = this.getProj4.bind(this);
    this.getPixelCoordFromLatLong = this.getPixelCoordFromLatLong.bind(this);
    this.calculateBoundingBoxAndZoom =
      this.calculateBoundingBoxAndZoom.bind(this);
    this.getLatLongFromPixelCoord = this.getLatLongFromPixelCoord.bind(this);
    this.getPixelCoordFromGeoCoord = this.getPixelCoordFromGeoCoord.bind(this);
    this.addListener = this.addListener.bind(this);
    this.removeListener = this.removeListener.bind(this);
    this.getListener = this.getListener.bind(this);
    this.suspendEvent = this.suspendEvent.bind(this);
    this.resumeEvent = this.resumeEvent.bind(this);
    this.getDimensionList = this.getDimensionList.bind(this);
    this.getDimension = this.getDimension.bind(this);
    this.setDimension = this.setDimension.bind(this);
    this.zoomToLayer = this.zoomToLayer.bind(this);
    this.setBBOX = this.setBBOX.bind(this);
    this.zoomOut = this.zoomOut.bind(this);
    this.zoomIn = this.zoomIn.bind(this);
    this.displayLegendInMap = this.displayLegendInMap.bind(this);
    this.showBoundingBox = this.showBoundingBox.bind(this);
    this.hideBoundingBox = this.hideBoundingBox.bind(this);
    this._adagucBeforeDraw = this._adagucBeforeDraw.bind(this);
    this._adagucBeforeCanvasDisplay =
      this._adagucBeforeCanvasDisplay.bind(this);
    this.displayScaleBarInMap = this.displayScaleBarInMap.bind(this);
    this.configureMapDimensions = this.configureMapDimensions.bind(this);
    this.mapPin = new MapPin(this);

    this.loadingDiv = document.createElement('div');
    this.loadingDiv.className = 'WMJSDivBuffer-loading';

    this.init();
  }

  setWMTileRendererTileSettings(_WMTileRendererTileSettings: unknown): void {
    this.tileRenderSettings = _WMTileRendererTileSettings;
  }

  /**
   * Function which make a component ID which is unique over several WebMapJS instances
   * @param the desired id
   * @return the unique id
   */
  makeComponentId(id: string): string {
    let availableId = `WebMapJSMapNo_${WebMapJSMapNo}`;
    if (!this.mainElement.id) {
      for (const testDestroyed in WebMapJSMapInstanceIds) {
        if (WebMapJSMapInstanceIds[testDestroyed] === false) {
          availableId = testDestroyed;
          this.hasGeneratedId = true;
          break;
        }
      }
      this.mainElement.id = availableId;
      WebMapJSMapInstanceIds[this.mainElement.id] = true;
    }
    if (this.hasGeneratedId === false) {
      this.hasGeneratedId = true;
      WebMapJSMapNo += 1;
    }
    return `${this.mainElement.id}_${id}`;
  }

  setMessage(message: string): void {
    if (!message || message === '') {
      this.setMessageValue = '';
    } else {
      this.setMessageValue = message;
    }
  }

  setTimeOffset(message: string): void {
    if (!message || message === '') {
      this.setTimeOffsetValue = '';
    } else {
      this.setTimeOffsetValue = message;
    }
  }

  _adagucBeforeDraw(ctx: CanvasRenderingContext2D): void {
    if (this.baseLayers) {
      for (let l = 0; l < this.baseLayers.length; l += 1) {
        if (
          this.baseLayers[l].enabled &&
          this.baseLayers[l].keepOnTop === false &&
          this.baseLayers[l].type &&
          this.baseLayers[l].type === 'twms' &&
          this.tileRenderSettings
        ) {
          ctx.globalAlpha = this.baseLayers[l].opacity;
          const { attributionText } = this.wmTileRenderer.render(
            this.bbox,
            this.updateBBOX,
            this.srs,
            this.width,
            this.height,
            ctx,
            bgMapImageStore,
            this.tileRenderSettings,
            this.baseLayers[l].name,
          );
          ctx.globalAlpha = 1.0;

          const adagucAttribution = `ADAGUC webmapjs ${this.WebMapJSMapVersion}`;
          const txt = attributionText
            ? `${adagucAttribution} | ${attributionText}`
            : adagucAttribution;
          const x = this.width - 8;
          const y = this.height - 8;
          ctx.font = '10px Arial';
          ctx.textAlign = 'right';
          ctx.textBaseline = 'middle';
          ctx.fillStyle = '#FFF';
          ctx.globalAlpha = 0.75;
          const { width } = ctx.measureText(txt);
          ctx.fillRect(x - width, y - 6, width + 8, 14);
          ctx.fillStyle = '#444';
          ctx.globalAlpha = 1.0;
          ctx.fillText(txt, x + 4, y + 2);
        }
      }
    }
  }

  _adagucBeforeCanvasDisplay(ctx: CanvasRenderingContext2D): void {
    const drawTextBG = (
      _ctx: CanvasRenderingContext2D,
      txt: string,
      x: number,
      y: number,
      fontSize: number,
    ): void => {
      _ctx.textBaseline = 'top';
      _ctx.textAlign = 'left';
      _ctx.fillStyle = '#FFF';
      _ctx.globalAlpha = 0.75;
      const { width } = _ctx.measureText(txt);
      _ctx.fillRect(x - 8, y - 8, width + 16, fontSize + 14);
      _ctx.fillStyle = '#444';
      _ctx.globalAlpha = 1.0;
      _ctx.fillText(txt, x, y + 2);
    };
    /* Map Pin */
    if (this.mapPin.displayMapPin) {
      this.mapPin.drawMarker(ctx);
    }

    // /* Draw legends */
    if (this._displayLegendInMap) {
      drawLegend(ctx, this.width, this.height, this.layers);
    }

    /* Map header */
    if (this.isMapHeaderEnabled) {
      ctx.beginPath();
      ctx.rect(0, 0, this.width, this.mapHeader.height);
      if (this.mapIsActivated === false) {
        ctx.globalAlpha = this.mapHeader.hovering
          ? this.mapHeader.hover.opacity
          : this.mapHeader.fill.opacity;
        ctx.fillStyle = this.mapHeader.hovering
          ? this.mapHeader.hover.color
          : this.mapHeader.fill.color;
      } else {
        ctx.globalAlpha = this.mapHeader.hovering
          ? this.mapHeader.hoverSelected.opacity
          : this.mapHeader.selected.opacity;
        ctx.fillStyle = this.mapHeader.hovering
          ? this.mapHeader.hoverSelected.color
          : this.mapHeader.selected.color;
      }
      ctx.fill();
      ctx.globalAlpha = 1;
    }

    /* Gather errors */
    for (let i = 0; i < this.layers.length; i += 1) {
      for (let j = 0; j < this.layers[i].dimensions.length; j += 1) {
        if (this.layers[i].dimensions[j].currentValue === WMDateOutSideRange) {
          this.canvasErrors.push({
            linkedInfo: {
              layer: this.layers[i],
              message: 'Time outside range',
            },
          });
        } else if (
          this.layers[i].dimensions[j].currentValue === WMDateTooEarlyString
        ) {
          this.canvasErrors.push({
            linkedInfo: { layer: this.layers[i], message: 'Time too early' },
          });
        } else if (
          this.layers[i].dimensions[j].currentValue === WMDateTooLateString
        ) {
          this.canvasErrors.push({
            linkedInfo: { layer: this.layers[i], message: 'Time too late' },
          });
        }
      }
    }

    /* Display errors found during drawing canvas */
    if (this.canvasErrors && this.canvasErrors.length > 0) {
      const errorsToDisplay = getErrorsToDisplay(
        this.canvasErrors,
        this.getLayerByServiceAndName,
      );
      this.canvasErrors = [];

      if (errorsToDisplay.length > 0) {
        const mw = this.width / 2;
        const mh = 6 + errorsToDisplay.length * 15;
        const mx = this.width - mw;
        const my = this.isMapHeaderEnabled ? this.mapHeader.height : 0;
        ctx.beginPath();
        ctx.rect(mx, my, mx + mw, my + mh);
        ctx.fillStyle = 'white';
        ctx.globalAlpha = 0.9;
        ctx.fill();
        ctx.globalAlpha = 1;
        ctx.fillStyle = 'black';
        ctx.font = '10pt Helvetica';
        ctx.textAlign = 'left';

        errorsToDisplay.forEach((message, j) => {
          ctx.fillText(message, mx + 5, my + 11 + j * 15);
        });
      }
    }

    // Time offset message
    if (this.setTimeOffsetValue !== '') {
      ctx.font = '14px Helvetica';
      drawTextBG(
        ctx,
        this.setTimeOffsetValue,
        this.width / 2 - 70,
        this.height - 26,
        20,
      );
    }

    // Set message value
    if (this.setMessageValue !== '') {
      ctx.font = '15px Helvetica';
      drawTextBG(ctx, this.setMessageValue, this.width / 2 - 70, 2, 15);
    }

    // ScaleBar
    if (this.showScaleBarInMap === true) {
      const getScaleBarProperties = (): { width: number; mapunits: number } => {
        const desiredWidth = 25;
        let realWidth = 0;
        let numMapUnits = 1.0 / 10000000.0;

        const boxWidth = this.updateBBOX.right - this.updateBBOX.left;
        const pixelsPerUnit = this.width / boxWidth;
        if (pixelsPerUnit <= 0) {
          return undefined;
        }

        const a = desiredWidth / pixelsPerUnit;

        let divFactor = 0;
        do {
          numMapUnits *= 10.0;
          divFactor = a / numMapUnits;
          if (divFactor === 0) return undefined;
          realWidth = desiredWidth / divFactor;
        } while (realWidth < desiredWidth);

        do {
          numMapUnits /= 2.0;
          divFactor = a / numMapUnits;
          if (divFactor === 0) return undefined;
          realWidth = desiredWidth / divFactor;
        } while (realWidth > desiredWidth);

        do {
          numMapUnits *= 1.2;
          divFactor = a / numMapUnits;
          if (divFactor === 0) return undefined;
          realWidth = desiredWidth / divFactor;
        } while (realWidth < desiredWidth);

        let roundedMapUnits = numMapUnits;

        const d = Math.pow(10, Math.round(Math.log10(numMapUnits) + 0.5) - 1);

        roundedMapUnits = Math.round(roundedMapUnits / d);
        if (roundedMapUnits < 2.5) roundedMapUnits = 2.5;
        if (roundedMapUnits > 2.5 && roundedMapUnits < 7.5) roundedMapUnits = 5;
        if (roundedMapUnits > 7.5) roundedMapUnits = 10;
        roundedMapUnits *= d;
        divFactor = desiredWidth / pixelsPerUnit / roundedMapUnits;
        realWidth = desiredWidth / divFactor;
        return {
          width: parseInt(realWidth as unknown as string, 10),
          mapunits: roundedMapUnits,
        };
      };

      const scaleBarProps = getScaleBarProperties();
      if (scaleBarProps) {
        const offsetX = 7.5;
        const offsetY = this.height - 25.5;
        const scaleBarHeight = 23;
        ctx.beginPath();
        ctx.lineWidth = 2.5;
        ctx.fillStyle = '#000';
        ctx.strokeStyle = '#000';
        ctx.font = '9px Helvetica';
        ctx.textBaseline = 'middle';
        ctx.textAlign = 'left';
        for (let j = 0; j < 2; j += 1) {
          ctx.moveTo(offsetX, scaleBarHeight - 2 - j + offsetY);
          ctx.lineTo(
            scaleBarProps.width * 2 + offsetX + 1,
            scaleBarHeight - 2 - j + offsetY,
          );
        }

        const subDivXW = parseInt(
          Math.round(scaleBarProps.width / 5) as unknown as string,
          10,
        );
        ctx.lineWidth = 0.5;
        for (let j = 1; j < 5; j += 1) {
          ctx.moveTo(offsetX + subDivXW * j, scaleBarHeight - 2 + offsetY);
          ctx.lineTo(offsetX + subDivXW * j, scaleBarHeight - 2 - 5 + offsetY);
        }
        ctx.lineWidth = 1.0;
        ctx.moveTo(offsetX, scaleBarHeight - 2 + offsetY);
        ctx.lineTo(offsetX, scaleBarHeight - 2 - 7 + offsetY);
        ctx.moveTo(offsetX + scaleBarProps.width, scaleBarHeight - 2 + offsetY);
        ctx.lineTo(
          offsetX + scaleBarProps.width,
          scaleBarHeight - 2 - 7 + offsetY,
        );
        ctx.moveTo(
          offsetX + scaleBarProps.width * 2 + 1,
          scaleBarHeight - 2 + offsetY,
        );
        ctx.lineTo(
          offsetX + scaleBarProps.width * 2 + 1,
          scaleBarHeight - 2 - 7 + offsetY,
        );

        let units = '';
        if (this.srs === 'EPSG:3411') units = 'meter';
        if (this.srs === 'EPSG:3412') units = 'meter';
        if (this.srs === 'EPSG:3575') units = 'meter';
        if (this.srs === 'EPSG:4326') units = 'degrees';
        if (this.srs === 'EPSG:28992') units = 'meter';
        if (this.srs === 'EPSG:32661') units = 'meter';
        if (this.srs === 'EPSG:3857') units = 'meter';
        if (this.srs === 'EPSG:900913') units = 'meter';
        if (this.srs === 'EPSG:102100') units = 'meter';

        if (units === 'meter') {
          if (scaleBarProps.mapunits > 1000) {
            scaleBarProps.mapunits /= 1000;
            units = 'km';
          }
        }
        ctx.fillText('0', offsetX - 3, 12 + offsetY);

        let valueStr = `${scaleBarProps.mapunits.toPrecision()}`;
        ctx.fillText(
          valueStr,
          offsetX + scaleBarProps.width - valueStr.length * 2.5 + 0,
          12 + offsetY,
        );
        valueStr = `${(scaleBarProps.mapunits * 2).toPrecision()}`;
        ctx.fillText(
          valueStr,
          offsetX + scaleBarProps.width * 2 - valueStr.length * 2.5 + 0,
          12 + offsetY,
        );
        ctx.fillText(
          units,
          offsetX + scaleBarProps.width * 2 + 10,
          18 + offsetY,
        );
        ctx.stroke();
      }
    }

    // Mouse projected coords
    ctx.font = '10px Helvetica';
    ctx.textBaseline = 'middle';
    ctx.textAlign = 'left';
    if (isDefined(this.mouseGeoCoordXY)) {
      let roundingFactor =
        1.0 /
        Math.pow(
          10,
          parseInt(
            (Math.log((this.bbox.right - this.bbox.left) / this.width) /
              Math.log(10)) as unknown as string,
            10,
          ) - 2,
        );
      if (roundingFactor < 1) roundingFactor = 1;
      ctx.fillStyle = '#000000';
      const xText =
        Math.round(this.mouseGeoCoordXY.x * roundingFactor) / roundingFactor;
      const yText =
        Math.round(this.mouseGeoCoordXY.y * roundingFactor) / roundingFactor;
      let units = '';
      if (this.srs === 'EPSG:3857') {
        units = 'meter';
      }
      ctx.fillText(
        `CoordYX: (${yText}, ${xText}) ${units}`,
        5,
        this.height - 50,
      );
    }
    // Mouse latlon coords
    if (isDefined(this.mouseUpdateCoordinates)) {
      const llCoord = this.getLatLongFromPixelCoord(
        this.mouseUpdateCoordinates,
      );

      if (isDefined(llCoord)) {
        const roundingFactor = 100;
        ctx.fillStyle = '#000000';
        const xText = Math.round(llCoord.x * roundingFactor) / roundingFactor;
        const yText = Math.round(llCoord.y * roundingFactor) / roundingFactor;
        ctx.fillText(
          `Lat/Lon: (${yText.toFixed(2)}, ${xText.toFixed(2)}) degrees`,
          5,
          this.height - 38,
        );
      }
    }
    ctx.fillStyle = '#000000';
    ctx.fillText(`Map projection: ${this.srs}`, 5, this.height - 26);

    /* Show layer metadata */
    if (this.showLayerInfo === true) {
      const metadataX = 100;
      const metadataY = 100;
      const dimStartX = 150;

      const rowHeight = 26;
      let rowNumber = 0;
      drawTextBG(
        ctx,
        'Layer name',
        metadataX,
        metadataY + rowNumber * rowHeight,
        10,
      );
      drawTextBG(
        ctx,
        'Dimensions',
        metadataX + dimStartX,
        metadataY + rowNumber * rowHeight,
        10,
      );
      rowNumber += 1;
      for (let j = 0; j < this.layers.length; j += 1) {
        const layer = this.layers[j];
        drawTextBG(
          ctx,
          `#${j}:${layer.name}`,
          metadataX,
          metadataY + rowNumber * rowHeight,
          10,
        );
        for (let d = 0; d < layer.dimensions.length; d += 1) {
          const dimension = layer.dimensions[d];
          drawTextBG(
            ctx,
            `${dimension.name} = ${dimension.currentValue}`,
            metadataX + d * 200 + dimStartX,
            metadataY + rowNumber * rowHeight,
            10,
          );
        }
        rowNumber += 1;
      }
    }
  }

  /* Is called when the WebMapJS object is created */
  init(): void {
    if (!this.mainElement) {
      return;
    }
    if (this.mainElement.style) {
      if (!this.mainElement.style.height) {
        this.mainElement.style.height = '1px';
      }
      if (!this.mainElement.style.width) {
        this.mainElement.style.width = '1px';
      }
    }
    // baseDiv
    const baseDivId = this.makeComponentId('baseDiv');
    this.baseDiv = document.createElement('div');
    this.baseDiv.id = baseDivId;
    Object.assign(this.baseDiv.style, {
      position: 'relative',
      overflow: 'hidden',
      width: this.mainElement.clientWidth,
      height: this.mainElement.clientHeight,
      margin: 0,
      padding: 0,
      clear: 'both',
      left: '0px',
      top: '0px',
      cursor: 'default',
    });

    this.mainElement.appendChild(this.baseDiv);
    this.mainElement.style.margin = '0px';
    this.mainElement.style.padding = '0px';
    this.mainElement.style.border = 'none';
    this.mainElement.style.lineHeight = '0px';
    this.mainElement.style.display = 'inline-block';

    // Attach zoombox
    this.divZoomBox.className = 'wmjs-zoombox';
    this.divZoomBox.style.position = 'absolute';
    this.divZoomBox.style.display = 'none';
    this.divZoomBox.style.border = '2px dashed #000000';
    this.divZoomBox.style.margin = '0px';
    this.divZoomBox.style.padding = '0px';
    this.divZoomBox.style.lineHeight = '0';
    this.divZoomBox.style.background = '#AFFFFF';
    this.divZoomBox.style.opacity = '0.3'; // Gecko
    this.divZoomBox.style.filter = 'alpha(opacity=30)'; // Windows
    this.divZoomBox.style.left = '0px';
    this.divZoomBox.style.top = '0px';
    this.divZoomBox.style.width = '100px';
    this.divZoomBox.style.height = '100px';
    this.divZoomBox.style.zIndex = '1000';
    this.divZoomBox.oncontextmenu = (): boolean => {
      return false;
    };
    this.baseDiv.appendChild(this.divZoomBox);

    // Attach bbox box
    this.divBoundingBox.className = 'wmjs-boundingbox';
    this.divBoundingBox.style.position = 'absolute';
    this.divBoundingBox.style.display = 'none';
    this.divBoundingBox.style.border = '3px solid #6060FF';
    this.divBoundingBox.style.margin = '0px';
    this.divBoundingBox.style.padding = '0px';
    this.divBoundingBox.style.lineHeight = '0';
    this.divBoundingBox.style.left = '0px';
    this.divBoundingBox.style.top = '0px';
    this.divBoundingBox.style.width = '100px';
    this.divBoundingBox.style.height = '100px';
    this.divBoundingBox.style.zIndex = '1000';
    this.divBoundingBox.oncontextmenu = (): boolean => {
      return false;
    };
    this.baseDiv.appendChild(this.divBoundingBox);

    /* Attach divDimInfo */
    this.divDimInfo.style.position = 'absolute';
    this.divDimInfo.style.zIndex = '1000';
    this.divDimInfo.style.width = 'auto';
    this.divDimInfo.style.height = 'auto';
    this.divDimInfo.style.background = 'none';

    this.divDimInfo.oncontextmenu = (): boolean => {
      return false;
    };
    this.divDimInfo.innerHTML = '';
    this.baseDiv.appendChild(this.divDimInfo);

    /* Attach loading image */

    this.baseDiv.appendChild(this.loadingDiv);
    /* Attach events */
    this.attachEvents();

    this.bbox.left = -180;
    this.bbox.bottom = -90;
    this.bbox.right = 180;
    this.bbox.top = 90;
    this.srs = 'EPSG:4326';

    this.setSize(this.mainElement.clientWidth, this.mainElement.clientHeight);
    // IMAGE buffer
    this.divBuffer = new WMCanvasBuffer(
      this.callBack,
      getMapImageStore,
      this.getWidth(),
      this.getHeight(),
      {
        beforecanvasstartdraw: this._adagucBeforeDraw,
        beforecanvasdisplay: this._adagucBeforeCanvasDisplay,
        canvasonerror: (e): void => {
          this.canvasErrors = e;
        },
      },
    );

    this.baseDiv.appendChild(this.divBuffer.getBuffer());

    getLegendImageStore().addImageEventCallback(
      (image, id, imageEventType: WMImageEventType) => {
        if (imageEventType === WMImageEventType.Loaded) {
          this.draw('legendImageStore loaded');
        }
      },
      this.mainElement.id,
    );

    this.callBack.addToCallback('display', this.display, true);
    this.callBack.addToCallback(
      'draw',
      () => {
        debug(DebugType.Log, 'draw event triggered externally, skipping');
      },
      true,
    );

    bgMapImageStore.addImageEventCallback(
      (image, id, imageEventType: WMImageEventType) => {
        if (imageEventType === WMImageEventType.Loaded) {
          this.draw('bgMapImageStore loaded');
        }
      },
      this.mainElement.id,
    );
    this._updateBoundingBox(this.bbox);
    this.wmAnimate = new WMAnimate(this);
    this.wmTileRenderer = new WMTileRenderer();
    this.initialized = 1;
  }

  rebuildMapDimensions(): void {
    for (let j = 0; j < this.mapdimensions.length; j += 1) {
      this.mapdimensions[j].used = false;
    }

    for (let i = 0; i < this.layers.length; i += 1) {
      for (let j = 0; j < this.layers[i].dimensions.length; j += 1) {
        const dim = this.layers[i].dimensions[j];

        const mapdim = this.getDimension(dim.name);
        if (isDefined(mapdim)) {
          mapdim.used = true;
        } else {
          const newdim = dim.clone();
          newdim.used = true;
          this.mapdimensions.push(newdim);
        }
      }
    }

    for (let j = 0; j < this.mapdimensions.length; j += 1) {
      if (this.mapdimensions[j].used === false) {
        this.mapdimensions.splice(j, 1);
        j -= 1;
      }
    }
    this.callBack.triggerEvent('onmapdimupdate');
  }

  getLayerByServiceAndName(layerService: string, layerName: string): WMLayer {
    for (let j = 0; j < this.layers.length; j += 1) {
      const layer = this.layers[this.layers.length - j - 1];
      if (layer.name === layerName) {
        if (layer.service === layerService) {
          return layer;
        }
      }
    }
    return undefined;
  }

  getLayers(): WMLayer[] {
    /* Provide layers in reverse order */
    const returnlayers = [];
    for (let j = 0; j < this.layers.length; j += 1) {
      const layer = this.layers[this.layers.length - j - 1];
      returnlayers.push(layer);
    }
    return returnlayers;
  }

  setLayer(layer: WMLayer): Promise<unknown> {
    return this.addLayer(layer);
  }

  /* Indicate weather this map component is active or not */
  setActive(active: boolean): void {
    this.mapIsActivated = active;
    this.isMapHeaderEnabled = true;
  }

  setActiveLayer(layer: WMLayer): void {
    for (let j = 0; j < this.layers.length; j += 1) {
      this.layers[j].active = false;
    }
    this.activeLayer = layer;
    this.activeLayer.active = true;

    this.callBack.triggerEvent('onchangeactivelayer');
  }

  // Calculates how many baselayers there are.
  // Useful when changing properties for a divbuffer index (for example setLayerOpacity)
  calculateNumBaseLayers(): void {
    this.numBaseLayers = 0;
    if (this.baseLayers) {
      for (let l = 0; l < this.baseLayers.length; l += 1) {
        if (this.baseLayers[l].enabled) {
          if (this.baseLayers[l].keepOnTop === false) {
            this.numBaseLayers += 1;
          }
        }
      }
    }
  }

  enableLayer(layer: WMLayer): void {
    layer.enabled = true;
    this.calculateNumBaseLayers();
    this.rebuildMapDimensions();
  }

  disableLayer(layer: WMLayer): void {
    layer.enabled = false;
    this.calculateNumBaseLayers();
    this.rebuildMapDimensions();
  }

  toggleLayer(layer: WMLayer): void {
    if (layer.enabled === true) {
      layer.enabled = false;
    } else layer.enabled = true;
    this.calculateNumBaseLayers();
    this.rebuildMapDimensions();
  }

  displayLayer(layer: WMLayer, enabled: boolean): void {
    layer.enabled = enabled;
    this.calculateNumBaseLayers();
    this.rebuildMapDimensions();
  }

  _getLayerIndex(layer: WMLayer): number {
    if (!layer) return undefined;
    for (let j = 0; j < this.layers.length; j += 1) {
      if (this.layers[j] === layer) {
        return j;
      }
    }
    return -1;
  }

  removeAllLayers(): void {
    for (let i = 0; i < this.layers.length; i += 1) {
      this.layers[i].setAutoUpdate(false);
    }
    this.layers.length = 0;
    this.mapdimensions.length = 0;
    this.callBack.triggerEvent('onlayeradd');
  }

  deleteLayer(layerToDelete: WMLayer): void {
    if (this.layers.length <= 0) return;
    layerToDelete.setAutoUpdate(false);
    const layerIndex = this._getLayerIndex(layerToDelete);
    if (layerIndex >= 0) {
      // move everything up with id's higher than this layer
      for (let j = layerIndex; j < this.layers.length - 1; j += 1) {
        this.layers[j] = this.layers[j + 1];
      }
      this.layers.length -= 1;

      this.activeLayer = undefined;
      if (layerIndex >= 0 && layerIndex < this.layers.length) {
        this.rebuildMapDimensions();
        this.setActiveLayer(this.layers[layerIndex]);
      } else if (this.layers.length > 0) {
        this.rebuildMapDimensions();
        this.setActiveLayer(this.layers[this.layers.length - 1]);
      }
    }
    this.callBack.triggerEvent('onlayerchange');
    this.rebuildMapDimensions();
  }

  moveLayerDown(layerToMove: WMLayer): void {
    const layerIndex = this._getLayerIndex(layerToMove);
    if (layerIndex > 0) {
      const layerToMoveDown = this.layers[layerIndex - 1];
      const layer = this.layers[layerIndex];
      if (layerToMoveDown && layer) {
        this.layers[layerIndex] = layerToMoveDown;
        this.layers[layerIndex - 1] = layer;
      }
    } else {
      try {
        debug(
          DebugType.Error,
          `WebMapJS warning: moveLayerDown: layer '${layerToMove.name}' not found.`,
        );
      } catch (e) {
        debug(
          DebugType.Error,
          'WebMapJS warning: moveLayerDown: layer invalid.',
        );
      }
    }
  }

  swapLayers(_layerA: WMLayer, _layerB: WMLayer): void {
    const layerIndexA = this._getLayerIndex(_layerA);
    const layerIndexB = this._getLayerIndex(_layerB);
    if (layerIndexA >= 0 && layerIndexB >= 0) {
      const layerA = this.layers[layerIndexA];
      const layerB = this.layers[layerIndexB];
      if (layerB && layerA) {
        this.layers[layerIndexA] = layerB;
        this.layers[layerIndexB] = layerA;
      }
    } else {
      try {
        debug(
          DebugType.Error,
          `WebMapJS: moveLayers: layer '${_layerA.name}' not found.`,
        );
      } catch (e) {
        debug(DebugType.Error, 'WebMapJS: moveLayers: layer invalid.');
      }
    }
  }

  moveLayerUp(layerToMove: WMLayer): void {
    const layerIndex = this._getLayerIndex(layerToMove);
    if (layerIndex < this.layers.length - 1) {
      const layerToMoveUp = this.layers[layerIndex + 1];
      const layer = this.layers[layerIndex];
      if (layerToMoveUp && layer) {
        this.layers[layerIndex] = layerToMoveUp;
        this.layers[layerIndex + 1] = layer;
      }
    } else {
      try {
        debug(
          DebugType.Error,
          `WebMapJS: moveLayerUp: layer '${layerToMove.name}' not found.`,
        );
      } catch (e) {
        debug(DebugType.Error, 'WebMapJS: moveLayerUp: layer invalid.');
      }
    }
  }

  configureMapDimensions(layer: WMLayer): void {
    layer.dimensions.forEach((dimension): void => {
      const mapDim = this.getDimension(dimension.name);
      if (
        isDefined(mapDim) &&
        isDefined(mapDim.currentValue) &&
        dimension.linked
      ) {
        dimension.setClosestValue(mapDim.currentValue);
      }
    });

    this.rebuildMapDimensions();
  }

  /**
   * @param layer of type WMLayer
   */
  addLayer(layer: WMLayer): Promise<unknown> {
    return new Promise((resolve, reject) => {
      if (!isDefined(layer)) {
        debug(DebugType.Warning, 'addLayer: undefined layer');
        reject(new Error('undefined layer'));
        return;
      }

      if (!layer.constructor) {
        debug(
          DebugType.Warning,
          'addLayer: layer has no constructor, skipping addLayer.',
        );
        reject(new Error('layer has no constructor'));
        return;
      }

      /* Set a reference in the layer to this map */
      layer.parentMap = this;

      this.layers.push(layer);

      const done = (layerCallback): void => {
        this.configureMapDimensions(layerCallback);
        this.callBack.triggerEvent('onlayeradd');
        resolve(this);
      };
      layer.parseLayer(done, undefined, 'WMJSMap addLayer');
    });
  }

  getActiveLayer(): WMLayer {
    return this.activeLayer;
  }

  /**
   * setProjection
   * Set the projection of the current webmap object
   *_srs also accepts a projectionProperty object
   */
  setProjection(
    _srs: string | { srs: string; bbox: WMBBOX },
    _bbox?: WMBBOX,
  ): void {
    if (!_srs) _srs = 'EPSG:4326';
    if (typeof _srs === 'object') {
      _bbox = _srs.bbox;
      _srs = _srs.srs;
    }
    if (!_srs) _srs = 'EPSG:4326';

    if (this.srs !== _srs) {
      this.defaultBBOX.setBBOX(_bbox);
    }

    this.srs = _srs;

    if (this.proj4.srs !== this.srs || !isDefined(this.proj4.projection)) {
      if (this.srs === 'GFI:TIME_ELEVATION') {
        this.proj4.projection = 'EPSG:4326';
      } else {
        this.proj4.projection = this.srs;
      }
      this.proj4.srs = this.srs;
    }
    this.setBBOX(_bbox);
    this.updateMouseCursorCoordinates(undefined);
    this.callBack.triggerEvent('onsetprojection', [this.srs, this.bbox]);
  }

  getBBOX(): WMBBOX {
    return this.updateBBOX;
  }

  getProjection(): { srs: string; bbox: WMBBOX } {
    return { srs: this.srs, bbox: this.resizeBBOX };
  }

  getSize(): { width: number; height: number } {
    return { width: this.width, height: this.height };
  }

  getWidth(): number {
    return this.width;
  }

  getHeight(): number {
    return this.height;
  }

  getMapPin(): MapPin {
    return this.mapPin;
  }

  repositionLegendGraphic(): void {
    if (!this._displayLegendInMap) {
      this.draw();
    }
  }

  setSize(w: number, h: number): void {
    debug(DebugType.Log, 'setSize', w, h);
    if (w < 4 || h < 4) {
      return;
    }
    this.resizeWidth = w;
    this.resizeHeight = h;
    /**
      Enable following line to enable smooth scaling during resize transitions. Is heavier for browser:
      this._setSize((this.resizeWidth) | 0, (this.resizeHeight) | 0);
    */

    if (this._resizeTimerBusy === false) {
      this._resizeTimerBusy = true;
      this._setSize(this.resizeWidth, this.resizeHeight);
      return;
    }
    this._resizeTimer.init(200, () => {
      this._resizeTimerBusy = false;
      this._setSize(this.resizeWidth, this.resizeHeight);
      this.draw('resizeTimer');
    });
  }

  _setSize(w: number, h: number): void {
    if (!w || !h) return;
    if (w < 4 || h < 4) {
      return;
    }

    debug(DebugType.Log, `setSize(${w},${h})`);
    const projinfo = this.getProjection();
    this.width = w;
    this.height = h;
    if (this.width < 4 || isNaN(this.width)) this.width = 4;
    if (this.height < 4 || isNaN(this.height)) this.height = 4;
    if (!projinfo.srs || !projinfo.bbox) {
      debug(
        DebugType.Error,
        'WebMapJS: this.setSize: Setting default projection (EPSG:4326 with (-180,-90,180,90)',
      );
      projinfo.srs = 'EPSG:4326';
      projinfo.bbox.left = -180;
      projinfo.bbox.bottom = -90;
      projinfo.bbox.right = 180;
      projinfo.bbox.top = 90;
      this.setProjection(projinfo.srs, projinfo.bbox);
    }

    Object.assign(this.baseDiv.style, {
      width: this.width,
      height: this.height,
    });

    if (!this.mainElement.style) return;
    this.mainElement.style.width = `${this.width}px`;
    this.mainElement.style.height = `${this.height}px`;
    this.setBBOX(this.resizeBBOX);

    this.showBoundingBox();
    this.divBuffer.resize(
      this.getWidth().toString(),
      this.getHeight().toString(),
    );

    this.repositionLegendGraphic();

    /* Fire the onresize event, to notify listeners that something happened. */
    this.callBack.triggerEvent('onresize', [this.width, this.height]);
  }

  abort(): void {
    this.callBack.triggerEvent('onmapready');
    this.mapBusy = false;
    this.drawPending = false;
    this.layersBusy = false;
    this.callBack.triggerEvent('onloadingcomplete');
  }

  isBusy(): boolean {
    if (this.suspendDrawing === true || this.mapBusy || this.layersBusy === 1) {
      return true;
    }
    if (this.divBuffer[0].ready === false || this.divBuffer[1].ready === false)
      return true;
    return false;
  }

  _makeInfoHTML(): void {
    try {
      // Create the layerinformation table
      let infoHTML = '<table class="myTable">';
      let infoHTMLHasValidContent = false;
      // Make first a header with 'Layer' and the dimensions
      infoHTML += '<tr><th>Layer</th>';
      if (this.mapdimensions) {
        for (let d = 0; d < this.mapdimensions.length; d += 1) {
          infoHTML += `<th>${this.mapdimensions[d].name}</th>`;
        }
      }
      infoHTML += '</tr>';
      infoHTML += '<tr><td>Map</tdh>';
      if (this.mapdimensions) {
        for (let d = 0; d < this.mapdimensions.length; d += 1) {
          infoHTML += `<td>${this.mapdimensions[d].currentValue}</td>`;
        }
      }
      infoHTML += '</tr>';
      let l = 0;
      for (l = 0; l < this.getNumLayers(); l += 1) {
        const j = this.getNumLayers() - 1 - l;
        if (this.layers[j].service && this.layers[j].enabled) {
          const layerDimensionsObject = this.layers[j].dimensions;
          if (layerDimensionsObject) {
            let layerTitle = '';
            layerTitle += this.layers[j].title;
            infoHTML += `<tr><td>${layerTitle}</td>`;
            for (
              let mapdim = 0;
              mapdim < this.mapdimensions.length;
              mapdim += 1
            ) {
              let foundDim = false;
              for (
                let layerdim = 0;
                layerdim < layerDimensionsObject.length;
                layerdim += 1
              ) {
                if (
                  layerDimensionsObject[layerdim].name.toUpperCase() ===
                  this.mapdimensions[mapdim].name.toUpperCase()
                ) {
                  infoHTML += `<td>${layerDimensionsObject[layerdim].currentValue}</td>`;
                  foundDim = true;
                  infoHTMLHasValidContent = true;
                  break;
                }
              }
              if (foundDim === false) infoHTML += '<td>-</td>';
            }
            infoHTML += '</tr>';
          }
        }
      }
      infoHTML += '</table>';
      if (infoHTMLHasValidContent === true) {
        this.divDimInfo.style.display = '';
        this.divDimInfo.innerHTML = infoHTML;
        const cx = 8;
        const cy = 8;
        this.divDimInfo.style.width = `${Math.min(
          this.width - parseInt(this.divDimInfo.style.marginLeft, 10) - 210,
          350,
        )}px`;
        this.divDimInfo.style.left = `${cx}px`;
        this.divDimInfo.style.top = `${cy}px`;
      } else {
        this.divDimInfo.style.display = 'none';
      }
    } catch (e) {
      debug(DebugType.Error, `WebMapJS: Exception${e}`);
    }
  }

  showScaleBar(): void {
    this.showScaleBarInMap = true;
  }

  hideScaleBar(): void {
    this.showScaleBarInMap = false;
  }

  displayScaleBarInMap(display: boolean): void {
    this.showScaleBarInMap = display;
  }

  display(): void {
    if (!this.divBuffer) return;
    this.divBuffer.display();
    this.drawnBBOX.setBBOX(this.bbox);
  }

  _animFrameRedraw(): void {
    this._draw(this._animationList);
  }

  draw(animationList = {}): void {
    if (this.isDestroyed) {
      return;
    }
    if (typeof animationList === 'object') {
      if (animationList !== {}) {
        this._animationList = animationList;
      }
    }
    if (this.isAnimating) {
      debug(DebugType.Log, `ANIMATING: Skipping draw:${animationList}`);
      return;
    }
    this._animFrameRedraw();
  }

  /**
   * API Function called to draw the layers, fires getmap request and shows the layers on the screen
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  _draw(animationList: string | any[]): void {
    debug(DebugType.Log, `draw:${animationList}`);
    this.drawnBBOX.setBBOX(this.bbox);
    this._drawAndLoad(animationList);
  }

  _drawReady(): void {
    this.drawPending = false;
    if (this.needsRedraw) {
      this.needsRedraw = false;
      this.draw(this._animationList);
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  _drawAndLoad(animationList: string | any[]): void {
    if (this.width < 4 || this.height < 4) {
      this._drawReady();
      return;
    }

    this.callBack.triggerEvent('beforedraw');

    if (this.isAnimating === false) {
      if (animationList !== undefined) {
        if (typeof animationList === 'object') {
          if (animationList.length > 0) {
            // Ensure the time dimension has been loaded for the map before starting animation
            const timeDim = this.getDimension('time');
            if (!timeDim) {
              return;
            }
            const selectedTime = timeDim.currentValue;
            this.isAnimating = true;
            this.callBack.triggerEvent('onstartanimation', this);
            this.currentAnimationStep = 0;
            this.animationList = [];
            this.mouseHoverAnimationBox = false;
            for (let j = 0; j < animationList.length; j += 1) {
              if (selectedTime && animationList[j].value === selectedTime) {
                this.currentAnimationStep = j;
              }
              const animationListObject = {
                name: animationList[j].name,
                value: animationList[j].value,
                requests: [],
              };
              this.setDimension(
                animationList[j].name,
                animationList[j].value,
                false,
              );
              animationListObject.requests = this.getWMSRequests();
              this.animationList.push(animationListObject);
            }
            this.setDimension(
              this.animationList[this.currentAnimationStep].name,
              this.animationList[this.currentAnimationStep].value,
              false,
            );
            this.wmAnimate.checkAnimation();
          }
        }
      }
    }
    this._pdraw();
  }

  _onLayersReadyCallbackFunction(): void {
    this.draw('onlayersready callback');
  }

  _onMapReadyCallbackFunction(): void {
    debug(DebugType.Log, '--> onmapready event called');
    this.draw('onmapready callback');
  }

  _onResumeSuspendCallbackFunction(): void {
    this.draw('onresumesuspend callback');
  }

  getWMSRequests(): {
    url: string;
    headers: Headers[];
  }[] {
    const requests = [];
    const n = this.getNumLayers();
    for (let j = 0; j < n; j += 1) {
      if (this.layers[j].service && this.layers[j].enabled) {
        const request = buildWMSGetMapRequest(this, this.layers[j]);
        if (request.url) {
          requests.push(request);
        }
      }
    }
    return requests;
  }

  _pdraw(): void {
    const loadLayers = (): void => {
      debug(DebugType.Log, 'loadLayers');

      this.divBuffer.reset();
      let currentLayerIndex = 0;
      this.numBaseLayers = 0;
      if (this.baseLayers) {
        for (let l = 0; l < this.baseLayers.length; l += 1) {
          if (
            this.baseLayers[l].enabled &&
            this.baseLayers[l].keepOnTop === false &&
            this.baseLayers[l].type &&
            this.baseLayers[l].type !== 'twms'
          ) {
            this.numBaseLayers += 1;
            const requestURL = buildWMSGetMapRequest(
              this,
              this.baseLayers[l],
            ).url;

            if (requestURL) {
              this.divBuffer.setSrc(
                currentLayerIndex,
                requestURL,
                { layer: this.baseLayers[l] },
                this.baseLayers[l].opacity,
                this.getDrawBBOX(),
              );
              currentLayerIndex += 1;
            }
          }
        }
      }
      /* Loop through all layers */
      for (let j = 0; j < this.getNumLayers(); j += 1) {
        if (
          this.layers[j].service &&
          this.layers[j].enabled &&
          this.layers[j].isConfigured
        ) {
          const requestURL = buildWMSGetMapRequest(this, this.layers[j]).url;
          if (requestURL) {
            this.divBuffer.setSrc(
              currentLayerIndex,
              requestURL,
              { layer: this.layers[j] },
              this.layers[j].opacity,
              this.getDrawBBOX(),
            );
            currentLayerIndex += 1;
          }
        }
      }

      if (this.baseLayers) {
        for (let l = 0; l < this.baseLayers.length; l += 1) {
          if (this.baseLayers[l].enabled) {
            if (this.baseLayers[l].keepOnTop === true) {
              const requestURL = buildWMSGetMapRequest(
                this,
                this.baseLayers[l],
              ).url;
              if (requestURL) {
                this.divBuffer.setSrc(
                  currentLayerIndex,
                  requestURL,
                  { layer: this.baseLayers[l] },
                  this.baseLayers[l].opacity,
                  this.getDrawBBOX(),
                );
                currentLayerIndex += 1;
              }
            }
          }
        }
      }
    };

    loadLayers();
    this.divBuffer.display();
    this.callBack.triggerEvent('onmapready', this);
  }

  _updateBoundingBox(_mapbbox: WMBBOX, triggerEvent = true): void {
    if (!this.divBuffer) return;
    let mapbbox = this.bbox;
    if (isDefined(_mapbbox)) mapbbox = _mapbbox;
    this.updateBBOX.setBBOX(mapbbox);
    this.divBuffer.setBBOX(this.updateBBOX, this.drawnBBOX);
    this.drawnBBOX.setBBOX(mapbbox);

    this.showBoundingBox(this.divBoundingBox.bbox, this.updateBBOX);

    this.mapPin.repositionMapPin(_mapbbox);

    if (triggerEvent) {
      this.callBack.triggerEvent('onupdatebbox', this.updateBBOX);
    }
  }

  redrawBuffer(): void {
    this.divBuffer.display();
    debug(DebugType.Log, 'loadedBBOX.setBBOX(bbox)');
    this.drawnBBOX.setBBOX(this.bbox);
    this.draw();
  }

  addBaseLayers(layer: WMLayer): void {
    if (layer) {
      this.numBaseLayers = 0;
      this.baseLayers.push(layer);
      for (let j = 0; j < this.baseLayers.length; j += 1) {
        if (this.baseLayers[j].keepOnTop === true) {
          this.numBaseLayers += 1;
        }
      }
      this.callBack.triggerEvent('onlayeradd');
    } else this.baseLayers = undefined;
  }

  setBaseLayers(layer: WMLayer[]): void {
    if (layer) {
      this.numBaseLayers = 0;
      this.baseLayers = layer;
      for (let j = 0; j < this.baseLayers.length; j += 1) {
        this.baseLayers[j].parentMap = this;

        if (this.baseLayers[j].keepOnTop !== true) {
          this.numBaseLayers += 1;
        }
      }
      this.callBack.triggerEvent('onlayerchange');
    } else this.baseLayers = undefined;
  }

  getBaseLayers(): WMLayer[] {
    return this.baseLayers;
  }

  getNumLayers(): number {
    return this.layers.length;
  }

  getBaseElement(): HTMLElement {
    return this.baseDiv;
  }

  mouseWheelEvent(event: WheelEvent): void {
    preventdefaultEvent(event);
    if (this.mouseWheelBusy === 1) return;
    const { x, y } = this.getMouseCoordinatesForDocument(event);
    this.setMouseCoordinates(x, y);

    const delta = -event.deltaY;
    this.mouseWheelBusy = 1;
    const w = this.updateBBOX.right - this.updateBBOX.left;
    const h = this.updateBBOX.bottom - this.updateBBOX.top;
    const geoMouseXY = this.getGeoCoordFromPixelCoord(
      { x: this.mouseX, y: this.mouseY },
      this.drawnBBOX,
    );
    const nx = (geoMouseXY.x - this.updateBBOX.left) / w; // Normalized to 0-1
    const ny = (geoMouseXY.y - this.updateBBOX.top) / h;

    let zoomW;
    let zoomH;
    if (delta < 0) {
      zoomW = w * -0.25;
      zoomH = h * -0.25;
    } else {
      zoomW = w * 0.2;
      zoomH = h * 0.2;
    }
    let newLeft = this.updateBBOX.left + zoomW;
    let newTop = this.updateBBOX.top + zoomH;
    let newRight = this.updateBBOX.right - zoomW;
    let newBottom = this.updateBBOX.bottom - zoomH;

    const newW = newRight - newLeft;
    const newH = newBottom - newTop;

    const newX = nx * newW + newLeft;
    const newY = ny * newH + newTop;

    const panX = newX - geoMouseXY.x;

    const panY = newY - geoMouseXY.y;
    newLeft -= panX;
    newRight -= panX;
    newTop -= panY;
    newBottom -= panY;
    this.mouseWheelEventBBOXCurrent.copy(this.updateBBOX);
    this.mouseWheelEventBBOXNew.left = newLeft;
    this.mouseWheelEventBBOXNew.bottom = newBottom;
    this.mouseWheelEventBBOXNew.right = newRight;
    this.mouseWheelEventBBOXNew.top = newTop;
    this.mouseWheelBusy = 0;

    if (Math.abs(delta) < 4) {
      this.wMFlyToBBox.flyZoomToBBOXStartZoomThrottled(
        this.mouseWheelEventBBOXCurrent,
        this.mouseWheelEventBBOXNew,
      );
    } else {
      this.wMFlyToBBox.flyZoomToBBOXStartZoom(
        this.mouseWheelEventBBOXCurrent,
        this.mouseWheelEventBBOXNew,
      );
    }
  }

  destroy(): void {
    this.stopAnimating();
    for (let i = this.layers.length - 1; i >= 0; i -= 1) {
      this.layers[i].setAutoUpdate(false);
    }
    this.detachEvents();

    this.callBack.destroy();

    getMapImageStore.removeEventCallback(this.mainElement.id);
    bgMapImageStore.removeEventCallback(this.mainElement.id);
    getLegendImageStore().removeEventCallback(this.mainElement.id);
    WebMapJSMapInstanceIds[this.mainElement.id] = false;
    this.isDestroyed = true;
  }

  detachEvents(): void {
    this.baseDiv.removeEventListener('mousedown', this.mouseDownEvent);
    this.baseDiv.removeEventListener('mouseup', this.mouseUpEvent);
    this.baseDiv.removeEventListener('mousemove', this.mouseMoveEvent);
    this.baseDiv.removeEventListener('contextmenu', this.onContextMenu);
    this.baseDiv.removeEventListener('wheel', this.mouseWheelEvent);
  }

  attachEvents(): void {
    this.baseDiv.addEventListener('mousedown', this.mouseDownEvent);
    this.baseDiv.addEventListener('mouseup', this.mouseUpEvent);
    this.baseDiv.addEventListener('mousemove', this.mouseMoveEvent);
    this.baseDiv.addEventListener('contextmenu', this.onContextMenu);
    this.baseDiv.addEventListener('wheel', this.mouseWheelEvent);

    this.setMapModePan();
  }

  onContextMenu = (): boolean => {
    return false;
  };

  /* Returns all dimensions with its current values as object */
  _buildLayerDims(): void {
    if (this.buildLayerDimsBusy === true) {
      return;
    }
    for (let k = 0; k < this.mapdimensions.length; k += 1) {
      const mapDim = this.mapdimensions[k];
      for (let j = 0; j < this.layers.length; j += 1) {
        for (let i = 0; i < this.layers[j].dimensions.length; i += 1) {
          const layerDim = this.layers[j].dimensions[i];
          if (layerDim.linked === true) {
            if (layerDim.name === mapDim.name) {
              if (
                mapDim.currentValue === 'current' ||
                mapDim.currentValue === 'default' ||
                mapDim.currentValue === '' ||
                mapDim.currentValue === 'earliest' ||
                mapDim.currentValue === 'middle' ||
                mapDim.currentValue === 'latest'
              ) {
                mapDim.currentValue = layerDim.getClosestValue(
                  mapDim.currentValue,
                );
              }
              this.buildLayerDimsBusy = true;
              layerDim.setClosestValue(mapDim.currentValue);
              this.buildLayerDimsBusy = false;
            }
          }
        }
      }
    }
  }

  getMapMode(): string {
    return this.mapMode;
  }

  // Makes a valid getfeatureinfoURL for each layer
  getWMSGetFeatureInfoRequestURL(
    layer: WMLayer,
    x: string,
    y: string,
    format = 'text/html',
  ): string {
    let request = WMJScheckURL(layer.service);
    request += `&SERVICE=WMS&REQUEST=GetFeatureInfo&VERSION=${layer.version}`;

    request += `&LAYERS=${URLEncode(layer.name)}`;

    const baseLayers = layer.name.split(',');
    request += `&QUERY_LAYERS=${URLEncode(baseLayers[baseLayers.length - 1])}`;
    request += `&${getBBOXandProjString(this, layer)}`;
    request += `WIDTH=${this.width}`;
    request += `&HEIGHT=${this.height}`;
    if (
      layer.version === WMSVersion.version100 ||
      layer.version === WMSVersion.version111
    ) {
      request += `&X=${x}`;
      request += `&Y=${y}`;
    }
    if (layer.version === WMSVersion.version130) {
      request += `&I=${x}`;
      request += `&J=${y}`;
    }
    request += '&FORMAT=image/gif';
    request += `&INFO_FORMAT=${format}`;
    request += '&STYLES=';
    try {
      request += `&${getMapDimURL(layer)}`;
    } catch (e) {
      return undefined;
    }
    debug(DebugType.Log, `<a target="_blank" href="${request}">${request}</a>`);
    return request;
  }

  /* End of GetFeature info handling */
  setMapModeGetInfo(): void {
    this.mapMode = 'info';
    this.baseDiv.style.cursor = 'default';
  }

  setMapModeZoomBoxIn(): void {
    this.mapMode = 'zoom';
    this.baseDiv.style.cursor = 'default';
  }

  setMapModeZoomOut(): void {
    this.mapMode = 'zoomout';
    this.baseDiv.style.cursor = 'default';
  }

  setMapModePan(): void {
    this.mapMode = 'pan';
    this.baseDiv.style.cursor = 'default';
  }

  setMapModePoint(): void {
    this.mapMode = 'point';
    this.baseDiv.style.cursor = 'url(webmapjs/img/aero_pen.cur), default';
  }

  setMapModeNone(): void {
    this.mapMode = 'none';
    this.baseDiv.style.cursor = 'default';
  }

  getMouseCoordinatesForDocument(e: MouseEvent | TouchEvent): {
    x: number;
    y: number;
  } {
    if (isDefined((e as TouchEvent).changedTouches)) {
      return {
        x: (e as TouchEvent).changedTouches[0].screenX,
        y: (e as TouchEvent).changedTouches[0].screenY,
      };
    }

    const parentOffset = this.mainElement.parentElement.getBoundingClientRect();

    let { pageX } = e as MouseEvent;
    let { pageY } = e as MouseEvent;
    if (pageX === undefined) {
      pageX = getMouseXCoordinate(e as MouseEvent);
    }
    if (pageY === undefined) {
      pageY = getMouseYCoordinate(e as MouseEvent);
    }

    const relX = pageX - parentOffset.left;
    const relY = pageY - parentOffset.top;
    return { x: relX, y: relY };
  }

  setMouseCoordinates = (x: number, y: number): void => {
    this.mouseX = x;
    this.mouseY = y;
  };

  getMouseCoordinatesForElement(e: MouseEvent | TouchEvent): {
    x: number;
    y: number;
  } {
    if (isDefined((e as TouchEvent).changedTouches)) {
      return {
        x: (e as TouchEvent).changedTouches[0].screenX,
        y: (e as TouchEvent).changedTouches[0].screenY,
      };
    }
    const parentOffset = this.mainElement.parentElement.getBoundingClientRect();

    let { pageX } = e as MouseEvent;
    let { pageY } = e as MouseEvent;
    if (pageX === undefined) {
      pageX = getMouseXCoordinate(e as MouseEvent);
    }
    if (pageY === undefined) {
      pageY = getMouseYCoordinate(e as MouseEvent);
    }
    const relX = pageX - parentOffset.left;
    const relY = pageY - parentOffset.top;
    return { x: relX, y: relY };
  }

  mouseDown(mouseCoordX: number, mouseCoordY: number, event: MouseEvent): void {
    let shiftKey = false;
    if (event) {
      if (event.shiftKey === true) {
        shiftKey = true;
      }
    }
    this.mouseDownX = mouseCoordX;
    this.mouseDownY = mouseCoordY;
    this.mouseDownPressed = 1;
    if (this.mouseDragging === 0) {
      if (
        this._checkInvalidMouseAction(this.mouseDownX, this.mouseDownY) === 0
      ) {
        const triggerResults = this.callBack.triggerEvent('beforemousedown', {
          mouseX: mouseCoordX,
          mouseY: mouseCoordY,
          mouseDown: true,
          event,
          leftButton: detectLeftButton(event),
          rightButton: detectRightButton(event),
          shiftKey,
        });
        for (let j = 0; j < triggerResults.length; j += 1) {
          if (triggerResults[j] === false) {
            return;
          }
        }
      }
    }
    this.controlsBusy = true;
    if (!shiftKey) {
      if (this.oldMapMode !== undefined) {
        this.mapMode = this.oldMapMode;
        this.oldMapMode = undefined;
      }
    } else {
      if (this.oldMapMode === undefined) this.oldMapMode = this.mapMode;
      this.mapMode = 'zoom';
    }
    this.callBack.triggerEvent('mousedown', {
      map: this,
      x: this.mouseDownX,
      y: this.mouseDownY,
    });

    if (this.mapMode === 'info' || this.mapMode === 'point') {
      const mouseDownLatLon = this.getLatLongFromPixelCoord({
        x: this.mouseDownX,
        y: this.mouseDownY,
      });
      this.callBack.triggerEvent('onsetmappin', {
        map: this,
        lon: mouseDownLatLon.x,
        lat: mouseDownLatLon.y,
      });
      this.mapPin.setMapPin(this.mouseDownX, this.mouseDownY);
    }
  }

  _checkInvalidMouseAction(MX: number, MY: number): -1 | 0 {
    if (MY < 0 || MX < 0 || MX > this.width || MY > this.height) {
      this.InValidMouseAction = 1;
      return -1;
    }
    return 0;
  }

  updateMouseCursorCoordinates(coordinates: { x: number; y: number }): void {
    this.mouseUpdateCoordinates = coordinates;
    this.mouseGeoCoordXY = this.getGeoCoordFromPixelCoord(coordinates);
    this.display();
  }

  mouseDownEvent(e: MouseEvent): void {
    this.previousMouseButtonState = 'down';
    const mouseCoords = this.getMouseCoordinatesForDocument(e);
    if (this.mapHeader.cursorSet && mouseCoords.y < this.mapHeader.height) {
      return;
    }

    this.mouseDown(mouseCoords.x, mouseCoords.y, e);
  }

  mouseMoveEvent(e: MouseEvent): void {
    /* Small state machine to detect mouse changes outside of the base element */
    const currentMouseButtonState = e.buttons === 0 ? 'up' : 'down';
    if (
      this.previousMouseButtonState === 'down' &&
      currentMouseButtonState === 'up'
    ) {
      this.mouseUpEvent(e);
    }

    if (this.previousMouseButtonState !== currentMouseButtonState) {
      this.previousMouseButtonState = currentMouseButtonState;
    }

    const mouseCoords = this.getMouseCoordinatesForDocument(e);
    if (
      this.mouseDownPressed === 0 &&
      mouseCoords.y >= 0 &&
      mouseCoords.y < this.mapHeader.height &&
      mouseCoords.x >= 0 &&
      mouseCoords.x <= this.width
    ) {
      if (this.mapHeader.cursorSet === false) {
        this.mapHeader.cursorSet = true;
        this.mapHeader.prevCursor = this.currentCursor;
        this.mapHeader.hovering = true;
        this.setCursor('pointer');
        this.draw('mouseMoveEvent');
      }
    } else if (this.mapHeader.cursorSet === true) {
      this.mapHeader.cursorSet = false;
      this.mapHeader.hovering = false;
      this.setCursor(this.mapHeader.prevCursor);
      this.draw('mouseMoveEvent');
    }
    this.mouseMove(mouseCoords.x, mouseCoords.y, e);
  }

  mouseUpEvent(e: MouseEvent): void {
    preventdefaultEvent(e);
    this.previousMouseButtonState = 'up';
    const mouseCoords = this.getMouseCoordinatesForDocument(e);
    this.mouseUp(mouseCoords.x, mouseCoords.y, e);
  }

  mouseMove(mouseCoordX: number, mouseCoordY: number, event: MouseEvent): void {
    this.setMouseCoordinates(mouseCoordX, mouseCoordY);
    // TODO: add tests to ensure mouse cursor coordinates are still updated while in drawing mode. See: https://gitlab.com/opengeoweb/opengeoweb/-/issues/1319
    this.updateMouseCursorCoordinates({ x: this.mouseX, y: this.mouseY });

    if (this.mouseDragging === 0) {
      const triggerResults = this.callBack.triggerEvent('beforemousemove', {
        mouseX: this.mouseX,
        mouseY: this.mouseY,
        mouseDown: this.mouseDownPressed === 1,
        leftButton: detectLeftButton(event),
        rightButton: detectRightButton(event),
      });
      for (let j = 0; j < triggerResults.length; j += 1) {
        if (triggerResults[j] === false) {
          return;
        }
      }
    }

    if (this.divBoundingBox.displayed === true && this.mapPanning === 0) {
      let tlpx = this.getPixelCoordFromGeoCoord({
        x: this.divBoundingBox.bbox.left,
        y: this.divBoundingBox.bbox.top,
      });
      let brpx = this.getPixelCoordFromGeoCoord({
        x: this.divBoundingBox.bbox.right,
        y: this.divBoundingBox.bbox.bottom,
      });

      let foundBBOXRib = false;

      if (this.mouseDownPressed === 0) {
        if (this.resizingBBOXEnabled === '')
          this.resizingBBOXCursor = getComputedStyle(this.baseDiv).cursor;
        // Find left rib
        if (
          Math.abs(this.mouseX - tlpx.x) < 6 &&
          this.mouseY > tlpx.y &&
          this.mouseY < brpx.y
        ) {
          foundBBOXRib = true;
          this.baseDiv.style.cursor = 'col-resize';
          this.resizingBBOXEnabled = 'left';
        }
        // Find top rib
        if (
          Math.abs(this.mouseY - tlpx.y) < 6 &&
          this.mouseX > tlpx.x &&
          this.mouseX < brpx.x
        ) {
          foundBBOXRib = true;
          this.baseDiv.style.cursor = 'row-resize';
          this.resizingBBOXEnabled = 'top';
        }
        // Find right rib
        if (
          Math.abs(this.mouseX - brpx.x) < 6 &&
          this.mouseY > tlpx.y &&
          this.mouseY < brpx.y
        ) {
          foundBBOXRib = true;
          this.baseDiv.style.cursor = 'col-resize';
          this.resizingBBOXEnabled = 'right';
        }
        // Find bottom rib
        if (
          Math.abs(this.mouseY - brpx.y) < 6 &&
          this.mouseX > tlpx.x &&
          this.mouseX < brpx.x
        ) {
          foundBBOXRib = true;
          this.baseDiv.style.cursor = 'row-resize';
          this.resizingBBOXEnabled = 'bottom';
        }
        // Find topleft corner
        if (
          Math.abs(this.mouseX - tlpx.x) < 6 &&
          Math.abs(this.mouseY - tlpx.y) < 6
        ) {
          foundBBOXRib = true;
          this.baseDiv.style.cursor = 'nw-resize';
          this.resizingBBOXEnabled = 'topleft';
        }
        // Find topright corner
        if (
          Math.abs(this.mouseX - brpx.x) < 6 &&
          Math.abs(this.mouseY - tlpx.y) < 6
        ) {
          foundBBOXRib = true;
          this.baseDiv.style.cursor = 'ne-resize';
          this.resizingBBOXEnabled = 'topright';
        }
        // Find bottomleft corner
        if (
          Math.abs(this.mouseX - tlpx.x) < 6 &&
          Math.abs(this.mouseY - brpx.y) < 6
        ) {
          foundBBOXRib = true;
          this.baseDiv.style.cursor = 'sw-resize';
          this.resizingBBOXEnabled = 'bottomleft';
        }
        // Find bottomright corner
        if (
          Math.abs(this.mouseX - brpx.x) < 6 &&
          Math.abs(this.mouseY - brpx.y) < 6
        ) {
          foundBBOXRib = true;
          this.baseDiv.style.cursor = 'se-resize';
          this.resizingBBOXEnabled = 'bottomright';
        }
      }

      if (
        foundBBOXRib === true ||
        (this.resizingBBOXEnabled !== '' && this.mouseDownPressed === 1)
      ) {
        if (this.mouseDownPressed === 1) {
          if (this.resizingBBOXEnabled === 'left') tlpx.x = this.mouseX;
          if (this.resizingBBOXEnabled === 'top') tlpx.y = this.mouseY;
          if (this.resizingBBOXEnabled === 'right') brpx.x = this.mouseX;
          if (this.resizingBBOXEnabled === 'bottom') brpx.y = this.mouseY;
          if (this.resizingBBOXEnabled === 'topleft') {
            tlpx.x = this.mouseX;
            tlpx.y = this.mouseY;
          }
          if (this.resizingBBOXEnabled === 'topright') {
            brpx.x = this.mouseX;
            tlpx.y = this.mouseY;
          }
          if (this.resizingBBOXEnabled === 'bottomleft') {
            tlpx.x = this.mouseX;
            brpx.y = this.mouseY;
          }
          if (this.resizingBBOXEnabled === 'bottomright') {
            brpx.x = this.mouseX;
            brpx.y = this.mouseY;
          }

          tlpx = this.getGeoCoordFromPixelCoord(tlpx);
          brpx = this.getGeoCoordFromPixelCoord(brpx);
          this.divBoundingBox.bbox.left = tlpx.x;
          this.divBoundingBox.bbox.top = tlpx.y;
          this.divBoundingBox.bbox.right = brpx.x;
          this.divBoundingBox.bbox.bottom = brpx.y;
          this.showBoundingBox(this.divBoundingBox.bbox);
          const data = { map: this, bbox: this.divBoundingBox.bbox };
          this.callBack.triggerEvent('bboxchanged', data);
        }
        return;
      }
      this.resizingBBOXEnabled = '';
      this.baseDiv.style.cursor = this.resizingBBOXCursor;
    }

    if (this._checkInvalidMouseAction(this.mouseX, this.mouseY) === -1) {
      try {
        this.callBack.triggerEvent('onmousemove', [undefined, undefined]);
        this.updateMouseCursorCoordinates(undefined);
      } catch (e) {
        debug(DebugType.Error, `WebMapJS: ${e}`);
      }
      this.mouseUpX = this.mouseX;
      this.mouseUpY = this.mouseY;
      if (this.mapPanning === 0) return;
      if (this.mouseDownPressed === 1)
        if (this.mapMode === 'zoomout') this.zoomOut();
      this.mouseDownPressed = 0;
      if (this.mouseDragging === 1) {
        this.mouseDragEnd(this.mouseUpX, this.mouseUpY);
      }
      return;
    }

    if (this.mouseDownPressed === 1) {
      if (
        !(
          Math.abs(this.mouseDownX - this.mouseX) < 3 &&
          Math.abs(this.mouseDownY - this.mouseY) < 3
        )
      ) {
        this.mouseDrag(this.mouseX, this.mouseY);
      }
    }
    this.callBack.triggerEvent('onmousemove', [this.mouseX, this.mouseY]);
  }

  mouseUp(mouseCoordX: number, mouseCoordY: number, e: MouseEvent): void {
    this.controlsBusy = false;
    this.mouseUpX = mouseCoordX;
    this.mouseUpY = mouseCoordY;
    if (this.mouseDragging === 0) {
      if (this._checkInvalidMouseAction(this.mouseUpX, this.mouseUpY) === 0) {
        const triggerResults = this.callBack.triggerEvent('beforemouseup', {
          mouseX: mouseCoordX,
          mouseY: mouseCoordY,
          mouseDown: false,
          event: e,
        });
        for (let j = 0; j < triggerResults.length; j += 1) {
          if (triggerResults[j] === false) {
            this.mouseDownPressed = 0;
            return;
          }
        }
      }
    }
    if (this.mouseDownPressed === 1) {
      if (this.mapMode === 'zoomout') {
        this.zoomOut();
      }
      if (this.mouseDragging === 0) {
        if (
          Math.abs(this.mouseDownX - this.mouseUpX) < 3 &&
          Math.abs(this.mouseDownY - this.mouseUpY) < 3
        ) {
          if (isDefined(e)) {
            this.callBack.triggerEvent('mouseclicked', {
              map: this,
              x: this.mouseUpX,
              y: this.mouseUpY,
              shiftKeyPressed: e.shiftKey === true,
            });
          }

          this.mapPin.setMapPin(this.mouseDownX, this.mouseDownY);
          const mouseDownLatLon = this.getLatLongFromPixelCoord({
            x: this.mouseDownX,
            y: this.mouseDownY,
          });
          this.callBack.triggerEvent('onsetmappin', {
            map: this,
            lon: mouseDownLatLon.x,
            lat: mouseDownLatLon.y,
          });
        }
      }
      this.callBack.triggerEvent('mouseup', {
        map: this,
        x: this.mouseUpX,
        y: this.mouseUpY,
      });
    }
    this.mouseDownPressed = 0;
    if (this.mouseDragging === 1) {
      this.mouseDragEnd(this.mouseUpX, this.mouseUpY);
    }
  }

  /* Derived mouse methods */
  _mouseDragStart(x: number, y: number): void {
    if (this.mapMode === 'pan') this._mapPanStart(x, y);
    if (this.mapMode === 'zoom') this._mapZoomStart();
  }

  mouseDrag(x: number, y: number): void {
    if (this.mouseDragging === 0) {
      this._mouseDragStart(x, y);
      this.mouseDragging = 1;
    }
    if (this.mapMode === 'pan') this._mapPan(x, y);
    if (this.mapMode === 'zoom') this._mapZoom();
  }

  mouseDragEnd(x: number, y: number): void {
    if (this.mouseDragging === 0) return;
    this.mouseDragging = 0;
    if (this.mapMode === 'pan') this._mapPanEnd(x, y);
    if (this.mapMode === 'zoom') this._mapZoomEnd();
    this.callBack.triggerEvent('mapdragend', {
      map: this,
      x: this.mouseUpX,
      y: this.mouseUpY,
    });
  }

  /* Map zoom and pan methodss */
  _mapPanStart(x: number, y: number): void {
    this.zoomTo(this.updateBBOX);
    this.wMFlyToBBox.flyZoomToBBOXStop();

    this.baseDiv.style.cursor = 'move';

    for (let j = 0; j < this.gfiDialogList.length; j += 1) {
      this.gfiDialogList[j].origX = this.gfiDialogList[j].x;
      this.gfiDialogList[j].origY = this.gfiDialogList[j].y;
    }
    this.mapPanning = 1;
    debug(DebugType.Log, 'updateBBOX.setBBOX(drawnBBOX)');
    this.bbox.setBBOX(this.drawnBBOX);
    this.updateBBOX.setBBOX(this.drawnBBOX);
    this.mapPanStartGeoCoords = this.getGeoCoordFromPixelCoord(
      { x, y },
      this.bbox,
    );
  }

  _mapPan(x: number, y: number): void {
    if (this.mapPanning === 0) return;

    if (
      this.mouseX < 0 ||
      this.mouseY < 0 ||
      this.mouseX > this.mainElement.clientWidth ||
      this.mouseY > this.mainElement.clientHeight
    ) {
      this._mapPanEnd(x, y);
      return;
    }
    const mapPanGeoCoords = this.getGeoCoordFromPixelCoord(
      { x, y },
      this.updateBBOX,
    );
    const diffX = mapPanGeoCoords.x - this.mapPanStartGeoCoords.x;
    const diffY = mapPanGeoCoords.y - this.mapPanStartGeoCoords.y;

    const newLeft = this.updateBBOX.left - diffX;
    const newBottom = this.updateBBOX.bottom - diffY;
    const newRight = this.updateBBOX.right - diffX;
    const newTop = this.updateBBOX.top - diffY;

    this.updateBBOX.left = newLeft;
    this.updateBBOX.bottom = newBottom;
    this.updateBBOX.right = newRight;
    this.updateBBOX.top = newTop;

    this._updateBoundingBox(this.updateBBOX);
  }

  _mapPanEnd(x: number, y: number): void {
    this.baseDiv.style.cursor = 'default';
    if (this.mapPanning === 0) return;
    this.mapPanning = 0;

    const mapPanGeoCoords = this.getGeoCoordFromPixelCoord(
      { x, y },
      this.drawnBBOX,
    );
    const diffX = mapPanGeoCoords.x - this.mapPanStartGeoCoords.x;
    const diffY = mapPanGeoCoords.y - this.mapPanStartGeoCoords.y;
    this.updateBBOX.left = this.drawnBBOX.left - diffX;
    this.updateBBOX.bottom = this.drawnBBOX.bottom - diffY;
    this.updateBBOX.right = this.drawnBBOX.right - diffX;
    this.updateBBOX.top = this.drawnBBOX.top - diffY;
    this._updateBoundingBox(this.updateBBOX);
    this.zoomTo(this.updateBBOX);
    this.draw('mapPanEnd');
  }

  mapPanPercentage(percentageDiffX: number, percentageDiffY: number): void {
    const { left, bottom, right, top } = this.updateBBOX;

    if (percentageDiffX !== 0) {
      const diffX = percentageDiffX * Math.abs(left - right);

      this.updateBBOX.left = left - diffX;
      this.updateBBOX.right = right - diffX;
    }
    if (percentageDiffY !== 0) {
      const diffY = percentageDiffY * Math.abs(top - bottom);

      this.updateBBOX.bottom = bottom - diffY;
      this.updateBBOX.top = top - diffY;
    }

    this._updateBoundingBox(this.updateBBOX);
    this._mapPanPercentageEnd();
  }

  _mapPanPercentageEnd(): void {
    this.zoomTo(this.updateBBOX);
    this.draw('mapPanEnd');
  }

  _mapZoomStart(): void {
    this.baseDiv.style.cursor = 'crosshair';
    this.mapZooming = 1;
  }

  _mapZoom(): void {
    if (this.mapZooming === 0) return;
    const x = this.mouseX - this.mouseDownX;
    const y = this.mouseY - this.mouseDownY;

    if (x < 0 && y < 0) {
      this.baseDiv.style.cursor = 'not-allowed';
    } else {
      this.baseDiv.style.cursor = 'crosshair';
    }
    let w = x;
    let h = y;
    this.divZoomBox.style.display = '';
    if (w < 0) {
      w = -w;
      this.divZoomBox.style.left = `${this.mouseX}px`;
    } else this.divZoomBox.style.left = `${this.mouseDownX}px`;
    if (h < 0) {
      h = -h;
      this.divZoomBox.style.top = `${this.mouseY}px`;
    } else this.divZoomBox.style.top = `${this.mouseDownY}px`;
    this.divZoomBox.style.width = `${w}px`;
    this.divZoomBox.style.height = `${h}px`;
  }

  _mapZoomEnd(): void {
    const x = this.mouseUpX - this.mouseDownX;
    const y = this.mouseUpY - this.mouseDownY;
    this.baseDiv.style.cursor = 'default';
    if (this.mapZooming === 0) return;
    this.mapZooming = 0;
    this.divZoomBox.style.display = 'none';
    if (x < 0 && y < 0) return;
    const zoomBBOXPixels = new WMBBOX();

    if (x < 0) {
      zoomBBOXPixels.left = this.mouseDownX + x;
      zoomBBOXPixels.right = this.mouseDownX;
    } else {
      zoomBBOXPixels.left = this.mouseDownX;
      zoomBBOXPixels.right = this.mouseDownX + x;
    }
    if (y < 0) {
      zoomBBOXPixels.top = this.mouseDownY + y;
      zoomBBOXPixels.bottom = this.mouseDownY;
    } else {
      zoomBBOXPixels.top = this.mouseDownY;
      zoomBBOXPixels.bottom = this.mouseDownY + y;
    }
    const p1 = this.pixelCoordinatesToXY({
      x: zoomBBOXPixels.left,
      y: zoomBBOXPixels.bottom,
    });
    const p2 = this.pixelCoordinatesToXY({
      x: zoomBBOXPixels.right,
      y: zoomBBOXPixels.top,
    });

    zoomBBOXPixels.left = p1.x;
    zoomBBOXPixels.bottom = p1.y;
    zoomBBOXPixels.right = p2.x;
    zoomBBOXPixels.top = p2.y;
    this.zoomTo(zoomBBOXPixels);
    this.draw('mapZoomEnd');
  }

  setCursor(cursor?: string): void {
    if (cursor) {
      this.currentCursor = cursor;
    } else {
      this.currentCursor = 'default';
    }
    this.baseDiv.style.cursor = this.currentCursor;
  }

  getId(): string {
    return this.makeComponentId('webmapjsinstance');
  }

  zoomTo(_newbbox: WMBBOX): void {
    debug(DebugType.Log, 'zoomTo');
    let setOrigBox = false;

    const newbbox = new WMBBOX(_newbbox);
    // Maintain aspect ratio
    let ratio = 1;
    try {
      ratio =
        (this.resizeBBOX.left - this.resizeBBOX.right) /
        (this.resizeBBOX.bottom - this.resizeBBOX.top);
    } catch (e) {
      setOrigBox = true;
    }
    // Check whether we have had valid bbox values
    if (isNaN(ratio)) {
      setOrigBox = true;
    }
    if (setOrigBox === true) {
      debug(
        DebugType.Error,
        'WebMapJS warning: Invalid bbox: setting ratio to 1',
      );
      ratio = 1;
    }
    if (ratio < 0) ratio = -ratio;

    const screenRatio = this.width / this.height;

    // Is W > H?
    if (ratio > screenRatio) {
      // W is more than H, so calc H
      const centerH = (newbbox.top + newbbox.bottom) / 2;
      const extentH = (newbbox.left - newbbox.right) / 2 / ratio;
      newbbox.bottom = centerH + extentH;
      newbbox.top = centerH - extentH;
    } else {
      // H is more than W, so calc W
      const centerW = (newbbox.right + newbbox.left) / 2;
      const extentW = ((newbbox.bottom - newbbox.top) / 2) * ratio;
      newbbox.left = centerW + extentW;
      newbbox.right = centerW - extentW;
    }

    this.setBBOX(newbbox);
    this._updateBoundingBox(this.bbox);
    this.drawnBBOX.setBBOX(this.bbox);

    const resetMapPinAndDialogs = (): void => {
      for (let j = 0; j < this.gfiDialogList.length; j += 1) {
        const newpos = this.getPixelCoordFromGeoCoord({
          x: this.gfiDialogList[j].geoPosX,
          y: this.gfiDialogList[j].geoPosY,
        });
        if (this.gfiDialogList[j].hasBeenDragged === false) {
          if (this.gfiDialogList[j].moveToMouseCursor === true) {
            this.gfiDialogList[j].setXY(
              this.gfiDialogList[j].origX + newpos.x,
              this.gfiDialogList[j].origY + newpos.y,
            );
          }
        }
      }
    };
    resetMapPinAndDialogs();
  }

  pixelCoordinatesToXY(coordinates: { x: number; y: number }): {
    x: number;
    y: number;
  } {
    return this.getGeoCoordFromPixelCoord(coordinates);
  }

  getGeoCoordFromPixelCoord(
    coordinates: { x: number; y: number },
    _bbox?: WMBBOX,
  ): {
    x: number;
    y: number;
  } {
    let mybbox = this.bbox;
    if (_bbox) mybbox = _bbox;
    if (!isDefined(coordinates)) return undefined;
    try {
      const lon =
        (coordinates.x / this.width) * (mybbox.right - mybbox.left) +
        mybbox.left;
      const lat =
        (coordinates.y / this.height) * (mybbox.bottom - mybbox.top) +
        mybbox.top;
      return { x: lon, y: lat };
    } catch (e) {
      return undefined;
    }
  }

  getProj4(): {
    lonlat: string;
    crs: string;
    proj4;
  } {
    if (!this.srs || this.srs === 'GFI:TIME_ELEVATION') {
      return null;
    }
    if (this.proj4.srs !== this.srs || !isDefined(this.proj4.projection)) {
      this.proj4.projection = this.srs;
      this.proj4.srs = this.srs;
    }
    return { lonlat: this.longlat, crs: this.proj4.projection, proj4 };
  }

  getPixelCoordFromLatLong(coordinates: { x; y }): {
    x;
    y;
  } {
    if (!this.srs || this.srs === 'GFI:TIME_ELEVATION') {
      return coordinates;
    }
    let result;
    try {
      const p: { x: number; y: number } = { x: undefined, y: undefined };
      p.x = parseFloat(coordinates.x);
      p.y = parseFloat(coordinates.y);
      if (this.proj4.srs !== this.srs || !isDefined(this.proj4.projection)) {
        this.proj4.projection = this.srs;
        this.proj4.srs = this.srs;
      }
      result = proj4(this.longlat, this.proj4.projection, [p.x, p.y]);
    } catch (e) {
      debug(
        DebugType.Error,
        `WebMapJS: error in getPixelCoordFromLatLong ${e}`,
      );
      return undefined;
    }
    return this.getPixelCoordFromGeoCoord({ x: result[0], y: result[1] });
  }

  calculateBoundingBoxAndZoom(lat: number, lng: number): void {
    let lengthToBBOX = 500000;
    if (this.srs === 'EPSG:4326' || this.srs === 'EPSG:50001') {
      lengthToBBOX = 5;
    }
    const latlng = this.getPixelCoordFromLatLong({ x: lng, y: lat });
    const geolatlng = this.getGeoCoordFromPixelCoord(latlng);
    const searchZoomBBOX = new WMBBOX();

    /* Making the boundingbox. */
    searchZoomBBOX.left = geolatlng.x - lengthToBBOX;
    searchZoomBBOX.bottom = geolatlng.y - lengthToBBOX;
    searchZoomBBOX.right = geolatlng.x + lengthToBBOX;
    searchZoomBBOX.top = geolatlng.y + lengthToBBOX;

    this.zoomTo(searchZoomBBOX);
    this.mapPin.positionMapPinByLatLon({ x: lng, y: lat });
    this.draw('zoomIn');
  }

  getLatLongFromPixelCoord(coordinates: { x: number; y: number }): {
    x: number;
    y: number;
  } {
    if (!this.srs || this.srs === 'GFI:TIME_ELEVATION') {
      return coordinates;
    }
    try {
      const p: { x: number; y: number } = { x: undefined, y: undefined };
      p.x =
        (coordinates.x / this.width) * (this.bbox.right - this.bbox.left) +
        this.bbox.left;
      p.y =
        (coordinates.y / this.height) * (this.bbox.bottom - this.bbox.top) +
        this.bbox.top;
      if (this.proj4.srs !== this.srs) {
        this.proj4.projection = this.srs;
        this.proj4.srs = this.srs;
      }
      const result = proj4(this.proj4.projection, this.longlat, [p.x, p.y]);
      return { x: result[0], y: result[1] };
    } catch (e) {
      return undefined;
    }
  }

  getPixelCoordFromGeoCoord(
    coordinates: { x: number; y: number },
    _bbox?: WMBBOX,
    _width?: number,
    _height?: number,
  ): {
    x: number;
    y: number;
  } {
    let w = this.width;
    let h = this.height;
    let b = this.updateBBOX;
    if (isDefined(_width)) w = _width;
    if (isDefined(_height)) h = _height;
    if (isDefined(_bbox)) b = _bbox;

    const x = (w * (coordinates.x - b.left)) / (b.right - b.left);
    const y = (h * (coordinates.y - b.top)) / (b.bottom - b.top);
    return { x, y };
  }

  // listeners:
  addListener(name: string, f: (param?) => void, keep = false): boolean {
    return this.callBack.addToCallback(name, f, keep);
  }

  removeListener(name: string, f: (param?) => void): void {
    return this.callBack.removeEvents(name, f);
  }

  getListener(): WMListener {
    return this.callBack;
  }

  suspendEvent(name: string): void {
    this.callBack.suspendEvent(name);
  }

  resumeEvent(name: string): void {
    this.callBack.resumeEvent(name);
  }

  getDimensionList(): WMJSDimension[] {
    return this.mapdimensions;
  }

  getDimension(name: string): WMJSDimension {
    for (let i = 0; i < this.mapdimensions.length; i += 1) {
      if (this.mapdimensions[i].name === name) {
        return this.mapdimensions[i];
      }
    }
    return undefined;
  }

  setDimension(
    name: string,
    value: string,
    triggerEvent = true,
    adjustLayerDims = true,
  ): void {
    debug(DebugType.Log, `WebMapJS::setDimension(${name},${value})`);
    if (!isDefined(name) || !isDefined(value)) {
      debug(
        DebugType.Error,
        'WebMapJS: Unable to set dimension with undefined value or name',
      );
      return;
    }
    let dim = this.getDimension(name);

    if (isDefined(dim) === false) {
      dim = new WMJSDimension({ name, currentValue: value });
      this.mapdimensions.push(dim);
    }

    if (dim.currentValue !== value) {
      dim.currentValue = value;
      if (adjustLayerDims) {
        this._buildLayerDims();
      }
      if (triggerEvent === true) {
        this.callBack.triggerEvent('ondimchange', name);
      }
    }
  }

  zoomToLayer(_layer: WMLayer): void {
    // Tries to zoom to the layers boundingbox corresponding to the current map projection
    // If something fails, the defaultBBOX is used instead.
    let layer = _layer;
    if (!layer) {
      layer = this.activeLayer;
    }
    if (!layer) {
      this.zoomTo(this.defaultBBOX);
      this.draw('zoomTolayer');
      return;
    }
    for (let j = 0; j < layer.projectionProperties.length; j += 1) {
      if (layer.projectionProperties[j].srs === this.srs) {
        const w =
          layer.projectionProperties[j].bbox.right -
          layer.projectionProperties[j].bbox.left;
        const h =
          layer.projectionProperties[j].bbox.top -
          layer.projectionProperties[j].bbox.bottom;
        const newBBOX = layer.projectionProperties[j].bbox.clone();
        newBBOX.left -= w / 100;
        newBBOX.right += w / 100;
        newBBOX.bottom -= h / 100;
        newBBOX.top += h / 100;

        this.zoomTo(newBBOX);
        this.draw('zoomTolayer');
        return;
      }
    }
    debug(
      DebugType.Error,
      `WebMapJS: Unable to find the correct bbox with current map projection ${this.srs} for layer ${layer.title}. Using default bbox instead.`,
    );
    this.zoomTo(this.defaultBBOX);
    this.draw('zoomTolayer');
  }

  setBBOX(
    left: number | WMBBOX,
    bottom?: number,
    right?: number,
    top?: number,
  ): boolean {
    debug(DebugType.Log, 'setBBOX');
    this.bbox.setBBOX(left, bottom, right, top);
    this.resizeBBOX.setBBOX(this.bbox);
    this.wMFlyToBBox.flyZoomToBBOXFly.setBBOX(this.bbox);

    if (this.srs !== 'GFI:TIME_ELEVATION') {
      const divRatio = this.width / this.height;
      const bboxRatio =
        (this.bbox.right - this.bbox.left) / (this.bbox.top - this.bbox.bottom);
      if (bboxRatio > divRatio) {
        const centerH = (this.bbox.top + this.bbox.bottom) / 2;
        const extentH = (this.bbox.left - this.bbox.right) / 2 / divRatio;
        this.bbox.bottom = centerH + extentH;
        this.bbox.top = centerH - extentH;
      } else {
        /* H is more than W, so calc W */
        const centerW = (this.bbox.right + this.bbox.left) / 2;
        const extentW = ((this.bbox.bottom - this.bbox.top) / 2) * divRatio;
        this.bbox.left = centerW + extentW;

        this.bbox.right = centerW - extentW;
      }
    }
    this.updateBBOX.setBBOX(this.bbox);
    this.drawnBBOX.setBBOX(this.bbox);
    this.mapPin.repositionMapPin(this.bbox);

    /* Undo part */
    if (this.DoRedo === 0 && this.DoUndo === 0) {
      if (this.UndoPointer !== 0) {
        for (let j = 0; j <= this.UndoPointer; j += 1)
          this.WMProjectionTempUndo[j] = this.WMProjectionUndo[j];
        for (let j = 0; j <= this.UndoPointer; j += 1)
          this.WMProjectionUndo[j] =
            this.WMProjectionTempUndo[this.UndoPointer - j];
        this.UndoPointer = 0;
      }
      for (let j = this.MaxUndos - 1; j > 0; j -= 1) {
        this.WMProjectionUndo[j].bbox.setBBOX(
          this.WMProjectionUndo[j - 1].bbox,
        );
        this.WMProjectionUndo[j].srs = this.WMProjectionUndo[j - 1].srs;
      }
      this.WMProjectionUndo[0].bbox.setBBOX(this.bbox);
      this.WMProjectionUndo[0].srs = this.srs;
      this.NrOfUndos += 1;
      if (this.NrOfUndos > this.MaxUndos) this.NrOfUndos = this.MaxUndos;
    }
    this.DoRedo = 0;
    this.DoUndo = 0;
    this.callBack.triggerEvent('aftersetbbox', this);
    if (this.bbox.equals(left, bottom, right, top) === true) {
      return false;
    }
    return true;
  }

  zoomOut(): void {
    const a = (this.resizeBBOX.right - this.resizeBBOX.left) / 6;
    this.zoomTo(
      new WMBBOX(
        this.resizeBBOX.left - a,
        this.resizeBBOX.bottom - a,
        this.resizeBBOX.right + a,
        this.resizeBBOX.top + a,
      ),
    );
    this.draw('zoomOut');
  }

  zoomIn(ratio: number): void {
    let a = (this.resizeBBOX.left - this.resizeBBOX.right) / 8;
    if (isDefined(ratio) === false) {
      ratio = 1;
    } else if (ratio === 0) return;
    a *= ratio;
    this.zoomTo(
      new WMBBOX(
        this.resizeBBOX.left - a,
        this.resizeBBOX.bottom - a,
        this.resizeBBOX.right + a,
        this.resizeBBOX.top + a,
      ),
    );
    this.draw('zoomIn');
  }

  displayLegendInMap(_displayLegendInMap: boolean): void {
    this._displayLegendInMap = _displayLegendInMap;
    this.repositionLegendGraphic();
  }

  showBoundingBox(_bbox?: WMBBOX, _mapbbox?: WMBBOX): void {
    if (isDefined(_bbox)) {
      this.divBoundingBox.bbox = _bbox;
      this.divBoundingBox.style.display = '';
      this.divBoundingBox.displayed = true;
    }
    if (this.divBoundingBox.displayed !== true) return;

    let b = this.bbox;
    if (isDefined(_mapbbox)) b = _mapbbox;
    const coord1 = this.getPixelCoordFromGeoCoord(
      { x: this.divBoundingBox.bbox.left, y: this.divBoundingBox.bbox.top },
      b,
    );
    const coord2 = this.getPixelCoordFromGeoCoord(
      { x: this.divBoundingBox.bbox.right, y: this.divBoundingBox.bbox.bottom },
      b,
    );

    this.divBoundingBox.style.left = `${coord1.x - 1}px`;
    this.divBoundingBox.style.top = `${coord1.y - 2}px`;
    this.divBoundingBox.style.width = `${coord2.x - coord1.x}px`;
    this.divBoundingBox.style.height = `${coord2.y - coord1.y - 1}px`;
  }

  hideBoundingBox(): void {
    this.divBoundingBox.style.display = 'none';
    this.divBoundingBox.displayed = false;
  }

  getDrawBBOX(): WMBBOX {
    return this.bbox;
  }
}
