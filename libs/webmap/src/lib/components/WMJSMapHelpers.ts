/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */

import type WMJSMap from './WMJSMap';
import WMLayer from './WMLayer';
import {
  isDefined,
  URLEncode,
  getMapDimURL,
  dateToISO8601,
  WMJScheckURL,
} from './WMJSTools';
import { WMSVersion } from './WMConstants';

export const getBBOXandProjString = (map: WMJSMap, layer: WMLayer): string => {
  let request = '';
  const { srs } = map.getProjection();
  const bbox = map.getDrawBBOX();
  if (
    layer.version === WMSVersion.version100 ||
    layer.version === WMSVersion.version111
  ) {
    request += `SRS=${URLEncode(srs)}&`;
    request += `BBOX=${bbox.left},${bbox.bottom},${bbox.right},${bbox.top}&`;
  }
  if (layer.version === WMSVersion.version130) {
    request += `CRS=${URLEncode(srs)}&`;

    if (srs === 'EPSG:4326' && layer.wms130bboxcompatibilitymode === false) {
      request += `BBOX=${bbox.bottom},${bbox.left},${bbox.top},${bbox.right}&`;
    } else {
      request += `BBOX=${bbox.left},${bbox.bottom},${bbox.right},${bbox.top}&`;
    }
  }
  return request;
};

// Build a valid WMS request for a certain layer
export const buildWMSGetMapRequest = (
  map: WMJSMap,
  layer: WMLayer,
): {
  url: string;
  headers: Headers[];
} => {
  if (!isDefined(layer.name)) return { url: undefined, headers: null };
  const { srs } = map.getProjection();
  const mapBbox = map.getDrawBBOX();
  const { width, height } = map.getSize();

  if (!layer.format) {
    layer.format = 'image/png';
  }
  if (layer.name.length < 1) return { url: undefined, headers: null };
  // GetFeatureInfo timeseries in the mapview
  if (srs === 'GFI:TIME_ELEVATION') {
    const x = 707;
    const y = 557;
    const bbox = '29109.947643979103,6500000,1190890.052356021,7200000';
    const srs = 'EPSG:3857';

    let request = layer.getmapURL;
    request += `&SERVICE=WMS&REQUEST=GetFeatureInfo&VERSION=${layer.version}`;

    request += `&LAYERS=${URLEncode(layer.name)}`;

    const baseLayers = layer.name.split(',');
    request += `&QUERY_LAYERS=${URLEncode(baseLayers[baseLayers.length - 1])}`;
    request += `&BBOX=${bbox}`;
    if (
      layer.version === WMSVersion.version100 ||
      layer.version === WMSVersion.version111
    ) {
      request += `&SRS=${URLEncode(srs)}&`;
    }
    if (layer.version === WMSVersion.version130) {
      request += `&CRS=${URLEncode(srs)}&`;
    }
    request += `WIDTH=${width}`;
    request += `&HEIGHT=${height}`;
    if (
      layer.version === WMSVersion.version100 ||
      layer.version === WMSVersion.version111
    ) {
      request += `&X=${x}`;
      request += `&Y=${y}`;
    }
    if (layer.version === WMSVersion.version130) {
      request += `&I=${x}`;
      request += `&J=${y}`;
    }

    if (layer.sldURL) {
      request += `&SLD=${URLEncode(layer.sldURL)}`;
    }

    request += '&FORMAT=image/gif';
    request += '&INFO_FORMAT=image/png';
    request += '&STYLES=';

    const startDate = dateToISO8601(new Date(mapBbox.left));
    const stopDate = dateToISO8601(new Date(mapBbox.right));

    request += `&time=${startDate}/${stopDate}`;
    request += `&elevation=${mapBbox.bottom}/${mapBbox.top}`;

    return { url: request, headers: layer.headers };
  }

  let request = WMJScheckURL(layer.getmapURL);
  request += '&SERVICE=WMS&';
  request += `VERSION=${layer.version}&`;
  request += 'REQUEST=GetMap&';
  request += `LAYERS=${URLEncode(layer.name)}&`;
  request += `WIDTH=${width}&`;
  request += `HEIGHT=${height}&`;

  request += getBBOXandProjString(map, layer);

  if (layer.sldURL) {
    request += `SLD=${URLEncode(layer.sldURL)}&`;
  }
  request += `STYLES=${URLEncode(layer.currentStyle)}&`;
  request += `FORMAT=${layer.format}&`;
  if (layer.transparent === true) {
    request += 'TRANSPARENT=TRUE&';
  } else {
    request += 'TRANSPARENT=FALSE&';
  }
  // Handle dimensions
  try {
    request += getMapDimURL(layer);
  } catch (e) {
    return { url: null, headers: null };
  }
  // Handle WMS extensions
  request += layer.wmsextensions.url;
  return { url: request, headers: layer.headers };
};
