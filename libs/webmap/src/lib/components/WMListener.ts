/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

// eslint-disable-next-line max-classes-per-file
import { debug, DebugType } from './WMJSTools';

class CallBackFunction {
  public name: string;

  public functionpointer: () => void;

  public finished: number;

  public keepOnCall: boolean;

  constructor() {
    this.name = undefined;
    this.functionpointer = undefined;
    this.finished = 0;
    this.keepOnCall = false;
  }
}

export default class WMListener {
  private _callBacks;

  private _suspendedEvents;

  private _allEventsSuspended;

  constructor() {
    this._callBacks = [];
    this._suspendedEvents = [];
    this.addToCallback = this.addToCallback.bind(this);
    this.removeEvents = this.removeEvents.bind(this);
    this.suspendEvent = this.suspendEvent.bind(this);
    this.resumeEvent = this.resumeEvent.bind(this);
    this.suspendEvents = this.suspendEvents.bind(this);
    this.resumeEvents = this.resumeEvents.bind(this);
    this.triggerEvent = this.triggerEvent.bind(this);
    this.destroy = this.destroy.bind(this);
    this._allEventsSuspended = false;
  }

  /* Add multiple functions which will be called after the event with the same name is triggered */
  public addToCallback(
    name: string,
    functionpointer: (param?) => void,
    keepOnCall = false,
  ): boolean {
    let cbp = -1; /* callbackpointer */
    for (let j = 0; j < this._callBacks.length; j += 1) {
      // A callback list index pointer. if finished==1, then this index may be replaced by a new one.
      if (this._callBacks[j].finished === 1) {
        cbp = j;
        break;
      }
      // If the current callback already exist, we will simply keep it
      if (
        this._callBacks[j].name === name &&
        this._callBacks[j].functionpointer === functionpointer
      ) {
        this._callBacks[j].keepOnCall = keepOnCall;
        return false;
      }
    }
    if (cbp === -1) {
      cbp = this._callBacks.length;
      this._callBacks.push(new CallBackFunction());
    }
    this._callBacks[cbp].name = name;
    this._callBacks[cbp].functionpointer = functionpointer;
    this._callBacks[cbp].finished = 0;
    this._callBacks[cbp].keepOnCall = keepOnCall;
    return true;
  }

  public removeEvents(name: string, f: (param?) => unknown): void {
    for (let j = 0; j < this._callBacks.length; j += 1) {
      if (this._callBacks[j].finished === 0) {
        if (this._callBacks[j].name === name) {
          if (!f) {
            this._callBacks[j].finished = 1;
          } else if (this._callBacks[j].functionpointer === f) {
            this._callBacks[j].finished = 1;
          }
        }
      }
    }
  }

  public destroy(): void {
    this._callBacks.length = 0;
  }

  public suspendEvent(name: string): void {
    this._suspendedEvents[name] = true;
  }

  public resumeEvent(name: string): void {
    this._suspendedEvents[name] = false;
  }

  public suspendEvents(): void {
    this._allEventsSuspended = true;
  }

  public resumeEvents(): void {
    this._allEventsSuspended = false;
  }

  // Trigger an event with a name
  public triggerEvent(name: string, param?: unknown): unknown[] {
    if (
      this._allEventsSuspended === true ||
      this._suspendedEvents[name] === true
    ) {
      return [];
    }
    const returnList = [];
    for (let j = 0; j < this._callBacks.length; j += 1) {
      if (this._callBacks[j].finished === 0) {
        if (this._callBacks[j].name === name) {
          if (this._callBacks[j].keepOnCall === false) {
            this._callBacks[j].finished = 1;
          }
          try {
            returnList.push(this._callBacks[j].functionpointer(param, this));
          } catch (e) {
            debug(DebugType.Error, `Error for event [${name}]: `, param, e);
          }
        }
      }
    }
    return returnList;
  }
}
