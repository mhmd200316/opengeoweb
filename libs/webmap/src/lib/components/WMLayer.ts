/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import type WMJSMap from './WMJSMap';
import I18n from '../utils/I18n/lang.en';
import {
  isDefined,
  toArray,
  WMJScheckURL,
  getUriWithParam,
  debug,
  DebugType,
  addStylesForLayer,
} from './WMJSTools';
import { WMEmptyLayerTitle, WMSVersion } from './WMConstants';
import WMJSDimension from './WMJSDimension';
import WMProjection from './WMProjection';
import WMBBOX from './WMBBOX';
import WMGetServiceFromStore from './WMGetServiceFromStore';
import { Capability, GetCapabilitiesJson } from './WMJSService';
import { Style, Dimension, WMSLayerFromGetCapabilities } from './types';

export enum LayerType {
  mapLayer = 'mapLayer',
  baseLayer = 'baseLayer',
  overLayer = 'overLayer',
}

export interface LayerOptions {
  id: string;
  name?: string;
  layerType: LayerType;
  service: string;
  active?: boolean;
  format?: string;
  getgraphinfoURL?: string;
  style?: string;
  currentStyle?: string;
  sldURL?: string;
  opacity?: number;
  title?: string;
  enabled?: boolean;
  keepOnTop?: boolean;
  transparent?: boolean;
  onReady?: (param) => void;
  type?: string;
  parentMap?: WMJSMap;
  dimensions?: Dimension[];
  headers?: Headers[];
  failure?: (layer: WMLayer, message: string) => void;
  ReactWMJSLayerId?: string;
  onLayerError?: (
    layer: WMLayer,
    error: Error,
    webmapInstance: WMJSMap,
  ) => void;
}

interface WMSLayerDimInfo {
  name: string;
  units: string;
  unitSymbol: string;
  value: string;
  default: string;
  isRefTimeDimension: boolean;
}

/**
 * Helper function to figure out the dimensions from the WMS layer object (From WMS GetCapabilities). Parses both WMS 1.1.1 and WMS 1.3.0.
 * @param layerObjectFromWMS The corresponding layer object from the WMS GetCapabilities.
 * @param layer THe WMLayer to configure
 * @param layerDimNamesToRemove A set of dimensions names which are flagged for removal, this function removes the dimension from the set if found in the layer.
 */
export const addDimsForLayer = (
  layerObjectFromWMS: WMSLayerFromGetCapabilities,
  layer: WMLayer,
  layerDimNamesToRemove: Set<string>,
): void => {
  /* Information from the WMS GetCapabilities document */
  const layerDims = toArray(layerObjectFromWMS.Dimension);
  const layerDimExtents = toArray(layerObjectFromWMS.Extent);

  /* Loop through all the dims from the WMS GetCapabilities document for this layer */
  for (let d = 0; d < layerDims.length; d += 1) {
    /* Obtain layerDim info from the GetCapabilities document and put into a WMSLayerDimInfo object  */
    const dimAttributes = layerDims[d].attr;
    const layerDim: WMSLayerDimInfo = {
      name: dimAttributes.name.toLowerCase(),
      units: dimAttributes.units,
      unitSymbol: dimAttributes.unitSymbol,
      value: layerDims[d].value,
      default: dimAttributes.default,
      isRefTimeDimension: dimAttributes.name.toLowerCase() === 'reference_time',
    };
    /* Add information to WMSLayerDimInfo from Extent elements: WMS 1.1.1 */
    for (let i = 0; i < layerDimExtents.length; i += 1) {
      if (layerDimExtents[i].attr.name.toLowerCase() === layerDim.name) {
        /* Check if a value in the WMS Extent is given: */
        if (layerDimExtents[i].value) {
          layerDim.value = layerDimExtents[i].value.trim();
        }
        /* Check for default in the WMS Extent */
        if (layerDimExtents[i].attr.default) {
          layerDim.default = layerDimExtents[i].attr.default.trim();
        }
      }
    }
    /* Graceful WMS check: Check if value is set, if not base it on the default */
    if (!layerDim.value && layerDim.default) {
      layerDim.value = layerDim.default;
    }
    /* Graceful WMS check: Check if default is set, if not base it on the value */
    if (!layerDim.default && layerDim.value) {
      /* If it is not set, take either the last value in case start/stop/res is defined, otherwise the first */
      const s = layerDim.value.split('/');
      let defaultIndex = 0;
      if (s.length > 1) defaultIndex = 1;
      else if (s.length > 0) defaultIndex = 0;
      layerDim.default = s[defaultIndex];
    }

    /* Flag that this dim should not be removed from the layer */
    layerDimNamesToRemove.delete(layerDim.name);

    /* Now configure the dimensions in the Layer based on the obtained info */
    const wmLayerDimensionIndex = layer.dimensions.findIndex(
      (dim): boolean => dim.name === layerDim.name,
    );

    /* Index === -1: Dimension was not found */
    if (wmLayerDimensionIndex === -1) {
      /* Add a new dimension for this layer */
      const dimension = new WMJSDimension({
        name: layerDim.name,
        units: layerDim.units,
        values: layerDim.value,
        currentValue: layerDim.default,
        defaultValue: layerDim.default,
        linked: !layerDim.isRefTimeDimension,
        unitSymbol: layerDim.unitSymbol,
      });
      /* Check if the dimension should take the value from the map */
      if (layer.parentMap) {
        const mapDim = layer.parentMap.getDimension(layerDim.name);
        if (mapDim && mapDim.linked && isDefined(mapDim.currentValue)) {
          const dimensionCurrentValue = dimension.getClosestValue(
            mapDim.currentValue,
          );
          dimension.setValue(dimensionCurrentValue);
        }
      }
      layer.dimensions.push(dimension);
    } else {
      /* Dimension already exists, keep its current value but update the rest of its properties */
      const dimensionToUpdate = layer.dimensions[wmLayerDimensionIndex];
      const currentValue = dimensionToUpdate.getValue();
      dimensionToUpdate.values = layerDim.value;
      dimensionToUpdate.name = layerDim.name;
      dimensionToUpdate.units = layerDim.units;
      dimensionToUpdate.unitSymbol = layerDim.unitSymbol;
      dimensionToUpdate.reInitializeValues(dimensionToUpdate.values);
      dimensionToUpdate.setClosestValue(currentValue);
    }
  }
};

/**
 * Helper function to configure the WMS Styles from the WMS GetCapabilities document.
 * @param nestedLayerPath Array of WMS Layers from `[Root WMS Layer] => [Parent(s) WMS Layer] => [This WMS layer]`.
 * @param wmLayer The WMLayer object to configure
 */
export const configureStyles = (
  nestedLayerPath: WMSLayerFromGetCapabilities[],
  wmLayer: WMLayer,
): void => {
  /* Now add the previous parent layer objects style info (inherit) and end with the Style info from this layer */
  try {
    for (let o = 0; o < nestedLayerPath.length; o += 1) {
      // eslint-disable-next-line no-param-reassign
      wmLayer.styles.push(...addStylesForLayer(nestedLayerPath[o]));
    }
    // eslint-disable-next-line no-empty
  } catch {}

  /* Set the default style */
  if (wmLayer.styles.length > 0) {
    /* Check if the current style is in the list */
    const styleIndex = wmLayer.styles.findIndex(
      (style) => style.name === wmLayer.currentStyle,
    );

    /* If not found or not set, set the first one */
    if (styleIndex === -1) {
      wmLayer.setStyle(wmLayer.styles[0].name);
    } else {
      /* Otherwise set it */
      wmLayer.setStyle(wmLayer.styles[styleIndex].name);
    }
  }
};

/**
 * Helper function to configure the dimensions for this layer from the WMS GetCapabilities document.
 * @param nestedLayerPath Array of WMS Layers from `[Root WMS Layer] => [Parent(s) WMS Layer] => [This WMS layer]`.
 * @param wmLayer The WMLayer object to configure
 */
export const configureDimensions = (
  nestedLayerPath: WMSLayerFromGetCapabilities[],
  wmLayer: WMLayer,
): void => {
  /*
  WMS 1.1.1:
    <Dimension name="time" units="ISO8601"/>
    <Extent name="time" default="2021-05-14T10:35:00Z" multipleValues="1" nearestValue="0">2021-03-31T09:25:00Z/2021-05-14T10:35:00Z/PT5M</Extent>

  WMS 1.3.0:
    <Dimension name="time" units="ISO8601" default="2021-05-14T07:35:00Z" multipleValues="1" nearestValue="0" current="1">2021-03-31T09:25:00Z/2021-05-14T07:35:00Z/PT5M</Dimension>
  */

  /* Flag dimensions which are currently in use for this layer, it is possible that the update will remove dimensions from this layer; keep track of the current ones */
  const layerDimNamesToRemove = new Set<string>();
  for (let d = 0; d < wmLayer.dimensions.length; d += 1) {
    layerDimNamesToRemove.add(wmLayer.dimensions[d].name);
  }

  /* Now start adding all dimensions starting with the parent layer. Layers more deeply nested will inherit Dimension info */
  for (let o = 0; o < nestedLayerPath.length; o += 1) {
    addDimsForLayer(nestedLayerPath[o], wmLayer, layerDimNamesToRemove);
  }

  /* The list with dimensions to remove is now complete, remove the unneeded dimensions from the layer */
  layerDimNamesToRemove.forEach((dimName: string): void => {
    const dimIndex = wmLayer.dimensions.findIndex(
      (dim) => dim.name === dimName,
    );
    if (dimIndex !== -1) {
      wmLayer.dimensions.splice(dimIndex, 1);
    }
  });

  /* Handle reference time dim */
  const refTimeDimension = wmLayer.getDimension('reference_time');
  if (refTimeDimension) {
    wmLayer.handleReferenceTime('reference_time', refTimeDimension.getValue());
  }

  if (wmLayer.parentMap) {
    wmLayer.parentMap.configureMapDimensions(wmLayer);
  }
};

export default class WMLayer {
  id: string;

  name?: string;

  ReactWMJSLayerId?: string;

  hasError?: boolean;

  public enabled?: boolean;

  lastError?: string;

  legendGraphic?: string;

  title?: string;

  public opacity?: number;

  service: string;

  layerType: LayerType;

  currentStyle?: string;

  linkedInfo: { layer: string; message: string };

  headers: Headers[];

  autoupdate: boolean;

  timer: NodeJS.Timeout;

  getmapURL: string;

  getfeatureinfoURL: string;

  getlegendgraphicURL: string;

  keepOnTop: boolean;

  transparent: boolean;

  legendIsDimensionDependent: boolean;

  wms130bboxcompatibilitymode: boolean;

  version: string;

  path: string;

  type: string;

  abstract: string;

  dimensions: WMJSDimension[];

  projectionProperties: WMProjection[];

  queryable: boolean;

  styles: Style[];

  serviceTitle: string;

  parentMap: WMJSMap;

  sldURL: string;

  active: boolean;

  getgraphinfoURL: string;

  format: string;

  _options: LayerOptions;

  onReady: (param) => void;

  optimalFormat: string;

  wmsextensions: { url: string; colorscalerange: Record<string, unknown>[] };

  isConfigured: boolean;

  init(): void {
    this.autoupdate = false;
    this.timer = undefined;
    this.service = undefined; // URL of the WMS Service
    this.getmapURL = undefined;
    this.getfeatureinfoURL = undefined;
    this.getlegendgraphicURL = undefined;
    this.keepOnTop = false;
    this.transparent = true;
    this.hasError = false;
    this.legendIsDimensionDependent = true;
    this.wms130bboxcompatibilitymode = false;
    this.version = WMSVersion.version111;
    this.path = '';
    this.type = 'wms';
    this.wmsextensions = { url: '', colorscalerange: [] };
    this.name = undefined;
    this.title = WMEmptyLayerTitle;
    this.abstract = undefined;
    this.dimensions = []; // Array of Dimension
    this.legendGraphic = '';
    this.projectionProperties = []; // Array of WMProjections
    this.queryable = false;
    this.enabled = true;
    this.styles = [];
    this.currentStyle = '';
    this.id = '-1';
    this.opacity = 1.0; // Ranges from 0.0-1.0
    this.serviceTitle = 'not defined';
    this.parentMap = null;
    this.sldURL = null;
    this.isConfigured = false;
  }

  constructor(options?: LayerOptions) {
    this.init = this.init.bind(this);
    this.getLayerName = this.getLayerName.bind(this);
    this.toggleAutoUpdate = this.toggleAutoUpdate.bind(this);
    this.setAutoUpdate = this.setAutoUpdate.bind(this);
    this.setOpacity = this.setOpacity.bind(this);
    this.getOpacity = this.getOpacity.bind(this);
    this.remove = this.remove.bind(this);
    this.moveUp = this.moveUp.bind(this);
    this.moveDown = this.moveDown.bind(this);
    this.zoomToLayer = this.zoomToLayer.bind(this);
    this.draw = this.draw.bind(this);
    this.handleReferenceTime = this.handleReferenceTime.bind(this);
    this.setDimension = this.setDimension.bind(this);
    this.parseLayer = this.parseLayer.bind(this);
    this.__parseGetCapForLayer = this.__parseGetCapForLayer.bind(this);
    this.parseLayerPromise = this.parseLayerPromise.bind(this);
    this.cloneLayer = this.cloneLayer.bind(this);
    this.setName = this.setName.bind(this);
    this.getLayerRelative = this.getLayerRelative.bind(this);
    this.autoSelectLayer = this.autoSelectLayer.bind(this);
    this.getNextLayer = this.getNextLayer.bind(this);
    this.getPreviousLayer = this.getPreviousLayer.bind(this);
    this.setStyle = this.setStyle.bind(this);
    this.getStyles = this.getStyles.bind(this);
    this.getStyleObject = this.getStyleObject.bind(this);
    this.getStyle = this.getStyle.bind(this);
    this.getDimension = this.getDimension.bind(this);
    this.getProjection = this.getProjection.bind(this);
    this.getCRS = this.getCRS.bind(this);
    this.getCRSByName = this.getCRSByName.bind(this);
    this.setSLDURL = this.setSLDURL.bind(this);
    this.display = this.display.bind(this);
    this.getDimensions = this.getDimensions.bind(this);
    this.init();
    this._options = options;
    this.sldURL = null;
    this.headers = [];
    if (options) {
      this.service = options.service;
      this.getmapURL = options.service;
      this.getfeatureinfoURL = options.service;
      this.getlegendgraphicURL = options.service;
      if (options.active === true) this.active = true;
      else this.active = false;
      this.name = options.name;
      if (options.getgraphinfoURL)
        this.getgraphinfoURL = options.getgraphinfoURL;
      if (options.style) {
        this.currentStyle = options.style;
      }
      if (options.currentStyle) {
        this.currentStyle = options.currentStyle;
      }
      if (options.sldURL) {
        this.sldURL = options.sldURL;
      }
      if (options.id) {
        this.id = options.id;
      }
      if (options.format) this.format = options.format;
      else this.format = 'image/png';
      if (isDefined(options.opacity)) {
        this.opacity = options.opacity;
      }
      if (options.title) this.title = options.title;
      this.abstract = I18n.not_available_message.text;

      if (options.enabled === false) this.enabled = false;

      if (options.keepOnTop === true) this.keepOnTop = true;

      if (options.transparent === false) {
        this.transparent = false;
      }
      if (options.dimensions && options.dimensions.length) {
        for (let d = 0; d < options.dimensions.length; d += 1) {
          this.dimensions.push(new WMJSDimension(options.dimensions[d]));
        }
      }
      if (isDefined(options.onReady)) {
        this.onReady = options.onReady;
      }
      if (isDefined(options.type)) {
        this.type = options.type;
      }
      if (options.parentMap) {
        this.parentMap = options.parentMap;
      }
      if (options.headers) {
        this.headers = options.headers;
      }
    }
  }

  getLayerName(): string {
    return this.name;
  }

  toggleAutoUpdate(): void {
    this.autoupdate = !this.autoupdate;
    if (this.autoupdate) {
      const numDeltaMS = 60000;
      this.timer = setInterval(() => {
        this.parseLayer(undefined, true, 'WMLayer toggleAutoUpdate');
      }, numDeltaMS);
    } else {
      clearInterval(this.timer);
    }
  }

  setAutoUpdate(val: boolean, interval?: number, callback?: () => void): void {
    if (val !== this.autoupdate) {
      this.autoupdate = val;
      if (!val) {
        clearInterval(this.timer);
      } else {
        this.timer = setInterval(() => {
          this.parseLayer(callback, true, 'WMLayer setAutoUpdate');
        }, interval);
      }
    }
  }

  setOpacity(opacityValue: string): void {
    this.opacity = parseFloat(opacityValue);

    this.parentMap && this.parentMap.redrawBuffer();
  }

  getOpacity(): number {
    return this.opacity;
  }

  remove(): void {
    if (this.parentMap) {
      this.parentMap.deleteLayer(this);
      this.parentMap.draw('WMLayer::remove');
    }
    clearInterval(this.timer);
  }

  moveUp(): void {
    if (this.parentMap) {
      this.parentMap.moveLayerUp(this);
      this.parentMap.draw('WMLayer::moveUp');
    }
  }

  moveDown(): void {
    if (this.parentMap) {
      this.parentMap.moveLayerDown(this);
      this.parentMap.draw('WMLayer::moveDown');
    }
  }

  zoomToLayer(): void {
    if (this.parentMap) {
      this.parentMap.zoomToLayer(this);
    }
  }

  draw(e: string): void {
    if (this.parentMap) {
      this.parentMap.draw(`WMLayer::draw::${e}`);
    }
  }

  handleReferenceTime(
    name: string,
    value: string,
    updateMapDimensions = true,
  ): void {
    if (name === 'reference_time') {
      const timeDim = this.getDimension('time');
      const referenceTimeDim = this.getDimension(name);
      if (timeDim) {
        timeDim.setTimeValuesForReferenceTime(value, referenceTimeDim);
        if (updateMapDimensions) {
          if (this.parentMap) {
            if (this.enabled !== false) {
              this.parentMap.getListener().triggerEvent('ondimchange', 'time');
            }
          }
        }
      }
    }
  }

  getDimensions(): WMJSDimension[] {
    return this.dimensions;
  }

  setDimension(name: string, value: string, updateMapDimensions = true): void {
    if (!isDefined(value)) return;

    const dimIndex = this.dimensions.findIndex((dim) => dim.name === name);
    if (dimIndex < 0) return;

    const dim = this.dimensions[dimIndex];

    dim.setValue(value);

    this.handleReferenceTime(name, value, updateMapDimensions);
    if (updateMapDimensions) {
      if (dim.linked === true) {
        if (this.parentMap) {
          this.parentMap.setDimension(name, dim.getValue());
        }
      }
    }
  }

  __parseGetCapForLayer(
    getcapabilitiesjson: GetCapabilitiesJson,
    layerDoneCallback: (layer: WMLayer) => void,
    fail: (layer: WMLayer, message: string) => void,
  ): void {
    if (!getcapabilitiesjson) {
      this.title = I18n.service_has_error.text;
      this.abstract = I18n.not_available_message.text;
      fail(this, I18n.unable_to_connect_server.text);
      return;
    }

    const wmjsService = WMGetServiceFromStore(this.service);

    // Get the capability object
    let capabilityObject: Capability;
    try {
      capabilityObject = wmjsService.getCapabilityElement(getcapabilitiesjson);
    } catch {
      fail(this, 'No capability element in service');
      return;
    }

    this.version = wmjsService.version;

    // Get the rootLayer
    const rootLayer = capabilityObject.Layer;
    if (!isDefined(rootLayer)) {
      fail(this, 'No Layer element in service');
      return;
    }

    try {
      this.serviceTitle = rootLayer.Title.value;
    } catch (e) {
      this.serviceTitle = 'Unnamed service';
    }

    this.optimalFormat = 'image/png';
    // Get the optimal image format for this layer
    try {
      const serverFormats = capabilityObject.Request.GetMap.Format;
      for (let f = 0; f < serverFormats.length; f += 1) {
        if (serverFormats[f].value.indexOf('24') > 0)
          this.optimalFormat = serverFormats[f].value;
        if (serverFormats[f].value.indexOf('32') > 0)
          this.optimalFormat = serverFormats[f].value;
      }
    } catch (e) {
      debug(
        DebugType.Error,
        'This WMS service has no getmap formats listed: using image/png',
      );
    }

    if (this.name === undefined || this.name.length < 1) {
      this.title = WMEmptyLayerTitle;
      this.abstract = I18n.not_available_message.text;
      layerDoneCallback(this);
      return;
    }

    this.getmapURL = undefined;
    try {
      this.getmapURL =
        capabilityObject.Request.GetMap.DCPType.HTTP.Get.OnlineResource.attr[
          'xlink:href'
        ];
    } catch (e) {
      /* Do nothing */
    }
    if (!isDefined(this.getmapURL)) {
      this.getmapURL = this.service;
      debug(
        DebugType.Error,
        'GetMap OnlineResource is not specified. Using default.',
      );
    }

    this.getfeatureinfoURL = undefined;
    try {
      this.getfeatureinfoURL =
        capabilityObject.Request.GetFeatureInfo.DCPType.HTTP.Get.OnlineResource.attr[
          'xlink:href'
        ];
    } catch (e) {
      /* Do nothing */
    }
    if (!isDefined(this.getfeatureinfoURL)) {
      this.getfeatureinfoURL = this.service;
      debug(
        DebugType.Error,
        'GetFeatureInfo OnlineResource is not specified. Using default.',
      );
    }

    this.getlegendgraphicURL = undefined;
    try {
      this.getlegendgraphicURL =
        capabilityObject.Request.GetLegendGraphic.DCPType.HTTP.Get.OnlineResource.attr[
          'xlink:href'
        ];
    } catch (e) {
      /* Do nothing */
    }

    if (!isDefined(this.getlegendgraphicURL)) {
      this.getlegendgraphicURL = this.service;
    }

    // TODO Should be arranged also for the other services:
    this.getmapURL = WMJScheckURL(this.getmapURL);
    this.getfeatureinfoURL = WMJScheckURL(this.getfeatureinfoURL);
    this.getlegendgraphicURL = WMJScheckURL(this.getlegendgraphicURL);

    this.styles = [];
    this.projectionProperties = [];
    /* Set default to layer name, try to find details in next steps */
    this.title = this.name;
    this.abstract = '';
    this.path = '';

    let foundLayer = 0;
    // Function will be called when the layer with the right name is found in the getcap doc
    const foundLayerFunction = (
      jsonlayer,
      path: string,
      nestedLayerPath,
    ): void => {
      this.title = jsonlayer.Title.value;
      try {
        this.abstract = jsonlayer.Abstract.value;
      } catch (e) {
        this.abstract = I18n.not_available_message.text;
      }
      this.path = path;

      /** ***************** Go through styles **************** */
      configureStyles(nestedLayerPath, this);

      /** ***************** Go through Dimensions **************** */
      configureDimensions(nestedLayerPath, this);

      let gp = toArray(jsonlayer.SRS);

      if (isDefined(jsonlayer.CRS)) {
        gp = toArray(jsonlayer.CRS);
      }

      let tempSRS = [];

      const getgpbbox = (data): void => {
        if (isDefined(data.BoundingBox)) {
          // Fill in SRS and BBOX on basis of BoundingBox attribute
          const gpbbox = toArray(data.BoundingBox);
          for (let j = 0; j < gpbbox.length; j += 1) {
            let srs;
            srs = gpbbox[j].attr.SRS;

            if (isDefined(gpbbox[j].attr.CRS)) {
              srs = gpbbox[j].attr.CRS;
            }
            if (srs) {
              if (srs.length > 0) {
                srs = decodeURIComponent(srs);
              }
            }
            let alreadyAdded = false;
            for (let i = 0; i < this.projectionProperties.length; i += 1) {
              if (srs === this.projectionProperties[i].srs) {
                alreadyAdded = true;
                break;
              }
            }

            if (alreadyAdded === false) {
              const geoProperty = new WMProjection();

              geoProperty.srs = srs;
              let swapBBOX = false;
              if (this.version === WMSVersion.version130) {
                if (
                  geoProperty.srs === 'EPSG:4326' &&
                  this.wms130bboxcompatibilitymode === false
                ) {
                  swapBBOX = true;
                }
              }
              if (swapBBOX === false) {
                geoProperty.bbox.left = parseFloat(gpbbox[j].attr.minx);
                geoProperty.bbox.bottom = parseFloat(gpbbox[j].attr.miny);
                geoProperty.bbox.right = parseFloat(gpbbox[j].attr.maxx);
                geoProperty.bbox.top = parseFloat(gpbbox[j].attr.maxy);
              } else {
                geoProperty.bbox.left = parseFloat(gpbbox[j].attr.miny);
                geoProperty.bbox.bottom = parseFloat(gpbbox[j].attr.minx);
                geoProperty.bbox.right = parseFloat(gpbbox[j].attr.maxy);
                geoProperty.bbox.top = parseFloat(gpbbox[j].attr.maxx);
              }

              this.projectionProperties.push(geoProperty);
              tempSRS.push(geoProperty.srs);
            }
          }
        }
      };

      getgpbbox(jsonlayer);
      getgpbbox(rootLayer);

      // Fill in SRS  on basis of SRS attribute
      for (let j = 0; j < gp.length; j += 1) {
        if (tempSRS.indexOf(gp[j].value) === -1) {
          const geoProperty = new WMProjection();
          debug(
            DebugType.Error,
            `Warning: BoundingBOX missing for SRS ${gp[j].value}`,
          );
          geoProperty.bbox.left = -180;
          geoProperty.bbox.bottom = -90;
          geoProperty.bbox.right = 180;
          geoProperty.bbox.top = 90;
          geoProperty.srs = gp[j].value;
          this.projectionProperties.push(geoProperty);
        }
      }
      tempSRS = [];
      /* Check if layer is queryable */
      this.queryable = false;
      try {
        if (parseInt(jsonlayer.attr.queryable, 10) === 1) this.queryable = true;
        else this.queryable = false;
      } catch (e) {
        debug(
          DebugType.Error,
          `Unable to detect whether this layer is queryable (for layer ${this.title})`,
        );
      }
      foundLayer = 1;
    };

    function recursivelyFindLayer(
      thisLayer,
      JSONLayers,
      path,
      _prevNestedLayerPath,
    ): WMLayer {
      for (let k = 0; k < JSONLayers.length; k += 1) {
        const nestedLayerPath = [];
        for (let i = 0; i < _prevNestedLayerPath.length; i += 1) {
          nestedLayerPath.push(_prevNestedLayerPath[i]);
        }
        nestedLayerPath.push(JSONLayers[k]);

        if (JSONLayers[k].Layer) {
          let pathnew = path;

          try {
            pathnew += `${JSONLayers[k].Title.value}/`;
          } catch (e) {
            /* Do nothing */
          }

          recursivelyFindLayer(
            thisLayer,
            toArray(JSONLayers[k].Layer),
            pathnew,
            nestedLayerPath,
          );
        } else if (JSONLayers[k].Name) {
          if (JSONLayers[k].Name.value === thisLayer.name) {
            foundLayerFunction(JSONLayers[k], path, nestedLayerPath);
            return;
          }
        }
      }
    }
    // Try to recursively find the name in the getcap doc
    const JSONLayers = toArray(rootLayer.Layer);
    const path = '';
    const nestedLayerPath = [rootLayer];
    recursivelyFindLayer(this, JSONLayers, path, nestedLayerPath);

    if (foundLayer === 0) {
      // Layer was not found...
      let message = '';
      if (this.name) {
        message = `Unable to find layer '${this.name}' in service '${this.service}'`;
      } else {
        message = `Unable to find layer '${this.title}' in service '${this.service}'`;
      }
      this.title = '--- layer not found in service ---';
      this.abstract = I18n.not_available_message.text;
      fail(this, message);
      return;
    }
    /* Layer was found */
    if (this.onReady) {
      this.onReady(this);
    }
    this.isConfigured = true;
    layerDoneCallback(this);
  }

  /**
   * Calls success with a configured layer object
   * Calls options.failure with error message.
   * Throws string exceptions when someting goes wrong
   */
  parseLayer(
    _layerDoneCallback: (layer: WMLayer) => void,
    forceReload: boolean,
    // eslint-disable-next-line no-unused-vars
    origin: string,
  ): void {
    this.hasError = false;
    const layerDoneCallback = (__layer): void => {
      if (isDefined(_layerDoneCallback)) {
        try {
          /* Enable these two console timings to do performance measurments of parseLayer */
          // console.time(origin);
          _layerDoneCallback(__layer);
          // console.timeEnd(origin);
        } catch (e) {
          debug(DebugType.Error, e);
        }
      }
    };
    const fail = (_layer: WMLayer, message: string): void => {
      this.hasError = true;
      this.lastError = message;
      this.title = I18n.service_has_error.text;
      layerDoneCallback(_layer);
      if (isDefined(this._options.failure)) {
        this._options.failure(_layer, message);
      }
    };

    const callback = (data: GetCapabilitiesJson): void => {
      this.__parseGetCapForLayer(data, layerDoneCallback, fail);
    };

    const requestfail = (): void => {
      fail(this, I18n.no_capability_element_found.text);
    };

    const options = { headers: {} };
    if (this.headers && this.headers.length > 0) {
      options.headers = this.headers;
    }

    const wmjsService = WMGetServiceFromStore(this.service);
    if (wmjsService.service !== undefined) {
      wmjsService.getCapabilities(
        (data) => {
          callback(data);
        },
        requestfail,
        forceReload,
        options,
      );
    }
  }

  /**
   * A Promise to parse the layer, it will fetch the WMS GetCapabilities document, configure the layer and resolve to a WMLayer object
   * @param forceReload Do not use the cache of the WMS GetCapabilties document, but instead fetch new data from the server
   * @returns WMLayer object
   */
  parseLayerPromise(forceReload = false): Promise<WMLayer> {
    return new Promise((resolve, reject) => {
      this.parseLayer(
        (layer: WMLayer): void => {
          if (layer.hasError === true) {
            debug(
              DebugType.Error,
              `[ERROR] Parse layer failed, Reason [${layer.lastError}]`,
            );
            reject(layer);
          } else {
            resolve(layer);
          }
        },
        forceReload,
        'WMLayer parseLayerPromise',
      );
    });
  }

  cloneLayer(): WMLayer {
    return new WMLayer(this);
  }

  /**
   * Set or change the name of this layer. This involves re-parsing the WMS GetCapabilities document and updating the affected layer properties. When done, it resolves the WMLayer
   * @param name The name of the layer
   * @returns Promise resolving to a WMLayer object
   */
  setName(name: string): Promise<WMLayer> {
    this.name = name;
    return this.parseLayerPromise(false);
  }

  getLayerRelative(
    success: (layer: WMLayer, index: number, length: number) => void,
    failure: (param: string) => void,
    prevNext: number,
  ): void {
    let localPrevNext = prevNext;
    if (!isDefined(prevNext)) {
      localPrevNext = 0;
    }
    const getLayerObjectsFinished = (layerObjects: WMLayer[]): void => {
      let currentLayerIndex = -1;
      for (let j = 0; j < layerObjects.length; j += 1) {
        if (layerObjects[j].name === this.name) {
          currentLayerIndex = j;
          break;
        }
      }
      if (currentLayerIndex === -1) {
        failure(`Current layer [${this.name}] not in this service`);
        return;
      }

      if (localPrevNext === -1) currentLayerIndex -= 1;
      if (localPrevNext === 1) currentLayerIndex += 1;
      if (currentLayerIndex > layerObjects.length - 1) currentLayerIndex = 0;
      if (currentLayerIndex < 0) currentLayerIndex = layerObjects.length - 1;
      success(
        layerObjects[currentLayerIndex],
        currentLayerIndex,
        layerObjects.length,
      );
    };
    const wmjsService = WMGetServiceFromStore(this.service);
    wmjsService.getLayerObjectsFlat(
      getLayerObjectsFinished,
      failure,
      undefined,
    );
  }

  autoSelectLayer(
    success: (layer: WMLayer) => void,
    failure: (param: string) => void,
  ): void {
    const getLayerObjectsFinished = (layerObjects: WMLayer[]): void => {
      for (let j = 0; j < layerObjects.length; j += 1) {
        if (isDefined(layerObjects[j].name)) {
          if (layerObjects[j].name.indexOf('baselayer') === -1) {
            if (layerObjects[j].path.indexOf('baselayer') === -1) {
              success(layerObjects[j]);
              return;
            }
          }
        }
      }
    };
    const wmjsService = WMGetServiceFromStore(this.service);
    wmjsService.getLayerObjectsFlat(
      getLayerObjectsFinished,
      failure,
      undefined,
    );
  }

  getNextLayer(
    success: (layer: WMLayer, index: number, length: number) => void,
    failure: (param: string) => void,
  ): void {
    this.getLayerRelative(success, failure, 1);
  }

  getPreviousLayer(
    success: (layer: WMLayer, index: number, length: number) => void,
    failure: (param: string) => void,
  ): void {
    this.getLayerRelative(success, failure, -1);
  }

  /**
   * Sets the style by its name
   * @param style: The name of the style (not the object)
   */
  setStyle(styleName: string): void {
    debug(DebugType.Log, `WMLayer::setStyle: ${styleName}`);

    if (!this.styles || this.styles.length === 0) {
      this.currentStyle = '';
      this.legendGraphic = '';
      debug(DebugType.Log, 'Layer has no styles.');
      return;
    }

    let styleFound = false;

    for (let j = 0; j < this.styles.length; j += 1) {
      if (this.styles[j].name === styleName) {
        this.legendGraphic = this.styles[j].legendURL;
        this.currentStyle = this.styles[j].name;
        styleFound = true;
      }
    }
    if (!styleFound) {
      debug(
        DebugType.Log,
        `WMLayer::setStyle: Style ${styleName} not found, setting style ${this.styles[0].name}`,
      );
      this.currentStyle = this.styles[0].name;
      this.legendGraphic = this.styles[0].legendURL;
    }

    /* Check if this legenURL has already a Layer Property set. If so set the Layer to the name of this layer */
    this.legendGraphic = getUriWithParam(this.legendGraphic, {
      layer: this.name,
    });
  }

  getStyles(): Style[] {
    if (this.styles) {
      return this.styles;
    }
    return [];
  }

  /**
   * Get the styleobject by name
   * @param styleName The name of the style
   * @param nextPrev, can be -1 or +1 to get the next or previous style object in circular manner.
   */
  getStyleObject(styleName: string, nextPrev: number): Style {
    if (isDefined(this.styles) === false) {
      return undefined;
    }
    for (let j = 0; j < this.styles.length; j += 1) {
      if (this.styles[j].name === styleName) {
        if (nextPrev === -1) j -= 1;
        if (nextPrev === 1) j += 1;
        if (j < 0) j = this.styles.length - 1;
        if (j > this.styles.length - 1) j = 0;
        return this.styles[j];
      }
    }
    return undefined;
  }

  /*
   *Get the current stylename as used in the getmap request
   */
  getStyle(): string {
    return this.currentStyle;
  }

  getDimension(name: string): WMJSDimension {
    const dimIndex = this.dimensions.findIndex((dim) => dim.name === name);
    if (dimIndex < 0) return undefined;
    return this.dimensions[dimIndex];
  }

  /**
   * Returns the Coordinate Reference Systems for this layer
   * @returns List of WMProjection objects
   */
  getCRS(): WMProjection[] {
    const crs: WMProjection[] = [];
    for (let j = 0; j < this.projectionProperties.length; j += 1) {
      const projectionProperty = this.projectionProperties[j];
      if (projectionProperty) {
        const { bbox } = projectionProperty;
        if (bbox) {
          crs.push({
            srs: `${projectionProperty.srs}`,
            bbox: new WMBBOX(bbox.left, bbox.bottom, bbox.right, bbox.top),
          });
        }
      }
    }
    return crs;
  }

  /**
   * Get Coordinate reference system by name
   *
   * @param srsName Name of the coordinate reference system to get
   * @returns A WMProjection object if found, otherwise undefined.
   */
  getCRSByName(srsName: string): WMProjection {
    for (let j = 0; j < this.projectionProperties.length; j += 1) {
      if (this.projectionProperties[j].srs === srsName) {
        const returnSRS = {
          srs: '',
          bbox: new WMBBOX(undefined, undefined, undefined, undefined),
        };
        returnSRS.srs = `${this.projectionProperties[j].srs}`;
        returnSRS.bbox = new WMBBOX(
          this.projectionProperties[j].bbox.left,
          this.projectionProperties[j].bbox.bottom,
          this.projectionProperties[j].bbox.right,
          this.projectionProperties[j].bbox.top,
        );
        return returnSRS;
      }
    }
    return undefined;
  }

  /**
   * Get Coordinate reference system by name. Note 2021-05-17: It is more inline with other function names to use the getCRSByName function instead.
   *
   * @param srsName Name of the coordinate reference system to get
   * @returns A WMProjection object if found, otherwise undefined.
   */
  getProjection(srsName: string): WMProjection {
    return this.getCRSByName(srsName);
  }

  setSLDURL(url: string): void {
    this.sldURL = url;
  }

  display(displayornot: boolean): void {
    this.enabled = displayornot;
    if (this.parentMap) {
      this.parentMap.displayLayer(this, this.enabled);
    }
  }
}
