/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  WMDateOutSideRange,
  WMDateTooEarlyString,
  WMDateTooLateString,
} from '.';
import WMJSDimension from './WMJSDimension';

describe('components/WMJSDimension', () => {
  it('should accept empty config passed in', () => {
    const dim = new WMJSDimension({});
    expect(dim.name).toBe(undefined);
    expect(dim.units).toBe(undefined);
    expect(dim.linked).toBe(true);
  });

  describe('generateAllValues', () => {
    it('should generate all values from passed in values', () => {
      const config = {
        values: '1,2,3,10,12,22',
      };
      const dim = new WMJSDimension(config);
      expect(dim.generateAllValues()).toStrictEqual([
        '1',
        '2',
        '3',
        '10',
        '12',
        '22',
      ]);
    });
  });

  describe('setTimeValuesForReferenceTime', () => {
    it('should reset timevalues for reference time for type timestartstopres', () => {
      const config = {
        name: 'time',
        values: '2009-01-01T10:00:00Z/2009-01-01T16:00:00Z/PT1H',
        currentValue: '2009-01-01T12:00:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      const refConfig = {
        name: 'reference_time',
        currentValue: '2009-01-01T12:00:00Z',
        values:
          '2009-01-01T10:00:00Z,2009-01-01T11:00:00Z,2009-01-01T12:00:00Z,2009-01-01T13:00:00Z',
        units: 'ISO8601',
      };
      const refDim = new WMJSDimension(refConfig);
      dim.setTimeValuesForReferenceTime('2009-01-01T10:00:00Z', refDim);
      expect(dim.generateAllValues()).toStrictEqual([
        '2009-01-01T10:00:00Z',
        '2009-01-01T11:00:00Z',
        '2009-01-01T12:00:00Z',
        '2009-01-01T13:00:00Z',
      ]);
    });
    it('should reset timevalues for reference time for type timevalues', () => {
      const config = {
        name: 'time',
        values:
          '2009-01-01T10:00:00Z,2009-01-01T11:00:00Z,2009-01-01T12:00:00Z,2009-01-01T13:00:00Z,2009-01-01T14:00:00Z,2009-01-01T15:00:00Z,2009-01-01T16:00:00Z',
        currentValue: '2009-01-01T12:00:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      const refConfig = {
        name: 'reference_time',
        currentValue: '2009-01-01T12:00:00Z',
        values:
          '2009-01-01T10:00:00Z,2009-01-01T11:00:00Z,2009-01-01T12:00:00Z,2009-01-01T13:00:00Z',
        units: 'ISO8601',
      };
      const refDim = new WMJSDimension(refConfig);
      dim.setTimeValuesForReferenceTime('2009-01-01T13:00:00Z', refDim);
      expect(dim.generateAllValues()).toStrictEqual([
        '2009-01-01T13:00:00Z',
        '2009-01-01T14:00:00Z',
        '2009-01-01T15:00:00Z',
        '2009-01-01T16:00:00Z',
      ]);
    });
  });

  describe('initialize', () => {
    it('should initialise with config passed in for type anyvalue, name should be converted to lowercase', () => {
      const config = {
        name: 'someDim',
        units: 'someDimUnits',
        values: '1,2,3,10,12,22',
      };
      const dim = new WMJSDimension(config);
      dim.initialize(false);
      expect(dim.name).toBe(config.name.toLowerCase());
      expect(dim.units).toBe(config.units);
      expect(dim.linked).toBe(false);
    });
    it('should initialise with config passed in for type anyvalue and calculate any intermediate steps', () => {
      const config = {
        name: 'someDim',
        units: 'someDimUnits',
        values: '1,2/8/2,9,10',
      };
      const dim = new WMJSDimension(config);
      dim.initialize(false);
      expect(dim.generateAllValues()).toStrictEqual([
        '1',
        '2',
        '4',
        '6',
        '8',
        '9',
        '10',
      ]);
    });
    it('should initialise with config passed in for timestartstopres type', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T11:00:00Z/2009-01-01T23:00:00Z/PT1H',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.initialize(false);
      expect(dim.name).toBe(config.name.toLowerCase());
      expect(dim.units).toBe(config.units);
      // If no default value passed in - it should take the first value
      expect(dim.defaultValue).toBe('2009-01-01T11:00:00Z');
      // If no currentValue value passed in - it should take the first value
      expect(dim.currentValue).toBe('2009-01-01T11:00:00Z');
    });
    it('should initialise with config passed in for timevalues type', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z',
        currentValue: '2021-03-22T12:44:40Z',
        defaultValue: '2021-03-22T12:54:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.initialize(false);
      expect(dim.name).toBe(config.name.toLowerCase());
      expect(dim.units).toBe(config.units);
      // If default value passed in as part of config - it should set that
      expect(dim.defaultValue).toBe(config.defaultValue);
      // If currentValue value passed in as part of config - it should set that
      expect(dim.currentValue).toBe(config.currentValue);
    });
    it('should initialise with current value = last value if config passed is too late for timevalues type', () => {
      const config = {
        name: 'somedim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z',
        currentValue: '2021-03-22T14:44:40Z',
        defaultValue: '2021-03-22T12:54:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.initialize(false);
      expect(dim.name).toBe(config.name);
      expect(dim.units).toBe(config.units);
      // If currentValue value passed in as part of config too late it should set last value
      expect(dim.currentValue).toBe('2021-03-22T13:04:40Z');
    });
    it('should initialise with current value = first value if config passed is too early for timestartstopres type', () => {
      const config = {
        name: 'somedim',
        values: '2009-01-01T11:00:00Z/2009-01-01T23:00:00Z/PT1H',
        currentValue: '2000-01-01T11:00:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.initialize(false);
      expect(dim.name).toBe(config.name);
      expect(dim.units).toBe(config.units);
      // If currentValue value passed in as part of config too early it should set first value
      expect(dim.currentValue).toBe('2009-01-01T11:00:00Z');
    });
    it('should initialise with config passed in for multiple timestartstopres types separated with comma', () => {
      const config = {
        name: 'somedim',
        values:
          '2021-03-12T03:00:00Z,2021-03-12T06:00:00Z/2021-03-17T00:00:00Z/PT3H,2021-03-17T06:00:00Z/2021-03-21T00:00:00Z/PT6H',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.initialize(false);
      expect(dim.name).toBe(config.name);
      expect(dim.units).toBe(config.units);
      // If no default value passed in - it should take the first value
      expect(dim.defaultValue).toBe('2021-03-12T03:00:00Z');
      // If no currentValue value passed in - it should take the first value
      expect(dim.currentValue).toBe('2021-03-12T03:00:00Z');

      // 56 values should be found
      expect(dim.size()).toBe(56);

      // Check all values
      expect(dim.generateAllValues()).toStrictEqual([
        '2021-03-12T03:00:00Z',
        '2021-03-12T06:00:00Z',
        '2021-03-12T09:00:00Z',
        '2021-03-12T12:00:00Z',
        '2021-03-12T15:00:00Z',
        '2021-03-12T18:00:00Z',
        '2021-03-12T21:00:00Z',
        '2021-03-13T00:00:00Z',
        '2021-03-13T03:00:00Z',
        '2021-03-13T06:00:00Z',
        '2021-03-13T09:00:00Z',
        '2021-03-13T12:00:00Z',
        '2021-03-13T15:00:00Z',
        '2021-03-13T18:00:00Z',
        '2021-03-13T21:00:00Z',
        '2021-03-14T00:00:00Z',
        '2021-03-14T03:00:00Z',
        '2021-03-14T06:00:00Z',
        '2021-03-14T09:00:00Z',
        '2021-03-14T12:00:00Z',
        '2021-03-14T15:00:00Z',
        '2021-03-14T18:00:00Z',
        '2021-03-14T21:00:00Z',
        '2021-03-15T00:00:00Z',
        '2021-03-15T03:00:00Z',
        '2021-03-15T06:00:00Z',
        '2021-03-15T09:00:00Z',
        '2021-03-15T12:00:00Z',
        '2021-03-15T15:00:00Z',
        '2021-03-15T18:00:00Z',
        '2021-03-15T21:00:00Z',
        '2021-03-16T00:00:00Z',
        '2021-03-16T03:00:00Z',
        '2021-03-16T06:00:00Z',
        '2021-03-16T09:00:00Z',
        '2021-03-16T12:00:00Z',
        '2021-03-16T15:00:00Z',
        '2021-03-16T18:00:00Z',
        '2021-03-16T21:00:00Z',
        '2021-03-17T00:00:00Z',
        '2021-03-17T06:00:00Z',
        '2021-03-17T12:00:00Z',
        '2021-03-17T18:00:00Z',
        '2021-03-18T00:00:00Z',
        '2021-03-18T06:00:00Z',
        '2021-03-18T12:00:00Z',
        '2021-03-18T18:00:00Z',
        '2021-03-19T00:00:00Z',
        '2021-03-19T06:00:00Z',
        '2021-03-19T12:00:00Z',
        '2021-03-19T18:00:00Z',
        '2021-03-20T00:00:00Z',
        '2021-03-20T06:00:00Z',
        '2021-03-20T12:00:00Z',
        '2021-03-20T18:00:00Z',
        '2021-03-21T00:00:00Z',
      ]);
    });
    it('should initialise with config passed in for multiple ranges types separated with comma', () => {
      const config = {
        name: 'somedim',
        values: '0,1,2,3,4/8/2,9,10,11,12/20/5',
        units: 'number',
      };
      const dim = new WMJSDimension(config);
      dim.initialize(false);
      expect(dim.name).toBe(config.name);
      expect(dim.units).toBe(config.units);
      // If no default value passed in - it should take the first value
      expect(dim.defaultValue).toBe('0');
      // If no currentValue value passed in - it should take the first value
      expect(dim.currentValue).toBe('0');

      // 56 values should be found
      expect(dim.size()).toBe(13);

      // Check all values
      const allValues = [];
      for (let j = 0; j < dim.size(); j += 1) {
        allValues.push(dim.getValueForIndex(j));
      }
      expect(allValues).toEqual([
        '0',
        '1',
        '2',
        '3',
        '4',
        '6',
        '8',
        '9',
        '10',
        '11',
        '12',
        '17',
        '22',
      ]);
    });
  });

  describe('getValue', () => {
    it('should return current value of the dimension', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '10',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getValue()).toBe('10');
    });
    it('should return the first value from the values array if no current value passed in', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        defaultValue: '3',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getValue()).toBe('1');
    });
    it('should return the default value if current value is undefined', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        defaultValue: '3',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      dim.setValue(undefined);
      expect(dim.getValue()).toBe('3');
    });
    it('should return time range if timeduration present', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T11:00:00Z/2009-01-01T23:00:00Z/PT1H',
        currentValue: '2009-01-01T12:00:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getValue()).toBe('2009-01-01T12:00:00Z');
      dim.setTimeRangeDuration('PT2H');
      expect(dim.getValue()).toBe('2009-01-01T12:00:00Z/2009-01-01T14:00:00Z');
    });
  });

  describe('setValue', () => {
    it('should set current value', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getValue()).toBe('12');
      dim.setValue('10');
      expect(dim.getValue()).toBe('10');
    });
    it('should set value if outside of range and no forceValue passed in or passed in as true', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getValue()).toBe('12');
      dim.setValue(WMDateOutSideRange);
      expect(dim.getValue()).toBe(WMDateOutSideRange);
      dim.setValue(WMDateTooEarlyString);
      expect(dim.getValue()).toBe(WMDateTooEarlyString);
      dim.setValue(WMDateTooLateString, true);
      expect(dim.getValue()).toBe(WMDateTooLateString);
    });
    it('should not set value if outside of range and forceValue is false', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getValue()).toBe('12');
      dim.setValue(WMDateOutSideRange, false);
      expect(dim.getValue()).toBe('12');
      dim.setValue(WMDateTooEarlyString, false);
      expect(dim.getValue()).toBe('12');
      dim.setValue(WMDateTooLateString, false);
      expect(dim.getValue()).toBe('12');
    });
  });

  describe('setClosestValue', () => {
    it('should find closest value and set that', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z',
        currentValue: '2021-03-22T12:44:40Z',
        defaultValue: '2021-03-22T12:54:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.setClosestValue('2021-03-22T12:56:40Z', false);
      expect(dim.currentValue).toBe('2021-03-22T12:54:40Z');
    });
    it('should find closest value and set that if outside of bounds and evenWhenOutsideRange is true', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z',
        currentValue: '2021-03-22T12:44:40Z',
        defaultValue: '2021-03-22T12:54:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.setClosestValue('2021-03-22T13:10:40Z', true);
      expect(dim.currentValue).toBe('2021-03-22T13:04:40Z');
    });
    it('should not set closest value if outside of bounds and evenWhenOutsideRange is false', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z',
        currentValue: '2021-03-22T12:44:40Z',
        defaultValue: '2021-03-22T12:54:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.setClosestValue('2021-03-22T13:10:40Z', false);
      expect(dim.currentValue).toBe(WMDateTooLateString);
    });
    it('should use current value if no new value passed in', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z',
        currentValue: '2021-03-22T12:50:40Z',
        defaultValue: '2021-03-22T12:54:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.setClosestValue();
      expect(dim.currentValue).toBe('2021-03-22T12:54:40Z');
    });
  });

  describe('setTimeRangeDuration', () => {
    it('should recalculate values for seconds if _type=timestartstopres', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T12:00:00Z/2009-01-01T12:06:00Z/PT60S',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.setTimeRangeDuration('PT120S');
      expect(dim.generateAllValues()).toStrictEqual([
        '2009-01-01T12:00:00Z',
        '2009-01-01T12:02:00Z',
        '2009-01-01T12:04:00Z',
        '2009-01-01T12:06:00Z',
      ]);
    });
    it('should recalculate values for minutes if _type=timestartstopres', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T12:00:00Z/2009-01-01T12:06:00Z/PT1M',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.setTimeRangeDuration('PT2M');
      expect(dim.generateAllValues()).toStrictEqual([
        '2009-01-01T12:00:00Z',
        '2009-01-01T12:02:00Z',
        '2009-01-01T12:04:00Z',
        '2009-01-01T12:06:00Z',
      ]);
    });
    it('should recalculate values for hours if _type=timestartstopres', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T12:00:00Z/2009-01-01T15:00:00Z/PT1H',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      dim.setTimeRangeDuration('PT2H');
      expect(dim.generateAllValues()).toStrictEqual([
        '2009-01-01T12:00:00Z',
        '2009-01-01T14:00:00Z',
        '2009-01-01T16:00:00Z',
      ]);
    });
    it('should recalculate values for days if _type=timestartstopres', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-02T00:00:00Z/2009-01-05T00:00:00Z/P1D',
        currentValue: '2009-01-01T00:00:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.generateAllValues()).toStrictEqual([
        '2009-01-02T00:00:00Z',
        '2009-01-03T00:00:00Z',
        '2009-01-04T00:00:00Z',
        '2009-01-05T00:00:00Z',
      ]);
      dim.setTimeRangeDuration('P2D');
      expect(dim.generateAllValues()).toStrictEqual([
        '2009-01-02T00:00:00Z',
        '2009-01-04T00:00:00Z',
        '2009-01-06T00:00:00Z',
      ]);
    });
    it('should recalculate values for months also if _type=timestartstopres', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T00:00:00Z/2009-05-01T00:00:00Z/P1M',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.generateAllValues()).toStrictEqual([
        '2009-01-01T00:00:00Z',
        '2009-02-01T00:00:00Z',
        '2009-03-01T00:00:00Z',
        '2009-04-01T00:00:00Z',
        '2009-05-01T00:00:00Z',
      ]);
      dim.setTimeRangeDuration('P2M');
      expect(dim.generateAllValues()).toStrictEqual([
        '2009-01-01T00:00:00Z',
        '2009-03-01T00:00:00Z',
        '2009-05-01T00:00:00Z',
      ]);
    });
  });

  describe('getClosestValue', () => {
    it('should return for first value if multiple passed', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z',
        currentValue: '2021-03-22T12:44:40Z',
        defaultValue: '2021-03-22T12:54:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(
        dim.getClosestValue('2021-03-22T12:48:40Z/2021-03-22T12:54:40Z', false),
      ).toBe('2021-03-22T12:44:40Z');
      expect(
        dim.getClosestValue('2021-03-22T12:48:40Z/2021-03-22T12:54:40Z', true),
      ).toBe('2021-03-22T12:44:40Z');
    });
    it('should return default value if "current" or "default" or "" passed', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z',
        currentValue: '2021-03-22T12:44:40Z',
        defaultValue: '2021-03-22T12:54:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getClosestValue('current', false)).toBe(
        '2021-03-22T12:54:40Z',
      );
      expect(dim.getClosestValue('default', false)).toBe(
        '2021-03-22T12:54:40Z',
      );
      expect(dim.getClosestValue('', false)).toBe('2021-03-22T12:54:40Z');
      expect(dim.getClosestValue('current', true)).toBe('2021-03-22T12:54:40Z');
      expect(dim.getClosestValue('default', true)).toBe('2021-03-22T12:54:40Z');
      expect(dim.getClosestValue('', true)).toBe('2021-03-22T12:54:40Z');
    });
    it('should return last value if "latest" passed or too late value with evenWhenOutsideRange for _type = timevalues', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z',
        currentValue: '2021-03-22T12:44:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getClosestValue('latest', false)).toBe('2021-03-22T13:04:40Z');
      expect(dim.getClosestValue('latest', true)).toBe('2021-03-22T13:04:40Z');
      expect(dim.getClosestValue('latest', false)).toBe('2021-03-22T13:04:40Z');
      expect(dim.getClosestValue('2021-03-23T18:54:40Z2', true)).toBe(
        '2021-03-22T13:04:40Z',
      );
      expect(dim.getClosestValue('2021-03-23T18:54:40Z2', false)).toBe(
        WMDateTooLateString,
      );
    });
    it('should return last value if "latest" passed or too late value with evenWhenOutsideRange for _type = timestartstopres', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T12:00:00Z/2009-01-01T12:05:00Z/PT60S',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getClosestValue('latest', false)).toBe('2009-01-01T12:05:00Z');
      expect(dim.getClosestValue('latest', true)).toBe('2009-01-01T12:05:00Z');
      expect(dim.getClosestValue('latest', false)).toBe('2009-01-01T12:05:00Z');
      expect(dim.getClosestValue('2021-03-23T18:54:40Z2', true)).toBe(
        '2009-01-01T12:05:00Z',
      );
      // expect date too late if outside of range and false passed
      expect(dim.getClosestValue('2021-03-23T18:54:40Z2', false)).toBe(
        WMDateTooLateString,
      );
    });
    it('should return last value if "earliest" passed or too early value for _type = timevalues', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z',
        currentValue: '2021-03-22T12:44:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getClosestValue('earliest', false)).toBe(
        '2021-03-22T12:34:40Z',
      );
      expect(dim.getClosestValue('earliest', true)).toBe(
        '2021-03-22T12:34:40Z',
      );
      expect(dim.getClosestValue('earliest', false)).toBe(
        '2021-03-22T12:34:40Z',
      );
      expect(dim.getClosestValue('2021-03-20T18:54:40Z2', true)).toBe(
        '2021-03-22T12:34:40Z',
      );
      // expect date too early if outside of range and false passed
      expect(dim.getClosestValue('2021-03-20T18:54:40Z2', false)).toBe(
        WMDateTooEarlyString,
      );
    });
    it('should return last value if "earliest" or too early value passed for _type = timestartstopres', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T12:00:00Z/2009-01-01T12:05:00Z/PT60S',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getClosestValue('earliest', false)).toBe(
        '2009-01-01T12:00:00Z',
      );
      expect(dim.getClosestValue('earliest', true)).toBe(
        '2009-01-01T12:00:00Z',
      );
      expect(dim.getClosestValue('2001-03-20T18:54:40Z2', true)).toBe(
        '2009-01-01T12:00:00Z',
      );
      // expect date too early if outside of range and false passed
      expect(dim.getClosestValue('2001-03-20T18:54:40Z2', false)).toBe(
        WMDateTooEarlyString,
      );
    });
    it('should return center (or center -1) of array if "middle" passed for _type = timevalues', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z',
        currentValue: '2021-03-22T12:44:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getClosestValue('middle', false)).toBe('2021-03-22T12:44:40Z');
      expect(dim.getClosestValue('middle', true)).toBe('2021-03-22T12:44:40Z');
    });
    it('should return center (or center -1) of array if "middle" passed for _type = timevalues', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z,2021-03-22T13:14:40Z',
        currentValue: '2021-03-22T12:44:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getClosestValue('middle', false)).toBe('2021-03-22T12:54:40Z');
      expect(dim.getClosestValue('middle', true)).toBe('2021-03-22T12:54:40Z');
    });
    it('should return center (or center -1) of array if "middle" passed for _type = timestartstopres', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T12:00:00Z/2009-01-01T12:04:00Z/PT60S',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getClosestValue('middle', false)).toBe('2009-01-01T12:02:00Z');
      expect(dim.getClosestValue('middle', true)).toBe('2009-01-01T12:02:00Z');
    });
  });

  describe('getValueForIndex and get', () => {
    it('should return WMDateTooEarly if index = -1', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getValueForIndex(-1)).toBe(WMDateTooEarlyString);
      expect(dim.get(-1)).toBe(WMDateTooEarlyString);
    });
    it('should return WMDateTooLate if index = -2', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getValueForIndex(-2)).toBe(WMDateTooLateString);
      expect(dim.get(-2)).toBe(WMDateTooLateString);
    });
    it('should return -1 if index < -2', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getValueForIndex(-10)).toBe(-1);
      expect(dim.getValueForIndex(-3)).toBe(-1);
      expect(dim.get(-10)).toBe(-1);
      expect(dim.get(-3)).toBe(-1);
    });
    it('should return value if _type = anyvalue', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getValueForIndex(0)).toBe('1');
      expect(dim.getValueForIndex(4)).toBe('12');
      expect(dim.get(0)).toBe('1');
      expect(dim.get(4)).toBe('12');
    });
    it('should return value if _type = timevalues', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z,2021-03-22T12:54:40Z2',
        currentValue: '2021-03-22T12:44:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);

      expect(dim.getValueForIndex(0)).toBe('2021-03-22T12:34:40Z');
      expect(dim.get(0)).toBe('2021-03-22T12:34:40Z');
      expect(dim.getValueForIndex(2)).toBe('2021-03-22T12:54:40Z');
      expect(dim.get(2)).toBe('2021-03-22T12:54:40Z');
    });
    it('should return value if _type = timestartstopres', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T12:00:00Z/2009-01-01T12:05:00Z/PT60S',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);

      expect(dim.getValueForIndex(0)).toBe('2009-01-01T12:00:00Z');
      expect(dim.get(0)).toBe('2009-01-01T12:00:00Z');
      expect(dim.getValueForIndex(4)).toBe('2009-01-01T12:04:00Z');
      expect(dim.get(4)).toBe('2009-01-01T12:04:00Z');
    });

    it('should return null if _type = timestartstopres and index NaN', () => {
      const config = {
        name: 'someDim',
        values: 'NaN/NaN',
        currentValue: 'outside range',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getValueForIndex(NaN)).toBe(null);
    });
  });

  describe('getFirstValue', () => {
    it('should return first element of values', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getFirstValue()).toBe('1');
    });
  });

  describe('getLastValue', () => {
    it('should return last element of values', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getLastValue()).toBe('22');
    });
  });

  describe('getIndexForValue', () => {
    it('should return default value if if value passed in is "current" ', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
        defaultValue: '10',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getIndexForValue('current', true)).toBe(3);
    });
    it('should return -1 if value not found for _type = anyvalues for both false and true', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getIndexForValue('7', true)).toBe(-1);
      expect(dim.getIndexForValue('-1', false)).toBe(-1);
      expect(dim.getIndexForValue('26', false)).toBe(-1);
    });
    it('should return index if value found for _type = anyvalues for both false and true', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getIndexForValue('2', true)).toBe(1);
      expect(dim.getIndexForValue('12', false)).toBe(4);
      expect(dim.getIndexForValue('22', true)).toBe(5);
    });
    it('should return index if value found (or closest value) for _type = timevalues for both false and true', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z,2021-03-22T12:54:40Z2',
        currentValue: '2021-03-22T12:44:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getIndexForValue('2021-03-22T12:34:40Z', true)).toBe(0);
      expect(dim.getIndexForValue('2021-03-22T13:04:40Z', false)).toBe(3);
      expect(dim.getIndexForValue('2021-03-22T12:55:40Z', false)).toBe(2);
      expect(dim.getIndexForValue('2021-03-22T12:53:40Z', true)).toBe(2);
    });
    it('should return -1 if not a valid date passed in for _type = timevalues for both false and true', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z,2021-03-22T12:54:40Z2',
        currentValue: '2021-03-22T12:44:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getIndexForValue('notADate1200', true)).toBe(-1);
      expect(dim.getIndexForValue('notADate1200', false)).toBe(-1);
    });
    it('should return index if value found (or closest value) for _type = timevalues for both false and true', () => {
      const config = {
        name: 'someDim',
        values:
          '2021-03-22T12:34:40Z,2021-03-22T12:44:40Z,2021-03-22T12:54:40Z,2021-03-22T13:04:40Z,2021-03-22T12:54:40Z2',
        currentValue: '2021-03-22T12:44:40Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getIndexForValue('2021-03-22T12:34:40Z', true)).toBe(0);
      expect(dim.getIndexForValue('2021-03-22T13:04:40Z', false)).toBe(3);
      expect(dim.getIndexForValue('2021-03-22T12:55:40Z', false)).toBe(2);
      expect(dim.getIndexForValue('2021-03-22T12:53:40Z', true)).toBe(2);
    });
    it('should return index if value found (or closest value) for _type = timestartstopres for both false and true', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T12:00:00Z/2009-01-01T12:05:00Z/PT60S',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getIndexForValue('2009-01-01T12:01:00Z', true)).toBe(1);
      expect(dim.getIndexForValue('2009-01-01T12:03:00Z', false)).toBe(3);
      expect(dim.getIndexForValue('2009-01-01T12:04:35Z', true)).toBe(4);
      expect(dim.getIndexForValue('2009-01-01T12:05:00Z', false)).toBe(5);
    });
    it('should return -2 or -1 if date not in list for _type = timestartstopres for true', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T12:00:00Z/2009-01-01T12:05:00Z/PT60S',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);
      expect(dim.getIndexForValue('2009-01-01T11:00:00Z', true)).toBe(-1);
      expect(dim.getIndexForValue('2009-01-01T15:00:00Z', true)).toBe(-2);
    });
    it('should return first or last element if date not in list for _type = timestartstopres for false', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T12:00:00Z/2009-01-01T12:05:00Z/PT60S',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);

      // if smaller than start returns 0
      expect(dim.getIndexForValue('2009-01-01T11:00:00Z', false)).toBe(0);
      // if bigger than end returns last index
      expect(dim.getIndexForValue('2009-01-01T15:00:00Z', false)).toBe(5);
    });
    it('should show same behavior for _type = timestartstopres if no string passed in as value', () => {
      const config = {
        name: 'someDim',
        values: '2009-01-01T12:00:00Z/2009-01-01T12:05:00Z/PT60S',
        currentValue: '2009-01-01T12:03:00Z',
        units: 'ISO8601',
      };
      const dim = new WMJSDimension(config);

      // should return index if value found (or closest value)
      expect(dim.getIndexForValue('2009-01-01T12:01:00Z', true)).toBe(1);
      expect(dim.getIndexForValue('2009-01-01T12:03:00Z', false)).toBe(3);
      expect(dim.getIndexForValue('2009-01-01T12:04:35Z', true)).toBe(4);
      expect(dim.getIndexForValue('2009-01-01T12:05:00Z', false)).toBe(5);
      // if smaller than start for false returns 0
      expect(dim.getIndexForValue('2009-01-01T11:00:00Z', false)).toBe(0);
      // if bigger than end returns for false last index
      expect(dim.getIndexForValue('2009-01-01T15:00:00Z', false)).toBe(5);
      // if smaller than start return -1 if date not in list for true
      expect(dim.getIndexForValue('2009-01-01T11:00:00Z', true)).toBe(-1);
      // if bigger than end return -2 if date not in list for true
      expect(dim.getIndexForValue('2009-01-01T15:00:00Z', true)).toBe(-2);
    });
  });

  describe('size', () => {
    it('should return size of elemens for _type = anyvalues', () => {
      const config = {
        name: 'someDim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      expect(dim.size()).toBe(6);
    });
  });

  describe('clone', () => {
    it('should clone dimension', () => {
      const config = {
        name: 'somedim',
        values: '1,2,3,10,12,22',
        currentValue: '12',
        defaultValue: '2',
        linked: false,
        units: 'someDimUnits',
      };
      const dim = new WMJSDimension(config);
      const clonedDime = dim.clone();
      expect(clonedDime.name).toBe(config.name);
      expect(clonedDime.units).toBe(config.units);
      expect(clonedDime.generateAllValues()).toStrictEqual([
        '1',
        '2',
        '3',
        '10',
        '12',
        '22',
      ]);
      expect(clonedDime.getValue()).toBe(config.currentValue);
      expect(clonedDime.linked).toBe(config.linked);
    });
  });
});
