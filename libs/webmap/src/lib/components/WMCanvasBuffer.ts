/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import WMImage, { getCurrentImageTime } from './WMImage';
import {
  DebugType,
  debug,
  isDefined,
  getUriWithParam,
  enableConsoleDebugging,
} from './WMJSTools';
import WMImageStore, { WMImageEventType } from './WMImageStore';
import WMBBOX from './WMBBOX';
import WMListener from './WMListener';
import { LinkedInfo, LinkedInfoContent, WMPosition } from './types';

/* Global variable, shared across multiple WMCanvasBuffers */
const sharedImagesList: Record<string, CanvasGeoLayer[]> = {};

type InternalCallback = {
  beforecanvasstartdraw?: (context: CanvasRenderingContext2D) => void;
  beforecanvasdisplay?: (context: CanvasRenderingContext2D) => void;
  canvasonerror?: (e: LinkedInfo[]) => void;
  aftercanvasdisplay?: (c: CanvasRenderingContext2D) => void;
};

interface CanvasBBOX {
  left: number;
  right: number;
  top: number;
  bottom: number;
}

export interface CanvasGeoLayer {
  bbox: CanvasBBOX;
  opacity: number;
  imageAge: number;
  imageSource: string;
}
/**
 * Function to find image url's which are similar to other images, without taking width, height and bbox into account
 * @param imageSource The source / url of the image to check
 * @returns A signature for this image, which is the same for images with different bbox and or width/height
 */
export const makeQueryStringWithoutGeoInfo = (imageSource: string): string => {
  return getUriWithParam(imageSource, {
    BBOX: '',
    WIDTH: '',
    HEIGHT: '',
  });
};

/**
 * Add an image to the sharedimages list
 * @param newLayer A canvaslayer object to add.
 */
export const addAlternativeImage = (newLayer: CanvasGeoLayer): void => {
  /* Get a signature for images which belong to each other */
  const sigImageUrl = makeQueryStringWithoutGeoInfo(newLayer.imageSource);

  /* Make an entry for these images in the sharedImagesList */
  if (!sharedImagesList[sigImageUrl]) {
    sharedImagesList[sigImageUrl] = [];
  }

  /* Get the images for this signature */
  const imagesForUrl: CanvasGeoLayer[] = sharedImagesList[sigImageUrl];

  /* Check if the image is not already in the sharedImagesList */
  const imageIsAlreadyIndex = imagesForUrl.findIndex((i) => {
    return i.imageSource === newLayer.imageSource;
  });

  /* It is not in the list, so add it */
  if (imageIsAlreadyIndex === -1) {
    /* Ensure that the oldest images are removed, keep a maximum of 10 images per signature url */
    while (sharedImagesList[sigImageUrl].length >= 10) {
      sharedImagesList[sigImageUrl].pop();
    }

    /* Add it to the beginning of the array for efficieny in searching at a later stage */
    sharedImagesList[sigImageUrl].unshift(newLayer);
  }
};

/**
 * Removes the image from the alternative imageslist (in case imagestore cache gets cleaned), this used when the imagestore triggers a delete event for an image
 * @param imageUrl The url for the image to remove
 */
export const removeAlternativeImage = (imageUrl: string): void => {
  const sigImageUrl = makeQueryStringWithoutGeoInfo(imageUrl);
  const imagesForUrl: CanvasGeoLayer[] = sharedImagesList[sigImageUrl];

  if (imagesForUrl && imagesForUrl.length > 0) {
    imagesForUrl.forEach((altImage: CanvasGeoLayer, index: number) => {
      if (altImage.imageSource === imageUrl) {
        imagesForUrl.splice(index, 1);
      }
    });
  }
  if (!imagesForUrl || imagesForUrl.length === 0) {
    delete sharedImagesList[sigImageUrl];
  }
};

/**
 * Made accessible for testing purposes
 * @returns
 */
export const getSharedImagesList = (): Record<string, CanvasGeoLayer[]> => {
  return sharedImagesList;
};

/**
 * Algorithm two determine how much two bboxes are similar
 * @param rect1
 * @param rect2
 * @returns
 */
export const geClosestBBox = (rect1: CanvasBBOX, rect2: CanvasBBOX): number => {
  const diffTLX = rect1.left - rect2.left;
  const diffTLY = rect1.top - rect2.top;

  const diffBRX = rect1.right - rect2.right;
  const diffBRY = rect1.bottom - rect2.bottom;

  return (
    Math.sqrt(diffTLX * diffTLX + diffTLY * diffTLY) +
    Math.sqrt(diffBRX * diffBRX + diffBRY * diffBRY)
  );
};

/**
 * Sort the imageslist by imagedate, newewst image first.
 * @param imagesList
 * @returns
 */
export const sortByImageDate = (
  imagesList: CanvasGeoLayer[],
): CanvasGeoLayer[] => {
  const sorted = imagesList.sort((a, b) => (a.imageAge < b.imageAge ? 1 : -1));
  return sorted.map((i) => i);
};

/**
 * Function to find the an alternative available image for an image url that is not yet loaded
 * @param imageUrl The image source / url to find an alternative for
 * @param imageStore The imagestore where the other images are located
 * @param bbox The boundingbox to search this image for
 * @returns A canvasLayer, containing the new image source with its geo properties
 */
export const getAlternativeImage = (
  imageUrl: string,
  imageStore: WMImageStore,
  // eslint-disable-next-line no-unused-vars
  _bbox: CanvasBBOX,
): CanvasGeoLayer => {
  const sigImageUrl = makeQueryStringWithoutGeoInfo(imageUrl);
  const imagesForUrl: CanvasGeoLayer[] = sharedImagesList[sigImageUrl];

  if (imagesForUrl && imagesForUrl.length > 0) {
    const sortedImagesForUrl: CanvasGeoLayer[] = sortByImageDate(imagesForUrl);

    /* Find the first image in this list which is also loaded and has no error */
    const index = sortedImagesForUrl.findIndex((altImage: CanvasGeoLayer) => {
      const cachedImage = imageStore.getImageForSrc(altImage.imageSource);
      return (
        cachedImage &&
        cachedImage.isLoaded() &&
        !cachedImage.isLoading() &&
        !cachedImage.hasError()
      );
    });

    if (index !== -1) return sortedImagesForUrl[index];
  }
  return null;
};

export default class WMCanvasBuffer {
  canvas: HTMLCanvasElement;

  _ctx: CanvasRenderingContext2D;

  _imageStore: WMImageStore;

  ready: boolean;

  layers: CanvasGeoLayer[];

  _defaultImage: WMImage;

  _currentbbox: WMBBOX;

  _currentnewbbox: WMBBOX;

  _width: number;

  _height: number;

  _webmapJSCallback: WMListener;

  nrLoading: number;

  onLoadReadyFunction: (param) => void;

  _internalCallbacks: InternalCallback;

  srs: string;

  bbox: WMBBOX;

  _errorList: LinkedInfo[];

  constructor(
    webmapJSCallback: WMListener,
    _imageStore: WMImageStore,
    w: number,
    h: number,
    internalCallbacks: InternalCallback,
  ) {
    this.canvas = document.createElement('canvas');
    this.canvas.width = w;
    this.canvas.height = h;
    this._ctx = this.canvas.getContext('2d');
    this._ctx.canvas.width = w;
    this._ctx.canvas.height = h;

    this._imageStore = _imageStore;
    this.ready = true;
    this.layers = [];
    this._defaultImage = new WMImage('webmapjs/img/stoploading.png', () => {
      this._statDivBufferImageLoaded();
    });
    this._currentbbox = undefined;
    this._currentnewbbox = undefined;

    this._width = w;
    this._height = h;
    this._webmapJSCallback = webmapJSCallback;
    this._internalCallbacks = internalCallbacks;

    /* Bind */
    this.getCanvasContext = this.getCanvasContext.bind(this);
    this.imageLoadComplete = this.imageLoadComplete.bind(this);
    this._statDivBufferImageLoaded = this._statDivBufferImageLoaded.bind(this);
    this.display = this.display.bind(this);
    this.finishedLoading = this.finishedLoading.bind(this);
    this.resize = this.resize.bind(this);
    this.setSrc = this.setSrc.bind(this);
    this._getPixelCoordFromGeoCoord =
      this._getPixelCoordFromGeoCoord.bind(this);
    this.setBBOX = this.setBBOX.bind(this);
    this.getBuffer = this.getBuffer.bind(this);
    this._drawImage = this._drawImage.bind(this);

    this._imageStore.addImageEventCallback(
      (image, id, imageEventType: WMImageEventType) => {
        if (imageEventType === WMImageEventType.Loaded) {
          this.imageLoadComplete(image);
        }
        if (imageEventType === WMImageEventType.Deleted) {
          removeAlternativeImage(image.getSrc());
        }
      },
      'WMCanvasBuffer',
    );
    this._errorList = [];
  }

  getCanvasContext(): CanvasRenderingContext2D {
    return this._ctx;
  }

  /**
   * Triggered by the WMImageStore when an image completes loading
   * @param image
   */
  imageLoadComplete(image: WMImage): void {
    this._statDivBufferImageLoaded();
    this._webmapJSCallback.triggerEvent('onimageload', undefined);
    this._webmapJSCallback.triggerEvent('onimagebufferimageload', image);
    this.display();
  }

  /**
   * Checks if all images are loaded, if so it will call finishedLoading
   * @returns
   */
  _statDivBufferImageLoaded(): void {
    for (let j = 0; j < this.layers.length; j += 1) {
      const image = this._imageStore.getImageForSrc(this.layers[j].imageSource);
      if (!image || image.isLoaded() === false) {
        return;
      }
    }
    this.finishedLoading();
  }

  reset(): void {
    this.layers.length = 0;
  }

  _drawImage(canvasLayer: CanvasGeoLayer): void {
    const { bbox: imageBBox, opacity, imageSource } = canvasLayer;
    const image = this._imageStore.getImageForSrc(imageSource);
    if (!image || image.hasError()) {
      return;
    }
    this._ctx.globalAlpha = opacity;
    const imageElement = image.getElement();

    const mapBBox = this._currentnewbbox;
    const coord1 = this._getPixelCoordFromGeoCoord(
      { x: imageBBox.left, y: imageBBox.top },
      mapBBox,
    );
    const coord2 = this._getPixelCoordFromGeoCoord(
      { x: imageBBox.right, y: imageBBox.bottom },
      mapBBox,
    );

    const imageX = coord1.x;
    const imageY = coord1.y;
    const imageW = coord2.x - coord1.x;
    const imageH = coord2.y - coord1.y;

    try {
      this._ctx.drawImage(imageElement, imageX, imageY, imageW, imageH);
      if (enableConsoleDebugging) {
        /* Enable this to draw the edges of the image */
        this._ctx.lineWidth = 0.2;
        this._ctx.strokeStyle = '#0000ff';
        this._ctx.beginPath();
        this._ctx.rect(imageX, imageY, imageW, imageH);
        this._ctx.stroke();
      }
    } catch (e) {
      debug(DebugType.Error, `image error: ${e}`);
    }
  }

  display(): void {
    this._ctx.globalAlpha = 1;
    this._ctx.fillStyle = 'white';
    this._ctx.beginPath();
    this._ctx.fillRect(0, 0, this._width, this._height);

    this._ctx.fill();
    this._webmapJSCallback.triggerEvent('beforecanvasstartdraw', this._ctx);
    if (
      this._internalCallbacks &&
      this._internalCallbacks.beforecanvasstartdraw
    ) {
      this._internalCallbacks.beforecanvasstartdraw(this._ctx);
    }

    /* Reset errors */
    this._errorList.length = 0;

    for (let j = 0; j < this.layers.length; j += 1) {
      const imageToDisplay = this._imageStore.getImageForSrc(
        this.layers[j].imageSource,
      );
      if (
        imageToDisplay &&
        imageToDisplay.isLoaded() &&
        !imageToDisplay.isLoading() &&
        !imageToDisplay.hasError()
      ) {
        // Draw this image, it is loaded and OK
        this._drawImage(this.layers[j]);
      } else {
        /**
         * Check if this image has an error.
         * If the error has occured some time ago, and the amount of retries is low, just retry to load the image.
         */
        if (imageToDisplay.hasError()) {
          if (
            imageToDisplay.isLoading() === false &&
            imageToDisplay.getLastErrorMSecondsAgo() > 5000
          ) {
            if (imageToDisplay.getNumFailedAttempts() < 10) {
              imageToDisplay.forceReload();
            }
          }
        }
        /* Find closest alternative and display instead image */
        const im = getAlternativeImage(
          this.layers[j].imageSource,
          this._imageStore,
          this._currentnewbbox,
        );
        if (im) {
          this._drawImage(im);
        }
      }
    }
    this._ctx.globalAlpha = 1;

    /* Display errors */
    if (this._errorList.length > 0) {
      this._webmapJSCallback.triggerEvent('canvasonerror', this._errorList);
      if (this._internalCallbacks && this._internalCallbacks.canvasonerror) {
        this._internalCallbacks.canvasonerror(this._errorList);
      }
    }
    this._webmapJSCallback.triggerEvent('beforecanvasdisplay', this._ctx);
    if (
      this._internalCallbacks &&
      this._internalCallbacks.beforecanvasdisplay
    ) {
      this._internalCallbacks.beforecanvasdisplay(this._ctx);
    }

    this.canvas.style.display = 'inline-block';
    this._webmapJSCallback.triggerEvent('aftercanvasdisplay', this._ctx);
    if (this._internalCallbacks && this._internalCallbacks.aftercanvasdisplay) {
      this._internalCallbacks.aftercanvasdisplay(this._ctx);
    }
  }

  finishedLoading(): void {
    if (this.ready) return;
    this.ready = true;
    for (let j = 0; j < this.layers.length; j += 1) {
      const image = this._imageStore.getImageForSrc(this.layers[j].imageSource);
      if (image.hasError()) {
        debug(DebugType.Error, `image error: ${image.getSrc()}`);
      }
    }
    try {
      if (isDefined(this.onLoadReadyFunction)) {
        this.onLoadReadyFunction(this);
      }
    } catch (e) {
      debug(DebugType.Error, `Exception in Divbuffer::finishedLoading: ${e}`);
    }
  }

  resize(w: string, h: string): void {
    const width = parseInt(w, 10);
    const height = parseInt(h, 10);
    if (this._width === width && this._height === height) return;
    this._width = width;
    this._height = height;
    this.canvas.width = width;
    this.canvas.height = height;
    this._ctx.canvas.height = height;
    this._ctx.canvas.width = width;
  }

  /**
   * Set's a layer inside the canvasbuffer with given index and imagesource.
   * The image is directly loaded, but if loading is not complete when drawing, an alternative image is search in the alternative images list
   * @param layerIndex The layer index, similar to z-Index, 0 drawn on the bottom, higher numbers on top
   * @param imageSource The url of the image, usually a GetMap url
   * @param linkedInfo A linkedinfo object to track properties for the image and layer, like errors
   * @param opacity Opacity of this layer
   * @param bbox The boundingbox for this layer
   * @returns Void
   */
  setSrc(
    layerIndex: number,
    imageSource: string,
    linkedInfo: LinkedInfoContent,
    opacity: number,
    bbox: WMBBOX,
  ): void {
    if (!isDefined(imageSource)) {
      debug(DebugType.Warning, 'Image source is not set');
      return;
    }

    /* HTTP Headers for the image, necessary in case of certain authentication forms */
    const headers =
      linkedInfo && linkedInfo.layer && linkedInfo.layer.headers
        ? linkedInfo.layer.headers
        : [];

    /* Make a new CanvasLayer */
    const newLayer: CanvasGeoLayer = {
      imageSource,
      imageAge: getCurrentImageTime(),
      opacity,
      bbox: {
        left: bbox.left,
        right: bbox.right,
        top: bbox.top,
        bottom: bbox.bottom,
      },
    };

    /* Build same layers array as map layers array */
    while (layerIndex >= this.layers.length) {
      this.layers.push(newLayer);
    }
    this.layers[layerIndex] = newLayer;

    /* Track this layer in the alternative images list */
    addAlternativeImage(newLayer);

    /* Get the image with this imagesource from the imagestore and load it */
    const image = this._imageStore.getImage(imageSource, { headers });
    if (!image.isLoaded() && !image.isLoading()) {
      image.load();
    }
  }

  _getPixelCoordFromGeoCoord(coordinates: WMPosition, b: WMBBOX): WMPosition {
    const x = (this._width * (coordinates.x - b.left)) / (b.right - b.left);
    const y = (this._height * (coordinates.y - b.top)) / (b.bottom - b.top);
    return { x, y };
  }

  setBBOX(newbbox: WMBBOX, loadedbbox: WMBBOX): void {
    this._currentbbox = loadedbbox;
    this._currentnewbbox = newbbox;
    this.display();
  }

  getBuffer(): HTMLCanvasElement {
    return this.canvas;
  }
}
