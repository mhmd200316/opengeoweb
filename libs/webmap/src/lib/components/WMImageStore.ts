/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { mapImageStoreLength } from './WMConstants';
import WMImage, { WMImageOptions } from './WMImage';
import { debug, DebugType } from './WMJSTools';

export enum WMImageEventType {
  Loaded,
  Deleted,
}

export default class WMImageStore {
  imagesbysrc: Record<string, WMImage>;

  imageLife: number;

  _imageLifeCounter: number;

  _loadEventCallbackList: Array<{
    id: string;
    callback: (
      image: WMImage,
      id: string,
      imageEventType: WMImageEventType,
    ) => void;
  }>;

  _maxNumberOfImages: number;

  _options: { id: string };

  emptyImage: WMImage;

  /**
   * Constructs a new WMImageStore with given amount of image cache.
   * @param {*} maxNumberOfImages
   * @param {*} options
   */
  constructor(maxNumberOfImages: number, options: { id: string }) {
    this.imagesbysrc = {};
    this.imageLife = 0;
    this._imageLifeCounter = 0;
    this._loadEventCallbackList = []; // Array of callbacks, as multiple instances can register listeners
    this._maxNumberOfImages = maxNumberOfImages;
    this._options = options;
    this.emptyImage = new WMImage(null, null, null);
    this.imageLoadEventCallback = this.imageLoadEventCallback.bind(this);
    this.getImageForSrc = this.getImageForSrc.bind(this);
    this.clear = this.clear.bind(this);
    this.addImageEventCallback = this.addImageEventCallback.bind(this);
    this.removeAllEventCallbacks = this.removeAllEventCallbacks.bind(this);
    this.getNumImagesLoading = this.getNumImagesLoading.bind(this);
    this.getImage = this.getImage.bind(this);
    this.load = this.load.bind(this);
  }

  load(imageUrl: string): void {
    return this.getImage(imageUrl).load();
  }

  imageLoadEventCallback(
    _img: WMImage,
    imageEventType: WMImageEventType,
  ): void {
    for (let j = 0; j < this._loadEventCallbackList.length; j += 1) {
      this._loadEventCallbackList[j].callback(
        _img,
        this._loadEventCallbackList[j].id,
        imageEventType,
      );
    }
  }

  /**
   * Check if we have similar images with the same source in the pipeline
   */
  getImageForSrc(src: string): WMImage {
    if (this.imagesbysrc[src]) {
      return this.imagesbysrc[src];
    }
    return undefined;
  }

  clear(): void {
    Object.keys(this.imagesbysrc).forEach((property) => {
      this.imagesbysrc[property].clear();
    });
  }

  addImageEventCallback(
    callback: (
      image: WMImage,
      id: string,
      imageEventType: WMImageEventType,
    ) => void,
    id: string,
  ): void {
    if (!id) {
      debug(DebugType.Error, 'addImageEventCallback: id not provided');
      return;
    }
    if (!callback) {
      debug(DebugType.Error, 'addImageEventCallback: callback not provided');
      return;
    }
    this._loadEventCallbackList.push({
      id,
      callback,
    });
  }

  removeAllEventCallbacks(): void {
    this._loadEventCallbackList.length = 0;
  }

  removeEventCallback(id: string): void {
    for (let j = this._loadEventCallbackList.length - 1; j >= 0; j -= 1) {
      if (this._loadEventCallbackList[j].id === id) {
        this._loadEventCallbackList.splice(j, 1);
      }
    }
  }

  getNumImagesLoading(): number {
    let numLoading = 0;
    Object.keys(this.imagesbysrc).forEach((property) => {
      if (this.imagesbysrc[property].isLoading()) {
        numLoading += 1;
      }
    });
    return numLoading;
  }

  /**
   * Get an WMImage object for given URL
   * @param {*} src The url for the image
   * @returns WMImage object
   */
  getImage(src: string, loadOptions?: WMImageOptions): WMImage {
    if (!src) {
      return null;
    }
    /** Check if we have an image in the pipeline * */
    let image = this.getImageForSrc(src);
    if (image !== undefined) {
      this._imageLifeCounter += 1;
      image.imageLife = this._imageLifeCounter;
      return image;
    }

    /** Create or reuse an image * */
    if (Object.keys(this.imagesbysrc).length < this._maxNumberOfImages) {
      image = new WMImage(src, (img: WMImage) => {
        this.imageLoadEventCallback(img, WMImageEventType.Loaded);
      });
      image.setSource(src, loadOptions);
      this.imagesbysrc[src] = image;
      this._imageLifeCounter += 1;
      image.imageLife = this._imageLifeCounter;
      return image;
    }
    /* We have to reuse an image */
    let imageId = '';
    let minImageLife = this._imageLifeCounter;
    Object.keys(this.imagesbysrc).forEach((property) => {
      const img = this.imagesbysrc[property];
      if (img.isLoading() === false) {
        // && img.isLoaded() === true) {
        if (minImageLife >= img.imageLife) {
          minImageLife = img.imageLife;
          imageId = property;
        }
      }
    });
    if (imageId === '') {
      return this.emptyImage;
    }

    image = this.imagesbysrc[imageId];
    delete this.imagesbysrc[imageId];
    this.imageLoadEventCallback(image, WMImageEventType.Deleted);
    image.clear();

    image.setSource(src, loadOptions);
    this.imagesbysrc[src] = image;
    this._imageLifeCounter += 1;
    image.imageLife = this._imageLifeCounter;
    return image;
  }
}

/* Global image stores */
export const getMapImageStore = new WMImageStore(mapImageStoreLength, {
  id: 'getMapImageStore',
});
