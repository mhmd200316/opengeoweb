/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';

import {
  WMDateOutSideRange,
  WMDateTooEarlyString,
  WMDateTooLateString,
} from './WMConstants';
import { isDefined, debug, DebugType } from './WMJSTools';
import {
  DateInterval,
  parseISO8601DateToDate,
  parseISO8601IntervalToDateInterval,
  ParseISOTimeRangeDuration,
} from './WMTime';
import { CustomDate } from './WMTimeTypes';

interface WMJSDimensionConfig {
  name?: string;
  units?: string;
  values?: string;
  currentValue?: string;
  defaultValue?: string;
  linked?: boolean;
  unitSymbol?: string;
  synced?: boolean;
}

/**
 * WMJSDimension Class
 * Keep all information for a single dimension, like time.
 * Author : MaartenPlieger (plieger at knmi.nl)
 * Copyright KNMI
 */

export default class WMJSDimension {
  public currentValue: string;

  public defaultValue: string;

  name: string;

  units: string;

  unitSymbol: string;

  linked: boolean;

  synced: boolean;

  public used?: boolean;

  public values?: string;

  private timeRangeDuration?;

  private _initialized: boolean;

  private _timeRangeDurationDate;

  private _allDates: CustomDate[];

  private _type: string;

  private _allValues: string[];

  private dimMinValue: string;

  private dimMaxValue: string;

  private dimTimeInterval: DateInterval;

  constructor(config?: WMJSDimensionConfig) {
    this.name = undefined; // Name of the dimension, e.g. 'time'
    this.units = undefined; // Units of the dimension, e.g. 'ISO8601'
    this.unitSymbol = undefined; // Unit symbol, eg: hPa
    this.values = undefined; // Values of the dimension, according to values defined in WMS specification, e.g. 2011-01-01T00:00:00Z/2012-01-01T00:00:00Z/P1M or list of values.
    this.timeRangeDuration = undefined;
    this.linked = true;
    this.synced = false;

    /* Private starts with _ */
    this._initialized = false;
    this._timeRangeDurationDate = null; // Used for timerange (start/stop/res)
    this._allDates = []; // Used for individual timevalues
    this._type = null; // Can be 'timestartstopres', 'timevalues' or 'anyvalue'
    this._allValues = [];

    this.setTimeValuesForReferenceTime =
      this.setTimeValuesForReferenceTime.bind(this);

    this.generateAllValues = this.generateAllValues.bind(this);
    this.reInitializeValues = this.reInitializeValues.bind(this);
    this.initialize = this.initialize.bind(this);
    this.getValue = this.getValue.bind(this);
    this.setValue = this.setValue.bind(this);
    this.getValues = this.setValue.bind(this);
    this.setClosestValue = this.setClosestValue.bind(this);
    this.addTimeRangeDurationToValue =
      this.addTimeRangeDurationToValue.bind(this);
    this.setTimeRangeDuration = this.setTimeRangeDuration.bind(this);
    this.getClosestValue = this.getClosestValue.bind(this);
    this.getValueForIndex = this.getValueForIndex.bind(this);
    this.get = this.get.bind(this);
    this.getFirstValue = this.getFirstValue.bind(this);
    this.getLastValue = this.getLastValue.bind(this);
    this.getDimInterval = this.getDimInterval.bind(this);

    this.getIndexForValue = this.getIndexForValue.bind(this);
    this.size = this.size.bind(this);
    this.clone = this.clone.bind(this);

    if (isDefined(config)) {
      if (isDefined(config.name)) {
        this.name = config.name.toLowerCase();
      }
      if (isDefined(config.units)) {
        this.units = config.units;
      }
      if (isDefined(config.unitSymbol)) {
        this.unitSymbol = config.unitSymbol;
      }
      if (isDefined(config.values)) {
        this.values = config.values;
      }
      if (isDefined(config.currentValue)) {
        this.currentValue = config.currentValue;
      }
      if (isDefined(config.defaultValue)) {
        this.defaultValue = config.defaultValue;
      }
      if (isDefined(config.linked)) {
        this.linked = config.linked;
      }
      if (isDefined(config.synced)) {
        this.synced = config.synced;
      }
    }
  }

  generateAllValues(): number[] {
    const vals = [];
    if (this.size() > 5000) {
      throw new Error(
        'Error: Dimension too large to query all possible values at once',
      );
    }
    for (let i = 0; i < this.size(); i += 1) {
      vals.push(this.getValueForIndex(i));
    }
    return vals;
  }

  setTimeValuesForReferenceTime(
    referenceTimeValueToSet: string,
    referenceTimeDim: WMJSDimension,
  ): void {
    if (!this._initialized) {
      this.initialize();
    }
    if (!referenceTimeValueToSet) {
      this.reInitializeValues(this.values);
      this.setClosestValue(true);
      debug(DebugType.Warning, 'returning');
      return;
    }

    /*
     * Calculate the model run length,
     * this can be done by looking at the difference between
     * the last dim_reference_time and last dim_time
     */

    /* 1. Get the last value in the list of the referencetime */
    const lastRefTimeAsMoment = moment.utc(referenceTimeDim.getLastValue());
    /* 2. Get the original last value of the time dimension, we cannot use this.getLastValue(), as we are changing this. */
    const timeLastValueAsMoment = moment.utc(this.values.split('/')[1]);
    /* 3. Now calculate the run length */
    const runLengthAsHour = moment
      .duration(timeLastValueAsMoment.clone().diff(lastRefTimeAsMoment))
      .asHours();

    const newStartValueTimeAsMoment = moment.utc(referenceTimeValueToSet);

    const newEndValueTimeAsMoment = newStartValueTimeAsMoment
      .clone()
      .add(runLengthAsHour, 'h');

    if (this._type === 'timestartstopres') {
      /* Compose a new dataString interval and re-initialize the time dimension */
      const dateStringItems = this.values.split('/');
      const dateStringInterval = dateStringItems[2];
      const newValue = `${newStartValueTimeAsMoment.toISOString()}/${newEndValueTimeAsMoment.toISOString()}/${dateStringInterval}`;
      this.defaultValue = newEndValueTimeAsMoment.toISOString();
      this.reInitializeValues(newValue, true);
    } else if (this._type === 'timevalues') {
      /* Filter all dates from the array which are lower than given start value */
      /* TODO: Maarten Plieger 2020-06-10: Make this also work for WMS times advertised as a comma separated list */
      const newValue = parseISO8601DateToDate(
        newStartValueTimeAsMoment.toISOString(),
      );
      const newArray = this._allDates.filter((x) => x >= newValue);
      let newValues = '';
      for (let j = 0; j < newArray.length; j += 1) {
        if (j > 0) newValues += ',';
        newValues += newArray[j].toISO8601();
      }
      this.reInitializeValues(newValues, true);
      this.setClosestValue();
    }
  }

  reInitializeValues(values: string, forceReferenceTimeValue = false): void {
    this._initialized = false;
    this.initialize(values, forceReferenceTimeValue);
  }

  initialize(
    forceothervalues = undefined,
    forceReferenceTimeValue = false,
  ): void {
    if (this._initialized === true) return;
    let ogcdimvalues = this.values;
    if (forceothervalues) {
      ogcdimvalues = forceothervalues;
    }
    if (!isDefined(ogcdimvalues)) return;
    this._allValues = [];
    this._initialized = true;
    if (this.units === 'ISO8601') {
      if (ogcdimvalues.indexOf('/') > 0 && ogcdimvalues.indexOf(',') === -1) {
        this._type = 'timestartstopres';
        this._timeRangeDurationDate = new ParseISOTimeRangeDuration(
          ogcdimvalues,
        );
      } else {
        // TODO Parse 2007-03-27T00:00:00.000Z/2007-03-31T00:00:00.000Z/PT1H,2007-04-07T00:00:00.000Z/2007-04-11T00:00:00.000Z/PT1H
        this._type = 'timevalues';
      }
    } else {
      this._type = 'anyvalue';
      this.linked = false;
    }
    if (this._type !== 'timestartstopres') {
      const values = ogcdimvalues.split(',');
      for (let j = 0; j < values.length; j += 1) {
        const valuesRanged = values[j].split('/');
        if (valuesRanged.length === 3) {
          /* Can be either time like '2021-03-17T06:00:00Z/2021-03-21T00:00:00Z/PT6H' or something else, like '0/24/2' */
          if (valuesRanged[2].charAt(0) === 'P') {
            const partTimeRanges = new ParseISOTimeRangeDuration(values[j]);
            for (let i = 0; i < partTimeRanges.getTimeSteps(); i += 1) {
              const dataAtTimeStep = partTimeRanges.getDateAtTimeStep(i);
              try {
                this._allValues.push(dataAtTimeStep.toISO8601());
              } catch (e) {
                this._allValues.push(dataAtTimeStep.toString());
              }
            }
          } else {
            const start = parseFloat(valuesRanged[0]);
            let stop = parseFloat(valuesRanged[1]);
            let res = parseFloat(valuesRanged[2]);
            stop += res;
            if (start > stop) stop = start;
            if (res <= 0) res = 1;
            for (let j2 = start; j2 < stop; j2 += res) {
              this._allValues.push(String(j2 as unknown) as string);
            }
          }
        } else {
          this._allValues.push(String(values[j] as unknown) as string);
        }
      }

      if (this._type === 'timevalues') {
        this._allDates.length = 0;
        for (let j = 0; j < this._allValues.length; j += 1) {
          this._allDates.push(parseISO8601DateToDate(this._allValues[j]));
        }
      }
    }

    if (!isDefined(this.defaultValue)) {
      this.defaultValue = this.getValueForIndex(0);
    }

    /* If no currentvalue is set, set the default */
    if (!isDefined(this.currentValue)) {
      this.currentValue = this.getFirstValue();
    }

    /* Check for out of range values and adjust accordingly to a valid range */
    const index = this.getIndexForValue(this.currentValue);
    if (index === -2) this.currentValue = this.getLastValue(); // Date is past the latest (-2), so set it to the last value
    if (index === -1) this.currentValue = this.getFirstValue(); // Date is past the first (-1), so set it to the first value

    /* In case of reference time, the time dimension range is changed, check if the current time still fits in that range */
    if (forceReferenceTimeValue) {
      if (this.getIndexForValue(this.currentValue, true) < 0) {
        this.currentValue = this.getClosestValue(this.currentValue);
      }
    }

    this.dimMinValue = this.getFirstValue();
    this.dimMaxValue = this.getLastValue();
    if (this._timeRangeDurationDate) {
      this.dimTimeInterval = this._timeRangeDurationDate.timeInterval;
    } else {
      this.dimTimeInterval = null;
    }
  }

  /**
   * Returns the current value of this dimensions.
   * If a ISO8601 period is set, the getValue will return a timeRange using addTimeRangeDurationToValue instead of a single value
   */
  getValue(): string {
    if (!this._initialized) {
      this.initialize();
    }
    let value = this.defaultValue;
    if (isDefined(this.currentValue)) {
      value = this.currentValue;
    }
    value = this.addTimeRangeDurationToValue(value);
    return value;
  }

  /**
   * Set current value of this dimension
   * @param value The new value for this dimension (string)
   * @param forceValue When set to false,check if the given value is outside range, and do nothing if this is the case
   */
  setValue(value: string, forceValue = true): void {
    if (!this._initialized) {
      this.initialize();
    }
    if (forceValue === false) {
      if (
        value === WMDateOutSideRange ||
        value === WMDateTooEarlyString ||
        value === WMDateTooLateString
      ) {
        return;
      }
    }
    this.currentValue = value;
  }

  /**
   * Returns values of the dimension, according to values defined in WMS specification, e.g. 2011-01-01T00:00:00Z/2012-01-01T00:00:00Z/P1M or list of values.
   * @returns
   */
  getValues(): string {
    this.initialize();
    return this.values;
  }

  setClosestValue(newValue = null, evenWhenOutsideRange = true): void {
    const newClosestValue = newValue || this.getValue();
    this.currentValue = this.getClosestValue(
      newClosestValue,
      evenWhenOutsideRange,
    );
  }

  addTimeRangeDurationToValue(value: string): string {
    if (
      value === WMDateOutSideRange ||
      value === WMDateTooEarlyString ||
      value === WMDateTooLateString
    ) {
      return value;
    }
    if (this.timeRangeDuration && this.timeRangeDuration.length > 0) {
      const interval = parseISO8601IntervalToDateInterval(
        this.timeRangeDuration,
      );
      const value2date = parseISO8601DateToDate(value);
      value2date.add(interval);
      const value2 = value2date.toISO8601();
      return `${value}/${value2}`;
    }
    return value;
  }

  // If a ISO8601 period is given, the getValue will return a timeRange instead of a single value
  setTimeRangeDuration(duration: string): void {
    this.timeRangeDuration = duration;
    if (duration && duration.length > 0) {
      this.reInitializeValues(this.values);
      const startDate = parseISO8601DateToDate(this.dimMinValue);
      const stopDate = this.dimMaxValue;
      const interval = parseISO8601IntervalToDateInterval(
        this.timeRangeDuration,
      );
      if (interval.minute !== 0) {
        startDate.setUTCSeconds(0);
      }
      if (interval.hour !== 0) {
        startDate.setUTCSeconds(0);
        startDate.setUTCMinutes(0);
      }
      if (interval.day !== 0) {
        startDate.setUTCSeconds(0);
        startDate.setUTCMinutes(0);
        startDate.setUTCHours(0);
      }
      if (interval.month !== 0) {
        startDate.setUTCSeconds(0);
        startDate.setUTCMinutes(0);
        startDate.setUTCHours(0);
        startDate.setUTCDate(1);
      }
      this.reInitializeValues(
        `${startDate.toISO8601()}/${stopDate}/${this.timeRangeDuration}`,
      );
    } else {
      this.reInitializeValues(this.values);
    }
  }

  getClosestValue(_newValue: string, evenWhenOutsideRange = false): string {
    const newValue =
      _newValue && _newValue.indexOf('/') !== -1
        ? _newValue.split('/')[0]
        : _newValue;

    if (newValue === 'current' || newValue === 'default' || newValue === '') {
      return this.defaultValue;
    }
    if (newValue === 'middle') {
      let middleIndex = Math.ceil(this.size() / 2) - 1;

      if (middleIndex < 0) middleIndex = 0;
      return this.getValueForIndex(middleIndex);
    }
    if (newValue === 'latest') {
      return this.getLastValue();
    }
    if (newValue === 'earliest') {
      return this.getFirstValue();
    }

    let index = -1;
    let value: string = WMDateOutSideRange;
    try {
      index = this.getIndexForValue(newValue);
      value = this.getValueForIndex(index);
    } catch (e) {
      if (typeof e.message === 'number') {
        if (e.message === 0) value = WMDateTooEarlyString;
        else value = WMDateTooLateString;
      }
    }

    if (evenWhenOutsideRange && value === WMDateTooLateString) {
      value = this.getLastValue();
    } else if (evenWhenOutsideRange && value === WMDateTooEarlyString) {
      value = this.getValueForIndex(0);
    }

    return value;
  }

  /**
   * Get dimension value for specified index
   */
  getValueForIndex(index: number): string {
    this.initialize();
    if (index < 0) {
      if (index === -1) {
        return WMDateTooEarlyString;
      }
      if (index === -2) {
        return WMDateTooLateString;
      }
      return -1 as unknown as string;
    }
    if (this._type === 'timestartstopres') {
      try {
        return this._timeRangeDurationDate.getDateAtTimeStep(index).toISO8601();
      } catch (e) {
        // nothing
      }
      return this._timeRangeDurationDate.getDateAtTimeStep(index);
    }
    if (this._type === 'timevalues') return this._allValues[index];
    if (this._type === 'anyvalue') return this._allValues[index];
    return null;
  }

  /**
   * Hint about the timestep size / time resolution of this dimension.
   * @returns The dimTimeInterval
   */
  getDimInterval(): DateInterval {
    this.initialize();
    return this.dimTimeInterval;
  }

  /**
   * Shorthand functionf or getValueForIndex
   */
  get(index: number): string {
    return this.getValueForIndex(index);
  }

  /**
   * Returns the first dimension value
   */
  getFirstValue(): string {
    return this.get(0);
  }

  /**
   * Returns the last dimension value
   */
  getLastValue(): string {
    return this.get(this.size() - 1);
  }

  /**
   * Get index value for specified value. Returns the index in the store for the given time value, either a date or a iso8601 string can be passes as input.
   * @param value Either a JS Date object or an ISO8601 String
   * @return The index of the value.  If outSideOfRangeFlag is false, a valid index will
   * always be returned. If outSideOfRangeFlag is true: -1 if the index is not in the store,
   * but is lower than available values, -2 if the index is not in store, but is higher than available values
   */
  getIndexForValue(value: string, outSideOfRangeFlag = true): number {
    this.initialize();
    if (typeof value === 'string') {
      if (value === 'current' && this.defaultValue !== 'current') {
        return this.getIndexForValue(this.defaultValue);
      }
    }
    if (this._type === 'timestartstopres') {
      try {
        if (typeof value === 'string') {
          return this._timeRangeDurationDate.getTimeStepFromISODate(
            value,
            outSideOfRangeFlag,
          );
        }
        return this._timeRangeDurationDate.getTimeStepFromDate(
          value,
          outSideOfRangeFlag,
        );
      } catch (e) {
        if (parseInt(e.message, 10) === 0) return -1;
        return -2;
      }
    }
    if (this._type === 'timevalues') {
      try {
        const dateToFind = parseISO8601DateToDate(value).getTime();
        let minDistance;
        let foundIndex = 0;
        let minTime = null;
        let maxTime = null;
        for (let j = 0; j < this._allValues.length; j += 1) {
          const time = this._allDates[j].getTime();
          if (time < minTime || minTime === null) minTime = time;
          if (time > maxTime || maxTime === null) maxTime = time;
          let distance = time - dateToFind;
          if (distance < 0) distance = -distance;
          if (j === 0) minDistance = distance;
          if (distance < minDistance) {
            minDistance = distance;
            foundIndex = j;
          }
        }
        if (outSideOfRangeFlag) {
          if (minTime > dateToFind) return -1;
          if (maxTime < dateToFind) return -2;
        }
        return foundIndex;
      } catch (e) {
        debug(DebugType.Error, `WMSJDimension::getIndexForValue,2: ${e}`);
        return -1;
      }
    }

    if (this._type === 'anyvalue') {
      for (let j = 0; j < this._allValues.length; j += 1) {
        if (this._allValues[j] === value) return j;
      }
    }

    return -1;
  }

  /**
   * Get number of values
   */
  size(): number {
    this.initialize();
    if (this._type === 'timestartstopres')
      return this._timeRangeDurationDate.getTimeSteps();
    if (this._type === 'timevalues' || this._type === 'anyvalue') {
      return this._allValues.length;
    }
    return null;
  }

  /**
   * Clone this dimension
   */
  clone(): WMJSDimension {
    const dim = new WMJSDimension();
    dim.name = this.name;
    dim.units = this.units;
    dim.unitSymbol = this.unitSymbol;
    dim.values = this.values;
    dim.initialize();
    dim.currentValue = this.currentValue;
    dim.defaultValue = this.defaultValue;
    dim.linked = this.linked;
    dim.synced = this.synced;
    return dim;
  }
}
