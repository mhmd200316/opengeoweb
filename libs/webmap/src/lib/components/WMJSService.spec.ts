/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { waitFor } from '@testing-library/react';
import { WMSVersion } from './WMConstants';
import { WMServiceStoreXML2JSONRequest } from './WMGlobals';
import {
  getWMSUrl,
  loadGetCapabilitiesViaProxy,
  recursivelyFindLayer,
  sortByKey,
  WMJSGetCapabilities,
  WMJSService,
} from './WMJSService';

describe('components/WMJSService', () => {
  it('should create class with correct properties', () => {
    const wmjsService = new WMJSService({
      service: 'test',
    });

    expect(wmjsService.getCapabilities).toBeTruthy();
    expect(wmjsService.service).toEqual('test');
  });
  describe('loadGetCapabilitiesViaProxy', () => {
    it('should call succes when capabilities are loaded', async () => {
      const url = 'test';
      const succes = jest.fn();
      const fail = jest.fn();
      const xml2jsonrequestURL = 'xmltesturl';

      // mock the getcapreq fetch with a succes response
      window.fetch = jest.fn().mockResolvedValue({
        text: () => Promise.resolve('succes'),
      });
      const spy = jest.spyOn(window, 'fetch');

      loadGetCapabilitiesViaProxy(url, succes, fail, xml2jsonrequestURL);

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(
          `${xml2jsonrequestURL}request=${url}`,
          {},
        );
        expect(succes).toHaveBeenCalledWith('succes');
        expect(fail).not.toHaveBeenCalled();
      });
    });

    it('should call fail when request fails', async () => {
      const url = 'test';
      const succes = jest.fn();
      const fail = jest.fn();
      const xml2jsonrequestURL = 'xmltesturl';

      // mock the getcapreq fetch with a failed request
      window.fetch = jest.fn().mockRejectedValueOnce({
        text: () => Promise.reject(new Error('fail')),
      });
      const spy = jest.spyOn(window, 'fetch');

      loadGetCapabilitiesViaProxy(url, succes, fail, xml2jsonrequestURL);

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(
          `${xml2jsonrequestURL}request=${url}`,
          {},
        );
        expect(succes).not.toHaveBeenCalled();
        expect(fail).toHaveBeenCalledWith(
          `Request failed for ${xml2jsonrequestURL}request=${url}`,
        );
      });
    });

    it('should call fail when fetch does not have any functions', async () => {
      const url = 'test';
      const succes = jest.fn();
      const fail = jest.fn();
      const xml2jsonrequestURL = 'xmltesturl';

      // mock the fetch functionality
      window.fetch = jest.fn();

      loadGetCapabilitiesViaProxy(url, succes, fail, xml2jsonrequestURL);

      await waitFor(() => {
        expect(succes).not.toHaveBeenCalled();
        expect(fail).toHaveBeenCalledWith(
          `Request failed for ${xml2jsonrequestURL}request=${url}`,
        );
      });
    });
  });
  describe('getWMSUrl', () => {
    it('should return correct service url', () => {
      const url = 'https://www.testurl.com/wms';
      const result = getWMSUrl(url);
      expect(result).toEqual(`${url}?service=WMS&request=GetCapabilities`);
    });
    it('should return correct service url with custom params', () => {
      const url = 'https://www.testurl.com/wms';
      const random = Math.random();
      const params = { random };
      const result = getWMSUrl(url, params);
      expect(result).toEqual(
        `${url}?service=WMS&request=GetCapabilities&random=${random}`,
      );
    });
    it('should not overwrite any service or request params already in the url and add if not there', () => {
      const url =
        'https://www.testurl.com/wms?request=GetCapabilitiesThatIsAlreadyThere';
      const result = getWMSUrl(url);
      expect(result).toEqual(`${url}&service=WMS`);
    });
  });

  describe('WMJSGetCapabilities', () => {
    const mockRandom = 123;
    beforeEach(() => {
      jest.spyOn(global.Math, 'random').mockReturnValue(mockRandom);
    });
    afterEach(() => {
      jest.spyOn(global.Math, 'random').mockRestore();
    });
    it('should call fail when service undefined', async () => {
      const service = undefined;
      const succes = jest.fn();
      const fail = jest.fn();
      const options = undefined;

      window.fetch = jest.fn();

      WMJSGetCapabilities(service, succes, fail, options);

      await waitFor(() => {
        expect(succes).not.toHaveBeenCalled();
        expect(fail).toHaveBeenCalledWith('No service defined');
      });
    });

    it('should call fail when service empty', async () => {
      const service = '';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = undefined;

      window.fetch = jest.fn();

      WMJSGetCapabilities(service, succes, fail, options);

      await waitFor(() => {
        expect(succes).not.toHaveBeenCalled();
        expect(fail).toHaveBeenCalledWith('Service URL is empty');
      });
    });

    it('should call fail when service doesnt start with http, https, / or //', async () => {
      const service = 'htt://serviceurl';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = undefined;

      window.fetch = jest.fn();

      WMJSGetCapabilities(service, succes, fail, options);

      await waitFor(() => {
        expect(succes).not.toHaveBeenCalled();
        expect(fail).toHaveBeenCalledWith('Service URL is empty');
      });
    });

    it('should call succes when capabilities are loaded', async () => {
      const service = 'https://testservice.nl/wms';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = {
        headers: [{ name: 'content-type', value: 'application/xml' }],
      };
      const expectedHeaders = new Headers();
      expectedHeaders.append(options.headers[0].name, options.headers[0].value);

      // mock the WMXMLParser fetch with a succes response
      window.fetch = jest.fn().mockResolvedValue({
        text: () =>
          Promise.resolve(
            '<?xml version="1.0" encoding="utf-8"?><testelement myattr="myattrvalue">myelementvalue</testelement>',
          ),
        headers: expectedHeaders,
      });
      const spy = jest.spyOn(window, 'fetch');

      WMJSGetCapabilities(service, succes, fail, options);

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(
          expect.stringContaining(
            `${service}?service=WMS&request=GetCapabilities`,
          ),
          { headers: expectedHeaders, method: 'GET', mode: 'cors' },
        );
        expect(succes).toHaveBeenCalledWith({
          testelement: {
            attr: { myattr: 'myattrvalue' },
            value: 'myelementvalue',
          },
        });
        expect(fail).not.toHaveBeenCalled();
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });

    it('should call success with disabled cache', async () => {
      const service = 'https://testservice.com/wms';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = {
        headers: [{ name: 'content-type', value: 'application/xml' }],
      };
      const expectedHeaders = new Headers();
      expectedHeaders.append(options.headers[0].name, options.headers[0].value);

      // mock the WMXMLParser fetch with a succes response
      window.fetch = jest.fn().mockResolvedValue({
        text: () =>
          Promise.resolve(
            '<?xml version="1.0" encoding="utf-8"?><testelement myattr="myattrvalue">myelementvalue</testelement>',
          ),
        headers: expectedHeaders,
      });

      const spy = jest.spyOn(window, 'fetch');

      WMJSGetCapabilities(service, succes, fail, options, true);

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(
          expect.stringContaining(
            `${service}?service=WMS&request=GetCapabilities&random=${mockRandom}`,
          ),
          { headers: expectedHeaders, method: 'GET', mode: 'cors' },
        );
        expect(succes).toHaveBeenCalledWith({
          testelement: {
            attr: { myattr: 'myattrvalue' },
            value: 'myelementvalue',
          },
        });
        expect(fail).not.toHaveBeenCalled();
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });

    it('should call success with a relative service url', async () => {
      const service = '/relative/service/url?';
      const succes = jest.fn();
      const fail = jest.fn();
      const options = {
        headers: [{ name: 'content-type', value: 'application/xml' }],
      };
      const expectedHeaders = new Headers();
      expectedHeaders.append(options.headers[0].name, options.headers[0].value);

      // mock the WMXMLParser fetch with a succes response
      window.fetch = jest.fn().mockResolvedValue({
        text: () =>
          Promise.resolve(
            '<?xml version="1.0" encoding="utf-8"?><testelement myattr="myattrvalue">myelementvalue</testelement>',
          ),
        headers: expectedHeaders,
      });
      const spy = jest.spyOn(window, 'fetch');

      WMJSGetCapabilities(service, succes, fail, options);

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(
          expect.stringContaining(
            `http://localhost/${service}service=WMS&request=GetCapabilities`,
          ),
          { headers: expectedHeaders, method: 'GET', mode: 'cors' },
        );
        expect(succes).toHaveBeenCalledWith({
          testelement: {
            attr: { myattr: 'myattrvalue' },
            value: 'myelementvalue',
          },
        });
        expect(fail).not.toHaveBeenCalled();
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });
  });

  it('should load capabilities via proxy when WMXMLParser fails', async () => {
    const service = 'https://testservice.com/wms';
    const succes = jest.fn();
    const fail = jest.fn();
    const xml2jsonrequestURL = 'xmltesturl';
    WMServiceStoreXML2JSONRequest.proxy = xml2jsonrequestURL;
    const options = {
      headers: [{ name: 'content-type', value: 'application/xml' }],
    };
    const expectedHeaders = new Headers();
    expectedHeaders.append(options.headers[0].name, options.headers[0].value);

    window.fetch = jest
      .fn()
      // mock the WMXMLParser fetch with a wrong response (without headers)
      .mockResolvedValueOnce({
        text: () => Promise.resolve({ data: {} }),
      })
      // mock the loadGetCapabilitiesViaProxy fetch
      .mockResolvedValueOnce({ text: () => Promise.resolve('succes') });
    const spy = jest.spyOn(window, 'fetch');

    WMJSGetCapabilities(service, succes, fail, options);

    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith(
        expect.stringContaining(
          `${service}?service=WMS&request=GetCapabilities`,
        ),
        { headers: expectedHeaders, method: 'GET', mode: 'cors' },
      );
      expect(fail).not.toHaveBeenCalled();
      expect(spy).toHaveBeenCalledTimes(2);
      expect(succes).toHaveBeenLastCalledWith('succes');
      expect(spy).toHaveBeenLastCalledWith(
        expect.stringContaining(
          `${xml2jsonrequestURL}request=https%3A%2F%2Ftestservice.com%2Fwms%3Fservice%3DWMS%26request%3DGetCapabilities`,
        ),
        {},
      );
    });
  });

  it('should call fail when both loading capabilities via WMXMLParser and via proxy fail', async () => {
    const service = 'https://testservice.com/wms';
    const succes = jest.fn();
    const fail = jest.fn();
    const options = {
      headers: [{ name: 'content-type', value: 'application/xml' }],
    };
    const expectedHeaders = new Headers();
    expectedHeaders.append(options.headers[0].name, options.headers[0].value);

    window.fetch = jest
      .fn()
      // mock the WMXMLParser fetch with a failure
      .mockRejectedValueOnce({
        text: () => Promise.reject(new Error('fail')),
      })
      // mock the loadGetCapabilitiesViaProxy fetch with a failure
      .mockRejectedValueOnce({
        text: () => Promise.reject(new Error('fail')),
      });
    const spy = jest.spyOn(window, 'fetch');

    WMJSGetCapabilities(service, succes, fail, options);

    await waitFor(() => {
      expect(spy).toHaveBeenCalledTimes(2);
      expect(fail).toHaveBeenCalledWith(
        expect.stringContaining(
          `Request failed for ${service}?service=WMS&request=GetCapabilities`,
        ),
      );
      expect(succes).not.toHaveBeenCalled();
    });
  });

  describe('sortByKey', () => {
    it('should do nothing on an empty array', () => {
      const array = [];
      sortByKey(array, 'text');
      expect(array).toEqual([]);
    });
    it('should do nothing on an array with one value', () => {
      const array = [{ text: 'string', leaf: true }];
      sortByKey(array, 'text');
      expect(array).toEqual([{ text: 'string', leaf: true }]);
    });
    it('should do nothing when given key does not exist', () => {
      const array = [
        { text: 'a', leaf: true },
        { text: 'b', leaf: true },
      ];
      sortByKey(array, 'hello');
      expect(array).toEqual([
        { text: 'a', leaf: true },
        { text: 'b', leaf: true },
      ]);
    });
    it('should do nothing on an array with two values that is already sorted alphabetically', () => {
      const array = [
        { text: 'a', leaf: true },
        { text: 'b', leaf: true },
      ];
      sortByKey(array, 'text');
      expect(array).toEqual([
        { text: 'a', leaf: true },
        { text: 'b', leaf: true },
      ]);
    });
    it('should sort on alphabet by given key', () => {
      const array = [
        { text: 'b', leaf: true, name: 'aa' },
        { text: 'a', leaf: false, name: 'cc' },
        { text: 'c', leaf: false, name: 'bb' },
        { text: 'd', leaf: false, name: 'dd' },
      ];
      sortByKey(array, 'text');
      expect(array).toEqual([
        { text: 'a', leaf: false, name: 'cc' },
        { text: 'b', leaf: true, name: 'aa' },
        { text: 'c', leaf: false, name: 'bb' },
        { text: 'd', leaf: false, name: 'dd' },
      ]);
      sortByKey(array, 'name');
      expect(array).toEqual([
        { text: 'b', leaf: true, name: 'aa' },
        { text: 'c', leaf: false, name: 'bb' },
        { text: 'a', leaf: false, name: 'cc' },
        { text: 'd', leaf: false, name: 'dd' },
      ]);
    });
  });

  describe('recursivelyFindLayer', () => {
    it('should not do anything when no layers', () => {
      const layers = [];
      const rootNode = [{}];
      const path = [];
      recursivelyFindLayer(layers, rootNode, path);
      expect(rootNode).toEqual([{}]);
    });
    it('should add given layers to the rootNode', () => {
      const layers = [
        {
          Name: { value: 'nameWithoutChildren' },
          Title: { value: 'titleWithouthChildren' },
        },
        {
          Name: { value: 'nameWithChildren' },
          Title: { value: 'titleWithChildren' },
          Layer: [
            {
              Name: { value: 'layer1' },
              Title: { value: 'title1' },
              Layer: [
                {
                  Name: { value: 'subLayer1' },
                  Title: { value: 'subTitle1' },
                },
              ],
            },
            {
              Name: { value: 'layer2' },
              Title: { value: 'title2' },
            },
          ],
        },
      ];
      const rootNode = [
        {
          leaf: true,
          name: 'alreadyhere',
          path: ['test'],
          text: 'Zlayer',
          keywords: ['test'],
          abstract: 'test',
        },
      ];
      const path = ['test'];
      recursivelyFindLayer(layers, rootNode, path);
      expect(rootNode).toEqual([
        {
          leaf: true,
          name: 'alreadyhere',
          path: ['test'],
          text: 'Zlayer',
          keywords: ['test'],
          abstract: 'test',
        },
        {
          leaf: false,
          name: 'nameWithChildren',
          path: ['test'],
          styles: [],
          text: 'titleWithChildren',
          keywords: [],
          abstract: undefined,
          children: [
            {
              leaf: false,
              name: 'layer1',
              path: ['test', 'titleWithChildren'],
              styles: [],
              text: 'title1',
              keywords: [],
              abstract: undefined,
              children: [
                {
                  leaf: true,
                  name: 'subLayer1',
                  path: ['test', 'titleWithChildren', 'title1'],
                  styles: [],
                  text: 'subTitle1',
                  keywords: [],
                  abstract: undefined,
                },
              ],
            },
            {
              leaf: true,
              name: 'layer2',
              path: ['test', 'titleWithChildren'],
              styles: [],
              text: 'title2',
              keywords: [],
              abstract: undefined,
            },
          ],
        },
        {
          leaf: true,
          name: 'nameWithoutChildren',
          styles: [],
          path: ['test'],
          text: 'titleWithouthChildren',
          keywords: [],
          abstract: undefined,
        },
      ]);
    });

    it('should handle layers with empty names and titles', () => {
      const layers = [
        {
          Name: null,
          Title: null,
        },
        {
          Name: null,
          Title: { value: 'title' },
        },
      ];
      const rootNode = [];
      const path = [];
      recursivelyFindLayer(layers, rootNode, path);
      expect(rootNode).toEqual([
        { leaf: false, styles: [], text: 'Layer' },
        { leaf: false, styles: [], text: 'title' },
      ]);
    });
  });

  describe('checkVersion', () => {
    it('should fail when no version data available', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {};
      expect(wmjsService.checkVersion).toBeTruthy();
      expect(() => wmjsService.checkVersion(jsonData)).toThrowError(
        new Error('Unable to determine WMS version'),
      );
    });
    it('should return version 1.1.1 when version is 1.0.0', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {
        WMT_MS_Capabilities: { attr: { version: WMSVersion.version100 } },
      };

      expect(wmjsService.checkVersion(jsonData)).toEqual(WMSVersion.version111);
    });

    it('should return version 1.1.1 when version is 1.1.1', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {
        WMT_MS_Capabilities: {
          attr: { version: WMSVersion.version111 },
          Capability: { Layer: {} },
        },
      };

      expect(wmjsService.checkVersion(jsonData)).toEqual(WMSVersion.version111);
    });

    it('should return version 1.3.0 when version is 1.3.0', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {
        WMS_Capabilities: {
          attr: { version: WMSVersion.version130 },
          Capability: { Layer: {} },
        },
      };

      expect(wmjsService.checkVersion(jsonData)).toEqual(WMSVersion.version130);
    });
    it('should fail when version 1.3.0 has no Capability', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {
        WMS_Capabilities: {
          attr: { version: WMSVersion.version130 },
        },
      };
      expect(() => wmjsService.checkVersion(jsonData)).toThrowError(
        new Error('No WMS Capability element found'),
      );
    });

    it('should fail when version 1.1.1 has no Capability', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {
        WMT_MS_Capabilities: {
          attr: { version: WMSVersion.version111 },
        },
      };

      expect(() => wmjsService.checkVersion(jsonData)).toThrowError(
        new Error('No WMS Capability element found'),
      );
    });
    it('should fail when version 1.3.0 Capability has no Layer', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {
        WMS_Capabilities: {
          attr: { version: WMSVersion.version130 },
          Capability: {},
        },
      };
      expect(() => wmjsService.checkVersion(jsonData)).toThrowError(
        new Error('No WMS Layer element found'),
      );
    });

    it('should fail when version 1.1.1 Capability has no Layer', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {
        WMT_MS_Capabilities: {
          attr: { version: WMSVersion.version111 },
          Capability: {},
        },
      };

      expect(() => wmjsService.checkVersion(jsonData)).toThrowError(
        new Error('No WMS Layer element found'),
      );
    });
    it('should fail when data contains an exception code', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {
        ServiceExceptionReport: {
          ServiceException: {
            attr: {
              code: '500',
            },
          },
        },
      };
      expect(() => wmjsService.checkVersion(jsonData)).toThrowError(
        new Error('WMS Service exception with code 500'),
      );
    });
    it('should fail when data contains an exception value', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {
        ServiceExceptionReport: {
          ServiceException: {
            attr: {
              code: '500',
            },
            value: 'Exception test',
          },
        },
      };
      expect(() => wmjsService.checkVersion(jsonData)).toThrowError(
        new Error('Exception: 500.\nException test'),
      );
    });
  });

  describe('getCapabilityElement', () => {
    it('should fail when no Capability data available', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {};
      expect(() => wmjsService.getCapabilityElement(jsonData)).toThrowError(
        new Error('No Capability element found in service'),
      );
    });

    it('should return Capability object for version 1.1.1', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {
        WMT_MS_Capabilities: {
          attr: { version: WMSVersion.version111 },
          Capability: { Layer: {} },
        },
      };
      expect(wmjsService.getCapabilityElement(jsonData)).toEqual(
        jsonData.WMT_MS_Capabilities.Capability,
      );
    });
    it('should return Capability object for version 1.3.0', () => {
      const wmjsService = new WMJSService({
        service: 'test',
      });

      const jsonData = {
        WMS_Capabilities: {
          attr: { version: WMSVersion.version130 },
          Capability: { Layer: {} },
        },
      };
      expect(wmjsService.getCapabilityElement(jsonData)).toEqual(
        jsonData.WMS_Capabilities.Capability,
      );
    });
  });

  describe('getCapabilities', () => {
    it('should call succes when capabilities are loaded', async () => {
      jest.useFakeTimers();
      const succes = jest.fn();
      const fail = jest.fn();
      const forceReload = false;
      const options = {
        headers: [{ name: 'content-type', value: 'application/xml' }],
      };
      const expectedHeaders = new Headers();
      expectedHeaders.append(options.headers[0].name, options.headers[0].value);

      const wmjsService = new WMJSService({
        service: 'https://testservice.com/wms',
      });

      // mock the WMXMLParser fetch with a succes response
      window.fetch = jest.fn().mockResolvedValue({
        text: () =>
          Promise.resolve(
            '<?xml version="1.0" encoding="utf-8"?><WMS_Capabilities version="1.3.0"><Capability><Layer/></Capability></WMS_Capabilities>',
          ),
        headers: expectedHeaders,
      });
      const spy = jest.spyOn(window, 'fetch');

      wmjsService.getCapabilities(succes, fail, forceReload, options);
      jest.runOnlyPendingTimers();

      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(succes).toHaveBeenCalledTimes(1);
        expect(fail).not.toHaveBeenCalled();
      });
      jest.clearAllTimers();
      jest.useRealTimers();
    });
    it('should call fail when capabilities could not be loaded', async () => {
      jest.useFakeTimers();
      const succes = jest.fn();
      const fail = jest.fn();
      const forceReload = false;
      const options = {
        headers: [{ name: 'content-type', value: 'application/xml' }],
      };
      const expectedHeaders = new Headers();
      expectedHeaders.append(options.headers[0].name, options.headers[0].value);

      const wmjsService = new WMJSService({
        service: 'https://testservice.com/wms',
      });

      // mock the WMXMLParser fetch with a succes response
      window.fetch = jest.fn().mockResolvedValue({
        text: () =>
          Promise.resolve(
            '<?xml version="1.0" encoding="utf-8"?><WMS_Capabilities version="1.3.0"><Capability><NotALayer/></Capability></WMS_Capabilities>',
          ),
        headers: expectedHeaders,
      });
      const spy = jest.spyOn(window, 'fetch');

      wmjsService.getCapabilities(succes, fail, forceReload, options);
      jest.runOnlyPendingTimers();

      await waitFor(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(fail).toHaveBeenCalledTimes(1);
        expect(succes).not.toHaveBeenCalled();
      });
      jest.clearAllTimers();
      jest.useRealTimers();
    });
  });
});
