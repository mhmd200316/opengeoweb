/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  getCorrectWMSDimName,
  getMapDimURL,
  isDefined,
  isNull,
  toArray,
  URLDecode,
  URLEncode,
  WMJScheckURL,
  getUriWithParam,
  dateToISO8601,
  getUriAddParam,
  addStylesForLayer,
} from './WMJSTools';
import WMLayer, { LayerType } from './WMLayer';
import {
  defaultReduxLayerRadarKNMI,
  multiDimensionLayer,
} from '../utils/testUtils';
import {
  WMDateOutSideRange,
  WMDateTooEarlyString,
  WMDateTooLateString,
} from './WMConstants';
import WMSLayerObjectForTestingWMLayerHelperFunctions from '../utils/specs/WMSLayerObjectForTestingWMLayerHelperFunctions.json';

describe('components/WMJSTools', () => {
  describe('isDefined', () => {
    it('should return false if undefined or null', () => {
      expect(isDefined(null)).toEqual(false);
      expect(isDefined(undefined)).toEqual(false);
      const undefConst = undefined;
      expect(isDefined(undefConst)).toEqual(false);
      expect(isDefined('hello')).toEqual(true);
      expect(isDefined(0)).toEqual(true);
      expect(isDefined(false)).toEqual(true);
      expect(isDefined({})).toEqual(true);
    });
  });

  describe('isNull', () => {
    it('should return true null', () => {
      expect(isNull(null)).toEqual(true);
      expect(isNull(undefined)).toEqual(false);
      expect(isNull('hello')).toEqual(false);
      expect(isNull(0)).toEqual(false);
      expect(isNull(false)).toEqual(false);
      expect(isNull({})).toEqual(false);
    });
  });

  describe('toArray', () => {
    it('should return as first element in array', () => {
      expect(toArray(12)).toEqual([12]);
      expect(toArray(0)).toEqual([0]);
      expect(toArray('12')).toEqual(['12']);
      expect(toArray(false)).toEqual([false]);
      expect(toArray(true)).toEqual([true]);
      expect(toArray({ weight: 'kg' })).toEqual([{ weight: 'kg' }]);
    });
    it('should return passed in variable if already array', () => {
      expect(toArray(['12'])).toEqual(['12']);
      expect(toArray([10, 15, 20])).toEqual([10, 15, 20]);
    });
    it('should return empty array if null/undefined passed', () => {
      expect(toArray(null)).toEqual([]);
      expect(toArray(undefined)).toEqual([]);
    });
  });

  describe('WMJScheckURL', () => {
    it('return url + ? if no ? found in string otherwise return the original (trimmed) url', () => {
      expect(WMJScheckURL('someurl.nl')).toEqual('someurl.nl?');
      expect(WMJScheckURL('someur//--lverylong.nl  ')).toEqual(
        'someur//--lverylong.nl?',
      );
      expect(WMJScheckURL('')).toEqual('?');
      expect(WMJScheckURL(null)).toEqual('?');
      expect(WMJScheckURL(undefined)).toEqual('?');
      expect(WMJScheckURL('?')).toEqual('?');
      expect(WMJScheckURL('someurl.nl? ')).toEqual('someurl.nl?');
    });
    it('return url + ? if no ? found in string', () => {
      expect(WMJScheckURL('someurl.nl')).toEqual('someurl.nl?');
      expect(WMJScheckURL('someur//--lverylong.nl  ')).toEqual(
        'someur//--lverylong.nl?',
      );
      expect(WMJScheckURL('')).toEqual('?');
      expect(WMJScheckURL(null)).toEqual('?');
      expect(WMJScheckURL(undefined)).toEqual('?');
    });
  });

  describe('URLDecode', () => {
    it('should return empty string if null or undefined passed', () => {
      expect(URLDecode(null)).toBe('');
      expect(URLDecode(undefined)).toBe('');
    });
    it('should return decoded string', () => {
      expect(URLDecode('www.url.com%2F')).toBe('www.url.com/');
      expect(URLDecode('www.url.com%2F')).toBe('www.url.com/');
      expect(URLDecode('www.url.com%2F%3C%3A')).toBe('www.url.com/<:');
      expect(URLDecode(undefined)).toBe('');
    });
  });

  describe('URLEncode', () => {
    it('should return passed parameter if undefined/""/no string is passed', () => {
      expect(URLEncode(null)).toBe(null);
      expect(URLEncode(undefined)).toBe(undefined);
      expect(URLEncode('')).toBe('');
    });
    it('should keep safe chars and replace unsafe chars', () => {
      expect(
        URLEncode(
          'www.url.com/RADNL_OPER_R___25PCPRR_L3.cgi&DIM_flight level=625&elevation=9000&time=2020-03-13T14-40-00Z',
        ),
      ).toBe(
        'www.url.com%2FRADNL_OPER_R___25PCPRR_L3.cgi%26DIM_flight%20level%3D625%26elevation%3D9000%26time%3D2020-03-13T14-40-00Z',
      );
      expect(URLEncode('www.encode.nl/ >?/')).toBe(
        'www.encode.nl%2F%20%3E%3F%2F',
      );
      expect(URLEncode('www.encode.nl/+=/^%&(')).toBe(
        'www.encode.nl%2F%2B%3D%2F%5E%25%26(',
      );
    });
    it('should replace chars with code > 255 with a +  ', () => {
      expect(URLEncode('www.encode.nl/œ')).toBe('www.encode.nl%2F+');
    });
    it('should replace chars with code < 255 with their ISO-8859-15 code', () => {
      expect(URLEncode('www.encode.nl/þ')).toBe('www.encode.nl%2F%FE');
    });
  });

  describe('getCorrectWMSDimName', () => {
    it('should return DIM_ passed in parameter unless time or elevation passed and always convert to upper case', () => {
      expect(getCorrectWMSDimName('time')).toEqual('TIME');
      expect(getCorrectWMSDimName('elevation')).toEqual('ELEVATION');
      expect(getCorrectWMSDimName('otherdimension')).toEqual(
        'DIM_OTHERDIMENSION',
      );
      expect(getCorrectWMSDimName('timee')).toEqual('DIM_TIMEE');
    });
  });

  describe('getMapDimURL', () => {
    it('should the dimension urls for all passed layer dimensions and convert dimension names to upper case', () => {
      const layer = new WMLayer(multiDimensionLayer);
      expect(getMapDimURL(layer)).toEqual(
        '&DIM_FLIGHT LEVEL=625&ELEVATION=9000&TIME=2020-03-13T14%3A40%3A00Z',
      );
      const layer2 = new WMLayer(defaultReduxLayerRadarKNMI);
      expect(getMapDimURL(layer2)).toEqual('&TIME=2020-03-13T13%3A30%3A00Z');
    });
    it('should throw an error if current value is too late/early or outide of the range', () => {
      const layer = new WMLayer({
        service: 'https://testservice',
        name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        format: 'image/png',
        style: 'knmiradar/nearest',
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: WMDateOutSideRange,
          },
        ],
        id: 'layerid_2',
      });
      expect(() => getMapDimURL(layer)).toThrow(WMDateOutSideRange);
      layer.dimensions[0].setValue(WMDateTooEarlyString);
      expect(() => getMapDimURL(layer)).toThrow(WMDateOutSideRange);
      layer.dimensions[0].setValue(WMDateTooLateString);
      expect(() => getMapDimURL(layer)).toThrow(WMDateOutSideRange);
    });
  });

  describe('getUriWithParam', () => {
    it('should parse the url and set change the key value pair', () => {
      const myQueryString =
        'http://abcd.efg/hijklm?parameterA=ShouldChangeThis&parameterB=ThisShouldBeLeftUnchanged';
      const parsedQueryString = getUriWithParam(myQueryString, {
        parameterA: 'MyNewValue',
        parameterC: 'AddedValue',
        parameterD: undefined,
      });

      expect(parsedQueryString).toBe(
        'http://abcd.efg/hijklm?parameterA=MyNewValue&parameterB=ThisShouldBeLeftUnchanged&parameterC=AddedValue',
      );
    });
  });

  describe('getUriAddParam', () => {
    it('should parse the url and set any additional parameters not yet set in the url', () => {
      const myQueryString =
        'http://abcd.efg/hijklm?parameterA=ThisShouldBeLeftUnchanged&parameterB=ThisShouldBeLeftUnchanged';
      const parsedQueryString = getUriAddParam(myQueryString, {
        parameterA: 'MyNewValue',
        parameterC: 'AddedValue',
        parameterD: undefined,
      });

      expect(parsedQueryString).toBe(
        'http://abcd.efg/hijklm?parameterA=ThisShouldBeLeftUnchanged&parameterB=ThisShouldBeLeftUnchanged&parameterC=AddedValue',
      );
    });
    it('should add parameters if none set', () => {
      const myQueryString = 'http://abcd.efg/hijklm';
      const parsedQueryString = getUriAddParam(myQueryString, {
        parameterA: 'MyNewValue',
        parameterC: 'AddedValue',
        parameterD: undefined,
      });

      expect(parsedQueryString).toBe(
        'http://abcd.efg/hijklm?parameterA=MyNewValue&parameterC=AddedValue',
      );
    });
    it('should be case insensitive', () => {
      const myQueryString =
        'http://abcd.efg/hijklm?parameterA=ThisShouldBeLeftUnchanged&PARAMETERB=ThisShouldBeLeftUnchanged&parameterc=ShouldNotTouched';
      const parsedQueryString = getUriAddParam(myQueryString, {
        PARAMETERa: 'MyNewValue',
        parameterB: 'AddedValue',
        parameterD: undefined,
      });

      expect(parsedQueryString).toBe(
        'http://abcd.efg/hijklm?parameterA=ThisShouldBeLeftUnchanged&PARAMETERB=ThisShouldBeLeftUnchanged&parameterc=ShouldNotTouched',
      );
    });
  });

  describe('dateToISO8601', () => {
    it('should convert a date to an ISO8601 string', () => {
      const myJSDate = new Date('2021-05-26T01:23:45Z');
      const iso8601String = dateToISO8601(myJSDate);
      expect(iso8601String).toBe('2021-05-26T01:23:45Z');
    });
  });

  describe('addStylesForLayer', () => {
    it('Check addStylesForLayer helper function', () => {
      const layer = new WMLayer();

      layer.styles = addStylesForLayer(
        WMSLayerObjectForTestingWMLayerHelperFunctions[0],
      );
      expect(layer.getStyles().length).toEqual(6);
      expect(layer.getStyles()[0].name).toEqual('radar/nearest');
      expect(layer.getStyles()[0].title).toEqual('radar/nearest');
      expect(layer.getStyles()[0].abstract).toEqual('No abstract available');
      expect(layer.getStyles()[0].legendURL).toBeDefined();

      const styleObject = layer.getStyleObject(
        'precip-blue-transparent/nearest',
        0,
      );
      expect(styleObject.name).toEqual('precip-blue-transparent/nearest');
      expect(styleObject.title).toEqual('Title for this style precip blue');
      expect(styleObject.abstract).toEqual(
        'Abstract for this style precip blue',
      );

      expect(layer.currentStyle).toBe('');
    });
  });
});
