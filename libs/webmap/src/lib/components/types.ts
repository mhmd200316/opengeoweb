/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import type WMJSDimension from './WMJSDimension';
import type WMLayer from './WMLayer';
import type { DateInterval } from './WMTime';

export type WMPosition = {
  x: number;
  y: number;
};

export type LinkedInfoContent = {
  layer: WMLayer;
  message?: string;
};

export type LinkedInfo = {
  linkedInfo: LinkedInfoContent;
};

export type Dimension = {
  name?: string;
  units?: string;
  currentValue: string;
  maxValue?: string;
  minValue?: string;
  timeInterval?: DateInterval;
  synced?: boolean;
  values?: string;
};

export interface Style {
  title: string;
  name: string;
  legendURL: string;
  abstract: string;
}

export type WMLayerType = {
  setDimension: (
    name: string,
    value: string,
    updateMapDimensions: boolean,
  ) => void;
  getDimension: (name: string) => WMJSDimension;
  dimensions?: WMJSDimension[];
  title?: string;
  headers?: Headers[];
};

/* From WMS GetCapabilties */
/**
 * This interface is for reading from the WMS GetCapabilties document, which can have different structures for V1.1.1 and V1.3.0
 */
export interface WMSLayerFromGetCapabilities {
  Dimension: unknown /* Dimension element in the WMS GetCapabilties document, can be single object or array */;
  Style: unknown /* Style element in the WMS GetCapabilties document, can be single object or array */;
  Extent: unknown /* Extent element in the WMS GetCapabilties document, can be single object or array */;
}
