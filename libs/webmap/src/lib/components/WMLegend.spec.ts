/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import WMLayer, { LayerType } from './WMLayer';
import { legendImageStoreLength } from './WMConstants';
import {
  getLegendGraphicURLForLayer,
  legendImageStore,
  getLegendImageStore,
  drawLegend,
} from './WMLegend';
import WMJSDimension from './WMJSDimension';

describe('components/WMLegend', () => {
  describe('legendImageStore', () => {
    it('should have max number of images defined by the right constant', () => {
      expect(legendImageStore._maxNumberOfImages).toEqual(
        legendImageStoreLength,
      );
    });

    it('should get the image store', () => {
      expect(getLegendImageStore()).toEqual(legendImageStore);
    });
  });

  describe('drawLegend', () => {
    it('should not draw a legend when no layers given', () => {
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d');
      drawLegend(context, 200, 300, []);
      expect(context.fillStyle).toEqual('#000000');
    });
    it('should start loading and draw the legend image when layer is enabled', () => {
      const testlayer = new WMLayer({
        id: 'testlayer',
        name: 'testlayer',
        service: 'https://testurl.com',
        layerType: LayerType.mapLayer,
        enabled: true,
      });
      testlayer.legendGraphic = 'https://testurl.com';
      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d');
      drawLegend(context, 200, 300, [testlayer]);
      expect(context.fillStyle).toEqual('#000000');
      expect(context.fillStyle).toEqual('#000000');
      // draw the legend again
      drawLegend(context, 200, 300, [testlayer]);
      // now legend should be drawn, cannot actually check the canvas image so checking the background style instead
      expect(context.fillStyle).toEqual('#ffffff');
    });
  });

  describe('getLegendGraphicURLForLayer', () => {
    it('should return undefined if layer has no legend graphic', () => {
      expect(getLegendGraphicURLForLayer(null)).toBeUndefined();
      const testlayer = new WMLayer();
      expect(getLegendGraphicURLForLayer(testlayer)).toBeUndefined();
    });
    it('should add the default width and height when not present', () => {
      const testlayer = new WMLayer({
        id: 'testlayer',
        name: 'testlayer',
        service: 'https://geoservices.knmi.nl/wms',
        layerType: LayerType.mapLayer,
      });
      testlayer.legendGraphic =
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest';
      expect(getLegendGraphicURLForLayer(testlayer)).toEqual(
        `${testlayer.legendGraphic}&layers=testlayer&&&transparent=true&&width=250&height=250&`,
      );
    });
    it('should not add the default width and height when present', () => {
      const testlayer = new WMLayer({
        id: 'testlayer',
        name: 'testlayer',
        service: 'https://geoservices.knmi.nl/wms',
        layerType: LayerType.mapLayer,
      });
      testlayer.legendGraphic =
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest&width=40&height=90';
      expect(getLegendGraphicURLForLayer(testlayer)).toEqual(
        `${testlayer.legendGraphic}&layers=testlayer&&&transparent=true&`,
      );
    });
    it('should add the sldurl when present', () => {
      const testlayer = new WMLayer({
        id: 'testlayer',
        name: 'testlayer',
        service: 'https://geoservices.knmi.nl/wms',
        layerType: LayerType.mapLayer,
      });
      testlayer.legendGraphic =
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest';
      testlayer.sldURL = 'sld-url-test';
      expect(getLegendGraphicURLForLayer(testlayer)).toEqual(
        `${testlayer.legendGraphic}&layers=testlayer&&&SLD=sld-url-test&transparent=true&&width=250&height=250&`,
      );
    });
    it('should not add the layers when already present', () => {
      const testlayer = new WMLayer({
        id: 'testlayer',
        name: 'testlayer',
        service: 'https://geoservices.knmi.nl/wms',
        layerType: LayerType.mapLayer,
      });
      testlayer.legendGraphic =
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest&layers=testlayer';
      expect(getLegendGraphicURLForLayer(testlayer)).toEqual(
        `${testlayer.legendGraphic}&&transparent=true&&width=250&height=250&`,
      );
    });
    it('should return undefined on error', () => {
      const testlayer = new WMLayer({
        id: 'testlayer',
        name: 'testlayer',
        service: 'https://geoservices.knmi.nl/wms',
        layerType: LayerType.mapLayer,
      });
      testlayer.legendGraphic =
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest';
      // adding an empty dimension will make it throw an error
      testlayer.dimensions = [new WMJSDimension()];
      expect(getLegendGraphicURLForLayer(testlayer)).toBeUndefined();
    });
  });
});
