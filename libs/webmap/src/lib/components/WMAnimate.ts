/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */
import moment from 'moment';
import { debug, DebugType } from './WMJSTools';
import WMTimer from './WMTimer';
import { getMapImageStore } from './WMImageStore';
import { getAlternativeImage } from './WMCanvasBuffer';
import type WMJSMap from './WMJSMap';

/**
 * Prefetches given requests
 *
 * @param requests An array of requests to prefetch
 * @return The list of images in prefetch
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const prefetch = (requests: any[]): any[] => {
  const prefetching = [];
  for (let j = 0; j < requests.length; j += 1) {
    const image = getMapImageStore.getImage(requests[j].url, {
      headers: requests[j].headers,
    });
    if (image.isLoaded() === false && image.isLoading() === false) {
      prefetching.push(image);
      image.load();
    }
  }
  return prefetching;
};

type AnimationStep = { name: string; value: string };

export default class WMJSAnimate {
  private _map: WMJSMap;

  private _divAnimationInfo: HTMLElement;

  private _callBack: { triggerEvent: (ev, val?) => void };

  private _imageStore: {
    getNumImagesLoading: () => number;
  };

  private isNextTimeCloseToWallClockTime = false;

  constructor(_map: WMJSMap) {
    _map.animationDelay = 100;
    this._callBack = _map.getListener();
    this._imageStore = getMapImageStore;
    this._divAnimationInfo = document.createElement('div');
    _map.currentAnimationStep = 0;
    _map.animationList = undefined;
    _map.isAnimating = false;
    _map.setAnimationDelay = (delay): void => {
      if (delay < 1) delay = 1;
      _map.animationDelay = delay;
    };
    this.isNextTimeCloseToWallClockTime = false;

    this._divAnimationInfo.style.zIndex = '10000';
    this._divAnimationInfo.style.background = 'none';
    this._divAnimationInfo.style.position = 'absolute';
    this._divAnimationInfo.style.border = 'none';
    this._divAnimationInfo.style.margin = '0px';
    this._divAnimationInfo.style.padding = '0px';
    this._divAnimationInfo.style.lineHeight = '14px';
    this._divAnimationInfo.style.fontFamily =
      '"Courier New", "Lucida Console", Monospace';
    this._divAnimationInfo.style.fontSize = '10px';
    _map.getBaseElement().append(this._divAnimationInfo);
    _map.isAnimatingLoopRunning = false;
    this._map = _map;

    /* Bind */
    this._animate = this._animate.bind(this);
    this._animateLoop = this._animateLoop.bind(this);
    this.checkAnimation = this.checkAnimation.bind(this);
    this.stopAnimating = this.stopAnimating.bind(this);
    _map.stopAnimating = this.stopAnimating;
    _map.checkAnimation = this.checkAnimation;
  }

  _isCurrentAnimationTimeCloseToWallClockTime(): boolean {
    if (this.isNextTimeCloseToWallClockTime) {
      this.isNextTimeCloseToWallClockTime = false;
      return true;
    }

    const currentAnimationStep = this._map.animationList[
      this._map.currentAnimationStep
    ] as AnimationStep;

    const nextAnimationStep = this._map.animationList[
      this._map.currentAnimationStep + 1 >= this._map.animationList.length
        ? 0
        : this._map.currentAnimationStep + 1
    ] as AnimationStep;

    const wallClockTime = moment.utc();
    const currentAnimationTime = moment.utc(currentAnimationStep.value);
    const nextAnimationTime = moment.utc(nextAnimationStep.value);

    if (
      wallClockTime.isBetween(
        currentAnimationTime,
        nextAnimationTime,
        undefined,
        '[)',
      )
    ) {
      const timeDiff1 = moment
        .duration(currentAnimationTime.clone().diff(wallClockTime))
        .asMinutes();

      const timeDiff2 = moment
        .duration(nextAnimationTime.clone().diff(wallClockTime))
        .asMinutes();

      if (Math.abs(timeDiff1) < Math.abs(timeDiff2)) {
        // Wall clock time is closer to current animation step time than to the next animation step time
        this.isNextTimeCloseToWallClockTime = false;
        return true;
      }

      // Wall clock time is closer to next animation step time than to the current animation step time
      this.isNextTimeCloseToWallClockTime = true;
    }

    return false;
  }

  _animate(): void {
    if (this._map.isAnimating === false) return;
    if (this._map.animateBusy === true) return;

    const animationStep = this._map.animationList[
      this._map.currentAnimationStep
    ] as AnimationStep;
    if (!animationStep) {
      debug(
        DebugType.Error,
        `No animation step for ${this._map.currentAnimationStep}`,
      );
      return;
    }
    this._map.setDimension(animationStep.name, animationStep.value, false);
    this._callBack.triggerEvent('ondimchange');
    this._callBack.triggerEvent('onnextanimationstep', this._map);
    this._map._pdraw();
    this._map.animateBusy = false;
  }

  _animateLoop(): void {
    if (this._map.isAnimating === false) {
      this._map.isAnimatingLoopRunning = false;
      return;
    }

    let { animationDelay } = this._map;
    if (this._map.currentAnimationStep === 0) {
      animationDelay *= 3;
    } else if (
      this._map.currentAnimationStep ===
      this._map.animationList.length - 1
    ) {
      animationDelay *= 5;
    } else if (this._isCurrentAnimationTimeCloseToWallClockTime()) {
      animationDelay *= 5;
    }
    this._map.animationTimer.init(animationDelay, this._animateLoop);
    this.checkAnimation();

    if (this._map.mouseHoverAnimationBox === false) {
      this._animate();

      let nextStep = this._map.currentAnimationStep + 1;
      if (nextStep >= this._map.animationList.length) {
        nextStep = 0;
      }

      let continueAnimation = false;
      let numReady = 0;

      let animationStep = this._map.animationList[nextStep] as AnimationStep;
      this._map.setDimension(animationStep.name, animationStep.value, false);
      this._map.animationList[nextStep].requests = this._map.getWMSRequests();
      animationStep = this._map.animationList[this._map.currentAnimationStep];
      this._map.setDimension(animationStep.name, animationStep.value, false);
      for (
        let i = 0;
        i < this._map.animationList[nextStep].requests.length;
        i += 1
      ) {
        const { url } = this._map.animationList[nextStep].requests[i];
        const image = getMapImageStore.getImageForSrc(url);
        /* Get a loaded image which has no error */
        if (
          image &&
          image.isLoaded() &&
          !image.isLoading() &&
          !image.hasError()
        ) {
          numReady += 1;
        } else {
          /* Check if a similar image is available instead, then we can continue with the smoother animation */
          const im = getAlternativeImage(
            url,
            getMapImageStore,
            this._map.getDrawBBOX(),
          );
          if (im) {
            numReady += 1;
          } else if (!(image && image.isLoading())) {
            /* No alternatives and current image is not loading, so lets continue the animation */
            numReady += 1;
          }
        }
      }
      if (numReady >= this._map.animationList[nextStep].requests.length) {
        continueAnimation = true;
      }

      if (continueAnimation) {
        this._map.currentAnimationStep = nextStep;
      }
    }
  }

  checkAnimation(): void {
    if (this._map.isAnimating === false) {
      this._map.isAnimatingLoopRunning = false;
      return;
    }
    if (!this._map.animationTimer) {
      this._map.animationTimer = new WMTimer();
    }
    if (this._map.mouseHoverAnimationBox === false) {
      const maxSimultaneousLoads = 4;
      let getNumImagesLoading = this._imageStore.getNumImagesLoading();
      if (getNumImagesLoading < maxSimultaneousLoads) {
        const numberPreCacheSteps = this._map.animationList.length;
        if (this._map.animationList.length > 0) {
          for (let j = 0; j < numberPreCacheSteps; j += 1) {
            let index = j + this._map.currentAnimationStep;
            while (index < 0) index += this._map.animationList.length;
            while (index >= this._map.animationList.length)
              index -= this._map.animationList.length;
            if (index < 0) index = 0;

            if (index >= 0) {
              let animationStep = this._map.animationList[
                index
              ] as AnimationStep;

              this._map.setDimension(
                animationStep.name,
                animationStep.value,
                false,
              );
              this._map.animationList[index].requests =
                this._map.getWMSRequests();

              animationStep =
                this._map.animationList[this._map.currentAnimationStep];

              this._map.setDimension(
                animationStep.name,
                animationStep.value,
                false,
              );

              this._map.animationList[index].imagesInPrefetch = prefetch(
                this._map.animationList[index].requests,
              );

              getNumImagesLoading = this._imageStore.getNumImagesLoading();
              if (getNumImagesLoading > maxSimultaneousLoads - 1) {
                break;
              }
            }
          }
        }
      }
    }

    if (this._map.isAnimatingLoopRunning === false) {
      this._map.isAnimatingLoopRunning = true;
      this._animateLoop();
    }
  }

  stopAnimating(): void {
    if (this._map.isAnimating === false) return;
    this._map._animationList = undefined;
    this._divAnimationInfo.style.display = 'none';
    this._map.isAnimating = false;
    this._map.animateBusy = false;
    this._map.rebuildMapDimensions();
    this._callBack.triggerEvent('onstopanimation', this._map);
  }
}
