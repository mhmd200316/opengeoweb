/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { waitFor } from '@testing-library/react';
import WMXMLParser, {
  WMXMLStringToJson,
  WMXMLStringToXMLDocument,
  WMXMLtoJSON,
} from './WMXMLParser';

const xmlStr =
  '<?xml version="1.0" encoding="utf-8"?>' +
  '<playlist name="mylist" xml:lang="en">' +
  '<song>' +
  '    <title>Little Fluffy Clouds</title>' +
  '    <artist>the Orb</artist>' +
  '</song>' +
  '<song>' +
  '    <title>Goodbye mother Earth</title>' +
  '    <artist>Underworld</artist>' +
  '</song>' +
  '</playlist>';

const jsonObj = {
  playlist: {
    attr: { name: 'mylist', 'xml:lang': 'en' },
    song: [
      {
        artist: { attr: {}, value: 'the Orb' },
        attr: {},
        title: { attr: {}, value: 'Little Fluffy Clouds' },
      },
      {
        artist: { attr: {}, value: 'Underworld' },
        attr: {},
        title: { attr: {}, value: 'Goodbye mother Earth' },
      },
    ],
  },
};

describe('components/WMXMLParser', () => {
  describe('WMXMLStringToXMLDocument', () => {
    it('should parse XML string', () => {
      const xmlDoc = WMXMLStringToXMLDocument(xmlStr);
      expect(xmlDoc.documentElement.textContent).toEqual(
        '    Little Fluffy Clouds    the Orb    Goodbye mother Earth    Underworld',
      );
    });

    it('should throw an exception when an invalid XML is given', () => {
      const t = (): void => {
        WMXMLStringToXMLDocument(`${xmlStr}wrongXMLDataEnding`);
      };
      expect(t).toThrow('Error while parsing');
    });
  });
  describe('WMXMLtoJSON', () => {
    it('should parse XML string', () => {
      const xmlDoc = WMXMLStringToXMLDocument(xmlStr);
      const jsonDoc = WMXMLtoJSON(xmlDoc);
      expect(jsonDoc).toEqual(jsonObj);
    });
  });
  describe('WMXMLStringToJson', () => {
    it('should parse XML string', () => {
      const jsonDoc = WMXMLStringToJson(xmlStr);
      expect(jsonDoc).toEqual(jsonObj);
    });

    it('should throw an exception when an invalid XML is given', () => {
      const t = (): void => {
        WMXMLStringToJson(`${xmlStr}wrongXMLDataEnding`);
      };
      expect(t).toThrow('Error while parsing');
    });
  });

  describe('WMXMLParser', () => {
    it('should fetch XML string and resolve to json', async () => {
      // mock the WMXMLParser fetch with a succes response
      const headers = [{ name: 'content-type', value: 'application/xml' }];

      const expectedHeaders = new Headers();

      expectedHeaders.append(headers[0].name, headers[0].value);

      // mock the fetch functionality
      window.fetch = jest.fn().mockResolvedValue({
        text: () => Promise.resolve(xmlStr),
        headers: expectedHeaders,
      });

      const spy = jest.spyOn(window, 'fetch');

      const data = await WMXMLParser('fakeurl', headers);
      expect(data).toEqual(jsonObj);

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(expect.stringContaining('fakeurl'), {
          headers: expectedHeaders,
          method: 'GET',
          mode: 'cors',
        });
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });

    it('should fetch invalid XML string and reject', async () => {
      // mock the WMXMLParser fetch with a succes response
      const headers = [{ name: 'content-type', value: 'application/xml' }];

      const expectedHeaders = new Headers();

      expectedHeaders.append(headers[0].name, headers[0].value);

      // mock the fetch functionality
      window.fetch = jest.fn().mockResolvedValue({
        text: () => Promise.resolve(`${xmlStr}wrongXMLDataEnding`),
        headers: expectedHeaders,
      });

      const spy = jest.spyOn(window, 'fetch');

      await expect(WMXMLParser('fakeurl', headers)).rejects.toThrow(
        'Error while parsing',
      );

      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith(expect.stringContaining('fakeurl'), {
          headers: expectedHeaders,
          method: 'GET',
          mode: 'cors',
        });
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });
  });
});
