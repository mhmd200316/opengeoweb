/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { WMJSMap, WMLayer } from '.';
import { WMSVersion } from './WMConstants';
import { getBBOXandProjString, buildWMSGetMapRequest } from './WMJSMapHelpers';

describe('components/WMJSMapHelpers', () => {
  describe('getBBOXandProjString', () => {
    it('should return BBOX and ProjString from a map', () => {
      const mockBaseElement = document.createElement('div');
      mockBaseElement.addEventListener = jest.fn();
      mockBaseElement.appendChild = jest.fn();
      const map = new WMJSMap(mockBaseElement);
      map.setBBOX(1, 2, 3, 4);
      map.setProjection('EPSG:4326');
      const layer = new WMLayer();
      layer.version = WMSVersion.version111;
      layer.wms130bboxcompatibilitymode = false;
      expect(getBBOXandProjString(map, layer)).toEqual(
        'SRS=EPSG%3A4326&BBOX=1,2,3,4&',
      );

      layer.version = WMSVersion.version130;
      expect(getBBOXandProjString(map, layer)).toEqual(
        'CRS=EPSG%3A4326&BBOX=2,1,4,3&',
      );

      layer.wms130bboxcompatibilitymode = true;
      expect(getBBOXandProjString(map, layer)).toEqual(
        'CRS=EPSG%3A4326&BBOX=1,2,3,4&',
      );
    });
  });

  describe('buildWMSGetMapRequest', () => {
    it('should return a GetMap url from buildWMSGetMapRequest for standard projection', () => {
      const mockBaseElement = document.createElement('div');
      mockBaseElement.addEventListener = jest.fn();
      mockBaseElement.appendChild = jest.fn();
      const map = new WMJSMap(mockBaseElement);
      map.setBBOX(1, 2, 3, 4);
      map.setProjection('EPSG:4326');
      const layer = new WMLayer();
      layer.version = WMSVersion.version111;
      layer.wms130bboxcompatibilitymode = false;
      layer.name = 'test';
      layer.getmapURL = 'http://testurl.test/';
      expect(buildWMSGetMapRequest(map, layer).url).toEqual(
        'http://testurl.test/?&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&LAYERS=test&WIDTH=2&HEIGHT=2&SRS=EPSG%3A4326&BBOX=1,2,3,4&STYLES=&FORMAT=image/png&TRANSPARENT=TRUE&',
      );
    });

    it('should return a GetMap url from buildWMSGetMapRequest for Time elevation projection', () => {
      const mockBaseElement = document.createElement('div');
      mockBaseElement.addEventListener = jest.fn();
      mockBaseElement.appendChild = jest.fn();
      const map = new WMJSMap(mockBaseElement);
      map.setBBOX(1, 2, 3, 4);
      map.setProjection('GFI:TIME_ELEVATION');
      const layer = new WMLayer();
      layer.version = WMSVersion.version111;
      layer.wms130bboxcompatibilitymode = false;
      layer.name = 'test';
      layer.getmapURL = 'http://testurl.test/';
      expect(buildWMSGetMapRequest(map, layer).url).toEqual(
        'http://testurl.test/&SERVICE=WMS&REQUEST=GetFeatureInfo&VERSION=1.1.1&LAYERS=test&QUERY_LAYERS=test&BBOX=29109.947643979103,6500000,1190890.052356021,7200000&SRS=EPSG%3A3857&WIDTH=2&HEIGHT=2&X=707&Y=557&FORMAT=image/gif&INFO_FORMAT=image/png&STYLES=&time=1970-01-01T00:00:00Z/1970-01-01T00:00:00Z&elevation=2/4',
      );
    });
  });
});
