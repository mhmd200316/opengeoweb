/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import WMCanvasBuffer, {
  addAlternativeImage,
  CanvasGeoLayer,
  getSharedImagesList,
  makeQueryStringWithoutGeoInfo,
  removeAlternativeImage,
  sortByImageDate,
} from './WMCanvasBuffer';
import { getCurrentImageTime } from './WMImage';
import { getMapImageStore } from './WMImageStore';
import WMListener from './WMListener';

describe('components/WMCanvasBuffer', () => {
  it('should create class with correct properties', () => {
    const canvasBuffer = new WMCanvasBuffer(
      new WMListener(),
      getMapImageStore,
      10,
      10,
      {
        beforecanvasstartdraw: (): void => {},
        beforecanvasdisplay: (): void => {},
        canvasonerror: (): void => {},
      },
    );

    expect(canvasBuffer).toBeDefined();
  });
  it('makeQueryStringWithoutGeoInfo should remove bbox, width and height from the url', () => {
    const urlToAdjust =
      'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&LAYERS=RAD_NL25_PCP_CM&WIDTH=946&HEIGHT=900&CRS=EPSG%3A3857&BBOX=-422843.6583413242,6074810.282795019,1456153.3856114622,7862439.605794499&STYLES=radar%2Fnearest&FORMAT=image/png&TRANSPARENT=TRUE&&time=2021-05-28T09%3A00%3A00Z';
    const expectedUrl =
      'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&LAYERS=RAD_NL25_PCP_CM&WIDTH=&HEIGHT=&CRS=EPSG%3A3857&BBOX=&STYLES=radar%2Fnearest&FORMAT=image%2Fpng&TRANSPARENT=TRUE&time=2021-05-28T09%3A00%3A00Z';
    expect(makeQueryStringWithoutGeoInfo(urlToAdjust)).toBe(expectedUrl);
  });

  it('addAlternativeImage and removeAlternativeImage', () => {
    const layerAV1 = 'http://t/w?name=A&BBOX=-10,-10,10,10&WIDTH=5&HEIGHT=5';
    const layerAV2 = 'http://t/w?name=A&BBOX=-12,-12,12,12&WIDTH=5&HEIGHT=5';
    const layerBV1 = 'http://t/w?name=B&BBOX=-12,-12,12,12&WIDTH=5&HEIGHT=5';
    const layerCV1 = 'http://t/w?name=C&BBOX=-12,-12,12,12&WIDTH=5&HEIGHT=5';

    /* Add a first image and check shared images list */
    addAlternativeImage({
      imageSource: layerAV1,
      bbox: { left: -10, bottom: -10, right: 10, top: 10 },
      opacity: 1,
      imageAge: getCurrentImageTime(),
    });

    const sharedImagesList = getSharedImagesList();
    expect(Object.keys(sharedImagesList).length).toBe(1);

    const firstKey = Object.keys(sharedImagesList)[0];
    expect(sharedImagesList[firstKey].length).toBe(1);

    /* Add another image with the same layer and check shared images list */
    addAlternativeImage({
      imageSource: layerAV2,
      bbox: { left: -12, bottom: -12, right: 12, top: 12 },
      opacity: 1,
      imageAge: getCurrentImageTime(),
    });

    expect(Object.keys(sharedImagesList).length).toBe(1);
    expect(sharedImagesList[firstKey].length).toBe(2);

    /* Add another image with the different layer and check shared images list */
    addAlternativeImage({
      imageSource: layerBV1,
      bbox: { left: -12, bottom: -12, right: 12, top: 12 },
      opacity: 1,
      imageAge: getCurrentImageTime(),
    });

    expect(Object.keys(sharedImagesList).length).toBe(2);
    const secondKey = Object.keys(sharedImagesList)[1];
    expect(sharedImagesList[secondKey].length).toBe(1);

    /* Adding the same again should not do anything */
    addAlternativeImage({
      imageSource: layerBV1,
      bbox: { left: -12, bottom: -12, right: 12, top: 12 },
      opacity: 1,
      imageAge: getCurrentImageTime(),
    });

    expect(Object.keys(sharedImagesList).length).toBe(2);
    expect(sharedImagesList[firstKey].length).toBe(2);
    expect(sharedImagesList[secondKey].length).toBe(1);

    /* Now check if removal works OK */
    removeAlternativeImage(layerAV1);
    expect(Object.keys(sharedImagesList).length).toBe(2);
    expect(sharedImagesList[firstKey].length).toBe(1);

    /* Now check if removal again works OK */
    removeAlternativeImage(layerAV1);
    expect(Object.keys(sharedImagesList).length).toBe(2);
    expect(sharedImagesList[firstKey].length).toBe(1);

    /* Remove the other too, and see if the whole entry is removed from the map */
    removeAlternativeImage(layerAV2);
    expect(Object.keys(sharedImagesList).length).toBe(1);
    expect(sharedImagesList[firstKey]).toBeUndefined();

    /* Removing an image not in the sharedimages list should not give an error */
    removeAlternativeImage(layerCV1);
  });

  it('sortByOverlappingArea should sort from high to low overlapping area', () => {
    const listToSort: CanvasGeoLayer[] = [
      {
        imageSource: 'a',
        bbox: { top: 12, left: 12, bottom: 18, right: 18 },
        opacity: 1,
        imageAge: 4,
      },
      {
        imageSource: 'b',
        bbox: { top: 11, left: 11, bottom: 12, right: 12 },
        opacity: 1,
        imageAge: 3,
      },
      {
        imageSource: 'c',
        bbox: { top: 14, left: 14, bottom: 16, right: 16 },
        opacity: 1,
        imageAge: 1,
      },
      {
        imageSource: 'e',
        bbox: { top: 2, left: 2, bottom: 4, right: 4 },
        opacity: 1,
        imageAge: 1000,
      },
      {
        imageSource: 'f',
        bbox: { top: 11, left: 11, bottom: 19, right: 19 },
        opacity: 1,
        imageAge: 2323,
      },
      {
        imageSource: 'g',
        bbox: { top: 12, left: 12, bottom: 19, right: 19 },
        opacity: 1,
        imageAge: 101,
      },
      {
        imageSource: 'h',
        bbox: { top: 0, left: 0, bottom: 100, right: 100 },
        opacity: 1,
        imageAge: 423424,
      },
    ];
    const list = sortByImageDate(listToSort);

    expect(list).toEqual([
      {
        imageSource: 'h',
        bbox: { top: 0, left: 0, bottom: 100, right: 100 },
        opacity: 1,
        imageAge: 423424,
      },
      {
        imageSource: 'f',
        bbox: { top: 11, left: 11, bottom: 19, right: 19 },
        opacity: 1,
        imageAge: 2323,
      },
      {
        imageSource: 'e',
        bbox: { top: 2, left: 2, bottom: 4, right: 4 },
        opacity: 1,
        imageAge: 1000,
      },
      {
        imageSource: 'g',
        bbox: { top: 12, left: 12, bottom: 19, right: 19 },
        opacity: 1,
        imageAge: 101,
      },
      {
        imageSource: 'a',
        bbox: { top: 12, left: 12, bottom: 18, right: 18 },
        opacity: 1,
        imageAge: 4,
      },
      {
        imageSource: 'b',
        bbox: { top: 11, left: 11, bottom: 12, right: 12 },
        opacity: 1,
        imageAge: 3,
      },
      {
        imageSource: 'c',
        bbox: { top: 14, left: 14, bottom: 16, right: 16 },
        opacity: 1,
        imageAge: 1,
      },
    ]);
  });
});
