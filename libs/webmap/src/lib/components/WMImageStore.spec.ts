/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import WMImage from './WMImage';
import WMImageStore, { WMImageEventType } from './WMImageStore';

describe('components/WMImageStore', () => {
  it('should initialise with correct params', () => {
    const store = new WMImageStore(100, { id: 'test' });
    expect(store._maxNumberOfImages).toEqual(100);
    expect(store._options.id).toEqual('test');
  });

  it('should return image if src exists', () => {
    const src = 'https://testurl.com';
    const src2 = 'https://anotherurl.com';
    const store = new WMImageStore(1, { id: 'test' });
    expect(store.getImageForSrc(src)).toEqual(undefined);
    expect(store.getImage('')).toEqual(null);
    expect(store.getImage(src)).toEqual(expect.any(WMImage));
    expect(store.getImageForSrc(src)).toEqual(expect.any(WMImage));
    store.getImage(src2);
    // first one should be removed because maxNumberOfImages was set to 1
    expect(store.getImageForSrc(src)).toEqual(undefined);
    expect(store.getImageForSrc(src2)).toEqual(expect.any(WMImage));
  });

  it('should re-initialize the images on clear', () => {
    const src = 'https://testurl.com';
    const store = new WMImageStore(1, { id: 'test' });
    expect(store.getImageForSrc(src)).toEqual(undefined);
    store.getImage(src);
    store.load(src);
    expect(store.getImage(src).isLoading()).toEqual(true);
    expect(store.getNumImagesLoading()).toEqual(1);
    store.clear();
    expect(store.getImageForSrc(src).isLoading()).toEqual(false);
    expect(store.getNumImagesLoading()).toEqual(0);
  });

  it('should trigger the image callback', () => {
    const src = 'https://testservice/';
    const callback = jest.fn();
    const image = new WMImage(src, callback);
    const store = new WMImageStore(1, { id: 'test' });
    store.addImageEventCallback(callback, 'image1');
    store.imageLoadEventCallback(image, WMImageEventType.Loaded);
    expect(callback).toHaveBeenCalled();
  });

  it('should handle updates to the callback list', () => {
    const callback1 = jest.fn();
    const callback2 = jest.fn();
    const store = new WMImageStore(1, { id: 'test' });
    store.addImageEventCallback(callback1, null);
    expect(store._loadEventCallbackList).toHaveLength(0);
    store.addImageEventCallback(null, 'image1');
    expect(store._loadEventCallbackList).toHaveLength(0);
    store.addImageEventCallback(callback1, 'image1');
    store.addImageEventCallback(callback2, 'image2');
    expect(store._loadEventCallbackList).toHaveLength(2);
    store.removeEventCallback('image1');
    expect(store._loadEventCallbackList).toHaveLength(1);
    expect(store._loadEventCallbackList[0].id).toEqual('image2');
    store.removeAllEventCallbacks();
    expect(store._loadEventCallbackList).toHaveLength(0);
  });
});
