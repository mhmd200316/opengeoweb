/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { LayerType } from '../components/WMLayer';

export const defaultReduxLayerRadarKNMI = {
  service: 'https://testservice',
  name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
  title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
  format: 'image/png',
  style: 'knmiradar/nearest',
  enabled: true,
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ],
  id: 'layerid_2',
};

export const multiDimensionLayer = {
  service: 'https://testservice',
  id: 'multiDimensionLayerMock',
  name: 'MULTI_DIMENSION_LAYER',
  title: 'MULTI_DIMENSION_LAYER',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T14:40:00Z',
    },
  ],
};
