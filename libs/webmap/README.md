![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/libs/webmap/package.json)
![coverage](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg?job=test-webmap)

# webmap

Library with webmap components for the opengeoweb project.
This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test webmap` to execute the unit tests via [Jest](https://jestjs.io).
