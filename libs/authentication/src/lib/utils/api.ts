/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { createApiInstance, Credentials } from '@opengeoweb/api';

export const createApi = (
  url: string,
  appUrl: string,
  auth: Credentials,
  setAuth: (cred: Credentials) => void,
  authTokenUrl: string,
  authClientId: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): any => {
  const axiosInstance = createApiInstance(
    url,
    appUrl,
    auth,
    setAuth,
    authTokenUrl,
    authClientId,
  );

  return {
    // api data
    getBeVersion: (): Promise<{ data: string }> => {
      return axiosInstance.get('/version');
    },
  };
};

export const fakeAxiosInstance = {
  // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars, @typescript-eslint/explicit-module-boundary-types
  get: (_url, _params?): Promise<void | string> =>
    new Promise((resolve) => {
      setTimeout(() => {
        return resolve();
      }, 300);
    }),
};

export const createFakeApi =
  (): // eslint-disable-next-line @typescript-eslint/no-explicit-any
  any => {
    return {
      getBeVersion: (): Promise<{ data: { version: string } }> =>
        Promise.resolve({ data: { version: 'version 1.0.1' } }),
    };
  };
