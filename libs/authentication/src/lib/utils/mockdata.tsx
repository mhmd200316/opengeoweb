/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { ApiProvider } from '@opengeoweb/api';
import { ThemeWrapper } from '@opengeoweb/theme';
import React from 'react';
import { createFakeApi } from './api';

export const mockloginSuccess = {
  statusCode: 200,
  body: {
    access_token:
      'eyJraWQiOiJ2TDB6UzFTdVdLMitBNWRKdHVKQmJxRnYraFgyaWhEYXlIN0dLUkRHZEVFPSIsImFsZyI6IkhTMjU2In0.eyJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6Im9wZW5pZCBlbWFpbCIsImF1dGhfdGltZSI6MTYwNjEzOTczNywiZXhwIjoxNjA2MTQzMzM3LCJpYXQiOjE2MDYxMzk3MzcsInZlcnNpb24iOjIsInVzZXJuYW1lIjoibmVpbC5hcm1zdHJvbmcifQ.KcK1ZLXZ5FX-PdWAuQ9mSQoHmLOfBC_y3XEQyXGBm24',
    refresh_token:
      'eyJhbGciOiJIUzI1NiJ9.eyJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MDYxMzk3MzcsImNvZ25pdG86dXNlcm5hbWUiOiJuZWlsLmFybXN0cm9uZyIsImV4cCI6MTYwNjE0MzMzNywiaWF0IjoxNjA2MTM5NzM3LCJlbWFpbCI6Im5laWwuYXJtc3Ryb25nQGtubWkubmwifQ.aH9l2cNMIabNW55koC-SspD5xy4f2KhxfvX__2vRTKo',
    id_token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdF9oYXNoIjoiM1duLW1nMGJUWWhzblFBIiwic3ViIjoiMzg3OWMzNWUtZTMwNi01YzExMDM3NWE0NGEiLCJhdWQiOiIyb2w2M3YwZ2x1ZjlyNjdzbGF1djZ1aiIsImNvZ25pdG86Z3JvdXBzIjpbImFkbWluaXN0cmF0b3JzIl0sImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTYzMTYzNDQ3LCJpc3MiOiJodHRwczovL2NvZ25pdG8taWRwLmV1LXdlc3QtMS5hbWF6b25hd3MuY29tIiwiY29nbml0bzp1c2VybmFtZSI6Im1heC52ZXJzdGFwcGVuIiwiZXhwIjoxNjMxMjcwNDcsImlhdCI6MTYzMTIzNDQ3LCJlbWFpbCI6Im1heC52ZXJzdGFwcGVuQGZha2VlbWFpbC5ubCJ9.nuMsFX9NE1xGuGVb8qPaJ4kDX2qeTx54ui9wTQNTzhQ',
  },
};

export const mockloginSuccessGitlab = {
  statusCode: 200,
  access_token:
    'eyJraWQiOiJ2TDB6UzFTdVdLMitBNWRKdHVKQmJxRnYraFgyaWhEYXlIN0dLUkRHZEVFPSIsImFsZyI6IkhTMjU2In0.eyJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6Im9wZW5pZCBlbWFpbCIsImF1dGhfdGltZSI6MTYwNjEzOTczNywiZXhwIjoxNjA2MTQzMzM3LCJpYXQiOjE2MDYxMzk3MzcsInZlcnNpb24iOjIsInVzZXJuYW1lIjoibWF4LnZlcnN0YXBwZW4ifQ.NTkN9bZVCjnESm5YUJ5EJoCj2mhpqGNo_QXnsLIS3ew',
  refresh_token:
    'eyJhbGciOiJIUzI1NiJ9.eyJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MDYxMzk3MzcsImV4cCI6MTYwNjE0MzMzNywiaWF0IjoxNjA2MTM5NzM3LCJlbWFpbCI6Im1heC52ZXJzdGFwcGVuQGtubWkubmwifQ.q9SGXYANkAyvbYedxqaLo52EywhDsrhsWw0C7VmulJY',
  id_token:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdF9oYXNoIjoiM1duLW1nMGJUWWhzblFBIiwic3ViIjoiMzg3OWMzNWUiLCJhdWQiOiIyb2w2M3YwZ2x1ZjlyNjdzbGF1djYiLCJncm91cHNfZGlyZWN0IjpbIm9wZW5nZW93ZWIiXSwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF1dGhfdGltZSI6MTYzMTYzNDQ3LCJpc3MiOiJodHRwczovL2dpdGxhYi5jb20iLCJjb2duaXRvOnVzZXJuYW1lIjoibWF4LnZlcnN0YXBwZW4iLCJleHAiOjE2MzEyNzA0NywiaWF0IjoxNjMxMjM0NDcsImVtYWlsIjoibWF4LnZlcnN0YXBwZW5AZmFrZWVtYWlsLm5sIn0.PfgvR-zvIhvLWPBkMGPs1wiyJjhN2yni7VUecN_RqrY',
};

export const mockloginFailed = {
  statusCode: 200,
  body: {},
};

interface WrapperProps {
  children: React.ReactNode;
}

export const TestWrapper: React.FC<WrapperProps> = ({
  children,
}: WrapperProps) => {
  return (
    <ThemeWrapper>
      <ApiProvider createApi={createFakeApi}>{children}</ApiProvider>;
    </ThemeWrapper>
  );
};
