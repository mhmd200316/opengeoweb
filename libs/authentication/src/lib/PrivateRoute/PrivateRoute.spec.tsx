/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { PrivateRoute } from './PrivateRoute';
import { AuthenticationProvider } from '../AuthenticationContext';
import { AuthenticationContextProps } from '../AuthenticationContext/AuthenticationContext';

interface TestComponentProps {
  value: AuthenticationContextProps;
}

const TestComponent: React.FC<TestComponentProps> = ({
  value,
}: TestComponentProps) => {
  const Login = (): JSX.Element => <div data-testid="login">login</div>;
  const Private = (): JSX.Element => <div data-testid="private">private</div>;

  return (
    <AuthenticationProvider value={value}>
      <Router>
        <Switch>
          <PrivateRoute exact path="/">
            <Private />
          </PrivateRoute>
          <Route exact path="/login" component={Login} />
        </Switch>
      </Router>
    </AuthenticationProvider>
  );
};

describe('PrivateRoute', () => {
  it('should wrap component and show protected route when logged in', async () => {
    const { queryByTestId } = render(
      <TestComponent
        value={{ isLoggedIn: true } as AuthenticationContextProps}
      />,
    );

    expect(queryByTestId('login')).toBeFalsy();
    expect(queryByTestId('private')).toBeTruthy();
  });

  it('should wrap component and redirect to login when not logged in', async () => {
    const { queryByTestId } = render(
      <TestComponent
        value={{ isLoggedIn: false } as AuthenticationContextProps}
      />,
    );

    expect(queryByTestId('login')).toBeTruthy();
    expect(queryByTestId('private')).toBeFalsy();
  });
});
