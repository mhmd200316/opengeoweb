/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';

import { AuthenticationProvider } from '../AuthenticationContext';
import Login from './Login';

// mock window.location
const storedWindowLocation = window.location;
delete window.location;
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
window.location = {
  assign: (): void => {},
};
afterAll(() => {
  window.location = storedWindowLocation;
});

describe('Login', () => {
  it('should render successfully', () => {
    const configURLS = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };
    const spy = jest.spyOn(window.location, 'assign');
    const { baseElement } = render(
      <AuthenticationProvider configURLS={configURLS}>
        <Login />
      </AuthenticationProvider>,
    );

    expect(baseElement).toBeTruthy();
    expect(spy).toHaveBeenCalledWith(configURLS.GW_AUTH_LOGIN_URL);
  });
});
