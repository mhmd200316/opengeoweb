/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter, Switch } from 'react-router-dom';
import { AuthenticationProvider } from '../AuthenticationContext';
import Logout from './Logout';
import { getSessionStorageProvider } from '../utils/session';

// mock window.location
const storedWindowLocation = window.location;
delete window.location;
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
window.location = {
  assign: (): void => {},
};
afterAll(() => {
  window.location = storedWindowLocation;
});

describe('Logout', () => {
  it('should logout when logged in and not show an alert', () => {
    const spy = jest.spyOn(window.location, 'assign');
    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: true,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider: getSessionStorageProvider(),
    };

    const configURLS = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };

    const { baseElement, queryByText } = render(
      <AuthenticationProvider configURLS={configURLS} value={props}>
        <BrowserRouter>
          <Switch>
            <Logout />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    expect(baseElement).toBeTruthy();
    expect(props.onLogin).toHaveBeenCalledWith(false);
    expect(props.onSetAuth).toHaveBeenCalledWith(null);
    expect(spy).toHaveBeenCalledWith(configURLS.GW_AUTH_LOGOUT_URL);
    expect(queryByText('You are logged out.')).toBeFalsy();
  });

  it('should show an alert if already logged out', async () => {
    const spy = jest.spyOn(window.location, 'assign');

    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider: getSessionStorageProvider(),
    };

    const configURLS = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };

    const { baseElement, findByText } = render(
      <AuthenticationProvider configURLS={configURLS} value={props}>
        <BrowserRouter>
          <Switch>
            <Logout />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    expect(baseElement).toBeTruthy();
    expect(props.onLogin).not.toHaveBeenCalled();
    expect(props.onSetAuth).not.toHaveBeenCalled();
    expect(spy).not.toHaveBeenCalled();
    expect(await findByText('You are logged out.')).toBeTruthy();
    expect(await findByText('Home Page')).toBeTruthy();
  });
});
