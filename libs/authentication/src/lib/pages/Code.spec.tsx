/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, waitFor } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { AuthenticationProvider } from '../AuthenticationContext';
import Code from './Code';
import {
  mockloginSuccess,
  mockloginFailed,
  mockloginSuccessGitlab,
} from '../utils/mockdata';
import { getSessionStorageProvider } from '../utils/session';
import { SessionStorageKey } from '../AuthenticationContext/types';

// mocking the Redirect function to make it easy to test
jest.mock('react-router-dom', () => {
  return {
    Redirect: jest.fn(({ to }) =>
      to.pathname ? `Redirected to ${to.pathname}` : `Redirected to ${to}`,
    ),
  };
});

// make sure the tests use the actual code for Router and Switch
const { BrowserRouter, Switch } = jest.requireActual('react-router-dom');

const props = {
  onSetAuth: jest.fn(),
  isLoggedIn: false,
  onLogin: jest.fn(),
  auth: null,
  sessionStorageProvider: getSessionStorageProvider(),
};

const configURLS = {
  GW_AUTH_LOGIN_URL: '/login&state=&code_challenge=',
  GW_AUTH_TOKEN_URL: '/token',
  GW_AUTH_LOGOUT_URL: '/logout',
  GW_AUTH_CLIENT_ID: 'someID',
  GW_APP_URL: 'localhost',
};

// declare which API requests to mock
const server = setupServer(
  // capture "POST /token" requests
  rest.post(configURLS.GW_AUTH_TOKEN_URL, (req, res, ctx) => {
    // respond using a mocked JSON body
    return res(ctx.json(mockloginSuccess));
  }),
);

const storedWindowLocation = window.location;

// establish API mocking before all tests
beforeAll(() => server.listen());
beforeEach(() => {
  delete window.location;
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  window.location = new URL('http://localhost/code?code=someId&state=someId');
  // mock session storage
  jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('someId');
});
afterEach(() => {
  // reset any request handlers that are declared as a part of our tests
  server.resetHandlers();
  // restore location
  window.location = storedWindowLocation;
});
// clean up once the tests are done
afterAll(() => server.close());

describe('Code', () => {
  it('should redirect to the homepage when user is logged in', async () => {
    // making sure the state in the url is different, should not matter when already logged in
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(
      'http://localhost/code?code=someOtherId&state=someOtherId',
    );
    const mockProps = {
      ...props,
      isLoggedIn: true,
    };
    const spySetAuthenticated = jest.spyOn(
      mockProps.sessionStorageProvider,
      'setHasAuthenticated',
    );
    const spyGetState = jest.spyOn(
      mockProps.sessionStorageProvider,
      'getOauthState',
    );
    const { getByText } = render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...mockProps }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(getByText('Redirected to /')).toBeTruthy();
      expect(spyGetState).not.toHaveBeenCalled();
      expect(mockProps.onSetAuth).not.toHaveBeenCalled();
      expect(mockProps.onLogin).not.toHaveBeenCalled();
      expect(spySetAuthenticated).not.toHaveBeenCalled();
    });
  });

  it('should handle a succesful login for cognito user', async () => {
    const mockProps = props;
    const spySetAuthenticated = jest.spyOn(
      mockProps.sessionStorageProvider,
      'setHasAuthenticated',
    );

    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...mockProps }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(mockProps.onSetAuth).toBeCalledWith({
        username: 'max.verstappen',
        token: mockloginSuccess.body.access_token,
        refresh_token: mockloginSuccess.body.refresh_token,
      });
      expect(mockProps.onLogin).toBeCalledWith(true);
      expect(spySetAuthenticated).toBeCalledWith('true');
    });
  });

  it('should handle a succesful login for non-cognito user', async () => {
    server.use(
      // override the initial "POST /token" request handler
      // to use a non-cognito user
      rest.post(configURLS.GW_AUTH_TOKEN_URL, (req, res, ctx) => {
        return res(ctx.json(mockloginSuccessGitlab));
      }),
    );
    const mockProps = props;
    const spySetAuthenticated = jest.spyOn(
      mockProps.sessionStorageProvider,
      'setHasAuthenticated',
    );
    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...mockProps }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(mockProps.onSetAuth).toBeCalledWith({
        username: 'max.verstappen@fakeemail.nl',
        token: mockloginSuccessGitlab.access_token,
        refresh_token: mockloginSuccessGitlab.refresh_token,
      });
      expect(mockProps.onLogin).toBeCalledWith(true);
      expect(spySetAuthenticated).toBeCalledWith('true');
    });
  });

  it('should handle a server error', async () => {
    server.use(
      // override the initial "POST /token" request handler
      // to return a 500 Server Error
      rest.post(configURLS.GW_AUTH_TOKEN_URL, (req, res, ctx) => {
        return res(ctx.status(500));
      }),
    );

    const { findByText } = render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...props }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await findByText('Redirected to /error');
  });

  it('should handle a failed login', async () => {
    server.use(
      // override the initial "POST /token" request handler
      // to return a failed login
      rest.post(configURLS.GW_AUTH_TOKEN_URL, (req, res, ctx) => {
        return res(ctx.json(mockloginFailed));
      }),
    );

    const { findByText } = render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...props }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await findByText('Redirected to /error');
  });

  it('should handle matching oauth states when state param configured', async () => {
    // mock window location with oauth state param
    delete window.location;
    const oauthStateParam = 'state123456';
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(`http://localhost/code?state=${oauthStateParam}`);

    // configure and require oauth state param
    const configWithState = {
      ...configURLS,
      GW_AUTH_LOGIN_URL: `/login/code&state=${oauthStateParam}`,
    };

    // mock session storage oauth state
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue(oauthStateParam);
    const mockProps = props;
    render(
      <AuthenticationProvider
        configURLS={configWithState}
        value={{ ...mockProps }}
      >
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(mockProps.onSetAuth).toBeCalled();
      expect(mockProps.onLogin).toBeCalledWith(true);
    });
  });

  it('should redirect to the error page when state is configured, but not returned', async () => {
    // mock window location without oauth state param
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(`http://localhost/code`);

    // configure and require oauth state param
    const configWithState = {
      ...configURLS,
      GW_AUTH_LOGIN_URL: `/login/code&state={state}`,
    };

    // mock session storage oauth state
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('xyz');

    const { findByText } = render(
      <AuthenticationProvider configURLS={configWithState} value={{ ...props }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );
    await findByText('Redirected to /error');
  });

  it('should redirect to the error page when oauth states do not match', async () => {
    // mock window location with oauth state param
    delete window.location;
    const stateQueryParam = '123456';
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(
      `http://localhost:4300/code?state=${stateQueryParam}`,
    );

    // configure and require oauth state param
    const sessionState = 'abcde';
    const configWithState = {
      ...configURLS,
      GW_AUTH_LOGIN_URL: `/login/code&state=${sessionState}`,
    };

    // mock session storage oauth state
    Storage.prototype.getItem = jest.fn(() => sessionState);

    const { findByText } = render(
      <AuthenticationProvider configURLS={configWithState} value={{ ...props }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );
    await findByText('Redirected to /error');
  });

  it('should redirect to the error page when oauth state is missing', async () => {
    // mock window location without oauth state param
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(`http://localhost:4300/code`);

    // configure and require oauth state param in login url
    const sessionState = 'abcde';
    const configWithState = {
      ...configURLS,
      GW_AUTH_LOGIN_URL: `/login/code&state=${sessionState}`,
    };

    const { findByText } = render(
      <AuthenticationProvider configURLS={configWithState} value={{ ...props }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );
    await findByText('Redirected to /error');
  });

  it('should redirect to the error page when oauth code verifier is missing', async () => {
    // mock session storage
    Storage.prototype.getItem = jest
      .fn()
      .mockReturnValueOnce('someId') // first call: state
      .mockReturnValueOnce(null); // second call: no code verifier

    const { findByText } = render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...props }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await findByText('Redirected to /error');
  });

  it('should redirect to the error page when oauth code is missing', async () => {
    // mock window location without oauth code param
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL(`http://localhost:4300/code?state=someId`);

    const { findByText } = render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...props }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );
    await findByText('Redirected to /error');
  });

  it('should clear and not reuse generated values for oauth if login successful', async () => {
    const spy = jest.spyOn(Storage.prototype, 'removeItem');
    const mockProps = props;
    render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...mockProps }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(mockProps.onSetAuth).toBeCalled();
      expect(mockProps.onLogin).toBeCalledWith(true);
      expect(spy).toHaveBeenNthCalledWith(2, SessionStorageKey.OAUTH_STATE);
      expect(spy).toHaveBeenNthCalledWith(
        3,
        SessionStorageKey.OAUTH_CODE_VERIFIER,
      );
      expect(spy).toHaveBeenNthCalledWith(
        4,
        SessionStorageKey.OAUTH_CODE_CHALLENGE,
      );
    });
  });

  it('should clear and not reuse generated values for oauth if login failed', async () => {
    server.use(
      // override the initial "POST /token" request handler
      // to return a failed login
      rest.post(configURLS.GW_AUTH_TOKEN_URL, (req, res, ctx) => {
        return res(ctx.json(mockloginFailed));
      }),
    );

    const spy = jest.spyOn(Storage.prototype, 'removeItem');

    const { findByText } = render(
      <AuthenticationProvider configURLS={configURLS} value={{ ...props }}>
        <BrowserRouter>
          <Switch>
            <Code />
          </Switch>
        </BrowserRouter>
      </AuthenticationProvider>,
    );

    await findByText('Redirected to /error');
    expect(spy).toHaveBeenNthCalledWith(2, SessionStorageKey.OAUTH_STATE);
    expect(spy).toHaveBeenNthCalledWith(
      3,
      SessionStorageKey.OAUTH_CODE_VERIFIER,
    );
    expect(spy).toHaveBeenNthCalledWith(
      4,
      SessionStorageKey.OAUTH_CODE_CHALLENGE,
    );
  });
});
