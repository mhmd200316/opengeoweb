/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
export interface AuthenticationConfig {
  GW_AUTH_LOGIN_URL: string;
  GW_AUTH_LOGOUT_URL: string;
  GW_AUTH_TOKEN_URL: string;
  GW_APP_URL: string;
  GW_AUTH_CLIENT_ID: string;
  GW_INFRA_BASE_URL?: string;
}

export interface SessionStorageProvider {
  setOauthState: (value: string) => void;
  getOauthState: () => string;
  removeOauthState: () => void;
  setOauthCodeVerifier: (value: string) => void;
  getOauthCodeVerifier: () => string;
  removeOauthCodeVerifier: () => void;
  setOauthCodeChallenge: (value: string) => void;
  getOauthCodeChallenge: () => string;
  removeOauthCodeChallenge: () => void;
  setHasAuthenticated: (value: string) => void;
  getHasAuthenticated: () => string;
  removeHasAuthenticated: () => void;
}

export enum SessionStorageKey {
  OAUTH_STATE = 'oauth_state',
  OAUTH_CODE_VERIFIER = 'code_verifier',
  OAUTH_CODE_CHALLENGE = 'code_challenge',
  HAS_AUTHENTICATED = 'has_authenticated',
}
