/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';

import {
  AuthenticationContext,
  AuthenticationProvider,
  getAuthConfig,
  replaceTemplateKeys,
  useAuthenticationContext,
  useAuthenticationDefaultProps,
} from './AuthenticationContext';
import { getSessionStorageProvider } from '../utils/session';

describe('/components/AuthenticationContext', () => {
  it('should return correct authUrls', () => {
    const testUrls = {
      GW_AUTH_LOGIN_URL: '/login/client_id={client_id}',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_TOKEN_URL: '/test124',
      GW_APP_URL: '/',
      GW_AUTH_CLIENT_ID: 'authclient123',
    };

    expect(getAuthConfig(testUrls)).toEqual({
      ...testUrls,
      GW_AUTH_LOGIN_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGIN_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
      ),
      GW_AUTH_LOGOUT_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGOUT_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
      ),
    });
  });

  it('should return correct authUrls with state param', () => {
    const testState = 'state123456';
    const spy = jest
      .spyOn(Storage.prototype, 'getItem')
      .mockReturnValue(testState);
    const testUrls = {
      GW_AUTH_LOGIN_URL: '/login/client_id={client_id}&state={state}',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_TOKEN_URL: '/test124',
      GW_APP_URL: '/',
      GW_AUTH_CLIENT_ID: 'authclient123',
    };

    expect(getAuthConfig(testUrls)).toEqual({
      ...testUrls,
      GW_AUTH_LOGIN_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGIN_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
        testState,
      ),
      GW_AUTH_LOGOUT_URL: replaceTemplateKeys(
        testUrls.GW_AUTH_LOGOUT_URL,
        testUrls.GW_AUTH_CLIENT_ID,
        testUrls.GW_APP_URL,
      ),
    });
    expect(spy).toHaveBeenCalledTimes(2);
    expect(spy).toHaveBeenCalledWith('oauth_state');
  });

  it('should use useAuthenticationDefaultProps', () => {
    const { result } = renderHook(() => useAuthenticationDefaultProps());

    expect(result.current.isLoggedIn).toBeFalsy();
    expect(result.current.auth).toBeNull();
  });

  it('should be able to render AuthenticationProvider', () => {
    const TestComponent: React.FC = () => <h1>Test</h1>;
    const { queryByText } = render(
      <AuthenticationProvider>
        <TestComponent />
      </AuthenticationProvider>,
    );
    expect(queryByText('Test')).toBeTruthy();
  });

  it('should be able to render with props', () => {
    const props = {
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: null,
      onSetAuth: jest.fn(),
      sessionStorageProvider: getSessionStorageProvider(),
    };

    const configUrls = {
      GW_AUTH_LOGIN_URL: '/login',
      GW_AUTH_TOKEN_URL: '/token',
      GW_AUTH_LOGOUT_URL: '/logout',
      GW_AUTH_CLIENT_ID: 'someID',
      GW_APP_URL: 'localhost',
    };

    let testProps;
    render(
      <AuthenticationProvider configURLS={configUrls} value={props}>
        <AuthenticationContext.Consumer>
          {(renderProps): React.ReactNode => {
            testProps = renderProps;
            return null;
          }}
        </AuthenticationContext.Consumer>
      </AuthenticationProvider>,
    );

    expect(testProps).toEqual({ ...props, authConfig: configUrls });
  });

  it('should be able to useAuthenticationContext ', () => {
    const TestComponent: React.FC = () => {
      const { isLoggedIn, onLogin } = useAuthenticationContext();

      if (isLoggedIn) {
        return <p>logged in</p>;
      }
      return (
        <p>
          not logged in
          <button type="button" onClick={(): void => onLogin(true)}>
            log in
          </button>
        </p>
      );
    };
    const { queryByText, container } = render(
      <AuthenticationProvider>
        <TestComponent />
      </AuthenticationProvider>,
    );
    expect(queryByText('not logged in')).toBeTruthy();
    const button = container.querySelector('button');

    fireEvent.click(button);
    expect(queryByText('logged in')).toBeTruthy();
  });
});
