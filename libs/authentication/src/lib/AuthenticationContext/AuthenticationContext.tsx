/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Credentials } from '@opengeoweb/api';
import { AuthenticationConfig, SessionStorageProvider } from './types';
import { getSessionStorageProvider } from '../utils/session';

const authConfigKeys = {
  GW_AUTH_LOGIN_URL: null,
  GW_AUTH_LOGOUT_URL: null,
  GW_AUTH_TOKEN_URL: null,
  GW_APP_URL: null,
  GW_INFRA_BASE_URL: null,
  GW_AUTH_CLIENT_ID: null,
};

export const getRandomString = (): string => {
  const array = new Uint32Array(8);
  window.crypto.getRandomValues(array);
  const randomString = Array.from(array, (dec) => dec.toString(16)).join('');
  return btoa(randomString);
};

export const getCodeChallenge = async (
  codeVerifier: string,
): Promise<string> => {
  const encoder = new TextEncoder();
  const data = encoder.encode(codeVerifier);
  const digest = await crypto.subtle.digest('SHA-256', data);
  return btoa(String.fromCharCode.apply(null, new Uint8Array(digest)))
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=+$/, '');
};

export const replaceTemplateKeys = (
  url: string,
  clientId: string,
  appUrl: string,
  oauthState?: string,
  codeChallenge?: string,
): string => {
  return url
    .replace('{client_id}', clientId)
    .replace('{app_url}', appUrl)
    .replace('{state}', oauthState)
    .replace('{code_challenge}', codeChallenge);
};

export const getAuthConfig = (
  _configUrls: AuthenticationConfig,
): AuthenticationConfig => {
  const {
    GW_AUTH_LOGIN_URL,
    GW_AUTH_LOGOUT_URL,
    GW_AUTH_TOKEN_URL,
    GW_APP_URL,
    GW_INFRA_BASE_URL,
    GW_AUTH_CLIENT_ID,
  } = _configUrls;
  const sessionStorageProvider = getSessionStorageProvider();

  const loginUrl = replaceTemplateKeys(
    GW_AUTH_LOGIN_URL,
    GW_AUTH_CLIENT_ID,
    GW_APP_URL,
    sessionStorageProvider.getOauthState(),
    sessionStorageProvider.getOauthCodeChallenge(),
  );
  const logOutUrl = replaceTemplateKeys(
    GW_AUTH_LOGOUT_URL,
    GW_AUTH_CLIENT_ID,
    GW_APP_URL,
  );

  return {
    GW_AUTH_LOGIN_URL: loginUrl,
    GW_AUTH_LOGOUT_URL: logOutUrl,
    GW_AUTH_TOKEN_URL,
    GW_APP_URL,
    GW_AUTH_CLIENT_ID,
    GW_INFRA_BASE_URL,
  };
};

export interface AuthenticationContextProps {
  isLoggedIn: boolean;
  onLogin: (isLoggedIn: boolean) => void;
  auth: Credentials;
  onSetAuth: (auth: Credentials) => void;
  authConfig: AuthenticationConfig;
  sessionStorageProvider: SessionStorageProvider;
}

export const AuthenticationContext: React.Context<AuthenticationContextProps> =
  React.createContext({
    isLoggedIn: null,
    onLogin: null,
    auth: null,
    onSetAuth: null,
    authConfig: null,
    sessionStorageProvider: null,
  });

interface AuthenticationDefaultStateProps {
  isLoggedIn: boolean;
  onLogin: (isLoggedIn: boolean) => void;
  auth: Credentials;
  onSetAuth: (auth: Credentials) => void;
  sessionStorageProvider: SessionStorageProvider;
}

export const useAuthenticationDefaultProps =
  (): AuthenticationDefaultStateProps => {
    const [isLoggedIn, onLogin] = React.useState(false);
    const [auth, onSetAuth] = React.useState(null);
    const sessionStorageProvider = getSessionStorageProvider();
    return { isLoggedIn, onLogin, auth, onSetAuth, sessionStorageProvider };
  };

interface AuthenticationProviderProps {
  children: JSX.Element;
  value?: AuthenticationDefaultStateProps;
  configURLS?: AuthenticationConfig;
}

export const AuthenticationProvider: React.FC<AuthenticationProviderProps> = ({
  children,
  value,
  configURLS = authConfigKeys,
}: AuthenticationProviderProps) => {
  const defaultValues = useAuthenticationDefaultProps();
  const { isLoggedIn, onLogin, auth, onSetAuth, sessionStorageProvider } =
    value || defaultValues;
  const authConfig = configURLS;

  return (
    <AuthenticationContext.Provider
      value={{
        isLoggedIn,
        onLogin,
        auth,
        onSetAuth,
        authConfig,
        sessionStorageProvider,
      }}
    >
      {children}
    </AuthenticationContext.Provider>
  );
};

export const useAuthenticationContext = (): AuthenticationContextProps =>
  React.useContext(AuthenticationContext);
