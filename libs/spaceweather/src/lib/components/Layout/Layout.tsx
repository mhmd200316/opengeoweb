/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Grid } from '@mui/material';

import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles({
  Body: {
    marginTop: 5,
    padding: '0px 8px 10px',
    height: '100%',
    width: 'calc(100% - 8px)',
    marginBottom: 0,
  },
});

interface LayoutProps {
  leftTop: React.ReactChild;
  leftBottom: React.ReactChild;
  right: React.ReactChild;
}

/**
 * Layout
 * Creates a grid with one component at the top, two on the left and one on the right.
 * IMPORTANT: WHEN USING THIS COMPONENT PLEASE WRAP INSIDE OF <NotificationTriggerProvider/>
 * @param {ReactChild} leftTop leftTop: ReactChild - React component to show on the left top
 * @param {ReactChild} leftBottom leftBottom: ReactChild - React component to show on the left bottom
 * @param {ReactChild} right right: ReactChild - React component to show on the right
 * @example
 * ``` <Layout leftTop={<Bulletin />} leftBottom={<Notifications />} right={<TimeSeries />} /> ```
 */
const Layout: React.FC<LayoutProps> = ({
  leftTop,
  leftBottom,
  right,
}: LayoutProps) => {
  const classes = useStyles();
  return (
    <Grid container direction="column">
      <Grid item className={classes.Body} container spacing={1}>
        <Grid item xs={12} sm={6} container direction="column">
          <Grid item xs>
            {leftTop}
          </Grid>
          <Grid item xs>
            {leftBottom}
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6}>
          {right}
        </Grid>
      </Grid>
    </Grid>
  );
};
export default Layout;
