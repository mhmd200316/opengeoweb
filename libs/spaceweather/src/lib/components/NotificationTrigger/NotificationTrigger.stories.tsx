/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Paper, Typography, Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { ErrorBoundary } from '@opengeoweb/shared';
import { NotificationTrigger } from './NotificationTrigger';
import { NotificationTriggerProvider } from './NotificationTriggerContext';
import { StoryWrapper } from '../../utils/storybookUtils';
import Layout from '../Layout/Layout';
import { Bulletin } from '../Bulletin';
import { Notifications } from '../Notifications';
import TimeSeries from '../TimeSeries/TimeSeries';

export default { title: 'components/Notification Trigger' };

const DummyNewNotificationTriggerAlertComponent = (): React.ReactElement => {
  return (
    <NotificationTriggerProvider>
      <NotificationTrigger />
    </NotificationTriggerProvider>
  );
};

export const DummyNewNotificationTriggerAlert = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <DummyNewNotificationTriggerAlertComponent />
    </StoryWrapper>
  );
};

const useStyles2 = makeStyles((theme: Theme) => ({
  Dialog: {
    position: 'absolute',
    top: '1%',
    left: '5%',
    maxWidth: '90%',
    maxHeight: '98%',
    display: 'flex',
    flexDirection: 'column',
    zIndex: 550,
    overflowY: 'hidden',
  },
  DialogHeader: {
    height: '30px',
    padding: theme.spacing(1),
    boxShadow:
      '0 1px 3px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.12), 0 0 2px 0 rgba(0, 0, 0, 0.14)',
    textAlign: 'center',
  },
  dialogBody: { overflowY: 'scroll', paddingBottom: '10px' },
}));

const SpaceweatherWithNotificationDemoComponent = (): React.ReactElement => {
  const classes = useStyles2();
  return (
    <NotificationTriggerProvider>
      <Paper data-testid="spaceWeatherDialog" className={classes.Dialog}>
        <div className={classes.DialogHeader}>
          <Typography variant="subtitle1">Space Weather Forecast</Typography>
        </div>
        <div>
          <NotificationTrigger />
        </div>
        <div className={classes.dialogBody}>
          <Layout
            leftTop={<Bulletin />}
            leftBottom={<Notifications />}
            right={<TimeSeries />}
          />
        </div>
      </Paper>
    </NotificationTriggerProvider>
  );
};

export const SpaceweatherWithNotificationDemo = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <ErrorBoundary>
        <SpaceweatherWithNotificationDemoComponent />
      </ErrorBoundary>
    </StoryWrapper>
  );
};
