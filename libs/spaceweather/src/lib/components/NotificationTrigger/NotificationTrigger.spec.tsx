/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import * as utils from '@opengeoweb/api';
import { render, waitFor } from '@testing-library/react';
import { NotificationTrigger } from './NotificationTrigger';
import { NotificationTriggerProvider } from '.';
import { TestWrapper } from '../../utils/testUtils';
import { fakeEventList } from '../../utils/fakedata';

jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual('../../../../api/src/index') as Record<
    string,
    unknown
  >),
}));

describe('src/components/NotificationTrigger/NotificationTrigger', () => {
  it('should render text for 2 triggers if 2 triggers received', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: false,
      error: null,
      result: [fakeEventList[1], fakeEventList[4]],
    });
    const { queryByText, queryByTestId } = render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <NotificationTrigger />
        </NotificationTriggerProvider>
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(
        queryByText('There are 2 unhandled notification triggers.'),
      ).toBeTruthy();
      expect(queryByTestId('notificationtrigger-alert')).toBeTruthy();
    });
  });

  it('should render an error message if error passed', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: false,
      error: new Error('error'),
      result: null,
    });
    const { queryByText } = render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <NotificationTrigger />
        </NotificationTriggerProvider>
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(
        queryByText('New notification trigger retrieval: error'),
      ).toBeTruthy();
    });
  });

  it('should render text for single trigger if only 1 trigger received', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: false,
      error: null,
      result: [fakeEventList[1]],
    });
    const { queryByText, queryByTestId } = render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <NotificationTrigger />
        </NotificationTriggerProvider>
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(
        queryByText('There is 1 unhandled notification trigger.'),
      ).toBeTruthy();
      expect(queryByTestId('notificationtrigger-alert')).toBeTruthy();
    });
  });
});
