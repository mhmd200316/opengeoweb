/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { Button } from '@mui/material';
import * as utils from '@opengeoweb/api';
import { NotificationTriggerProvider, useNotificationTriggerContext } from '.';
import { TestWrapper } from '../../utils/testUtils';
import { fetchFakeNewNotifications } from '../../utils/fakeApi';

jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual('../../../../api/src/index') as Record<
    string,
    unknown
  >),
}));

describe('src/components/NotificationTrigger/NotificationTriggerContext', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('should render successfully', async () => {
    const { queryByText } = render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <div>Hello there</div>
        </NotificationTriggerProvider>
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(queryByText('Hello there')).toBeTruthy();
    });
  });

  it('should refetch triggers periodically', async () => {
    const fetchNewData = jest.fn();
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: false,
      error: null,
      result: fetchFakeNewNotifications(),
      fetchApiData: fetchNewData,
    });
    const { queryByText } = render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <div>Hello there</div>
        </NotificationTriggerProvider>
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(queryByText('Hello there')).toBeTruthy();
    });

    jest.runOnlyPendingTimers();
    // Test new data has been fetched
    expect(fetchNewData).toHaveBeenCalled();

    jest.runOnlyPendingTimers();
    // Test new data has been fetched
    expect(fetchNewData).toHaveBeenCalledTimes(2);
  });

  it('should refetch triggers periodically also if error', async () => {
    const fetchNewData = jest.fn();
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: false,
      error: new Error('error'),
      result: null,
      fetchApiData: fetchNewData,
    });
    const { queryByText } = render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <div>Hello there</div>
        </NotificationTriggerProvider>
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(queryByText('Hello there')).toBeTruthy();
    });

    jest.runOnlyPendingTimers();
    // Test new data has been fetched
    expect(fetchNewData).toHaveBeenCalled();

    jest.runOnlyPendingTimers();
    // Test new data has been fetched
    expect(fetchNewData).toHaveBeenCalledTimes(2);
  });

  it('should not call periodically if triggers still loading', async () => {
    const fetchNewData = jest.fn();
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: true,
      error: null,
      result: null,
      fetchApiData: fetchNewData,
    });

    const { queryByText } = render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <div>Hello there</div>
        </NotificationTriggerProvider>
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(queryByText('Hello there')).toBeTruthy();
    });

    jest.runOnlyPendingTimers();

    // Test new data has not been fetched
    expect(fetchNewData).not.toHaveBeenCalled();

    jest.runOnlyPendingTimers();
    expect(fetchNewData).not.toHaveBeenCalled();
  });

  it('should fetch new data if onFetchNewNotificationTriggerData is called ', async () => {
    const TestComponent: React.FC = () => {
      const { onFetchNewNotificationTriggerData } =
        useNotificationTriggerContext();

      return (
        <Button
          data-testid="fakebutton"
          onClick={onFetchNewNotificationTriggerData}
        >
          Hello there
        </Button>
      );
    };
    const fetchNewData = jest.fn();
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: false,
      error: null,
      result: fetchFakeNewNotifications(),
      fetchApiData: fetchNewData,
    });
    const { getByTestId } = render(
      <TestWrapper>
        <NotificationTriggerProvider>
          <TestComponent />
        </NotificationTriggerProvider>
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(getByTestId('fakebutton')).toBeTruthy();
    });

    fireEvent.click(getByTestId('fakebutton'));

    // Test new data has been fetched
    expect(fetchNewData).toHaveBeenCalledTimes(1);
  });
});
