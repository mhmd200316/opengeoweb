/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { TimeRange } from 'pondjs';
import moment from 'moment';
import { fireEvent, render } from '@testing-library/react';
import ResetTimeRangeButton from './ResetTimeRangeButton';
import { defaultTimeRange } from '../../utils/defaultTimeRange';

jest.mock('../../utils/api');

describe('src/components/TimeSeries/ResetTimeRangeButton', () => {
  it('should be hidden when current timerange is default timerange', async () => {
    const props = {
      timeRange: defaultTimeRange,
      onResetTimeRange: jest.fn(),
    };
    const { queryByTestId } = render(<ResetTimeRangeButton {...props} />);

    expect(queryByTestId('reset-timerange-button')).toBeFalsy();
  });

  it('should reset the timerange to default when clicked', async () => {
    const dummyTimeRange = new TimeRange(
      moment.utc().subtract(5, 'days'),
      moment.utc().subtract(2, 'days'),
    );

    const props = {
      timeRange: dummyTimeRange,
      onResetTimeRange: jest.fn(),
    };
    const { getByTestId } = render(<ResetTimeRangeButton {...props} />);

    const resetButton = getByTestId('reset-timerange-button');
    fireEvent.click(resetButton);
    expect(props.onResetTimeRange).toHaveBeenCalledTimes(1);
  });
  it('should show tooltip when hovering over button', async () => {
    const dummyTimeRange = new TimeRange(
      moment.utc().subtract(5, 'days'),
      moment.utc().subtract(2, 'days'),
    );

    const props = {
      timeRange: dummyTimeRange,
      onResetTimeRange: jest.fn(),
    };
    const { getByTestId, findByText } = render(
      <ResetTimeRangeButton {...props} />,
    );

    fireEvent.mouseOver(getByTestId('reset-timerange-button'));
    // Wait until tooltip appears
    expect(await findByText('Reset timeline')).toBeTruthy();
  });
});
