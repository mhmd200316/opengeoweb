/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
export const getTrackerData = (
  trackerPosition: Date,
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  charts,
): [{ key: string; value: string }][] => {
  if (trackerPosition) {
    return charts.map((chart) => {
      const values = chart.series.map((serie, chartIndex) => {
        if (
          serie.range() &&
          trackerPosition >= serie.begin() &&
          trackerPosition <= serie.end()
        ) {
          const key =
            chart.graphType === 'AREA'
              ? chart.columns[0][chartIndex]
              : chart.columns[chartIndex];
          const event = serie.atTime(trackerPosition);

          if (
            !event ||
            event.get(key) === undefined ||
            event.get(key) === null
          ) {
            return { key: '', value: '' };
          }

          // eslint-disable-next-line no-underscore-dangle
          const unit = serie._data.get('unit');

          let value = '';
          value = event.get(key).toString();

          if (chart.graphType === 'LOG') {
            value = parseFloat(value).toPrecision(3);
          } else if (chart.graphType === 'AREA') {
            value = parseFloat(value).toPrecision(4);
          }
          // if (value !== 'NaN') return ` ${value} ${unit}`;
          if (value !== 'NaN')
            return { key, value: ` ${value} ${unit !== '-' ? unit : ''}` };
          return { key: '', value: '' };
        }
        return [];
      });
      return values;
    });
  }
  return [];
};
