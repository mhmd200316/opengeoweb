/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { TimeSeries, Index } from 'pondjs';

export const createChartSeriesData = (
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  response,
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  { id, columns, timeWidth, graphType },
): [] => {
  return response.map((resp, responseIndex) => {
    const column = columns[responseIndex];
    const newColumns = Array.isArray(column)
      ? ['index', ...column]
      : ['index', column];

    // series
    const series = new TimeSeries({
      name: id,
      columns: newColumns,
      unit: resp.data.unit,
      points: resp.data.data.map(({ timestamp, value }) => {
        if (graphType === 'AREA' || graphType === 'BAND') {
          return [
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            Index.getIndexString(timeWidth, new Date(timestamp)),
            value,
            value,
          ];
        }

        return [
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          Index.getIndexString(timeWidth, new Date(timestamp)),
          value,
        ];
      }),
    });

    return series;
  });
};
