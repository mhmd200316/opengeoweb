/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import moment from 'moment';
import { Paper } from '@mui/material';

import { markerStyle, curveColors, chartMarkerStyle } from './styles';
import { useTimeTrackerContext } from './TimeTrackerContext';
import { GraphItem } from './types';
import { getTrackerData } from './Tracker.utils';
import { getGraphHeightInPx } from './TimeSeries.utils';

interface TrackerProps {
  graphs: GraphItem[];
}

const trackerMargin = 5;
const maxTrackerInfobox = 150;

function useFlipTrackerInfobox(
  container: React.RefObject<HTMLDivElement>,
  trackerX: number,
): [boolean] {
  const [isFlipped, setFlipped] = React.useState(false);

  React.useEffect(() => {
    if (container.current) {
      const isSmaller =
        container.current.offsetWidth - Math.trunc(trackerX) <
        maxTrackerInfobox;

      setFlipped(() => isSmaller);
    }
  }, [container, trackerX]);

  return [isFlipped];
}

const Tracker: React.FC<TrackerProps> = ({ graphs }: TrackerProps) => {
  const { tracker, trackerX } = useTimeTrackerContext();
  const trackerPositionData = getTrackerData(tracker, graphs);
  const container = React.useRef<HTMLDivElement>(null);
  const [isFlipped] = useFlipTrackerInfobox(container, trackerX);
  if (trackerX === null || !trackerPositionData.length) {
    return null;
  }

  const currentTime = moment
    .utc(tracker)
    .format('YYYY-MM-DD HH:mm')
    .concat(' UTC');

  const graphHeightWithTitle = getGraphHeightInPx() + 28;

  return (
    <div ref={container}>
      {trackerPositionData.map((position, index) => {
        return (
          // eslint-disable-next-line react/no-array-index-key
          <div style={{ position: 'relative' }} key={`tracker-${index}`}>
            <div
              style={{
                position: 'absolute',
                left: trackerX + trackerMargin,
                top: `${25 + index * graphHeightWithTitle}px`, // Ensure tooltips are at correct position
                border: '1px solid lightgrey',
                backgroundColor: 'white',
                zIndex: 1,
                opacity: 0.8,
                ...(isFlipped ? { marginLeft: -maxTrackerInfobox } : {}),
              }}
            >
              {position.length ? (
                <Paper>
                  <div style={markerStyle}>
                    {currentTime}
                    &nbsp;
                  </div>
                  {position.map((value) => {
                    if (value.value) {
                      return (
                        <div
                          style={
                            curveColors[value.key] === 'lightgrey'
                              ? chartMarkerStyle('gray') // lightgrey is hard to read on a white background
                              : chartMarkerStyle(curveColors[value.key])
                          }
                          key={`tracker-value-${value.key}-${value.value}`}
                        >
                          {value.value}
                        </div>
                      );
                    }
                    return null;
                  })}
                </Paper>
              ) : null}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Tracker;
