/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { TimeRange } from 'pondjs';
import moment from 'moment';
import { TimeTrackerContext, TimeTrackerProvider } from './TimeTrackerContext';
import { GraphContainer } from './GraphContainer';
import {
  barGraphWithSeries,
  areaGraphWithSeries,
  logGraphWithSeries,
  bandGraphWithSeries,
  bandGraphWithSeriesFixedDates,
  barGraphWithSeriesFixedDates,
  areaGraphWithSeriesFixedDates,
  logGraphWithSeriesFixedDates,
} from '../../utils/DummyData';
import Tracker from './Tracker';
import { defaultTimeRange } from '../../utils/defaultTimeRange';
import {
  StoryWrapperWithNetworkError,
  StoryWrapper,
  StoryWrapperWithOnlyOneNetworkError,
} from '../../utils/storybookUtils';
import TimeSeries from './TimeSeries';
import { GraphItem } from './types';

export default { title: 'components/TimeSeries' };

export const BarGraphDummyData: React.FC<Record<string, unknown>> = () => {
  const [timeRange, setTimeRange] = React.useState(defaultTimeRange);

  return (
    <StoryWrapper>
      <TimeTrackerProvider>
        <Tracker graphs={[barGraphWithSeries]} />
        <GraphContainer
          graphs={[barGraphWithSeries]}
          timeRange={timeRange}
          setTimeRange={setTimeRange}
        />
      </TimeTrackerProvider>
    </StoryWrapper>
  );
};

export const AreaGraphDummyData: React.FC<Record<string, unknown>> = () => {
  const [timeRange, setTimeRange] = React.useState(defaultTimeRange);

  return (
    <StoryWrapper>
      <TimeTrackerProvider>
        <Tracker graphs={[areaGraphWithSeries]} />
        <GraphContainer
          graphs={[areaGraphWithSeries]}
          timeRange={timeRange}
          setTimeRange={setTimeRange}
        />
      </TimeTrackerProvider>
    </StoryWrapper>
  );
};

export const LogarithmicGraphDummyData: React.FC<Record<string, unknown>> =
  () => {
    const [timeRange, setTimeRange] = React.useState(defaultTimeRange);

    return (
      <StoryWrapper>
        <TimeTrackerProvider>
          <Tracker graphs={[logGraphWithSeries]} />
          <GraphContainer
            graphs={[logGraphWithSeries]}
            timeRange={timeRange}
            setTimeRange={setTimeRange}
          />
        </TimeTrackerProvider>
      </StoryWrapper>
    );
  };

export const BandGraphDummyData: React.FC<Record<string, unknown>> = () => {
  const [timeRange, setTimeRange] = React.useState(defaultTimeRange);

  return (
    <StoryWrapper>
      <TimeTrackerProvider>
        <Tracker graphs={[bandGraphWithSeries]} />
        <GraphContainer
          graphs={[bandGraphWithSeries]}
          timeRange={timeRange}
          setTimeRange={setTimeRange}
        />
      </TimeTrackerProvider>
    </StoryWrapper>
  );
};

export const TimeSeriesDummyData: React.FC = () => {
  return (
    <StoryWrapper>
      <TimeSeries />
    </StoryWrapper>
  );
};

export const TimeSeriesWithError: React.FC = () => {
  return (
    <StoryWrapperWithNetworkError>
      <TimeSeries />
    </StoryWrapperWithNetworkError>
  );
};

export const TimeSeriesWithKpIndexError: React.FC = () => {
  return (
    <StoryWrapperWithOnlyOneNetworkError>
      <TimeSeries />
    </StoryWrapperWithOnlyOneNetworkError>
  );
};

const beginTime = moment('2022-01-03T03:00:00Z');
const endTime = moment('2022-01-07T03:00:00Z');
const initialTimeRange = new TimeRange(beginTime, endTime);
const trackerValue = {
  tracker: moment('2022-01-06T03:00:00Z').toDate(),
  trackerX: 610, // to position the tracker info
  onChangeTracker: (): void => null,
};

const SnapshotGraph = ({
  graphData,
}: {
  graphData: GraphItem;
}): React.ReactElement => {
  const [timeRange, setTimeRange] = React.useState(initialTimeRange);
  return (
    <div style={{ height: '200px', width: '800px' }}>
      <StoryWrapper>
        <TimeTrackerContext.Provider value={trackerValue}>
          <Tracker graphs={[graphData]} />
          <GraphContainer
            graphs={[graphData]}
            timeRange={timeRange}
            setTimeRange={setTimeRange}
          />
        </TimeTrackerContext.Provider>
      </StoryWrapper>
    </div>
  );
};

export const BandGraphSnap = (): React.ReactElement => (
  <SnapshotGraph graphData={bandGraphWithSeriesFixedDates} />
);

BandGraphSnap.storyName = 'Band Graph (takeSnapshot)';

export const BarGraphSnap = (): React.ReactElement => (
  <SnapshotGraph graphData={barGraphWithSeriesFixedDates} />
);

BarGraphSnap.storyName = 'Bar Graph (takeSnapshot)';

export const AreaGraphSnap = (): React.ReactElement => (
  <SnapshotGraph graphData={areaGraphWithSeriesFixedDates} />
);

AreaGraphSnap.storyName = 'Area Graph (takeSnapshot)';

export const LogarithmicGraphSnap = (): React.ReactElement => (
  <SnapshotGraph graphData={logGraphWithSeriesFixedDates} />
);

LogarithmicGraphSnap.storyName = 'Logarithmic Graph (takeSnapshot)';
