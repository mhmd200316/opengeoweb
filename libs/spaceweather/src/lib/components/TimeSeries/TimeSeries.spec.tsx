/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { createFakeApiInstance } from '@opengeoweb/api';
import TimeSeries, { config } from './TimeSeries';
import { TestWrapper } from '../../utils/testUtils';
import * as api from '../../utils/api';
import {
  createApi as createFakeApi,
  getDummyTimeSerie,
} from '../../utils/fakeApi';
import { SWErrors, TimeseriesResponseData } from '../../types';

jest.mock('../../utils/api');

describe('src/components/TimeSeries/TimeSeries', () => {
  const fakeAxiosInstance = createFakeApiInstance();
  // Need to set the offsetWidth for Resizable to render its children
  Object.defineProperty(HTMLElement.prototype, 'offsetWidth', {
    configurable: true,
    value: 500,
  });

  it('should load graphs', async () => {
    jest.spyOn(console, 'warn').mockImplementation();
    const { getByText, getAllByTestId } = render(
      <TestWrapper>
        <TimeSeries />
      </TestWrapper>,
    );

    // each graph should show loading state
    await waitFor(() => {
      config.forEach((graph) => {
        expect(getByText(`Loading ${graph.title}`)).toBeTruthy();
      });
    });
    // wait for all graphs to be loaded
    await waitFor(() => {
      expect(getAllByTestId('graph-container').length).toEqual(config.length);
    });
  });

  it('should show the ResetTimeRangeButton only on hover', async () => {
    const { getAllByTestId, queryByTestId, getByTestId } = render(
      <TestWrapper>
        <TimeSeries />
      </TestWrapper>,
    );

    // wait for all graphs to be loaded
    await waitFor(() => {
      expect(getAllByTestId('graph-container').length).toEqual(config.length);
    });

    expect(queryByTestId('reset-timerange-button-div')).toBeFalsy();
    fireEvent.mouseEnter(getAllByTestId('graph-container')[0]);
    expect(getByTestId('reset-timerange-button-div')).toBeTruthy();
    fireEvent.mouseLeave(getAllByTestId('graph-container')[0]);
    expect(queryByTestId('reset-timerange-button-div')).toBeFalsy();
  });

  it('should show an error message for each graph when loading fails', async () => {
    const spy = jest.spyOn(api, 'createApi').mockImplementationOnce(() => {
      return {
        ...createFakeApi(),
        getTimeSeriesMultiple: (): Promise<
          { data: TimeseriesResponseData }[]
        > => Promise.reject(new Error('test error message')),
      };
    });

    const { getAllByRole } = render(
      <TestWrapper>
        <TimeSeries />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(getAllByRole('alert').length).toEqual(config.length);
      getAllByRole('alert').forEach((alert) => {
        expect(alert.textContent).toEqual('test error message');
      });
    });

    spy.mockRestore();
  });

  it('should show load the graphs but show the small error overlay if one of multiple series in one graph fails', async () => {
    const spy = jest.spyOn(api, 'createApi').mockImplementationOnce(() => {
      return {
        ...createFakeApi(),
        getTimeSeriesMultiple: (
          params,
        ): Promise<{ data: TimeseriesResponseData }[]> => {
          return Promise.all(
            params.map((param) => {
              if (param.stream === 'kp') {
                return Promise.reject(
                  new Error('Network test error message'),
                ).catch((error) => {
                  return error;
                });
              }
              return fakeAxiosInstance.get('/timeseries/data').then(() => ({
                data: getDummyTimeSerie(param.stream, param.parameter),
              }));
            }),
          );
        },
      };
    });

    const { getByRole, getAllByTestId, getByTestId } = render(
      <TestWrapper>
        <TimeSeries />
      </TestWrapper>,
    );

    await waitFor(() => {
      // expect all graphs to have loaded
      expect(getAllByTestId('graph-container').length).toEqual(config.length);
      // But the error to be visible
      expect(getByTestId('temp-error-inner-graph')).toBeTruthy();
      expect(getByRole('alert')).toBeTruthy();
      expect(getByRole('alert').textContent).toEqual(
        'Network test error message',
      );
    });

    spy.mockRestore();
  });
  it('should not show an error when a request was cancelled', async () => {
    const spy = jest.spyOn(api, 'createApi').mockImplementationOnce(() => {
      return {
        ...createFakeApi(),
        getTimeSeriesMultiple: (
          params,
        ): Promise<{ data: TimeseriesResponseData }[]> => {
          return Promise.all(
            params.map((param) => {
              if (param.stream === 'xray') {
                return Promise.reject(new Error(SWErrors.USER_CANCELLED));
              }
              return fakeAxiosInstance.get('/timeseries/data').then(() => ({
                data: getDummyTimeSerie(param.stream, param.parameter),
              }));
            }),
          );
        },
      };
    });

    const { queryByRole, getAllByTestId, queryByTestId } = render(
      <TestWrapper>
        <TimeSeries />
      </TestWrapper>,
    );

    await waitFor(() => {
      // expect all graphs to have loaded minus one
      expect(getAllByTestId('graph-container').length).toEqual(
        config.length - 1,
      );
      // and no error to be visible
      expect(queryByTestId('temp-error-inner-graph')).toBeFalsy();
      expect(queryByRole('alert')).toBeFalsy();
    });

    spy.mockRestore();
  });
});
