/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
/* eslint-disable @typescript-eslint/naming-convention */
import * as React from 'react';
import { Grid, Typography, Skeleton } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { TimeRange } from 'pondjs';
import { useApiContext, useDebounce } from '@opengeoweb/api';
import { AlertBanner, usePoller } from '@opengeoweb/shared';
import {
  defaultTimeRange,
  timeRangeToUTC,
  getDefaultTimeRange,
} from '../../utils/defaultTimeRange';

import Tracker from './Tracker';
import { GraphContainer } from './GraphContainer';
import { TimeTrackerProvider } from './TimeTrackerContext';
import { SWErrors } from '../../types';
import { GraphItem } from './types';
import { TimeSeriesHeader } from './TimeSeriesHeader';
import ResetTimeRangeButton from './ResetTimeRangeButton';
import {
  createStateById,
  stateAsArray,
  cancelRequests,
  hasSeries,
  useUserSleeping,
  getGraphIndexWithLastData,
  useStreams,
  getGraphHeightInPx,
} from './TimeSeries.utils';
import { createChartSeriesData } from './createChartSeriesData';
import GraphStatus from './GraphStatus';
import { SpaceWeatherApi } from '../../utils/api';

const graphHeight = getGraphHeightInPx();

const useStyles = makeStyles({
  container: {
    position: 'relative',
    '&:after': {
      content: '""',
      display: 'block',
      position: 'absolute',
      top: 0,
      bottom: 0,
      width: 72,
      right: 0,
      background:
        'linear-gradient(90deg, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%)',
      pointerEvents: 'none',
    },
  },
});

// config
export const config: GraphItem[] = [
  // 1 Kp Index
  {
    id: '0-kp-barGraph',
    title: 'Kp Index',
    graphType: 'BAR',
    params: [
      {
        stream: 'kp',
        parameter: 'kp',
      },
      {
        stream: 'kpforecast',
        parameter: 'kpforecast',
      },
    ],
    columns: ['kpIndex', 'kpIndexForecast'],
    timeWidth: '3h',
    yMinValue: 0,
    yMaxValue: 9,
    threshold: [
      { title: 'Met Office', value: 5 },
      { title: 'KNMI', value: 7 },
    ],
  },
  // 2 Interplanetary Magnetic Field Bt and Bz (nT)
  {
    id: '1-rtsw-bandGraph',
    title: 'Interplanetary Magnetic Field Bt and Bz (nT)',
    graphType: 'BAND',
    params: [
      { stream: 'rtsw_mag', parameter: 'bt' },
      { stream: 'rtsw_mag', parameter: 'bz_gsm' },
    ],
    columns: ['magneticFieldBt', 'magneticFieldBz'],
    timeWidth: '1s',
    threshold: [{ title: '', value: -20 }],
    yMinValue: -25,
    yMaxValue: 25,
  },
  // 3 X-ray Solar Flux
  {
    id: '2-xray-logarithmicGraph',
    title: 'X-ray Solar Flux (W/m\u00B2)',
    graphType: 'LOG',
    params: [
      {
        stream: 'xray',
        parameter: 'solar_x_ray_flux_long',
      },
    ],
    columns: ['lineSolarXray'],
    timeWidth: '1s',
    threshold: [{ title: '', value: 5e-5 }],
    yMinValue: 1e-9,
    yMaxValue: 1e-2,
    tickCount: 8,
    tickValues: (value: number): string => {
      switch (value) {
        case 1:
          return '10^-9';
        case 2:
          return '10^-8';
        case 3:
          return '10^-7';
        case 4:
          return '10^-6';
        case 5:
          return '10^-5';
        case 6:
          return '10^-4';
        case 7:
          return '10^-3';
        case 8:
          return '10^-2';
        default:
          return '';
      }
    },
  },
  // 4 Solar Wind Speed (km/s)
  {
    id: '3-wind-areaGraph',
    title: 'Solar Wind Speed (km/s)',
    graphType: 'AREA',
    params: [{ stream: 'rtsw_wind', parameter: 'proton_speed' }],
    columns: [['lineSolarWindSpeed', 'areaSolarWindSpeed']],
    timeWidth: '1s',
    yMinValue: 200,
    yMaxValue: 800,
  },
  // 5 Solar Wind Density (nPa)
  {
    id: '4-wind-density-logarithmicGraph',
    title: 'Solar Wind Density (1/cm\u00B3)',
    graphType: 'LOG',
    params: [
      {
        stream: 'rtsw_wind',
        parameter: 'proton_density',
      },
    ],
    columns: ['lineSolarWindDensity'],
    timeWidth: '1s',
    yMinValue: 1,
    yMaxValue: 100,
    tickCount: 3,
    tickValues: (value: number): number => {
      switch (value) {
        case 1:
        default:
          return 1;
        case 2:
          return 10;
        case 3:
          return 100;
      }
    },
  },
  // 6 Solar Wind Pressure (nPa)
  {
    id: '5-wind-pressure-areaGraph',
    title: 'Solar Wind Pressure (nPa)',
    graphType: 'AREA',
    params: [
      {
        stream: 'rtsw_wind',
        parameter: 'proton_pressure',
      },
    ],
    columns: [['lineSolarWindPressure', 'areaSolarWindPressure']],
    timeWidth: '1s',
    yMinValue: 0,
    yMaxValue: 15,
  },
];

// component
const TimeSeriesGraphs: React.FC = () => {
  const classes = useStyles();
  const { api } = useApiContext<SpaceWeatherApi>();
  const [isUserSleeping, resetUserIsSleeping] = useUserSleeping();
  const [timeRange, setTimeRange] = React.useState(defaultTimeRange);
  const [time_start, time_stop] = timeRangeToUTC(timeRange);
  // chart state
  const [newChart, setNewChart] = React.useState(null);
  const [loadedCharts, setLoadedChart] = React.useState(
    createStateById(config, { time_start, time_stop }),
  );
  const [isHovered, onHover] = React.useState(false);
  const [loadedStreams, fetchStreams] = useStreams(api);
  // holds every loaded graph as keys
  const [parsedCharts, setParsedCharts] = React.useState(
    stateAsArray(createStateById(config, { time_start, time_stop })),
  ); // state as array
  const [trackerData, setTrackerData] = React.useState([]);
  // params
  const debouncedTimeRange = useDebounce(JSON.stringify(timeRange), 1000);
  // user actions
  const onUserSetTimeRange = (newTimeRange: TimeRange): void => {
    cancelRequests(config);
    setTimeRange(newTimeRange);
    resetUserIsSleeping();
  };
  const onUserHoverTimeRange = (): void => {
    resetUserIsSleeping();
  };

  // fetch all charts
  const fetchChart = (chart, params): Promise<void> =>
    api
      .getTimeSeriesMultiple(params)
      .then((responses) => {
        // Filter out any errors
        const filteredResponses = responses.reduce(
          (obj, response) => {
            if (!response.data) {
              if (response.message !== SWErrors.USER_CANCELLED) {
                obj.errors.push(response);
              }
              return obj;
            }
            obj.success.push(response);
            return obj;
          },
          { errors: [], success: [] },
        );

        if (filteredResponses.success.length > 0) {
          const newLoadedChart = {
            ...chart,
            series: createChartSeriesData(filteredResponses.success, chart),
            params: [...params],
          };

          // when first time chart is loaded, trigger resize to prevent alignment issues
          if (!loadedCharts[chart.id].data[0].series.length) {
            window.dispatchEvent(new Event('resize'));
          }

          setNewChart(() => ({
            [chart.id]: {
              data: [newLoadedChart],
              isLoading: false,
              error:
                filteredResponses.errors.length > 0
                  ? filteredResponses.errors[0]
                  : null,
            },
          }));
        } else if (filteredResponses.errors.length > 0) {
          throw new Error(filteredResponses.errors[0]);
        }
      })
      .catch((error) => {
        if (error.message !== SWErrors.USER_CANCELLED) {
          setNewChart(() => ({
            [chart.id]: {
              data: [{ ...chart }],
              isLoading: false,
              error,
            },
          }));
        }
      });

  const refetchChartsWithData = (timeStart: string, timeStop: string): void => {
    cancelRequests(config);
    const chartsWithData = parsedCharts.reduce((list, item) => {
      if (item.data && item.data.length) {
        return list.concat(item.data[0]);
      }
      return list;
    }, []);

    chartsWithData.forEach((chart) =>
      fetchChart(
        chart,
        chart.params.map((param) => ({
          ...param,
          time_start: timeStart,
          time_stop: timeStop,
        })),
      ),
    );
  };

  // eslint-disable-next-line consistent-return
  const autoUpdate = async (): Promise<void> => {
    try {
      if (isUserSleeping) {
        setTimeRange(getDefaultTimeRange());
      }
      fetchStreams();
    } catch (error) {
      return null;
    }
  };

  // cancel requests on unmount
  React.useEffect(() => {
    return (): void => {
      cancelRequests(config);
    };
  }, []);

  // fetch charts on debounce
  React.useEffect(() => {
    // only refetch data when user is not sleeping
    if (isUserSleeping) {
      return;
    }

    // reset loading and error
    const newState = Object.keys(loadedCharts).reduce(
      (list, key) => ({
        ...list,
        [key]: {
          ...loadedCharts[key],
          isLoading: true,
          error: null,
        },
      }),
      {},
    );
    setLoadedChart(newState);
    // update parsed charts
    const chartsAsArray = stateAsArray(newState);
    setParsedCharts(chartsAsArray);
    // refetch data
    refetchChartsWithData(time_start, time_stop);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedTimeRange]);

  // sync state update
  React.useEffect(() => {
    if (newChart) {
      const newState = {
        ...loadedCharts,
        ...newChart,
      };
      // stores the new chart
      setLoadedChart(newState);
      // store state as array as well
      const chartsAsArray = stateAsArray(newState);
      setParsedCharts(chartsAsArray);
      // store state with data for timetracker
      const newTrackerData = chartsAsArray.map((chart) => chart.data[0]);
      setTrackerData(newTrackerData);
    }
    // whenever a new chart has been added async, update and parse the data of that
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newChart]);

  usePoller(
    [timeRange, debouncedTimeRange, parsedCharts, loadedCharts],
    autoUpdate,
    60000,
  );

  React.useEffect(() => {
    if (isUserSleeping) {
      // immediately jump to current date when fell asleep
      const newTimeRange = getDefaultTimeRange();
      setTimeRange(getDefaultTimeRange());
      // user can be in future/history/zoomed so refetch all data only once
      const [newTimeStart, newTimeEnd] = timeRangeToUTC(newTimeRange);
      refetchChartsWithData(newTimeStart, newTimeEnd);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isUserSleeping]);

  React.useEffect(() => {
    if (loadedStreams) {
      // find and update charts with new data
      parsedCharts.forEach((chart) => {
        const chartData = chart.data[0];
        const { stream } = chartData.params[0];
        const newStream = loadedStreams[stream];
        // stream has newer data, update the chart
        if (newStream) {
          const params = chartData.params.map((param) => ({
            ...param,
            time_stop: newStream.end,
          }));
          fetchChart({ ...chartData, end: newStream.end }, params);
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loadedStreams]);

  const lastIndexWithData = React.useMemo(
    () => getGraphIndexWithLastData(parsedCharts),
    [parsedCharts],
  );

  return (
    <TimeTrackerProvider>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <TimeSeriesHeader />
        </Grid>
        <Grid
          item
          xs={12}
          onMouseEnter={(): void => onHover(true)}
          onMouseLeave={(): void => onHover(false)}
        >
          <Tracker graphs={trackerData} />
          <Grid container alignItems="stretch" className={classes.container}>
            {isHovered && (
              <ResetTimeRangeButton
                timeRange={timeRange}
                onResetTimeRange={onUserSetTimeRange}
              />
            )}
            {parsedCharts.map((chart, index) => (
              <Grid
                item
                xs={12}
                // eslint-disable-next-line react/no-array-index-key
                key={`chart-wrapper-${index}`}
                style={{
                  height:
                    index === lastIndexWithData
                      ? graphHeight + 55 // to save space for timeline and title
                      : graphHeight + 28, // to save space for the title
                  position: 'relative',
                }}
              >
                {chart.isLoading && !chart.error && !hasSeries(chart) ? (
                  <>
                    <Typography
                      style={{
                        top: 0,
                        margin: 10,
                        position: 'absolute',
                      }}
                      variant="caption"
                    >{`Loading ${chart.data[0].title}`}</Typography>
                    <Skeleton
                      width="100%"
                      height={graphHeight}
                      variant="rectangular"
                    />
                  </>
                ) : null}
                {hasSeries(chart) ? (
                  <>
                    <GraphStatus
                      isLoading={chart.isLoading}
                      error={chart.error}
                    />
                    <GraphContainer
                      graphs={chart.data}
                      timeRange={timeRange}
                      setTimeRange={onUserSetTimeRange}
                      hideTimeAxis={index !== lastIndexWithData}
                      onHoverTracker={onUserHoverTimeRange}
                    />
                  </>
                ) : null}
                {chart.error && !hasSeries(chart) ? (
                  <AlertBanner
                    severity="error"
                    title={
                      chart.error.message
                        ? chart.error.message
                        : 'Failed to load chart'
                    }
                  />
                ) : null}
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </TimeTrackerProvider>
  );
};

export default TimeSeriesGraphs;
