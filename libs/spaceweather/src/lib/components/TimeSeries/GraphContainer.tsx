/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { ChartContainer, Resizable, ChartRow } from 'react-timeseries-charts';
import { TimeRange } from 'pondjs';
import { Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import moment from 'moment';
import AreaGraph from './AreaGraph';
import BarGraph from './BarGraph';
import LogarithmicGraph from './LogarithmicGraph';
import BandGraph from './BandGraph';
import { useTimeTrackerContext } from './TimeTrackerContext';
import { GraphItem } from './types';
import { trackerStyle } from './styles';

const useStyles = makeStyles({
  timeAxisLable: {
    position: 'relative',
    fontSize: '10px',
    lineHeight: 1.33,
    letterSpacing: '0.4px',
    color: '#051039',
    opacity: 0.7,
    marginTop: -25,
    background: 'white',
    width: 'fit-content',
  },
});

const renderGraph = (charts: GraphItem[]): typeof ChartRow =>
  charts.map((chart) => {
    if (chart.graphType === 'BAR') {
      return BarGraph(chart);
    }

    if (chart.graphType === 'BAND') {
      return BandGraph(chart);
    }

    if (chart.graphType === 'AREA') {
      return AreaGraph(chart);
    }

    if (chart.graphType === 'LOG') {
      return LogarithmicGraph(chart);
    }

    return null;
  });

const onFormatXAxis = (date: Date): string =>
  moment.utc(date).format('MM-DD HH:mm');

interface GraphContainerProps {
  timeRange: TimeRange;
  graphs?: GraphItem[];
  setTimeRange?: (timeRange: TimeRange) => void;
  children?: React.ReactNode;
  hideTimeAxis?: boolean;
  onHoverTracker?: () => void;
}

export const GraphContainer: React.FC<GraphContainerProps> = ({
  graphs,
  timeRange,
  setTimeRange = (): void => null,
  children,
  hideTimeAxis = false,
  onHoverTracker = null,
}: GraphContainerProps) => {
  const classes = useStyles();
  const { tracker, onChangeTracker } = useTimeTrackerContext();

  const onTrackerChanged = (date, scale): void => {
    onChangeTracker(date, scale);
    if (onHoverTracker) {
      onHoverTracker();
    }
  };

  return (
    <div data-testid="graph-container">
      <Resizable>
        <ChartContainer
          utc
          timeRange={timeRange}
          enablePanZoom
          onTimeRangeChanged={setTimeRange}
          trackerPosition={tracker}
          onTrackerChanged={onTrackerChanged}
          hideTimeAxis={hideTimeAxis}
          trackerStyle={trackerStyle}
          format={onFormatXAxis}
          timeAxisTickCount={6}
        >
          {graphs ? renderGraph(graphs) : children}
        </ChartContainer>
      </Resizable>
      {!hideTimeAxis ? (
        <Typography className={classes.timeAxisLable}>UTC time</Typography>
      ) : null}
    </div>
  );
};

export default GraphContainer;
