/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render } from '@testing-library/react';
import BandGraph from './BandGraph';
import { bandGraphWithSeries } from '../../utils/DummyData';
import { TimeTrackerProvider } from './TimeTrackerContext';
import { GraphContainer } from './GraphContainer';
import { defaultTimeRange } from '../../utils/defaultTimeRange';

describe('src/components/TimeSeries/BandGraph', () => {
  // Need to set the offsetWidth for Resizable to render it's children
  Object.defineProperty(HTMLElement.prototype, 'offsetWidth', {
    configurable: true,
    value: 500,
  });

  it('should show the graph title', () => {
    jest.spyOn(console, 'warn').mockImplementation();
    const { getByText } = render(
      <TimeTrackerProvider>
        <GraphContainer timeRange={defaultTimeRange}>
          {BandGraph(bandGraphWithSeries)}
        </GraphContainer>
      </TimeTrackerProvider>,
    );
    expect(getByText(bandGraphWithSeries.title)).toBeTruthy();
  });

  it('should show the given y axis range', () => {
    const { container } = render(
      <TimeTrackerProvider>
        <GraphContainer timeRange={defaultTimeRange}>
          {BandGraph(bandGraphWithSeries)}
        </GraphContainer>
      </TimeTrackerProvider>,
    );
    const yAxisValues = container.querySelectorAll('.yaxis .tick');
    expect(yAxisValues[0].textContent).toEqual('-20');
    expect(yAxisValues[yAxisValues.length - 1].textContent).toEqual('20');
  });

  it('should show the treshold when given', () => {
    const { getByText } = render(
      <TimeTrackerProvider>
        <GraphContainer timeRange={defaultTimeRange}>
          {BandGraph(bandGraphWithSeries)}
        </GraphContainer>
      </TimeTrackerProvider>,
    );

    expect(getByText(bandGraphWithSeries.threshold[0].title)).toBeTruthy();
  });
});
