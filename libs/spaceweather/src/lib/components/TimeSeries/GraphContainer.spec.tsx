/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render } from '@testing-library/react';
import moment from 'moment';
import {
  barGraphWithSeries,
  areaGraphWithSeries,
  logGraphWithSeries,
  bandGraphWithSeries,
} from '../../utils/DummyData';
import { TimeTrackerProvider } from './TimeTrackerContext';
import { GraphContainer } from './GraphContainer';
import {
  defaultTimeRange,
  beginTime,
  endTime,
} from '../../utils/defaultTimeRange';

interface ElementWithData extends Element {
  __data__: string;
}

describe('src/components/TimeSeries/GraphContainer', () => {
  // Need to set the offsetWidth for Resizable to render it's children
  Object.defineProperty(HTMLElement.prototype, 'offsetWidth', {
    configurable: true,
    value: 500,
  });

  it('should load all the graph types', () => {
    jest.spyOn(console, 'warn').mockImplementation();
    const { getByText } = render(
      <TimeTrackerProvider>
        <GraphContainer
          timeRange={defaultTimeRange}
          graphs={[
            barGraphWithSeries,
            areaGraphWithSeries,
            logGraphWithSeries,
            bandGraphWithSeries,
          ]}
        />
      </TimeTrackerProvider>,
    );
    expect(getByText(barGraphWithSeries.title)).toBeTruthy();
    expect(getByText(areaGraphWithSeries.title)).toBeTruthy();
    expect(getByText(logGraphWithSeries.title)).toBeTruthy();
    expect(getByText(bandGraphWithSeries.title)).toBeTruthy();
  });

  it('should show the default time range on the x axis', () => {
    const { container } = render(
      <TimeTrackerProvider>
        <GraphContainer
          timeRange={defaultTimeRange}
          graphs={[barGraphWithSeries]}
        />
      </TimeTrackerProvider>,
    );

    // Depending on the current time, either the first or second value of the x axis shows the begintime (other value shows '12 PM'). But when first of the month it will show only the month, so also check the third and fourth value.
    const xAxisValues = container.querySelectorAll('.x .tick');
    // clone moment object because of mutability
    const expectedTimeOnFirstOrSecondTick = beginTime
      .clone()
      .add(1, 'day')
      .format('DD');
    const expectedTimeOnThirdOrFourthTick = beginTime
      .clone()
      .add(2, 'days')
      .format('DD');
    const firstX = xAxisValues[0].textContent;
    const secondX = xAxisValues[1].textContent;
    const thirdX = xAxisValues[2].textContent;
    const fourthX = xAxisValues[3].textContent;

    expect(
      firstX.includes(expectedTimeOnFirstOrSecondTick) ||
        secondX.includes(expectedTimeOnFirstOrSecondTick) ||
        thirdX.includes(expectedTimeOnThirdOrFourthTick) ||
        fourthX.includes(expectedTimeOnThirdOrFourthTick),
    ).toBeTruthy();

    // Depending on the current time, either the last or second last value of the x axis shows the endtime (other value shows '12 PM'). But when first of the month it will show only the month, so also check the third and fourth value.
    const expectedEndTime = endTime.format('DD');
    const expectedEndTimeThirdOrFourth = endTime
      .subtract(1, 'day')
      .format('DD');
    const lastX = xAxisValues[xAxisValues.length - 1].textContent;
    const secondLastX = xAxisValues[xAxisValues.length - 2].textContent;
    const thirdLastX = xAxisValues[xAxisValues.length - 3].textContent;
    const fourthLastX = xAxisValues[xAxisValues.length - 4].textContent;
    expect(
      lastX.includes(expectedEndTime) ||
        secondLastX.includes(expectedEndTime) ||
        thirdLastX.includes(expectedEndTimeThirdOrFourth) ||
        fourthLastX.includes(expectedEndTimeThirdOrFourth),
    ).toBeTruthy();
  });

  it('should format the x axis time values', () => {
    const { container } = render(
      <TimeTrackerProvider>
        <GraphContainer
          timeRange={defaultTimeRange}
          graphs={[barGraphWithSeries]}
        />
      </TimeTrackerProvider>,
    );

    const xAxisValues = container.querySelectorAll('.x .tick');

    xAxisValues.forEach((xAxis: ElementWithData) =>
      expect(
        // eslint-disable-next-line no-underscore-dangle
        moment(xAxis.__data__, 'MM-DD HH:mm', true).isValid(),
      ).toBeTruthy(),
    );
  });
});
