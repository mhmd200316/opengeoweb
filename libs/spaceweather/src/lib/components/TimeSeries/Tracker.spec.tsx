/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render } from '@testing-library/react';
import moment from 'moment';
import Tracker from './Tracker';
import { TimeTrackerContext } from './TimeTrackerContext';
import { areaGraphWithSeries } from '../../utils/DummyData';

describe('src/components/TimeseriesNew/Tracker', () => {
  it('should show the date corresponding to the trackerposition', () => {
    const props = {
      graphs: [areaGraphWithSeries],
    };
    const value = {
      tracker: moment.utc().subtract('1', 'hours').toDate(),
      trackerX: 1,
    };

    const { getByText } = render(
      <TimeTrackerContext.Provider value={value}>
        <Tracker {...props} />
      </TimeTrackerContext.Provider>,
    );

    expect(
      getByText(
        moment.utc(value.tracker).format('YYYY-MM-DD HH:mm').concat(' UTC'),
      ),
    ).toBeTruthy();
  });

  it('should show the value and unit corresponding to the trackerposition', () => {
    const props = {
      graphs: [areaGraphWithSeries],
    };
    const value = {
      tracker: moment.utc().subtract('1', 'hours').toDate(),
      trackerX: 1,
      onChangeTracker: jest.fn(),
    };

    const { getByText } = render(
      <TimeTrackerContext.Provider value={value}>
        <Tracker {...props} />
      </TimeTrackerContext.Provider>,
    );

    expect(getByText('313.0 km/s')).toBeTruthy();
  });
});
