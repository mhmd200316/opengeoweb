/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { SWEvent, SWNotification, ThresholdUnits } from '../../types';
import {
  fakeEventList,
  mockEventAcknowledgedExternalDraft,
  mockEventExternalMultipleNotificationsWithoutInternalProvider,
  mockEventExternalWithEmptyInternalNotifications,
} from '../../utils/fakedata';
import {
  constructBaseNotification,
  formatDate,
  constructOutgoingNotification,
  getLatestNotification,
  getNewInternalStatusTagContent,
  constructRepopulateNotification,
  ConstructRepopulateNotificationType,
} from './utils';

describe('src/components/LifeCycleDialog/utils', () => {
  describe('formatDate', () => {
    it('should format date', () => {
      expect(formatDate('2020-07-12T04:28:00Z')).toEqual('2020-07-12 04:28');
    });
  });
  describe('getLatestNotification', () => {
    it('should return an empty object when no event or lifecycles', async () => {
      expect(getLatestNotification(null)).toEqual({});
      expect(getLatestNotification(undefined)).toEqual({});
      expect(getLatestNotification({} as SWEvent)).toEqual({});
      expect(getLatestNotification({ lifecycles: {} } as SWEvent)).toEqual({});
      expect(
        getLatestNotification({
          lifecycles: { mockLifecyle: {} } as unknown,
        } as SWEvent),
      ).toEqual({});
    });
    it('should return the last internal notification if present', async () => {
      expect(getLatestNotification(fakeEventList[0])).toEqual(
        fakeEventList[0].lifecycles.internalprovider.notifications[1],
      );
    });

    it('should return the last external notification if no internal provider present', async () => {
      expect(
        getLatestNotification(
          mockEventExternalMultipleNotificationsWithoutInternalProvider,
        ),
      ).toEqual(
        mockEventExternalMultipleNotificationsWithoutInternalProvider.lifecycles
          .externalprovider.notifications[1],
      );
    });
    it('should return the last external notification if no internal notifications present', async () => {
      expect(
        getLatestNotification(mockEventExternalWithEmptyInternalNotifications),
      ).toEqual(
        mockEventExternalWithEmptyInternalNotifications.lifecycles
          .externalprovider.notifications[1],
      );
    });
  });

  describe('constructBaseNotification', () => {
    it('should construct a new notification', () => {
      expect(constructBaseNotification({} as SWEvent, 'new')).toEqual(
        expect.objectContaining<SWNotification>({
          category: 'XRAY_RADIO_BLACKOUT',
          categorydetail: '',
          changestateto: 'issued',
          datasource: '',
          draft: false,
          impulsetime: '',
          initialgscale: '',
          label: 'ALERT',
          magnetometerdeflection: undefined,
          message: '',
          neweventend: '',
          neweventlevel: '',
          neweventstart: expect.any(String),
          observedpolaritybz: undefined,
          observedsolarwind: undefined,
          originator: 'KNMI',
          peakclass: '',
          peakflux: undefined,
          peakfluxtime: '',
          threshold: undefined,
          thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
          xrayclass: '',
        }),
      );
    });
    it('should set the startdate of a new notification to now minus 5 minutes', () => {
      const now = `2022-01-09T14:00:00Z`;
      jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
      const newNotification = constructBaseNotification({} as SWEvent, 'new');
      expect(newNotification.neweventstart).toEqual('2022-01-09 13:55');
    });
    it('should construct a draft notification', () => {
      expect(
        constructBaseNotification(mockEventAcknowledgedExternalDraft, 'draft'),
      ).toEqual({
        ...mockEventAcknowledgedExternalDraft.lifecycles.internalprovider
          .notifications[2],
        neweventstart: formatDate(
          mockEventAcknowledgedExternalDraft.lifecycles.internalprovider
            .notifications[2].neweventstart,
        ),
        originator: 'KNMI',
      });
    });
    it('should construct an internal notification based on the last external notification', () => {
      expect(
        constructBaseNotification(
          mockEventExternalWithEmptyInternalNotifications,
          'externalprovider',
        ),
      ).toEqual({
        ...mockEventExternalWithEmptyInternalNotifications.lifecycles
          .externalprovider.notifications[1],
        changestateto: '',
        neweventend: null,
        draft: false,
        originator: 'KNMI',
        title: '',
        message: '',
        eventid: mockEventExternalWithEmptyInternalNotifications.eventid,
        neweventstart: formatDate(
          mockEventExternalWithEmptyInternalNotifications.lifecycles
            .externalprovider.notifications[1].neweventstart,
        ),
      });
    });
    it('should construct an internal notification based on the last internal notification overwritten where necessary by the external notification if this exists', () => {
      // if internal message present this should be used
      expect(
        constructBaseNotification(fakeEventList[3], 'internalprovider'),
      ).toEqual({
        ...fakeEventList[3].lifecycles.internalprovider.notifications[1],
        ...fakeEventList[3].lifecycles.externalprovider.notifications[1],
        title: '',
        message: '',
        categorydetail: '',
        eventid: fakeEventList[3].eventid,
        changestateto: '',
        originator: 'KNMI',
        draft: false,
      });
      expect(
        constructBaseNotification(fakeEventList[5], 'internalprovider'),
      ).toEqual({
        ...fakeEventList[5].lifecycles.internalprovider.notifications[1],
        categorydetail: '',
        changestateto: '',
        originator: 'KNMI',
        title: '',
        message: '',
        eventid: fakeEventList[5].eventid,
        neweventstart: expect.any(String),
        neweventend: expect.any(String),
        draft: false,
      });
    });
    it('should use the eventid of the event, not of the notification', () => {
      // Change eventid on last notification
      mockEventExternalWithEmptyInternalNotifications.lifecycles.externalprovider.notifications[1].eventid =
        'RANDOM EVENT ID';
      expect(
        constructBaseNotification(
          mockEventExternalWithEmptyInternalNotifications,
          'externalprovider',
        ),
      ).toEqual({
        ...mockEventExternalWithEmptyInternalNotifications.lifecycles
          .externalprovider.notifications[1],
        changestateto: '',
        neweventend: null,
        draft: false,
        originator: 'KNMI',
        title: '',
        message: '',
        eventid: mockEventExternalWithEmptyInternalNotifications.eventid,
        neweventstart: formatDate(
          mockEventExternalWithEmptyInternalNotifications.lifecycles
            .externalprovider.notifications[1].neweventstart,
        ),
      });
    });
  });

  describe('getNewInternalStatusTagContent', () => {
    it('should derive the appropriate status tag content from the action and label', async () => {
      expect(getNewInternalStatusTagContent('Updateextend', 'WARNING')).toBe(
        'Warning',
      );
      expect(getNewInternalStatusTagContent('Updateextend', 'ALERT')).toBe(
        'Alert',
      );
      expect(getNewInternalStatusTagContent('Summarise', 'ALERT')).toBe(
        'Summary',
      );
      expect(getNewInternalStatusTagContent('Summarise', 'WARNING')).toBe(
        'Summary',
      );
      expect(getNewInternalStatusTagContent('Cancel', 'ALERT')).toBe(
        'Cancelled',
      );
      expect(getNewInternalStatusTagContent('Cancel', 'WARNING')).toBe(
        'Cancelled',
      );
      expect(getNewInternalStatusTagContent('None', 'WARNING')).toBe('Warning');
      expect(getNewInternalStatusTagContent('None', 'ALERT')).toBe('Alert');
      expect(
        getNewInternalStatusTagContent(
          'Updateextend',
          'WARNING',
          'PROTON_FLUX_10',
        ),
      ).toBe('Warning');
      expect(
        getNewInternalStatusTagContent(
          'Updateextend',
          'ALERT',
          'PROTON_FLUX_10',
        ),
      ).toBe('Alert');
      expect(
        getNewInternalStatusTagContent('Summarise', 'ALERT', 'PROTON_FLUX_10'),
      ).toBe('Summary');
      expect(
        getNewInternalStatusTagContent(
          'Summarise',
          'WARNING',
          'PROTON_FLUX_10',
        ),
      ).toBe('Summary');
      expect(
        getNewInternalStatusTagContent('Cancel', 'ALERT', 'PROTON_FLUX_10'),
      ).toBe('Cancelled');
      expect(
        getNewInternalStatusTagContent('Cancel', 'WARNING', 'PROTON_FLUX_10'),
      ).toBe('Cancelled');
      expect(
        getNewInternalStatusTagContent('None', 'WARNING', 'PROTON_FLUX_10'),
      ).toBe('Warning');
      expect(
        getNewInternalStatusTagContent('None', 'ALERT', 'PROTON_FLUX_10'),
      ).toBe('Alert');
      expect(
        getNewInternalStatusTagContent(
          'Updateextend',
          'WARNING',
          'GEOMAGNETIC_STORM',
        ),
      ).toBe('Watch');
      expect(
        getNewInternalStatusTagContent(
          'Updateextend',
          'ALERT',
          'GEOMAGNETIC_STORM',
        ),
      ).toBe('Alert');
      expect(
        getNewInternalStatusTagContent(
          'Summarise',
          'ALERT',
          'GEOMAGNETIC_STORM',
        ),
      ).toBe('Summary');
      expect(
        getNewInternalStatusTagContent(
          'Summarise',
          'WARNING',
          'GEOMAGNETIC_STORM',
        ),
      ).toBe('Summary');
      expect(
        getNewInternalStatusTagContent('Cancel', 'ALERT', 'GEOMAGNETIC_STORM'),
      ).toBe('Cancelled');
      expect(
        getNewInternalStatusTagContent(
          'Cancel',
          'WARNING',
          'GEOMAGNETIC_STORM',
        ),
      ).toBe('Cancelled');
      expect(
        getNewInternalStatusTagContent('None', 'WARNING', 'GEOMAGNETIC_STORM'),
      ).toBe('Watch');
      expect(
        getNewInternalStatusTagContent('None', 'ALERT', 'GEOMAGNETIC_STORM'),
      ).toBe('Alert');
    });
  });

  describe('constructOutgoingNotification', () => {
    it('should construct an outgoing notification for issuing a draft Geomagnetic Storm Watch', () => {
      const notification: SWNotification = {
        originator: null,
        category: 'GEOMAGNETIC',
        categorydetail: 'GEOMAGNETIC_STORM',
        label: 'WARNING',
        initialgscale: '2',
        neweventstart: '2021-07-12T11:55:00Z',
        neweventend: '2021-07-13T11:55:00Z',
        message: '2',
        changestateto: 'issued',
        draft: true,
        notificationid: 'adbfa280-e309-11eb-942d-bb62a1aa6f48',
        eventid: 'bb5c555d-e308-11eb-8f1c-9dd63e6a398e',
        issuetime: '2021-07-12T12:07:07Z',
      };

      const result = constructOutgoingNotification(
        notification,
        'Watch',
        false,
      );
      expect(result).toEqual({
        category: 'GEOMAGNETIC',
        categorydetail: 'GEOMAGNETIC_STORM',
        label: 'WARNING',
        initialgscale: '2',
        neweventstart: '2021-07-12T11:55:00Z',
        neweventend: '2021-07-13T11:55:00Z',
        message: '2',
        changestateto: 'issued',
        draft: false,
        notificationid: 'adbfa280-e309-11eb-942d-bb62a1aa6f48',
        eventid: 'bb5c555d-e308-11eb-8f1c-9dd63e6a398e',
        issuetime: '2021-07-12T12:07:07Z',
      });
    });
    it('should construct an outgoing notification for saving a Proton Flux Alert Summary as draft', () => {
      const notification: SWNotification = {
        category: 'PROTON_FLUX',
        categorydetail: 'PROTON_FLUX_100',
        changestateto: 'issued',
        datasource: 'SOURCE',
        draft: true,
        eventid: 'ee146e49-e301-11eb-aa23-c39d988d3180',
        issuetime: '2021-07-12T12:17:48Z',
        label: 'ALERT',
        message: 'efwerwersdfsdfsdfsdfsdf',
        neweventend: '2021-07-12 11:19',
        neweventstart: '2021-07-12 11:06',
        notificationid: 'fd2caafd-e301-11eb-be27-c39d988d3180',
        originator: null,
        threshold: 1000,
        thresholdunit: 'particles cm^-2 s^-1 sr^-1',
      };

      const result = constructOutgoingNotification(
        notification,
        'Summary',
        true,
      );
      expect(result).toEqual({
        category: 'PROTON_FLUX',
        categorydetail: 'PROTON_FLUX_100',
        changestateto: 'ended',
        datasource: 'SOURCE',
        draft: true,
        eventid: 'ee146e49-e301-11eb-aa23-c39d988d3180',
        issuetime: '2021-07-12T12:17:48Z',
        label: 'ALERT',
        message: 'efwerwersdfsdfsdfsdfsdf',
        neweventend: '2021-07-12T11:19:00Z',
        neweventstart: '2021-07-12T11:06:00Z',
        notificationid: 'fd2caafd-e301-11eb-be27-c39d988d3180',
        threshold: 1000,
        thresholdunit: 'particles cm^-2 s^-1 sr^-1',
      });
    });
  });
  describe('constructRepopulateNotification', () => {
    it('should construct a notification for retrieving the Geomagnetic Storm Watch templates', () => {
      const notification = {
        originator: null,
        category: 'GEOMAGNETIC',
        categorydetail: 'GEOMAGNETIC_STORM',
        label: 'WARNING',
        initialgscale: '2',
        neweventstart: '2021-07-12T11:55:00Z',
        neweventend: '2021-07-13T11:55:00Z',
        message: '2',
        changestateto: 'issued',
        draft: false,
        notificationid: 'adbfa280-e309-11eb-942d-bb62a1aa6f48',
        eventid: 'bb5c555d-e308-11eb-8f1c-9dd63e6a398e',
        issuetime: '2021-07-12T12:07:07Z',
        IS_POPULATE_REQUEST: true,
      };

      const result = constructRepopulateNotification(
        notification as ConstructRepopulateNotificationType,
      );
      expect(result).toEqual({
        category: 'GEOMAGNETIC',
        categorydetail: 'GEOMAGNETIC_STORM',
        label: 'WARNING',
        initialgscale: '2',
        neweventstart: '2021-07-12T11:55:00Z',
        neweventend: '2021-07-13T11:55:00Z',
        message: '2',
        changestateto: '',
        draft: false,
        notificationid: 'adbfa280-e309-11eb-942d-bb62a1aa6f48',
        eventid: 'bb5c555d-e308-11eb-8f1c-9dd63e6a398e',
        issuetime: '2021-07-12T12:07:07Z',
      });
    });
    it('should construct a notification for retrieving the templates for a Proton Flux Alert Summary ', () => {
      const notification = {
        category: 'PROTON_FLUX',
        categorydetail: 'PROTON_FLUX_100',
        changestateto: 'issued',
        datasource: 'SOURCE',
        draft: false,
        eventid: 'ee146e49-e301-11eb-aa23-c39d988d3180',
        issuetime: '2021-07-12T12:17:48Z',
        label: 'ALERT',
        message: 'efwerwersdfsdfsdfsdfsdf',
        neweventend: '2021-07-12 11:19',
        neweventstart: '2021-07-12 11:06',
        notificationid: 'fd2caafd-e301-11eb-be27-c39d988d3180',
        originator: null,
        threshold: 1000,
        thresholdunit: 'particles cm^-2 s^-1 sr^-1',
        IS_POPULATE_REQUEST: true,
      };

      const result = constructRepopulateNotification(
        notification as ConstructRepopulateNotificationType,
        'Summarise',
      );
      expect(result).toEqual({
        category: 'PROTON_FLUX',
        categorydetail: 'PROTON_FLUX_100',
        changestateto: 'ended',
        datasource: 'SOURCE',
        draft: false,
        eventid: 'ee146e49-e301-11eb-aa23-c39d988d3180',
        issuetime: '2021-07-12T12:17:48Z',
        label: 'ALERT',
        message: 'efwerwersdfsdfsdfsdfsdf',
        neweventend: '2021-07-12T11:19:00Z',
        neweventstart: '2021-07-12T11:06:00Z',
        notificationid: 'fd2caafd-e301-11eb-be27-c39d988d3180',
        threshold: 1000,
        thresholdunit: 'particles cm^-2 s^-1 sr^-1',
      });
    });
    it('should construct a notification for retrieving the Geomagnetic Storm Watch Cancellation templates', () => {
      const notification = {
        originator: null,
        category: 'GEOMAGNETIC',
        categorydetail: 'GEOMAGNETIC_STORM',
        label: 'WARNING',
        initialgscale: '2',
        neweventstart: '2021-07-12T11:55:00Z',
        neweventend: '2021-07-13T11:55:00Z',
        message: '2',
        changestateto: 'issued',
        draft: false,
        notificationid: 'adbfa280-e309-11eb-942d-bb62a1aa6f48',
        eventid: 'bb5c555d-e308-11eb-8f1c-9dd63e6a398e',
        issuetime: '2021-07-12T12:07:07Z',
        IS_POPULATE_REQUEST: true,
      };

      const result = constructRepopulateNotification(
        notification as ConstructRepopulateNotificationType,
        'Cancel',
      );
      expect(result).toEqual({
        category: 'GEOMAGNETIC',
        categorydetail: 'GEOMAGNETIC_STORM',
        label: 'WARNING',
        initialgscale: '2',
        neweventstart: '2021-07-12T11:55:00Z',
        neweventend: '2021-07-13T11:55:00Z',
        message: '2',
        changestateto: 'ended',
        draft: false,
        notificationid: 'adbfa280-e309-11eb-942d-bb62a1aa6f48',
        eventid: 'bb5c555d-e308-11eb-8f1c-9dd63e6a398e',
        issuetime: '2021-07-12T12:07:07Z',
      });
    });
  });
});
