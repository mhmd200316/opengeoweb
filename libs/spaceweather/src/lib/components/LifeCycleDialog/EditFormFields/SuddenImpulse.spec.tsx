/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import SuddenImpulse, {
  POLARITY_MIN_MAX_MESSAGE,
  SOLAR_WIND_MIN_MAX_MESSAGE,
} from './SuddenImpulse';

describe('components/LifeCycleDialog/EditFormFields/SuddenImpulse', () => {
  it('should show an error message when invalid polarity is entered', async () => {
    const { getByTestId, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            label: 'Warning',
          },
        }}
      >
        <SuddenImpulse />
      </ReactHookFormProvider>,
    );
    const polarityInput = getByTestId('observedpolaritybz-input');
    fireEvent.change(polarityInput, { target: { value: '-1001' } });
    await waitFor(() =>
      expect(queryByText(POLARITY_MIN_MAX_MESSAGE)).toBeTruthy(),
    );
    fireEvent.change(polarityInput, { target: { value: '-1000' } });
    await waitFor(() =>
      expect(queryByText(POLARITY_MIN_MAX_MESSAGE)).toBeFalsy(),
    );
    fireEvent.change(polarityInput, { target: { value: '1001' } });
    await waitFor(() =>
      expect(queryByText(POLARITY_MIN_MAX_MESSAGE)).toBeTruthy(),
    );
    fireEvent.change(polarityInput, { target: { value: '1000' } });
    await waitFor(() =>
      expect(queryByText(POLARITY_MIN_MAX_MESSAGE)).toBeFalsy(),
    );
  });
  it('should show an error message when invalid solar wind is entered', async () => {
    const { getByTestId, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            label: 'Warning',
          },
        }}
      >
        <SuddenImpulse />
      </ReactHookFormProvider>,
    );
    const solarWindInput = getByTestId('observedsolarwind-input');
    fireEvent.change(solarWindInput, { target: { value: '99' } });
    await waitFor(() =>
      expect(queryByText(SOLAR_WIND_MIN_MAX_MESSAGE)).toBeTruthy(),
    );
    fireEvent.change(solarWindInput, { target: { value: '100' } });
    await waitFor(() =>
      expect(queryByText(SOLAR_WIND_MIN_MAX_MESSAGE)).toBeFalsy(),
    );
    fireEvent.change(solarWindInput, { target: { value: '10001' } });
    await waitFor(() =>
      expect(queryByText(SOLAR_WIND_MIN_MAX_MESSAGE)).toBeTruthy(),
    );
    fireEvent.change(solarWindInput, { target: { value: '10000' } });
    await waitFor(() =>
      expect(queryByText(SOLAR_WIND_MIN_MAX_MESSAGE)).toBeFalsy(),
    );
  });
});
