/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid } from '@mui/material';
import { CalendarToday } from '@opengeoweb/theme';
import { useFormContext } from 'react-hook-form';
import moment from 'moment';
import {
  ReactHookFormDateTime,
  isValidDate,
  ReactHookFormHiddenInput,
  isAfter,
} from '@opengeoweb/form-fields';

import { useStyles } from './EditFormFields.styles';
import { dateFormat } from '../../../types';
import { isRecentPast, isInPastOrFutureAllowed } from './validations';
import { DATE_INPUT_MASK } from '../utils';

export const MESSAGE_END_DATE_IN_FUTURE = 'End date should be in the future';
export const MESSAGE_END_DATE_AFTER_START_DATE =
  'End date should be after start date';

export const isInFuture = (
  ownDate: string,
  isAllowed: boolean,
): boolean | string => {
  if (!ownDate || isAllowed) return true;
  return (
    moment.utc().isBefore(moment.utc(ownDate)) || MESSAGE_END_DATE_IN_FUTURE
  );
};

export const isAfterStart = (
  ownDate: string,
  otherDate: string,
): boolean | string => {
  if (!ownDate) return true;
  return isAfter(ownDate, otherDate) || MESSAGE_END_DATE_AFTER_START_DATE;
};

export const isEndDateRequired = (
  ownValue: string,
  label: string,
  actionMode?: string,
): boolean | string => {
  if (ownValue) return true;
  if (label === 'WARNING' || actionMode === 'Summarise') {
    return 'This field is required';
  }
  return true;
};

interface StartTimeEndTimeProps {
  actionMode: string;
}

const StartTimeEndTime: React.FC<StartTimeEndTimeProps> = ({
  actionMode,
}: StartTimeEndTimeProps) => {
  const classes = useStyles();
  const { watch, trigger, getValues } = useFormContext();

  const label = watch('label');
  const allowEndDateInPast =
    actionMode === 'Summarise' || actionMode === 'Cancel';

  return (
    <Grid container align-items="flex-start">
      <Grid item xs={5}>
        <ReactHookFormDateTime
          name="neweventstart"
          label="Start (UTC Time)"
          format={dateFormat}
          data-testid="start-date-picker"
          className={classes.inputField}
          components={{
            OpenPickerIcon: (): React.ReactElement => (
              <CalendarToday fontSize="small" />
            ),
          }}
          openTo="day"
          InputAdornmentProps={{ position: 'end' }}
          rules={{
            required: true,
            validate: {
              isValidDate,
              isInPastOrFutureAllowed: (value): boolean | string =>
                isInPastOrFutureAllowed(
                  value,
                  getValues('label') === 'WARNING',
                ),
              isRecentPast,
            },
          }}
          onChange={(): void => {
            trigger('neweventend');
          }}
          mask={DATE_INPUT_MASK}
        />
      </Grid>
      <Grid item xs={5}>
        {actionMode === 'Summarise' || label === 'WARNING' ? (
          <ReactHookFormDateTime
            name="neweventend"
            label="End (UTC Time)"
            disablePast={!allowEndDateInPast}
            format={dateFormat}
            data-testid="end-date-picker"
            className={classes.inputField}
            components={{
              OpenPickerIcon: (): React.ReactElement => (
                <CalendarToday fontSize="small" />
              ),
            }}
            openTo="day"
            InputAdornmentProps={{ position: 'end' }}
            rules={{
              validate: {
                isRequired: (value): boolean | string =>
                  isEndDateRequired(value, label, actionMode),
                isValidDate,
                isAfterStart: (value): boolean | string =>
                  isAfterStart(value, getValues('neweventstart')),
                isInFuture: (value): boolean | string =>
                  isInFuture(value, allowEndDateInPast),
                isRecentPast,
              },
            }}
            mask={DATE_INPUT_MASK}
          />
        ) : (
          <ReactHookFormHiddenInput name="neweventend" />
        )}
      </Grid>
    </Grid>
  );
};

export default StartTimeEndTime;
