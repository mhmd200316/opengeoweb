/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, MenuItem } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import {
  ReactHookFormSelect,
  ReactHookFormTextField,
  ReactHookFormNumberField,
} from '@opengeoweb/form-fields';

import { useStyles } from './EditFormFields.styles';
import {
  ThresholdValues,
  EventLevels,
  XrayClasses,
  ThresholdUnits,
} from '../../../types';

const Threshold: React.FC = () => {
  const classes = useStyles();
  const { watch, setValue } = useFormContext();

  const category = watch('category');
  const categorydetail = watch('categorydetail');

  React.useEffect(() => {
    const unit =
      category === 'ELECTRON_FLUX'
        ? ThresholdUnits[categorydetail]
        : ThresholdUnits[category];
    setValue('thresholdunit', unit);
  }, [category, categorydetail, setValue]);

  return (
    <Grid container>
      <Grid item xs={5}>
        {category === 'XRAY_RADIO_BLACKOUT' ||
        category === 'PROTON_FLUX' ||
        categorydetail === 'KP_INDEX' ? (
          <ReactHookFormSelect
            name="threshold"
            label="Threshold"
            className={classes.inputField}
            inputProps={{
              SelectDisplayProps: {
                'data-testid': 'threshold-select',
              },
            }}
            rules={{ required: true }}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
              const index = ThresholdValues[category].findIndex(
                (value: string) => value === event.target.value,
              );
              setValue('neweventlevel', EventLevels[category][index], {
                shouldValidate: true,
              });
              if (category === 'XRAY_RADIO_BLACKOUT') {
                setValue('xrayclass', XrayClasses[index], {
                  shouldValidate: true,
                });
              }
            }}
          >
            {ThresholdValues[category].map((value) => (
              <MenuItem value={value} key={value}>
                {value}
              </MenuItem>
            ))}
          </ReactHookFormSelect>
        ) : (
          <ReactHookFormNumberField
            name="threshold"
            inputMode="decimal"
            className={classes.inputField}
            inputProps={{ 'data-testid': 'threshold-input' }}
            label="Threshold"
            rules={{
              required: true,
            }}
          />
        )}
      </Grid>
      {category !== 'GEOMAGNETIC' && (
        <Grid item xs={5}>
          <ReactHookFormTextField
            name="thresholdunit"
            inputProps={{ 'data-testid': 'thresholdunit-input' }}
            label="Threshold unit"
            className={classes.inputField}
            disabled
            rules={{
              required: true,
            }}
          />
        </Grid>
      )}
    </Grid>
  );
};

export default Threshold;
