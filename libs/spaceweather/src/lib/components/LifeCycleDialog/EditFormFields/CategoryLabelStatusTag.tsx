/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, MenuItem } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormSelect } from '@opengeoweb/form-fields';
import { StatusTag } from '@opengeoweb/shared';

import { useStyles } from './EditFormFields.styles';
import {
  EventCategory,
  EventCategoryDetail,
  EventCategoryDetails,
  EventLabels,
  NotificationLabel,
} from '../../../types';
import { getNewInternalStatusTagContent } from '../utils';
import { getStatusTagColorFromContent } from '../../Notifications/utils';
import { isCategoryDetailRequired } from './validations';

interface CategoryLabelProps {
  statusTagText: string;
  setStatusTagText: React.Dispatch<React.SetStateAction<string>>;
}

const CategoryLabelStatusTag: React.FC<CategoryLabelProps> = ({
  statusTagText,
  setStatusTagText,
}: CategoryLabelProps) => {
  const classes = useStyles();
  const { setValue, watch, trigger } = useFormContext();

  const category = watch('category');
  const categorydetail = watch('categorydetail');

  return (
    <>
      <Grid container>
        <Grid item xs={5}>
          <ReactHookFormSelect
            name="category"
            label="Category"
            className={classes.inputField}
            inputProps={{
              SelectDisplayProps: {
                'data-testid': 'category-select',
              },
            }}
            rules={{ required: true }}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
              setValue('neweventlevel', '');
              setValue('threshold', '');
              const firstCategoryDetail =
                EventCategoryDetails[event.target.value][0];
              setValue('categorydetail', firstCategoryDetail);
              setValue('label', EventLabels[firstCategoryDetail][0]);
              setStatusTagText(
                getNewInternalStatusTagContent(
                  '',
                  EventLabels[firstCategoryDetail][0],
                  firstCategoryDetail,
                ),
              );
              trigger('neweventstart');
            }}
          >
            {Object.keys(EventCategory).map((key) => (
              <MenuItem value={key} key={key}>
                {EventCategory[key]}
              </MenuItem>
            ))}
          </ReactHookFormSelect>
        </Grid>
        <Grid item xs={5}>
          <ReactHookFormSelect
            name="categorydetail"
            label="Category detail"
            className={
              category !== 'XRAY_RADIO_BLACKOUT'
                ? classes.inputField
                : classes.hide
            }
            disabled={category === 'XRAY_RADIO_BLACKOUT'}
            inputProps={{
              SelectDisplayProps: {
                'data-testid': 'categorydetail-select',
              },
            }}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
              setValue('label', EventLabels[event.target.value][0]);
              setStatusTagText(
                getNewInternalStatusTagContent(
                  '',
                  EventLabels[event.target.value][0],
                  event.target.value,
                ),
              );
              trigger('neweventstart');
            }}
            rules={{
              validate: {
                isRequired: (value: string): boolean | string =>
                  isCategoryDetailRequired(value, category),
              },
            }}
          >
            {EventCategoryDetails[category].map((detail: string) => (
              <MenuItem value={detail} key={detail}>
                {EventCategoryDetail[detail]}
              </MenuItem>
            ))}
          </ReactHookFormSelect>
        </Grid>
        <Grid item xs={2} data-testid="notification-tag">
          <StatusTag
            content={statusTagText}
            color={getStatusTagColorFromContent(statusTagText)}
          />
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={5}>
          <ReactHookFormSelect
            name="label"
            label="Label"
            className={classes.inputField}
            inputProps={{
              SelectDisplayProps: {
                'data-testid': 'label-select',
              },
            }}
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
              setStatusTagText(
                event.target.value === 'WARNING' ? 'Warning' : 'Alert',
              );
              trigger('neweventstart');
            }}
            rules={{ required: true }}
          >
            {EventLabels[categorydetail].map((label) => (
              <MenuItem value={label} key={label}>
                {categorydetail === 'GEOMAGNETIC_STORM'
                  ? 'Watch'
                  : NotificationLabel[label]}
              </MenuItem>
            ))}
          </ReactHookFormSelect>
        </Grid>
      </Grid>
    </>
  );
};

export default CategoryLabelStatusTag;
