/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, MenuItem } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import {
  ReactHookFormSelect,
  ReactHookFormTextField,
} from '@opengeoweb/form-fields';

import { useStyles } from './EditFormFields.styles';
import { ThresholdValues, EventLevels, XrayClasses } from '../../../types';

export const PEAK_CLASS_MESSAGE =
  'Peak Class should start with M or X, followed by a positive number with one decimal';

export const isValidPeakClass = (value: string): boolean | string => {
  const regexPeakClass = /^[MX]\d+\.\d$/;
  return regexPeakClass.test(value) ? true : PEAK_CLASS_MESSAGE;
};

interface XrayClassPeakClassProps {
  actionMode: string;
}

const XrayClassPeakClass: React.FC<XrayClassPeakClassProps> = ({
  actionMode,
}: XrayClassPeakClassProps) => {
  const classes = useStyles();
  const { watch, setValue } = useFormContext();

  const category = watch('category');

  return (
    <Grid container>
      <Grid item xs={5}>
        <ReactHookFormSelect
          name="xrayclass"
          label="Xray Class"
          className={classes.inputField}
          inputProps={{
            SelectDisplayProps: {
              'data-testid': 'xrayclass-select',
            },
          }}
          rules={{ required: true }}
          onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
            const index = XrayClasses.findIndex(
              (item) => item === event.target.value,
            );
            setValue('neweventlevel', EventLevels[category][index], {
              shouldValidate: true,
            });
            setValue('threshold', ThresholdValues[category][index], {
              shouldValidate: true,
            });
          }}
        >
          {Object.keys(XrayClasses).map((key) => (
            <MenuItem value={XrayClasses[key]} key={key}>
              {XrayClasses[key]}
            </MenuItem>
          ))}
        </ReactHookFormSelect>
      </Grid>
      {actionMode === 'Summarise' && (
        <Grid item xs={5}>
          <ReactHookFormTextField
            name="peakclass"
            className={classes.inputField}
            inputProps={{ 'data-testid': 'peakclass-input' }}
            label="Peak Class"
            upperCase
            rules={{
              required: true,
              validate: {
                isValidPeakClass,
              },
            }}
          />
        </Grid>
      )}
    </Grid>
  );
};

export default XrayClassPeakClass;
