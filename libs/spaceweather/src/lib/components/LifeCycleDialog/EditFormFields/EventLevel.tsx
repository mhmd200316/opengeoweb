/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid, MenuItem } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormSelect } from '@opengeoweb/form-fields';

import { useStyles } from './EditFormFields.styles';
import { ThresholdValues, EventLevels, XrayClasses } from '../../../types';

const EventLevel: React.FC = () => {
  const classes = useStyles();
  const { watch, setValue } = useFormContext();

  const category = watch('category');
  const categorydetail = watch('categorydetail');

  return (
    <Grid container>
      <Grid item xs={5}>
        <ReactHookFormSelect
          name="neweventlevel"
          label="Event level"
          className={classes.inputField}
          inputProps={{
            SelectDisplayProps: {
              'data-testid': 'eventlevel-select',
            },
          }}
          rules={{ required: true }}
          onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
            if (
              category === 'XRAY_RADIO_BLACKOUT' ||
              categorydetail === 'PROTON_FLUX_10' ||
              categorydetail === 'KP_INDEX'
            ) {
              const index = EventLevels[category].findIndex(
                (level: string) => level === event.target.value,
              );
              setValue('threshold', ThresholdValues[category][index], {
                shouldValidate: true,
              });
              if (category === 'XRAY_RADIO_BLACKOUT') {
                setValue('xrayclass', XrayClasses[index], {
                  shouldValidate: true,
                });
              }
            }
          }}
        >
          {EventLevels[category] &&
            EventLevels[category].map((eventlevel: string) => (
              <MenuItem value={eventlevel} key={eventlevel}>
                {eventlevel}
              </MenuItem>
            ))}
        </ReactHookFormSelect>
      </Grid>
    </Grid>
  );
};

export default EventLevel;
