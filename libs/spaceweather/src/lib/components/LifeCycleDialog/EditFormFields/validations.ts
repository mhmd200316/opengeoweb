/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { errorMessages, isAfter } from '@opengeoweb/form-fields';
import moment from 'moment';
import { dateFormat } from '../../../types';

export const MESSAGE_RECENT_DATE =
  'Date should not be more than 1 week in the past';
export const MESSAGE_DATE_IN_PAST = 'Date should be in the past';

export const isCategoryDetailRequired = (
  value: string,
  category: string,
): boolean | string => {
  if (category !== 'XRAY_RADIO_BLACKOUT' && !value) {
    return errorMessages.required;
  }
  return true;
};

export const isRecentPast = (ownDate: string): boolean | string => {
  if (!ownDate) return true;
  const oneWeekAgo = moment.utc().subtract(1, 'week').format(dateFormat);
  return isAfter(ownDate, oneWeekAgo) || MESSAGE_RECENT_DATE;
};

export const isInPastOrFutureAllowed = (
  ownDate: string,
  isFutureAllowed = false,
): boolean | string => {
  if (!ownDate || isFutureAllowed) return true;
  return moment.utc().isAfter(ownDate) || MESSAGE_DATE_IN_PAST;
};
