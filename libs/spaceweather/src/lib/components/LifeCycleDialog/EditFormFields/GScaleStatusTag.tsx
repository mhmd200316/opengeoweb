/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormTextField } from '@opengeoweb/form-fields';
import { StatusTag } from '@opengeoweb/shared';

import { useStyles } from './EditFormFields.styles';
import { getNewInternalStatusTagContent } from '../utils';
import { getStatusTagColorFromContent } from '../../Notifications/utils';

interface GScaleStatusTagProps {
  statusTagText: string;
  actionMode: string;
  eventTypeDisabled: boolean;
}

const GScaleStatusTag: React.FC<GScaleStatusTagProps> = ({
  statusTagText,
  actionMode,
  eventTypeDisabled,
}: GScaleStatusTagProps) => {
  const classes = useStyles();
  const { watch } = useFormContext();

  const label = watch('label');
  const categorydetail = watch('categorydetail');

  return (
    <Grid container>
      <Grid item xs={10}>
        <ReactHookFormTextField
          name="initialgscale"
          className={classes.inputField}
          inputProps={{ 'data-testid': 'initialgscale-input' }}
          label="Initial G-scale"
          rules={{
            required: true,
          }}
        />
      </Grid>
      {eventTypeDisabled && (
        <Grid item xs={2} data-testid="notification-tag">
          <StatusTag
            content={getNewInternalStatusTagContent(
              actionMode,
              label,
              categorydetail,
            )}
            color={getStatusTagColorFromContent(statusTagText)}
          />
        </Grid>
      )}
    </Grid>
  );
};

export default GScaleStatusTag;
