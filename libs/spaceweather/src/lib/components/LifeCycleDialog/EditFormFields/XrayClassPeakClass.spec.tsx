/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';

import XrayClassPeakClass, {
  isValidPeakClass,
  PEAK_CLASS_MESSAGE,
} from './XrayClassPeakClass';

describe('components/LifeCycleDialog/EditFormFields/XrayClassPeakClass', () => {
  it('should show an error message when invalid peak class is entered', async () => {
    const { getByTestId, queryByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
          defaultValues: {
            category: 'XRAY_RADIO_BLACKOUT',
          },
        }}
      >
        <XrayClassPeakClass actionMode="Summarise" />
      </ReactHookFormProvider>,
    );
    const peakClassInput = getByTestId('peakclass-input');
    fireEvent.change(peakClassInput, { target: { value: '5.5' } });
    await waitFor(() => expect(queryByText(PEAK_CLASS_MESSAGE)).toBeTruthy());
    fireEvent.change(peakClassInput, { target: { value: 'm5.5' } });
    await waitFor(() => expect(queryByText(PEAK_CLASS_MESSAGE)).toBeFalsy());
    // should convert to uppercase
    expect(peakClassInput.getAttribute('value')).toEqual('M5.5');
    fireEvent.change(peakClassInput, { target: { value: 'X5.10' } });
    await waitFor(() => expect(queryByText(PEAK_CLASS_MESSAGE)).toBeTruthy());
    fireEvent.change(peakClassInput, { target: { value: 'X10.7' } });
    await waitFor(() => expect(queryByText(PEAK_CLASS_MESSAGE)).toBeFalsy());
    fireEvent.change(peakClassInput, { target: { value: 'X10' } });
    await waitFor(() => expect(queryByText(PEAK_CLASS_MESSAGE)).toBeTruthy());
  });
  describe('isValidPeakClass', () => {
    it('should return an error message for invalid peak class value', () => {
      expect(isValidPeakClass('1')).toEqual(PEAK_CLASS_MESSAGE);
      expect(isValidPeakClass('M5')).toEqual(PEAK_CLASS_MESSAGE);
      expect(isValidPeakClass('x10.2')).toEqual(PEAK_CLASS_MESSAGE);
      expect(isValidPeakClass('m5.5')).toEqual(PEAK_CLASS_MESSAGE);
      expect(isValidPeakClass('M.4')).toEqual(PEAK_CLASS_MESSAGE);
      expect(isValidPeakClass('X10')).toEqual(PEAK_CLASS_MESSAGE);
      expect(isValidPeakClass('X10,2')).toEqual(PEAK_CLASS_MESSAGE);
      expect(isValidPeakClass('X1.20')).toEqual(PEAK_CLASS_MESSAGE);
    });
    it('should return true for a valid peak class value', () => {
      expect(isValidPeakClass('M5.5')).toEqual(true);
      expect(isValidPeakClass('X10.2')).toEqual(true);
    });
  });
});
