/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid } from '@mui/material';
import { CalendarToday } from '@opengeoweb/theme';
import { useFormContext } from 'react-hook-form';
import {
  ReactHookFormDateTime,
  ReactHookFormNumberField,
  isValidDate,
} from '@opengeoweb/form-fields';

import { useStyles } from './EditFormFields.styles';
import { dateFormat } from '../../../types';
import { isInPastOrFutureAllowed, isRecentPast } from './validations';
import { DATE_INPUT_MASK } from '../utils';

export const POLARITY_MIN_MAX_MESSAGE =
  'Polarity should be between -1000 and 1000';
export const SOLAR_WIND_MIN_MAX_MESSAGE =
  'Solar wind should be between 100 and 10000';

const SuddenImpulse: React.FC = () => {
  const classes = useStyles();
  const { watch } = useFormContext();

  const label = watch('label');

  return label === 'ALERT' ? (
    <Grid container>
      <Grid item xs={5}>
        <ReactHookFormDateTime
          name="impulsetime"
          label="Impulse time"
          format={dateFormat}
          data-testid="impulsetime-picker"
          className={classes.inputField}
          components={{
            OpenPickerIcon: (): React.ReactElement => (
              <CalendarToday fontSize="small" />
            ),
          }}
          openTo="day"
          InputAdornmentProps={{ position: 'end' }}
          disableFuture
          rules={{
            required: true,
            validate: {
              isValidDate,
              isInPastOrFutureAllowed,
              isRecentPast,
            },
          }}
          mask={DATE_INPUT_MASK}
        />
      </Grid>
      <Grid item xs={5}>
        <ReactHookFormNumberField
          name="magnetometerdeflection"
          inputMode="decimal"
          className={classes.inputField}
          inputProps={{
            'data-testid': 'magnetometerdeflection-input',
          }}
          label="Magnetometer deflection"
          rules={{
            required: true,
          }}
        />
      </Grid>
    </Grid>
  ) : (
    <>
      <Grid container>
        <Grid item xs={11}>
          <ReactHookFormDateTime
            name="shocktime"
            label="Shock time"
            format={dateFormat}
            data-testid="shocktime-picker"
            className={classes.inputField}
            components={{
              OpenPickerIcon: (): React.ReactElement => (
                <CalendarToday fontSize="small" />
              ),
            }}
            openTo="day"
            InputAdornmentProps={{ position: 'end' }}
            disableFuture
            rules={{
              required: true,
              validate: {
                isValidDate,
                isInPastOrFutureAllowed,
                isRecentPast,
              },
            }}
            mask={DATE_INPUT_MASK}
          />
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={5}>
          <ReactHookFormNumberField
            name="observedpolaritybz"
            inputMode="decimal"
            className={classes.inputField}
            inputProps={{
              'data-testid': 'observedpolaritybz-input',
            }}
            label="Observed polarity Bz"
            rules={{
              required: true,
              min: {
                value: -1000,
                message: POLARITY_MIN_MAX_MESSAGE,
              },
              max: {
                value: 1000,
                message: POLARITY_MIN_MAX_MESSAGE,
              },
            }}
          />
        </Grid>
        <Grid item xs={6}>
          <ReactHookFormNumberField
            name="observedsolarwind"
            inputMode="decimal"
            className={classes.inputFieldLongLabel}
            inputProps={{
              'data-testid': 'observedsolarwind-input',
            }}
            label="Observed solar wind speed"
            rules={{
              required: true,
              min: {
                value: 100,
                message: SOLAR_WIND_MIN_MAX_MESSAGE,
              },
              max: {
                value: 10000,
                message: SOLAR_WIND_MIN_MAX_MESSAGE,
              },
            }}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default SuddenImpulse;
