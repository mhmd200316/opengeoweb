/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import moment from 'moment';
import { dateFormat } from '../../../types';
import {
  isCategoryDetailRequired,
  isInPastOrFutureAllowed,
  isRecentPast,
  MESSAGE_DATE_IN_PAST,
  MESSAGE_RECENT_DATE,
} from './validations';

describe('src/components/LifeCycleDialog/EditFormFields/validations', () => {
  describe('isCategoryDetailRequired', () => {
    it('should give an error when category is other than XRAY_RADIO_BLACKOUT and field is empty', () => {
      expect(isCategoryDetailRequired('', 'GEOMAGNETIC')).toEqual(
        'This field is required',
      );
      expect(isCategoryDetailRequired('KP_INDEX', 'GEOMAGNETIC')).toEqual(true);
      expect(isCategoryDetailRequired('', 'XRAY_RADIO_BLACKOUT')).toEqual(true);
    });
  });
  describe('isRecentPast', () => {
    it('should give an error when date is more than 1 week in the past', () => {
      const date = moment
        .utc()
        .subtract(7, 'days')
        .subtract(1, 'hour')
        .format(dateFormat);
      expect(isRecentPast(date)).toEqual(MESSAGE_RECENT_DATE);
    });
    it('should not give an error when date is less than 1 week in the past', () => {
      const date = moment
        .utc()
        .subtract(6, 'days')
        .subtract(23, 'hours')
        .format(dateFormat);
      expect(isRecentPast(date)).toEqual(true);
    });
    it('should not give an error when date is in the future', () => {
      const date = moment.utc().add(1, 'hour').format(dateFormat);
      expect(isRecentPast(date)).toEqual(true);
    });
  });
  describe('isInPastOrFutureAllowed', () => {
    it('should give an error when date is not in the past', () => {
      const date = moment.utc().add(1, 'hour').format(dateFormat);
      expect(isInPastOrFutureAllowed(date)).toEqual(MESSAGE_DATE_IN_PAST);
    });
    it('should not give an error when date is in the past', () => {
      const date = moment.utc().subtract(1, 'hour').format(dateFormat);
      expect(isInPastOrFutureAllowed(date)).toEqual(true);
    });
  });
});
