/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid } from '@mui/material';
import { CalendarToday } from '@opengeoweb/theme';
import {
  ReactHookFormDateTime,
  ReactHookFormNumberField,
  isValidDate,
} from '@opengeoweb/form-fields';

import { useStyles } from './EditFormFields.styles';
import { dateFormat } from '../../../types';
import { isInPastOrFutureAllowed, isRecentPast } from './validations';
import { DATE_INPUT_MASK } from '../utils';

export const PEAK_FLUX_MIN_MESSAGE = 'Peak flux should be a positive number';

const PeakFlux: React.FC = () => {
  const classes = useStyles();

  return (
    <Grid container>
      <Grid item xs={5}>
        <ReactHookFormNumberField
          name="peakflux"
          inputMode="decimal"
          className={classes.inputField}
          inputProps={{ 'data-testid': 'peakflux-input' }}
          label="Peak Flux"
          rules={{
            required: true,
            min: {
              value: 0,
              message: PEAK_FLUX_MIN_MESSAGE,
            },
          }}
        />
      </Grid>
      <Grid item xs={5}>
        <ReactHookFormDateTime
          name="peakfluxtime"
          label="Peak Flux Time"
          format={dateFormat}
          data-testid="peakfluxtime-picker"
          className={classes.inputField}
          components={{
            OpenPickerIcon: (): React.ReactElement => (
              <CalendarToday fontSize="small" />
            ),
          }}
          InputAdornmentProps={{ position: 'end' }}
          openTo="day"
          disableFuture
          rules={{
            required: true,
            validate: {
              isValidDate,
              isInPastOrFutureAllowed,
              isRecentPast,
            },
          }}
          mask={DATE_INPUT_MASK}
        />
      </Grid>
    </Grid>
  );
};

export default PeakFlux;
