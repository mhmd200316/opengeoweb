/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid } from '@mui/material';
import { ReactHookFormTextField } from '@opengeoweb/form-fields';
import { StatusTag } from '@opengeoweb/shared';

import { useStyles } from './EditFormFields.styles';
import { getStatusTagColorFromContent } from '../../Notifications/utils';

interface DataSourceStatustagProps {
  statusTagText: string;
  eventTypeDisabled: boolean;
}

const DataSourceStatustag: React.FC<DataSourceStatustagProps> = ({
  statusTagText,
  eventTypeDisabled,
}: DataSourceStatustagProps) => {
  const classes = useStyles();

  return (
    <Grid container>
      <Grid item xs={10}>
        <ReactHookFormTextField
          name="datasource"
          className={classes.inputField}
          inputProps={{ 'data-testid': 'datasource-input' }}
          label="Source"
          rules={{
            required: true,
          }}
        />
      </Grid>
      {eventTypeDisabled && (
        <Grid item xs={2} data-testid="notification-tag">
          <StatusTag
            content={statusTagText}
            color={getStatusTagColorFromContent(statusTagText)}
          />
        </Grid>
      )}
    </Grid>
  );
};

export default DataSourceStatustag;
