/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import moment from 'moment';

import {
  SWNotification,
  SWEvent,
  EventCategory,
  NotificationLabel,
  ThresholdUnits,
  dateFormat,
} from '../../types';

export const DATE_INPUT_MASK = '____-__-__ __:__';

export const formatDate = (date: string): string =>
  moment.utc(date).format(dateFormat);

export const getLatestNotification = (event: SWEvent): SWNotification => {
  // get latest internal notification if there, otherwise latest external notification
  if (
    !event ||
    !event.lifecycles ||
    (!event.lifecycles.internalprovider && !event.lifecycles.externalprovider)
  ) {
    return {} as SWNotification;
  }
  return event.lifecycles.internalprovider &&
    event.lifecycles.internalprovider.notifications &&
    event.lifecycles.internalprovider.notifications.length
    ? event.lifecycles.internalprovider.notifications[
        event.lifecycles.internalprovider.notifications.length - 1
      ]
    : event.lifecycles.externalprovider.notifications[
        event.lifecycles.externalprovider.notifications.length - 1
      ];
};

export const constructBaseNotification = (
  event: SWEvent | null,
  mode = 'new',
): SWNotification => {
  switch (mode) {
    case 'draft': {
      const draftNotification =
        event.lifecycles.internalprovider.notifications.find(
          (notification) => notification.draft,
        );
      const neweventstart = formatDate(draftNotification.neweventstart);

      const neweventend =
        draftNotification.neweventend &&
        draftNotification.neweventend !== null &&
        draftNotification.neweventend !== ''
          ? formatDate(draftNotification.neweventend)
          : '';
      return {
        originator: 'KNMI',
        ...draftNotification,
        eventid: event.eventid,
        neweventstart,
        neweventend,
        // For backwards compatibility, if title is null or not set, replace with ''
        title: draftNotification.title || '',
        neweventlevel:
          draftNotification.neweventlevel !== null
            ? draftNotification.neweventlevel
            : '',
      };
    }
    case 'internalprovider': {
      const lifeCycle = event.lifecycles.internalprovider;
      const externalNotifications =
        event.lifecycles.externalprovider &&
        event.lifecycles.externalprovider.notifications
          ? event.lifecycles.externalprovider.notifications
          : [];
      const lastExternalNotification =
        externalNotifications.length > 0
          ? externalNotifications[externalNotifications.length - 1]
          : {};

      const neweventstart = formatDate(lifeCycle.eventstart);
      const neweventend =
        lifeCycle.eventend &&
        lifeCycle.eventend !== null &&
        lifeCycle.eventend !== ''
          ? formatDate(lifeCycle.eventend)
          : null;

      return {
        ...(lifeCycle.eventid !== null && { eventid: lifeCycle.eventid }),
        ...getLatestNotification(event),
        neweventstart,
        neweventend,
        // Overwrite any fields from the incoming message if it exists
        ...lastExternalNotification,
        eventid: event.eventid,
        category: event.category,
        categorydetail: event.categorydetail,
        label: lifeCycle.label,
        changestateto: '',
        draft: false,
        // Message and title will be autopopulated from the template so should be set to empty
        message: '',
        title: '',
        originator: 'KNMI',
      };
    }
    case 'externalprovider': {
      const lifeCycle = event.lifecycles.externalprovider;
      const neweventstart = formatDate(lifeCycle.eventstart);
      const neweventend =
        lifeCycle.eventend &&
        lifeCycle.eventend !== null &&
        lifeCycle.eventend !== ''
          ? formatDate(lifeCycle.eventend)
          : null;

      return {
        ...(lifeCycle.eventid !== null && { eventid: lifeCycle.eventid }),
        ...getLatestNotification(event),
        eventid: event.eventid,
        category: event.category,
        categorydetail: event.categorydetail,
        label: lifeCycle.label,
        changestateto: '',
        draft: false,
        neweventstart,
        neweventend,
        // Message and title will be autopopulated from the template so should be set to empty
        message: '',
        title: '',
        originator: 'KNMI',
      };
    }
    case 'new':
    default: {
      // Defaults for the date
      const now = moment.utc().subtract(5, 'minutes').format(dateFormat);
      return {
        category: Object.keys(EventCategory)[0],
        categorydetail: '',
        label: Object.keys(NotificationLabel)[0],
        draft: false,
        neweventstart: now,
        neweventend: '',
        changestateto: 'issued',
        threshold: undefined,
        neweventlevel: '',
        message: '',
        title: '',
        originator: 'KNMI',
        thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
        datasource: '',
        impulsetime: '',
        initialgscale: '',
        magnetometerdeflection: undefined,
        observedpolaritybz: undefined,
        observedsolarwind: undefined,
        peakclass: '',
        peakflux: undefined,
        peakfluxtime: '',
        xrayclass: '',
        shocktime: '',
      };
    }
  }
};

export const constructOutgoingNotification = (
  _notification: SWNotification,
  newState: string,
  draft = false,
): SWNotification => {
  // strip null values
  const notification = Object.keys(_notification).reduce((list, key) => {
    if (_notification[key] !== null) {
      return {
        ...list,
        [key]: _notification[key],
      };
    }
    return list;
  }, {}) as SWNotification;

  const changestateto =
    newState === 'Alert' || newState === 'Warning' || newState === 'Watch'
      ? 'issued'
      : 'ended';
  const neweventend =
    notification.neweventend &&
    notification.neweventend !== '' &&
    notification.neweventend !== null
      ? moment.utc(notification.neweventend).format()
      : '';

  return {
    ...notification,
    neweventstart: moment.utc(notification.neweventstart).format(),
    neweventend,
    draft,
    changestateto,
  };
};

export interface ConstructRepopulateNotificationType extends SWNotification {
  IS_POPULATE_REQUEST: boolean;
}

export const constructRepopulateNotification = (
  _notification: ConstructRepopulateNotificationType,
  actionMode = '',
): SWNotification => {
  const { IS_POPULATE_REQUEST, ...notificationToPost } = _notification;
  // strip null values
  const notification = Object.keys(notificationToPost).reduce((list, key) => {
    if (notificationToPost[key] !== null) {
      return {
        ...list,
        [key]: notificationToPost[key],
      };
    }
    return list;
  }, {}) as SWNotification;
  const neweventend =
    notification.neweventend &&
    notification.neweventend !== '' &&
    notification.neweventend !== null
      ? moment.utc(notification.neweventend).format()
      : '';

  const changestateto =
    actionMode === 'Summarise' || actionMode === 'Cancel' ? 'ended' : '';

  return {
    ...notification,
    neweventstart: moment.utc(notification.neweventstart).format(),
    neweventend,
    changestateto,
  };
};

export const getNewInternalStatusTagContent = (
  actionMode: string,
  label: string,
  categorydetail = '',
): string => {
  if (actionMode === 'Summarise') {
    return 'Summary';
  }
  if (actionMode === 'Cancel') {
    return 'Cancelled';
  }
  if (label === 'WARNING') {
    if (categorydetail === 'GEOMAGNETIC_STORM') {
      return 'Watch';
    }
    return 'Warning';
  }
  return 'Alert';
};
