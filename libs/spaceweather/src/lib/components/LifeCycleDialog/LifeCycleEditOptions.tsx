/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Grid } from '@mui/material';

import { ReactHookFormHiddenInput } from '@opengeoweb/form-fields';
import { useFormContext } from 'react-hook-form';

import {
  CategoryLabelStatusTag,
  StartTimeEndTime,
  PeakFlux,
  XrayClassPeakClass,
  Threshold,
  EventLevel,
  SuddenImpulse,
  DataSourceStatusTag,
  GScaleStatusTag,
} from './EditFormFields';

interface LifeCycleEditOptionsProps {
  statusTagContent: string;
  eventTypeDisabled?: boolean;
  actionMode?: string;
}

const LifeCycleEditOptions: React.FC<LifeCycleEditOptionsProps> = ({
  statusTagContent,
  eventTypeDisabled = false,
  actionMode = 'none',
}: LifeCycleEditOptionsProps) => {
  const { watch } = useFormContext();
  const [statusTagText, setStatusTagText] = React.useState(statusTagContent);

  const category = watch('category');
  const categorydetail = watch('categorydetail');

  return (
    <Grid container alignItems="center">
      {eventTypeDisabled ? (
        <>
          <ReactHookFormHiddenInput name="category" />
          <ReactHookFormHiddenInput name="categorydetail" />
          <ReactHookFormHiddenInput name="label" />
        </>
      ) : (
        <CategoryLabelStatusTag
          statusTagText={statusTagText}
          setStatusTagText={setStatusTagText}
        />
      )}
      {categorydetail === 'GEOMAGNETIC_STORM' ? (
        <GScaleStatusTag
          statusTagText={statusTagText}
          actionMode={actionMode}
          eventTypeDisabled={eventTypeDisabled}
        />
      ) : (
        <DataSourceStatusTag
          statusTagText={statusTagText}
          eventTypeDisabled={eventTypeDisabled}
        />
      )}
      {categorydetail === 'GEOMAGNETIC_SUDDEN_IMPULSE' && <SuddenImpulse />}
      {categorydetail !== 'GEOMAGNETIC_STORM' &&
        categorydetail !== 'GEOMAGNETIC_SUDDEN_IMPULSE' && (
          <>
            {category !== 'ELECTRON_FLUX' &&
              categorydetail !== 'PROTON_FLUX_100' && <EventLevel />}
            <Threshold />
          </>
        )}
      {category === 'XRAY_RADIO_BLACKOUT' && (
        <XrayClassPeakClass actionMode={actionMode} />
      )}
      {actionMode === 'Summarise' && <PeakFlux />}
      <StartTimeEndTime actionMode={actionMode} />
    </Grid>
  );
};

export default LifeCycleEditOptions;
