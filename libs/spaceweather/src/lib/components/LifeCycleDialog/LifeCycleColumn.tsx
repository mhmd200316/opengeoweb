/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Grid, Typography, List, ListItem } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { SubdirectoryArrowRight } from '@opengeoweb/theme';

import { StatusTag } from '@opengeoweb/shared';
import { EventCategoryDetail, SWEventLifeCycle } from '../../types';
import {
  getStatusTagColorFromContent,
  getStatusTagContent,
} from '../Notifications/utils';
import { formatDate } from './utils';

const useStyles = makeStyles({
  NotificationHeader: { marginTop: '15px', marginBottom: '10px' },
  messageHeader: { fontWeight: 'bold', paddingTop: '10px' },
});

interface LifeCycleColumnProps {
  lifeCycle: SWEventLifeCycle;
  categoryDetail: string;
  type: string;
}

export const formatDateWithUTC = (value: string): string =>
  value ? formatDate(value).concat(' UTC') : null;

const LifeCycleColumn: React.FC<LifeCycleColumnProps> = ({
  lifeCycle,
  categoryDetail,
  type,
}: LifeCycleColumnProps) => {
  const classes = useStyles();

  return (
    <List data-testid="lifecycle-column">
      <ListItem>
        <Grid container spacing={1}>
          <Grid item xs={6}>
            <Typography variant="overline" data-testid="lifecycle-column-owner">
              Originator
            </Typography>
            <Typography variant="body1">{lifeCycle.owner}</Typography>
          </Grid>
          {categoryDetail !== '' && (
            <Grid item xs={6}>
              <Typography variant="overline">Category detail</Typography>
              <Typography
                variant="body1"
                data-testid="lifecycle-column-categorydetail"
              >
                {EventCategoryDetail[categoryDetail]}
              </Typography>
            </Grid>
          )}
        </Grid>
      </ListItem>

      {lifeCycle.notifications &&
        lifeCycle.notifications
          .filter(
            (notification) =>
              // Remove all drafts, they should not be displayed
              !notification.draft,
          )
          .map((notification, key) => {
            const eventStartFormat = formatDateWithUTC(
              notification.neweventstart,
            );
            const eventEndFormat =
              notification.neweventend && notification.neweventend !== ''
                ? formatDateWithUTC(notification.neweventend)
                : null;

            const statusTagContent = getStatusTagContent(
              notification.label,
              categoryDetail,
              notification.changestateto,
            );
            return (
              <ListItem
                // eslint-disable-next-line react/no-array-index-key
                key={`${notification.notificationid}-${notification.issuetime}-${key}`}
                data-testid={`${notification.notificationid}-${notification.issuetime}`}
              >
                <Grid container spacing={1}>
                  <Grid item xs={12} className={classes.NotificationHeader}>
                    {key !== 0 && <SubdirectoryArrowRight />}
                    <Grid container data-testid="lifecycle-column-notification">
                      <Grid item xs={10}>
                        <Typography variant="overline">Issue time</Typography>
                        <Typography
                          variant="body1"
                          data-testid="lifecycle-issuetime"
                        >
                          {formatDateWithUTC(notification.issuetime)}
                        </Typography>
                      </Grid>
                      <Grid item xs={2}>
                        <StatusTag
                          content={statusTagContent}
                          color={getStatusTagColorFromContent(statusTagContent)}
                        />
                      </Grid>
                    </Grid>
                  </Grid>

                  {notification.datasource && (
                    <Grid item xs={12}>
                      <Typography variant="overline">Source</Typography>
                      <Typography
                        variant="body1"
                        data-testid="lifecycle-column-datasource"
                      >
                        {notification.datasource}
                      </Typography>
                    </Grid>
                  )}
                  <Grid item xs={12}>
                    <Grid container>
                      {notification.neweventlevel && (
                        <Grid item xs={2}>
                          <Typography variant="overline">
                            Event Level
                          </Typography>
                          <Typography
                            variant="body1"
                            data-testid="lifecycle-column-neweventlevel"
                          >
                            {notification.neweventlevel}
                          </Typography>
                        </Grid>
                      )}
                      {notification.threshold && (
                        <Grid item xs={3}>
                          <Typography variant="overline">Threshold</Typography>
                          <Typography
                            variant="body1"
                            data-testid="lifecycle-column-threshold"
                          >
                            {notification.threshold}
                          </Typography>
                        </Grid>
                      )}
                      {notification.thresholdunit && (
                        <Grid item xs={3}>
                          <Typography variant="overline">
                            Threshold unit
                          </Typography>
                          <Typography
                            variant="body1"
                            data-testid="lifecycle-column-thresholdunit"
                          >
                            {notification.thresholdunit}
                          </Typography>
                        </Grid>
                      )}
                      {notification.initialgscale && (
                        <Grid item xs={3}>
                          <Typography variant="overline">
                            Initial G-scale
                          </Typography>
                          <Typography
                            variant="body1"
                            data-testid="lifecycle-column-initialgscale"
                          >
                            {notification.initialgscale}
                          </Typography>
                        </Grid>
                      )}
                    </Grid>
                  </Grid>
                  {notification.xrayclass && (
                    <Grid item xs={12}>
                      <Grid container>
                        <Grid item xs={2}>
                          <Typography variant="overline">Xray Class</Typography>
                          <Typography
                            variant="body1"
                            data-testid="lifecycle-column-xrayclass"
                          >
                            {notification.xrayclass}
                          </Typography>
                        </Grid>
                        {notification.peakclass && (
                          <Grid item xs={2}>
                            <Typography variant="overline">
                              Peak Class
                            </Typography>
                            <Typography
                              variant="body1"
                              data-testid="lifecycle-column-peakclass"
                            >
                              {notification.peakclass}
                            </Typography>
                          </Grid>
                        )}
                      </Grid>
                    </Grid>
                  )}
                  {notification.peakflux && (
                    <Grid item xs={12}>
                      <Grid container>
                        <Grid item xs={5}>
                          <Typography variant="overline">Peak Flux</Typography>
                          <Typography
                            variant="body1"
                            data-testid="lifecycle-column-peakflux"
                          >
                            {notification.peakflux}
                          </Typography>
                        </Grid>
                        {notification.peakfluxtime && (
                          <Grid item xs={5}>
                            <Typography variant="overline">
                              Peak Flux Time
                            </Typography>
                            <Typography
                              variant="body1"
                              data-testid="lifecycle-column-peakfluxtime"
                            >
                              {formatDateWithUTC(notification.peakfluxtime)}
                            </Typography>
                          </Grid>
                        )}
                      </Grid>
                    </Grid>
                  )}
                  {notification.impulsetime && (
                    <Grid item xs={12}>
                      <Grid container>
                        <Grid item xs={5}>
                          <Typography variant="overline">
                            Impulse time
                          </Typography>
                          <Typography
                            variant="body1"
                            data-testid="lifecycle-column-impulsetime"
                          >
                            {formatDateWithUTC(notification.impulsetime)}
                          </Typography>
                        </Grid>
                        {notification.magnetometerdeflection && (
                          <Grid item xs={3}>
                            <Typography variant="overline">
                              Magnetometer deflection
                            </Typography>
                            <Typography
                              variant="body1"
                              data-testid="lifecycle-column-magnetometerdeflection"
                            >
                              {notification.magnetometerdeflection}
                            </Typography>
                          </Grid>
                        )}
                      </Grid>
                    </Grid>
                  )}
                  {notification.shocktime && (
                    <Grid item xs={12}>
                      <Grid container>
                        <Grid item xs={5}>
                          <Typography variant="overline">Shock time</Typography>
                          <Typography
                            variant="body1"
                            data-testid="lifecycle-column-shocktime"
                          >
                            {formatDateWithUTC(notification.shocktime)}
                          </Typography>
                        </Grid>
                        {notification.observedpolaritybz && (
                          <Grid item xs={3}>
                            <Typography variant="overline">
                              Observed polarity Bz
                            </Typography>
                            <Typography
                              variant="body1"
                              data-testid="lifecycle-column-observedpolaritybz"
                            >
                              {notification.observedpolaritybz}
                            </Typography>
                          </Grid>
                        )}
                        {notification.observedsolarwind && (
                          <Grid item xs={4}>
                            <Typography variant="overline">
                              Observed solar wind speed
                            </Typography>
                            <Typography
                              variant="body1"
                              data-testid="lifecycle-column-observedsolarwind"
                            >
                              {notification.observedsolarwind}
                            </Typography>
                          </Grid>
                        )}
                      </Grid>
                    </Grid>
                  )}
                  <Grid item xs={12}>
                    <Grid container>
                      <Grid item xs={5}>
                        <Typography variant="overline">Start</Typography>
                        <Typography>{eventStartFormat}</Typography>
                      </Grid>
                      {eventEndFormat && (
                        <Grid item xs={5}>
                          <Typography variant="overline">End</Typography>
                          <Typography data-testid="lifecycle-column-eventend">
                            {eventEndFormat}
                          </Typography>
                        </Grid>
                      )}
                    </Grid>
                  </Grid>
                  <Grid item xs={12}>
                    {type === 'external' && notification.message && (
                      <>
                        <Typography
                          variant="body2"
                          className={classes.messageHeader}
                        >
                          Potential impact
                        </Typography>
                        <Typography
                          variant="body2"
                          data-testid="lifecycle-column-message-impact"
                          dangerouslySetInnerHTML={{
                            __html: notification.message,
                          }}
                          paragraph
                        />
                      </>
                    )}
                    {type === 'internal' && (
                      <>
                        {notification.title && (
                          <Typography
                            variant="body2"
                            className={classes.messageHeader}
                            data-testid="lifecycle-column-title"
                          >
                            {notification.title}
                          </Typography>
                        )}
                        <Typography
                          variant="body2"
                          data-testid="lifecycle-column-message"
                          dangerouslySetInnerHTML={{
                            __html: notification.message,
                          }}
                        />
                      </>
                    )}
                  </Grid>
                </Grid>
              </ListItem>
            );
          })}
    </List>
  );
};

export default LifeCycleColumn;
