/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';
import LifeCycleDialog, { getDialogTitle } from './LifeCycleDialog';
import {
  StoryWrapper,
  StoryWrapperWithErrorOnSave,
} from '../../utils/storybookUtils';
import { fakeEventList, fakeEventListFixedDates } from '../../utils/fakedata';
import { NotificationTriggerProvider } from '../NotificationTrigger';
import { ContentDialog } from '../ContentDialog';
import { LifeCycleEditForm } from './LifeCycleEdit';
import { constructBaseNotification } from './utils';
import {
  EventCategory,
  EventCategoryDetail,
  NotificationLabel,
} from '../../types';
import { createFakeApiFixedDates } from '../../utils/fakeApi';

export default { title: 'components/LifeCycleDialog' };

const NewNotificationComponent = (): React.ReactElement => {
  return (
    <LifeCycleDialog
      open
      toggleStatus={(): void => {
        /* Do nothing */
      }}
      dialogMode="new"
    />
  );
};

export const NewNotification = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <NewNotificationComponent />
    </StoryWrapper>
  );
};

export const MetOfficeRadioBlackoutEvent = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <LifeCycleDialog
        open
        toggleStatus={(): void => {
          /* Do nothing */
        }}
        dialogMode="METRB1"
        event={fakeEventList[3]}
      />
    </StoryWrapper>
  );
};

export const MetOfficeSummarisedEvent = (): React.ReactElement => {
  return (
    <StoryWrapper createApiFunc={createFakeApiFixedDates}>
      <LifeCycleDialog
        open
        toggleStatus={(): void => {
          /* Do nothing */
        }}
        dialogMode="METRB3"
        event={fakeEventListFixedDates[6]}
      />
    </StoryWrapper>
  );
};

MetOfficeSummarisedEvent.storyName =
  'Met Office Summarised Event (takeSnapshot)';

export const MetOfficeCancelledEvent = (): React.ReactElement => {
  return (
    <StoryWrapper createApiFunc={createFakeApiFixedDates}>
      <LifeCycleDialog
        open
        toggleStatus={(): void => {
          /* Do nothing */
        }}
        dialogMode="METRB122"
        event={fakeEventListFixedDates[2]}
      />
    </StoryWrapper>
  );
};

MetOfficeCancelledEvent.storyName = 'Met Office Cancelled Event (takeSnapshot)';

export const MetOfficeGeomagneticSuddenImpulseAlert =
  (): React.ReactElement => {
    return (
      <StoryWrapper>
        <LifeCycleDialog
          open
          toggleStatus={(): void => {
            /* Do nothing */
          }}
          dialogMode="METRB2"
          event={fakeEventList[1]}
        />
      </StoryWrapper>
    );
  };

export const MetOfficeGeomagneticSuddenImpulseWarning =
  (): React.ReactElement => {
    return (
      <StoryWrapper>
        <LifeCycleDialog
          open
          toggleStatus={(): void => {
            /* Do nothing */
          }}
          dialogMode="METRB4"
          event={fakeEventList[8]}
        />
      </StoryWrapper>
    );
  };

export const MetOfficeGeomagneticKpIndexAlert = (): React.ReactElement => {
  React.useEffect(() => {
    // blur field for snapshot
    setTimeout(() => {
      (document.activeElement as HTMLElement).blur();
    }, 1000);
  }, []);
  return (
    <StoryWrapper createApiFunc={createFakeApiFixedDates}>
      <LifeCycleDialog
        open
        toggleStatus={(): void => {
          /* Do nothing */
        }}
        dialogMode="METRB345"
        event={fakeEventListFixedDates[11]}
      />
    </StoryWrapper>
  );
};

MetOfficeGeomagneticKpIndexAlert.storyName =
  'Met Office Geomagnetic KpIndex Alert (takeSnapshot)';

export const KNMIGeomagneticKpIndexWarning = (): React.ReactElement => {
  return (
    <StoryWrapper createApiFunc={createFakeApiFixedDates}>
      <LifeCycleDialog
        open
        toggleStatus={(): void => {
          /* Do nothing */
        }}
        dialogMode="KNMI002"
        event={fakeEventListFixedDates[12]}
      />
    </StoryWrapper>
  );
};

KNMIGeomagneticKpIndexWarning.storyName =
  'KNMI Geomagnetic KpIndex Warning (takeSnapshot)';

export const MetOfficeGeomagneticStormWatch = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <LifeCycleDialog
        open
        toggleStatus={(): void => {
          /* Do nothing */
        }}
        dialogMode="METRWATCH121"
        event={fakeEventList[14]}
      />
    </StoryWrapper>
  );
};

export const MetOfficeProtonFlux100AlertSummary = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <LifeCycleDialog
        open
        toggleStatus={(): void => {
          /* Do nothing */
        }}
        dialogMode="METRB1123454"
        event={fakeEventList[0]}
      />
    </StoryWrapper>
  );
};

export const MetOfficeProtonFlux100Warning = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <LifeCycleDialog
        open
        toggleStatus={(): void => {
          /* Do nothing */
        }}
        dialogMode="METRB122"
        event={fakeEventList[2]}
      />
    </StoryWrapper>
  );
};
export const MetOfficeProtonFlux10Warning = (): React.ReactElement => {
  React.useEffect(() => {
    // blur field for snapshot
    setTimeout(() => {
      (document.activeElement as HTMLElement).blur();
    }, 1000);
  }, []);
  return (
    <StoryWrapper createApiFunc={createFakeApiFixedDates}>
      <LifeCycleDialog
        open
        toggleStatus={(): void => {
          /* Do nothing */
        }}
        dialogMode="METRBWARNPRO1084847"
        event={fakeEventListFixedDates[13]}
      />
    </StoryWrapper>
  );
};

MetOfficeProtonFlux10Warning.storyName =
  'Met Office Proton Flux 10 Warning (takeSnapshot)';

export const MetOfficeProtonFlux10AlertSummary = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <LifeCycleDialog
        open
        toggleStatus={(): void => {
          /* Do nothing */
        }}
        dialogMode="METRB1123453453454"
        event={fakeEventList[10]}
      />
    </StoryWrapper>
  );
};

export const MetOfficeElectronFlux2AlertSummary = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <LifeCycleDialog
        open
        toggleStatus={(): void => {
          /* Do nothing */
        }}
        dialogMode="METRB3"
        event={fakeEventList[6]}
      />
    </StoryWrapper>
  );
};

const MetOfficeElectronFluenceWarningComponent = (): React.ReactElement => {
  return (
    <LifeCycleDialog
      open
      toggleStatus={(): void => {
        /* Do nothing */
      }}
      dialogMode="METRB25"
      event={fakeEventList[9]}
    />
  );
};

export const MetOfficeElectronFluenceWarning = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <MetOfficeElectronFluenceWarningComponent />
    </StoryWrapper>
  );
};

export const NewNotificationWithTrigger = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <NotificationTriggerProvider>
        <NewNotificationComponent />
      </NotificationTriggerProvider>
    </StoryWrapper>
  );
};

export const WithErrorOnSaveNewNotification = (): React.ReactElement => {
  return (
    <StoryWrapperWithErrorOnSave>
      <NotificationTriggerProvider>
        <NewNotificationComponent />
      </NotificationTriggerProvider>
    </StoryWrapperWithErrorOnSave>
  );
};

export const WithErrorOnSaveElectronFluenceWarning = (): React.ReactElement => {
  return (
    <StoryWrapperWithErrorOnSave>
      <NotificationTriggerProvider>
        <MetOfficeElectronFluenceWarningComponent />
      </NotificationTriggerProvider>
    </StoryWrapperWithErrorOnSave>
  );
};

export const NewNotificationGeomagneticSnap = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <ContentDialog
        open
        title={getDialogTitle('new', null)}
        toggleStatus={(): void => null}
        maxWidth="sm"
      >
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...constructBaseNotification(null, 'new'),
              neweventstart: '2022-01-01T12:00:00Z',
              category: Object.keys(EventCategory)[1],
              categorydetail: Object.keys(EventCategoryDetail)[0],
              label: Object.keys(NotificationLabel)[1],
            },
          }}
        >
          <LifeCycleEditForm
            toggleDialogOpen={(): void => null}
            baseNotificationType="new"
            statusTagContent="Watch"
          />
        </ReactHookFormProvider>
      </ContentDialog>
    </StoryWrapper>
  );
};

export const NewNotificationWithTriggerSnap = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <NotificationTriggerProvider>
        <ContentDialog
          open
          title={getDialogTitle('new', null)}
          toggleStatus={(): void => null}
          maxWidth="sm"
        >
          <ReactHookFormProvider
            options={{
              ...defaultFormOptions,
              defaultValues: {
                ...constructBaseNotification(null, 'new'),
                neweventstart: '2022-01-01T12:00:00Z',
              },
            }}
          >
            <LifeCycleEditForm
              toggleDialogOpen={(): void => null}
              baseNotificationType="new"
            />
          </ReactHookFormProvider>
        </ContentDialog>
      </NotificationTriggerProvider>
    </StoryWrapper>
  );
};

NewNotificationGeomagneticSnap.storyName =
  'New Notification Geomagnetic Storm (takeSnapshot)';

NewNotificationWithTriggerSnap.storyName =
  'New Notification With Trigger (takeSnapshot)';
