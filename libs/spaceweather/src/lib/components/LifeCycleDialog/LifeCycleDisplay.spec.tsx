/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, waitFor, fireEvent } from '@testing-library/react';
import * as utils from '@opengeoweb/api';
import { ApiProvider } from '@opengeoweb/api';
import LifeCycleDisplay from './LifeCycleDisplay';
import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';

jest.mock('../../utils/api');

jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual('../../../../api/src/index') as Record<
    string,
    unknown
  >),
}));

describe('src/components/LifeCycleDialog/LifeCycleDisplay', () => {
  it('should display the lifecycle with an external and internal column for an external event', async () => {
    const props = {
      eventId: 'METRB1123454',
      toggleDialogOpen: jest.fn,
    };
    const { getByTestId, getAllByTestId, queryByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <LifeCycleDisplay {...props} />
      </ApiProvider>,
    );
    await waitFor(() => {
      expect(getByTestId('display-lifecycle-externalcolumn')).toBeTruthy();
      expect(getByTestId('display-lifecycle-internalcolumn')).toBeTruthy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(2);
      expect(queryByTestId('display-lifecycle-error')).toBeFalsy();
      expect(queryByTestId('display-lifecycle-loading')).toBeFalsy();
    });
  });
  it('should display an error if error returned by api', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: false,
      error: new Error('error'),
      result: null,
    });
    const props = {
      eventId: 'METRB1123454',
      toggleDialogOpen: jest.fn,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <LifeCycleDisplay {...props} />
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(getByTestId('display-lifecycle-error')).toBeTruthy();
      expect(queryByTestId('display-lifecycle-loading')).toBeFalsy();
      expect(queryByTestId('display-lifecycle-internalcolumn')).toBeFalsy();
      expect(queryByTestId('display-lifecycle-externalcolumn')).toBeFalsy();
    });
  });
  it('should display the loading screen while api is still loading', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: true,
      error: null,
      result: null,
    });
    const props = {
      eventId: 'METRB1123454',
      toggleDialogOpen: jest.fn,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <LifeCycleDisplay {...props} />
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(getByTestId('display-lifecycle-loading')).toBeTruthy();
      expect(queryByTestId('display-lifecycle-error')).toBeFalsy();
      expect(queryByTestId('display-lifecycle-internalcolumn')).toBeFalsy();
      expect(queryByTestId('display-lifecycle-externalcolumn')).toBeFalsy();
    });
  });
  it('should display the action buttons (update/extend and cancelled) for an external WARNING for which internal notifications have already been issued', async () => {
    const props = {
      eventId: 'METRB122',
      toggleDialogOpen: jest.fn,
    };
    const { getByTestId, queryByTestId, getAllByTestId, findByText } = render(
      <ApiProvider createApi={createFakeApi}>
        <LifeCycleDisplay {...props} />
      </ApiProvider>,
    );
    await waitFor(() => {
      expect(getByTestId('display-lifecycle-externalcolumn')).toBeTruthy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(2);
      expect(getByTestId('lifecycle-actions-displaybuttons')).toBeTruthy();
      expect(getByTestId('cancel')).toBeTruthy();
      expect(getByTestId('updateextend')).toBeTruthy();
      expect(queryByTestId('summarise')).toBeFalsy();
      expect(queryByTestId('edit-lifecycle')).toBeFalsy();
    });

    // Then, when pressing the cancel button the edit fields should be displayed with the right tag
    fireEvent.click(getByTestId('cancel'));
    await waitFor(() => {
      expect(getByTestId('display-lifecycle-externalcolumn')).toBeTruthy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(2);
      expect(getByTestId('edit-lifecycle')).toBeTruthy();
      expect(queryByTestId('lifecycle-actions-displaybuttons')).toBeFalsy();
      expect(getByTestId('lifecycle-display-internaleditaction')).toBeTruthy();
      expect(queryByTestId('lifecycle-display-internaldraftedit')).toBeFalsy();
      expect(queryByTestId('lifecycle-actions-displaybuttons')).toBeFalsy();
      expect(getByTestId('notification-tag').textContent).toEqual('Cancelled');
    });

    // Check that the title and message have been retrieved
    expect(await findByText('Template message text for you')).toBeTruthy();
    expect(getByTestId('notification-title').getAttribute('value')).toBe(
      'Template title for you',
    );
  });
  it('should display the edit section for an external alert for which no internal notifications have been issued', async () => {
    const props = {
      eventId: 'METRB345',
      toggleDialogOpen: jest.fn,
    };
    const { getByTestId, queryByTestId, getAllByTestId, findByText } = render(
      <ApiProvider createApi={createFakeApi}>
        <LifeCycleDisplay {...props} />
      </ApiProvider>,
    );

    await waitFor(() => {
      expect(getByTestId('display-lifecycle-externalcolumn')).toBeTruthy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(1);
      expect(getByTestId('edit-lifecycle')).toBeTruthy();
      expect(queryByTestId('lifecycle-actions-displaybuttons')).toBeFalsy();
      expect(queryByTestId('lifecycle-display-internaleditaction')).toBeFalsy();
      expect(queryByTestId('lifecycle-display-internaldraftedit')).toBeFalsy();
    });

    // Check that the title and message have been retrieved
    expect(await findByText('Template message text for you')).toBeTruthy();
    expect(getByTestId('notification-title').getAttribute('value')).toBe(
      'Template title for you',
    );
  });
  it('should display no external column if it is a internally issued event', async () => {
    const props = {
      eventId: 'KNMI002',
      toggleDialogOpen: jest.fn,
    };
    const { getByTestId, queryByTestId, getAllByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <LifeCycleDisplay {...props} />
      </ApiProvider>,
    );
    await waitFor(() => {
      expect(queryByTestId('display-lifecycle-externalcolumn')).toBeFalsy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(1);
      expect(getByTestId('lifecycle-actions-displaybuttons')).toBeTruthy();
      expect(queryByTestId('summarise')).toBeFalsy();
      expect(getByTestId('updateextend')).toBeTruthy();
      expect(queryByTestId('cancel')).toBeTruthy();
      expect(queryByTestId('edit-lifecycle')).toBeFalsy();
    });
  });
  it('should display the edit section for an external alert for which a draft has been saved', async () => {
    const props = {
      eventId: 'METRB1123453453454',
      toggleDialogOpen: jest.fn,
    };
    const { getByTestId, queryByTestId, getAllByTestId, findByText } = render(
      <ApiProvider createApi={createFakeApi}>
        <LifeCycleDisplay {...props} />
      </ApiProvider>,
    );
    await waitFor(() => {
      expect(getByTestId('display-lifecycle-externalcolumn')).toBeTruthy();
      expect(getAllByTestId('lifecycle-column').length).toEqual(2);
      expect(getByTestId('edit-lifecycle')).toBeTruthy();
      expect(getByTestId('lifecycle-display-internaldraftedit')).toBeTruthy();
      expect(queryByTestId('lifecycle-display-internaleditaction')).toBeFalsy();
      expect(queryByTestId('lifecycle-actions-displaybuttons')).toBeFalsy();
    });

    // Check that the title and message have been retrieved
    expect(await findByText('Geen impact')).toBeTruthy();
    expect(getByTestId('notification-title').getAttribute('value')).toBe(
      'Proton flux 10 alert update',
    );
  });
});
