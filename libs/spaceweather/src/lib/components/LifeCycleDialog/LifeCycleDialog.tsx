/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import moment from 'moment';
import { Breakpoint } from '@mui/material';

import { AlertBanner, useConfirmationDialog } from '@opengeoweb/shared';
import { ContentDialog } from '../ContentDialog';
import LifeCycleEdit from './LifeCycleEdit';
import LifeCycleDisplay from './LifeCycleDisplay';
import { EventCategory, EventCategoryDetail, SWEvent } from '../../types';

export const getDialogTitle = (dialogMode: string, event: SWEvent): string => {
  const titleTime =
    event &&
    event.lifecycles.internalprovider &&
    event.lifecycles.internalprovider.draft !== true
      ? moment
          .utc(
            event.lifecycles.externalprovider
              ? event.lifecycles.externalprovider.firstissuetime
              : event.lifecycles.internalprovider.firstissuetime,
          )
          .format('YYYY-MM-DD HH:mm')
          .concat(' UTC')
      : 'Draft';

  if (dialogMode !== 'new') {
    if (event.categorydetail !== '') {
      return `${EventCategoryDetail[event.categorydetail]}: ${titleTime}`;
    }
    return `${EventCategory[event.category]}: ${titleTime}`;
  }
  return 'New Notification';
};

interface LifeCycleDialogProps {
  open: boolean;
  toggleStatus: () => void;
  dialogMode?: string;
  event?: SWEvent;
}

const LifeCycleDialog: React.FC<LifeCycleDialogProps> = ({
  open,
  toggleStatus,
  dialogMode = 'new',
  event = null,
}: LifeCycleDialogProps) => {
  const confirmDialog = useConfirmationDialog();
  const [dialogSize, setDialogSize] = React.useState<Breakpoint>('lg');
  const [isFormChanged, setFormIsChanged] = React.useState(false);
  const [errorStoreMessage, setErrorStoreMessage] = React.useState('');
  const [errorRetrievePopulate, setErrorRetrievePopulate] = React.useState('');

  const onChangeForm = (hasChanged: boolean): void => {
    setFormIsChanged(hasChanged);
  };

  React.useEffect(() => {
    if (dialogMode === 'new' || (event && event.originator === 'KNMI')) {
      setDialogSize('sm');
    } else {
      setDialogSize('lg');
    }
  }, [dialogMode, event]);

  const onToggleDialog = (formSaved = false): void => {
    if (isFormChanged && !formSaved) {
      confirmDialog({
        title: 'Close notification',
        description:
          'Are you sure you would like to close this notification window? Any changes made will not be saved',
        confirmLabel: 'Close',
        cancelLabel: 'Cancel',
      }).then(() => {
        toggleStatus();
      });
    } else {
      toggleStatus();
    }
  };

  return (
    <ContentDialog
      open={open}
      toggleStatus={onToggleDialog}
      data-testid="lifecycle-dialog"
      title={getDialogTitle(dialogMode, event)}
      maxWidth={dialogSize}
      fullWidth
      disableEscapeKeyDown
      onClose={onToggleDialog}
      alertBanner={
        errorStoreMessage || errorRetrievePopulate ? (
          <AlertBanner
            severity="error"
            title={
              errorStoreMessage
                ? 'An error has occurred while saving, please try again'
                : 'An error has occurred while retrieving the notification templates, please try again'
            }
            info={errorStoreMessage || errorRetrievePopulate}
          />
        ) : null
      }
    >
      {dialogMode === 'new' && (
        <LifeCycleEdit
          toggleDialogOpen={onToggleDialog}
          statusTagContent="Alert"
          baseNotificationType="new"
          onFormChange={onChangeForm}
          setErrorStoreMessage={setErrorStoreMessage}
          setErrorRetrievePopulate={setErrorRetrievePopulate}
        />
      )}
      {dialogMode !== 'new' && (
        <LifeCycleDisplay
          eventId={dialogMode}
          toggleDialogOpen={onToggleDialog}
          setErrorStoreMessage={setErrorStoreMessage}
          setErrorRetrievePopulate={setErrorRetrievePopulate}
          onFormChange={onChangeForm}
        />
      )}
    </ContentDialog>
  );
};

export default LifeCycleDialog;
