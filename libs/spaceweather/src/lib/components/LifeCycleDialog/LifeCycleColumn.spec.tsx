/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render } from '@testing-library/react';
import LifeCycleColumn, { formatDateWithUTC } from './LifeCycleColumn';
import {
  fakeEventList,
  mockEventAcknowledgedExternal,
  mockEventAcknowledgedExternalDraft,
  mockEventUnacknowledgedExternal,
  mockEventWarning,
} from '../../utils/fakedata';

describe('src/components/LifeCycleDialog/LifeCycleColumn', () => {
  it('should format date and append UTCs', () => {
    expect(formatDateWithUTC('2020-07-12T04:28:00Z')).toEqual(
      '2020-07-12 04:28 UTC',
    );
    expect(formatDateWithUTC(undefined)).toBeNull();
    expect(formatDateWithUTC(null)).toBeNull();
    expect(formatDateWithUTC('')).toBeNull();
  });

  it('should display all notifications in the lifecyle except for the drafts', () => {
    const props = {
      lifeCycle: mockEventAcknowledgedExternalDraft.lifecycles.internalprovider,
      type: 'internal',
      categoryDetail: mockEventAcknowledgedExternalDraft.categorydetail,
    };

    const { getByTestId, getAllByTestId, queryByTestId } = render(
      <LifeCycleColumn {...props} />,
    );

    const {
      notifications,
    } = mockEventAcknowledgedExternalDraft.lifecycles.internalprovider;

    expect(getAllByTestId('lifecycle-column-notification').length).toEqual(2);
    expect(
      getByTestId(
        `${notifications[0].notificationid}-${notifications[0].issuetime}`,
      ),
    ).toBeTruthy();
    expect(
      getByTestId(
        `${notifications[1].notificationid}-${notifications[1].issuetime}`,
      ),
    ).toBeTruthy();
    expect(
      queryByTestId(
        `${notifications[2].notificationid}-${notifications[2].issuetime}`,
      ),
    ).toBeFalsy();
  });
  it('should display the notification message with headers for UKMO', () => {
    const props = {
      lifeCycle: mockEventUnacknowledgedExternal.lifecycles.externalprovider,
      type: 'external',
      categoryDetail: mockEventUnacknowledgedExternal.categorydetail,
    };

    const {
      notifications,
    } = mockEventUnacknowledgedExternal.lifecycles.externalprovider;

    const { getByTestId } = render(<LifeCycleColumn {...props} />);

    expect(getByTestId('lifecycle-column-message-impact').textContent).toEqual(
      notifications[0].message,
    );
  });
  it('should display the notification message correctly for KNMI', () => {
    const props = {
      lifeCycle: mockEventAcknowledgedExternal.lifecycles.internalprovider,
      type: 'internal',
      categoryDetail: mockEventAcknowledgedExternal.categorydetail,
    };

    const {
      notifications,
    } = mockEventAcknowledgedExternal.lifecycles.internalprovider;

    const { getAllByTestId } = render(<LifeCycleColumn {...props} />);

    expect(getAllByTestId('lifecycle-column-message')[0].textContent).toEqual(
      notifications[0].message,
    );
  });
  it('should display the event end date if passed', () => {
    const props = {
      lifeCycle: mockEventWarning.lifecycles.internalprovider,
      type: 'internal',
      categoryDetail: mockEventWarning.categorydetail,
    };

    const { getByTestId } = render(<LifeCycleColumn {...props} />);

    // If passed - eventend should be shown
    expect(getByTestId('lifecycle-column-eventend')).toBeTruthy();
  });
  it('should not display the event end date if not passed', () => {
    // if not passed - eventend should be hidden
    const props = {
      lifeCycle: mockEventUnacknowledgedExternal.lifecycles.externalprovider,
      type: 'external',
      categoryDetail: mockEventUnacknowledgedExternal.categorydetail,
    };

    const { queryByTestId } = render(<LifeCycleColumn {...props} />);
    expect(queryByTestId('lifecycle-column-eventend')).toBeFalsy();
  });
  it('should display the radio blackout fields if passed', () => {
    const props = {
      lifeCycle: fakeEventList[3].lifecycles.externalprovider,
      type: 'external',
      categoryDetail: fakeEventList[3].categorydetail,
    };

    const { queryAllByTestId } = render(<LifeCycleColumn {...props} />);
    expect(queryAllByTestId('lifecycle-column-categorydetail').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-eventend')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-neweventlevel')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-datasource')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-threshold')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-thresholdunit')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-xrayclass')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-peakclass')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-peakflux')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-peakfluxtime')).toBeTruthy();
  });
  it('should not display the radio blackout fields if not passed', () => {
    const props = {
      lifeCycle: fakeEventList[3].lifecycles.externalprovider,
      type: 'external',
      categoryDetail: fakeEventList[3].categorydetail,
    };
    props.lifeCycle.notifications[0].neweventend = '';
    props.lifeCycle.notifications[0].neweventlevel = '';
    props.lifeCycle.notifications[0].datasource = '';
    props.lifeCycle.notifications[0].threshold = undefined;
    props.lifeCycle.notifications[0].thresholdunit = '';
    props.lifeCycle.notifications[0].xrayclass = '';
    props.lifeCycle.notifications[0].peakclass = '';
    props.lifeCycle.notifications[0].peakflux = undefined;
    props.lifeCycle.notifications[0].peakfluxtime = '';
    props.lifeCycle.notifications[1].neweventend = '';
    props.lifeCycle.notifications[1].neweventlevel = '';
    props.lifeCycle.notifications[1].datasource = '';
    props.lifeCycle.notifications[1].threshold = undefined;
    props.lifeCycle.notifications[1].thresholdunit = '';
    props.lifeCycle.notifications[1].xrayclass = '';
    props.lifeCycle.notifications[1].peakclass = '';
    props.lifeCycle.notifications[1].peakflux = undefined;
    props.lifeCycle.notifications[1].peakfluxtime = '';

    const { queryByTestId } = render(<LifeCycleColumn {...props} />);
    expect(queryByTestId('lifecycle-column-categorydetail')).toBeFalsy();
    expect(queryByTestId('lifecycle-column-eventend')).toBeFalsy();
    expect(queryByTestId('lifecycle-column-neweventlevel')).toBeFalsy();
    expect(queryByTestId('lifecycle-column-datasource')).toBeFalsy();
    expect(queryByTestId('lifecycle-column-threshold')).toBeFalsy();
    expect(queryByTestId('lifecycle-column-thresholdunit')).toBeFalsy();
    expect(queryByTestId('lifecycle-column-xrayclass')).toBeFalsy();
    expect(queryByTestId('lifecycle-column-peakclass')).toBeFalsy();
    expect(queryByTestId('lifecycle-column-peakflux')).toBeFalsy();
    expect(queryByTestId('lifecycle-column-peakfluxtime')).toBeFalsy();
  });
  it('should display the geomagnetic sudden impulse alert fields if passed', () => {
    const props = {
      lifeCycle: fakeEventList[1].lifecycles.externalprovider,
      type: 'external',
      categoryDetail: fakeEventList[1].categorydetail,
    };

    const { queryAllByTestId } = render(<LifeCycleColumn {...props} />);
    expect(queryAllByTestId('lifecycle-column-owner')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-categorydetail')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-eventstart')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-eventend').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-neweventlevel').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-datasource')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-threshold').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-impulsetime')).toBeTruthy();
    expect(
      queryAllByTestId('lifecycle-column-magnetometerdeflection'),
    ).toBeTruthy();
  });
  it('should display the geomagnetic sudden impulse warning fields if passed', () => {
    const props = {
      lifeCycle: fakeEventList[8].lifecycles.externalprovider,
      type: 'external',
      categoryDetail: fakeEventList[8].categorydetail,
    };

    const { queryAllByTestId } = render(<LifeCycleColumn {...props} />);
    expect(queryAllByTestId('lifecycle-column-owner')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-categorydetail')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-eventstart')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-eventend')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-neweventlevel').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-datasource')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-threshold').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-impulsetime').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-shocktime')).toBeTruthy();
    expect(
      queryAllByTestId('lifecycle-column-observedpolaritybz'),
    ).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-observedsolarwind')).toBeTruthy();
    expect(
      queryAllByTestId('lifecycle-column-magnetometerdeflection').length,
    ).toBe(0);
  });

  it('should display the geomagnetic storm watch fields if passed', () => {
    const props = {
      lifeCycle: fakeEventList[14].lifecycles.externalprovider,
      type: 'external',
      categoryDetail: fakeEventList[14].categorydetail,
    };

    const { queryAllByTestId } = render(<LifeCycleColumn {...props} />);
    expect(queryAllByTestId('lifecycle-column-owner')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-categorydetail')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-eventstart')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-eventend')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-initialgscale')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-datasource').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-threshold').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-impulsetime').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-neweventlevel').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-shocktime').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-observedpolaritybz').length).toBe(
      0,
    );
    expect(queryAllByTestId('lifecycle-column-observedsolarwind').length).toBe(
      0,
    );
    expect(
      queryAllByTestId('lifecycle-column-magnetometerdeflection').length,
    ).toBe(0);
  });

  it('should display the geomagnetic kp index alert fields if passed', () => {
    const props = {
      lifeCycle: fakeEventList[11].lifecycles.externalprovider,
      type: 'external',
      categoryDetail: fakeEventList[11].categorydetail,
    };

    const { queryAllByTestId } = render(<LifeCycleColumn {...props} />);
    expect(queryAllByTestId('lifecycle-column-owner')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-categorydetail')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-eventstart')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-eventend').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-initialgscale').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-datasource')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-threshold')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-impulsetime').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-neweventlevel')).toBeTruthy();
    expect(queryAllByTestId('lifecycle-column-shocktime').length).toBe(0);
    expect(queryAllByTestId('lifecycle-column-observedpolaritybz').length).toBe(
      0,
    );
    expect(queryAllByTestId('lifecycle-column-observedsolarwind').length).toBe(
      0,
    );
    expect(
      queryAllByTestId('lifecycle-column-magnetometerdeflection').length,
    ).toBe(0);
  });

  it('should correctly render dates', async () => {
    const lifeCycle = fakeEventList[0].lifecycles.externalprovider;
    const props = {
      lifeCycle,
      type: 'internal',
      categoryDetail: fakeEventList[0].categorydetail,
    };

    const { queryAllByTestId } = render(<LifeCycleColumn {...props} />);

    queryAllByTestId('lifecycle-issuetime').forEach((element, index) =>
      expect(element.innerHTML).toEqual(
        formatDateWithUTC(lifeCycle.notifications[index].issuetime),
      ),
    );
    expect(
      queryAllByTestId('lifecycle-column-peakfluxtime')[0].innerHTML,
    ).toEqual(formatDateWithUTC(lifeCycle.notifications[1].peakfluxtime));
  });
});
