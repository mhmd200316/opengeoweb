/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import LifeCycleEdit from './LifeCycleEdit';
import {
  fakeEventList,
  mockDraftEvent,
  mockEventInternal,
  mockEventMissingData,
} from '../../utils/fakedata';
import { TestWrapper } from '../../utils/testUtils';
import * as api from '../../utils/api';
import { createApi as createFakeApi } from '../../utils/fakeApi';

jest.mock('../../utils/api');

describe('src/components/LifeCycleDialog/LifeCycleEdit', () => {
  it('should handle saving a new notification as draft', async () => {
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: fakeEventList[17],
    };
    const { getByTestId, findByText } = render(
      <ConfirmationServiceProvider>
        <TestWrapper>
          <LifeCycleEdit {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );

    // Wait till title and message have been retrieved
    expect(await findByText('Template message text for you')).toBeTruthy();
    expect(getByTestId('notification-title').getAttribute('value')).toBe(
      'Template title for you',
    );

    fireEvent.click(getByTestId('draft'));

    // loader should be visible
    await waitFor(() =>
      expect(getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 1;',
      ),
    );

    // wait for the api response, loader should be gone
    await waitFor(() =>
      expect(getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 0;',
      ),
    );
    expect(props.toggleDialogOpen).toHaveBeenCalled();
  });
  it('should set error message when issue notification fails', async () => {
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: mockEventInternal,
      setErrorStoreMessage: jest.fn(),
    };
    const { getByTestId, findByText } = render(
      <ConfirmationServiceProvider>
        <TestWrapper>
          <LifeCycleEdit {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );

    // Wait till title and message have been retrieved
    expect(await findByText('Template message text for you')).toBeTruthy();
    expect(await getByTestId('notification-title').getAttribute('value')).toBe(
      'Template title for you',
    );

    // enter eventlevel
    fireEvent.mouseDown(getByTestId('eventlevel-select'));
    const menuItemLevelR = await findByText('R1');
    fireEvent.click(menuItemLevelR);

    fireEvent.click(getByTestId('issue'));
    // loader should be visible
    await waitFor(() => {
      expect(getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 1;',
      );
    });
    // wait for the api response, loader should be gone
    await waitFor(() => {
      expect(getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 0;',
      );
    });
    expect(props.toggleDialogOpen).not.toHaveBeenCalled();
    expect(props.setErrorStoreMessage).toHaveBeenCalled();
  });

  it('should handle discarding a draft notification and show confirmation dialog', async () => {
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'draft',
      baseNotificationData: mockDraftEvent,
    };
    const { container, getByTestId, queryByTestId, findByText, queryByText } =
      render(
        <ThemeWrapperOldTheme>
          <ConfirmationServiceProvider>
            <TestWrapper>
              <LifeCycleEdit {...props} />
            </TestWrapper>
          </ConfirmationServiceProvider>
        </ThemeWrapperOldTheme>,
      );

    // Check that the draft title and message are used
    expect(await findByText('This is a draft message')).toBeTruthy();
    expect(await getByTestId('notification-title').getAttribute('value')).toBe(
      'Geomagnetic Kp index alert',
    );

    // Click on the discard button and expect the confirm dialog to be visible
    fireEvent.click(getByTestId('discard'));
    await waitFor(() => {
      expect(getByTestId('confirmationDialog')).toBeTruthy();
      expect(getByTestId('confirmationDialog-close')).toBeTruthy();
    });

    // Click on Cancel and expect the confirm dialog to close but the draft to still be visible
    const cancelButton = container.parentElement.querySelector(
      '[data-testid=confirmationDialog-cancel]',
    );
    fireEvent.click(cancelButton);
    await waitFor(() => {
      expect(queryByTestId('confirmationDialog')).toBeFalsy();
    });
    expect(queryByText('This is a draft message')).toBeTruthy();

    // Click on back button and expect the confirm dialog to be visible
    fireEvent.click(getByTestId('discard'));
    await waitFor(() => {
      expect(getByTestId('confirmationDialog')).toBeTruthy();
    });

    // Click on Discard and Close and expect the confirm dialog to close and also the draft to close
    const confirmButton = container.parentElement.querySelector(
      '[data-testid=confirmationDialog-confirm]',
    );
    fireEvent.click(confirmButton);

    // loader should be visible
    await waitFor(() =>
      expect(getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 1;',
      ),
    );

    // wait for the api response, loader should be gone
    await waitFor(() =>
      expect(getByTestId('loader').getAttribute('style')).toContain(
        'opacity: 0;',
      ),
    );

    expect(props.toggleDialogOpen).toHaveBeenCalled();
  });

  it('should handle discarding a new notification', async () => {
    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'new',
      baseNotificationData: null,
    };
    const { getByTestId } = render(
      <ConfirmationServiceProvider>
        <TestWrapper>
          <LifeCycleEdit {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );
    // Expect title to be empty
    expect(getByTestId('notification-title').getAttribute('value')).toBe('');
    expect(getByTestId('notification-text').textContent).toBe('');

    fireEvent.click(getByTestId('discard'));

    expect(props.toggleDialogOpen).toHaveBeenCalled();
    // loader should not be visible
    expect(getByTestId('loader').getAttribute('style')).toContain(
      'opacity: 0;',
    );
  });
  it('should not call the template populate function in case of a new notification', async () => {
    const fakePromiseFunction = jest.fn();
    const spy = jest.spyOn(api, 'createApi').mockImplementationOnce(() => {
      return {
        ...createFakeApi(),
        getRePopulateTemplateContent: (): Promise<{
          data: { message: string; title: string };
        }> => fakePromiseFunction(),
      };
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'new',
      baseNotificationData: null,
      setErrorRetrievePopulate: jest.fn(),
    };
    const { getByTestId } = render(
      <ConfirmationServiceProvider>
        <TestWrapper>
          <LifeCycleEdit {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );

    await waitFor(() => {
      expect(fakePromiseFunction).not.toHaveBeenCalled();
    });

    // Ensure no templates have been added
    expect(getByTestId('notification-text').textContent).toBe('');
    expect(getByTestId('notification-title').getAttribute('value')).toBe('');

    spy.mockRestore();
  });
  it('should not call the template populate function in case of a draft notification', async () => {
    const fakePromiseFunction = jest.fn();
    const spy = jest.spyOn(api, 'createApi').mockImplementationOnce(() => {
      return {
        ...createFakeApi(),
        getRePopulateTemplateContent: (): Promise<{
          data: { message: string; title: string };
        }> => fakePromiseFunction(),
      };
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'draft',
      baseNotificationData: mockDraftEvent,
      setErrorRetrievePopulate: jest.fn(),
    };
    const { getByTestId, findByText } = render(
      <ConfirmationServiceProvider>
        <TestWrapper>
          <LifeCycleEdit {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );

    await waitFor(() => {
      expect(fakePromiseFunction).not.toHaveBeenCalled();
    });

    // Check that the draft title and message are used
    expect(await findByText('This is a draft message')).toBeTruthy();
    expect(await getByTestId('notification-title').getAttribute('value')).toBe(
      'Geomagnetic Kp index alert',
    );

    spy.mockRestore();
  });
  it('should call the template populate function in case there are no validation errors', async () => {
    const fakePromiseFunction = jest.fn();

    const spy = jest.spyOn(api, 'createApi').mockImplementationOnce(() => {
      return {
        ...createFakeApi(),
        getRePopulateTemplateContent: (): Promise<{
          data: { message: string; title: string };
        }> => {
          fakePromiseFunction();
          return Promise.resolve({
            data: {
              message: 'fake template returned message',
              title: 'fake template returned title',
            },
          });
        },
      };
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: fakeEventList[17],
      setErrorRetrievePopulate: jest.fn(),
    };
    const { getByTestId, findByText } = render(
      <ConfirmationServiceProvider>
        <TestWrapper>
          <LifeCycleEdit {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );

    await waitFor(() => {
      expect(fakePromiseFunction).toHaveBeenCalled();
    });

    // Check that returned message and title are inserted into the form
    expect(await findByText('fake template returned message')).toBeTruthy();
    expect(await getByTestId('notification-title').getAttribute('value')).toBe(
      'fake template returned title',
    );

    spy.mockRestore();
  });
  it('should not call the template populate function in case there are validation errors', async () => {
    const fakePromiseFunction = jest.fn();

    const spy = jest.spyOn(api, 'createApi').mockImplementationOnce(() => {
      return {
        ...createFakeApi(),
        getRePopulateTemplateContent: (): Promise<{
          data: { message: string; title: string };
        }> => {
          fakePromiseFunction();
          return Promise.resolve({
            data: {
              message: 'fake template returned message',
              title: 'fake template returned title',
            },
          });
        },
      };
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: mockEventMissingData,
    };
    const { container, getByTestId } = render(
      <ConfirmationServiceProvider>
        <TestWrapper>
          <LifeCycleEdit {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );

    await waitFor(() => {
      expect(container.querySelector('.Mui-error')).toBeTruthy();
      expect(fakePromiseFunction).not.toHaveBeenCalled();
    });

    // Ensure no templates have been added
    expect(getByTestId('notification-text').textContent).toBe('');
    expect(getByTestId('notification-title').getAttribute('value')).toBe('');

    spy.mockRestore();
  });
  it('should call the template populate function when pressing the button and there are no validation errors', async () => {
    const fakePromiseFunction = jest.fn();

    const spy = jest.spyOn(api, 'createApi').mockImplementationOnce(() => {
      return {
        ...createFakeApi(),
        getRePopulateTemplateContent: (): Promise<{
          data: { message: string; title: string };
        }> => {
          fakePromiseFunction();
          return Promise.resolve({
            data: {
              message: 'fake template returned message',
              title: 'fake template returned title',
            },
          });
        },
      };
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: mockEventMissingData,
    };
    const { container, getByTestId, findByText } = render(
      <ConfirmationServiceProvider>
        <TestWrapper>
          <LifeCycleEdit {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );

    await waitFor(() => {
      expect(container.querySelector('.Mui-error')).toBeTruthy();
      expect(fakePromiseFunction).not.toHaveBeenCalled();
    });

    // Ensure no templates have been added
    expect(getByTestId('notification-text').textContent).toBe('');
    expect(getByTestId('notification-title').getAttribute('value')).toBe('');

    fireEvent.change(getByTestId('datasource-input'), {
      target: { value: 'GOES' },
    });

    fireEvent.click(getByTestId('notification-repopulate'));

    await waitFor(() => {
      expect(fakePromiseFunction).toHaveBeenCalled();
    });

    // Check that returned message and title are inserted into the form
    expect(await findByText('fake template returned message')).toBeTruthy();
    expect(await getByTestId('notification-title').getAttribute('value')).toBe(
      'fake template returned title',
    );

    spy.mockRestore();
  });
  it('should pass an error message to be shown in the dialog alert when retrieval of templates fails', async () => {
    const spy = jest.spyOn(api, 'createApi').mockImplementationOnce(() => {
      return {
        ...createFakeApi(),
        getRePopulateTemplateContent: (): Promise<{
          data: { title: string; message: string };
        }> => Promise.reject(new Error('test error message for templates')),
      };
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: fakeEventList[17],
      setErrorRetrievePopulate: jest.fn(),
    };
    const { getByTestId } = render(
      <ConfirmationServiceProvider>
        <TestWrapper>
          <LifeCycleEdit {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );

    await waitFor(() => {
      expect(props.setErrorRetrievePopulate).toHaveBeenCalled();
      expect(props.setErrorRetrievePopulate).toHaveBeenCalledWith(
        'test error message for templates',
      );
    });

    // Ensure no templates have been added
    expect(getByTestId('notification-text').textContent).toBe('');
    expect(getByTestId('notification-title').getAttribute('value')).toBe('');

    spy.mockRestore();
  });
  it('should pass an error message to be shown in the dialog alert when retrieval of templates fails in case AxiosError is detected', async () => {
    const fakeAxiosError = {
      isAxiosError: true,
      config: undefined,
      toJSON: undefined,
      name: 'API error',
      message: 'Some other message',
      response: {
        data: 'Axios error message',
        status: 400,
        statusText: '',
        config: undefined,
        headers: [],
      },
    } as Error;

    const spy = jest.spyOn(api, 'createApi').mockImplementationOnce(() => {
      return {
        ...createFakeApi(),
        getRePopulateTemplateContent: (): Promise<{
          data: { title: string; message: string };
        }> => Promise.reject(fakeAxiosError),
      };
    });

    const props = {
      toggleDialogOpen: jest.fn(),
      baseNotificationType: 'internalprovider',
      baseNotificationData: fakeEventList[17],
      setErrorRetrievePopulate: jest.fn(),
    };
    const { getByTestId } = render(
      <ConfirmationServiceProvider>
        <TestWrapper>
          <LifeCycleEdit {...props} />
        </TestWrapper>
      </ConfirmationServiceProvider>,
    );

    await waitFor(() => {
      expect(props.setErrorRetrievePopulate).toHaveBeenCalled();
      expect(props.setErrorRetrievePopulate).toHaveBeenCalledWith(
        'Axios error message',
      );
    });

    // Ensure no templates have been added
    expect(getByTestId('notification-text').textContent).toBe('');
    expect(getByTestId('notification-title').getAttribute('value')).toBe('');

    spy.mockRestore();
  });
});
