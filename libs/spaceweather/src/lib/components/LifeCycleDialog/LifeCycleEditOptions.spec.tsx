/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import moment from 'moment';
import { errorMessages } from '@opengeoweb/form-fields';

import LifeCycleEditOptions from './LifeCycleEditOptions';
import {
  fakeEventList,
  mockEvent,
  mockEventInternal,
  mockEventWarning,
} from '../../utils/fakedata';
import { constructBaseNotification } from './utils';
import {
  MESSAGE_RECENT_DATE,
  MESSAGE_DATE_IN_PAST,
} from './EditFormFields/validations';
import {
  dateFormat,
  EventCategory,
  EventCategoryDetail,
  SWEvent,
  ThresholdUnits,
} from '../../types';
import FormWrapper from './FormWrapper';
import {
  MESSAGE_END_DATE_AFTER_START_DATE,
  MESSAGE_END_DATE_IN_FUTURE,
} from './EditFormFields/StartTimeEndTime';

interface WrapperProps {
  baseNotificationData?: SWEvent;
  baseNotificationType: string;
  children: React.ReactNode;
}

const Wrapper: React.FC<WrapperProps> = ({
  baseNotificationData,
  baseNotificationType,
  children,
}: WrapperProps) => {
  return (
    <FormWrapper
      baseNotificationData={baseNotificationData}
      baseNotificationType={baseNotificationType}
    >
      {children}
    </FormWrapper>
  );
};

describe('src/components/LifeCycleDialog/LifeCycleEditOptions', () => {
  it('should display the correct date and time provided', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
      actionMode: 'Summarise',
    };
    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const startDateInput =
      getByTestId('start-date-picker').querySelector('input');
    expect((startDateInput as HTMLInputElement).value).toEqual(
      moment
        .utc(mockEventWarning.lifecycles.internalprovider.eventstart)
        .format(dateFormat),
    );

    const endDateInput = getByTestId('end-date-picker').querySelector('input');
    expect((endDateInput as HTMLInputElement).value).toEqual(
      moment
        .utc(mockEventWarning.lifecycles.internalprovider.eventend)
        .format(dateFormat),
    );
  });

  it('should give an error message when end date is before start date', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T09:59:00Z';
    mockEventWarning.lifecycles.internalprovider.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
      actionMode: 'Summarise',
    };

    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = getByTestId('end-date-picker');

    fireEvent.change(endDatePicker.querySelector('input'), {
      target: { value: end },
    });

    await waitFor(() =>
      expect(
        endDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error'),
    );
    expect(endDatePicker.nextElementSibling.textContent).toContain(
      MESSAGE_END_DATE_AFTER_START_DATE,
    );
  });

  it('should give an error message when end date is equal to start date', async () => {
    const start = '2020-07-01T10:00:00Z';
    mockEventWarning.lifecycles.internalprovider.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
      actionMode: 'Summarise',
    };

    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    const endDatePicker = getByTestId('end-date-picker');

    fireEvent.change(endDatePicker.querySelector('input'), {
      target: { value: start },
    });

    await waitFor(() =>
      expect(
        endDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error'),
    );
    expect(endDatePicker.nextElementSibling.textContent).toContain(
      MESSAGE_END_DATE_AFTER_START_DATE,
    );
  });

  it('should not give an error message when end date is valid', () => {
    const start = '2020-07-01T10:00:00Z';
    const end = moment.utc().add(1, 'hour').format('YYYY-MM-DD HH:mm');
    mockEventWarning.lifecycles.internalprovider.eventstart = start;
    mockEventWarning.lifecycles.internalprovider.eventend = end;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
      actionMode: 'Summarise',
    };
    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = getByTestId('end-date-picker');
    expect(
      endDatePicker.querySelector('label').getAttribute('class'),
    ).not.toContain('Mui-error');
    expect(endDatePicker.querySelector('p')).toBeFalsy();
  });

  it('should give an error message when end date is in the past and its not allowed', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T11:00:00Z';
    mockEventWarning.lifecycles.internalprovider.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = getByTestId('end-date-picker');

    fireEvent.change(endDatePicker.querySelector('input'), {
      target: { value: end },
    });

    await waitFor(() =>
      expect(
        endDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error'),
    );

    expect(endDatePicker.nextElementSibling.textContent).toContain(
      MESSAGE_END_DATE_IN_FUTURE,
    );
  });

  it('should give an error message when end date is current datetime and end date in the past is not allowed', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = moment.utc().format();
    mockEventWarning.lifecycles.internalprovider.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = getByTestId('end-date-picker');

    fireEvent.change(endDatePicker.querySelector('input'), {
      target: { value: end },
    });

    await waitFor(() =>
      expect(
        endDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error'),
    );
  });

  it('should not display end date when label is Alert and not summarising', async () => {
    const start = '2020-07-01T10:00:00Z';
    mockEvent.lifecycles.internalprovider.eventstart = start;
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    const { queryByTestId } = render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('end-date-picker')).toBeFalsy();
  });

  it('should give an error message when end date is empty and label is Warning', async () => {
    const start = '2020-07-01T10:00:00Z';
    mockEventWarning.lifecycles.internalprovider.eventstart = start;
    const props = {
      statusTagContent: 'Warning',
      eventTypeDisabled: false,
    };

    const { getByTestId, findByText, queryByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = getByTestId('end-date-picker');

    fireEvent.change(endDatePicker.querySelector('input'), {
      target: { value: '' },
    });

    await waitFor(() =>
      expect(
        endDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error'),
    );

    // Expect end date to be removed when changing to label = alert
    fireEvent.mouseDown(getByTestId('label-select'));
    const menuItem = await findByText('Alert');
    fireEvent.click(menuItem);
    await waitFor(() => expect(queryByTestId('end-date-picker')).toBeFalsy());
  });

  it('should not give an error message when end date is in the past and it is allowed', async () => {
    const start = moment.utc().subtract(4, 'days').format(dateFormat);
    const end = moment.utc().subtract(1, 'day').format(dateFormat);
    mockEventWarning.lifecycles.internalprovider.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
      actionMode: 'Summarise',
    };
    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = getByTestId('end-date-picker');

    fireEvent.change(endDatePicker.querySelector('input'), {
      target: { value: end },
    });

    await waitFor(() =>
      expect(
        endDatePicker.querySelector('label').getAttribute('class'),
      ).not.toContain('Mui-error'),
    );
    expect(endDatePicker.nextElementSibling).toBeFalsy();
  });

  it('should give an error message when start date is empty', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const startDatePicker = getByTestId('start-date-picker');

    fireEvent.change(startDatePicker.querySelector('input'), {
      target: { value: '' },
    });

    await waitFor(() =>
      expect(
        startDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error'),
    );

    expect(startDatePicker.nextElementSibling.textContent).toContain(
      errorMessages.required,
    );
  });
  it('should give an error message when start date is more than 1 week in the past', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const startDatePicker = getByTestId('start-date-picker');
    const newStartDate = moment
      .utc()
      .subtract(7, 'days')
      .subtract(1, 'hour')
      .format(dateFormat);

    fireEvent.change(startDatePicker.querySelector('input'), {
      target: { value: newStartDate },
    });

    await waitFor(() =>
      expect(
        startDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error'),
    );

    expect(startDatePicker.nextElementSibling.textContent).toContain(
      MESSAGE_RECENT_DATE,
    );
  });
  it('should give an error message when start date has wrong format', async () => {
    const start = '2020-07-0233 11:00';

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const startDatePicker = getByTestId('start-date-picker');

    fireEvent.change(startDatePicker.querySelector('input'), {
      target: { value: start },
    });

    await waitFor(() =>
      expect(
        startDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error'),
    );

    expect(startDatePicker.nextElementSibling.textContent).toContain(
      errorMessages.isValidDate,
    );
  });

  it('should update the tag when changing the label', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    expect(getByTestId('notification-tag').textContent).toEqual('Alert');
    fireEvent.mouseDown(getByTestId('label-select'));
    const menuItem = await findByText('Warning');
    fireEvent.click(menuItem);
    await waitFor(() =>
      expect(getByTestId('notification-tag').textContent).toEqual('Warning'),
    );
  });

  it('should be possible to select a different category and should automatically update the event level options and label', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    expect(getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    expect(getByTestId('statusTag').textContent).toEqual('Alert');
    fireEvent.mouseDown(getByTestId('eventlevel-select'));
    const menuItemLevelR = await findByText('R1');
    fireEvent.click(menuItemLevelR);
    expect(getByTestId('eventlevel-select').textContent).toEqual('R1');

    fireEvent.mouseDown(getByTestId('category-select'));
    const menuItemCategory = await findByText(EventCategory.GEOMAGNETIC);
    await waitFor(() => fireEvent.click(menuItemCategory));
    expect(getByTestId('category-select').textContent).toEqual(
      EventCategory.GEOMAGNETIC,
    );
    expect(getByTestId('statusTag').textContent).toEqual('Watch');
    fireEvent.mouseDown(getByTestId('categorydetail-select'));
    const menuItemCategoryDetail = await findByText(
      EventCategoryDetail.KP_INDEX,
    );
    await waitFor(() => fireEvent.click(menuItemCategoryDetail));
    expect(getByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.KP_INDEX,
    );
    expect(getByTestId('statusTag').textContent).toEqual('Alert');
    fireEvent.mouseDown(getByTestId('eventlevel-select'));
    const menuItemLevelG = await findByText('G1');
    await waitFor(() => fireEvent.click(menuItemLevelG));
    expect(getByTestId('eventlevel-select').textContent).toEqual('G1');
  });

  it('should hide category, categorydetail and label input for user when eventType is disabled', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T11:00:00Z';
    mockEvent.lifecycles.internalprovider.eventstart = start;
    mockEvent.lifecycles.internalprovider.eventend = end;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: true,
    };
    const { queryByTestId, container } = render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const categoryHiddenInput = container.querySelector('[name="category"]');
    expect(categoryHiddenInput).toBeTruthy();
    expect(categoryHiddenInput.getAttribute('value')).toEqual(
      mockEvent.category,
    );
    const categorydetailHiddenInput = container.querySelector(
      '[name="categorydetail"]',
    );
    expect(categorydetailHiddenInput).toBeTruthy();
    expect(categorydetailHiddenInput.getAttribute('value')).toEqual(
      mockEvent.categorydetail,
    );
    const labelHiddenInput = container.querySelector('[name="label"]');
    expect(labelHiddenInput).toBeTruthy();
    expect(labelHiddenInput.getAttribute('value')).toEqual(
      mockEvent.lifecycles.internalprovider.label,
    );
    expect(queryByTestId('category-select')).toBeFalsy();
    expect(queryByTestId('categorydetail-select')).toBeFalsy();
    expect(queryByTestId('label-select')).toBeFalsy();
    expect(queryByTestId('statusTag')).toBeTruthy();
    expect(queryByTestId('eventlevel-select')).toBeTruthy();
    expect(queryByTestId('threshold-select')).toBeTruthy();
    expect(queryByTestId('start-date-picker')).toBeTruthy();
    expect(queryByTestId('end-date-picker')).toBeFalsy();
  });

  it('should be possible to select an event level and should automatically update threshold for KP_INDEX', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    expect(getByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.KP_INDEX,
    );
    expect(getByTestId('eventlevel-select').textContent).toEqual('G1');
    expect(getByTestId('threshold-select').textContent).toEqual('5');
    fireEvent.mouseDown(getByTestId('eventlevel-select'));
    const menuItem = await findByText('G2');
    fireEvent.click(menuItem);
    await waitFor(() => {
      expect(getByTestId('eventlevel-select').textContent).toEqual('G2');
      expect(getByTestId('threshold-select').textContent).toEqual('6');
    });
  });

  it('should be possible to select a threshold and should automatically update eventlevel for KP_INDEX', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    expect(getByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.KP_INDEX,
    );
    expect(getByTestId('eventlevel-select').textContent).toEqual('G1');
    expect(getByTestId('threshold-select').textContent).toEqual('5');
    fireEvent.mouseDown(getByTestId('threshold-select'));
    const menuItem = await findByText('7');
    fireEvent.click(menuItem);
    await waitFor(() => {
      expect(getByTestId('threshold-select').textContent).toEqual('7');
      expect(getByTestId('eventlevel-select').textContent).toEqual('G3');
    });
  });

  it('should be possible to change the startdate', async () => {
    const start = '2020-07-01T10:00:00Z';
    mockEvent.lifecycles.internalprovider.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEvent}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const startDateInput =
      getByTestId('start-date-picker').querySelector('input');
    expect((startDateInput as HTMLInputElement).value).toEqual(
      moment.utc(start).format(dateFormat),
    );
    const newDate = '2020-06-28 09:00';
    await waitFor(() =>
      fireEvent.change(startDateInput, {
        target: { value: newDate },
      }),
    );
    expect((startDateInput as HTMLInputElement).value).toEqual(newDate);
  });

  it('should be possible to change the enddate', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-01T11:00:00Z';
    mockEventWarning.lifecycles.internalprovider.eventstart = start;
    mockEventWarning.lifecycles.internalprovider.eventend = end;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDateInput = getByTestId('end-date-picker').querySelector('input');
    expect((endDateInput as HTMLInputElement).value).toEqual(
      moment.utc(end).format(dateFormat),
    );
    const newDate = '2020-07-28 09:00';
    await waitFor(() =>
      fireEvent.change(endDateInput, {
        target: { value: newDate },
      }),
    );

    expect((endDateInput as HTMLInputElement).value).toEqual(newDate);
  });

  it('should hide category, categorydetail and label fields when summarizing', async () => {
    const props = {
      statusTagContent: 'Summary',
      eventTypeDisabled: true,

      actionMode: 'Summarise',
    };
    const { container, queryByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventInternal}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    // Hidden inputs should be present
    expect(container.querySelector('[name="category"]')).toBeTruthy();
    expect(container.querySelector('[name="categorydetail"]')).toBeTruthy();
    expect(container.querySelector('[name="label"]')).toBeTruthy();

    // But visible inputs should not be there
    expect(queryByTestId('category-select')).toBeFalsy();
    expect(queryByTestId('categorydetail-select')).toBeFalsy();
    expect(queryByTestId('label-select')).toBeFalsy();

    // Visible inputs for other fields should be there
    expect(queryByTestId('threshold-select')).toBeTruthy();
    expect(queryByTestId('thresholdunit-input')).toBeTruthy();
    expect(queryByTestId('eventlevel-select')).toBeTruthy();
    expect(queryByTestId('datasource-input')).toBeTruthy();
  });

  it('should hide category, categorydetail and label fields when cancelling', async () => {
    const props = {
      statusTagContent: 'Cancelled',
      eventTypeDisabled: true,

      actionMode: 'Cancel',
    };
    const { container, queryByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventInternal}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    // Hidden inputs should be present
    expect(container.querySelector('[name="category"]')).toBeTruthy();
    expect(container.querySelector('[name="categorydetail"]')).toBeTruthy();
    expect(container.querySelector('[name="label"]')).toBeTruthy();

    // But visible inputs should not be there
    expect(queryByTestId('category-select')).toBeFalsy();
    expect(queryByTestId('categorydetail-select')).toBeFalsy();
    expect(queryByTestId('label-select')).toBeFalsy();

    // Visible inputs for other fields should be there
    expect(queryByTestId('threshold-select')).toBeTruthy();
    expect(queryByTestId('thresholdunit-input')).toBeTruthy();
    expect(queryByTestId('eventlevel-select')).toBeTruthy();
    expect(queryByTestId('datasource-input')).toBeTruthy();
  });

  it('should give an error message when end date has an invalid format', async () => {
    const start = '2020-07-01T10:00:00Z';
    const end = '2020-07-0233 11:00';
    mockEventWarning.lifecycles.internalprovider.eventstart = start;

    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };

    const { getByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventWarning}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    const endDatePicker = getByTestId('end-date-picker');

    fireEvent.change(endDatePicker.querySelector('input'), {
      target: { value: end },
    });

    await waitFor(() =>
      expect(
        endDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error'),
    );
  });

  it('should update the categorydetail, label and statustag when changing category', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    // New notification opens with default values for XRAY_RADIO_BLACKOUT
    expect(getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    // Category detail should be empty, disabled and invisible for category XRAY_RADIO_BLACKOUT
    expect(getByTestId('categorydetail-select').textContent).toMatch('');
    expect(getByTestId('categorydetail-select').classList).toContain(
      'Mui-disabled',
    );
    expect(
      getByTestId('categorydetail-select').parentElement.parentElement
        .className,
    ).toContain('makeStyles-hide');
    expect(getByTestId('label-select').textContent).toEqual('Alert');
    expect(getByTestId('statusTag').textContent).toEqual('Alert');

    // Change category to GEOMAGNETIC
    fireEvent.mouseDown(getByTestId('category-select'));
    const geomagnetic = await findByText(EventCategory.GEOMAGNETIC);
    await waitFor(() => fireEvent.click(geomagnetic));
    expect(getByTestId('category-select').textContent).toEqual(
      EventCategory.GEOMAGNETIC,
    );
    // Categorydetail, label and statustag should be updated
    expect(getByTestId('categorydetail-select')).toBeTruthy();
    expect(getByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.GEOMAGNETIC_STORM,
    );
    expect(getByTestId('label-select').textContent).toEqual('Watch');
    expect(getByTestId('statusTag').textContent).toEqual('Watch');
  });
  it('should update the label and statustag when changing categorydetail', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    // Change category to GEOMAGNETIC so we have a categorydetail field
    fireEvent.mouseDown(getByTestId('category-select'));
    const geomagnetic = await findByText(EventCategory.GEOMAGNETIC);
    await waitFor(() => fireEvent.click(geomagnetic));
    expect(getByTestId('category-select').textContent).toEqual(
      EventCategory.GEOMAGNETIC,
    );
    // Categorydetail, label and statustag should be updated
    expect(getByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.GEOMAGNETIC_STORM,
    );
    expect(getByTestId('label-select').textContent).toEqual('Watch');
    expect(getByTestId('statusTag').textContent).toEqual('Watch');
    // change categorydetail to KP_INDEX
    fireEvent.mouseDown(getByTestId('categorydetail-select'));
    const kpindex = await findByText(EventCategoryDetail.KP_INDEX);
    await waitFor(() => fireEvent.click(kpindex));
    expect(getByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.KP_INDEX,
    );
    // label and statustag should be updated
    expect(getByTestId('label-select').textContent).toEqual('Alert');
    expect(getByTestId('statusTag').textContent).toEqual('Alert');
  });
  it('should update threshold and xrayclass when changing eventlevel for XRAY_RADIO_BLACKOUT', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    // check eventlevel, threshold, xrayclass values
    expect(getByTestId('eventlevel-select').textContent).toMatch('');
    expect(getByTestId('threshold-select').textContent).toMatch('');
    expect(getByTestId('xrayclass-select').textContent).toMatch('');

    // change eventlevel
    fireEvent.mouseDown(getByTestId('eventlevel-select'));
    const newlevel = await findByText('R2');
    fireEvent.click(newlevel);
    await waitFor(() =>
      expect(getByTestId('eventlevel-select').textContent).toEqual('R2'),
    );

    // threshold and xrayclass should be updated
    expect(getByTestId('threshold-select').textContent).toEqual('0.00005');
    expect(getByTestId('xrayclass-select').textContent).toEqual('M5');
  });
  it('should update eventlevel and xrayclass when changing threshold for XRAY_RADIO_BLACKOUT', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    // check eventlevel, threshold, xrayclass values
    expect(getByTestId('eventlevel-select').textContent).toMatch('');
    expect(getByTestId('threshold-select').textContent).toMatch('');
    expect(getByTestId('xrayclass-select').textContent).toMatch('');

    // change threshold
    fireEvent.mouseDown(getByTestId('threshold-select'));
    const newlevel = await findByText('0.00005');
    fireEvent.click(newlevel);
    await waitFor(() =>
      expect(getByTestId('threshold-select').textContent).toEqual('0.00005'),
    );

    // eventlevel and xrayclass should be updated
    expect(getByTestId('eventlevel-select').textContent).toEqual('R2');
    expect(getByTestId('xrayclass-select').textContent).toEqual('M5');
  });
  it('should update eventlevel and threshold when changing xrayclass for XRAY_RADIO_BLACKOUT', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(getByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    // check eventlevel, threshold, xrayclass values
    expect(getByTestId('eventlevel-select').textContent).toMatch('');
    expect(getByTestId('threshold-select').textContent).toMatch('');
    expect(getByTestId('xrayclass-select').textContent).toMatch('');

    // change xrayclass
    fireEvent.mouseDown(getByTestId('xrayclass-select'));
    const newlevel = await findByText('M5');
    fireEvent.click(newlevel);
    await waitFor(() =>
      expect(getByTestId('xrayclass-select').textContent).toEqual('M5'),
    );

    // eventlevel and threshold should be updated
    expect(getByTestId('eventlevel-select').textContent).toEqual('R2');
    expect(getByTestId('threshold-select').textContent).toEqual('0.00005');
  });
  it('should show xray class, peak class, peak flux, peak flux time and end time when summarising XRAY_RADIO_BLACKOUT', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,

      actionMode: 'Summarise',
    };
    const { queryByTestId } = render(
      <Wrapper
        baseNotificationData={mockEventInternal}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    expect(queryByTestId('xrayclass-select')).toBeTruthy();
    expect(queryByTestId('peakclass-input')).toBeTruthy();
    expect(queryByTestId('peakflux-input')).toBeTruthy();
    expect(queryByTestId('peakfluxtime-picker')).toBeTruthy();
    expect(queryByTestId('end-date-picker')).toBeTruthy();
  });
  it('should show correct fields for Geomagnetic Storm Watch', () => {
    const props = {
      statusTagContent: 'Watch',
      eventTypeDisabled: false,
    };
    const { queryByTestId } = render(
      <Wrapper
        baseNotificationData={fakeEventList[14]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.GEOMAGNETIC,
    );
    expect(queryByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.GEOMAGNETIC_STORM,
    );
    expect(queryByTestId('initialgscale-input')).toBeTruthy();
    expect(queryByTestId('start-date-picker')).toBeTruthy();
    expect(queryByTestId('end-date-picker')).toBeTruthy();
    expect(queryByTestId('statusTag').textContent).toEqual('Watch');

    expect(queryByTestId('shocktime-picker')).toBeFalsy();
    expect(queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(queryByTestId('eventlevel-select')).toBeFalsy();
    expect(queryByTestId('datasource-input')).toBeFalsy();
    expect(queryByTestId('threshold-select')).toBeFalsy();
    expect(queryByTestId('thresholdunit-input')).toBeFalsy();
    expect(queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Geomagnetic Sudden Impuls Alert', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { queryByTestId } = render(
      <Wrapper
        baseNotificationData={fakeEventList[1]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.GEOMAGNETIC,
    );
    expect(queryByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.GEOMAGNETIC_SUDDEN_IMPULSE,
    );
    expect(queryByTestId('statusTag').textContent).toEqual('Alert');
    expect(queryByTestId('datasource-input')).toBeTruthy();
    expect(queryByTestId('start-date-picker')).toBeTruthy();
    expect(queryByTestId('impulsetime-picker')).toBeTruthy();
    expect(queryByTestId('magnetometerdeflection-input')).toBeTruthy();

    expect(queryByTestId('shocktime-picker')).toBeFalsy();
    expect(queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(queryByTestId('initialgscale-input')).toBeFalsy();
    expect(queryByTestId('end-date-picker')).toBeFalsy();
    expect(queryByTestId('eventlevel-select')).toBeFalsy();
    expect(queryByTestId('threshold-select')).toBeFalsy();
    expect(queryByTestId('thresholdunit-input')).toBeFalsy();
    expect(queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Geomagnetic Sudden Impuls Warning', () => {
    const props = {
      statusTagContent: 'Warning',
      eventTypeDisabled: false,
    };
    const { queryByTestId } = render(
      <Wrapper
        baseNotificationData={fakeEventList[8]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.GEOMAGNETIC,
    );
    expect(queryByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.GEOMAGNETIC_SUDDEN_IMPULSE,
    );
    expect(queryByTestId('statusTag').textContent).toEqual('Warning');
    expect(queryByTestId('datasource-input')).toBeTruthy();
    expect(queryByTestId('start-date-picker')).toBeTruthy();
    expect(queryByTestId('end-date-picker')).toBeTruthy();
    expect(queryByTestId('shocktime-picker')).toBeTruthy();
    expect(queryByTestId('observedpolaritybz-input')).toBeTruthy();
    expect(queryByTestId('observedsolarwind-input')).toBeTruthy();

    expect(queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(queryByTestId('initialgscale-input')).toBeFalsy();
    expect(queryByTestId('eventlevel-select')).toBeFalsy();
    expect(queryByTestId('threshold-select')).toBeFalsy();
    expect(queryByTestId('thresholdunit-input')).toBeFalsy();
    expect(queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Geomagnetic KP Index Alert', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { queryByTestId } = render(
      <Wrapper
        baseNotificationData={fakeEventList[11]}
        baseNotificationType="externalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.GEOMAGNETIC,
    );
    expect(queryByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.KP_INDEX,
    );
    expect(queryByTestId('statusTag').textContent).toEqual('Alert');
    expect(queryByTestId('start-date-picker')).toBeTruthy();
    expect(queryByTestId('eventlevel-select')).toBeTruthy();
    expect(queryByTestId('datasource-input')).toBeTruthy();
    expect(queryByTestId('threshold-select')).toBeTruthy();

    expect(queryByTestId('thresholdunit-input')).toBeFalsy();
    expect(queryByTestId('shocktime-picker')).toBeFalsy();
    expect(queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(queryByTestId('end-date-picker')).toBeFalsy();
    expect(queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(queryByTestId('initialgscale-input')).toBeFalsy();
    expect(queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Proton Flux 100', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { queryByTestId } = render(
      <Wrapper
        baseNotificationData={fakeEventList[15]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.PROTON_FLUX,
    );
    expect(queryByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.PROTON_FLUX_100,
    );
    expect(queryByTestId('statusTag').textContent).toEqual('Alert');
    expect(queryByTestId('start-date-picker')).toBeTruthy();
    expect(queryByTestId('datasource-input')).toBeTruthy();
    expect(queryByTestId('threshold-select')).toBeTruthy();
    expect(queryByTestId('thresholdunit-input')).toBeTruthy();

    expect(queryByTestId('eventlevel-select')).toBeFalsy();
    expect(queryByTestId('shocktime-picker')).toBeFalsy();
    expect(queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(queryByTestId('end-date-picker')).toBeFalsy();
    expect(queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(queryByTestId('initialgscale-input')).toBeFalsy();
    expect(queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Proton Flux 10', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { queryByTestId } = render(
      <Wrapper
        baseNotificationData={fakeEventList[16]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.PROTON_FLUX,
    );
    expect(queryByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.PROTON_FLUX_10,
    );
    expect(queryByTestId('statusTag').textContent).toEqual('Alert');
    expect(queryByTestId('start-date-picker')).toBeTruthy();
    expect(queryByTestId('datasource-input')).toBeTruthy();
    expect(queryByTestId('eventlevel-select')).toBeTruthy();
    expect(queryByTestId('threshold-select')).toBeTruthy();
    expect(queryByTestId('thresholdunit-input')).toBeTruthy();

    expect(queryByTestId('shocktime-picker')).toBeFalsy();
    expect(queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(queryByTestId('end-date-picker')).toBeFalsy();
    expect(queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(queryByTestId('initialgscale-input')).toBeFalsy();
    expect(queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should update threshold when changing eventlevel for Proton Flux 10', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { queryByTestId, getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={fakeEventList[16]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.PROTON_FLUX,
    );
    expect(queryByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.PROTON_FLUX_10,
    );
    expect(queryByTestId('eventlevel-select')).toBeTruthy();
    expect(queryByTestId('threshold-select')).toBeTruthy();

    // check eventlevel and threshold values
    expect(getByTestId('eventlevel-select').textContent).toMatch('');
    expect(getByTestId('threshold-select').textContent).toMatch('');

    // change eventlevel
    fireEvent.mouseDown(getByTestId('eventlevel-select'));
    const newlevel = await findByText('S2');
    fireEvent.click(newlevel);
    await waitFor(() =>
      expect(getByTestId('eventlevel-select').textContent).toEqual('S2'),
    );

    // threshold should be updated
    expect(getByTestId('threshold-select').textContent).toEqual('100');
  });

  it('should show correct fields for Electron Flux 2', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { queryByTestId } = render(
      <Wrapper
        baseNotificationData={fakeEventList[17]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.ELECTRON_FLUX,
    );
    expect(queryByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.ELECTRON_FLUX_2,
    );
    expect(queryByTestId('statusTag').textContent).toEqual('Alert');
    expect(queryByTestId('start-date-picker')).toBeTruthy();
    expect(queryByTestId('datasource-input')).toBeTruthy();
    expect(queryByTestId('threshold-input')).toBeTruthy();
    expect(queryByTestId('thresholdunit-input')).toBeTruthy();

    expect(queryByTestId('eventlevel-select')).toBeFalsy();
    expect(queryByTestId('threshold-select')).toBeFalsy();
    expect(queryByTestId('shocktime-picker')).toBeFalsy();
    expect(queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(queryByTestId('end-date-picker')).toBeFalsy();
    expect(queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(queryByTestId('initialgscale-input')).toBeFalsy();
    expect(queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should show correct fields for Electron Fluence', () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { queryByTestId } = render(
      <Wrapper
        baseNotificationData={fakeEventList[9]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.ELECTRON_FLUX,
    );
    expect(queryByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.ELECTRON_FLUENCE,
    );
    expect(queryByTestId('statusTag').textContent).toEqual('Alert');
    expect(queryByTestId('start-date-picker')).toBeTruthy();
    expect(queryByTestId('end-date-picker')).toBeTruthy();
    expect(queryByTestId('datasource-input')).toBeTruthy();
    expect(queryByTestId('threshold-input')).toBeTruthy();
    expect(queryByTestId('thresholdunit-input')).toBeTruthy();

    expect(queryByTestId('eventlevel-select')).toBeFalsy();
    expect(queryByTestId('threshold-select')).toBeFalsy();
    expect(queryByTestId('shocktime-picker')).toBeFalsy();
    expect(queryByTestId('observedpolaritybz-input')).toBeFalsy();
    expect(queryByTestId('observedsolarwind-input')).toBeFalsy();
    expect(queryByTestId('impulsetime-picker')).toBeFalsy();
    expect(queryByTestId('magnetometerdeflection-input')).toBeFalsy();
    expect(queryByTestId('initialgscale-input')).toBeFalsy();
    expect(queryByTestId('xrayclass-select')).toBeFalsy();
  });
  it('should update the threshold unit when switching category detail for ELECTRON_FLUX', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { queryByTestId, getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={fakeEventList[17]}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.ELECTRON_FLUX,
    );
    expect(queryByTestId('categorydetail-select').textContent).toEqual(
      EventCategoryDetail.ELECTRON_FLUX_2,
    );
    expect(queryByTestId('thresholdunit-input').getAttribute('value')).toEqual(
      ThresholdUnits['ELECTRON_FLUX_2'],
    );
    fireEvent.mouseDown(getByTestId('categorydetail-select'));
    const menuItemCategoryDetail = await findByText(
      EventCategoryDetail.ELECTRON_FLUENCE,
    );
    await waitFor(() => fireEvent.click(menuItemCategoryDetail));
    expect(queryByTestId('thresholdunit-input').getAttribute('value')).toEqual(
      ThresholdUnits['ELECTRON_FLUENCE'],
    );
    fireEvent.mouseDown(getByTestId('categorydetail-select'));
    const menuItemCategoryDetail2 = await findByText(
      EventCategoryDetail.ELECTRON_FLUX_2,
    );
    await waitFor(() => fireEvent.click(menuItemCategoryDetail2));
    expect(queryByTestId('thresholdunit-input').getAttribute('value')).toEqual(
      ThresholdUnits['ELECTRON_FLUX_2'],
    );
  });
  it('should update the threshold unit when switching category', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { queryByTestId, getByTestId, findByText } = render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );
    expect(queryByTestId('thresholdunit-input').getAttribute('value')).toEqual(
      ThresholdUnits['XRAY_RADIO_BLACKOUT'],
    );
    fireEvent.mouseDown(getByTestId('category-select'));
    const menuItemCategory = await findByText(EventCategory.PROTON_FLUX);
    await waitFor(() => fireEvent.click(menuItemCategory));
    expect(queryByTestId('thresholdunit-input').getAttribute('value')).toEqual(
      ThresholdUnits['PROTON_FLUX'],
    );
  });
  it('should show an error that startdate should be in the past for an Alert but not for a Warning', async () => {
    const props = {
      statusTagContent: 'Alert',
      eventTypeDisabled: false,
    };
    const { queryByTestId, getByTestId, findByText, queryByText } = render(
      <Wrapper
        baseNotificationData={constructBaseNotification(null, 'new') as SWEvent}
        baseNotificationType="new"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.XRAY_RADIO_BLACKOUT,
    );

    fireEvent.mouseDown(getByTestId('category-select'));
    const menuItemCategory = await findByText(EventCategory.PROTON_FLUX);
    await waitFor(() => fireEvent.click(menuItemCategory));

    expect(queryByTestId('category-select').textContent).toEqual(
      EventCategory.PROTON_FLUX,
    );

    const startDatePicker = getByTestId('start-date-picker');

    // Set start date to future
    const startDateInput = startDatePicker.querySelector('input');
    const newDate = moment.utc().add('1', 'hour').format(dateFormat);
    await waitFor(() =>
      fireEvent.change(startDateInput, {
        target: { value: newDate },
      }),
    );
    expect((startDateInput as HTMLInputElement).value).toEqual(newDate);

    await waitFor(() => {
      expect(
        startDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error');
      expect(queryByText(MESSAGE_DATE_IN_PAST)).toBeTruthy();
    });

    const labelDropdown = getByTestId('label-select');

    // change to Warning
    fireEvent.mouseDown(labelDropdown);
    const menuItemWarning = await findByText('Warning');
    fireEvent.click(menuItemWarning);
    await waitFor(() => {
      expect(
        startDatePicker.querySelector('label').getAttribute('class'),
      ).not.toContain('Mui-error');
      expect(queryByText(MESSAGE_DATE_IN_PAST)).toBeFalsy();
    });

    // change to Alert
    fireEvent.mouseDown(labelDropdown);
    const menuItemAlert = await findByText('Alert');
    fireEvent.click(menuItemAlert);

    await waitFor(() => {
      expect(
        startDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error');
      expect(queryByText(MESSAGE_DATE_IN_PAST)).toBeTruthy();
    });
  });
  it('should show an error for enddate when its more than a week in the past', async () => {
    const props = {
      statusTagContent: 'Warning',
      eventTypeDisabled: false,
      actionMode: 'Cancel',
    };
    const event = fakeEventList[2];

    const { getByTestId, queryAllByText } = render(
      <Wrapper
        baseNotificationData={event}
        baseNotificationType="internalprovider"
      >
        <LifeCycleEditOptions {...props} />
      </Wrapper>,
    );
    expect(getByTestId('category-select').textContent).toEqual(
      EventCategory.PROTON_FLUX,
    );

    const startDatePicker = getByTestId('start-date-picker');
    fireEvent.change(startDatePicker.querySelector('input'), {
      target: { value: moment.utc().subtract(10, 'days').format(dateFormat) },
    });

    const endDatePicker = getByTestId('end-date-picker');
    fireEvent.change(endDatePicker.querySelector('input'), {
      target: { value: moment.utc().subtract(8, 'days').format(dateFormat) },
    });

    await waitFor(() => {
      expect(
        endDatePicker.querySelector('label').getAttribute('class'),
      ).toContain('Mui-error');
      expect(queryAllByText(MESSAGE_RECENT_DATE)).toHaveLength(2);
    });
  });
});
