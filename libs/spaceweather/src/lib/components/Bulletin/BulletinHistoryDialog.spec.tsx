/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { ApiProvider } from '@opengeoweb/api';
import * as utils from '@opengeoweb/api';
import BulletinHistoryDialog from './BulletinHistoryDialog';
import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';

jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual('../../../../api/src/index') as Record<
    string,
    unknown
  >),
}));

describe('src/components/Bulletin/BulletinHistoryDialog', () => {
  it('should render correctly and display the bulletin history list and select the first one ', async () => {
    const props = {
      open: true,
      toggleStatus: jest.fn(),
    };
    const { queryByTestId, getByTestId, getAllByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <BulletinHistoryDialog {...props} />
      </ApiProvider>,
    );

    await waitFor(() => {
      expect(getAllByTestId('historyItem')).toBeTruthy();
      expect(getByTestId('bulletinviewer')).toBeTruthy();
      expect(queryByTestId('history-dialog-loading')).toBeFalsy();
      expect(queryByTestId('history-dialog-error')).toBeFalsy();
    });

    // check first item is displayed
    expect(getByTestId('bulletinContent').textContent).toContain(
      'First bulletin content',
    );
  });

  it('should display an error if error returned by api', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: false,
      error: new Error('error'),
      result: null,
      fetchApiData: jest.fn(),
    });
    const props = {
      open: true,
      toggleStatus: jest.fn(),
    };
    const { queryByTestId, getByTestId } = render(
      <TestWrapper>
        <BulletinHistoryDialog {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(getByTestId('history-dialog-error')).toBeTruthy();
      expect(queryByTestId('history-dialog-loading')).toBeFalsy();
      expect(queryByTestId('historyItem')).toBeFalsy();
      expect(queryByTestId('bulletinviewer')).toBeFalsy();
    });
  });
  it('should display the loading screen while api is still loading', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: true,
      error: null,
      result: null,
      fetchApiData: jest.fn(),
    });
    const props = {
      open: true,
      toggleStatus: jest.fn(),
    };
    const { queryByTestId, getByTestId } = render(
      <TestWrapper>
        <BulletinHistoryDialog {...props} />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(getByTestId('history-dialog-loading')).toBeTruthy();
      expect(queryByTestId('history-dialog-error')).toBeFalsy();
      expect(queryByTestId('historyItem')).toBeFalsy();
      expect(queryByTestId('bulletinviewer')).toBeFalsy();
    });
  });

  it('should update the selected bulletin when clicking an item in the history list', async () => {
    const props = {
      open: true,
      toggleStatus: jest.fn(),
    };
    const { getAllByTestId, getByTestId, queryByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <BulletinHistoryDialog {...props} />
      </ApiProvider>,
    );

    await waitFor(() => {
      expect(getAllByTestId('historyItem')).toBeTruthy();
      expect(getByTestId('bulletinviewer')).toBeTruthy();
      expect(queryByTestId('history-dialog-loading')).toBeFalsy();
      expect(queryByTestId('history-dialog-error')).toBeFalsy();
    });

    // check first item is displayed
    expect(getByTestId('bulletinContent').textContent).toContain(
      'First bulletin content',
    );

    // click second item
    const historyItems = getAllByTestId('historyItem');
    fireEvent.click(historyItems[1]);

    // check second item is displayed
    expect(getByTestId('bulletinContent').textContent).toContain(
      'Second bulletin content',
    );
  });
});
