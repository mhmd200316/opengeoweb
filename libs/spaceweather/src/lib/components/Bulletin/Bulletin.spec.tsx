/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { ApiProvider } from '@opengeoweb/api';
import * as utils from '@opengeoweb/api';
import Bulletin from './Bulletin';
import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { fakeBulletins } from '../../utils/fakedata';

jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual('../../../../api/src/index') as Record<
    string,
    unknown
  >),
}));

describe('src/components/Bulletin/Bulletin', () => {
  it('should show the history dialog', async () => {
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('history'));
    expect(getByTestId('history-dialog')).toBeTruthy();

    // When closing the dialog - should be hidden
    fireEvent.click(getByTestId('contentdialog-close'));
    await waitFor(() => expect(queryByTestId('history-dialog')).toBeFalsy());
  });

  it('should show the bulletin viewer once the latest bulletin has been retrieved', async () => {
    const { getByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <Bulletin />
      </ApiProvider>,
    );
    await waitFor(() => expect(getByTestId('bulletinviewer')).toBeTruthy());
  });

  it('should show the bulletin tabs and have the technical forecast selected as default', async () => {
    const { getByTestId } = render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(getByTestId('bulletintabs')).toBeTruthy();
      expect(getByTestId('technical').classList).toContain('Mui-selected');
      expect(getByTestId('plain').classList).not.toContain('Mui-selected');
    });
  });
  it('should show as loading as long as the bulletin is still loading', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: true,
      error: null,
      result: null,
      fetchApiData: jest.fn(),
    });

    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    expect(getByTestId('bulletintabs')).toBeTruthy();
    expect(getByTestId('bulletin-loading')).toBeTruthy();
    await waitFor(() => expect(queryByTestId('bulletinviewer')).toBeFalsy());
  });
  it('should show error if bulletin api fails', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: false,
      error: new Error('error'),
      result: null,
      fetchApiData: jest.fn(),
    });

    const { getByTestId, queryByTestId, getByText } = render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    expect(getByTestId('bulletintabs')).toBeTruthy();
    expect(getByTestId('bulletin-error')).toBeTruthy();
    expect(getByText('error')).toBeTruthy();
    await waitFor(() => expect(queryByTestId('bulletinviewer')).toBeFalsy());
  });
  it('should show info message if {} returned from BE for latest bulletin', async () => {
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: false,
      error: null,
      result: {},
      fetchApiData: jest.fn(),
    });

    const { getByTestId, queryByTestId, getByText } = render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    expect(getByTestId('bulletintabs')).toBeTruthy();
    expect(getByTestId('bulletin-no-avail-bulletins')).toBeTruthy();
    expect(
      getByText('There are no available bulletins from the last 24 hours'),
    ).toBeTruthy();
    await waitFor(() => expect(queryByTestId('bulletinviewer')).toBeFalsy());
  });

  it('should refetch the bulletin periodically if there are no bulletins for the last 24 hours (result from BE is {})', async () => {
    jest.useFakeTimers();

    const fetchNewData = jest.fn();
    jest.spyOn(utils, 'useApi').mockReturnValue({
      isLoading: false,
      error: null,
      result: {},
      fetchApiData: fetchNewData,
    });

    const { getByTestId } = render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(getByTestId('bulletintabs')).toBeTruthy();
      expect(getByTestId('bulletin-no-avail-bulletins')).toBeTruthy();
    });
    jest.runOnlyPendingTimers();

    // Test new data has been fetched
    await waitFor(() => expect(fetchNewData).toHaveBeenCalledTimes(1));

    jest.runOnlyPendingTimers();
    await waitFor(() => expect(fetchNewData).toHaveBeenCalledTimes(2));

    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('should refetch the bulletin periodically if there is a bulletin loaded', async () => {
    jest.useFakeTimers();

    const fetchNewData = jest.fn();
    jest.spyOn(utils, 'useApi').mockReturnValue({
      isLoading: false,
      error: null,
      result: fakeBulletins[0],
      fetchApiData: fetchNewData,
    });

    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(getByTestId('bulletintabs')).toBeTruthy();
      expect(queryByTestId('bulletinviewer')).toBeTruthy();
    });
    jest.runOnlyPendingTimers();

    // Test new data has been fetched
    await waitFor(() => {
      expect(fetchNewData).toHaveBeenCalledTimes(1);
    });

    jest.runOnlyPendingTimers();
    await waitFor(() => {
      expect(fetchNewData).toHaveBeenCalledTimes(2);
    });

    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('should refetch the bulletin periodically if there is an error', async () => {
    jest.useFakeTimers();

    const fetchNewData = jest.fn();
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: false,
      error: new Error('error'),
      result: null,
      fetchApiData: fetchNewData,
    });

    const { getByTestId } = render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    expect(getByTestId('bulletintabs')).toBeTruthy();
    expect(getByTestId('bulletin-error')).toBeTruthy();
    jest.runOnlyPendingTimers();

    // Test new data has been fetched
    await waitFor(() => expect(fetchNewData).toHaveBeenCalledTimes(1));

    jest.runOnlyPendingTimers();
    await waitFor(() => expect(fetchNewData).toHaveBeenCalledTimes(2));

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not call periodically if bulletin still loading', async () => {
    jest.useFakeTimers();

    const fetchNewData = jest.fn();
    jest.spyOn(utils, 'useApi').mockReturnValueOnce({
      isLoading: true,
      error: null,
      result: null,
      fetchApiData: fetchNewData,
    });

    const { getByTestId } = render(
      <TestWrapper>
        <Bulletin />
      </TestWrapper>,
    );

    expect(getByTestId('bulletintabs')).toBeTruthy();
    expect(getByTestId('bulletin-loading')).toBeTruthy();
    jest.runOnlyPendingTimers();

    // Test new data has not been fetched
    await waitFor(() => expect(fetchNewData).not.toHaveBeenCalled());

    jest.runOnlyPendingTimers();
    await waitFor(() => expect(fetchNewData).not.toHaveBeenCalled());

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
