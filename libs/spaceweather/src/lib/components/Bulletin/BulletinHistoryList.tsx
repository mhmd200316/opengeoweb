/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { List, ListItem, ListItemText, ListSubheader } from '@mui/material';
import moment from 'moment';
import { Bulletin } from '../../types';

interface BulletinHistoryListProps {
  selectedBulletin: string;
  bulletinHistoryList: Bulletin[];
  onSelectBulletin: (bulletin: string) => void;
}

export const BulletinHistoryList: React.FC<BulletinHistoryListProps> = ({
  selectedBulletin,
  bulletinHistoryList,
  onSelectBulletin,
}: BulletinHistoryListProps) => {
  return (
    <List
      sx={{
        height: 550,
        overflowY: 'scroll',
        paddingTop: '0px',
        '& .Mui-selected': {
          backgroundColor: 'rgba(0, 117, 169, 0.15) !important',
          borderLeft: '4px solid #0075a9',
        },
        '& .MuiListItem-button:hover': {
          backgroundColor: 'rgba(0, 117, 169, 0.15)',
        },
      }}
    >
      <ListSubheader
        sx={{
          fontSize: '12px',
          backgroundColor: '#ffffff',
        }}
      >
        Publication Date (UTC)
      </ListSubheader>
      {bulletinHistoryList.map((bulletin) => {
        return (
          <ListItem
            data-testid="historyItem"
            button
            key={bulletin.bulletin_id}
            selected={selectedBulletin === bulletin.bulletin_id}
            onClick={(): void => {
              onSelectBulletin(bulletin.bulletin_id);
            }}
          >
            <ListItemText
              primaryTypographyProps={{
                variant: 'body2',
              }}
              primary={moment
                .utc(bulletin.message.overview.timestamp)
                .format('YYYY-MM-DD HH:mm')}
            />
          </ListItem>
        );
      })}
    </List>
  );
};
