/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render } from '@testing-library/react';
import moment from 'moment';
import { BulletinViewer } from './BulletinViewer';
import { BulletinType } from '../../types';
import { mockBulletin } from '../../utils/fakedata';

describe('src/components/Bulletin/BulletinViewer', () => {
  it('should show the technical bulletin content and timestamp', () => {
    const props = {
      bulletinType: BulletinType.technical,
      bulletin: mockBulletin,
    };
    const { getByTestId } = render(<BulletinViewer {...props} />);

    const content = getByTestId('bulletinContent');
    expect(content).toBeTruthy();
    expect(content.textContent).toContain(
      mockBulletin.message.overview.content,
    );
    const expectedTime = moment
      .utc(mockBulletin.message.overview.timestamp)
      .format('YYYY-MM-DD HH:mm');
    expect(content.textContent).toContain(`Published: ${expectedTime}`);
  });

  it('should show the simple bulletin content', () => {
    const props = {
      bulletinType: BulletinType.plain,
      bulletin: mockBulletin,
    };
    const { getByTestId } = render(<BulletinViewer {...props} />);

    const content = getByTestId('bulletinContent');
    expect(content).toBeTruthy();
    expect(content.textContent).toContain(
      mockBulletin.message.overview.simplified_content,
    );
  });
});
