/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box, Grid, Skeleton } from '@mui/material';

import { AlertBanner } from '@opengeoweb/shared';
import { useApiContext, useApi } from '@opengeoweb/api';
import { BulletinType } from '../../types';
import { BulletinViewer } from './BulletinViewer';
import { ContentDialog } from '../ContentDialog';
import { BulletinHistoryList } from './BulletinHistoryList';
import { BulletinTabs } from './BulletinTabs';
import { SpaceWeatherApi } from '../../utils/api';

interface BulletinHistoryDialogProps {
  open: boolean;
  toggleStatus: () => void;
}

const BulletinHistoryDialog: React.FC<BulletinHistoryDialogProps> = ({
  open,
  toggleStatus,
}: BulletinHistoryDialogProps) => {
  const [bulletinType, setBulletinType] = React.useState(
    BulletinType.technical,
  );
  const [selectedBulletin, setSelectedBulletin] = React.useState('');
  const { api } = useApiContext<SpaceWeatherApi>();

  const {
    isLoading,
    error,
    result: bulletinHistoryList,
    fetchApiData,
  } = useApi(api.getBulletinHistory);

  React.useEffect(() => {
    if (
      bulletinHistoryList &&
      bulletinHistoryList.length &&
      !selectedBulletin
    ) {
      setSelectedBulletin(bulletinHistoryList[0].bulletin_id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [bulletinHistoryList]);

  React.useEffect(() => {
    if (open) {
      fetchApiData(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);

  return (
    <ContentDialog
      open={open}
      toggleStatus={toggleStatus}
      title="Forecast History"
      data-testid="history-dialog"
      fullWidth
      disableEscapeKeyDown
    >
      <Grid container>
        <Grid item xs={2}>
          {bulletinHistoryList && (
            <BulletinHistoryList
              selectedBulletin={selectedBulletin}
              bulletinHistoryList={bulletinHistoryList}
              onSelectBulletin={setSelectedBulletin}
            />
          )}
          {isLoading && (
            <Skeleton
              data-testid="history-dialog-loading"
              variant="rectangular"
              height={558}
              width="90%"
            />
          )}
          {error && (
            <AlertBanner
              severity="error"
              dataTestId="history-dialog-error"
              title={error.message ? error.message : ''}
            />
          )}
        </Grid>
        <Grid item xs={10}>
          <Grid container direction="column" alignItems="stretch" spacing={1}>
            <BulletinTabs
              selected={bulletinType}
              onSetBulletinType={setBulletinType}
            />
            <Grid item>
              <Box
                sx={{
                  padding: '10px 10px 0px 10px',
                }}
              >
                {bulletinHistoryList && selectedBulletin && (
                  <BulletinViewer
                    bulletinType={bulletinType}
                    smallContent={false}
                    bulletin={bulletinHistoryList.find(
                      (bulletin) => bulletin.bulletin_id === selectedBulletin,
                    )}
                  />
                )}
              </Box>
              {isLoading && (
                <Skeleton variant="rectangular" height={500} width="100%" />
              )}
              {error && (
                <AlertBanner
                  severity="error"
                  title={error.message ? error.message : ''}
                />
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </ContentDialog>
  );
};

export default BulletinHistoryDialog;
