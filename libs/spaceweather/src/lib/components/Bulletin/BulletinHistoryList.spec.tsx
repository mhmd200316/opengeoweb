/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { BulletinHistoryList } from './BulletinHistoryList';
import { mockBulletinHistory } from '../../utils/fakedata';

describe('src/components/Bulletin/BulletinHistoryList', () => {
  it('should show all passed bulletins in the list', () => {
    const props = {
      selectedBulletin: mockBulletinHistory.result[1].bulletin_id,
      onSelectBulletin: jest.fn(),
      bulletinHistoryList: mockBulletinHistory.result,
    };
    const { getAllByTestId } = render(<BulletinHistoryList {...props} />);
    expect(getAllByTestId('historyItem').length).toEqual(
      mockBulletinHistory.result.length,
    );
  });
  it('should show the selected bulletin as selected', () => {
    const props = {
      selectedBulletin: mockBulletinHistory.result[1].bulletin_id,
      onSelectBulletin: jest.fn(),
      bulletinHistoryList: mockBulletinHistory.result,
    };
    const { getAllByTestId } = render(<BulletinHistoryList {...props} />);
    const historyItems = getAllByTestId('historyItem');
    expect(historyItems[1].getAttribute('class')).toContain('Mui-selected');
  });
  it('should select a bulletin when clicked in the list', () => {
    const props = {
      selectedBulletin: mockBulletinHistory.result[1].bulletin_id,
      onSelectBulletin: jest.fn(),
      bulletinHistoryList: mockBulletinHistory.result,
    };
    const { getAllByTestId } = render(<BulletinHistoryList {...props} />);
    const historyItems = getAllByTestId('historyItem');

    fireEvent.click(historyItems[1]);
    expect(props.onSelectBulletin).toHaveBeenCalledTimes(1);
    expect(props.onSelectBulletin).toHaveBeenCalledWith(
      mockBulletinHistory.result[1].bulletin_id,
    );
  });
});
