/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';

import NotificationList from './NotificationList';
import {
  mockEvent,
  mockEventUnacknowledgedExternal,
  mockEventAcknowledgedExternal,
} from '../../utils/fakedata';

describe('src/components/NotificationList/NotificationList', () => {
  it('should call onChangeTabValue when tab gets changed', () => {
    const props = {
      tabValue: 'ALL',
      newNotifications: [],
      eventList: [
        mockEvent,
        mockEventAcknowledgedExternal,
        mockEventUnacknowledgedExternal,
      ],
      isLoading: null,
      error: null,
      onChangeTabValue: jest.fn(),
      handleNotificatioNRowClick: jest.fn(),
    };

    const { getByTestId } = render(<NotificationList {...props} />);

    const KNMITab = getByTestId('newNotificationTabKNMI');

    fireEvent.click(KNMITab);

    expect(props.onChangeTabValue).toHaveBeenCalledWith('KNMI');
  });

  it('should call onNotificationRowClick clicking on a row', () => {
    const props = {
      tabValue: 'ALL',
      newNotifications: [],
      eventList: [
        mockEvent,
        mockEventAcknowledgedExternal,
        mockEventUnacknowledgedExternal,
      ],
      isLoading: null,
      error: null,
      onChangeTabValue: jest.fn(),
      handleNotificatioNRowClick: jest.fn(),
    };
    const { getAllByRole } = render(<NotificationList {...props} />);

    fireEvent.click(getAllByRole('button')[1]);

    expect(props.handleNotificatioNRowClick).toHaveBeenCalledWith(
      mockEventAcknowledgedExternal,
    );
  });

  it('should show a skeleton if loading = true', () => {
    const props = {
      tabValue: 'ALL',
      newNotifications: [],
      eventList: null,
      isLoading: true,
      error: null,
      onChangeTabValue: jest.fn(),
      handleNotificatioNRowClick: jest.fn(),
    };

    const { queryAllByTestId, getByTestId } = render(
      <NotificationList {...props} />,
    );

    expect(getByTestId('notificationList-skeleton')).toBeTruthy();
    expect(queryAllByTestId('notificationRow-listitem')).toEqual([]);
  });

  it('should show an error message if error is set', () => {
    const props = {
      tabValue: 'ALL',
      newNotifications: [],
      eventList: null,
      isLoading: null,
      error: new Error('Mocking error message'),
      onChangeTabValue: jest.fn(),
      handleNotificatioNRowClick: jest.fn(),
    };

    const { queryAllByTestId, getByTestId } = render(
      <NotificationList {...props} />,
    );

    expect(getByTestId('notificationList-alert')).toBeTruthy();
    expect(queryAllByTestId('notificationRow-listitem')).toEqual([]);
  });

  it('should show each event in its own notification row', () => {
    const props = {
      tabValue: 'ALL',
      newNotifications: [],
      eventList: [
        mockEvent,
        mockEventAcknowledgedExternal,
        mockEventUnacknowledgedExternal,
      ],
      isLoading: null,
      error: null,
      onChangeTabValue: jest.fn(),
      handleNotificatioNRowClick: jest.fn(),
    };

    const { queryAllByTestId } = render(<NotificationList {...props} />);

    expect(queryAllByTestId('notificationRow-listitem').length).toEqual(
      props.eventList.length,
    );
  });
});
