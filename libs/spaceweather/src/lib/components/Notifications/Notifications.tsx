/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Grid, Typography, Button } from '@mui/material';

import makeStyles from '@mui/styles/makeStyles';

import { useApiContext, useApi } from '@opengeoweb/api';
import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import NotificationList from './NotificationList';
import LifeCycleDialog from '../LifeCycleDialog/LifeCycleDialog';
import { EventCategoryParams, SWEvent } from '../../types';
import { useNotificationTriggerContext } from '../NotificationTrigger';
import { SpaceWeatherApi } from '../../utils/api';

const useStyles = makeStyles({
  NotificationHeader: {
    textAlign: 'center',
  },
  ButtonRow: { marginTop: '8px', marginRight: '32px' },
});

/**
 * Notifications
 * View incoming notifications and issue new notifications following the notification lifecycle
 *
 * @example
 * ``` <Notifications /> ```
 */
const Notifications: React.FC = () => {
  const classes = useStyles();
  const { api } = useApiContext<SpaceWeatherApi>();

  const {
    notificationTriggers: newNotifications,
    onFetchNewNotificationTriggerData,
  } = useNotificationTriggerContext();

  const [dialogEvent, setDialogEvent] = React.useState(null);
  const [lifeCycleDialogMode, setLifeCycleDialogMode] = React.useState('new');
  const [lifeCycleDialogOpen, setLifeCycleDialogOpen] = React.useState(false);

  const [tabValue, setTabValue] = React.useState('ALL');

  // Retrieve eventList
  const params: EventCategoryParams =
    tabValue === 'KNMI' ? { originator: tabValue } : { category: tabValue };

  const {
    error: errorList,
    isLoading: isLoadingList,
    result: eventList,
    clearResults,
    fetchApiData: fetchNewEventList,
  } = useApi(api.getEventList, tabValue === 'ALL' ? {} : params);

  const getNewEventList = (): void => {
    const newparams =
      tabValue === 'KNMI' ? { originator: tabValue } : { category: tabValue };
    fetchNewEventList(tabValue === 'ALL' ? {} : newparams);
  };

  React.useEffect(() => {
    if (clearResults) clearResults();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tabValue]);

  React.useEffect(() => {
    getNewEventList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newNotifications]);

  const toggleDialogOpen = (): void => {
    // Refresh list on closing dialog
    if (lifeCycleDialogOpen === true) getNewEventList();
    setLifeCycleDialogOpen(!lifeCycleDialogOpen);
  };

  React.useEffect(() => {
    if (lifeCycleDialogMode !== 'new') {
      toggleDialogOpen();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lifeCycleDialogMode]);

  const toggleCreateNotification = (): void => {
    setLifeCycleDialogMode('new');
    toggleDialogOpen();
  };

  const handleNotificatioNRowClick = (event: SWEvent): void => {
    setDialogEvent(event);
    if (lifeCycleDialogMode !== event.eventid) {
      setLifeCycleDialogMode(event.eventid);
      if (event.notacknowledged === true) {
        api
          .setAcknowledged(event.eventid)
          .then(() => {
            onFetchNewNotificationTriggerData();
          })
          // eslint-disable-next-line no-console
          .catch((e) => console.warn(e));
      }
    } else {
      toggleDialogOpen();
    }
  };

  return (
    <ConfirmationServiceProvider>
      <Grid container spacing={1} style={{ height: '100%' }}>
        <Grid item xs={12}>
          <Typography
            variant="subtitle1"
            className={classes.NotificationHeader}
          >
            Notifications
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <NotificationList
            tabValue={tabValue}
            handleNotificatioNRowClick={handleNotificatioNRowClick}
            onChangeTabValue={setTabValue}
            newNotifications={newNotifications}
            eventList={eventList || null}
            isLoading={isLoadingList}
            error={errorList}
          />
        </Grid>
        <Grid item xs={12}>
          <Grid container justifyContent="flex-end">
            <Grid item className={classes.ButtonRow}>
              <Button
                size="small"
                variant="outlined"
                color="secondary"
                onClick={toggleCreateNotification}
                data-testid="notifications-newnotification"
              >
                Issue a KNMI notification
              </Button>
            </Grid>
          </Grid>
        </Grid>
        {lifeCycleDialogOpen && (
          <LifeCycleDialog
            open={lifeCycleDialogOpen}
            toggleStatus={toggleDialogOpen}
            dialogMode={lifeCycleDialogMode}
            {...(lifeCycleDialogMode !== 'new' &&
              dialogEvent !== null && { event: dialogEvent })}
          />
        )}
      </Grid>
    </ConfirmationServiceProvider>
  );
};

export default Notifications;
