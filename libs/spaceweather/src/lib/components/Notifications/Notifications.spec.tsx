/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import { ApiProvider } from '@opengeoweb/api';

import Notifications from './Notifications';
import { TestWrapper } from '../../utils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { fakeEventList } from '../../utils/fakedata';

describe('src/components/Notifications/Notifications', () => {
  it('should retrieve the eventlist on load', async () => {
    const { queryAllByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <Notifications />
      </ApiProvider>,
    );

    await waitFor(() =>
      expect(queryAllByTestId('notificationRow-listitem').length).toEqual(
        fakeEventList.length,
      ),
    );
  });

  it('should set the selected tab when a new tab header is clicked and load the right list', async () => {
    const { queryAllByTestId, getByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <Notifications />
      </ApiProvider>,
    );

    // Should first load the whole list
    await waitFor(() =>
      expect(queryAllByTestId('notificationRow-listitem').length).toEqual(
        fakeEventList.length,
      ),
    );

    const KNMITab = getByTestId('newNotificationTabKNMI');
    fireEvent.click(KNMITab);

    // Should only load events with originator KNMI and have KNMI tab selected
    await waitFor(() => {
      expect(getByTestId('newNotificationTabKNMI').classList).toContain(
        'Mui-selected',
      );
      expect(queryAllByTestId('notificationRow-listitem').length).toEqual(6);
    });

    const ALLTab = getByTestId('newNotificationTabAll');
    fireEvent.click(ALLTab);

    // Should load all events and have all tab selected
    await waitFor(() => {
      expect(getByTestId('newNotificationTabAll').classList).toContain(
        'Mui-selected',
      );
    });

    await waitFor(() => {
      expect(queryAllByTestId('notificationRow-listitem').length).toEqual(
        fakeEventList.length,
      );
    });
  });

  it('should open dialog when eventrow is clicked', async () => {
    const { queryAllByTestId, queryByTestId, getAllByTestId } = render(
      <ApiProvider createApi={createFakeApi}>
        <Notifications />
      </ApiProvider>,
    );
    expect(queryByTestId('lifecycle-dialog')).toBeFalsy();
    await waitFor(() =>
      expect(queryAllByTestId('notificationRow-listitem').length).toEqual(
        fakeEventList.length,
      ),
    );
    fireEvent.click(getAllByTestId('notificationRow-listitem')[1]);
    expect(queryByTestId('lifecycle-dialog')).toBeTruthy();
  });

  it('should open dialog when clicking the new notification button', async () => {
    const { queryByTestId, getByTestId } = render(
      <TestWrapper>
        <Notifications />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('notifications-newnotification'));
    await waitFor(() => expect(queryByTestId('lifecycle-dialog')).toBeTruthy());
  });
});
