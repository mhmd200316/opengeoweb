/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  Grid,
  Typography,
  Card,
  CardContent,
  Button,
  ListItem,
  Hidden,
} from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { Edit, Notifications } from '@opengeoweb/theme';
import moment from 'moment';

import { StatusTag } from '@opengeoweb/shared';
import { getStatusTagColorFromContent } from './utils';
import { EventCategory, SWEvent } from '../../types';

const useStyles = makeStyles({
  cardWrapper: {
    width: '100%',
  },
  cardContent: {
    padding: 14,
    '&:last-child': {
      paddingBottom: 14,
    },
  },
  notificationListItem: {
    padding: '2px',
  },
});

const getStatusTagContent = (lifeCycle, categorydetail = ''): string => {
  if (lifeCycle.label === 'WARNING') {
    if (lifeCycle.state === 'issued') {
      return categorydetail === 'GEOMAGNETIC_STORM' ? 'Watch' : 'Warning';
    }
    return lifeCycle.state === 'expired' ? 'Expired' : 'Cancelled';
  }

  return lifeCycle.state === 'issued' ? 'Alert' : 'Summary';
};

interface NotificationRowProps {
  event: SWEvent;
  onNotificationRowClick: (event: SWEvent) => void;
}

const NotificationRow: React.FC<NotificationRowProps> = ({
  event,
  onNotificationRowClick,
}: NotificationRowProps) => {
  const classes = useStyles();

  const externalProviderLifeCycle =
    event.originator !== 'KNMI'
      ? event.lifecycles.externalprovider
      : {
          firstissuetime: '',
          lastissuetime: '',
          state: '',
          label: '',
          eventlevel: '',
        };

  const internalProviderLifeCycle =
    event.lifecycles.internalprovider !== undefined
      ? event.lifecycles.internalprovider
      : {
          lastissuetime: '',
          state: '',
          draft: false,
          label: '',
          eventlevel: '',
        };

  const internalTagContent = getStatusTagContent(
    internalProviderLifeCycle,
    event.categorydetail,
  );
  const externalTagContent = getStatusTagContent(
    externalProviderLifeCycle,
    event.categorydetail,
  );

  const eventlevel =
    event.originator !== 'KNMI'
      ? externalProviderLifeCycle.eventlevel
      : internalProviderLifeCycle.eventlevel;

  return (
    <ListItem
      button
      onClick={(): void => {
        onNotificationRowClick(event);
      }}
      key={event.eventid}
      className={classes.notificationListItem}
      data-testid="notificationRow-listitem"
    >
      <Card elevation={0} variant="outlined" className={classes.cardWrapper}>
        <CardContent className={classes.cardContent}>
          <Grid container alignItems="center" spacing={1}>
            <Grid item xs={12} lg={3}>
              <Typography variant="subtitle1">
                {`${EventCategory[event.category]}${
                  eventlevel ? ` - ${eventlevel}` : ''
                }`}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={12} lg={9}>
              <Grid container spacing={1} alignItems="center">
                <Grid item xs={12} sm={12} lg={6}>
                  {event.originator !== 'KNMI' ? (
                    <Grid
                      container
                      alignItems="center"
                      spacing={1}
                      data-testid="notificationRow-externalCycle"
                    >
                      <Grid item xs={7}>
                        <Typography variant="body2">
                          {moment
                            .utc(externalProviderLifeCycle.lastissuetime)
                            .format('YYYY-MM-DD HH:mm')
                            .concat(' UTC')}
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        xs
                        data-testid="notificationRow-externalStatusTag"
                      >
                        <StatusTag
                          content={externalTagContent}
                          color={getStatusTagColorFromContent(
                            externalTagContent,
                          )}
                        />
                      </Grid>
                      {/* Show "new notification" bell icon here for all screensizes but lg */}
                      {event.notacknowledged === true && (
                        <Hidden lgUp>
                          <Grid item xs>
                            <Notifications
                              color="action"
                              data-testid="newNotificationRowIcon-small"
                            />
                          </Grid>
                        </Hidden>
                      )}
                    </Grid>
                  ) : null}
                </Grid>
                <Grid item xs={12} sm={12} lg={6}>
                  <Grid container alignItems="center" spacing={1}>
                    {/* For lg show "new notification" bell icon here */}
                    {event.notacknowledged === true && (
                      <Hidden lgDown>
                        <Grid item xs>
                          <Notifications
                            color="action"
                            data-testid="newNotificationRowIcon-large"
                          />
                        </Grid>
                      </Hidden>
                    )}
                    <Grid item xs={7}>
                      <Typography
                        variant="body2"
                        data-testid="notificationRow-internalLastIssueTime"
                      >
                        {event.notacknowledged !== true &&
                          internalProviderLifeCycle.lastissuetime &&
                          moment
                            .utc(internalProviderLifeCycle.lastissuetime)
                            .format('YYYY-MM-DD HH:mm')
                            .concat(' UTC')}
                      </Typography>
                    </Grid>
                    {internalProviderLifeCycle.draft !== undefined &&
                    internalProviderLifeCycle.draft === true ? (
                      <Grid item xs>
                        <Grid container alignItems="center">
                          <Button
                            color="secondary"
                            size="small"
                            data-testid="notificationRow-draft"
                            startIcon={<Edit color="secondary" />}
                          >
                            Edit
                          </Button>
                        </Grid>
                      </Grid>
                    ) : (
                      <Grid
                        item
                        xs
                        data-testid="notificationRow-internalStatusTag"
                      >
                        {event.notacknowledged !== true &&
                        internalProviderLifeCycle.lastissuetime ? (
                          <StatusTag
                            content={internalTagContent}
                            color={getStatusTagColorFromContent(
                              internalTagContent,
                            )}
                          />
                        ) : null}
                      </Grid>
                    )}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </ListItem>
  );
};

export default NotificationRow;
