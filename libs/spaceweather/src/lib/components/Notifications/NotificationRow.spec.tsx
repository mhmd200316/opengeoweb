/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import moment from 'moment';

import { render, fireEvent } from '@testing-library/react';
import NotificationRow from './NotificationRow';
import {
  mockEventUnacknowledgedExternal,
  mockEventAcknowledgedExternal,
  mockEventAcknowledgedExternalDraft,
} from '../../utils/fakedata';
import { EventCategory } from '../../types';
import { SizeWrapper } from '../../utils/testUtils';

describe('src/components/NotificationRow/NotificationRow', () => {
  it('should call onNotificationRowClick clicking on a row', () => {
    const props = {
      event: mockEventUnacknowledgedExternal,
      onNotificationRowClick: jest.fn(),
    };
    const { getByRole } = render(<NotificationRow {...props} />);

    fireEvent.click(getByRole('button'));

    expect(props.onNotificationRowClick).toHaveBeenCalledWith(
      mockEventUnacknowledgedExternal,
    );
  });

  it('should show unacknowledged external event correctly (on lg size screens)', () => {
    const props = {
      event: mockEventUnacknowledgedExternal,
      onNotificationRowClick: jest.fn(),
    };
    const { getByTestId, getByText, queryByTestId } = render(
      <SizeWrapper>
        <NotificationRow {...props} />
      </SizeWrapper>,
    );

    // Show right event category and level
    expect(
      getByText(
        `${EventCategory[mockEventUnacknowledgedExternal.category]} - ${
          mockEventUnacknowledgedExternal.lifecycles.externalprovider.eventlevel
        }`,
      ),
    ).toBeTruthy();

    // external event - should show last issue time
    expect(queryByTestId('notificationRow-externalCycle')).toBeTruthy();
    expect(
      getByText(
        moment
          .utc(
            mockEventUnacknowledgedExternal.lifecycles.externalprovider
              .lastissuetime,
          )
          .format('YYYY-MM-DD HH:mm')
          .concat(' UTC'),
      ),
    ).toBeTruthy();

    // Show the right tag
    expect(
      getByTestId('notificationRow-externalStatusTag').textContent,
    ).toEqual('Alert');

    // Show the new notification icon (lg size screen so large should show)
    expect(getByTestId('newNotificationRowIcon-large')).toBeTruthy();
    expect(queryByTestId('newNotificationRowIcon-small')).toBeFalsy();

    // internal last issue time is empty
    expect(
      getByTestId('notificationRow-internalLastIssueTime').textContent,
    ).toEqual('');

    // no edit draft button to be present
    expect(queryByTestId('notificationRow-draft')).toBeFalsy();

    // internal notification tag should not be present
    expect(
      getByTestId('notificationRow-internalStatusTag').textContent,
    ).toEqual('');
  });

  it('should show unacknowledged external event correctly on >lg size screen', () => {
    const props = {
      event: mockEventUnacknowledgedExternal,
      onNotificationRowClick: jest.fn(),
    };
    const { getByTestId, queryByTestId } = render(
      <SizeWrapper width="xl">
        <NotificationRow {...props} />
      </SizeWrapper>,
    );

    // Show the new notification icon (xl size screen so large should show)
    expect(getByTestId('newNotificationRowIcon-large')).toBeTruthy();
    expect(queryByTestId('newNotificationRowIcon-small')).toBeFalsy();
  });

  it('should show unacknowledged external event correctly on md size screen', () => {
    const props = {
      event: mockEventUnacknowledgedExternal,
      onNotificationRowClick: jest.fn(),
    };
    const { getByTestId, queryByTestId } = render(
      <SizeWrapper width="md">
        <NotificationRow {...props} />
      </SizeWrapper>,
    );

    // Show the new notification icon (md size screen so small should show)
    expect(getByTestId('newNotificationRowIcon-small')).toBeTruthy();
    expect(queryByTestId('newNotificationRowIcon-large')).toBeFalsy();
  });

  it('should show acknowledged external event with internal issued notifications correctly', () => {
    const props = {
      event: mockEventAcknowledgedExternal,
      onNotificationRowClick: jest.fn(),
    };
    const { getByTestId, getByText, queryByTestId } = render(
      <NotificationRow {...props} />,
    );

    // Show right event category and level
    expect(
      getByText(
        `${EventCategory[mockEventAcknowledgedExternal.category]} - ${
          mockEventAcknowledgedExternal.lifecycles.externalprovider.eventlevel
        }`,
      ),
    ).toBeTruthy();

    // external event - should show last issue time
    expect(queryByTestId('notificationRow-externalCycle')).toBeTruthy();
    expect(
      getByText(
        moment
          .utc(
            mockEventAcknowledgedExternal.lifecycles.externalprovider
              .lastissuetime,
          )
          .format('YYYY-MM-DD HH:mm')
          .concat(' UTC'),
      ),
    ).toBeTruthy();

    // Show the right tag
    expect(
      getByTestId('notificationRow-externalStatusTag').textContent,
    ).toEqual('Alert');

    // Not show the new notification icon
    expect(queryByTestId('newNotificationRowIcon')).toBeFalsy();

    // internal last issue time
    expect(
      getByTestId('notificationRow-internalLastIssueTime').textContent,
    ).toEqual(
      moment
        .utc(
          mockEventAcknowledgedExternal.lifecycles.internalprovider
            .lastissuetime,
        )
        .format('YYYY-MM-DD HH:mm')
        .concat(' UTC'),
    );

    // no edit draft button to be present
    expect(queryByTestId('notificationRow-draft')).toBeFalsy();

    // internal notification tag should be present
    expect(
      getByTestId('notificationRow-internalStatusTag').textContent,
    ).toEqual('Alert');
  });

  it('should show draft notification correctly', () => {
    const props = {
      event: mockEventAcknowledgedExternalDraft,
      onNotificationRowClick: jest.fn(),
    };
    const { getByTestId, getByText, queryByTestId } = render(
      <NotificationRow {...props} />,
    );

    // Show right event category and no level
    expect(
      getByText(
        `${EventCategory[mockEventAcknowledgedExternalDraft.category]}`,
      ),
    ).toBeTruthy();

    // external event - should show last issue time
    expect(queryByTestId('notificationRow-externalCycle')).toBeTruthy();
    expect(
      getByText(
        moment
          .utc(
            mockEventAcknowledgedExternalDraft.lifecycles.externalprovider
              .lastissuetime,
          )
          .format('YYYY-MM-DD HH:mm')
          .concat(' UTC'),
      ),
    ).toBeTruthy();

    // Show the right tag
    expect(
      getByTestId('notificationRow-externalStatusTag').textContent,
    ).toEqual('Alert');

    // Not show the new notification icon
    expect(queryByTestId('newNotificationRowIcon')).toBeFalsy();

    // internal last issue time
    expect(
      getByTestId('notificationRow-internalLastIssueTime').textContent,
    ).toEqual(
      moment
        .utc(
          mockEventAcknowledgedExternalDraft.lifecycles.internalprovider
            .lastissuetime,
        )
        .format('YYYY-MM-DD HH:mm')
        .concat(' UTC'),
    );

    // no edit draft button to be present
    expect(queryByTestId('notificationRow-draft')).toBeTruthy();

    // internal notification tag should be present
    expect(queryByTestId('notificationRow-internalStatusTag')).toBeFalsy();
  });
});
