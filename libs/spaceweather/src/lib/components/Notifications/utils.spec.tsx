/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { getStatusTagColorFromContent, getStatusTagContent } from './utils';

describe('src/components/Notifications/utils/getStatusTagContent', () => {
  it('should return the appropriate label for the Tag', () => {
    expect(getStatusTagContent('ALERT', 'GEOMAGNETIC_STORM')).toEqual('Alert');
    expect(getStatusTagContent('ALERT', '', 'issued')).toEqual('Alert');
    expect(getStatusTagContent('ALERT', '', 'ended')).toEqual('Summary');
    expect(getStatusTagContent('ALERT', 'GEOMAGNETIC_STORM', 'issued')).toEqual(
      'Alert',
    );
    expect(getStatusTagContent('ALERT', 'GEOMAGNETIC_STORM', 'ended')).toEqual(
      'Summary',
    );
    expect(getStatusTagContent('WARNING', '', 'issued')).toEqual('Warning');
    expect(getStatusTagContent('WARNING', '')).toEqual('Warning');
    expect(getStatusTagContent('WARNING', '', 'ended')).toEqual('Cancelled');
    expect(getStatusTagContent('WARNING', '', 'expired')).toEqual('Expired');
    expect(getStatusTagContent('WARNING', 'PROTON_FLUX_10', 'issued')).toEqual(
      'Warning',
    );
    expect(getStatusTagContent('WARNING', 'PROTON_FLUX_10')).toEqual('Warning');
    expect(getStatusTagContent('WARNING', 'PROTON_FLUX_10', 'ended')).toEqual(
      'Cancelled',
    );
    expect(getStatusTagContent('WARNING', 'PROTON_FLUX_10', 'expired')).toEqual(
      'Expired',
    );
    // For issued geomagnetic storm the label should contain Watch
    expect(
      getStatusTagContent('WARNING', 'GEOMAGNETIC_STORM', 'issued'),
    ).toEqual('Watch');
    expect(getStatusTagContent('WARNING', 'GEOMAGNETIC_STORM')).toEqual(
      'Watch',
    );
    expect(
      getStatusTagContent('WARNING', 'GEOMAGNETIC_STORM', 'ended'),
    ).toEqual('Cancelled');
    expect(
      getStatusTagContent('WARNING', 'GEOMAGNETIC_STORM', 'expired'),
    ).toEqual('Expired');
  });
});

describe('src/components/Notifications/utils/getStatusTagColorFromContent', () => {
  it('should return the appropriate color for the tag content', () => {
    expect(getStatusTagColorFromContent('Alert')).toEqual('red');
    expect(getStatusTagColorFromContent('Summary')).toEqual('purple');
    expect(getStatusTagColorFromContent('Warning')).toEqual('yellow');
    expect(getStatusTagColorFromContent('Cancelled')).toEqual('grey');
    expect(getStatusTagColorFromContent('Expired')).toEqual('grey');
    expect(getStatusTagColorFromContent('Watch')).toEqual('yellow');
  });
});
