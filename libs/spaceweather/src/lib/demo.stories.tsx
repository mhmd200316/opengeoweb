/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box } from '@mui/material';
import {
  ErrorBoundary,
  ToolContainerDraggable,
  calculateDialogSizeAndPosition,
} from '@opengeoweb/shared';

import { LayoutTitle } from './components/Layout';
import Layout from './components/Layout/Layout';
import { Bulletin } from './components/Bulletin';
import { Notifications } from './components/Notifications';
import {
  NotificationTrigger,
  NotificationTriggerProvider,
} from './components/NotificationTrigger';
import TimeSeries from './components/TimeSeries/TimeSeries';
import {
  StoryWrapper,
  StoryWrapperWithErrorOnSave,
} from './utils/storybookUtils';

export default { title: 'demo' };

const { width, height, top, left } = calculateDialogSizeAndPosition();

const SpaceWeatherDemoContent: React.FC = () => {
  return (
    <NotificationTriggerProvider>
      <Box
        sx={{
          maxHeight: '95vh',
          display: 'flex',
          flexDirection: 'column',
          overflowY: 'hidden',
        }}
      >
        <LayoutTitle />
        <NotificationTrigger />
        <Box sx={{ overflowY: 'scroll', paddingBottom: '10px' }}>
          <Layout
            leftTop={<Bulletin />}
            leftBottom={<Notifications />}
            right={<TimeSeries />}
          />
        </Box>
      </Box>
    </NotificationTriggerProvider>
  );
};

export const SpaceWeatherDemo = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <SpaceWeatherDemoContent />
    </StoryWrapper>
  );
};

const SpaceWeatherDialogDemoContent = (): React.ReactElement => {
  return (
    <ToolContainerDraggable
      onClose={(): void => null}
      title="Space Weather Forecast"
      startPosition={{ top, left }}
      startSize={{ width, height }}
      minWidth={1150}
      minHeight={300}
      data-testid="SpaceWeatherModule"
    >
      <NotificationTriggerProvider>
        <Layout
          leftTop={<Bulletin />}
          leftBottom={<Notifications />}
          right={<TimeSeries />}
        />
      </NotificationTriggerProvider>
    </ToolContainerDraggable>
  );
};

export const SpaceWeatherDialogDemo = (): React.ReactElement => {
  return (
    <StoryWrapper>
      <SpaceWeatherDialogDemoContent />
    </StoryWrapper>
  );
};

const SpaceWeatherWithHighLevelErrorBoundaryDemoContent: React.FC = () => {
  return (
    <NotificationTriggerProvider>
      <ToolContainerDraggable
        onClose={(): void => null}
        title="Space Weather Forecast"
        startPosition={{ top, left }}
        startSize={{ width, height }}
        minWidth={1150}
        minHeight={300}
        data-testid="SpaceWeatherModule"
      >
        <NotificationTrigger />
        <ErrorBoundary>
          <Layout
            leftTop={<Bulletin />}
            leftBottom={<Notifications />}
            right={<TimeSeries />}
          />
        </ErrorBoundary>
      </ToolContainerDraggable>
    </NotificationTriggerProvider>
  );
};

export const SpaceWeatherWithHighLevelErrorBoundaryDemo =
  (): React.ReactElement => {
    return (
      <StoryWrapperWithErrorOnSave>
        <SpaceWeatherWithHighLevelErrorBoundaryDemoContent />
      </StoryWrapperWithErrorOnSave>
    );
  };

const SpaceWeatherWithLowLevelErrorBoundaryDemoContent: React.FC = () => {
  return (
    <NotificationTriggerProvider>
      <ToolContainerDraggable
        onClose={(): void => null}
        title="Space Weather Forecast"
        startPosition={{ top, left }}
        startSize={{ width, height }}
        minWidth={1150}
        minHeight={300}
        data-testid="SpaceWeatherModule"
      >
        <NotificationTrigger />
        <ErrorBoundary>
          <Layout
            leftTop={<Bulletin />}
            leftBottom={<Notifications />}
            right={
              <ErrorBoundary>
                <TimeSeries />
              </ErrorBoundary>
            }
          />
        </ErrorBoundary>
      </ToolContainerDraggable>
    </NotificationTriggerProvider>
  );
};

export const SpaceWeatherWithLowLevelErrorBoundaryDemo =
  (): React.ReactElement => {
    return (
      <StoryWrapperWithErrorOnSave>
        <SpaceWeatherWithLowLevelErrorBoundaryDemoContent />
      </StoryWrapperWithErrorOnSave>
    );
  };
