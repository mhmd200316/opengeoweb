/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import {
  kpIndexDummyData,
  kpIndexForecastDummyData,
  interplanetaryMagneticFieldBt,
  solarWindSpeedDummyData,
  solarWindPressureDummyData,
  solarWindDensityDummyData,
  xRayFluxDummyData,
} from '../DummyData';
import {
  StreamResponse,
  SWNotification,
  TimeseriesParams,
  TimeseriesResponseData,
} from '../../types';
import { SpaceWeatherApi } from '../api';
import { createApi as createFakeApi } from '../fakeApi';

export const cancelRequestById = jest.fn();
export const createCancelRequestId = jest.fn();

export const api = {
  getStreams: (): Promise<{ data: StreamResponse[] }> => {
    return new Promise((resolve) => {
      resolve({
        data: [
          {
            stream: 'xray',
            start: '1970-01-01T00:00:00Z',
            end: '2021-01-01T00:00:00Z',
            params: [
              { name: 'solar_x_ray_flux_long', unit: 'W/m2' },
              { name: 'solar_x_ray_flux_short', unit: 'W/m2' },
            ],
            sources: ['GOES-16'],
            hasactive: false,
          },
        ],
      });
    });
  },
  issueNotification: (formData: SWNotification): Promise<void> => {
    if (formData.draft) {
      return new Promise((resolve) => {
        resolve();
      });
    }
    return Promise.reject(
      new Error('Mocking a failure for issue notification'),
    );
  },
  discardDraftNotification: (): Promise<void> => {
    return new Promise((resolve) => {
      resolve();
    });
  },
  getTimeSeriesMultiple: (
    params: TimeseriesParams[],
  ): Promise<{ data: TimeseriesResponseData }[]> => {
    const bundeledSeries = params.map((param) => {
      const getDummyTimeSerie = (
        stream: string,
        parameter: string,
      ): TimeseriesResponseData => {
        switch (stream) {
          case 'kp':
            return kpIndexDummyData;
          case 'kpforecast':
            return kpIndexForecastDummyData;
          case 'rtsw_mag':
            return parameter === 'bt'
              ? interplanetaryMagneticFieldBt
              : interplanetaryMagneticFieldBt;
          case 'rtsw_wind': {
            if (param.parameter === 'proton_speed') {
              return solarWindSpeedDummyData;
            }
            if (param.parameter === 'proton_pressure') {
              return solarWindPressureDummyData;
            }
            if (param.parameter === 'proton_density') {
              return solarWindDensityDummyData;
            }
            return solarWindDensityDummyData;
          }
          case 'xray':
            return xRayFluxDummyData;
          default:
            return kpIndexDummyData;
        }
      };

      return {
        data: getDummyTimeSerie(param.stream, param.parameter),
      };
    });
    return Promise.all(bundeledSeries);
  },
  getRePopulateTemplateContent: (): Promise<{
    data: { title: string; message: string };
  }> => {
    return new Promise((resolve) => {
      resolve({
        data: {
          title: 'Template title for you',
          message: 'Template message text for you',
        },
      });
    });
  },
};

export const createApi = (): SpaceWeatherApi => ({
  ...createFakeApi(),
  ...api,
});
