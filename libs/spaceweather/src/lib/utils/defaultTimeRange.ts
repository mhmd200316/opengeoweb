/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { TimeRange } from 'pondjs';
import moment, { Moment } from 'moment';

export const getBeginTime = (): Moment => moment.utc().subtract(3, 'days');
export const getEndTime = (): Moment => moment.utc().add(1, 'days');

export const beginTime = getBeginTime();
export const endTime = getEndTime();

export const defaultTimeRange = new TimeRange(beginTime, endTime);

export const getDefaultTimeRange = (): TimeRange =>
  new TimeRange(getBeginTime(), getEndTime());

export const timeRangeToUTC = (timeRange: TimeRange): [string, string] => {
  const [begin, end] = timeRange.toJSON();
  return [moment.utc(begin).format(), moment.utc(end).format()];
};
