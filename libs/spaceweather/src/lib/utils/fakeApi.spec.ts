/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import {
  createApi as createFakeApi,
  fetchBulletin,
  fetchEvent,
  fetchEventList,
  fetchFakeNewNotifications,
  getDummyTimeSerie,
} from './fakeApi';
import { fakeBulletins, fakeEventList } from './fakedata';
import * as apiFuncs from './api';
import { Streams, Parameters } from '../types';
import {
  interplanetaryMagneticFieldBt,
  interplanetaryMagneticFieldBz,
  kpIndexDummyData,
  kpIndexForecastDummyData,
  solarWindDensityDummyData,
  solarWindPressureDummyData,
  solarWindSpeedDummyData,
  xRayFluxDummyData,
} from './DummyData';

describe('src/utils/fakeApi', () => {
  describe('fetchBulletin', () => {
    it('should return the first bulletin of the list if no id passed in', () => {
      expect(fetchBulletin()).toEqual(fakeBulletins[0]);
    });
    it('should return the correct bulletin from the list if id passed in', () => {
      expect(fetchBulletin('5f08c0deff58c0d68c0d8c0dc2')).toEqual(
        fakeBulletins[2],
      );
    });
  });
  describe('fetchEventList', () => {
    it('should return the whole list if no params passed in', () => {
      expect(fetchEventList()).toEqual(fakeEventList);
    });
    it('should return all items in the list for that category if category passed in', () => {
      expect(fetchEventList({ category: 'PROTON_FLUX' })).toEqual([
        fakeEventList[0],
        fakeEventList[2],
        fakeEventList[10],
        fakeEventList[13],
        fakeEventList[15],
        fakeEventList[16],
      ]);
      expect(fetchEventList({ category: 'GEOMAGNETIC' })).toEqual([
        fakeEventList[1],
        fakeEventList[8],
        fakeEventList[11],
        fakeEventList[12],
        fakeEventList[14],
      ]);
      // Even if originator passed - only filter on category
      expect(
        fetchEventList({ category: 'GEOMAGNETIC', originator: 'KNMI' }),
      ).toEqual([
        fakeEventList[1],
        fakeEventList[8],
        fakeEventList[11],
        fakeEventList[12],
        fakeEventList[14],
      ]);
    });
    it('should return all items for originator KNMI if that originator passed in', () => {
      expect(fetchEventList({ originator: 'KNMI' })).toEqual([
        fakeEventList[5],
        fakeEventList[7],
        fakeEventList[12],
        fakeEventList[15],
        fakeEventList[16],
        fakeEventList[17],
      ]);
    });
    it('should ignore any other originators besides KNMI', () => {
      expect(fetchEventList({ originator: 'METOFFICE' })).toEqual(
        fakeEventList,
      );
    });
  });
  describe('fetchEvent', () => {
    it('should return the corresponding event from the eventlist', () => {
      expect(fetchEvent('METRB1123454')).toEqual(fakeEventList[0]);
      expect(fetchEvent('METRB1')).toEqual(fakeEventList[3]);
      expect(fetchEvent('KNMIRB1')).toEqual(fakeEventList[5]);
    });
  });
  describe('fetchFakeNewNotifications', () => {
    it('should return all notifications that have not been acknowledged yet', () => {
      expect(fetchFakeNewNotifications()).toEqual([
        fakeEventList[1],
        fakeEventList[4],
      ]);
    });
  });
  describe('createCancelRequestId', () => {
    it('should return correctly formatted request id', async () => {
      const charts = {
        stream: 'test-s-1' as Streams,
        parameter: 'test-p-1' as Parameters,
      };
      expect(apiFuncs.createCancelRequestId(charts)).toEqual(
        '-test-s-1-test-p-1',
      );
    });
  });
  describe('createFakeApi', () => {
    it('should contain all api calls', async () => {
      const api = createFakeApi();
      expect(api.getTimeSeriesMultiple).toBeTruthy();
      expect(api.getBulletin).toBeTruthy();
      expect(api.getBulletinHistory).toBeTruthy();
      expect(api.getEventList).toBeTruthy();
      expect(api.getEvent).toBeTruthy();
      expect(api.getNewNotifications).toBeTruthy();
      expect(api.issueNotification).toBeTruthy();
      expect(api.discardDraftNotification).toBeTruthy();
      expect(api.setAcknowledged).toBeTruthy();
      expect(api.getRePopulateTemplateContent).toBeTruthy();
    });

    it('should getTimeSeries', () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getTimeSeries');

      expect(spy).not.toHaveBeenCalled();

      const params = {
        parameter: 'bz_gsm' as const,
        stream: 'rtsw_mag' as const,
        time_start: '2022-09-02T14:16:04Z',
        time_stop: '2022-09-06T14:16:04Z',
      };
      const requestId = 'dummy-requestId';
      api.getTimeSeries(params, requestId);
      expect(spy).toHaveBeenCalledWith(params, requestId);
    });

    it('should getTimeSeriesMultiple', () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getTimeSeriesMultiple');

      expect(spy).not.toHaveBeenCalled();

      const params = [
        {
          parameter: 'bz_gsm' as const,
          stream: 'rtsw_mag' as const,
          time_start: '2022-09-02T14:16:04Z',
          time_stop: '2022-09-06T14:16:04Z',
        },
      ];
      api.getTimeSeriesMultiple(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should getStreams', () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getStreams');

      expect(spy).not.toHaveBeenCalled();

      const params = { stream: 'rtsw_mag' as const };
      api.getStreams(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should getBulletin', () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getBulletin');

      expect(spy).not.toHaveBeenCalled();

      const params = 'bulletin-id';
      api.getBulletin(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should getBulletinHistory', () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getBulletinHistory');

      expect(spy).not.toHaveBeenCalled();

      api.getBulletinHistory();
      expect(spy).toHaveBeenCalled();
    });

    it('should getEventList', () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getEventList');

      expect(spy).not.toHaveBeenCalled();

      const params = { category: 'rtsw_mag' as const };
      api.getEventList(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should getEvent', () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getEvent');

      expect(spy).not.toHaveBeenCalled();

      const params = 'event-id';
      api.getEvent(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should issueNotification', () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'issueNotification');

      expect(spy).not.toHaveBeenCalled();

      const params = {
        IS_POPULATE_REQUEST: false,
        category: 'ELECTRON_FLUX',
        categorydetail: 'ELECTRON_FLUX_2',
        label: 'ALERT',
        datasource: 'GOES16',
        threshold: 1000,
        thresholdunit: 'particles cm^-2 s^-1 sr^-1',
        neweventstart: '2022-09-05T11:20:00Z',
        message:
          'Vanaf 2022-09-05 11:20:00 UTC (2022-09-05 13:20:00 lokale tijd) is de flux van energetische elektronen (>2 MeV), gemeten door de GOES16 satelliet, boven de drempelwaarde van 1000.0 pfu uitgekomen. Hierdoor is er voor satellieten in de geostationaire ring een verhoogde kans op interne opbouw van elektrische lading, wat kan resulteren in schade aan de elektronica aan boord van deze satellieten. Operators van deze satellieten kunnen hiermee rekening houden bij het plannen van hun operaties.\n',
        changestateto: 'issued',
        draft: false,
        notificationid: '74647448-90f4-41f3-b6f0-d6733c6efd71',
        eventid: '74647448-90f4-41f3-b6f0-d6733c6efd71',
        issuetime: '2022-09-05T11:37:01Z',
        neweventend: '',
      };
      api.issueNotification(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should discardDraftNotification', () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'discardDraftNotification');

      expect(spy).not.toHaveBeenCalled();

      const params = { notificationid: 'rtsw_mag', eventid: 'test' };
      api.discardDraftNotification(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should setAcknowledged', () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'setAcknowledged');

      expect(spy).not.toHaveBeenCalled();

      const params = 'event-id';
      api.setAcknowledged(params);
      expect(spy).toHaveBeenCalledWith(params);
    });

    it('should getRePopulateTemplateContent', () => {
      const api = createFakeApi();
      const spy = jest.spyOn(api, 'getRePopulateTemplateContent');

      expect(spy).not.toHaveBeenCalled();

      const params = {
        IS_POPULATE_REQUEST: false,
        category: 'ELECTRON_FLUX',
        categorydetail: 'ELECTRON_FLUX_2',
        label: 'ALERT',
        datasource: 'GOES16',
        threshold: 1000,
        thresholdunit: 'particles cm^-2 s^-1 sr^-1',
        neweventstart: '2022-09-05T11:20:00Z',
        message:
          'Vanaf 2022-09-05 11:20:00 UTC (2022-09-05 13:20:00 lokale tijd) is de flux van energetische elektronen (>2 MeV), gemeten door de GOES16 satelliet, boven de drempelwaarde van 1000.0 pfu uitgekomen. Hierdoor is er voor satellieten in de geostationaire ring een verhoogde kans op interne opbouw van elektrische lading, wat kan resulteren in schade aan de elektronica aan boord van deze satellieten. Operators van deze satellieten kunnen hiermee rekening houden bij het plannen van hun operaties.\n',
        changestateto: 'issued',
        draft: false,
        notificationid: '74647448-90f4-41f3-b6f0-d6733c6efd71',
        eventid: '74647448-90f4-41f3-b6f0-d6733c6efd71',
        issuetime: '2022-09-05T11:37:01Z',
        neweventend: '',
      };
      api.getRePopulateTemplateContent(params);
      expect(spy).toHaveBeenCalledWith(params);
    });
  });
  describe('getDummyTimeSerie', () => {
    it('should return correct data for Kp and KpForecast', async () => {
      expect(getDummyTimeSerie('kp', 'kp')).toEqual(kpIndexDummyData);
      expect(getDummyTimeSerie('kpforecast', 'kpforecast')).toEqual(
        kpIndexForecastDummyData,
      );
    });
    it('should return correct data for Kp and KpForecast', async () => {
      expect(getDummyTimeSerie('kp', 'kp')).toEqual(kpIndexDummyData);
      expect(getDummyTimeSerie('kpforecast', 'kpforecast')).toEqual(
        kpIndexForecastDummyData,
      );
    });
    it('should return correct data for rtsw_mag', async () => {
      expect(getDummyTimeSerie('rtsw_mag', 'bt')).toEqual(
        interplanetaryMagneticFieldBt,
      );
      expect(getDummyTimeSerie('rtsw_mag', 'bz_gsm')).toEqual(
        interplanetaryMagneticFieldBz,
      );
    });
    it('should return correct data for rtsw_wind', async () => {
      expect(getDummyTimeSerie('rtsw_wind', 'proton_speed')).toEqual(
        solarWindSpeedDummyData,
      );
      expect(getDummyTimeSerie('rtsw_wind', 'proton_pressure')).toEqual(
        solarWindPressureDummyData,
      );
      expect(getDummyTimeSerie('rtsw_wind', 'proton_density')).toEqual(
        solarWindDensityDummyData,
      );
    });
    it('should return correct data for xray', async () => {
      expect(getDummyTimeSerie('xray', 'solar_x_ray_flux_long')).toEqual(
        xRayFluxDummyData,
      );
    });
  });
});
