/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as utils from '@opengeoweb/api';
import { createFakeApiInstance } from '@opengeoweb/api';
import { createApi } from './api';
import * as apiFuncs from './api';
import {
  Streams,
  Parameters,
  TimeseriesParams,
  StreamParams,
  SWNotification,
} from '../types';

jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual(
    '../../../../api/src/lib/components/ApiContext/utils',
  ) as Record<string, unknown>),
}));

describe('src/utils/api', () => {
  const fakeAxiosInstance = createFakeApiInstance();
  describe('createCancelRequestId', () => {
    it('should return correctly formatted request id', async () => {
      const charts = {
        stream: 'test-s-1' as Streams,
        parameter: 'test-p-1' as Parameters,
      };
      expect(apiFuncs.createCancelRequestId(charts)).toEqual(
        '-test-s-1-test-p-1',
      );
    });
  });
  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );
      expect(api.getTimeSeries).toBeTruthy();
      expect(api.getTimeSeriesMultiple).toBeTruthy();
      expect(api.getStreams).toBeTruthy();
      expect(api.getBulletin).toBeTruthy();
      expect(api.getBulletinHistory).toBeTruthy();
      expect(api.getEventList).toBeTruthy();
      expect(api.getEvent).toBeTruthy();
      expect(api.getNewNotifications).toBeTruthy();
      expect(api.issueNotification).toBeTruthy();
      expect(api.discardDraftNotification).toBeTruthy();
      expect(api.setAcknowledged).toBeTruthy();
      expect(api.getRePopulateTemplateContent).toBeTruthy();
    });

    it('should call with the right params for getTimeSeries and getTimeSeriesMultiple', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const timeSeriesParams = [
        { params: [{ stream: 'test-s-1', parameter: 'test-p-1' }] },
        { params: [{ stream: 'test-s-2', parameter: 'test-p-2' }] },
      ] as TimeseriesParams;
      await api.getTimeSeries(timeSeriesParams, 'request-id-1');
      expect(spy).toHaveBeenCalledWith('/timeseries/data', {
        params: timeSeriesParams,
        cancelToken: expect.any(Object),
      });

      const timeSeriesParams2 = [
        [{ params: [{ stream: 'test-s-2', parameter: 'test-p-2' }] }],
        [{ params: [{ stream: 'test-s-1', parameter: 'test-p-1' }] }],
      ] as TimeseriesParams[];
      await api.getTimeSeriesMultiple(timeSeriesParams2);
      expect(spy).toHaveBeenCalledWith('/timeseries/data', {
        params: timeSeriesParams2[0],
        cancelToken: expect.any(Object),
      });
      expect(spy).toHaveBeenCalledWith('/timeseries/data', {
        params: timeSeriesParams2[1],
        cancelToken: expect.any(Object),
      });
    });

    it('should call with the right params for getStreams', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const streams = {
        stream: 'xray',
      } as StreamParams;
      await api.getStreams(streams);
      expect(spy).toHaveBeenCalledWith('/timeseries/streams', {
        params: streams,
      });
    });

    it('should call with the right params for getBulletin', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      await api.getBulletin('test-id-1');
      expect(spy).toHaveBeenCalledWith('/bulletin/getBulletin', {
        params: {
          bulletinId: 'test-id-1',
        },
      });

      const bulletinId = 'bulletinId';
      await api.getBulletin(bulletinId);
      expect(spy).toHaveBeenCalledWith('/bulletin/getBulletin', {
        params: { bulletinId },
      });
    });

    it('should call with the right params for getBulletinHistory', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      await api.getBulletinHistory();
      expect(spy).toHaveBeenCalledWith('/bulletin/getBulletinHistory');
    });

    it('should call with the right params for getEventList', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const params = { originator: 'KNMI' as const };
      await api.getEventList(params);
      expect(spy).toHaveBeenCalledWith('/notification/eventList', { params });
    });

    it('should call with the right params for getEvent', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const eventId = 'someEventId';
      await api.getEvent(eventId);
      expect(spy).toHaveBeenCalledWith(`/notification/event/${eventId}`);
    });

    it('should call with the right params for getNewNotifications', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      await api.getNewNotifications();
      expect(spy).toHaveBeenCalledWith('/notification/newNotifications');
    });

    it('should call with the right params for issueNotification', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const formData = {
        category: 'XRAY_RADIO_BLACKOUT',
        categorydetail: '',
      } as SWNotification;
      await api.issueNotification(formData);
      expect(spy).toHaveBeenCalledWith('/notification/store', { ...formData });
    });

    it('should call with the right params for discardDraftNotification', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const params = {
        notificationid: 'notificationId',
        eventid: 'eventId',
      };
      await api.discardDraftNotification(params);
      expect(spy).toHaveBeenCalledWith('/notification/discard', null, {
        params,
      });
    });

    it('should call with the right params for setAcknowledged', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const eventId = 'someEventId';
      await api.setAcknowledged(eventId);
      expect(spy).toHaveBeenCalledWith(`/notification/acknowledged/${eventId}`);
    });

    it('should return an error message and a result when one timeseries call fails and one succeeds', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      jest
        .spyOn(fakeAxiosInstance, 'get')
        .mockRejectedValueOnce(new Error('first call fails'))
        .mockResolvedValueOnce('second call resolves');

      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const timeSeriesParams2 = [
        [{ params: [{ stream: 'test-s-2', parameter: 'test-p-2' }] }],
        [{ params: [{ stream: 'test-s-1', parameter: 'test-p-1' }] }],
      ] as TimeseriesParams[];
      const result = await api.getTimeSeriesMultiple(timeSeriesParams2);
      expect(result).toEqual([
        new Error('first call fails'),
        'second call resolves',
      ]);
    });

    it('should call with the right params for getRePopulateTemplateContent', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const formData = {
        category: 'XRAY_RADIO_BLACKOUT',
        categorydetail: '',
        changestateto: '',
        datasource: 'DATA SOURCE',
        draft: false,
        eventid: 'b8e3a08e-2845-11ed-a257-ea7f77985b4f',
        issuetime: '2022-08-30T09:25:43Z',
        label: 'ALERT',
        message: '',
        neweventend: '',
        neweventlevel: 'R1',
        neweventstart: '2022-08-30T09:20:00Z',
        notificationid: 'b8e3a08e-2845-11ed-a257-ea7f77985b4f',
        threshold: 0.00001,
        thresholdunit: 'W.m^-2',
        title: '',
        xrayclass: 'M1',
      };
      await api.getRePopulateTemplateContent(formData);
      expect(spy).toHaveBeenCalledWith('/notification/populate', {
        ...formData,
      });
    });
  });
});
