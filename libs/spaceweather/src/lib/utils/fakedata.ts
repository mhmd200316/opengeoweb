/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import moment from 'moment';
import {
  dateFormat,
  SWEvent,
  SWEventLifeCycle,
  ThresholdUnits,
  ThresholdValues,
} from '../types';

const todayDate = moment.utc().format('YYYY-MM-DD');
const yesterday = moment.utc().subtract(1, 'day').format(dateFormat);
const yesterdayDate = moment.utc().subtract(1, 'day').format('YYYY-MM-DD');
const dayBeforeYesterdayDate = moment
  .utc()
  .subtract(2, 'day')
  .format('YYYY-MM-DD');
const dayMin3Date = moment.utc().subtract(3, 'day').format('YYYY-MM-DD');

const generateFakeEventList = (
  todayDate,
  yesterdayDate,
  dayBeforeYesterdayDate,
  dayMin3Date,
): SWEvent[] => {
  return [
    // Event 0
    {
      eventid: 'METRB1123454',
      category: 'PROTON_FLUX',
      categorydetail: 'PROTON_FLUX_100',
      originator: 'METOffice',
      lifecycles: {
        externalprovider: {
          eventid: 'METRB1123454',
          label: 'ALERT',
          owner: 'METOffice',
          firstissuetime: `${todayDate}T03:22:00Z`,
          lastissuetime: `${todayDate}T03:22:00Z`,
          state: 'ended',
          eventstart: `${todayDate}T04:00:00Z`,
          eventend: `${todayDate}T06:00:00Z`,
          canbe: [],
          notifications: [
            {
              eventid: 'METRB1123454',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_100',
              label: 'ALERT',
              changestateto: 'issued',
              threshold: 10,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              neweventstart: `${todayDate}T04:00:00Z`,
              neweventend: '',
              datasource: 'GOES13',
              message:
                'Biological: Passengers and crew in high-flying aircraft at high latitudes may be exposed to radiation risk within normal background limits.\nSatellite operations: Possible minor problems including single-event upsets, noise in imaging systems, and slight reduction of efficiency in solar panel are likely.',
              notificationid: 'METRB1123454',
              issuetime: `${todayDate}T03:22:00Z`,
            },
            {
              eventid: 'METRB1123454',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_100',
              label: 'ALERT',
              changestateto: 'ended',
              threshold: 10,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              neweventstart: `${todayDate}T04:00:00Z`,
              neweventend: `${todayDate}T06:00:00Z`,
              datasource: 'GOES13',
              message:
                'Biological: Passengers and crew in high-flying aircraft at high latitudes may be exposed to radiation risk within normal background limits.\nSatellite operations: Possible minor problems including single-event upsets, noise in imaging systems, and slight reduction of efficiency in solar panel are likely.',
              peakflux: 1.3404200077056885,
              peakfluxtime: `${todayDate}T04:28:00Z`,
              notificationid: 'METRB1123454',
              issuetime: `${todayDate}T03:22:00Z`,
            },
          ],
        },
        internalprovider: {
          eventid: 'METRB1123454',
          label: 'ALERT',
          owner: 'KNMI',
          state: 'ended',
          eventstart: `${todayDate}T04:00:00Z`,
          eventend: `${todayDate}T06:00:00Z`,
          firstissuetime: `${todayDate}T03:30:00Z`,
          lastissuetime: `${todayDate}T03:50:00Z`,
          draft: false,
          canbe: [],
          notifications: [
            {
              eventid: 'METRB1123454',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_100',
              label: 'ALERT',
              changestateto: 'issued',
              draft: false,
              neweventstart: `${todayDate}T04:00:00Z`,
              neweventend: '',
              threshold: 10,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              datasource: 'GOES13',
              title: 'Proton Flux Alert verwacht 12-07-2021 04:00 UTC',
              message:
                'Er is wat zwaar weer onderweg. Rekening houden met verstoring van satellieten',
              notificationid: 'DRAFTNOTIFICATION122455',
              issuetime: `${todayDate}T03:30:00Z`,
            },
            {
              eventid: 'METRB1123454',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_100',
              label: 'ALERT',
              changestateto: 'ended',
              draft: false,
              neweventstart: `${todayDate}T04:20:00Z`,
              neweventend: `${todayDate}T06:00:00Z`,
              threshold: 10,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              datasource: 'GOES13',
              title: 'Proton Flux Alert 12-07-2021 afgelopen',
              message:
                'Er is wat zwaar weer onderweg. Passagiers in vliegruigen worden mogelijk blootgesteld aan verhoogde stralings niveaus. Rekening houden met verstoring van satellieten',
              peakflux: 1.3404200077056885,
              peakfluxtime: `${todayDate}T04:28:00Z`,
              notificationid: 'DRAFTNOTIFICATION122455',
              issuetime: `${todayDate}T03:50:00Z`,
            },
          ],
        },
      },
    },
    // Event 1
    {
      eventid: 'METRB2',
      category: 'GEOMAGNETIC',
      categorydetail: 'GEOMAGNETIC_SUDDEN_IMPULSE',
      originator: 'METOffice',
      notacknowledged: true,
      lifecycles: {
        externalprovider: {
          eventid: 'METRB2',
          label: 'ALERT',
          owner: 'METOffice',
          eventstart: `${yesterdayDate}T11:20:00Z`,
          eventend: '',
          firstissuetime: `${yesterdayDate}T11:13:00Z`,
          lastissuetime: `${yesterdayDate}T11:13:00Z`,
          state: 'issued',
          canbe: ['updated', 'extended'],
          notifications: [
            {
              eventid: 'METRB2',
              category: 'GEOMAGNETIC',
              categorydetail: 'GEOMAGNETIC_SUDDEN_IMPULSE',
              label: 'ALERT',
              changestateto: 'issued',
              neweventstart: `${yesterdayDate}T11:20:00Z`,
              neweventend: '',
              message:
                'This alert will be followed by issue of geomagnetic warnings and alerts as necessary.  The Geomagnetic impulse detected should include reference to any current existing Kp warning.',
              notificationid: 'METRB2MET_8373489',
              datasource: 'BGS Hartland magnetometer',
              impulsetime: `${yesterdayDate}T11:11:00Z`,
              magnetometerdeflection: 36.0,
              issuetime: `${yesterdayDate}T11:13:00Z`,
            },
          ],
        },
        internalprovider: {
          eventid: 'METRB2',
          label: 'ALERT',
          owner: 'KNMI',
          eventstart: `${yesterdayDate}T11:20:00Z`,
          eventend: '',
          firstissuetime: `${yesterdayDate}T11:13:00Z`,
          lastissuetime: `${yesterdayDate}T11:13:00Z`,
          state: 'issued',
          canbe: ['updated', 'extended'],
          notifications: [
            {
              eventid: 'METRB2',
              category: 'GEOMAGNETIC',
              categorydetail: 'GEOMAGNETIC_SUDDEN_IMPULSE',
              label: 'ALERT',
              changestateto: 'issued',
              neweventstart: `${yesterdayDate}T11:20:00Z`,
              neweventend: '',
              title: 'Geomagnetic sudden impulse 14-07-2020 at 11:20UTC',
              message:
                'This alert will be followed by issue of geomagnetic warnings and alerts as necessary.  The Geomagnetic impulse detected should include reference to any current existing Kp warning.',
              notificationid: 'METRB2MET_8373489',
              datasource: 'BGS Hartland magnetometer',
              impulsetime: `${yesterdayDate}T11:11:00Z`,
              magnetometerdeflection: 36.0,
              issuetime: `${yesterdayDate}T11:13:00Z`,
            },
          ],
        },
      },
    },
    // Event 2
    {
      eventid: 'METRB122',
      category: 'PROTON_FLUX',
      categorydetail: 'PROTON_FLUX_100',
      originator: 'METOffice',
      lifecycles: {
        externalprovider: {
          eventid: 'METRB122',
          label: 'WARNING',
          owner: 'METOffice',
          firstissuetime: `${yesterdayDate}T13:11:00Z`,
          lastissuetime: `${yesterdayDate}T18:06:00Z`,
          eventstart: `${yesterdayDate}T13:30:00Z`,
          eventend: `${todayDate}T23:55:00Z`,
          state: 'ended',
          canbe: [],
          notifications: [
            {
              eventid: 'METRB122',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_100',
              label: 'WARNING',
              changestateto: 'issued',
              neweventstart: `${yesterdayDate}T13:30:00Z`,
              neweventend: `${todayDate}T23:55:00Z`,
              threshold: 100,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              datasource: 'GOES13',
              message:
                'Biological: Passengers and crew in high-flying aircraft at high latitudes may be exposed to radiation risk within normal background limits.\nSatellite operations: Possible minor problems including single-event upsets, noise in imaging systems, and slight reduction of efficiency in solar panel are likely.',
              notificationid: 'METRB1MET1_973454564568',
              issuetime: `${yesterdayDate}T13:11:00Z`,
            },
            {
              eventid: 'METRB122',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_100',
              label: 'WARNING',
              changestateto: 'ended',
              neweventstart: `${yesterdayDate}T13:30:00Z`,
              neweventend: `${todayDate}T23:55:00Z`,
              threshold: 100,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              datasource: 'GOES13',
              message: '100MeV proton flux now declining. ',
              notificationid: 'METRB1MET1_973454564568',
              issuetime: `${yesterdayDate}T18:06:00Z`,
            },
          ],
        },
        internalprovider: {
          eventid: 'METRB122',
          label: 'WARNING',
          owner: 'KNMI',
          firstissuetime: `${yesterdayDate}T13:16:00Z`,
          lastissuetime: `${yesterdayDate}T13:16:00Z`,
          eventstart: `${yesterdayDate}T13:30:00Z`,
          eventend: `${todayDate}T23:55:00Z`,
          state: 'issued',
          canbe: ['updated', 'extended', 'cancelled'],
          notifications: [
            {
              eventid: 'METRB122',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_100',
              label: 'WARNING',
              changestateto: 'issued',
              neweventstart: `${yesterdayDate}T13:30:00Z`,
              neweventend: `${todayDate}T23:55:00Z`,
              threshold: 100,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              datasource: 'GOES13',
              title: 'Proton flux 100 warning for 14-07-2020 13:30UTC',
              message:
                'Passagiers en crewleden in vliegtuigen kunnen mogelijk verhoogde blootstelling aan straling ondervinden. De verhoogde starling ligt binnen de achtergrond straling limieten. Voor satelieten kunnen er mogelijk kleine porblemen onstaan.',
              notificationid: 'KNMIMET1_973454564568',
              issuetime: `${yesterdayDate}T13:16:00Z`,
            },
          ],
        },
      },
    },
    // Event 3
    {
      eventid: 'METRB1',
      category: 'XRAY_RADIO_BLACKOUT',
      categorydetail: '',
      originator: 'METOffice',
      lifecycles: {
        externalprovider: {
          eventid: 'METRB1',
          label: 'ALERT',
          eventlevel: 'R2',
          owner: 'METOffice',
          firstissuetime: `${yesterdayDate}T12:00:00Z`,
          lastissuetime: `${yesterdayDate}T12:00:00Z`,
          eventstart: `${yesterdayDate}T13:00:00Z`,
          eventend: '',
          state: 'ended',
          canbe: [],
          notifications: [
            {
              eventid: 'METRB1',
              category: 'XRAY_RADIO_BLACKOUT',
              categorydetail: '',
              datasource: 'GOES15',
              neweventlevel: 'R2',
              label: 'ALERT',
              changestateto: 'issued',
              neweventstart: `${yesterdayDate}T13:00:00Z`,
              neweventend: '',
              threshold: ThresholdValues.XRAY_RADIO_BLACKOUT[1],
              thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
              xrayclass: 'M5',
              message:
                'HF Radio: Limited blackout of HF radio communication on sunlit side, loss of radio contact for tens of minutes.\nNavigation: Degradation of low-frequency navigation signals for tens of minutes.',
              notificationid: 'METRB1MET1_9749374728',
              issuetime: `${yesterdayDate}T12:00:00Z`,
            },
            {
              eventid: 'METRB1',
              category: 'XRAY_RADIO_BLACKOUT',
              categorydetail: '',
              datasource: 'GOES15',
              neweventlevel: 'R2',
              label: 'ALERT',
              changestateto: 'ended',
              neweventstart: `${yesterdayDate}T13:00:00Z`,
              neweventend: `${yesterdayDate} 17:00`,
              threshold: ThresholdValues.XRAY_RADIO_BLACKOUT[1],
              thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
              xrayclass: 'M5',
              message:
                'HF Radio: Limited blackout of HF radio communication on sunlit side, loss of radio contact for tens of minutes.\nNavigation: Degradation of low-frequency navigation signals for tens of minutes.',
              notificationid: 'METRB1MET1_9749374728',
              issuetime: `${yesterdayDate}T12:00:00Z`,
              peakclass: 'M5.5',
              peakflux: 5.5376898671966046e-5,
              peakfluxtime: `${yesterdayDate}T16:33:00Z`,
            },
          ],
        },
        internalprovider: {
          eventid: 'METRB1',
          label: 'ALERT',
          owner: 'KNMI',
          firstissuetime: `${yesterdayDate}T12:10:00Z`,
          lastissuetime: `${yesterdayDate}T14:58:00Z`,
          state: 'issued',
          eventlevel: 'R2',
          eventstart: `${yesterdayDate}T13:00:00Z`,
          eventend: '',
          canbe: ['updated', 'extended', 'summarised'],
          notifications: [
            {
              eventid: 'METRB1',
              category: 'XRAY_RADIO_BLACKOUT',
              categorydetail: '',
              datasource: 'GOES15',
              label: 'ALERT',
              changestateto: 'issued',
              neweventlevel: 'R2',
              neweventstart: `${yesterdayDate}T13:00:00Z`,
              neweventend: '',
              threshold: ThresholdValues.XRAY_RADIO_BLACKOUT[1],
              thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
              xrayclass: 'M5',
              title: 'Xray radio blackout alert for 13-07-2020 13:00UTC',
              message: 'NOTIFICATIE TEXT IN HET NEDERLANDS JAWEEEL',
              notificationid: 'METRB1KNMI_8474620372',
              issuetime: `${yesterdayDate}T12:10:00Z`,
            },
            {
              eventid: 'METRB1',
              category: 'XRAY_RADIO_BLACKOUT',
              categorydetail: '',
              datasource: 'GOES15',
              label: 'ALERT',
              changestateto: 'issued',
              neweventlevel: 'R2',
              neweventstart: `${yesterdayDate}T16:00:00Z`,
              neweventend: '',
              threshold: ThresholdValues.XRAY_RADIO_BLACKOUT[1],
              thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
              xrayclass: 'M5',
              title:
                'Xray radio blackout alert for 13-07-2020 16:00UTC updated',
              message: 'en een update voor ons jaweeeel',
              notificationid: 'METRB1KNMI_89847263494',
              issuetime: `${yesterdayDate}T14:58:00Z`,
            },
          ],
        },
      },
    },
    // Event 4
    {
      eventid: 'METRB13',
      category: 'XRAY_RADIO_BLACKOUT',
      categorydetail: '',
      originator: 'METOffice',
      notacknowledged: true,
      lifecycles: {
        externalprovider: {
          label: 'ALERT',
          eventid: 'METRB13',
          owner: 'METOffice',
          firstissuetime: `${yesterdayDate}T03:05:00Z`,
          lastissuetime: `${yesterdayDate}T05:30:00Z`,
          eventlevel: 'R2',
          state: 'ended',
          eventstart: `${yesterdayDate}T03:00:00Z`,
          eventend: `${yesterdayDate}T05:12:00Z`,
          canbe: [],
          notifications: [
            {
              eventid: 'METRB13',
              category: 'XRAY_RADIO_BLACKOUT',
              categorydetail: '',
              datasource: 'GOES15',
              neweventlevel: 'R2',
              label: 'ALERT',
              changestateto: 'issued',
              neweventstart: `${yesterdayDate}T03:00:00Z`,
              neweventend: '',
              threshold: ThresholdValues.XRAY_RADIO_BLACKOUT[1],
              thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
              xrayclass: 'M5',
              message:
                '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
              notificationid: 'METRB13MET1_356456',
              issuetime: `${yesterdayDate}T03:05:00Z`,
            },
            {
              eventid: 'METRB1',
              category: 'XRAY_RADIO_BLACKOUT',
              categorydetail: '',
              datasource: 'GOES15',
              neweventlevel: 'R2',
              label: 'ALERT',
              changestateto: 'ended',
              neweventstart: `${yesterdayDate}T03:00:00Z`,
              neweventend: `${yesterdayDate}T05:30:00Z`,
              threshold: ThresholdValues.XRAY_RADIO_BLACKOUT[1],
              thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
              xrayclass: 'M5',
              message: 'its done',
              notificationid: 'METRB1KNMI_34547657',
              issuetime: `${yesterdayDate}T05:30:00Z`,
              peakclass: 'M5.5',
              peakflux: 5.5376898671966046e-5,
              peakfluxtime: `${yesterdayDate}T16:33:00Z`,
            },
          ],
        },
        internalprovider: {
          eventid: 'METRB13',
          label: 'ALERT',
          owner: 'KNMI',
          firstissuetime: `${yesterdayDate}T03:05:00Z`,
          lastissuetime: `${yesterdayDate}T03:10:00Z`,
          eventlevel: 'R2',
          state: 'issued',
          eventstart: `${yesterdayDate}T03:00:00Z`,
          eventend: '',
          canbe: ['updated', 'extended', 'summarised'],
          notifications: [
            {
              eventid: 'METRB13',
              category: 'XRAY_RADIO_BLACKOUT',
              categorydetail: '',
              datasource: 'GOES15',
              neweventlevel: 'R2',
              label: 'ALERT',
              changestateto: 'issued',
              neweventstart: `${yesterdayDate}T03:00:00Z`,
              neweventend: '',
              threshold: ThresholdValues.XRAY_RADIO_BLACKOUT[1],
              thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
              xrayclass: 'M5',
              title: 'Xray radio blackout alert for 13-07-2020 03:00UTC',
              message:
                '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
              notificationid: 'METRB13KNMI_365687',
              issuetime: `${yesterdayDate}T03:10:00Z`,
            },
          ],
        },
      },
    },
    // Event 5
    {
      eventid: 'KNMIRB1',
      category: 'XRAY_RADIO_BLACKOUT',
      categorydetail: '',
      originator: 'KNMI',
      lifecycles: {
        internalprovider: {
          eventid: 'KNMIRB1',
          label: 'ALERT',
          owner: 'KNMI',
          eventlevel: 'R2',
          firstissuetime: `${dayBeforeYesterdayDate}T15:10:00Z`,
          lastissuetime: `${yesterdayDate}T00:30:00Z`,
          state: 'summary',
          eventstart: `${dayBeforeYesterdayDate}T19:00:00Z`,
          eventend: `${yesterdayDate}T00:20:00Z`,
          canbe: [],
          notifications: [
            {
              eventid: 'KNMIRB1',
              category: 'XRAY_RADIO_BLACKOUT',
              categorydetail: '',
              datasource: 'GOES15',
              neweventlevel: 'R2',
              label: 'ALERT',
              changestateto: 'issued',
              neweventstart: `${dayBeforeYesterdayDate}T19:00:00Z`,
              neweventend: '',
              threshold: ThresholdValues.XRAY_RADIO_BLACKOUT[1],
              thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
              xrayclass: 'M5',
              title: 'Xray radio blackout alert for 12-07-2020 19:00UTC',
              message: 'Er staat dus echt iets heeel ergs te gebeuren zeg he',
              notificationid: 'KNMIB1KNMI_903847589',
              issuetime: `${dayBeforeYesterdayDate}T15:10:00Z`,
            },
            {
              eventid: 'KNMIRB1',
              category: 'XRAY_RADIO_BLACKOUT',
              categorydetail: '',
              datasource: 'GOES15',
              neweventlevel: 'R2',
              label: 'ALERT',
              changestateto: 'ended',
              neweventstart: `${dayBeforeYesterdayDate}T19:00:00Z`,
              neweventend: `${yesterdayDate}T00:20:00Z`,
              threshold: ThresholdValues.XRAY_RADIO_BLACKOUT[1],
              thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
              xrayclass: 'M5',
              title:
                'Xray radio blackout alert voor 12-07-2020 19:00UTC geeindigd',
              message: 'Het is allemaall voorbij jongens, geen paniek',
              notificationid: 'METRB1KNMI_3656734',
              issuetime: `${yesterdayDate}T00:25:00Z`,
              peakclass: 'M5.5',
              peakflux: 5.5376898671966046e-5,
              peakfluxtime: `${dayBeforeYesterdayDate}T16:33:00Z`,
            },
          ],
        },
      },
    },
    // Event 6
    {
      eventid: 'METRB3',
      category: 'ELECTRON_FLUX',
      categorydetail: 'ELECTRON_FLUX_2',
      originator: 'METOffice',
      lifecycles: {
        externalprovider: {
          eventid: 'METRB3',
          label: 'ALERT',
          owner: 'METOffice',
          firstissuetime: `${dayBeforeYesterdayDate}T06:13:00Z`,
          lastissuetime: `${dayBeforeYesterdayDate}T07:59:00Z`,
          state: 'ended',
          eventstart: `${dayBeforeYesterdayDate}T06:00:00Z`,
          eventend: `${dayBeforeYesterdayDate}T08:12:00Z`,
          canbe: [],
          notifications: [
            {
              eventid: 'METRB3',
              category: 'ELECTRON_FLUX',
              categorydetail: 'ELECTRON_FLUX_2',
              label: 'ALERT',
              changestateto: 'issued',
              neweventstart: `${dayBeforeYesterdayDate}T06:00:00Z`,
              neweventend: '',
              threshold: 1000,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              datasource: 'GOES13',
              message:
                'Potential Impact: Satellites may experience significant charging resulting in increased risk to satellite systems.',
              notificationid: 'METRB3MET1_9874795',
              issuetime: `${dayBeforeYesterdayDate}T06:13:00Z`,
            },
            {
              eventid: 'METRB3',
              category: 'ELECTRON_FLUX',
              categorydetail: 'ELECTRON_FLUX_2',
              label: 'ALERT',
              changestateto: 'ended',
              neweventstart: `${dayBeforeYesterdayDate}T06:00:00Z`,
              neweventend: `${dayBeforeYesterdayDate}T07:55:00Z`,
              threshold: 1000,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              datasource: 'GOES13',
              peakflux: 1768.760009765625,
              peakfluxtime: `${dayBeforeYesterdayDate}T06:25`,
              message:
                'Potential Impact: Satellites may experience significant charging resulting in increased risk to satellite systems.',
              notificationid: 'METRB3MET12_3453545',
              issuetime: `${dayBeforeYesterdayDate}T07:59:00Z`,
            },
          ],
        },
        internalprovider: {
          eventid: 'METRB3',
          owner: 'KNMI',
          label: 'ALERT',
          firstissuetime: `${dayBeforeYesterdayDate}T06:16:00Z`,
          lastissuetime: `${dayBeforeYesterdayDate}T08:06:00Z`,
          state: 'ended',
          eventstart: `${dayBeforeYesterdayDate}T06:00:00Z`,
          eventend: `${dayBeforeYesterdayDate}T07:55:00Z`,
          canbe: [],
          notifications: [
            {
              eventid: 'METRB3',
              category: 'ELECTRON_FLUX',
              categorydetail: 'ELECTRON_FLUX_2',
              label: 'ALERT',
              changestateto: 'issued',
              neweventstart: `${dayBeforeYesterdayDate}T06:00:00Z`,
              neweventend: '',
              threshold: 1000,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              title: 'Elektron flux alert voor 09-07-2020 06:00UTC',
              message:
                'Satellieten ondervinden mogelijk verstoringen resulterend in verhoogd risico voor satelliet systemen',
              datasource: 'GOES13',
              notificationid: 'METRB3KNMI_4656778',
              issuetime: `${dayBeforeYesterdayDate}T06:00:00Z`,
            },
            {
              eventid: 'METRB3',
              category: 'ELECTRON_FLUX',
              categorydetail: 'ELECTRON_FLUX_2',
              label: 'ALERT',
              changestateto: 'ended',
              neweventstart: `${dayBeforeYesterdayDate}T06:00:00Z`,
              neweventend: `${dayBeforeYesterdayDate}T07:55:00Z`,
              threshold: 1000,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              datasource: 'GOES13',
              peakflux: 1768.760009765625,
              peakfluxtime: `${dayBeforeYesterdayDate}T06:25`,
              title: 'Elektron flux alert voor 09-07-2020 06:00UTC geeindigd',
              message:
                'Satellieten ondervinden mogelijk verstoringen resulterend in verhoogd risico voor satelliet systemen',
              notificationid: 'METRB3MET12_3453545',
              issuetime: `${dayBeforeYesterdayDate}T08:06:00Z`,
            },
          ],
        },
      },
    },
    // Event 7
    {
      eventid: 'KNMIRB24',
      category: 'XRAY_RADIO_BLACKOUT',
      categorydetail: '',
      originator: 'KNMI',
      lifecycles: {
        internalprovider: {
          eventid: 'KNMIRB24',
          label: 'ALERT',
          owner: 'KNMI',
          firstissuetime: `${dayBeforeYesterdayDate}T00:34:00Z`,
          lastissuetime: `${dayBeforeYesterdayDate}T00:34:00Z`,
          eventstart: `${dayBeforeYesterdayDate}T00:15:00Z`,
          eventend: '',
          eventlevel: 'R2',
          state: 'issued',
          canbe: ['updated', 'extended', 'summarised'],
          notifications: [
            {
              eventid: 'KNMIRB24',
              category: 'XRAY_RADIO_BLACKOUT',
              categorydetail: '',
              datasource: 'GOES15',
              neweventlevel: 'R2',
              label: 'ALERT',
              changestateto: 'issued',
              neweventstart: `${dayBeforeYesterdayDate}T00:15:00Z`,
              neweventend: '',
              threshold: ThresholdValues.XRAY_RADIO_BLACKOUT[1],
              thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
              xrayclass: 'M5',
              title: 'Xray radio blackout alert voor 09-07-2020 00:15UTC',
              message: 'Er staat dus echt iets heeel ergs te gebeuren zeg he',
              notificationid: 'KNMIRB24_234289',
              issuetime: `${dayBeforeYesterdayDate}T00:34:00Z`,
            },
          ],
        },
      },
    },
    // Event 8
    {
      eventid: 'METRB4',
      category: 'GEOMAGNETIC',
      categorydetail: 'GEOMAGNETIC_SUDDEN_IMPULSE',
      originator: 'METOffice',
      lifecycles: {
        externalprovider: {
          eventid: 'METRB4',
          label: 'WARNING',
          owner: 'METOffice',
          firstissuetime: `${dayBeforeYesterdayDate}T03:22:00Z`,
          lastissuetime: `${dayBeforeYesterdayDate}T03:22:00Z`,
          state: 'issued',
          eventstart: `${dayBeforeYesterdayDate}T03:12:00Z`,
          eventend: `${dayBeforeYesterdayDate}T05:55:00Z`,
          canbe: [],
          notifications: [
            {
              eventid: 'METRB4',
              category: 'GEOMAGNETIC',
              categorydetail: 'GEOMAGNETIC_SUDDEN_IMPULSE',
              label: 'WARNING',
              changestateto: 'issued',
              neweventstart: `${dayBeforeYesterdayDate}T03:12:00Z`,
              neweventend: `${dayBeforeYesterdayDate}T05:55:00Z`,
              message:
                'This warning will be followed by the issue of geomagnetic K-scale warnings and alerts as necessary.',
              notificationid: 'METRB4MET1_36456',
              datasource: 'DSCOVR',
              observedpolaritybz: 11.0,
              observedsolarwind: 611.0,
              shocktime: `${dayBeforeYesterdayDate}T03:07:00Z`,
              issuetime: `${dayBeforeYesterdayDate}T03:22:00Z`,
            },
          ],
        },
        internalprovider: {
          eventid: 'METRB4',
          label: 'WARNING',
          owner: 'KNMI',
          firstissuetime: `${dayBeforeYesterdayDate}T03:22:00Z`,
          lastissuetime: `${dayBeforeYesterdayDate}T03:22:00Z`,
          state: 'issued',
          eventstart: `${dayBeforeYesterdayDate}T03:12:00Z`,
          eventend: `${dayBeforeYesterdayDate}T05:55:00Z`,
          canbe: ['updated', 'extended', 'cancelled'],
          notifications: [
            {
              eventid: 'METRB4',
              category: 'GEOMAGNETIC',
              categorydetail: 'GEOMAGNETIC_SUDDEN_IMPULSE',
              label: 'WARNING',
              changestateto: 'issued',
              neweventstart: `${dayBeforeYesterdayDate}T03:12:00Z`,
              neweventend: `${dayBeforeYesterdayDate}T05:55:00Z`,
              title:
                'Geomagnetic sudden impulse waarschuwing voor 08-07-2020 03:12UTC',
              message:
                'This warning will be followed by the issue of geomagnetic K-scale warnings and alerts as necessary.',
              notificationid: 'METRB4MET1_36456',
              datasource: 'DSCOVR',
              observedpolaritybz: 11.0,
              observedsolarwind: 611.0,
              shocktime: `${dayBeforeYesterdayDate}T03:07:00Z`,
              issuetime: `${dayBeforeYesterdayDate}T03:22:00Z`,
            },
          ],
        },
      },
    },
    // Event 9
    {
      eventid: 'METRB25',
      category: 'ELECTRON_FLUX',
      categorydetail: 'ELECTRON_FLUENCE',
      originator: 'METOffice',
      lifecycles: {
        externalprovider: {
          eventid: 'METRB25',
          label: 'WARNING',
          owner: 'METOffice',
          firstissuetime: `${dayMin3Date}T06:13:00Z`,
          lastissuetime: `${dayMin3Date}T06:13:00Z`,
          state: 'issued',
          eventstart: `${dayMin3Date}T06:00:00Z`,
          eventend: `${dayBeforeYesterdayDate}T06:00:00Z`,
          canbe: [],
          notifications: [
            {
              eventid: 'METRB25',
              category: 'ELECTRON_FLUX',
              categorydetail: 'ELECTRON_FLUENCE',
              label: 'WARNING',
              changestateto: 'issued',
              neweventstart: `${dayMin3Date}T06:00:00Z`,
              neweventend: `${dayBeforeYesterdayDate}T06:00:00Z`,
              threshold: 100000000,
              thresholdunit: 'particles cm^-2 day^-1 sr^-1',
              datasource: 'REFM',
              message:
                'Potential Impact: Satellites may experience significant charging resulting in increased risk to satellite systems.',
              notificationid: 'METRB3MET1_9874795',
              issuetime: `${dayMin3Date}T06:13:00Z`,
            },
          ],
        },
        internalprovider: {
          eventid: 'METRB25',
          owner: 'KNMI',
          label: 'WARNING',
          firstissuetime: `${dayMin3Date}T06:19:00Z`,
          lastissuetime: `${dayMin3Date}T06:19:00Z`,
          state: 'issued',
          eventstart: `${dayMin3Date}T06:00:00Z`,
          eventend: `${dayBeforeYesterdayDate}T05:00:00Z`,
          draft: false,
          canbe: ['updated', 'extended', 'cancelled'],
          notifications: [
            {
              eventid: 'METRB25',
              category: 'ELECTRON_FLUX',
              categorydetail: 'ELECTRON_FLUENCE',
              label: 'WARNING',
              changestateto: 'issued',
              neweventstart: `${dayMin3Date}T06:00:00Z`,
              neweventend: `${dayBeforeYesterdayDate}T06:00:00Z`,
              threshold: 100000000,
              thresholdunit: 'particles cm^-2 day^-1 sr^-1',
              datasource: 'REFM',
              title: 'Electron fluence waarschuwing voor 07-07-2020 06:00UTC',
              message: 'Verhoogd risico op uitvallen satelliet systemen',
              notificationid: 'METRB3KNMI_4656778',
              issuetime: `${dayMin3Date}T06:19:00Z`,
            },
          ],
        },
      },
    },
    // Event 10
    {
      eventid: 'METRB1123453453454',
      category: 'PROTON_FLUX',
      categorydetail: 'PROTON_FLUX_10',
      originator: 'METOffice',
      lifecycles: {
        externalprovider: {
          eventid: 'METRB1123453453454',
          label: 'ALERT',
          owner: 'METOffice',
          firstissuetime: `${dayMin3Date}T00:36:00Z`,
          lastissuetime: `${dayMin3Date}T00:41:00Z`,
          eventlevel: 'S1',
          state: 'ended',
          eventstart: `${dayMin3Date}T00:30:00Z`,
          eventend: `${dayMin3Date}T00:35:00Z`,
          canbe: [],
          notifications: [
            {
              eventid: 'METRB1123453453454',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_10',
              label: 'ALERT',
              changestateto: 'issued',
              neweventlevel: 'S1',
              threshold: 10,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              neweventstart: `${dayMin3Date}T04:00:00Z`,
              neweventend: '',
              message: 'Biological: none.\nSatellite operations: none.',
              notificationid: 'METRB1123453453454',
              datasource: 'GOES13',
              issuetime: `${dayMin3Date}T03:22:00Z`,
            },
            {
              eventid: 'METRB1123453453454',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_10',
              label: 'ALERT',
              changestateto: 'ended',
              neweventlevel: 'S1',
              threshold: 10,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              neweventstart: `${dayMin3Date}T04:00:00Z`,
              neweventend: `${dayMin3Date}T00:35:00Z`,
              message: 'Biological: none.\nSatellite operations: none.',
              notificationid: 'METRB1123453453454',
              datasource: 'GOES13',
              peakflux: 11.163700103759766,
              peakfluxtime: `${dayMin3Date}T00:30:00Z`,
              issuetime: `${dayMin3Date}T00:41:00Z`,
            },
          ],
        },
        internalprovider: {
          eventid: 'METRB1123453453454',
          label: 'ALERT',
          owner: 'KNMI',
          state: 'issued',
          firstissuetime: `${dayMin3Date}T00:44:00Z`,
          lastissuetime: `${dayMin3Date}T00:44:00Z`,
          eventlevel: 'S1',
          eventstart: `${dayMin3Date}T00:30:00Z`,
          eventend: '',
          draft: true,
          canbe: ['updated', 'extended', 'summarised'],
          notifications: [
            {
              eventid: 'METRB1123453453454',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_10',
              label: 'ALERT',
              changestateto: 'issued',
              neweventlevel: 'S1',
              threshold: 10,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              neweventstart: `${dayMin3Date}T00:30:00Z`,
              neweventend: '',
              title: 'Proton flux 10 alert voor 12-07-2020 00:30UTC',
              message: 'Geen biologische impact en ook geen sateliet impact',
              datasource: 'GOES13',
              notificationid: '2344433NOTIFICATION122455',
              issuetime: `${dayMin3Date}T00:44:00Z`,
            },
            {
              eventid: 'METRB1123453453454',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_10',
              label: 'ALERT',
              changestateto: 'issued',
              draft: true,
              neweventlevel: 'S1',
              threshold: 10,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              neweventstart: `${dayMin3Date}T00:30:00Z`,
              neweventend: `${dayMin3Date}T00:35:00Z`,
              peakflux: 11.163700103759766,
              peakfluxtime: `${dayMin3Date}T00:30:00Z`,
              title: 'Proton flux 10 alert update',
              message: 'Geen impact',
              datasource: 'GOES13',
              notificationid: 'DRAFTNOTIFICATION122455',
              issuetime: `${dayMin3Date}T03:50:00Z`,
            },
          ],
        },
      },
    },
    // Event 11
    {
      eventid: 'METRB345',
      category: 'GEOMAGNETIC',
      categorydetail: 'KP_INDEX',
      originator: 'MetOffice',
      notacknowledged: false,
      lifecycles: {
        externalprovider: {
          eventid: 'METRB345',
          label: 'ALERT',
          owner: 'MetOffice',
          eventstart: `${dayMin3Date}T11:20:00Z`,
          eventend: '',
          eventlevel: 'G2',
          firstissuetime: `${dayMin3Date}T11:13:00Z`,
          lastissuetime: `${dayMin3Date}T11:13:00Z`,
          state: 'issued',
          canbe: ['updated', 'extended'],
          notifications: [
            {
              eventid: 'METRB345',
              category: 'GEOMAGNETIC',
              categorydetail: 'KP_INDEX',
              neweventlevel: 'G2',
              threshold: 6,
              label: 'ALERT',
              changestateto: 'issued',
              neweventstart: `${dayMin3Date}T11:20:00Z`,
              neweventend: '',
              message:
                'Power systems: No impact on UK power grid.\nSpacecraft operations: Corrective actions to orientation may be required by ground control; possible changes in drag affect orbit predictions.\nOther systems: HF radio propagation can fade at higher latitudes, and aurora has been as low as 55° geomagnetic lat.',
              notificationid: 'METRB2MET_8373489',
              datasource: 'BGS',
              issuetime: `${dayMin3Date}T11:13:00Z`,
            },
          ],
        },
      },
    },
    // Event 12
    {
      eventid: 'KNMI002',
      category: 'GEOMAGNETIC',
      categorydetail: 'KP_INDEX',
      originator: 'KNMI',
      notacknowledged: false,
      lifecycles: {
        internalprovider: {
          eventid: 'KNMI002',
          label: 'WARNING',
          owner: 'KNMI',
          eventstart: `${dayMin3Date}T11:20:00Z`,
          eventend: `${dayMin3Date}T12:55:00Z`,
          eventlevel: 'G3',
          firstissuetime: `${dayMin3Date}T11:13:00Z`,
          lastissuetime: `${dayMin3Date}T11:13:00Z`,
          state: 'issued',
          canbe: ['updated', 'extended', 'cancelled'],
          notifications: [
            {
              eventid: 'KNMI002',
              category: 'GEOMAGNETIC',
              categorydetail: 'KP_INDEX',
              neweventlevel: 'G3',
              threshold: 7,
              label: 'WARNING',
              changestateto: 'issued',
              neweventstart: `${dayMin3Date}T11:20:00Z`,
              neweventend: `${dayMin3Date}T12:55:00Z`,
              title:
                'Geomagnetic Kp index waarschuwing voor 14-07-2020 11:20UTC',
              message:
                'This warning supersedes any current Geomagnetic Storm Watch.\nPower systems: No significant impact on UK power grid likely.\nSpacecraft operations: Surface charging may occur on satellite components, drag may increase on low-Earth-orbit satellites, and corrections may be needed for orientation problems.\nOther systems: Intermittent GPS satellite navigation and low-frequency radio navigation problems may occur, HF radio may be intermittent, and aurora has been seen as low as 50° geomagnetic lat.',
              notificationid: 'METRB2MET_8373489',
              datasource: 'DSCVR / BGS',
              issuetime: `${dayMin3Date}T11:13:00Z`,
            },
          ],
        },
      },
    },
    // Event 13
    {
      eventid: 'METRBWARNPRO1084847',
      category: 'PROTON_FLUX',
      categorydetail: 'PROTON_FLUX_10',
      originator: 'METOffice',
      lifecycles: {
        externalprovider: {
          eventid: 'METRBWARNPRO1084847',
          label: 'WARNING',
          owner: 'METOffice',
          firstissuetime: `${dayMin3Date}T11:52:00Z`,
          lastissuetime: `${dayMin3Date}T11:52:00Z`,
          eventstart: `${dayMin3Date}T05:30:00Z`,
          eventend: `${dayMin3Date}T15:00:00Z`,
          eventlevel: 'S2',
          state: 'expired',
          canbe: [],
          notifications: [
            {
              eventid: 'METRBWARNPRO1084847',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_10',
              label: 'WARNING',
              changestateto: 'issued',
              neweventlevel: 'S2',
              neweventstart: `${dayMin3Date}T05:30:00Z`,
              neweventend: `${dayMin3Date}T15:00:00Z`,
              threshold: 100,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              datasource: 'GOES13',
              message:
                'Biological: No additional risk.\nSatellite operations: Infrequent single-event upsets possible.',
              notificationid: 'METRB1MET1_345830948509345',
              issuetime: `${dayMin3Date}T11:52:00Z`,
            },
          ],
        },
      },
    },
    // Event 14
    {
      eventid: 'METRWATCH121',
      category: 'GEOMAGNETIC',
      categorydetail: 'GEOMAGNETIC_STORM',
      originator: 'MetOffice',
      notacknowledged: false,
      lifecycles: {
        externalprovider: {
          eventid: 'METRWATCH121',
          label: 'WARNING',
          owner: 'MetOffice',
          eventstart: `${dayMin3Date}T11:20:00Z`,
          eventend: `${dayMin3Date}T14:00:00Z`,
          firstissuetime: `${dayMin3Date}T11:13:00Z`,
          lastissuetime: `${dayMin3Date}T11:13:00Z`,
          state: 'expired',
          canbe: ['updated', 'extended', 'summarised'],
          notifications: [
            {
              eventid: 'METRWATCH121',
              category: 'GEOMAGNETIC',
              categorydetail: 'GEOMAGNETIC_STORM',
              label: 'WARNING',
              changestateto: 'issued',
              neweventstart: `${dayMin3Date}T11:20:00Z`,
              neweventend: `${dayMin3Date}T14:00:00Z`,
              initialgscale: 'G2-3',
              message:
                'An Earth directed CME (Coronal Mass Ejection) is expected from a significant flare released yesterday afternoon/evening (10 Sept.) and is expected to arrive during the morning of 12 Sept. and lasting into 13 Sept. The chance of G3 is considered to be slight at 20%. This Geomagnetic Storm Watch may be superseded by Kp alerts and Warnings where necessary as the CME arrives.',
              notificationid: 'METMETRWATCH1213489',
              issuetime: `${dayMin3Date}T11:13:00Z`,
            },
          ],
        },
        internalprovider: {
          eventid: 'METRWATCH121',
          label: 'WARNING',
          owner: 'KNMI',
          eventstart: `${dayMin3Date}T11:20:00Z`,
          eventend: `${dayMin3Date}T14:00:00Z`,
          firstissuetime: `${dayMin3Date}T11:13:00Z`,
          lastissuetime: `${dayMin3Date}T11:13:00Z`,
          state: 'expired',
          canbe: ['updated'],
          notifications: [
            {
              eventid: 'METRWATCH121',
              category: 'GEOMAGNETIC',
              categorydetail: 'GEOMAGNETIC_STORM',
              label: 'WARNING',
              changestateto: 'issued',
              neweventstart: `${dayMin3Date}T11:20:00Z`,
              neweventend: `${dayMin3Date}T14:00:00Z`,
              initialgscale: 'G2-3',
              title: 'Geomagnetic storm waarschuwing voor 14-07-2020 11:20UTC',
              message:
                'An Earth directed CME (Coronal Mass Ejection) is expected from a significant flare released yesterday afternoon/evening (10 Sept.) and is expected to arrive during the morning of 12 Sept. and lasting into 13 Sept. The chance of G3 is considered to be slight at 20%. This Geomagnetic Storm Watch may be superseded by Kp alerts and Warnings where necessary as the CME arrives.',
              notificationid: 'METMETRWATCH1213489',
              issuetime: `${dayMin3Date}T11:13:00Z`,
            },
          ],
        },
      },
    },
    // Event 15
    {
      eventid: 'METRB1123457',
      category: 'PROTON_FLUX',
      categorydetail: 'PROTON_FLUX_100',
      originator: 'KNMI',
      lifecycles: {
        internalprovider: {
          eventid: 'METRB1123457',
          label: 'ALERT',
          owner: 'KNMI',
          state: 'issued',
          eventstart: `${dayMin3Date}T04:00:00Z`,
          eventend: '',
          firstissuetime: `${dayMin3Date}T03:30:00Z`,
          lastissuetime: `${dayMin3Date}T03:50:00Z`,
          draft: false,
          canbe: ['updated', 'extended', 'summarised'],
          notifications: [
            {
              eventid: 'METRB1123457',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_100',
              label: 'ALERT',
              changestateto: 'issued',
              draft: false,
              neweventstart: `${dayMin3Date}T04:00:00Z`,
              neweventend: '',
              threshold: 10,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              datasource: 'GOES13',
              title: 'Proton flux 100 alert voor 12-07-2020 04:00UTC',
              message:
                'Er is wat zwaar weer onderweg. Rekening houden met verstoring van satellieten',
              notificationid: 'NOTIFICATION122457',
              issuetime: `${dayMin3Date}T03:30:00Z`,
            },
          ],
        },
      },
    },
    // Event 16
    {
      eventid: 'METRB1123458',
      category: 'PROTON_FLUX',
      categorydetail: 'PROTON_FLUX_10',
      originator: 'KNMI',
      lifecycles: {
        internalprovider: {
          eventid: 'METRB1123458',
          label: 'ALERT',
          owner: 'KNMI',
          state: 'issued',
          eventstart: `${dayMin3Date}T04:00:00Z`,
          eventend: '',
          eventlevel: 'S1',
          firstissuetime: `${dayMin3Date}T03:30:00Z`,
          lastissuetime: `${dayMin3Date}T03:50:00Z`,
          draft: false,
          canbe: ['updated', 'extended', 'summarised'],
          notifications: [
            {
              eventid: 'METRB1123458',
              category: 'PROTON_FLUX',
              categorydetail: 'PROTON_FLUX_10',
              label: 'ALERT',
              changestateto: 'issued',
              draft: false,
              neweventstart: `${dayMin3Date}T04:00:00Z`,
              neweventend: '',
              threshold: 10,
              neweventlevel: 'S1',
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              datasource: 'GOES13',
              title: 'Proton flux 10 alert voor 12-07-2020 04:00UTC',
              message:
                'Er is wat zwaar weer onderweg. Rekening houden met verstoring van satellieten',
              notificationid: 'NOTIFICATION122458',
              issuetime: `${dayMin3Date}T03:30:00Z`,
            },
          ],
        },
      },
    },
    // Event 17
    {
      eventid: 'METRB9',
      category: 'ELECTRON_FLUX',
      categorydetail: 'ELECTRON_FLUX_2',
      originator: 'KNMI',
      lifecycles: {
        internalprovider: {
          eventid: 'METRB9',
          owner: 'KNMI',
          label: 'ALERT',
          firstissuetime: `${dayMin3Date}T06:16:00Z`,
          lastissuetime: `${dayMin3Date}T08:06:00Z`,
          state: 'issued',
          eventstart: `${dayMin3Date}T06:00:00Z`,
          eventend: '',
          canbe: ['updated', 'extended', 'summarised'],
          notifications: [
            {
              eventid: 'METRB9',
              category: 'ELECTRON_FLUX',
              categorydetail: 'ELECTRON_FLUX_2',
              label: 'ALERT',
              changestateto: 'issued',
              neweventstart: `${dayMin3Date}T06:00:00Z`,
              neweventend: '',
              threshold: 1000,
              thresholdunit: 'particles cm^-2 s^-1 sr^-1',
              title: 'Electron flux alert voor 09-07-2020 06:00UTC',
              message:
                'Satellieten ondervinden mogelijk verstoringen resulterend in verhoogd risico voor satelliet systemen',
              datasource: 'GOES13',
              notificationid: 'METRB9KNMI_4656778',
              issuetime: `${dayMin3Date}T06:19`,
            },
          ],
        },
      },
    },
  ];
};

// Event list containing fake events that gets retrieved when doing a get /eventlist request using the current date
export const fakeEventList: SWEvent[] = generateFakeEventList(
  todayDate,
  yesterdayDate,
  dayBeforeYesterdayDate,
  dayMin3Date,
);

export const fakeEventListFixedDates: SWEvent[] = generateFakeEventList(
  '2022-01-05',
  '2022-01-04',
  '2022-01-03',
  '2022-01-02',
);

export const fakeBulletins = [
  {
    bulletin_id: '5f0ef3eff5b3da68c0d1e3c2',
    id: '5f0ef3eff5b3da68c0d1e3c2',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-15T12:17:46',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-15T12:17:46',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-15T12:17:46',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-15T12:17:46',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          'First bulletin content<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-15T12:17:46',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '5ff3eff5bhfk3f3eff5b1e3c2',
    id: '5ff3eff5bhfk3f3eff5b1e3c2',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-15T00:17:46',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-15T00:17:46',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-15T00:17:46',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-15T00:17:46',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          'Second bulletin content<p><b>Space Weather Forecast Headline: Extremely very very severe activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Extremely very very severe activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-15T00:17:46',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '5f08c0deff58c0d68c0d8c0dc2',
    id: '5f08c0deff58c0d68c0d8c0dc2',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-14T12:11:46',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-14T12:11:46',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-14T12:11:46',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-14T12:11:46',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          '<p><b>Space Weather Forecast Headline: Some medium activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Some medium activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-14T12:11:46',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '5f0ef35f0e5f0ec0d1e35f0e2',
    id: '5f0ef35f0e5f0ec0d1e35f0e2',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-14T00:10:12',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-14T00:10:12',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-14T00:10:12',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-14T00:10:12',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-14T00:10:12',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '5f0ef35f0e5ff35c0d1f35f0f35',
    id: '5f0ef35f0e5ff35c0d1f35f0f35',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-13T12:32:59',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-13T12:32:59',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-13T12:32:59',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-13T12:32:59',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-13T12:32:59',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '5f0e5f05f05f0f0ec5f01e35f0f0e2',
    id: '5f0e5f05f05f0f0ec5f01e35f0f0e2',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-13T00:22:47',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-13T00:22:47',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-13T00:22:47',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-13T00:22:47',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-13T00:22:47',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '5f0fg25f0e5f0f0fg251e35f0e2',
    id: '5f0fg25f0e5f0f0fg251e35f0e2',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-12T12:17:25',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-12T12:17:25',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-12T12:17:25',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-12T12:17:25',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-12T12:17:25',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '24f2035f0e5f0ef2035f2035ff0e2',
    id: '24f2035f0e5f0ef2035f2035ff0e2',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-12T00:12:11',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-12T00:12:11',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-12T00:12:11',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-12T00:12:11',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-12T00:12:11',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '5f0ef35f0efcf0efe35ff0ef',
    id: '5f0ef35f0efcf0efe35ff0ef',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-11T12:09:34',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-11T12:09:34',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-11T12:09:34',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-11T12:09:34',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-11T12:09:34',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '5f0ef35f0e5f0ef35f0e0e2',
    id: '5f0ef35f0e5f0ef35f0e0e2',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-11T00:20:22',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-11T00:20:22',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-11T00:20:22',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-11T00:20:22',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-11T00:20:22',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '5f0ef35f0eg67j95f0e0e2',
    id: '5f0ef35f0eg67j95f0e0e2',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-10T12:11:46',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-10T12:11:46',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-10T12:11:46',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-10T12:11:46',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-10T12:11:46',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '55f0e25f0ef35f0eg67j95f0e0e2',
    id: '55f0e25f0ef35f0eg67j95f0e0e2',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-10T00:09:23',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-10T00:09:23',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-10T00:09:23',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-10T00:09:23',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-10T00:09:23',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
  {
    bulletin_id: '567ldg0ec0d1e35f0e2',
    id: '567ldg0ec0d1e35f0e2',
    message: {
      categories: [
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
          timestamp: '2020-07-09T12:24:51',
          type: 'ELECTRON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
          timestamp: '2020-07-09T12:24:51',
          type: 'GEOMAG',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
          timestamp: '2020-07-09T12:24:51',
          type: 'PROTON',
        },
        {
          previously_exceeded: null,
          probabilities: [
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
            {
              day1: 1,
              day2: 1,
              day3: 1,
              day4: 1,
            },
          ],
          simplified_summary: 'Nil',
          status: 'ISSUED',
          summary:
            '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
          timestamp: '2020-07-09T12:24:51',
          type: 'XRAY',
        },
      ],
      overview: {
        content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind speed, as measured by DSCOVR, remained mostly slow-ambient peaking at 417 km/s at 15/0956UTC. The solar wind density was average and the interplanetary magnetic field (IMF, Bt) was weak to moderate, varying between 2nT and 7nT. The north-south component, Bz, ranged between -5nT and &#43;5nT. The phi angle remained mostly positive (away from the Sun) with a slow rotation in evidence. This gradual rotation, coupled with Bz rotation potentially suggests a glancing blow from a weak CME during 14 July, although this is complicated by the likely presence of the weak high speed stream. Geomagnetic activity was Quiet (Kp 0-2).</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV) as measured by GOES-16 at GEO was at background levels. The high energy electron flux (greater than 2 MeV) was predominantly at background levels. The corresponding 24-hour fluence remained well below the Active threshold (1e8 integrated pfu).</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity: </b>Solar activity is forecast to remain very low, with no sunspots on, or expected to rotate onto the visible disc.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no Earth-directed interplanetary CMEs. Solar winds are expected to remain mainly slow-ambient with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b> The high energy proton flux (greater than 10 MeV), as measured by GOES-16, is expected to persist at background levels. The high energy electron flux (greater than 2 MeV) is forecast to remain at mainly background levels. The associated electron fluence is forecast to remain well below the Active (1e8 integrated pfu) level.&#160;</p>',
        simplified_content:
          '<p><b>Space Weather Forecast Headline: Pretty significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-ray flares observed and no sunspots on the sun. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;The solar wind has remained mostly Slow. The magnetic field carried by the particles of the solar wind remained weak to moderate in strength. Geomagnetic Activity was Quiet.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) remained at background with no solar radiation storms observed.</p><p><b>Four-Day Space Weather Forecast Summary</b></p><p><b></b><b>Solar Activity:&#160;</b>Solar activity is forecast to remain very low, with no sunspots on, nor expected to rotate onto the facing side of the sun.</p><p><b>Solar Wind / Geomagnetic Activity:</b>&#160;There are no CMEs currently in the interplanetary space between the Earth and the Sun. Solar winds are expected to remain near background with Quiet geomagnetic activity.</p><p><b>Energetic Particles / Solar Radiation:</b>&#160;The number of energetic particles (high energy protons) is forecast to stay at background with no solar radiation storms occurring.</p>',
        timestamp: '2020-07-09T12:24:51',
        type: 'ISSUED',
      },
      status: 'ISSUED',
    },
    time: 1594815466,
  },
];

export const mockEventUnacknowledgedExternal: SWEvent = {
  eventid: 'METRB345',
  category: 'GEOMAGNETIC',
  categorydetail: 'KP_INDEX',
  originator: 'MetOffice',
  notacknowledged: true,
  lifecycles: {
    externalprovider: {
      eventid: 'METRB345',
      label: 'ALERT',
      owner: 'MetOffice',
      eventstart: `${yesterdayDate}T11:20:00Z`,
      eventend: '',
      eventlevel: 'G1',
      firstissuetime: `${yesterdayDate}T11:13:00Z`,
      lastissuetime: `${yesterdayDate}T11:13:00Z`,
      state: 'issued',
      canbe: ['updated', 'extended'],
      notifications: [
        {
          eventid: 'METRB345',
          category: 'GEOMAGNETIC',
          categorydetail: 'KP_INDEX',
          neweventlevel: 'G1',
          threshold: 5,
          datasource: 'GOES',
          label: 'ALERT',
          changestateto: 'issued',
          neweventstart: `${yesterdayDate}T11:20:00Z`,
          neweventend: '',
          message: 'Here is this jolly old potential impact message',
          notificationid: 'METRB2MET_8373489',
          issuetime: '2020-07-14T11:13:00Z',
        },
      ],
    },
  },
};

export const mockEventAcknowledgedExternal: SWEvent = {
  eventid: 'METRB1123454',
  category: 'PROTON_FLUX',
  categorydetail: 'PROTON_FLUX_10',
  originator: 'METOffice',
  lifecycles: {
    externalprovider: {
      eventid: 'METRB1123454',
      label: 'ALERT',
      owner: 'METOffice',
      firstissuetime: `${yesterdayDate}T03:22:00Z`,
      lastissuetime: `${yesterdayDate}T03:22:00Z`,
      eventlevel: 'S2',
      state: 'issued',
      eventstart: `${yesterdayDate}T04:00:00Z`,
      eventend: '',
      canbe: [],
      notifications: [
        {
          eventid: 'METRB1123454',
          category: 'PROTON_FLUX',
          categorydetail: 'PROTON_FLUX_10',
          label: 'ALERT',
          changestateto: 'issued',
          neweventlevel: 'S2',
          threshold: 100,
          thresholdunit: 'particles cm^-2 s^-1 sr^-1',
          neweventstart: `${yesterdayDate}T04:00:00Z`,
          neweventend: '',
          datasource: 'GOES13',
          message:
            '<p><b>Space Weather Forecast Headline: No significant activity expected.</b></p><p><b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
          notificationid: 'METRB1123454',
          issuetime: `${yesterdayDate}T03:22:00Z`,
        },
      ],
    },
    internalprovider: {
      eventid: 'METRB1123454',
      label: 'ALERT',
      owner: 'KNMI',
      state: 'issued',
      eventstart: `${yesterdayDate}T04:00:00Z`,
      eventend: '',
      firstissuetime: `${yesterdayDate}T03:30:00Z`,
      lastissuetime: `${yesterdayDate}T03:50:00Z`,
      draft: false,
      canbe: ['updated', 'extended', 'cancelled'],
      notifications: [
        {
          eventid: 'METRB1123454',
          category: 'PROTON_FLUX',
          categorydetail: 'PROTON_FLUX_10',
          neweventlevel: 'S2',
          label: 'ALERT',
          changestateto: 'issued',
          draft: false,
          neweventstart: `${yesterdayDate}T04:20:00Z`,
          neweventend: '',
          threshold: 100,
          thresholdunit: 'particles cm^-2 s^-1 sr^-1',
          title: 'Proton flux 10 alert voor 12-07-2020 04:20UTC',
          message:
            'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
          datasource: 'GOES13',
          notificationid: 'DRAFTNOTIFICATION122455',
          issuetime: `${yesterdayDate}T03:30:00Z`,
        },
        {
          eventid: 'METRB1123454',
          category: 'PROTON_FLUX',
          categorydetail: 'PROTON_FLUX_10',
          neweventlevel: 'S2',
          label: 'ALERT',
          changestateto: 'issued',
          draft: false,
          neweventstart: `${yesterdayDate}T04:20:00Z`,
          neweventend: '',
          threshold: 100,
          thresholdunit: 'particles cm^-2 s^-1 sr^-1',
          datasource: 'GOES13',
          title: 'Proton flux 10 alert voor 12-07-2020 04:20UTC update',
          message:
            'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
          notificationid: 'DRAFTNOTIFICATION122455',
          issuetime: `${yesterdayDate}T03:50:00Z`,
        },
      ],
    },
  },
};

export const mockEvent: SWEvent = {
  eventid: 'METRB2',
  category: 'GEOMAGNETIC',
  categorydetail: 'KP_INDEX',
  originator: 'KNMI',
  notacknowledged: true,
  lifecycles: {
    internalprovider: {
      eventid: 'KNMI',
      label: 'ALERT',
      owner: 'KNMI',
      eventstart: `${yesterdayDate}T11:20:00Z`,
      eventend: '',
      eventlevel: 'G1',
      firstissuetime: `${yesterdayDate}T11:13:00Z`,
      lastissuetime: `${yesterdayDate}T11:13:00Z`,
      state: 'issued',
      canbe: ['updated', 'extended'],
      notifications: [
        {
          eventid: 'METRB2',
          category: 'GEOMAGNETIC',
          categorydetail: 'KP_INDEX',
          neweventlevel: 'G1',
          threshold: 5,
          datasource: 'GOES',
          label: 'ALERT',
          changestateto: 'issued',
          neweventstart: `${yesterdayDate}T11:20:00Z`,
          neweventend: '',
          title: 'Geomagnetic Kp index alert voor 14-07-2020 11:20UTC update',
          message:
            '<b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
          notificationid: 'METRB2MET_8373489',
          issuetime: `${yesterdayDate}T11:13:00Z`,
        },
      ],
    },
  },
};

export const mockEventMissingData: SWEvent = {
  eventid: 'METRB2',
  category: 'GEOMAGNETIC',
  categorydetail: 'KP_INDEX',
  originator: 'KNMI',
  notacknowledged: true,
  lifecycles: {
    internalprovider: {
      eventid: 'KNMI',
      label: 'ALERT',
      owner: 'KNMI',
      eventstart: `${yesterdayDate}T11:20:00Z`,
      eventend: '',
      eventlevel: 'G1',
      firstissuetime: `${yesterdayDate}T11:13:00Z`,
      lastissuetime: `${yesterdayDate}T11:13:00Z`,
      state: 'issued',
      canbe: ['updated', 'extended'],
      notifications: [
        {
          eventid: 'METRB2',
          category: 'GEOMAGNETIC',
          categorydetail: 'KP_INDEX',
          neweventlevel: 'G1',
          threshold: 5,
          datasource: '',
          label: 'ALERT',
          changestateto: 'issued',
          neweventstart: `${yesterdayDate}T11:20:00Z`,
          neweventend: '',
          title: 'Geomagnetic Kp index alert voor 14-07-2020 11:20UTC update',
          message:
            '<b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
          notificationid: 'METRB2MET_8373489',
          issuetime: `${yesterdayDate}T11:13:00Z`,
        },
      ],
    },
  },
};

export const mockEventWarning: SWEvent = {
  eventid: 'METRB2',
  category: 'GEOMAGNETIC',
  categorydetail: 'KP_INDEX',
  originator: 'KNMI',
  notacknowledged: true,
  lifecycles: {
    internalprovider: {
      eventid: 'KNMI',
      label: 'WARNING',
      owner: 'KNMI',
      eventstart: `${yesterdayDate}T11:20:00Z`,
      eventend: `${yesterdayDate}T12:55:00Z`,
      eventlevel: 'G1',
      firstissuetime: `${yesterdayDate}T11:13:00Z`,
      lastissuetime: `${yesterdayDate}T11:13:00Z`,
      state: 'issued',
      canbe: ['updated', 'extended', 'cancelled'],
      notifications: [
        {
          eventid: 'METRB2',
          category: 'GEOMAGNETIC',
          categorydetail: 'KP_INDEX',
          neweventlevel: 'G1',
          threshold: 5,
          label: 'WARNING',
          datasource: 'GOES',
          changestateto: 'issued',
          neweventstart: `${yesterdayDate}T11:20:00Z`,
          neweventend: `${yesterdayDate}T12:55:00Z`,
          title: 'Geomagnetic Kp index warning voor 14-07-2020 11:20UTC',
          message:
            '<b>Analysis of Space Weather Activity over past 24 hours</b></p><p><b></b><b>Solar Activity:</b>&#160;Solar activity was very low over the past 24 hours, with no significant X-Ray flares observed and no sunspots on the visible disc. No Earth-directed Coronal Mass Ejections (CMEs) have been observed on available satellite imagery.</p>',
          notificationid: 'METRB2MET_8373489',
          issuetime: `${yesterdayDate}T11:13:00Z`,
        },
      ],
    },
  },
};

export const mockDraftEvent: SWEvent = {
  eventid: 'METRB2',
  category: 'GEOMAGNETIC',
  categorydetail: 'KP_INDEX',
  originator: 'KNMI',
  notacknowledged: true,
  lifecycles: {
    internalprovider: {
      eventid: 'KNMI',
      label: 'ALERT',
      owner: 'KNMI',
      eventstart: yesterday,
      eventend: '',
      eventlevel: 'G1',
      firstissuetime: yesterday,
      lastissuetime: yesterday,
      state: 'issued',
      canbe: ['updated', 'extended'],
      draft: true,
      notifications: [
        {
          draft: true,
          changestateto: '',
          eventid: 'METRB2',
          category: 'GEOMAGNETIC',
          categorydetail: 'KP_INDEX',
          neweventlevel: 'G1',
          threshold: 5,
          label: 'ALERT',
          neweventstart: yesterday,
          neweventend: '',
          title: 'Geomagnetic Kp index alert',
          message: 'This is a draft message',
          datasource: 'GOES',
          notificationid: 'METRB2MET_8373489',
          issuetime: yesterday,
        },
      ],
    },
  },
};

export const mockEventInternal: SWEvent = {
  eventid: '11111',
  category: 'XRAY_RADIO_BLACKOUT',
  categorydetail: '',
  originator: 'KNMI',
  notacknowledged: true,
  lifecycles: {
    internalprovider: {
      eventid: 'KNMI',
      label: 'ALERT',
      owner: 'KNMI',
      eventstart: yesterday,
      eventend: '',
      eventlevel: 'R2',
      firstissuetime: yesterday,
      lastissuetime: yesterday,
      state: 'draft',
      canbe: ['issued'],
      notifications: [
        {
          eventid: '11111',
          category: 'XRAY_RADIO_BLACKOUT',
          categorydetail: '',
          neweventlevel: 'R2',
          threshold: ThresholdValues.XRAY_RADIO_BLACKOUT[1],
          thresholdunit: ThresholdUnits.XRAY_RADIO_BLACKOUT,
          xrayclass: 'M5',
          label: 'ALERT',
          changestateto: 'issued',
          neweventstart: yesterday,
          neweventend: '',
          title: 'Xray radio blackout alert alert',
          message: 'message',
          notificationid: 'METRB2MET_8373489',
          issuetime: yesterday,
          datasource: 'GOES16',
        },
      ],
    },
  },
};

export const mockBulletin = {
  bulletin_id: '5f0ef3eff5b3da68c0d1e3c2',
  id: '5f0ef3eff5b3da68c0d1e3c2',
  message: {
    categories: [
      {
        previously_exceeded: null,
        probabilities: [
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
        ],
        simplified_summary: 'Nil',
        status: 'ISSUED',
        summary:
          '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
        timestamp: '2020-07-15T12:17:46',
        type: 'ELECTRON',
      },
      {
        previously_exceeded: null,
        probabilities: [
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
        ],
        simplified_summary: 'Nil',
        status: 'ISSUED',
        summary:
          '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
        timestamp: '2020-07-15T12:17:46',
        type: 'GEOMAG',
      },
      {
        previously_exceeded: null,
        probabilities: [
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
        ],
        simplified_summary: 'Nil',
        status: 'ISSUED',
        summary:
          'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
        timestamp: '2020-07-15T12:17:46',
        type: 'PROTON',
      },
      {
        previously_exceeded: null,
        probabilities: [
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
          {
            day1: 1,
            day2: 1,
            day3: 1,
            day4: 1,
          },
        ],
        simplified_summary: 'Nil',
        status: 'ISSUED',
        summary:
          '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
        timestamp: '2020-07-15T12:17:46',
        type: 'XRAY',
      },
    ],
    overview: {
      content: 'This is some fake bulletin content',
      simplified_content: 'This is some fake bulletin content',
      timestamp: '2020-07-15T12:17:46',
      type: 'ISSUED',
    },
    status: 'ISSUED',
  },
  time: 1594815466,
};

export const mockEventAcknowledgedExternalDraft: SWEvent = {
  eventid: 'METRB1123453453454',
  category: 'PROTON_FLUX',
  categorydetail: 'PROTON_FLUX_100',
  originator: 'METOffice',
  lifecycles: {
    externalprovider: {
      eventid: 'METRB1123453453454',
      label: 'ALERT',
      owner: 'METOffice',
      firstissuetime: `${yesterdayDate}T03:22:00Z`,
      lastissuetime: `${yesterdayDate}T03:22:00Z`,
      state: 'issued',
      eventstart: `${yesterdayDate}T04:00:00Z`,
      eventend: '',
      canbe: [],
      notifications: [
        {
          eventid: 'METRB1123453453454',
          category: 'PROTON_FLUX',
          categorydetail: 'PROTON_FLUX_100',
          label: 'ALERT',
          changestateto: 'issued',
          threshold: 1000,
          thresholdunit: 'particles cm^-2 s^-1 sr^-1',
          neweventstart: `${yesterdayDate}T04:00:00Z`,
          neweventend: `${yesterdayDate}T06:00:00Z`,
          title: 'Some draft title',
          message: 'Some draft message',
          notificationid: 'METRB1123453453454',
          issuetime: `${yesterdayDate}T03:22:00Z`,
          datasource: 'GOES-15',
        },
      ],
    },
    internalprovider: {
      eventid: 'METRB1123453453454',
      label: 'ALERT',
      owner: 'KNMI',
      state: 'issued',
      eventstart: `${yesterdayDate}T04:00:00Z`,
      eventend: '',
      firstissuetime: `${yesterdayDate}T03:30:00Z`,
      lastissuetime: `${yesterdayDate}T03:50:00Z`,
      draft: true,
      canbe: ['updated', 'extended', 'summarised'],
      notifications: [
        {
          eventid: 'METRB1123453453454',
          category: 'PROTON_FLUX',
          categorydetail: 'PROTON_FLUX_100',
          label: 'ALERT',
          changestateto: 'issued',
          draft: false,
          neweventstart: `${yesterdayDate}T04:20:00Z`,
          neweventend: '',
          threshold: 1000,
          thresholdunit: 'particles cm^-2 s^-1 sr^-1',
          title: 'Protonflux 100 alert',
          message:
            'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
          notificationid: 'DRAFTNOTIFICATION122455',
          issuetime: `${yesterdayDate}T03:30:00Z`,
          datasource: 'GOES-15',
        },
        {
          eventid: 'METRB1123453453454',
          category: 'PROTON_FLUX',
          categorydetail: 'PROTON_FLUX_100',
          label: 'ALERT',
          changestateto: 'issued',
          draft: false,
          neweventstart: `${yesterdayDate}T04:20:00Z`,
          neweventend: '',
          threshold: 1000,
          thresholdunit: 'particles cm^-2 s^-1 sr^-1',
          title: 'Protonflux 100 alert',
          message:
            'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
          notificationid: 'DRAFTNOTIFICATION1224455655',
          issuetime: `${yesterdayDate}T03:35`,
          datasource: 'GOES-15',
        },
        {
          eventid: 'METRB1123453453454',
          category: 'PROTON_FLUX',
          categorydetail: 'PROTON_FLUX_100',
          neweventlevel: 'S2',
          label: 'ALERT',
          changestateto: 'issued',
          draft: true,
          neweventstart: `${yesterdayDate}T04:20:00Z`,
          neweventend: '',
          threshold: 1000,
          thresholdunit: 'particles cm^-2 s^-1 sr^-1',
          title: 'Protonflux 100 alert',
          message:
            'Er is wat zwaar weer onderweg. Rekening houden met uitval van satellieten',
          notificationid: 'DRAFTNOTIFICATION122455',
          issuetime: `${yesterdayDate}T03:50:00Z`,
          datasource: 'GOES-15',
        },
      ],
    },
  },
};

export const mockBulletinHistory = {
  isLoading: false,
  error: null,
  fetchApiData: (): Promise<void> =>
    new Promise((resolve) => {
      resolve();
    }),
  result: [
    {
      bulletin_id: '45345345345',
      id: '45345345345',
      message: {
        categories: [
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
            timestamp: '2020-07-15T12:17:46',
            type: 'ELECTRON',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
            timestamp: '2020-07-15T12:17:46',
            type: 'GEOMAG',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
            timestamp: '2020-07-15T12:17:46',
            type: 'PROTON',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
            timestamp: '2020-07-15T12:17:46',
            type: 'XRAY',
          },
        ],
        overview: {
          content: 'This is some fake bulletin content',
          simplified_content: 'This is some fake bulletin content',
          timestamp: '2020-07-15T12:17:46',
          type: 'ISSUED',
        },
        status: 'ISSUED',
      },
      time: 1594815466,
    },
    {
      bulletin_id: '5f0ef3eff5b3da68c0d1e3c2',
      id: '5f0ef3eff5b3da68c0d1e3c2',
      message: {
        categories: [
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>The high energy (greater than 2MeV) electron flux as observed by GOES-16, is expected to remain at background levels through the period, with no significant sources of enhancement forecast.</p><p>The associated 24-hour high energy electron fluence is forecast to remain well below the active threshold (1e8 integrated pfu), with REFM currently giving sensible guidance.</p>',
            timestamp: '2020-07-14T12:17:46',
            type: 'ELECTRON',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>There are no Earth-directed interplanetary CMEs.</p><p>Solar winds are currently slow-ambient and close to the slightly elevated threshold. No significant sources of solar wind enhancement are in the forecast, and solar winds are likely to remain at similar levels through the forecast period, with a probable slight downwards trend.&#160;</p><p>Geomagnetic activity is forecast to be Quiet throughout.&#160;</p>',
            timestamp: '2020-07-14T12:17:46',
            type: 'GEOMAG',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              'The high energy proton flux (greater than 10 MeV) is forecast to remain at background levels, with no solar radiation storms expected in the near-Earth space environment.',
            timestamp: '2020-07-14T12:17:46',
            type: 'PROTON',
          },
          {
            previously_exceeded: null,
            probabilities: [
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
              {
                day1: 1,
                day2: 1,
                day3: 1,
                day4: 1,
              },
            ],
            simplified_summary: 'Nil',
            status: 'ISSUED',
            summary:
              '<p>Solar activity is forecast to remain very low, with no sunspots currently on, or expected to rotate onto, the visible disc,</p>',
            timestamp: '2020-07-14T12:17:46',
            type: 'XRAY',
          },
        ],
        overview: {
          content: 'This is some fake bulletin content2',
          simplified_content: 'This is some fake bulletin content2',
          timestamp: '2020-07-14T12:17:46',
          type: 'ISSUED',
        },
        status: 'ISSUED',
      },
      time: 1594815466,
    },
  ],
};

export const mockEventExternalWithEmptyInternalNotifications: SWEvent = {
  eventid: 'METRB1123454',
  category: 'PROTON_FLUX',
  categorydetail: 'PROTON_FLUX_100',
  originator: 'METOffice',
  lifecycles: {
    externalprovider: {
      eventid: 'METRB1123454',
      label: 'ALERT',
      owner: 'METOffice',
      firstissuetime: `${yesterdayDate}T03:22:00Z`,
      lastissuetime: `${yesterdayDate}T03:22:00Z`,
      state: 'issued',
      eventstart: `${yesterdayDate}T04:00:00Z`,
      eventend: '',
      canbe: [],
      notifications: [
        {
          eventid: 'METRB1123454',
          category: 'PROTON_FLUX',
          categorydetail: 'PROTON_FLUX_100',
          label: 'ALERT',
          changestateto: 'issued',
          threshold: 10,
          thresholdunit: 'particles cm^-2 s^-1 sr^-1',
          neweventstart: `${yesterdayDate}T04:00:00Z`,
          neweventend: '',
          message: 'No significant activity expected.',
          notificationid: 'METRB1123454',
          issuetime: `${yesterdayDate}T03:22:00Z`,
          datasource: 'GOES-15',
        },
        {
          eventid: 'METRB1123454',
          category: 'PROTON_FLUX',
          categorydetail: 'PROTON_FLUX_100',
          label: 'ALERT',
          changestateto: 'issued',
          threshold: 100,
          thresholdunit: 'particles cm^-2 s^-1 sr^-1',
          neweventstart: `${yesterdayDate}T04:00:00Z`,
          neweventend: '',
          message: 'A lot of significant activity expected.',
          notificationid: 'METRB1123454',
          issuetime: `${yesterdayDate}T03:22:00Z`,
          datasource: 'GOES-15',
        },
      ],
    },
    internalprovider: {} as SWEventLifeCycle,
  },
};

export const mockEventExternalMultipleNotificationsWithoutInternalProvider: SWEvent =
  {
    eventid: 'METRB1123454',
    category: 'PROTON_FLUX',
    categorydetail: 'PROTON_FLUX_10',
    originator: 'METOffice',
    lifecycles: {
      externalprovider: {
        eventid: 'METRB1123454',
        label: 'WARNING',
        owner: 'METOffice',
        firstissuetime: `${yesterdayDate}T03:22:00Z`,
        lastissuetime: `${yesterdayDate}T03:22:00Z`,
        eventlevel: 'S2',
        state: 'issued',
        eventstart: `${yesterdayDate}T04:00:00Z`,
        eventend: '',
        canbe: [],
        notifications: [
          {
            eventid: 'METRB1123454',
            category: 'PROTON_FLUX',
            categorydetail: 'PROTON_FLUX_10',
            label: 'WARNING',
            changestateto: 'issued',
            neweventlevel: 'S2',
            threshold: 100,
            thresholdunit: 'particles cm^-2 s^-1 sr^-1',
            neweventstart: `${yesterdayDate}T04:00:00Z`,
            neweventend: '',
            message: 'No significant activity expected.',
            notificationid: 'METRB1123454',
            issuetime: `${yesterdayDate}T03:22:00Z`,
            datasource: 'GOES-15',
          },
          {
            eventid: 'METRB1123454',
            category: 'PROTON_FLUX',
            categorydetail: 'PROTON_FLUX_10',
            label: 'WARNING',
            changestateto: 'issued',
            neweventlevel: 'S3',
            threshold: 1000,
            thresholdunit: 'particles cm^-2 s^-1 sr^-1',
            neweventstart: `${yesterdayDate}T04:00:00Z`,
            neweventend: '',
            message: 'A lot of significant activity expected.',
            notificationid: 'METRB1123454',
            issuetime: `${yesterdayDate}T03:22:00Z`,
            datasource: 'GOES-15',
          },
        ],
      },
    },
  };
