/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { ApiProvider } from '@opengeoweb/api';
import { createTheme } from '@mui/material';
import { AxiosError, AxiosResponseHeaders } from 'axios';
import mediaQuery from 'css-mediaquery';

import { ThemeWrapper } from '@opengeoweb/theme';
import { createApi } from './api';

interface WrapperProps {
  children: React.ReactNode;
}
export const TestWrapper: React.FC<WrapperProps> = ({
  children,
}: WrapperProps) => {
  return <ApiProvider createApi={createApi}>{children}</ApiProvider>;
};

// helper for testing media queries https://mui.com/components/use-media-query/#testing
function createMatchMedia(width) {
  return (query): unknown => ({
    matches: mediaQuery.match(query, {
      width,
    }),
    addListener: (): void => {},
    removeListener: (): void => {},
  });
}

interface SizeWrapperProps {
  children: React.ReactNode;
  width?: 'xs' | 'sm' | 'md' | 'lg' | 'xl';
}
export const SizeWrapper: React.FC<SizeWrapperProps> = ({
  children,
  width = 'lg',
}: SizeWrapperProps) => {
  const theme = createTheme();
  const screenSize = theme.breakpoints.values[width];
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  window.matchMedia = createMatchMedia(screenSize);

  return <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;
};

export const fakeBackendError: AxiosError = {
  isAxiosError: true,
  config: undefined,
  toJSON: undefined,
  name: 'API error',
  message: '',
  response: {
    data: 'Unable to store data',
    status: 400,
    statusText: '',
    config: undefined,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};
