![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/libs/spaceweather/package.json)
![coverage](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg?job=test-spaceweather)

# Spaceweather

React component library with Spaceweather components for the opengeoweb project.
This library was generated with [Nx](https://nx.dev).

## Installation

```
npm install @opengeoweb/spaceweather
```

## Use

The front-end uses the [axios](https://github.com/axios/axios) library to fetch data from the api. Importing components from the Space Weather repository should be wrapped by the ApiProvider component with a specified baseURL. Example:

```
import { ApiProvider } from '@opengeoweb/api';
import { TimeSeries } from '@opengeoweb/spaceweather';

const Component = () =>
    <ApiProvider
        baseURL="http://test.com"
        appURL="http://app.com"
        auth={auth}
        onSetAuth={onSetAuth}
        createApi={createApi}
        authTokenUrl="url/tokenrefresh"
      >
        <TimeSeries />
    </ApiProvider>
```

## Documentation

https://opengeoweb.gitlab.io/opengeoweb/docs/spaceweather/

### API

Documentation of the API can be found on Swagger (make sure you're connected to the KNMI network). There are three controllers:

1. [TimeSeries API](http://ec2-34-242-250-251.eu-west-1.compute.amazonaws.com:8080/ui/#/time-series-controller)
2. [Bulletin API](http://ec2-34-242-250-251.eu-west-1.compute.amazonaws.com:8080/bulletin/ui/)
3. [Notification API](http://ec2-34-242-250-251.eu-west-1.compute.amazonaws.com:8080/notification/ui/)
