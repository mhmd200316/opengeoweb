/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { Button } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';

import { CalendarToday, ThemeWrapperOldTheme } from '@opengeoweb/theme';
import { ReactHookFormDateTime, ReactHookFormProvider, isValidDate } from '.';

export default {
  title: 'ReactHookForm/Date Time',
};

const DateTimes = (): React.ReactElement => (
  <div style={{ width: '260px' }}>
    <div style={{ marginBottom: '30px' }}>
      <ReactHookFormDateTime
        name="dateTime-A"
        rules={{
          required: true,
          validate: {
            isValidDate,
          },
        }}
        label="Test label"
      />
    </div>
    <div style={{ marginBottom: '30px' }}>
      <ReactHookFormDateTime
        name="dateTime-B"
        rules={{
          required: true,
          validate: {
            isValidDate,
          },
        }}
      />
    </div>
    <div style={{ marginBottom: '30px' }}>
      <ReactHookFormDateTime
        name="dateTime-C"
        disabled
        rules={{
          required: true,
          validate: {
            isValidDate,
          },
        }}
      />
    </div>
    <div style={{ marginBottom: '30px' }}>
      <ReactHookFormDateTime
        name="dateTime-D"
        disabled
        rules={{
          required: true,
          validate: {
            isValidDate,
          },
        }}
      />
    </div>
    <div style={{ marginBottom: '30px' }}>
      <ReactHookFormDateTime
        name="dateTime-E"
        label="Spaceweather example"
        disablePast
        format="YYYY-MM-DD HH:mm"
        components={{
          OpenPickerIcon: (): React.ReactElement => <CalendarToday />,
        }}
        openTo="day"
        InputAdornmentProps={{ position: 'end' }}
        mask="____-__-__ __:__"
        rules={{
          required: true,
          validate: {
            isValidDate,
          },
        }}
      />
    </div>
  </div>
);

const ReactHookFormDateTimeWrapper = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <ThemeWrapperOldTheme>
      <DateTimes />
      <Button
        variant="contained"
        color="secondary"
        onClick={(): void => {
          handleSubmit(() => {})();
        }}
      >
        Validate
      </Button>
    </ThemeWrapperOldTheme>
  );
};

export const DateTime = (): React.ReactElement => (
  <ReactHookFormProvider
    options={{
      mode: 'onChange',
      reValidateMode: 'onChange',
      defaultValues: {
        'dateTime-B': '2021-01-01T12:00:00Z',
        'dateTime-D': '2021-01-01T12:00:00Z',
      },
    }}
  >
    <ReactHookFormDateTimeWrapper />
  </ReactHookFormProvider>
);

export const DateTimeSnapshot = (): React.ReactElement => (
  <ReactHookFormProvider
    options={{
      mode: 'onChange',
      reValidateMode: 'onChange',
      defaultValues: {
        'dateTime-B': '2021-01-01T12:00:00Z',
        'dateTime-D': '2021-01-01T12:00:00Z',
      },
    }}
  >
    <ThemeWrapperOldTheme>
      <DateTimes />
    </ThemeWrapperOldTheme>
  </ReactHookFormProvider>
);

DateTimeSnapshot.storyName = 'Date Time (takeSnapshot)';
