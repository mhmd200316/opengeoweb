/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { useFormContext } from 'react-hook-form';
import { Button } from '@mui/material';

import ReactHookFormHiddenInput from './ReactHookFormHiddenInput';
import ReactHookFormProvider from './ReactHookFormProvider';

describe('ReactHookFormHiddenInput', () => {
  it('should render a hidden input field', () => {
    const { container } = render(
      <ReactHookFormProvider options={{ defaultValues: { test: '' } }}>
        <ReactHookFormHiddenInput name="test" />
      </ReactHookFormProvider>,
    );

    expect(container.querySelector('input').type).toEqual('hidden');
    expect(container.querySelector('input').value).toEqual('');
    expect(container.querySelector('input').name).toEqual('test');
  });

  it('should not render the input when value is null', () => {
    const { container } = render(
      <ReactHookFormProvider options={{ defaultValues: { test: null } }}>
        <ReactHookFormHiddenInput name="test" />
      </ReactHookFormProvider>,
    );

    expect(container.querySelector('input')).toBeFalsy();
  });

  it('should render a hidden input field with default value', async () => {
    const { container } = render(
      <ReactHookFormProvider options={{ defaultValues: { test: '12345' } }}>
        <ReactHookFormHiddenInput name="test" />
      </ReactHookFormProvider>,
    );

    expect(container.querySelector('input').type).toEqual('hidden');
    await waitFor(() =>
      expect(container.querySelector('input').value).toEqual('12345'),
    );
    expect(container.querySelector('input').name).toEqual('test');
  });

  it('should have default value empty', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      const [value, setValue] = React.useState('hello');
      return (
        <>
          <ReactHookFormHiddenInput name="test" />
          <div data-testid="testValue">{value}</div>
          <Button onClick={handleSubmit((data) => setValue(data['test']))}>
            Submit
          </Button>
        </>
      );
    };

    const { getByText, getByTestId } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );
    expect(getByTestId('testValue').textContent).toEqual('hello');

    fireEvent.click(getByText('Submit'));
    await waitFor(() => {
      expect(getByTestId('testValue').textContent).toEqual('');
    });
  });

  it('should submit given value', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      const [value, setValue] = React.useState('');
      return (
        <>
          <ReactHookFormHiddenInput name="test" defaultValue="hello" />
          <div data-testid="testValue">{value}</div>
          <Button onClick={handleSubmit((data) => setValue(data['test']))}>
            Submit
          </Button>
        </>
      );
    };

    const { getByText, getByTestId } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );
    expect(getByTestId('testValue').textContent).toEqual('');

    fireEvent.click(getByText('Submit'));
    await waitFor(() => {
      expect(getByTestId('testValue').textContent).toEqual('hello');
    });
  });

  it('should validate', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit, errors } = useFormContext();

      return (
        <>
          <ReactHookFormHiddenInput
            name="test"
            rules={{
              required: true,
            }}
          />
          {!!errors.test && <div data-testid="testError" />}
          <Button onClick={handleSubmit(() => {})}>Validate</Button>
        </>
      );
    };

    const { getByText, queryByTestId } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    expect(queryByTestId('testError')).toBeFalsy();

    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(queryByTestId('testError')).toBeTruthy();
    });
  });
});
