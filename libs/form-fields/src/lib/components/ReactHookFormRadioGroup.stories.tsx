/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { Button, FormControlLabel, Radio, Typography } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import { ReactHookFormRadioGroup, ReactHookFormProvider } from '.';

export default {
  title: 'ReactHookForm/Radio Group',
};

const ReactHookFormRadioGroupWrapper = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <ThemeWrapperOldTheme>
      <div style={{ marginBottom: '30px' }}>
        <Typography>Row:</Typography>
        <ReactHookFormRadioGroup
          name="radio1"
          row
          rules={{
            required: true,
          }}
        >
          <FormControlLabel value="one" control={<Radio />} label="One" />
          <FormControlLabel value="two" control={<Radio />} label="Two" />
          <FormControlLabel value="three" control={<Radio />} label="Three" />
        </ReactHookFormRadioGroup>
      </div>
      <div style={{ marginBottom: '30px' }}>
        <Typography>List:</Typography>
        <ReactHookFormRadioGroup
          name="radio2"
          rules={{
            required: true,
            validate: {
              eatApples: (value: string): boolean | string =>
                value === 'apples' || 'You should eat more apples',
            },
          }}
        >
          <FormControlLabel
            value="bananas"
            control={<Radio />}
            label="Bananas"
          />
          <FormControlLabel value="apples" control={<Radio />} label="Apples" />
        </ReactHookFormRadioGroup>
      </div>
      <div style={{ marginBottom: '30px' }}>
        <Typography>Disabled:</Typography>
        <ReactHookFormRadioGroup
          name="radio3"
          rules={{
            required: true,
          }}
          disabled
        >
          <FormControlLabel
            key="apples"
            value="apples"
            control={<Radio />}
            label="Apples"
          />
          <FormControlLabel
            key="bananas"
            value="bananas"
            control={<Radio />}
            label="Bananas"
          />
        </ReactHookFormRadioGroup>
      </div>
      <Button
        variant="contained"
        color="secondary"
        onClick={(): void => {
          handleSubmit(() => {})();
        }}
      >
        Validate
      </Button>
    </ThemeWrapperOldTheme>
  );
};

export const RadioGroup = (): React.ReactElement => (
  <ReactHookFormProvider
    options={{
      mode: 'onChange',
      reValidateMode: 'onChange',
      defaultValues: {
        radio3: 'bananas',
        radio2: 'bananas',
      },
    }}
  >
    <ReactHookFormRadioGroupWrapper />
  </ReactHookFormProvider>
);

const RadioGroups = (): React.ReactElement => (
  <ThemeWrapperOldTheme>
    <div style={{ width: '300px' }}>
      <div style={{ marginBottom: '30px' }}>
        <ReactHookFormRadioGroup
          name="radio1"
          row
          rules={{
            required: true,
          }}
        >
          <FormControlLabel value="one" control={<Radio />} label="One" />
          <FormControlLabel value="two" control={<Radio />} label="Two" />
          <FormControlLabel value="three" control={<Radio />} label="Three" />
        </ReactHookFormRadioGroup>
      </div>
      <div style={{ marginBottom: '30px' }}>
        <ReactHookFormRadioGroup
          name="radio2"
          rules={{
            required: true,
            validate: {
              eatApples: (value: string): boolean | string =>
                value === 'apples' || 'You should eat more apples',
            },
          }}
        >
          <FormControlLabel
            value="bananas"
            control={<Radio />}
            label="Bananas"
          />
          <FormControlLabel value="apples" control={<Radio />} label="Apples" />
        </ReactHookFormRadioGroup>
      </div>
      <div style={{ marginBottom: '30px' }}>
        <ReactHookFormRadioGroup
          name="radio3"
          rules={{
            required: true,
          }}
          disabled
        >
          <FormControlLabel
            key="bananas"
            value="bananas"
            control={<Radio />}
            label="Bananas"
          />
          <FormControlLabel
            key="apples"
            value="apples"
            control={<Radio />}
            label="Apples"
          />
        </ReactHookFormRadioGroup>
      </div>
    </div>
  </ThemeWrapperOldTheme>
);

export const RadioGroupSnapshot = (): React.ReactElement => (
  <ReactHookFormProvider
    options={{
      mode: 'onChange',
      reValidateMode: 'onChange',
      defaultValues: {
        radio3: 'bananas',
        radio2: 'bananas',
      },
    }}
  >
    <RadioGroups />
  </ReactHookFormProvider>
);

RadioGroupSnapshot.storyName = 'Radio Group (takeSnapshot)';
