/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { InputLabel, Select, SelectProps } from '@mui/material';
import { Controller, useFormContext } from 'react-hook-form';

import ReactHookFormFormControl from './ReactHookFormFormControl';
import { Rules } from './types';
import { getErrors } from './formUtils';

type ReactHookFormSelectProps = Partial<SelectProps> & {
  rules: Rules;
  onChange?: (value) => void;
};

const ReactHookFormSelect: React.FC<ReactHookFormSelectProps> = ({
  name,
  label,
  defaultValue = '',
  children,
  rules,
  onChange = (): void => {},
  disabled,
  className,
  sx,
  ...otherProps
}: ReactHookFormSelectProps) => {
  const labelId = `${name}-label`;
  const inputRef = React.useRef(null);
  const { control, errors: formErrors } = useFormContext();
  const errors = getErrors(name, formErrors);

  return (
    <ReactHookFormFormControl
      className={className}
      sx={sx}
      disabled={disabled}
      errors={errors}
    >
      <InputLabel variant="filled" id={labelId}>
        {label}
      </InputLabel>
      <Controller
        render={(_props): React.ReactElement => (
          <Select
            labelId={labelId}
            label={label}
            inputRef={inputRef}
            name={name}
            onChange={(
              changeEvent: React.ChangeEvent<HTMLInputElement>,
            ): void => {
              _props.onChange(changeEvent.target.value);
              // default props
              onChange(changeEvent);
            }}
            value={_props.value}
            variant="filled"
            {...otherProps}
          >
            {children}
          </Select>
        )}
        name={name}
        control={control}
        defaultValue={defaultValue}
        rules={rules}
        onFocus={(): void => {
          inputRef.current.disabled = false;
          inputRef.current?.focus();
        }}
      />
    </ReactHookFormFormControl>
  );
};
export default ReactHookFormSelect;
