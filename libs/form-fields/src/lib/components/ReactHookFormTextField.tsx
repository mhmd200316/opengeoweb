/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { TextField, TextFieldProps } from '@mui/material';
import { Controller, useFormContext } from 'react-hook-form';

import ReactHookFormFormControl from './ReactHookFormFormControl';
import { Rules } from './types';
import { getErrors } from './formUtils';

const styles = {
  textEditor: {
    backgroundColor: 'rgba(0, 117, 169, 0.05)',
  },
};

export const convertTextInputValue = (
  value: string,
  inCapitals: boolean,
): number | string => {
  return inCapitals ? value.toUpperCase() : value;
};

type ReactHookFormTextFieldProps = Partial<TextFieldProps> & {
  rules: Rules;
  upperCase?: boolean;
};

const ReactHookFormTextField: React.FC<ReactHookFormTextFieldProps> = ({
  name,
  label,
  defaultValue = '',
  rules,
  disabled = false,
  upperCase = false,
  helperText = '',
  className,
  sx,
  onChange = (): void => null,
  ...otherProps
}: ReactHookFormTextFieldProps) => {
  const inputRef = React.useRef(null);
  const { control, errors: formErrors } = useFormContext();
  const errors = getErrors(name, formErrors);

  return (
    <ReactHookFormFormControl
      className={className}
      sx={sx}
      disabled={disabled}
      errors={errors}
    >
      <Controller
        render={(_props): React.ReactElement => {
          return (
            <TextField
              label={label}
              style={otherProps.multiline ? styles.textEditor : {}}
              error={!!errors}
              helperText={helperText}
              value={_props.value}
              variant="filled"
              type="text"
              name={name}
              onChange={(evt: React.ChangeEvent<HTMLInputElement>): void => {
                const { value } = evt.target;
                const convertedValue = convertTextInputValue(value, upperCase);

                _props.onChange(convertedValue);
                onChange(null);
              }}
              disabled={disabled}
              inputRef={inputRef}
              {...otherProps}
            />
          );
        }}
        name={name}
        control={control}
        defaultValue={defaultValue}
        rules={rules}
        disabled={disabled}
        onFocus={(): void => {
          inputRef.current.disabled = false;
          inputRef.current?.focus();
        }}
      />
    </ReactHookFormFormControl>
  );
};
export default ReactHookFormTextField;
