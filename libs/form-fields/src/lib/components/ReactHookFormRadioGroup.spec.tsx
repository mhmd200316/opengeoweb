/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Button, FormControlLabel, Radio } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import ReactHookFormRadioGroup from './ReactHookFormRadioGroup';
import ReactHookFormProvider from './ReactHookFormProvider';

describe('ReactHookFormRadioGroup', () => {
  it('should render successfully', () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormRadioGroup name="test" rules={{ required: true }}>
            <FormControlLabel
              value="test1"
              control={<Radio />}
              label="Test 1"
            />
          </ReactHookFormRadioGroup>
        </ReactHookFormProvider>
      );
    };
    const { baseElement } = render(<Wrapper />);
    expect(baseElement).toBeTruthy();
  });

  it('should select a radiobutton', async () => {
    const testOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormRadioGroup
            name="test"
            rules={{ required: true }}
            onChange={testOnChange}
          >
            <FormControlLabel
              value="test1"
              control={<Radio />}
              label="Test 1"
            />
            <FormControlLabel
              data-testid="test2"
              value="test2"
              control={<Radio />}
              label="Test 2"
            />
          </ReactHookFormRadioGroup>
        </ReactHookFormProvider>
      );
    };

    const { getByTestId } = render(<Wrapper />);
    const radioTest2 = getByTestId('test2');
    fireEvent.click(radioTest2);

    await waitFor(() => {
      expect(testOnChange).toHaveBeenCalledTimes(1);
      expect(radioTest2.querySelector('.Mui-checked')).toBeTruthy();
    });
  });

  it('should show the error message on error', async () => {
    const { container, getByText, getByTestId } = render(
      <ReactHookFormProvider
        options={{
          mode: 'onChange',
          reValidateMode: 'onChange',
          defaultValues: {
            radio: 'apples',
          },
        }}
      >
        <ReactHookFormRadioGroup
          name="radio"
          rules={{
            required: true,
            validate: {
              eatApples: (value: string): boolean | string =>
                value === 'apples' || 'You should eat more apples',
            },
          }}
        >
          <FormControlLabel
            data-testid="apples"
            value="apples"
            control={<Radio />}
            label="Apples"
          />
          <FormControlLabel
            data-testid="bananas"
            value="bananas"
            control={<Radio />}
            label="Bananas"
          />
        </ReactHookFormRadioGroup>
      </ReactHookFormProvider>,
    );

    const apples = getByTestId('apples');
    const bananas = getByTestId('bananas');
    expect(apples.querySelector('.Mui-checked')).toBeTruthy();
    expect(bananas.querySelector('.Mui-checked')).toBeFalsy();

    fireEvent.click(bananas);

    await waitFor(() => {
      expect(apples.querySelector('.Mui-checked')).toBeFalsy();
      expect(bananas.querySelector('.Mui-checked')).toBeTruthy();
      expect(getByText('You should eat more apples')).toBeTruthy();
      expect(container.querySelector('.Mui-error')).toBeTruthy();
    });
  });

  it('should focus the first radiobutton on TAB', () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormRadioGroup
            name="test"
            rules={{ required: true }}
            data-testid="fruit"
          >
            <FormControlLabel
              data-testid="apples"
              value="apples"
              control={<Radio />}
              label="Apples"
            />
            <FormControlLabel
              data-testid="bananas"
              value="bananas"
              control={<Radio />}
              label="Bananas"
            />
          </ReactHookFormRadioGroup>
        </ReactHookFormProvider>
      );
    };
    const { getByTestId } = render(<Wrapper />);
    expect(
      getByTestId('apples').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();
    expect(
      getByTestId('bananas').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();

    userEvent.tab();

    expect(
      getByTestId('apples').querySelector('.Mui-focusVisible'),
    ).toBeTruthy();
    expect(
      getByTestId('bananas').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();

    userEvent.tab();

    expect(
      getByTestId('apples').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();
    expect(
      getByTestId('bananas').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();
  });

  it('should focus the first radiobutton on error', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();

      return (
        <>
          <ReactHookFormRadioGroup
            name="test"
            rules={{ required: true }}
            data-testid="fruit"
          >
            <FormControlLabel
              data-testid="apples"
              value="apples"
              control={<Radio />}
              label="Apples"
            />
            <FormControlLabel
              data-testid="bananas"
              value="bananas"
              control={<Radio />}
              label="Bananas"
            />
          </ReactHookFormRadioGroup>
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };
    const { getByTestId, getByText } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );
    expect(
      getByTestId('apples').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();
    expect(
      getByTestId('bananas').querySelector('.Mui-focusVisible'),
    ).toBeFalsy();

    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(
        getByTestId('apples').querySelector('.Mui-focusVisible'),
      ).toBeTruthy();
      expect(
        getByTestId('bananas').querySelector('.Mui-focusVisible'),
      ).toBeFalsy();
      expect(getByText('This field is required')).toBeTruthy();
    });
  });
});
