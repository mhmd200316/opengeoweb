/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { useFormContext } from 'react-hook-form';
import { Button } from '@mui/material';
import ReactHookFormNumberField, {
  convertNumericInputValue,
  getAllowedKeys,
  parseInput,
} from './ReactHookFormNumberField';
import ReactHookFormProvider from './ReactHookFormProvider';

describe('ReactHookFormNumberField', () => {
  it('should render successfully', () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>
      );
    };

    const { baseElement } = render(<Wrapper />);
    expect(baseElement).toBeTruthy();
    expect(baseElement.querySelector('input').type).toEqual('text');
  });

  it('should render successfully with given value and inputMode numeric', () => {
    const testValue = '123';
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider
          options={{
            defaultValues: {
              test: testValue,
            },
          }}
        >
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>
      );
    };

    const { baseElement } = render(<Wrapper />);
    const { type, value, inputMode } = baseElement.querySelector('input');
    expect(type).toEqual('text');
    expect(inputMode).toEqual('numeric');
    expect(value).toEqual(testValue);
  });

  it('should convert value to integer for inputMode numeric and call onchange', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };

    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input');
    userEvent.type(textField, '10.7');
    expect(textField.getAttribute('value')).toEqual('107');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });

  it('should ignore alphabetic letters', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };

    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input');
    userEvent.type(textField, 'D10A.B7');
    expect(textField.getAttribute('value')).toEqual('107');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });

  it('should not handle more then one . seperator', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };

    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input');
    userEvent.type(textField, '100.123.07');
    expect(textField.getAttribute('value')).toEqual('100.12307');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });

  it('should only allow - at start of inputfield', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };

    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input');
    userEvent.type(textField, '-100.12307-34');
    await waitFor(() =>
      expect(textField.getAttribute('value')).toEqual('-100.1230734'),
    );
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });

  it('should allow negative decimal numbers', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };

    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input');
    userEvent.type(textField, '-.123.45-67');
    await waitFor(() =>
      expect(textField.getAttribute('value')).toEqual('-.1234567'),
    );
    await waitFor(() => expect(mockOnChange).toHaveBeenCalledWith(null));
  });

  it('should handle inputMode decimal and call onchange', async () => {
    const mockOnChange = jest.fn();
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            inputMode="decimal"
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>
      );
    };

    const { container } = render(<Wrapper />);
    const textField = container.querySelector('input');
    fireEvent.change(textField, { target: { value: '10.7' } });
    expect(textField.getAttribute('value')).toEqual('10.7');
    await waitFor(() => expect(mockOnChange).toHaveBeenCalled());
  });

  it('should not have the styling if multiline property is false', async () => {
    const Wrapper = (): React.ReactElement => {
      return (
        <ReactHookFormProvider>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            multiline={false}
            rules={{ required: true }}
          />
        </ReactHookFormProvider>
      );
    };

    const { container } = render(<Wrapper />);
    expect(
      container.querySelector('.MuiTextField-root').hasAttribute('style'),
    ).toBeFalsy();
  });

  it('should set focus when error on submit', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{ required: true }}
            inputMode="decimal"
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { container, getByText, queryByText } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(getByText('This field is required')).toBeTruthy();
      expect(container.querySelector('.Mui-error')).toBeTruthy();
      expect(container.querySelector('.Mui-focused')).toBeTruthy();
    });

    // fix error and test value
    await waitFor(() =>
      fireEvent.change(container.querySelector('input'), {
        target: { value: '-10.23' },
      }),
    );

    await waitFor(() => {
      fireEvent.click(getByText('Validate'));
    });

    expect(testFn).toHaveBeenCalledWith({ test: -10.23 });

    await waitFor(() => {
      expect(queryByText('This field is required')).toBeFalsy();
      expect(container.querySelector('.Mui-error')).toBeFalsy();
    });
  });

  it('should return NaN when passing null as defaultValue of formprovider', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { getByText } = render(
      <ReactHookFormProvider options={{ defaultValues: { test: null } }}>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: NaN });
    });
  });

  it('should return NaN when not passing any initial value', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { getByText } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: NaN });
    });
  });

  it('should return NaN when passing initial value null as prop', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
            defaultValue={null}
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { getByText } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: NaN });
    });
  });

  it('should return NaN when passing initial value NaN as prop', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{}}
            inputMode="decimal"
            defaultValue={NaN}
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { getByText } = render(
      <ReactHookFormProvider options={{ defaultValues: { test: NaN } }}>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: NaN });
    });
  });

  it('should handle required when not passing any defaultValue', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{
              required: true,
            }}
            inputMode="decimal"
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { getByText, container } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(testFn).not.toHaveBeenCalled();
    });

    userEvent.type(container.querySelector('input'), '-.0123.-45-');
    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: -0.012345 });
    });
  });

  it('should handle required when passing defaultValue null in provider', async () => {
    const testFn = jest.fn();
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormNumberField
            name="test"
            label="Test"
            rules={{
              required: true,
            }}
            inputMode="decimal"
          />
          <Button
            onClick={(): void => {
              handleSubmit((formValues) => {
                testFn(formValues);
              })();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { getByText, container } = render(
      <ReactHookFormProvider options={{ defaultValues: { test: null } }}>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(testFn).not.toHaveBeenCalled();
    });

    await waitFor(() => {
      fireEvent.change(container.querySelector('input'), {
        target: { value: 12 },
      });
    });
    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(testFn).toHaveBeenCalledWith({ test: 12 });
    });
  });

  describe('convertNumericInputValue', () => {
    it('should return string if "" or NaN', () => {
      expect(typeof convertNumericInputValue('', 'decimal')).toEqual('string');
      expect(typeof convertNumericInputValue('', 'numeric')).toEqual('string');
      expect(typeof convertNumericInputValue('ff', 'decimal')).toEqual(
        'string',
      );
      expect(typeof convertNumericInputValue('ff', 'numeric')).toEqual(
        'string',
      );
    });

    it('should return number if integer passed and non-empty, non-end on dot or non-NaN string is passed', () => {
      expect(typeof convertNumericInputValue('5', 'numeric')).toEqual('number');
      expect(typeof convertNumericInputValue('500.0', 'numeric')).toEqual(
        'number',
      );
    });

    it('should return string if float is passed in', () => {
      expect(typeof convertNumericInputValue('5', 'decimal')).toEqual('string');
      expect(typeof convertNumericInputValue('500.0', 'decimal')).toEqual(
        'string',
      );
    });
  });

  describe('parseInput', () => {
    it('should return correct value to string', () => {
      expect(parseInput(0)).toEqual('0');
      expect(parseInput(10)).toEqual('10');
      expect(parseInput(123.12)).toEqual('123.12');
      expect(parseInput(NaN as number)).toEqual('');
      expect(parseInput('' as unknown as number)).toEqual('');
      expect(parseInput('some string' as unknown as number)).toEqual('');
      expect(parseInput('' as unknown as number)).toEqual('');
      expect(parseInput(null)).toEqual('');
      expect(parseInput(undefined)).toEqual('');
      expect(parseInput('-')).toEqual('-');
      expect(parseInput('-100')).toEqual('-100');
      expect(parseInput('-123.24')).toEqual('-123.24');
      expect(parseInput('-123.24-')).toEqual('-123.24');
      expect(parseInput('-.123.24-')).toEqual('-.123.24');
    });
  });

  describe('getAllowedKeys', () => {
    it('should return correct allowed keys', () => {
      expect(getAllowedKeys('decimal').includes('.')).toBeTruthy();
      expect(getAllowedKeys('numeric').includes('.')).toBeFalsy();
      expect(getAllowedKeys().includes('.')).toBeFalsy();
      expect(getAllowedKeys('numeric').includes('A')).toBeFalsy();
      expect(getAllowedKeys('numeric').includes('X')).toBeFalsy();
    });
  });
});
