/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { InputAdornment, SxProps, TextField, Theme } from '@mui/material';

import { Controller, useFormContext } from 'react-hook-form';
import moment, { Moment } from 'moment-timezone';
import { DateTimePicker, DateTimePickerProps } from '@mui/lab';
import ReactHookFormFormControl from './ReactHookFormFormControl';
import { Rules } from './types';
import { getErrors } from './formUtils';

// TODO: Place this default setting somewhere more appropriate, should only need to fire once https://gitlab.com/opengeoweb/opengeoweb/-/issues/331
moment.tz.setDefault('Etc/GMT-0');

export const getFormattedValue = (value: Moment | string): string | Moment => {
  const isMoment = moment(value).isValid();
  return isMoment ? moment.utc(value).format() : value;
};

type ReactHookKeyboardDateTimePickerProps = Partial<DateTimePickerProps> & {
  rules: Rules;
  defaultNullValue?: string;
  value?: string;
  onChange?: (value) => void;
  name: string;
  helperText?: string;
  label?: string;
  format?: string;
  sx?: SxProps<Theme>;
};

const ReactHookKeyboardDateTimePicker: React.FC<ReactHookKeyboardDateTimePickerProps> =
  ({
    name,
    rules,
    disabled,
    label = 'Select date and time',
    format = 'YYYY/MM/DD HH:mm',
    openTo = 'hours',
    defaultNullValue = null,
    helperText = '',
    onChange = (): void => null,
    className,
    sx,
    ...otherProps
  }: ReactHookKeyboardDateTimePickerProps) => {
    const inputRef = React.useRef(null);
    const { control, errors: formErrors, getValues } = useFormContext();
    const errors = getErrors(name, formErrors);

    return (
      <ReactHookFormFormControl
        disabled={disabled}
        errors={errors}
        className={className}
        sx={sx}
      >
        <Controller
          render={(_props): React.ReactElement => (
            <DateTimePicker
              desktopModeMediaQuery="@media (min-width: 720px)"
              ampm={false}
              mask="____/__/__ __:__"
              inputFormat={format}
              label={label}
              value={_props.value}
              openTo={openTo}
              disabled={disabled}
              onChange={(value: Moment): void => {
                _props.onChange(getFormattedValue(value));
                onChange(getFormattedValue(value));
              }}
              inputRef={inputRef}
              InputAdornmentProps={{ position: 'start' }}
              InputProps={{
                endAdornment: (
                  <InputAdornment style={{ paddingTop: '16px' }} position="end">
                    UTC
                  </InputAdornment>
                ),
              }}
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              // eslint-disable-next-line react/jsx-no-duplicate-props
              inputProps={
                disabled
                  ? {
                      placeholder: '',
                    }
                  : undefined
              }
              renderInput={({ error, ...props }): React.ReactElement => (
                <TextField
                  helperText={helperText}
                  error={!!errors}
                  name={name}
                  data-testid={otherProps['data-testid']}
                  variant="filled"
                  {...props}
                />
              )}
              {...otherProps}
            />
          )}
          name={name}
          control={control}
          rules={rules}
          defaultValue={getValues(name) || defaultNullValue}
          onFocus={(): void => {
            if (inputRef.current) {
              inputRef.current.disabled = false;
              inputRef.current?.focus();
            }
          }}
        />
      </ReactHookFormFormControl>
    );
  };
export default ReactHookKeyboardDateTimePicker;
