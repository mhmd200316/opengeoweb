/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { Button } from '@mui/material';
import moment from 'moment';
import { useFormContext } from 'react-hook-form';

import ReactHookFormDateTime, {
  getFormattedValue,
} from './ReactHookFormDateTime';
import ReactHookFormProvider from './ReactHookFormProvider';

describe('components/ReactHookFormDateTime', () => {
  describe('ReactHookFormDateTime component', () => {
    it('should render successfully', () => {
      const { baseElement } = render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(baseElement).toBeTruthy();
    });

    it('should show UTC in the field', () => {
      const { getByText } = render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(getByText('UTC')).toBeTruthy();
    });

    it('should be possible to set a date by typing in the field', async () => {
      const mockOnChange = jest.fn();

      const { getByRole } = render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>,
      );
      const dateInput = getByRole('textbox');
      fireEvent.change(dateInput, {
        target: { value: '2021/02/18 12:37' },
      });

      await waitFor(() =>
        expect(mockOnChange).toHaveBeenLastCalledWith('2021-02-18T12:37:00Z'),
      );
    });

    it('should open the date and time pickers and should be possible to choose a date', async () => {
      const mockOnChange = jest.fn();
      const { getByRole, getByTestId, getByText } = render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
            onChange={mockOnChange}
          />
        </ReactHookFormProvider>,
      );
      const datePicker = getByRole('button');
      fireEvent.click(datePicker);

      // hours tab should be open
      const timeIcon = getByTestId('TimeIcon').parentElement;
      const dateRangeIcon = getByTestId('DateRangeIcon').parentElement;

      // hours tab should be open
      expect(timeIcon.getAttribute('aria-selected')).toEqual('true');
      expect(dateRangeIcon.getAttribute('aria-selected')).toEqual('false');

      // hours should be visible
      expect(getByText('12')).toBeTruthy();
      // switch to minutes
      fireEvent.click(getByTestId('ArrowRightIcon'));
      // minutes should be visible
      expect(getByText('55')).toBeTruthy();

      // switch to date tab
      fireEvent.click(getByTestId('DateRangeIcon'));

      expect(timeIcon.getAttribute('aria-selected')).toEqual('false');
      expect(dateRangeIcon.getAttribute('aria-selected')).toEqual('true');

      // Dates should be visible
      expect(getByText('F')).toBeTruthy(); // F from Friday
      // Choose first day of the month
      fireEvent.click(getByText('1'));
      // Close the datepicker
      fireEvent.click(datePicker);
      await waitFor(() => expect(mockOnChange).toHaveBeenCalled());
    });

    it('should be disabled', () => {
      const { container } = render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
            disabled
          />
        </ReactHookFormProvider>,
      );
      expect(container.querySelector('.Mui-disabled')).toBeTruthy();
    });

    it('should show default label and value', () => {
      const { getAllByText, getByRole } = render(
        <ReactHookFormProvider>
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(getAllByText('Select date and time')).toBeTruthy();
      const dateInput = getByRole('textbox');
      expect(dateInput.getAttribute('value')).toEqual('');
    });

    it('should show given label and value', () => {
      const { getAllByText, getByRole } = render(
        <ReactHookFormProvider
          options={{
            defaultValues: {
              testDateTime: '2021-01-01T12:00:00Z',
            },
          }}
        >
          <ReactHookFormDateTime
            name="testDateTime"
            label="Test label"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      expect(getAllByText('Test label')).toBeTruthy();
      const dateInput = getByRole('textbox');
      expect(dateInput.getAttribute('value')).toEqual('2021/01/01 12:00');
    });

    it('should show error state on error', async () => {
      const { container, getByText, getByRole } = render(
        <ReactHookFormProvider
          options={{
            mode: 'onChange',
            reValidateMode: 'onChange',
            defaultValues: {
              testDateTime: '2021-01-01T12:00:00Z',
            },
          }}
        >
          <ReactHookFormDateTime
            name="testDateTime"
            rules={{ required: true }}
          />
        </ReactHookFormProvider>,
      );
      const dateInput = getByRole('textbox');
      fireEvent.input(dateInput, {
        target: { value: '' },
      });
      await waitFor(() => {
        expect(dateInput.getAttribute('value')).toEqual('');
        expect(getByText('This field is required')).toBeTruthy();
        expect(container.querySelector('.Mui-error')).toBeTruthy();
      });
    });

    it('should set focus when error on submit', async () => {
      const Wrapper = (): React.ReactElement => {
        const { handleSubmit } = useFormContext();
        return (
          <>
            <ReactHookFormDateTime
              name="testDateTime"
              rules={{ required: true }}
            />
            <Button
              onClick={(): void => {
                handleSubmit(() => {})();
              }}
            >
              Validate
            </Button>
          </>
        );
      };

      const { container, getByText } = render(
        <ReactHookFormProvider>
          <Wrapper />
        </ReactHookFormProvider>,
      );

      fireEvent.click(getByText('Validate'));

      await waitFor(() => {
        expect(getByText('This field is required')).toBeTruthy();
        expect(container.querySelector('.Mui-error')).toBeTruthy();
        expect(container.querySelector('.Mui-focused')).toBeTruthy();
      });
    });
  });

  describe('getFormattedValue', () => {
    it('should return formatted date when valid date', () => {
      const test = moment.utc(new Date());

      expect(getFormattedValue(test).constructor).toEqual(String);
      expect(getFormattedValue(test) instanceof moment).toBeFalsy();
    });

    it('should return moment object when invalid date', () => {
      jest.spyOn(console, 'warn').mockImplementation();
      const test = moment('test');
      expect(getFormattedValue(test) instanceof moment).toBeTruthy();
      expect(console.warn).toHaveBeenCalledWith(
        expect.stringContaining(
          'value provided is not in a recognized RFC2822 or ISO format',
        ),
      );
    });
  });
});
