/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { InputLabel, RadioGroup, RadioGroupProps } from '@mui/material';
import { Controller, useFormContext } from 'react-hook-form';

import ReactHookFormFormControl from './ReactHookFormFormControl';
import { Rules } from './types';
import { getErrors } from './formUtils';

type ReactHookFormRadioGroupProps = Partial<RadioGroupProps> & {
  rules: Rules;
  label?: string;
  disabled?: boolean;
};

const ReactHookFormRadioGroup: React.FC<ReactHookFormRadioGroupProps> = ({
  name,
  label,
  defaultValue = '',
  children,
  rules,
  disabled = false,
  onChange = (): void => null,
  ...otherProps
}: ReactHookFormRadioGroupProps) => {
  const labelId = `${name}-label`;
  const inputRef = React.useRef(null);

  const { control, errors: formErrors } = useFormContext();
  const errors = getErrors(name, formErrors);

  return (
    <ReactHookFormFormControl disabled={disabled} errors={errors}>
      <InputLabel id={labelId}>{label}</InputLabel>
      <Controller
        render={(_props): React.ReactElement => (
          <RadioGroup
            value={_props.value}
            onChange={(changeEvent): void => {
              _props.onChange(changeEvent.target.value);
              onChange(changeEvent, changeEvent.target.value);
            }}
            ref={inputRef}
            name={name}
            {...otherProps}
          >
            {children}
          </RadioGroup>
        )}
        name={name}
        control={control}
        defaultValue={defaultValue}
        rules={rules}
        onFocus={(): void => {
          // move focus to first radio on error
          const firstElement = inputRef.current.querySelector('input');
          if (firstElement) {
            firstElement.disabled = false;
            firstElement?.focus();
          }
        }}
      />
    </ReactHookFormFormControl>
  );
};
export default ReactHookFormRadioGroup;
