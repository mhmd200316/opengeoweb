/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { Button, Typography } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import { ReactHookFormHiddenInput, ReactHookFormProvider } from '.';

export default {
  title: 'ReactHookForm/Hidden Input',
};

const ReactHookFormHiddenInputWrapper = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();
  const [value, setValue] = React.useState('');

  return (
    <ThemeWrapperOldTheme>
      <Typography>
        Click submit to see the hidden input value that would get submitted
      </Typography>
      <ReactHookFormHiddenInput name="hide" defaultValue="hidden value" />
      <Button
        style={{ marginBottom: '20px' }}
        variant="contained"
        color="secondary"
        onClick={(): void => {
          handleSubmit((data) => setValue(data['hide']))();
        }}
      >
        Submit
      </Button>
      {value && <Typography>{value}</Typography>}
    </ThemeWrapperOldTheme>
  );
};

export const HiddenInput = (): React.ReactElement => (
  <ReactHookFormProvider>
    <ReactHookFormHiddenInputWrapper />
  </ReactHookFormProvider>
);
