/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { Button, FormControlLabel, MenuItem, Radio } from '@mui/material';
import * as React from 'react';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';

import { useFormContext } from 'react-hook-form';
import {
  ReactHookFormDateTime,
  ReactHookFormProvider,
  isValidDate,
  ReactHookFormRadioGroup,
  ReactHookFormSelect,
  ReactHookFormTextField,
  ReactHookFormNumberField,
  defaultFormOptions,
} from '.';

export default {
  title: 'ReactHookForm/Form Provider',
};

const ReactHookFormDateTimeWrapper = (): React.ReactElement => {
  const { handleSubmit, watch } = useFormContext();

  return (
    <ThemeWrapperOldTheme>
      <div style={{ width: '260px' }}>
        <div style={{ marginBottom: '30px' }}>
          <ReactHookFormDateTime
            name="dateTime-A"
            rules={{
              required: true,
              validate: {
                isValidDate,
              },
            }}
          />
        </div>
        <div style={{ marginBottom: '30px' }}>
          <ReactHookFormTextField
            name="textdemo"
            label="Text"
            rules={{ required: true }}
          />
        </div>
        <div style={{ marginBottom: '30px' }}>
          <ReactHookFormNumberField
            name="numberdemo"
            label="Decimal number"
            inputMode="decimal"
            defaultValue={12.239}
            rules={{ required: true }}
          />
        </div>
        <div style={{ marginBottom: '30px' }}>
          <ReactHookFormSelect
            name="options"
            label="options"
            rules={{
              required: true,
            }}
          >
            <MenuItem value="">-</MenuItem>
            <MenuItem value="single" key="single">
              single
            </MenuItem>
            <MenuItem value="multiple" key="multiple">
              multiple
            </MenuItem>
          </ReactHookFormSelect>
        </div>
        {watch('options') === 'multiple' && (
          <div style={{ marginBottom: '30px' }}>
            <ReactHookFormTextField
              name="extraInfo"
              label="extra info"
              rules={{ required: true }}
            />
          </div>
        )}
        <div style={{ marginBottom: '30px' }}>
          <ReactHookFormRadioGroup
            name="radio2"
            rules={{
              required: true,
              validate: {
                eatApples: (value: string): boolean | string =>
                  value === 'apples' || 'You should eat more apples',
              },
            }}
          >
            <FormControlLabel
              value="bananas"
              control={<Radio />}
              label="Bananas"
            />
            <FormControlLabel
              value="apples"
              control={<Radio />}
              label="Apples"
            />
          </ReactHookFormRadioGroup>
        </div>
        <Button
          variant="contained"
          color="secondary"
          onClick={(): void => {
            handleSubmit((formValues) => {
              // eslint-disable-next-line no-console
              console.log('submit value', formValues);
            })();
          }}
        >
          Validate
        </Button>
      </div>
    </ThemeWrapperOldTheme>
  );
};

export const FormProvider = (): React.ReactElement => (
  <ReactHookFormProvider
    options={{
      ...defaultFormOptions,
      defaultValues: {
        'dateTime-A': '2021-01-01T12:00:00Z',
      },
    }}
  >
    <ReactHookFormDateTimeWrapper />
  </ReactHookFormProvider>
);
