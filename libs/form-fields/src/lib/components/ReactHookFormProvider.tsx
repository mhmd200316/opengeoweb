/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';

import { FormProvider, useForm, UseFormOptions } from 'react-hook-form';
import AdapterMoment from '@mui/lab/AdapterMoment';
import LocalizationProvider from '@mui/lab/LocalizationProvider';

interface WrapperProps {
  children: React.ReactNode;
  options?: UseFormOptions;
}

export const defaultFormOptions = {
  mode: 'onChange',
  reValidateMode: 'onChange',
} as UseFormOptions;

const ReactHookFormProvider: React.FC<WrapperProps> = ({
  children,
  options = defaultFormOptions,
}: WrapperProps) => {
  const formMethods = useForm(options);
  return <FormProvider {...formMethods}>{children}</FormProvider>;
};

const ReactHookFormProviderWrapper: React.FC<WrapperProps> = ({
  children,
  options = defaultFormOptions,
}: WrapperProps) => (
  <LocalizationProvider dateAdapter={AdapterMoment}>
    <ReactHookFormProvider options={options}>{children}</ReactHookFormProvider>
  </LocalizationProvider>
);

export default ReactHookFormProviderWrapper;
