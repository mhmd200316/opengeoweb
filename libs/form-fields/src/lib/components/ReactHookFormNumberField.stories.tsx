/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { Typography, Button } from '@mui/material';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormProvider, ReactHookFormNumberField } from '.';
import { isInteger, isNumeric, containsNoCommas } from './utils';

export default {
  title: 'ReactHookForm/Number Field',
};

const ReactHookFormNumberFieldWrapped = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <ThemeWrapperOldTheme>
      <div style={{ width: '300px' }}>
        <div style={{ marginBottom: '30px' }}>
          <Typography>Inputmode Decimal</Typography>
          <ReactHookFormNumberField
            name="decimaldemo"
            label="Decimal with validation"
            rules={{
              required: true,
              validate: {
                isNumeric,
                containsNoCommas,
              },
            }}
            inputMode="decimal"
          />
        </div>
        <div style={{ marginBottom: '30px' }}>
          <ReactHookFormNumberField
            name="decimaldemo2"
            label="Decimal defaultValue from provider"
            rules={{}}
            inputMode="decimal"
          />
        </div>
        <div style={{ marginBottom: '30px' }}>
          <ReactHookFormNumberField
            name="decimaldemo3"
            label="Decimal defaultValue NaN"
            rules={{}}
            inputMode="decimal"
          />
        </div>
        <div style={{ marginBottom: '30px' }}>
          <ReactHookFormNumberField
            name="decimaldemo4"
            label="Decimal no defaultValue, required"
            rules={{
              required: true,
            }}
            inputMode="decimal"
          />
        </div>
        <div style={{ marginBottom: '30px' }}>
          <Typography>Inputmode Numeric</Typography>
          <ReactHookFormNumberField
            name="numericdemo"
            label="Numeric"
            rules={{
              required: true,
              validate: {
                isInteger,
              },
            }}
            defaultValue={12}
          />
        </div>
        <Button
          variant="contained"
          color="secondary"
          onClick={(): void => {
            handleSubmit((formData) => {
              // eslint-disable-next-line no-console
              console.log(formData);
            })();
          }}
        >
          Validate
        </Button>
      </div>
    </ThemeWrapperOldTheme>
  );
};

export const NumberField = (): React.ReactElement => (
  <ReactHookFormProvider
    options={{
      defaultValues: {
        decimaldemo: null,
        decimaldemo2: -13.45,
        decimaldemo3: NaN,
      },
    }}
  >
    <ReactHookFormNumberFieldWrapped />
  </ReactHookFormProvider>
);

const NumberFieldSnapshot = (): React.ReactElement => (
  <ThemeWrapperOldTheme>
    <div style={{ width: '300px' }}>
      <div style={{ marginBottom: '30px' }}>
        <ReactHookFormNumberField
          name="emptydemo"
          label="Empty field"
          rules={{
            required: true,
            validate: {
              isNumeric,
              containsNoCommas,
            },
          }}
          inputMode="decimal"
        />
      </div>
      <div style={{ marginBottom: '30px' }}>
        <ReactHookFormNumberField
          name="disableddemo"
          label="Disabled field"
          rules={{}}
          disabled
          inputMode="decimal"
          defaultValue={12}
        />
      </div>
      <div style={{ marginBottom: '30px' }}>
        <ReactHookFormNumberField
          name="decimaldemo"
          label="Decimal field"
          rules={{}}
          inputMode="decimal"
          defaultValue={-13.45}
        />
      </div>
      <ReactHookFormNumberField
        name="numericdemo"
        label="Numeric field"
        rules={{
          required: true,
          validate: {
            isInteger,
          },
        }}
        defaultValue={12}
      />
    </div>
  </ThemeWrapperOldTheme>
);

export const NumberFieldSnapshotWrapped = (): React.ReactElement => (
  <ReactHookFormProvider>
    <NumberFieldSnapshot />
  </ReactHookFormProvider>
);

NumberFieldSnapshotWrapped.storyName = 'Number Field (takeSnapshot)';
