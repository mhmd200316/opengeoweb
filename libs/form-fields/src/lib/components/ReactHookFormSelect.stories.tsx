/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { Button, MenuItem } from '@mui/material';
import * as React from 'react';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import { useFormContext } from 'react-hook-form';

import { ReactHookFormSelect, ReactHookFormProvider } from '.';

export default {
  title: 'ReactHookForm/Select',
};

const SelectBoxes = (): React.ReactElement => (
  <div style={{ width: '100px' }}>
    <div style={{ marginBottom: '30px' }}>
      <ReactHookFormSelect
        name="select1"
        label="Unit"
        rules={{
          required: true,
        }}
      >
        <MenuItem value="">-</MenuItem>
        <MenuItem value="FL" key="FL">
          FL
        </MenuItem>
        <MenuItem value="M" key="M">
          M
        </MenuItem>
        <MenuItem value="FT" key="FT">
          FT
        </MenuItem>
      </ReactHookFormSelect>
    </div>
    <div style={{ marginBottom: '30px' }}>
      <ReactHookFormSelect
        name="select2"
        label="Unit"
        defaultValue="FT"
        rules={{
          required: true,
        }}
      >
        <MenuItem value="FL" key="FL">
          FL
        </MenuItem>
        <MenuItem value="M" key="M">
          M
        </MenuItem>
        <MenuItem value="FT" key="FT">
          FT
        </MenuItem>
      </ReactHookFormSelect>
    </div>
    <div style={{ marginBottom: '30px' }}>
      <ReactHookFormSelect
        name="select3"
        label="Unit"
        defaultValue="FT"
        disabled
        rules={{
          required: true,
        }}
      >
        <MenuItem value="FL" key="FL">
          FL
        </MenuItem>
        <MenuItem value="M" key="M">
          M
        </MenuItem>
        <MenuItem value="FT" key="FT">
          FT
        </MenuItem>
      </ReactHookFormSelect>
    </div>
  </div>
);

const ReactHookFormSelectWrapper = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <ThemeWrapperOldTheme>
      <SelectBoxes />
      <Button
        variant="contained"
        color="secondary"
        onClick={(): void => {
          handleSubmit(() => {})();
        }}
      >
        Validate
      </Button>
    </ThemeWrapperOldTheme>
  );
};

export const Select = (): React.ReactElement => (
  <ReactHookFormProvider>
    <ReactHookFormSelectWrapper />
  </ReactHookFormProvider>
);

export const SelectSnapshot = (): React.ReactElement => (
  <ReactHookFormProvider>
    <ThemeWrapperOldTheme>
      <SelectBoxes />
    </ThemeWrapperOldTheme>
  </ReactHookFormProvider>
);

SelectSnapshot.storyName = 'Select (takeSnapshot)';
