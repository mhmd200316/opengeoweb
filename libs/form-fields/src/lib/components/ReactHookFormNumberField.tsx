/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { TextField, TextFieldProps } from '@mui/material';
import { Controller, useFormContext } from 'react-hook-form';

import ReactHookFormFormControl from './ReactHookFormFormControl';
import { Rules } from './types';
import { getErrors } from './formUtils';

type InputMode = 'numeric' | 'decimal';

export const convertNumericInputValue = (
  value: string,
  inputMode: InputMode,
): number | string => {
  if (value !== '' && !isNaN(parseFloat(value))) {
    // float is returned as string, option 'valueAsNumber: true' will take care of the parsing
    return inputMode === 'numeric' ? parseInt(value, 10) : value;
  }
  return value;
};

export const parseInput = (value: number | string): string => {
  // allow '-' at the start of the value
  if (typeof value === 'string' && value.includes('-')) {
    return value.split('-').reduce((list, el) => {
      if (!el.length && !list.length) {
        return '-';
      }
      return list.concat(el);
    }, '');
  }
  // return value when '.' is given
  if (typeof value === 'string' && value.includes('.')) {
    return value;
  }

  return value == null || isNaN(value as number) ? '' : value.toString();
};

export const getAllowedKeys = (inputMode: InputMode = 'numeric'): string[] => {
  const allowedKeys = [
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '-',
    'Backspace',
    'ArrowRight',
    'ArrowLeft',
    'Tab',
    'Home',
    'End',
    'PageDown',
    'PageUp',
    'Delete',
    ...(inputMode === 'decimal' ? ['.'] : []),
  ];
  return allowedKeys;
};

type ReactHookFormNumberFieldProps = Partial<TextFieldProps> & {
  rules: Rules;
  inputMode?: InputMode;
};

const ReactHookFormNumberField: React.FC<ReactHookFormNumberFieldProps> = ({
  name,
  label,
  defaultValue = '',
  rules,
  disabled = false,
  inputMode = 'numeric',
  helperText = '',
  className,
  sx,
  onChange = (): void => null,
  ...otherProps
}: ReactHookFormNumberFieldProps) => {
  const inputRef = React.useRef(null);
  const { control, errors: formErrors } = useFormContext();
  const errors = getErrors(name, formErrors);
  const validationRules = {
    ...rules,
    setValueAs: (value: string): number => parseFloat(value),
  };
  const allowedKeyes = getAllowedKeys(inputMode);

  const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>): void => {
    if (
      !allowedKeyes.includes(event.key) ||
      // only allow one seperator
      (event.key === '.' &&
        (event.target as HTMLInputElement).value.includes('.')) ||
      // only allow one -
      (event.key === '-' &&
        (event.target as HTMLInputElement).value.includes('-'))
    ) {
      event.preventDefault();
    }
  };

  return (
    <ReactHookFormFormControl
      className={className}
      sx={sx}
      disabled={disabled}
      errors={errors}
    >
      <Controller
        render={(_props): React.ReactElement => {
          return (
            <TextField
              label={label}
              error={!!errors}
              helperText={helperText}
              inputMode={inputMode}
              value={parseInput(_props.value)}
              variant="filled"
              type="text"
              inputProps={{ inputMode }}
              name={name}
              onChange={(evt: React.ChangeEvent<HTMLInputElement>): void => {
                _props.onChange(
                  convertNumericInputValue(evt.target.value, inputMode),
                );
                onChange(null);
              }}
              onKeyDown={onKeyDown}
              disabled={disabled}
              inputRef={inputRef}
              {...otherProps}
            />
          );
        }}
        name={name}
        control={control}
        defaultValue={defaultValue}
        rules={validationRules}
        disabled={disabled}
        onFocus={(): void => {
          inputRef.current.disabled = false;
          inputRef.current?.focus();
        }}
      />
    </ReactHookFormFormControl>
  );
};
export default ReactHookFormNumberField;
