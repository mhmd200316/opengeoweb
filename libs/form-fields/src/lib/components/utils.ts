/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import moment, { Moment } from 'moment';
import { isEqual } from 'lodash';

export const errorMessages = {
  required: 'This field is required',
  isValidDate: 'Not a valid date',
  isAfter: 'Date should not be before',
  isXHoursBefore: 'Date is too far in the past',
  isXHoursAfter: 'Date is too far in the future',
  isBefore: 'Timestamp too late',
  minLevel: 'The minimum level is 1',
  isLatitude:
    'Invalid latitude, expected number with maximum two decimal degrees between -90.00 and 90.00',
  isLongitude:
    'Invalid longitude, expected number with maximum two decimal degrees between -180.00 and 180.00',
  isValidMax: 'The maximum value is exceeded',
  isLevelLower: 'The lower level has to be below the upper level',
  isNonOrBothCoordinates: 'Please enter both coordinates',
  isInteger: 'Invalid entry, enter a non-decimal number',
  isNumeric: 'Invalid entry, enter a numeric value',
  containsNoCommas: 'Please use a dot (.) as decimal separator',
  minVisibility: 'The minimum visibility is 0',
  maxVisibility: 'The maximum visibility is 4900',
};

const regexMaxTwoDecimals = /^\s*-?\d+(\.\d{1,2})?\s*$/;

// validations
export const isEmpty = (value: number | string): boolean => {
  if (
    (typeof value === 'string' && !value.trim().length) ||
    value === null ||
    value === undefined
  ) {
    return true;
  }
  return false;
};

export const isValidDate = (value: Moment | string): boolean =>
  value ? moment(value).isValid() : true;

export const isBefore = (ownDate: string, referenceDate: string): boolean =>
  ownDate ? moment.utc(ownDate) < moment.utc(referenceDate) : true;

export const isAfter = (ownDate: string, otherDate: string): boolean =>
  ownDate ? moment.utc(ownDate).isAfter(moment.utc(otherDate)) : true;

export const isBetween = (
  ownDate: string,
  otherDateStart: string,
  otherDateEnd: string,
): boolean =>
  moment.utc(ownDate) >= moment.utc(otherDateStart) &&
  moment.utc(ownDate) <= moment.utc(otherDateEnd);

export const isXHoursBefore = (
  ownDate: string,
  otherDate: string,
  hours = 4,
): boolean => {
  if (isEmpty(ownDate) || isEmpty(otherDate)) {
    return true;
  }
  const duration = moment.duration(moment.utc(ownDate).diff(otherDate));
  const inHours = duration.asHours();
  return inHours >= -hours;
};

export const isXHoursAfter = (
  ownDate: string,
  otherDate: string,
  hours = 4,
): boolean => {
  if (isEmpty(ownDate) || isEmpty(otherDate)) {
    return true;
  }
  const duration = moment.duration(moment.utc(ownDate).diff(otherDate));
  const inHours = duration.asHours();
  return inHours <= hours;
};

export const isLatitude = (lat: number): boolean => {
  if (!lat) return true;
  return (
    isFinite(lat) &&
    Math.abs(lat) <= 90 &&
    regexMaxTwoDecimals.test(lat.toString())
  );
};

export const isLongitude = (lng: number): boolean => {
  if (!lng) return true;
  return (
    isFinite(lng) &&
    Math.abs(lng) <= 180 &&
    lng >= -180 &&
    regexMaxTwoDecimals.test(lng.toString())
  );
};

export const isValidMax = (value: number, maxValue: number): boolean => {
  if (isEmpty(value)) {
    return true;
  }
  return !(value > maxValue);
};

export const isValidMin = (value: number, minValue: number): boolean => {
  if (isEmpty(value)) {
    return true;
  }
  return !(value < minValue);
};

export const hasValidGeometry = (
  geojson: GeoJSON.FeatureCollection,
): boolean => {
  if (
    !geojson ||
    !geojson.features ||
    !geojson.features.length ||
    !geojson.features[0].geometry ||
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    !geojson.features[0].geometry.coordinates ||
    !geojson.features[0].geometry.type
  ) {
    return false;
  }
  return true;
};

export const isValidGeoJsonCoordinates = (
  geojson: GeoJSON.FeatureCollection,
): boolean => {
  const hasGeometry = hasValidGeometry(geojson);
  if (!hasGeometry) {
    return false;
  }

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const { coordinates, type } = geojson.features[0].geometry;
  // For type POINT coordinates is an array of 2
  if (coordinates.length === 2 && type === 'Point') {
    return true;
  }
  // For type POLYGON coordinates is an array of arrays
  if (
    coordinates.length === 1 &&
    coordinates[0].length > 0 &&
    type === 'Polygon'
  ) {
    return true;
  }
  return false;
};

export const isMaximumOneDrawing = (
  geojson: GeoJSON.FeatureCollection,
): boolean => {
  const hasGeometry = hasValidGeometry(geojson);
  if (!hasGeometry) {
    return true; // no need to check further when there is no drawing yet
  }

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const { coordinates, type } = geojson.features[0].geometry;
  // For type POLYGON only one array of coordinates is allowed
  if (type === 'Polygon' && coordinates.length > 1) {
    return false;
  }

  return true;
};

export const hasIntersectionWithFIR = (
  geojson: GeoJSON.FeatureCollection,
  intersection: GeoJSON.FeatureCollection,
): boolean => {
  if (
    !hasValidGeometry(geojson) ||
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    !geojson.features[0].geometry.coordinates.length ||
    (geojson.features[0].geometry.type === 'Polygon' &&
      !geojson.features[0].geometry.coordinates[0].length)
  ) {
    return true; // no need to check further when there is no drawing yet
  }

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const { coordinates } = intersection.features[0].geometry;
  if (coordinates.length > 0 && coordinates[0].length === 0) {
    return false;
  }
  return true;
};

export const hasMaxFeaturePoints = (
  geojson: GeoJSON.FeatureCollection,
  maxPoints = 7,
): boolean => {
  const hasGeometry = hasValidGeometry(geojson);

  if (!hasGeometry) {
    return false;
  }

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const { coordinates, type } = geojson.features[0].geometry;
  if (type === 'Polygon' && coordinates[0].length > maxPoints) {
    return true;
  }

  return false;
};

export const hasMulitpleIntersections = (
  geojson: GeoJSON.FeatureCollection,
): boolean => {
  const hasGeometry = hasValidGeometry(geojson);
  if (!hasGeometry) {
    return false;
  }
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const { coordinates, type } = geojson.features[0].geometry;

  if (type === 'MultiPolygon' || (coordinates.length > 1 && type !== 'Point')) {
    return true;
  }

  return false;
};

export const isGeometryDirty = (
  geoJSON: GeoJSON.FeatureCollection,
  formGeoJSON: GeoJSON.FeatureCollection,
): boolean => {
  if (
    geoJSON === (null || undefined) ||
    formGeoJSON === (null || undefined) ||
    !hasValidGeometry(geoJSON) ||
    !hasValidGeometry(formGeoJSON)
  ) {
    return false;
  }

  return !isEqual(
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    geoJSON.features[0].geometry.coordinates,
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    formGeoJSON.features[0].geometry.coordinates,
  );
};

export const isInteger = (value: number): boolean => {
  if (isEmpty(value)) {
    return true;
  }
  return Number.isInteger(value);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
export const isNumeric = (value: any): boolean =>
  !isNaN(parseFloat(value)) && isFinite(value);

export const isNonOrBothCoordinates = (
  coordinate: number,
  otherCoordinate: number,
): boolean => {
  if ((coordinate && otherCoordinate) || (!coordinate && !otherCoordinate)) {
    return true;
  }
  if (coordinate) {
    return true;
  }

  return false;
};
export const containsNoCommas = (val: string | number): boolean => {
  if (typeof val === 'string' && val.includes(',')) {
    return false;
  }

  return true;
};
