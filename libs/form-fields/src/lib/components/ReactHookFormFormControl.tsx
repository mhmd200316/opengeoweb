/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';

import { FormControl, FormHelperText, FormControlProps } from '@mui/material';
import { FieldErrors } from 'react-hook-form';
import { errorMessages } from './utils';

interface ReactHookFormFormControlProps extends FormControlProps {
  errors?: FieldErrors;
}

const ReactHookFormFormControl: React.FC<ReactHookFormFormControlProps> = ({
  children,
  errors,
  ...props
}: ReactHookFormFormControlProps) => {
  return (
    <FormControl fullWidth {...props} error={!!errors}>
      {children}
      {errors && (
        <FormHelperText variant="filled">
          {errors.message || errorMessages[errors.type]}
        </FormHelperText>
      )}
    </FormControl>
  );
};
export default ReactHookFormFormControl;
