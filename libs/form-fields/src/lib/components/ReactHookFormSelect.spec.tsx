/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import { Button, MenuItem } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import ReactHookFormSelect from './ReactHookFormSelect';
import ReactHookFormProvider from './ReactHookFormProvider';

describe('ReactHookFormSelect', () => {
  const Wrapper = (props): React.ReactElement => {
    return (
      <ReactHookFormProvider>
        <ReactHookFormSelect name="unit" rules={{ required: true }} {...props}>
          <MenuItem value="FL">FL</MenuItem>
          <MenuItem value="M">M</MenuItem>
        </ReactHookFormSelect>
      </ReactHookFormProvider>
    );
  };

  it('should render successfully', () => {
    const { baseElement } = render(<Wrapper />);
    expect(baseElement).toBeTruthy();
  });

  it('should be disabled', () => {
    const { container } = render(<Wrapper disabled />);
    expect(container.querySelector('.Mui-disabled')).toBeTruthy();
  });

  it('should show default value empty', () => {
    const { container } = render(<Wrapper />);
    const selectInput = container.querySelector('[name=unit]');
    expect(selectInput.getAttribute('value')).toEqual('');
  });

  it('should show given label and value', () => {
    const { getByText, container } = render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            unit: 'FL',
          },
        }}
      >
        <ReactHookFormSelect
          name="unit"
          label="Select unit"
          rules={{ required: true }}
        >
          <MenuItem value="FL">FL</MenuItem>
          <MenuItem value="M">M</MenuItem>
        </ReactHookFormSelect>
      </ReactHookFormProvider>,
    );
    expect(getByText('Select unit')).toBeTruthy();
    const selectInput = container.querySelector('[name=unit]');
    expect(selectInput.getAttribute('value')).toEqual('FL');
  });

  it('should select an option', async () => {
    const { container, getByRole, findByText } = render(<Wrapper />);
    const selectInput = container.querySelector('[name=unit]');
    expect(selectInput.getAttribute('value')).toEqual('');

    fireEvent.mouseDown(getByRole('button'));
    const menuItem = await findByText('M');
    fireEvent.click(menuItem);

    await waitFor(() => {
      expect(selectInput.getAttribute('value')).toEqual('M');
      expect(
        container.querySelector('[name=unit]').previousElementSibling
          .textContent,
      ).toEqual('M');
    });
  });

  it('should show error state and set focus when error on submit', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <ReactHookFormSelect name="unit" rules={{ required: true }}>
            <MenuItem value="FL">FL</MenuItem>
            <MenuItem value="M">M</MenuItem>
          </ReactHookFormSelect>
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    const { container, getByText } = render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );

    fireEvent.click(getByText('Validate'));

    await waitFor(() => {
      expect(getByText('This field is required')).toBeTruthy();
      expect(container.querySelector('.Mui-error')).toBeTruthy();
      expect(container.querySelector('.Mui-focused')).toBeTruthy();
    });
  });
});
