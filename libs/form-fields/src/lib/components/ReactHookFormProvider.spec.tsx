/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { useFormContext } from 'react-hook-form';

import ReactHookFormProvider from './ReactHookFormProvider';
import ReactHookFormTextField from './ReactHookFormTextField';

type TestFormValues = {
  field1: string;
  field2?: string;
};

interface TestComponentProps {
  onSubmitForm: (data: TestFormValues) => void;
}

const TestComponent: React.FC<TestComponentProps> = ({
  onSubmitForm,
}: TestComponentProps) => {
  const { handleSubmit, watch } = useFormContext();

  return (
    <>
      <ReactHookFormTextField
        name="field1"
        label="Text"
        rules={{ required: true }}
      />
      {watch('field1') === 'test' && (
        <ReactHookFormTextField name="field2" label="Text" rules={{}} />
      )}

      <button
        type="submit"
        onClick={(): void => {
          handleSubmit((formValues: TestFormValues) => {
            onSubmitForm(formValues);
          })();
        }}
      >
        Send
      </button>
    </>
  );
};

describe('ReactHookFormProvider', () => {
  it('should not send unmounted field values on submit', async () => {
    const props = {
      onSubmitForm: jest.fn(),
    };
    const { container } = render(
      <ReactHookFormProvider>
        <TestComponent {...props} />
      </ReactHookFormProvider>,
    );

    const button = container.querySelector('button');
    await waitFor(() => fireEvent.click(button));
    expect(props.onSubmitForm).not.toHaveBeenCalled();
    expect(container.querySelector('.Mui-error')).toBeTruthy();

    // fill in required field
    const field1 = container.querySelector('[name="field1"]');
    await waitFor(() => fireEvent.change(field1, { target: { value: 't' } }));
    await waitFor(() => fireEvent.click(button));
    expect(props.onSubmitForm).toHaveBeenCalledWith({ field1: 't' });

    props.onSubmitForm.mockClear();

    // fill in second field
    await waitFor(() =>
      fireEvent.change(field1, { target: { value: 'test' } }),
    );
    const field2 = container.querySelector('[name="field2"]');
    await waitFor(() =>
      fireEvent.change(field2, { target: { value: 'test field 2' } }),
    );
    await waitFor(() => fireEvent.click(button));
    expect(props.onSubmitForm).toHaveBeenCalledWith({
      field1: 'test',
      field2: 'test field 2',
    });

    props.onSubmitForm.mockClear();

    // make sure second field is unmounted and submit again
    await waitFor(() => fireEvent.change(field1, { target: { value: 't' } }));
    expect(container.querySelector('[name="field2"]')).toBeFalsy();
    await waitFor(() => fireEvent.click(button));
    expect(props.onSubmitForm).toHaveBeenCalledWith({
      field1: 't',
    });
  });
});
