/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { Typography, Button } from '@mui/material';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormProvider, ReactHookFormTextField } from '.';

export default {
  title: 'ReactHookForm/Text Field',
};

const ReactHookFormTextFieldWrapped = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <ThemeWrapperOldTheme>
      <div style={{ width: '150px' }}>
        <div style={{ marginBottom: '30px' }}>
          <Typography>Inputmode Text (default)</Typography>
          <ReactHookFormTextField
            name="textdemo"
            label="Text"
            rules={{ required: true }}
          />
        </div>
        <div style={{ marginBottom: '30px' }}>
          <Typography>Disabled</Typography>
          <ReactHookFormTextField
            name="disableddemo"
            label="Dummy label"
            rules={{
              required: true,
            }}
            disabled
            defaultValue="Dummy value"
          />
        </div>
        <div style={{ marginBottom: '30px' }}>
          <Typography>With helpertext and default value</Typography>
          <ReactHookFormTextField
            name="helpertextdemo"
            label="Dummy label"
            helperText="Here to help"
            rules={{
              required: true,
            }}
            defaultValue="Default value"
          />
        </div>
        <Button
          variant="contained"
          color="secondary"
          onClick={(): void => {
            handleSubmit(() => {})();
          }}
        >
          Validate
        </Button>
      </div>
    </ThemeWrapperOldTheme>
  );
};

export const TextField = (): React.ReactElement => (
  <ReactHookFormProvider>
    <ReactHookFormTextFieldWrapped />
  </ReactHookFormProvider>
);

const ReactHookFormTextFieldWrappedSnapshot = (): React.ReactElement => {
  return (
    <ThemeWrapperOldTheme>
      <div style={{ width: '150px' }}>
        <div style={{ marginBottom: '30px' }}>
          <ReactHookFormTextField
            name="textdemo"
            label="Text"
            rules={{ required: true }}
          />
        </div>
        <div style={{ marginBottom: '30px' }}>
          <ReactHookFormTextField
            name="disableddemo"
            label="Dummy label"
            rules={{
              required: true,
            }}
            disabled
            defaultValue="Dummy value"
          />
        </div>
        <div style={{ marginBottom: '30px' }}>
          <ReactHookFormTextField
            name="helpertextdemo"
            label="Dummy label"
            helperText="Here to help"
            rules={{
              required: true,
            }}
            defaultValue="Default value"
          />
        </div>
      </div>
    </ThemeWrapperOldTheme>
  );
};

export const TextFieldSnapshot = (): React.ReactElement => (
  <ReactHookFormProvider>
    <ReactHookFormTextFieldWrappedSnapshot />
  </ReactHookFormProvider>
);

TextFieldSnapshot.storyName = 'Text Field (takeSnapshot)';
