/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { ReactNode } from 'react';
import { createIntl, createIntlCache } from 'react-intl';
import translations from './languageTranslation';

interface Languages {
  language: string;
  code: string;
  flag: string;
}

const getLanguages = (): Languages[] => {
  return [
    { language: 'English', code: 'en', flag: '🇬🇧' },
    { language: 'Dutch', code: 'nl', flag: '🇳🇱' },
    { language: 'Finnish', code: 'fi', flag: '🇫🇮' },
    { language: 'Norwegian', code: 'no', flag: '🇳🇴' },
  ];
};

const cache = createIntlCache();

let int = createIntl(
  {
    locale: 'en',
    // eslint-disable-next-line dot-notation
    messages: translations['en'],
  },
  cache,
);

export function changeLanguage(lang: string): void {
  const newIntl = createIntl(
    {
      locale: lang,
      messages: translations[lang],
    },
    cache,
  );
  int = newIntl;
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const translate = (id: string, values?): ReactNode | number => {
  return int.formatMessage({ id }, values);
};

export default getLanguages();
