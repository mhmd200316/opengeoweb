/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { LayerType } from '@opengeoweb/webmap';
import {
  InitialAppPreset,
  InitialAppPresetProps,
  filterMapPresets,
  getInitialAppPresets,
  filterLayers,
  parseBoolean,
  parseLayer,
} from './jsonPresetFilter';
import defaultMapPresets from './initialPresets.json';
import { mapTypes } from '..';
import { Layer } from '../store/mapStore/types';

describe('utils/JsonPresetFilter', () => {
  const undefinedFilteredPresets: InitialAppPresetProps = {
    presetType: undefined,
    presetId: undefined,
    presetName: undefined,
    services: undefined,
    baseServices: undefined,
  };

  const testPreset: InitialAppPreset = {
    preset: {
      presetType: 'mapPreset',
      presetId: 'test1',
      presetName: 'test',
      services: [
        {
          name: 'KNMI Radar',
          url: 'https://geoservices.knmi.nl/cgi-bin/RADNL_OPER_R___25PCPRR_L3.cgi?',
          id: 'knmi-radar',
        },
      ],
      baseServices: [
        {
          name: 'KNMIgeoservicesBaselayers',
          url: 'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
          id: 'KNMIgeoservicesBaselayers',
        },
        {
          name: 'DWD',
          url: 'https://maps.dwd.de/geoserver/ows?',
          id: 'dwd',
        },
      ],
      layers: [
        {
          id: 'base-layer-1',
          name: 'WorldMap_Light_Grey_Canvas',
          type: 'twms',
          layerType: mapTypes.LayerType.baseLayer,
        },
        {
          service: 'https://geoservices.knmi.nl/cgi-bin/worldmaps.cgi?',
          name: 'ne_10m_admin_0_countries_simplified',
          format: 'image/png',
          enabled: true,
          layerType: mapTypes.LayerType.overLayer,
        },
        {
          service:
            'https://geoservices.knmi.nl/cgi-bin/RADNL_OPER_R___25PCPRR_L3.cgi?',
          name: 'RADNL_OPER_R___25PCPRR_L3_COLOR',
          id: 'RAD_NL25_PCP_CM',
          format: 'image/png',
          enabled: true,
          style: 'knmiradar/nearest',
          layerType: mapTypes.LayerType.mapLayer,
        },
        {
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=HARM_N25&',
          name: 'air_temperature__at_pl',
          id: 'air_temperature__at_pl',
          format: 'image/png',
          enabled: true,
          style: '',
          layerType: mapTypes.LayerType.mapLayer,
          dimensions: [
            {
              name: 'elevation',
              currentValue: '850',
            },
          ],
        },
      ],
    },
  };

  describe('parseBoolean', () => {
    it('should return correct value as boolean', () => {
      expect(parseBoolean(true)).toBeTruthy();
      expect(parseBoolean('true')).toBeTruthy();
      expect(parseBoolean(false)).toBeFalsy();
      expect(parseBoolean('false')).toBeFalsy();
    });
  });

  describe('parseLayer', () => {
    it('should parse layer', () => {
      const testLayer1 = { layerType: LayerType.baseLayer };
      expect(parseLayer(testLayer1)).toEqual({
        ...testLayer1,
        id: expect.stringContaining('layer'),
      });

      const testLayer2 = {
        id: 'test',
        layerType: LayerType.baseLayer,
        name: 'testing',
      };
      expect(parseLayer(testLayer2)).toEqual(testLayer2);

      const testLayer3 = {
        id: 'test',
        layerType: LayerType.baseLayer,
        enabled: 'true',
      } as unknown as Layer;

      expect(parseLayer(testLayer3)).toEqual({
        ...testLayer3,
        enabled: true,
      });
    });
  });

  describe('filterLayers', () => {
    it('should filter baselayers, mapLayers and overLayers', () => {
      expect(filterLayers()).toEqual({
        mapLayers: [],
        baseLayers: [],
        overLayers: [],
      });
      expect(filterLayers([])).toEqual({
        mapLayers: [],
        baseLayers: [],
        overLayers: [],
      });
      expect(
        filterLayers([
          { id: 'test-baselayer-1', layerType: LayerType.baseLayer },
        ]),
      ).toEqual({
        mapLayers: [],
        baseLayers: [
          { id: 'test-baselayer-1', layerType: LayerType.baseLayer },
        ],
        overLayers: [],
      });
      expect(
        filterLayers([
          { id: 'test-maplayer-1', layerType: LayerType.mapLayer },
        ]),
      ).toEqual({
        mapLayers: [{ id: 'test-maplayer-1', layerType: LayerType.mapLayer }],
        baseLayers: [],
        overLayers: [],
      });
      expect(
        filterLayers([
          { id: 'test-overLayer-1', layerType: LayerType.overLayer },
        ]),
      ).toEqual({
        mapLayers: [],
        baseLayers: [],
        overLayers: [
          { id: 'test-overLayer-1', layerType: LayerType.overLayer },
        ],
      });

      expect(
        filterLayers([
          { id: 'test-maplayer-1', layerType: LayerType.mapLayer },
          { id: 'test-baseLayer-1', layerType: LayerType.baseLayer },
          { id: 'test-overlayer-1', layerType: LayerType.overLayer },
          { id: 'test-maplayer-2', layerType: LayerType.mapLayer },
          { id: 'test-baseLayer-2', layerType: LayerType.baseLayer },
          { id: 'test-overlayer-2', layerType: LayerType.overLayer },
          { id: 'test-maplayer-3', layerType: LayerType.mapLayer },
          { id: 'test-baseLayer-3', layerType: LayerType.baseLayer },
        ]),
      ).toEqual({
        mapLayers: [
          { id: 'test-maplayer-1', layerType: LayerType.mapLayer },
          { id: 'test-maplayer-2', layerType: LayerType.mapLayer },
          { id: 'test-maplayer-3', layerType: LayerType.mapLayer },
        ],
        baseLayers: [
          { id: 'test-baseLayer-1', layerType: LayerType.baseLayer },
          { id: 'test-baseLayer-2', layerType: LayerType.baseLayer },
          { id: 'test-baseLayer-3', layerType: LayerType.baseLayer },
        ],
        overLayers: [
          { id: 'test-overlayer-1', layerType: LayerType.overLayer },
          { id: 'test-overlayer-2', layerType: LayerType.overLayer },
        ],
      });
    });

    it('should be able to add a parser', () => {
      const layersWithoutId = [
        {
          name: 'test-baselayer',
          layerType: LayerType.baseLayer,
        },
        {
          name: 'test-overlayer',
          layerType: LayerType.overLayer,
        },
        {
          name: 'test-mapLayer',
          layerType: LayerType.mapLayer,
        },
      ];

      expect(filterLayers(layersWithoutId, parseLayer)).toEqual({
        baseLayers: [
          {
            ...layersWithoutId[0],
            id: expect.stringContaining('layer'),
          },
        ],
        overLayers: [
          {
            ...layersWithoutId[1],
            id: expect.stringContaining('layer'),
          },
        ],
        mapLayers: [
          {
            ...layersWithoutId[2],
            id: expect.stringContaining('layer'),
          },
        ],
      });
    });
  });

  describe('filterMapPresets', () => {
    it('should return undefined values if presets is null or empty', () => {
      expect(filterMapPresets(null)).toEqual(undefinedFilteredPresets);
      expect(filterMapPresets({ preset: {} })).toEqual(
        undefinedFilteredPresets,
      );

      expect(filterMapPresets({ preset: {} }).baseServices).toBeUndefined();
    });

    it('should have the correct default values', () => {
      const defaultPreset = filterMapPresets(
        defaultMapPresets as InitialAppPreset,
      );

      expect(defaultPreset.services).toHaveLength(8);
      expect(defaultPreset.baseServices).toHaveLength(2);
      expect(defaultPreset.baseLayers).toHaveLength(4);
    });

    it('should return filtered presets', () => {
      const filteredTestPreset = filterMapPresets(testPreset);

      expect(filteredTestPreset.baseLayers).toHaveLength(1);

      expect(filteredTestPreset.mapLayers).toHaveLength(2);
      expect(filteredTestPreset.services).toHaveLength(1);
      expect(filteredTestPreset.baseServices).toHaveLength(2);
      expect(filteredTestPreset.baseServices).toEqual(
        testPreset.preset.baseServices,
      );
    });
  });

  describe('getInitialAppPresets', () => {
    it('should return filtered map presets object with undefined values if preset is empty', () => {
      expect(
        getInitialAppPresets(
          { preset: {} },
          defaultMapPresets as InitialAppPreset,
        ),
      ).toEqual(undefinedFilteredPresets);
    });

    it('should return default initial presets if presets is null', () => {
      const initialPresets = getInitialAppPresets(
        null,
        defaultMapPresets as InitialAppPreset,
      );

      expect(initialPresets.services).toHaveLength(8);
      expect(initialPresets.baseServices).toEqual(
        defaultMapPresets.preset.baseServices,
      );
      expect(initialPresets.baseServices).toHaveLength(2);
    });
  });
});
