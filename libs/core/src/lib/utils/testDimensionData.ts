/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

const elevationLayer1 = {
  dimensionName: 'elevation',
  layerId: 'layer_1',
  layerName: 'first layer',
  unit: 'hPa',
  marks: [
    { value: 200, label: '200 hPa' },
    { value: 400, label: '400 hPa' },
    { value: 600, label: '600 hPa' },
    { value: 800, label: '800 hPa' },
    { value: 1000, label: '1000 hPa' },
  ],
  synced: true,
  validSelection: true,
  currentValue: '1000',
};

const elevationLayer2 = {
  dimensionName: 'elevation',
  layerId: 'layer_2',
  layerName: 'second layer',
  unit: 'hPa',
  marks: [
    { value: 200, label: '200 hPa' },
    { value: 300, label: '300 hPa' },
    { value: 600, label: '600 hPa' },
    { value: 1000, label: '1000 hPa' },
  ],
  synced: true,
  validSelection: true,
  currentValue: '1000',
};

const elevationLayerInvalid = {
  dimensionName: 'elevation',
  layerId: 'layer_invalid',
  layerName: 'invalid layer',
  unit: 'hPa',
  marks: [
    { value: 200, label: '200 hPa' },
    { value: 300, label: '300 hPa' },
    { value: 600, label: '600 hPa' },
    { value: 1000, label: '1000 hPa' },
  ],
  synced: true,
  validSelection: false,
  currentValue: '400',
};

export const elevationDimMapSingleLayer = [elevationLayer1];

export const elevationDimMapTwoLayers = [elevationLayer1, elevationLayer2];

export const elevationInvalidValue = [elevationLayerInvalid, elevationLayer1];
