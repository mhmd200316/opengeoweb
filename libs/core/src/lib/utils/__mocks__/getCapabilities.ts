/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { LayerTree } from '../getCapabilities';

export const MOCK_URL_WITH_CHILDREN = 'https://mockUrlWithChildren.nl';
export const MOCK_URL_NO_CHILDREN = 'https://mockUrlNoChildren.nl';
export const MOCK_URL_WITH_NO_TITLE = 'https://mockUrlWithNoTitle.nl';
export const MOCK_URL_WITH_NO_TITLE_OR_NAME =
  'https://mockUrlWithNoTitleOrName.nl/wms?';
export const MOCK_URL_WITH_SUBCATEGORY = 'https://mockUrlWithSubcategory.nl';
export const MOCK_URL_INVALID = 'https://notawmsservice.nl';
export const MOCK_URL_SLOW = 'https://slowresolve.nl';
export const MOCK_URL_SLOW_FAILS = 'https://slowreject.nl';
export const MOCK_URL_DEFAULT = 'https://defaultservice.nl';
export const MOCK_URL_DEFAULT2 = 'https://defaultservice2.nl';
export const MOCK_URL_HTTP = 'http://wmsservice.nl';

export const mockLayersNoChildren = {
  leaf: false,
  name: null,
  title: 'RADNL_OPER_R___25PCPRR_L3_WMS',
  path: [],
  children: [],
};

export const mockLayersWithChildren = {
  leaf: false,
  name: null,
  title: 'RADNL_OPER_R___25PCPRR_L3_WMS',
  path: [],
  children: [
    {
      leaf: true,
      name: 'RAD_NL25_PCP_CM',
      title: 'RAD_NL25_PCP_CM',
      path: [],
      children: [],
    },
    {
      leaf: true,
      name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
      title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
      path: [],
      children: [],
    },
  ],
};

export const mockLayersWithNoTitle = {
  leaf: false,
  name: 'UNTITLED_WMS',
  title: null,
  path: [],
  children: [],
};

export const mockLayersWithNoTitleOrName = {
  leaf: false,
  name: null,
  title: null,
  path: [],
  children: [],
};

export const mockLayersWithSubcategory = {
  leaf: false,
  name: null,
  title: 'Service with subcategory',
  path: [],
  children: [
    {
      leaf: true,
      name: 'cloud_area_fraction',
      title: 'Cloud cover (flag)',
      path: [],
      children: [],
    },
    {
      leaf: false,
      name: null,
      title: 'Subcategory',
      path: [],
      children: [
        {
          leaf: true,
          name: 'baselayer',
          title: 'Baselayer (-)',
          path: ['Subcategory'],
          children: [],
        },
        {
          leaf: false,
          name: null,
          title: 'Klima',
          path: ['Subcategory'],
          children: [
            {
              leaf: true,
              name: 'haic_airports',
              title: 'HAIC Airports',
              path: ['SubcategoryKlima'],
              children: [],
            },
          ],
        },
      ],
    },
  ],
};

export const mockLayersDefault = {
  leaf: false,
  name: null,
  title: 'Default title for default layer',
  path: [],
  children: [],
};
export const mockLayersDefault2 = {
  leaf: false,
  name: null,
  title: 'Default title for default 2 layer',
  path: [],
  children: [],
};

export const getLayersFromService = (url: string): Promise<LayerTree> => {
  switch (url) {
    case MOCK_URL_WITH_CHILDREN:
      return new Promise((resolve) => {
        resolve(mockLayersWithChildren);
      });
    case MOCK_URL_NO_CHILDREN:
      return new Promise((resolve) => {
        resolve(mockLayersNoChildren);
      });
    case MOCK_URL_WITH_NO_TITLE:
      return new Promise((resolve) => {
        resolve(mockLayersWithNoTitle);
      });
    case MOCK_URL_WITH_NO_TITLE_OR_NAME:
      return new Promise((resolve) => {
        resolve(mockLayersWithNoTitleOrName);
      });
    case MOCK_URL_WITH_SUBCATEGORY:
      return new Promise((resolve) => {
        resolve(mockLayersWithSubcategory);
      });
    case MOCK_URL_SLOW:
      return new Promise((resolve) => {
        setTimeout(() => resolve(mockLayersWithChildren), 300);
      });
    case MOCK_URL_SLOW_FAILS:
      return new Promise((resolve, reject) => {
        setTimeout(
          () =>
            reject(new Error(`Mock url '${url}' service returned an error.`)),
          300,
        );
      });
    case MOCK_URL_INVALID:
      return Promise.reject(new Error(`Url '${url}' is not a wms service.`));
    case MOCK_URL_HTTP:
      return Promise.reject(new Error(`Url '${url}' is not https.`));
    case MOCK_URL_DEFAULT:
      return new Promise((resolve) => {
        resolve(mockLayersDefault);
      });
    case MOCK_URL_DEFAULT2:
      return new Promise((resolve) => {
        resolve(mockLayersDefault2);
      });
    default: {
      const message = `Url '${url}' is not a mock-url.`;
      return Promise.reject(new Error(message));
    }
  }
};
