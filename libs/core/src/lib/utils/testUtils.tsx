/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  defaultReduxServices,
  WmMultiDimensionServices,
} from './defaultTestSettings';
import {
  WebMapState,
  LayerState,
  LayerType,
  Dimension,
  Layer,
  WebMapStateModuleState,
  Scale,
} from '../store/mapStore/types';
import { AppStore } from '../types/types';
import { createMap } from '../store/mapStore/map/utils';
import { createLayer } from '../store/mapStore/layers/reducer';
import { defaultAnimationDelayAtStart } from '../components/TimeSlider/TimeSliderUtils';

export const createWebmapState = (...mapIds: string[]): WebMapState => {
  const mockState = {
    byId: {},
    allIds: [],
  };

  for (let i = 0; i < mapIds.length; i += 1) {
    const mapState = createMap({ id: mapIds[i] });
    mockState.byId[mapIds[i]] = { ...mapState };
    mockState.allIds.push(mapIds[i]);
  }
  return mockState;
};

export const createLayersState = (layerId: string, props = {}): LayerState => ({
  byId: {
    [layerId]: createLayer({
      id: layerId,
      ...props,
    }),
  },
  allIds: [layerId],
  availableBaseLayers: { allIds: [], byId: {} },
});

export const createMultipleLayersState = (
  layers: Layer[],
  mapId: string,
): LayerState => {
  const mockState = {
    byId: {},
    allIds: [],
    availableBaseLayers: { allIds: [], byId: {} },
  };

  for (let i = 0; i < layers.length; i += 1) {
    const layerState = createLayer({ ...layers[i], id: layers[i].id, mapId });
    mockState.byId[layers[i].id] = { ...layerState };
    mockState.allIds.push(layers[i].id);
    if (layers[i].layerType === LayerType.baseLayer) {
      mockState.availableBaseLayers.byId[layers[i].id] = { ...layerState };
      mockState.availableBaseLayers.allIds.push(layers[i].id);
    }
  }
  return mockState;
};

export const createMapDimensionsState = (
  dimensions?: Dimension[],
  ...mapIds: string[]
): WebMapState => {
  const mockState = {
    byId: {},
    allIds: [],
  };

  for (let i = 0; i < mapIds.length; i += 1) {
    const mapState = createMap({ id: mapIds[i] });
    mockState.byId[mapIds[i]] = { ...mapState };
    mockState.byId[mapIds[i]].dimensions = dimensions;
    mockState.allIds.push(mapIds[i]);
  }
  return mockState;
};

const webmapStateWithAddedLayer = (
  layerType: LayerType,
  layerId: string,
  mapId: string,
  dimensions?: Dimension[],
): WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the dimensions to the map state
  if (dimensions && dimensions.length) {
    dimensions.forEach((dimension) => {
      webmap.byId[mapId].dimensions.push(dimension);
    });
  }

  // Add the layer to the map state
  switch (layerType) {
    case LayerType.mapLayer:
      webmap.byId[mapId].mapLayers.push(layerId);
      break;
    case LayerType.baseLayer:
      webmap.byId[mapId].baseLayers.push(layerId);
      break;
    case LayerType.overLayer:
      webmap.byId[mapId].overLayers.push(layerId);
      break;
    default:
      break;
  }

  // first layer is active layer
  webmap.byId[mapId].activeLayerId = layerId;

  return webmap;
};

const webmapStateWithDimensions = (
  mapId: string,
  dimensions?: Dimension[],
): WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the dimensions to the map state
  if (dimensions && dimensions.length) {
    dimensions.forEach((dimension) => {
      webmap.byId[mapId].dimensions.push(dimension);
    });
  }
  return webmap;
};

export const mockStateMapWithLayer = (
  layer: Layer,
  mapId: string,
): AppStore => ({
  webmap: webmapStateWithAddedLayer(
    layer.layerType,
    layer.id,
    mapId,
    layer.dimensions,
  ),
  services: { byId: defaultReduxServices, allIds: ['serviceid_1'] },
  layers: createLayersState(layer.id, {
    name: layer.name,
    layerType: layer.layerType,
    mapId,
    service: layer.service,
    dimensions: layer.dimensions,
    style: layer.styles && layer.styles.length ? layer.styles[0].name : '',
  }),
  syncronizationGroupStore: {
    groups: {
      byId: {},
      allIds: [],
    },
    sources: {
      byId: {},
      allIds: [],
    },
    viewState: {
      timeslider: {
        groups: [],
        sourcesById: [],
      },
      zoompane: {
        groups: [],
        sourcesById: [],
      },
      level: {
        groups: [],
        sourcesById: [],
      },
    },
  },
});

const webmapStateWithMultipleLayers = (
  layers: Layer[],
  mapId: string,
): WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the layers to the map state
  layers.forEach((layer) => {
    switch (layer.layerType) {
      case LayerType.mapLayer:
        webmap.byId[mapId].mapLayers.push(layer.id);
        break;
      case LayerType.baseLayer:
        webmap.byId[mapId].baseLayers.push(layer.id);
        break;
      case LayerType.overLayer:
        webmap.byId[mapId].overLayers.push(layer.id);
        break;
      default:
        break;
    }
  });

  // first layer is active layer
  if (layers.length) webmap.byId[mapId].activeLayerId = layers[0].id;

  return webmap;
};

export const mockStateMapWithMultipleLayers = (
  layers: Layer[],
  mapId: string,
): WebMapStateModuleState => ({
  webmap: webmapStateWithMultipleLayers(layers, mapId),
  services: { byId: defaultReduxServices, allIds: ['serviceid_1'] },
  layers: createMultipleLayersState(layers, mapId),
});

export const mockStateMapWithDimensions = (
  layer: Layer,
  mapId: string,
): WebMapStateModuleState => ({
  webmap: webmapStateWithAddedLayer(
    layer.layerType,
    layer.id,
    mapId,
    layer.dimensions,
  ),
  services: {
    byId: WmMultiDimensionServices,
    allIds: ['serviceid_1', 'serviceid_2'],
  },
  layers: createLayersState(layer.id, {
    name: layer.name,
    layerType: layer.layerType,
    mapId,
    service: layer.service,
    dimensions: layer.dimensions,
  }),
});

export const mockStateMapWithDimensionsWithoutLayers = (
  mapId: string,
): WebMapStateModuleState => ({
  webmap: webmapStateWithDimensions(mapId, [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
    },
  ]),
});

const webmapStateWithTimeSliderScale = (
  mapId: string,
  timeSliderScale: Scale,
): WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the scale to the map state
  webmap.byId[mapId].timeSliderScale = timeSliderScale;
  return webmap;
};

const webmapStateWithTimeStep = (
  mapId: string,
  timeStep: number,
): WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the timeStep to the map state
  webmap.byId[mapId].timeStep = timeStep;
  return webmap;
};

export const mockStateMapWithTimeSliderScaleWithoutLayers = (
  mapId: string,
  timeSliderScale: Scale,
): WebMapStateModuleState => ({
  webmap: webmapStateWithTimeSliderScale(mapId, timeSliderScale),
});

export const mockStateMapWithTimeStepWithoutLayers = (
  mapId: string,
  timeStep: number,
): WebMapStateModuleState => ({
  webmap: webmapStateWithTimeStep(mapId, timeStep),
});

const webmapStateWithAnimationDelay = (
  mapId: string,
  animationDelay: number,
): WebMapState => {
  const webmap = createWebmapState(mapId);

  // Add the dimensions to the map state
  webmap.byId[mapId].animationDelay = animationDelay;
  return webmap;
};

export const mockStateMapWithAnimationDelayWithoutLayers = (
  mapId: string,
): WebMapStateModuleState => ({
  webmap: webmapStateWithAnimationDelay(mapId, defaultAnimationDelayAtStart),
});
