/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { LayerOptions, Style, WMLayer } from '@opengeoweb/webmap';
import { Service } from '../components/WMSLoader/services';
import { Layer, LayerType, Services } from '../store/mapStore/types';
import { radarLayer } from './publicLayers';
import {
  MOCK_URL_DEFAULT,
  MOCK_URL_DEFAULT2,
} from './__mocks__/getCapabilities';

export const defaultReduxLayerRadarColor = {
  service: 'https://testservice',
  name: 'RAD_NL25_PCP_CM',
  title: 'RAD_NL25_PCP_CM',
  format: 'image/png',
  style: 'rainbow/nearest',
  enabled: true,
  layerType: LayerType.mapLayer,
  dimensions: [],
  styles: [
    {
      title: 'rainbow/nearest',
      name: 'rainbow/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
      abstract: 'No abstract available',
    },
  ],
  id: 'layerid_1',
};

export const defaultReduxLayerRadarKNMI: Layer = {
  service: 'https://testservice',
  name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
  title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
  format: 'image/png',
  style: 'knmiradar/nearest',
  enabled: true,
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ],
  styles: [
    {
      title: 'knmiradar/nearest',
      name: 'knmiradar/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=knmiradar/nearest',
      abstract: 'No abstract available',
    },
    {
      title: 'precip/nearest',
      name: 'precip/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip/nearest',
      abstract: 'No abstract available',
    },
    {
      title: 'precip-transparent/nearest',
      name: 'precip-transparent/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-transparent/nearest',
      abstract: 'No abstract available',
    },
    {
      title: 'precip-gray/nearest',
      name: 'precip-gray/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-gray/nearest',
      abstract: 'No abstract available',
    },
    {
      title: 'precip-rainbow/nearest',
      name: 'precip-rainbow/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-rainbow/nearest',
      abstract: 'No abstract available',
    },
    {
      title: 'precip-blue/nearest',
      name: 'precip-blue/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-blue/nearest',
      abstract: 'No abstract available',
    },
    {
      title: 'precip-blue-transparent/nearest',
      name: 'precip-blue-transparent/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-blue-transparent/nearest',
      abstract: 'No abstract available',
    },
    {
      title: 'pseudoradar/nearest',
      name: 'pseudoradar/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=pseudoradar/nearest',
      abstract: 'No abstract available',
    },
  ],
  id: 'layerid_2',
};

export const WmdefaultReduxLayerRadarKNMI = new WMLayer({
  service: 'https://testservice',
  name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
  title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
  format: 'image/png',
  style: 'knmiradar/nearest',
  enabled: true,
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ],
  id: 'layerid_3',
});

export const makeGeoservicesRadarLayer = (): WMLayer => {
  return new WMLayer({
    ...radarLayer,
    title: 'RAD_NL25_PCP_CM',
    dimensions: [
      {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-04-15T00:00:00Z',
      },
    ],
  });
};

export const styleListForRADNL25PCPCMLayer: Style[] = [
  {
    title: 'radar/nearest',
    name: 'radar/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=radar/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'precip-rainbow/nearest',
    name: 'precip-rainbow/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'precip-gray/nearest',
    name: 'precip-gray/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-gray/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'precip-blue/nearest',
    name: 'precip-blue/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-blue/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'precip-blue-transparent/nearest',
    name: 'precip-blue-transparent/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-blue-transparent/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'precip-with-range/nearest',
    name: 'precip-with-range/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-with-range/nearest',
    abstract: 'No abstract available',
  },
];

export const styleListForRADNLOPERR25PCPRRL3KNMILayer: Style[] = [
  {
    title: 'knmiradar/nearest',
    name: 'knmiradar/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=knmiradar/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'precip/nearest',
    name: 'precip/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'precip-transparent/nearest',
    name: 'precip-transparent/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-transparent/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'precip-gray/nearest',
    name: 'precip-gray/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-gray/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'precip-rainbow/nearest',
    name: 'precip-rainbow/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-rainbow/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'precip-blue/nearest',
    name: 'precip-blue/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-blue/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'precip-blue-transparent/nearest',
    name: 'precip-blue-transparent/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=precip-blue-transparent/nearest',
    abstract: 'No abstract available',
  },
  {
    title: 'pseudoradar/nearest',
    name: 'pseudoradar/nearest',
    legendURL:
      'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=pseudoradar/nearest',
    abstract: 'No abstract available',
  },
];

export const defaultReduxServices: Services = {
  serviceid_1: {
    name: 'testservice',
    serviceUrl: 'https://testservice',
    layers: [
      {
        name: 'RAD_NL25_PCP_CM',
        text: 'RADAR NL COLOR',
        leaf: true,
        path: [],
        keywords: ['keyword'],
        abstract: 'RADAR NL COLOR abstract',
      },
      {
        name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        text: 'RADAR NL KNMI',
        leaf: true,
        path: [],
        keywords: ['testword'],
        abstract: 'RADAR NL KNMI abstract',
        styles: styleListForRADNLOPERR25PCPRRL3KNMILayer,
      },
      {
        name: 'MULTI_DIMENSION_LAYER',
        text: 'Multi Dimension Layer',
        leaf: true,
        path: [],
        keywords: [],
      },
    ],
  },
};

export const multiDimensionLayer: Layer = {
  service: 'https://testservice',
  id: 'multiDimensionLayerMock',
  name: 'MULTI_DIMENSION_LAYER',
  title: 'MULTI_DIMENSION_LAYER',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T14:40:00Z',
    },
  ],
  styles: [
    {
      title: 'rainbow/nearest',
      name: 'rainbow/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
      abstract: 'No abstract available',
    },
  ],
};

export const flDimensionLayer: Layer = {
  service: 'https://testservice',
  id: 'multiDimensionLayerMock',
  name: 'MULTI_DIMENSION_LAYER',
  title: 'MULTI_DIMENSION_LAYER',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
    { name: 'time', units: 'ISO8601', currentValue: '2020-03-13T14:40:00Z' },
  ],
};

export const WmMultiDimensionLayer = new WMLayer({
  service: 'https://testservice',
  id: 'multiDimensionLayerMock',
  name: 'MULTI_DIMENSION_LAYER',
  title: 'MULTI_DIMENSION_LAYER',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T14:40:00Z',
      values: '2020-03-13T10:40:00Z/2020-03-13T14:40:00Z/PT5M',
    },
  ],
});

export const multiDimensionLayer2: Layer = {
  service: 'https://testservicedimensions',
  id: 'multiDimensionLayerMock2',
  name: 'netcdf_5dims',
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'member',
      units: 'member number',
      currentValue: 'member1',
      values: 'member1,member2,member3,member4',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
    },
  ],
  styles: [
    {
      title: 'rainbow/nearest',
      name: 'rainbow/nearest',
      legendURL:
        'https://geoservices.knmi.nl/cgi-bin/geoweb-demo.cgi?SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=netcdf_5dims&format=image/png&STYLE=rainbow/nearest',
      abstract: 'No abstract available',
    },
  ],
};

export const multiDimensionLayer3: Layer = {
  service: 'https://testservicedimensions',
  id: 'multiDimensionLayerMock2',
  name: 'netcdf_5dims',
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'member',
      units: 'member number',
      currentValue: 'member1',
      values: 'member1,member2,member3,member4',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
      timeInterval: {
        year: 0,
        month: 0,
        day: 0,
        hour: 0,
        minute: 5,
        second: 0,
        isRegularInterval: true,
        getTime: null,
        toISO8601: null,
      },
    },
  ],
  styles: [
    {
      title: 'rainbow/nearest',
      name: 'rainbow/nearest',
      legendURL:
        'https://geoservices.knmi.nl/cgi-bin/geoweb-demo.cgi?SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=netcdf_5dims&format=image/png&STYLE=rainbow/nearest',
      abstract: 'No abstract available',
    },
  ],
};

export const WmMultiDimensionLayer2 = new WMLayer({
  service: 'https://testservicedimensions',
  id: 'multiDimensionLayerMock2',
  name: 'netcdf_5dims',
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'member',
      units: 'member number',
      currentValue: 'member1',
      values: 'member1,member2,member3,member4',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
    },
  ],
});

export const WmMultiDimensionLayer3 = new WMLayer({
  service: 'https://testservicedimensions',
  id: 'multiDimensionLayerMock2',
  name: 'netcdf_5dims',
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'member',
      units: 'member number',
      currentValue: 'member1',
      values: 'member1,member2,member3,member4',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
    },
  ],
});

export const WmMultiDimensionServices: Services = {
  serviceid_1: {
    name: 'testservicedimensions',
    serviceUrl: 'https://testservicedimensions',
    layers: [
      {
        name: 'netcdf_5dims',
        text: 'netcdf_5dims',
        leaf: true,
        path: [],
      },
    ],
  },
  serviceid_2: {
    name: 'testservice',
    serviceUrl: 'https://testservice',
    layers: [
      {
        name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        text: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        leaf: true,
        path: [],
      },
      {
        name: 'LAYER_WITHOUT_TIME',
        text: 'LAYER_WITHOUT_TIME',
        leaf: true,
        path: [],
      },
    ],
  },
};

export const layerWithoutTimeDimension: LayerOptions = {
  service: 'https://testservice',
  id: 'layerWithoutTime',
  name: 'LAYER_WITHOUT_TIME',
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
  ],
};

export const WmLayerWithoutTimeDimension = new WMLayer(
  layerWithoutTimeDimension,
);

export const defaultTestServices: Service[] = [
  {
    name: 'Mock URL A for default service',
    url: MOCK_URL_DEFAULT,
    id: 'MOCK_URL_DEFAULT',
  },
  {
    name: 'Mock URL B for default service',
    url: MOCK_URL_DEFAULT2,
    id: 'MOCK_URL_DEFAULT2',
  },
];
