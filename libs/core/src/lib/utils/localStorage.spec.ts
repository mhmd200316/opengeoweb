/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { getUserAddedServices, setUserAddedServices } from './localStorage';
import { UserAddedServices } from './types';

beforeEach(() => {
  window.localStorage.clear();
});

const testServices: UserAddedServices = {
  'https://testservice1.fi/wms?': {
    url: 'https://testservice1.fi/wms?',
    name: 'test service 1',
  },
  'https://testservice2.fi/wms?': {
    url: 'https://testservice2.fi/wms?',
    name: 'test service 2',
  },
};

const servicesAfterLocalStorage: UserAddedServices = {
  'https://testservice1.fi/wms?': {
    url: 'https://testservice1.fi/wms?',
    name: 'test service 1',
    isUserAddedService: true,
  },
  'https://testservice2.fi/wms?': {
    url: 'https://testservice2.fi/wms?',
    name: 'test service 2',
    isUserAddedService: true,
  },
};

describe('src/lib/utils/localStorage', () => {
  describe('getUserAddedServices', () => {
    it('should return empty userAddedServices when localStorage is empty', () => {
      expect(getUserAddedServices()).toEqual({});
    });
    it('should return valid UserAddedServices from localStorage', () => {
      window.localStorage.setItem(
        'userAddedServices',
        JSON.stringify(testServices),
      );
      expect(getUserAddedServices()).toEqual(servicesAfterLocalStorage);
    });
    it('should filter out invalid persisted data', () => {
      const savedData = {
        ...testServices,
        ...{ hello: 'world' },
        ...{ '42': '' },
      };
      window.localStorage.setItem(
        'userAddedServices',
        JSON.stringify(savedData),
      );
      expect(getUserAddedServices()).toEqual(servicesAfterLocalStorage);
    });
  });
});

describe('setUserAddedServices', () => {
  it('should persist given UserAddedServices', () => {
    setUserAddedServices(testServices);
    expect(
      JSON.parse(window.localStorage.getItem('userAddedServices')),
    ).toEqual(testServices);
  });
});
