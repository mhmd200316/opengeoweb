/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { waitFor } from '@testing-library/react';
import { styleListForRADNL25PCPCMLayer } from './defaultTestSettings';
import {
  getLayersFlattenedFromService,
  LayerTree,
  recurseNodes,
} from './getCapabilities';
import WMS111GetCapabilitiesGeoServicesRADAR from './__mocks__/WMS111GetCapabilitiesGeoServicesRADAR';

describe('utils/getCapabilities', () => {
  const storedFetch = global['fetch'];

  afterEach(() => {
    global['fetch'] = storedFetch;
  });

  describe('recurseNodes', () => {
    it('should have hierarchical layer structure', () => {
      const webmapJSNodeListInput = {
        text: 'A',
        leaf: false,
        keywords: [],
        abstract: '',
        styles: [],
        children: [
          {
            text: 'B',
            leaf: false,
            path: ['A'],
            keywords: [],
            abstract: '',
            styles: [],
            children: [
              {
                text: 'C',
                path: ['A', 'B'],
                leaf: false,
                keywords: [],
                abstract: '',
                styles: [],
                children: [
                  {
                    text: 'd',
                    name: 'd',
                    leaf: true,
                    path: ['A', 'B', 'C'],
                    children: [],
                    keywords: [],
                    styles: [],
                    abstract: '',
                  },
                ],
              },
            ],
          },
        ],
      };
      const layerTree: LayerTree = {
        leaf: false,
        title: 'A',
        name: undefined,
        path: [],
        keywords: [],
        abstract: '',
        styles: [],
        children: [
          {
            leaf: false,
            title: 'B',
            name: undefined,
            path: ['A'],
            keywords: [],
            abstract: '',
            styles: [],
            children: [
              {
                leaf: false,
                name: undefined,
                title: 'C',
                path: ['A', 'B'],
                keywords: [],
                abstract: '',
                styles: [],
                children: [
                  {
                    leaf: true,
                    name: 'd',
                    title: 'd',
                    path: ['A', 'B', 'C'],
                    children: [],
                    keywords: [],
                    styles: [],
                    abstract: '',
                  },
                ],
              },
            ],
          },
        ],
      };
      expect(recurseNodes(webmapJSNodeListInput as LayerTree)).toEqual(
        layerTree,
      );
    });
  });

  describe('getLayersFlattenedFromService', () => {
    it('should return layers flattened', async () => {
      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest.fn().mockResolvedValueOnce({
        text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
        headers,
      });
      const spy = jest.spyOn(window, 'fetch');
      const layers = [
        {
          name: 'RAD_NL25_PCP_CM',
          text: 'Precipitation Radar NL',
          leaf: true,
          path: [],
          keywords: ['Radar'],
          abstract: 'Radar NL',
          styles: styleListForRADNL25PCPCMLayer,
        },
      ];
      await waitFor(() => {
        getLayersFlattenedFromService('https://testurlnodes/wms?').then(
          (nodes) => {
            expect(nodes).toEqual(layers);
          },
        );
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });

    it('should handle empty layers', async () => {
      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest.fn().mockResolvedValueOnce({
        text: () =>
          Promise.resolve(
            `<?xml version="1.0" encoding="utf-8"?>
            <WMS_Capabilities version="1.3.0">
              <Capability>
                <Layer/>
              </Capability>
            </WMS_Capabilities>`,
          ),
        headers,
      });
      const spy = jest.spyOn(window, 'fetch');
      await waitFor(() => {
        getLayersFlattenedFromService('https://testurlempty/wms?').then(
          (nodes) => {
            expect(nodes).toEqual([]);
          },
        );
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });

    it('should not reload allready loaded services', async () => {
      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest
        .fn()
        .mockResolvedValueOnce({
          text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
          headers,
        })
        .mockResolvedValueOnce({
          text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
          headers,
        });
      const spy = jest.spyOn(window, 'fetch');
      const layers = [
        {
          name: 'RAD_NL25_PCP_CM',
          text: 'Precipitation Radar NL',
          leaf: true,
          path: [],
          keywords: ['Radar'],
          abstract: 'Radar NL',
          styles: styleListForRADNL25PCPCMLayer,
        },
      ];
      await waitFor(() => {
        getLayersFlattenedFromService('https://sameurltwice/wms?').then(
          (nodes) => {
            expect(nodes).toEqual(layers);
          },
        );
        getLayersFlattenedFromService('https://sameurltwice/wms?').then(
          (nodes) => {
            expect(nodes).toEqual(layers);
          },
        );
        expect(spy).toHaveBeenCalledTimes(1);
      });
    });

    it('should reload previously loaded services', async () => {
      // mock the WMXMLParser fetch
      const headers = new Headers();
      window.fetch = jest
        .fn()
        .mockResolvedValueOnce({
          text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
          headers,
        })
        .mockResolvedValueOnce({
          text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
          headers,
        });
      const spy = jest.spyOn(window, 'fetch');
      const layers = [
        {
          name: 'RAD_NL25_PCP_CM',
          text: 'Precipitation Radar NL',
          leaf: true,
          path: [],
          keywords: ['Radar'],
          abstract: 'Radar NL',
          styles: styleListForRADNL25PCPCMLayer,
        },
      ];
      await waitFor(() => {
        getLayersFlattenedFromService('https://sameurltwice/wms?', true).then(
          (nodes) => {
            expect(nodes).toEqual(layers);
          },
        );
        getLayersFlattenedFromService('https://sameurltwice/wms?', true).then(
          (nodes) => {
            expect(nodes).toEqual(layers);
          },
        );
        expect(spy).toHaveBeenCalledTimes(2);
      });
    });

    it('should return an error message when the request fails', async () => {
      // mock the WMXMLParser fetch with a failed request
      window.fetch = jest
        .fn()
        .mockImplementationOnce(() => Promise.reject(new Error('failed')));

      await waitFor(() => {
        getLayersFlattenedFromService('https://testfailedurl/wms?').catch(
          (error) => {
            expect(error.message).toEqual(
              'Request failed for https://testfailedurl/wms?service=WMS&request=GetCapabilities',
            );
          },
        );
      });
    });
  });
});
