/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { WMJSDimension } from '@opengeoweb/webmap';
import { dimensionConfig } from '../components/MultiMapDimensionSelect/MultiDimensionSelectConfig';

export interface Mark {
  value: number | string;
  label?: React.ReactNode;
}

export const marksByDimension = (dim: WMJSDimension): Mark[] => {
  if (!dim || !dim.name) return [];
  const dimCnf = dimensionConfig.find((cnf) => cnf.name === dim.name);
  const defaultUnit = dimCnf ? dimCnf.defaultUnit : '';
  const marks: Mark[] = [];
  if (dim && dim.size) {
    for (let i = 0; i < dim.size(); i += 1) {
      marks.push({
        value: Number(dim.getValueForIndex(i)) || dim.getValueForIndex(i),
        label: `${Number(dim.getValueForIndex(i)) || dim.getValueForIndex(i)} ${
          dim.unitSymbol ? dim.unitSymbol : defaultUnit
        }`,
      });
    }
  }
  return marks;
};

/**
 * Re-formats moment's isoString to the string that is expected by WMJSDimension (for time).
 * @param dateIn The input date to re-format, usually in the format `YYYY-MM-DDThh:mm:ss.uuuZ`
 * @returns The formated input date in the format `YYYY-MM-DDThh:mm:ssZ`
 */
export const handleMomentISOString = (dateIn: string): string => {
  /* Try to fix the timestrings generated by moment js made with moment.toISOString() */
  if (!dateIn) return null;

  if (dateIn.length > 20) {
    const fixedDate = dateIn.substring(0, 19);

    return `${fixedDate}Z`;
  }

  if (dateIn.length === 17) {
    const fixedDate = dateIn.substring(0, 16);

    return `${fixedDate}:00Z`;
  }
  return dateIn;
};
