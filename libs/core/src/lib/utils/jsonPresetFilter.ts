/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Service } from '../components/WMSLoader/services';
import { Layer, LayerType } from '../store/mapStore/types';
import { generateLayerId } from '../store/mapStore/utils/helpers';

type FilteredLayerList = {
  mapLayers: Layer[];
  baseLayers: Layer[];
  overLayers?: Layer[];
};

interface FilteredMapPresets extends FilteredLayerList {
  services?: Service[];
  baseServices?: Service[];
}

// this preset is only used with initialPresets.json files
export interface InitialAppPresetProps {
  presetType?: string;
  presetId?: string;
  presetName?: string;
  services?: Service[];
  baseServices?: Service[];
  layers?: Layer[];
  baseLayers?: Layer[];
}

export interface InitialAppPreset {
  preset: InitialAppPresetProps;
}

const isValidService = (service: Service): boolean =>
  Object.prototype.hasOwnProperty.call(service, 'url');

export const parseBoolean = (value: string | boolean): boolean =>
  typeof value === 'boolean' ? value : value === 'true';

export const parseLayer = (layer: Layer): Layer => ({
  ...layer,
  ...(!layer.id && { id: generateLayerId() }),
  ...(layer.enabled && { enabled: parseBoolean(layer.enabled) }),
});

const createEmtpyFilteredList = (existingList: []): FilteredLayerList => ({
  mapLayers: existingList,
  baseLayers: existingList,
  overLayers: existingList,
});

export const filterLayers = (
  layers: Layer[] = [],
  layerParser = (layer: Layer): Layer => layer,
): FilteredLayerList =>
  layers.reduce((layers, layer) => {
    const { mapLayers, baseLayers, overLayers } = layers;
    const parsedLayer = layerParser(layer);
    if (layer.layerType === LayerType.baseLayer) {
      return {
        mapLayers,
        baseLayers: baseLayers.concat(parsedLayer),
        overLayers,
      };
    }

    if (layer.layerType === LayerType.mapLayer) {
      return {
        mapLayers: mapLayers.concat(parsedLayer),
        baseLayers,
        overLayers,
      };
    }

    if (layer.layerType === LayerType.overLayer) {
      return {
        mapLayers,
        baseLayers,
        overLayers: overLayers.concat(parsedLayer),
      };
    }

    return layers;
  }, createEmtpyFilteredList([]));

export const filterServices = (presetArray: Service[]): Service[] => {
  const hasServices =
    presetArray && Array.isArray(presetArray) && presetArray.length > 0;
  const validServices = hasServices
    ? presetArray.filter((preset) => isValidService(preset))
    : null;
  const hasValidServices = validServices && validServices.length > 0;
  const services = hasValidServices ? validServices : undefined;

  return services;
};

export const filterMapPresets = (
  initialAppPreset: InitialAppPreset,
): FilteredMapPresets => {
  const preset = (initialAppPreset && initialAppPreset.preset) || {};
  const { layers, services, baseServices } = preset;

  const { baseLayers, mapLayers } = layers
    ? filterLayers(layers, parseLayer)
    : createEmtpyFilteredList(undefined);

  const filterredServices = services ? filterServices(services) : undefined;
  const filteredBaseServices = baseServices
    ? filterServices(baseServices)
    : undefined;

  const filteredPreset = {
    services: filterredServices,
    baseServices: filteredBaseServices,
    baseLayers,
    mapLayers,
  };

  return filteredPreset;
};

// loads the initialPresets.json
export const getInitialAppPresets = (
  mapPreset: InitialAppPreset,
  defaultInitialPresets: InitialAppPreset,
): InitialAppPresetProps => {
  const filteredPresets = mapPreset ? filterMapPresets(mapPreset) : null;
  const initialPresets =
    filteredPresets || filterMapPresets(defaultInitialPresets);

  return initialPresets;
};
