/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as mapStoreActions from './store';
import {
  mapSelectors,
  uiActions,
  uiSelectors,
  layerActions,
  genericActions,
  snackbarActions,
} from './store';
import * as mapTypes from './store/mapStore/types';
import * as mapUtils from './store/mapStore/map/utils';
import * as uiTypes from './store/ui/types';
import { reducer as layerReducer } from './store/mapStore/layers/reducer';

import synchronizationGroupConfig from './store/generic/config';

/* Synchronization groups */
import * as SyncGroups from './store/generic/synchronizationGroups';

import coreModuleConfig from './store/coreModuleConfig';

import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from './components/LayerManager';

import {
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
} from './components/MultiMapDimensionSelect';

export * from './components/MultiMapView/MultiMapViewConnect';
export * from './components/MultiMapView/HarmoniePresets';

export { MapViewConnect, MapView, MapViewLayer } from './components/MapView';

export { ReactMapView, ReactMapViewLayer } from './components/ReactMapView';

export {
  LegendConnect,
  Legend,
  LegendMapButtonConnect,
} from './components/Legend';

export { ZoomControls, ZoomControlConnect } from './components/MapControls';

export { TimeSliderConnect } from './components/TimeSlider';

export { MapControls } from './components/MapControls';

export const mapActions = {
  ...mapStoreActions.layerActions,
  ...mapStoreActions.mapActions,
  ...mapStoreActions.serviceActions,
};

export {
  mapSelectors,
  mapTypes,
  mapUtils,
  layerReducer,
  layerActions,
  uiActions,
  uiSelectors,
  uiTypes,
  genericActions as syncGroupActions,
  snackbarActions,
};

export { synchronizationGroupConfig as synchronizationGroupModuleConfig };
export * from './components/ComponentsLookUp';
export * from './components/ConfigurableMap';

export * from './store/mapStore/utils/helpers';

export { SyncGroupViewerConnect } from './components/SyncGroups/SyncGroupViewerConnect';
export { SyncGroups };

export { store } from './storybookUtils/store';

export * from './types/types';

export { coreModuleConfig };

export { LayerManagerConnect, LayerManagerMapButtonConnect };

export {
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
};

export * as publicLayers from './utils/publicLayers';
export * as publicServices from './utils/publicServices';
export * as testLayers from './utils/testLayers';
export * from './utils/jsonPresetFilter';
export * from './utils/types';
export * as defaultConfigurations from './utils/defaultConfigurations';
export * from './components/Providers/Providers';
export * from './components/Snackbar';

export { default as uiModuleConfig } from './store/ui/config';
export { default as mapModuleConfig } from './store/mapStore/config';
export { default as synchronizationGroupsConfig } from './store/generic/config';

export * from './components/MapWarning/MapWarningProperties';

export type {
  FeatureEvent,
  GeoFeatureStyle,
} from './components/ReactMapView/AdagucMapDraw';
