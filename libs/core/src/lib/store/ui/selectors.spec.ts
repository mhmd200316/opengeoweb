/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import produce from 'immer';
import * as uiSelectors from './selectors';
import { DialogType, UIModuleState } from './types';

describe('store/ui/selectors', () => {
  describe('getUiStore', () => {
    it('should return ui store', () => {
      const mockStore = {
        ui: {
          dialogs: {
            legend: {
              type: 'legend' as DialogType,
              activeMapId: 'map1',
              isOpen: false,
            },

            layerManager: {
              type: 'layerManager' as DialogType,
              activeMapId: 'map2',
              isOpen: true,
            },
          },
          order: [],
        },
      };

      expect(uiSelectors.getUiStore(mockStore)).toEqual(mockStore.ui);
    });
  });

  describe('getDialogDetailsByType', () => {
    it('should return the dialog details for that type', () => {
      const mockStore = {
        ui: {
          dialogs: {
            legend: {
              type: 'legend' as DialogType,
              activeMapId: 'map1',
              isOpen: false,
            },
            layerManager: {
              type: 'layerManager' as DialogType,
              activeMapId: 'map2',
              isOpen: true,
            },
          },
          order: [],
        },
      };

      expect(uiSelectors.getDialogDetailsByType(mockStore, 'legend')).toEqual({
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      });
      expect(
        uiSelectors.getDialogDetailsByType(mockStore, 'layerManager'),
      ).toEqual({
        type: 'layerManager' as DialogType,
        activeMapId: 'map2',
        isOpen: true,
      });
      // Should return false and empty string if doesn't exist
      expect(
        uiSelectors.getDialogDetailsByType(
          mockStore,
          'someNonExistentType' as DialogType,
        ),
      ).toEqual(null);
    });
  });

  describe('getisDialogOpen', () => {
    it('should return whether the corresponding dialog is open and what the active mapId is', () => {
      const mockStore = {
        ui: {
          dialogs: {
            legend: {
              type: 'legend' as DialogType,
              activeMapId: 'map1',
              isOpen: false,
            },

            layerManager: {
              type: 'layerManager' as DialogType,
              activeMapId: 'map2',
              isOpen: true,
            },
          },
          order: [],
        },
      };

      expect(uiSelectors.getisDialogOpen(mockStore, 'legend')).toEqual(false);
      expect(uiSelectors.getisDialogOpen(mockStore, 'layerManager')).toEqual(
        true,
      );
      // Should return false and empty string if doesn't exist
      expect(
        uiSelectors.getisDialogOpen(
          mockStore,
          'someNonExistentType' as DialogType,
        ),
      ).toEqual(false);
    });
  });

  describe('getDialogMapId', () => {
    it('should return whether the corresponding dialog is open and what the active mapId is', () => {
      const mockStore = {
        ui: {
          dialogs: {
            legend: {
              type: 'legend' as DialogType,
              activeMapId: 'map1',
              isOpen: false,
            },

            layerManager: {
              type: 'layerManager' as DialogType,
              activeMapId: 'map2',
              isOpen: true,
            },
          },
          order: [],
        },
      };

      expect(uiSelectors.getDialogMapId(mockStore, 'legend')).toEqual('map1');
      expect(uiSelectors.getDialogMapId(mockStore, 'layerManager')).toEqual(
        'map2',
      );
      // Should return false and empty string if doesn't exist
      expect(
        uiSelectors.getDialogMapId(
          mockStore,
          'someNonExistentType' as DialogType,
        ),
      ).toEqual('');
    });
  });
});

describe('getDialogOrder', () => {
  it('should return order of dialog', () => {
    const mockStore = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as DialogType,
            activeMapId: 'map1',
            isOpen: true,
          },

          layerManager: {
            type: 'layerManager' as DialogType,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: ['legend', 'layerManager'] as DialogType[],
      },
    };

    expect(uiSelectors.getDialogOrder(mockStore, 'legend')).toEqual(2);
    expect(uiSelectors.getDialogOrder(mockStore, 'layerManager')).toEqual(1);

    // return 0 when not existing
    expect(
      uiSelectors.getDialogOrder(mockStore, 'dimensionSelect-elevation'),
    ).toEqual(0);
    expect(uiSelectors.getDialogOrder({}, 'dimensionSelect-elevation')).toEqual(
      0,
    );
  });

  it('should return order of visible dialog', () => {
    const mockStore = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },
          layerManager: {
            type: 'layerManager' as DialogType,
            activeMapId: 'map2',
            isOpen: true,
          },
          'dimensionSelect-elevation': {
            type: 'dimensionSelect-elevation' as DialogType,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: [
          'legend',
          'layerManager',
          'dimensionSelect-elevation',
        ] as DialogType[],
      },
    };

    expect(uiSelectors.getDialogOrder(mockStore, 'legend')).toEqual(0);
    expect(uiSelectors.getDialogOrder(mockStore, 'layerManager')).toEqual(2);
    expect(
      uiSelectors.getDialogOrder(mockStore, 'dimensionSelect-elevation'),
    ).toEqual(1);
  });
});

describe('getDialogIsOrderedOnTop', () => {
  it('should return is ordered on top', () => {
    const mockStore = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },

          layerManager: {
            type: 'layerManager' as DialogType,
            activeMapId: 'map2',
            isOpen: true,
          },
        },
        order: ['legend', 'layerManager'] as DialogType[],
      },
    };

    expect(
      uiSelectors.getDialogIsOrderedOnTop(mockStore, 'legend'),
    ).toBeTruthy();
    expect(
      uiSelectors.getDialogIsOrderedOnTop(mockStore, 'layerManager'),
    ).toBeFalsy();

    // return false when not existing
    expect(
      uiSelectors.getDialogOrder(mockStore, 'dimensionSelect-elevation'),
    ).toBeFalsy();
    expect(uiSelectors.getDialogIsOrderedOnTop({}, 'layerManager')).toBeFalsy();
  });

  describe('getActiveWindowId', () => {
    const mockStore: UIModuleState = {
      ui: {
        dialogs: {
          legend: {
            type: 'legend' as DialogType,
            activeMapId: 'map1',
            isOpen: false,
          },
        },
        order: ['legend'] as DialogType[],
      },
    };
    it('should return active window id when it exists', () => {
      const activeWindowId = 'activeWindowId';
      const storeWithActiveWindowId = produce(mockStore, (draft) => {
        draft.ui.activeWindowId = activeWindowId;
      });
      expect(uiSelectors.getActiveWindowId(storeWithActiveWindowId)).toEqual(
        activeWindowId,
      );
    });
    it('should return undefined when active window id doesnt exist', () => {
      expect(uiSelectors.getActiveWindowId(mockStore)).toEqual(undefined);
    });
    it('should return undefined when store doesnt exist', () => {
      expect(uiSelectors.getActiveWindowId(null)).toEqual(undefined);
    });
  });
});
