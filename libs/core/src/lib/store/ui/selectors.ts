/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from '@reduxjs/toolkit';
import { AppStore } from '../../types/types';
import { DialogType, UIStoreType, UIType } from './types';

export const getUiStore = (store: AppStore): UIStoreType => {
  if (store && store.ui) {
    return store.ui;
  }
  return null;
};

/**
 * Gets the active map Id and wether a dialog is open or closed
 *
 * Example: getDialogDetailsByType(store, 'legend')
 * @param {object} store store object from which the ui state wll be extracted
 * @param {string} dialogType type of the dialog 'legend' | 'layerManager' | 'dimensionSelect-elevation'
 * @returns {array} [string, boolean] first element gives the active map id, second element whether the dialog is open
 */
export const getDialogDetailsByType = (
  store: AppStore,
  dialogType: string,
): UIType => {
  if (store && store.ui && store.ui.dialogs && store.ui.dialogs[dialogType]) {
    return store.ui.dialogs[dialogType];
  }
  return null;
};

/**
 * For a given ui component: gets wether the dialog is open or closed
 *
 * @param {object} store store object from which the ui state wll be extracted
 * @param {string} dialogType type of the dialog 'legend' | 'layerManager' | 'dimensionSelect-elevation'
 * @returns {boolean} returnType: boolean
 */
export const getisDialogOpen = createSelector(
  getDialogDetailsByType,
  (details): boolean => {
    if (details) {
      return details.isOpen;
    }
    return false;
  },
);

/**
 *For a given ui component: gets the active map Id
 *
 * @param {object} store store object from which the ui state wll be extracted
 * @param {string} dialogType type of the dialog 'legend' | 'layerManager' | 'dimensionSelect-elevation'
 * @returns {string} returnType: string - the active map id
 */
export const getDialogMapId = createSelector(
  getDialogDetailsByType,
  (details) => {
    if (details) {
      return details.activeMapId;
    }
    return '';
  },
);

/**
 *For a given ui component: gets the order of visible dialog
 *
 * @param {object} store store object from which the ui state wll be extracted
 * @param {DialogType} dialogType type of the dialog 'legend' | 'layerManager' | 'dimensionSelect-elevation' | dimensionSelect-ensemble_member
 * @returns {number} the higher the order number the higher the zIndex
 */
export const getDialogOrder = createSelector(
  (store: AppStore, dialogType: DialogType): number => {
    if (store && store.ui && store.ui.order) {
      const { order } = store.ui;
      const visibleOrder = order.filter(
        (orderedDialogType) => store.ui.dialogs[orderedDialogType].isOpen,
      );
      if (!visibleOrder.includes(dialogType)) {
        return 0;
      }
      return visibleOrder.length - visibleOrder.indexOf(dialogType);
    }
    return 0;
  },
  (order: number): number => order,
);

/**
 *For a given ui component: returns if ordered on top
 *
 * @param {object} store store object from which the ui state wll be extracted
 * @param {DialogType} dialogType type of the dialog 'legend' | 'layerManager' | 'dimensionSelect-elevation' | dimensionSelect-ensemble_member
 * @returns {boolean}
 */
export const getDialogIsOrderedOnTop = createSelector(
  (store: AppStore, dialogType: DialogType): boolean => {
    if (store && store.ui && store.ui.order) {
      const { order } = store.ui;

      return order.includes(dialogType) && order.indexOf(dialogType) === 0;
    }
    return false;
  },
  (isOrderedOnTop: boolean): boolean => isOrderedOnTop,
);

export const getDialogSource = createSelector(
  getDialogDetailsByType,
  (details) => {
    if (details && details.source) {
      return details.source;
    }
    return 'app';
  },
);

/**
 * Get the active window that should receive keyboard shortcuts
 *
 * Example: getActiveWindowId(store)
 * @param {object} store store object from which the window state will be extracted
 * @returns {string} active window id
 */
export const getActiveWindowId = (store: AppStore): string => {
  return store?.ui?.activeWindowId;
};
