/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from '@reduxjs/toolkit';
import { AppStore } from '../../types/types';
import { selectorMemoizationOptions } from '../utils';
import { LayerSelectStoreType, ActiveServices, KeywordObject } from './types';

const layerSelectStore = (store: AppStore): LayerSelectStoreType =>
  store && store.layerSelect ? store.layerSelect : null;

/**
 * Returns search filter string
 *
 * Example getSearchFilter(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: string
 */
export const getSearchFilter = createSelector(
  layerSelectStore,
  (store): string => (store ? store.filters.searchFilter : ''),
);

/**
 * Returns active services array
 *
 * Example getActiveServices(store);
 * @param {object} store store: object - store object
 * @returns {object} returnType: object of active services
 */
export const getActiveServices = createSelector(
  layerSelectStore,
  (store): ActiveServices => {
    return store ? store?.filters?.activeServices.byId : {};
  },
  selectorMemoizationOptions,
);

/**
 * Returns all ids of enabled services
 *
 * Example getEnabledServiceIds(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: array of service ids that are enabled
 */
export const getEnabledServiceIds = createSelector(
  layerSelectStore,
  (store): string[] => {
    const enabledServices =
      store?.filters?.activeServices.allIds.filter(
        (serviceId) => store?.filters?.activeServices.byId[serviceId].enabled,
      ) ?? [];
    return enabledServices;
  },
  selectorMemoizationOptions,
);

/**
 * Returns all ids of checked keywords
 *
 * Example getCheckedKeywordIds(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: array of keyword ids that are checked
 */
export const getCheckedKeywordIds = createSelector(
  layerSelectStore,
  (store): string[] => {
    const checkedKeywords =
      store?.filters?.keywords.allIds.filter(
        (keyword) => store?.filters?.keywords.byId[keyword].checked,
      ) ?? [];
    return checkedKeywords.map(
      (keyword) => store?.filters?.keywords.byId[keyword].id,
    );
  },
  selectorMemoizationOptions,
);

/**
 * Returns ids of all keywords
 *
 * Example getAllKeywordIds(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: array of all keyword ids
 */
export const getAllKeywordIds = createSelector(
  layerSelectStore,
  (store): string[] => store?.filters?.keywords.allIds ?? [],
  selectorMemoizationOptions,
);

/**
 * Returns if all keywords are checked
 *
 * Example isAllKeywordsChecked(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: boolean if all keywords are checked
 */
export const isAllKeywordsChecked = createSelector(
  layerSelectStore,
  (store): boolean => {
    return store?.filters?.keywords?.allIds.every(
      (keyword) => store?.filters?.keywords?.byId[keyword].checked,
    );
  },
);

/**
 * Gets a KeywordObject by its id
 *
 * Example: keywordObject = getKeywordObjectById(store, 'keywordId')
 * @param {object} store object from which the keyword state will be extracted
 * @param {string} keywordId Id of the keyword
 * @returns {object} object containing keyword information (id, amount, amountVisible, checked)
 */
export const getKeywordObjectById = (
  store: AppStore,
  keywordId: string,
): KeywordObject => {
  return store?.layerSelect?.filters?.keywords?.byId[keywordId] ?? {};
};
