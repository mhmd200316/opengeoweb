/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  AddKeywordsAndActiveServicesPayload,
  DisableActiveServicePayload,
  EnableActiveServicePayload,
  EnableOnlyOneKeywordPayload,
  LayerSelectServiceRemovedPayload,
  LayerSelectStoreType,
  SetSearchFilterPayload,
  ToggleKeywordsPayload,
} from './types';

export const initialState: LayerSelectStoreType = {
  filters: {
    searchFilter: '',
    activeServices: {
      byId: {},
      allIds: [],
    },
    keywords: {
      byId: {},
      allIds: [],
    },
  },
};

const slice = createSlice({
  initialState,
  name: 'layerSelect',
  reducers: {
    setSearchFilter: (draft, action: PayloadAction<SetSearchFilterPayload>) => {
      const { filterText } = action.payload;
      draft.filters.searchFilter = filterText;
    },
    addKeywordsAndActiveServices: (
      draft,
      action: PayloadAction<AddKeywordsAndActiveServicesPayload>,
    ) => {
      const {
        serviceId,
        serviceName,
        serviceUrl,
        keywords,
        isUserAddedService,
      } = action.payload;
      if (!draft.filters.activeServices.byId[serviceId]) {
        draft.filters.activeServices.byId[serviceId] = {};
        for (const keyword of keywords) {
          if (!draft.filters.keywords.byId[keyword]) {
            draft.filters.keywords.byId[keyword] = {
              id: keyword,
              amount: 1,
              amountVisible: 1,
              checked: true,
            };
            draft.filters.keywords.allIds.push(keyword);
          } else {
            draft.filters.keywords.byId[keyword].amount += 1;
            draft.filters.keywords.byId[keyword].amountVisible += 1;
          }
        }
      }
      draft.filters.activeServices.byId[serviceId].serviceName = serviceName;
      draft.filters.activeServices.byId[serviceId].serviceUrl = serviceUrl;
      draft.filters.activeServices.byId[serviceId].enabled = true;
      draft.filters.activeServices.byId[serviceId].isUserAddedService =
        isUserAddedService;
      draft.filters.activeServices.byId[serviceId].keywordsPerService =
        keywords;
      if (!draft.filters.activeServices.allIds.includes(serviceId))
        draft.filters.activeServices.allIds.push(serviceId);
    },
    layerSelectServiceRemoved: (
      draft,
      action: PayloadAction<LayerSelectServiceRemovedPayload>,
    ) => {
      const { serviceId } = action.payload;
      // 1. Find a service that is to be removed
      const foundService = draft.filters.activeServices.byId[serviceId];

      // 2. Go through all keywords for removed service, and decrement the amount of all found keywords
      for (const keyword of foundService.keywordsPerService) {
        const foundObject = draft.filters.keywords.byId[keyword];
        if (foundObject && foundObject.amount) {
          foundObject.amount -= 1;
          foundObject.amountVisible -= 1;
        }
        if (foundObject && foundObject.amount === 0) {
          delete draft.filters.keywords.byId[keyword];
          draft.filters.keywords.allIds = draft.filters.keywords.allIds.filter(
            (serviceId) => serviceId !== keyword,
          );
        }
      }
      // Finally remove object from activeServices, so if the service is added again later on, new object and keywords will be re-added
      delete draft.filters.activeServices.byId[serviceId];
      draft.filters.activeServices.allIds =
        draft.filters.activeServices.allIds.filter((id) => id !== serviceId);
    },
    enableActiveService: (
      draft,
      action: PayloadAction<EnableActiveServicePayload>,
    ) => {
      const { serviceId, keywords } = action.payload;
      draft.filters.activeServices.byId[serviceId].enabled = true;
      for (const keyword of keywords) {
        draft.filters.keywords.byId[keyword].amountVisible += 1;
      }
    },
    disableActiveService: (
      draft,
      action: PayloadAction<DisableActiveServicePayload>,
    ) => {
      const { serviceId, keywords } = action.payload;
      draft.filters.activeServices.byId[serviceId].enabled = false;
      for (const keyword of keywords) {
        draft.filters.keywords.byId[keyword].amountVisible -= 1;
      }
    },
    toggleKeywords: (draft, action: PayloadAction<ToggleKeywordsPayload>) => {
      const { keywords } = action.payload;
      for (const keyword of keywords) {
        draft.filters.keywords.byId[keyword].checked =
          !draft.filters.keywords.byId[keyword].checked;
      }
    },
    enableOnlyOneKeyword: (
      draft,
      action: PayloadAction<EnableOnlyOneKeywordPayload>,
    ) => {
      const { keyword } = action.payload;
      draft.filters.keywords.allIds.forEach((serviceId) => {
        draft.filters.keywords.byId[serviceId].checked = false;
      });
      draft.filters.keywords.byId[keyword].checked = true;
    },
  },
});

export const { reducer } = slice;
export const layerSelectActions = slice.actions;
