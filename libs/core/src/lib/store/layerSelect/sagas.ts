/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { put, takeEvery } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import {
  SetLayersForServicePayload,
  MapStoreRemoveServicePayload,
} from '../mapStore/types';
import {
  getUserAddedServices,
  setUserAddedServices,
} from '../../utils/localStorage';
import { ServiceActions, serviceActions } from '../mapStore/service/reducer';
import { layerSelectActions } from './reducer';

export function* newServiceAddedSaga(
  capturedAction: ServiceActions,
): SagaIterator {
  const addedService = capturedAction.payload as SetLayersForServicePayload;
  const { id, serviceUrl, name, isUserAddedService } = addedService;

  const keywords = addedService.layers.reduce((keywordAccumulator, layer) => {
    return keywordAccumulator.concat(layer.path, layer.keywords);
  }, []);

  yield put(
    layerSelectActions.addKeywordsAndActiveServices({
      serviceId: id,
      serviceName: name,
      serviceUrl,
      keywords,
      isUserAddedService,
    }),
  );
}

export function* layerSelectServiceRemovedSaga(
  capturedAction: ServiceActions,
): SagaIterator {
  const removedService = capturedAction.payload as MapStoreRemoveServicePayload;
  const { id: serviceId } = removedService;
  yield put(layerSelectActions.layerSelectServiceRemoved({ serviceId }));
}

export function addServiceToLocalStorageSaga({
  payload,
}: ReturnType<typeof serviceActions.serviceSetLayers>): void {
  const { isUserAddedService, name, serviceUrl } = payload;
  if (!isUserAddedService) return;
  const localStorageServices = getUserAddedServices();
  setUserAddedServices({
    ...localStorageServices,
    [serviceUrl]: {
      name,
      url: serviceUrl,
    },
  });
}
export function removeServiceFromLocalStorageSaga({
  payload,
}: ReturnType<typeof serviceActions.mapStoreRemoveService>): void {
  const { serviceUrl } = payload;
  const localStorageServices = getUserAddedServices();
  if (!localStorageServices[serviceUrl]) return;

  const updatedServices = { ...localStorageServices };
  delete updatedServices[serviceUrl];

  setUserAddedServices(updatedServices);
}

export function* rootSaga(): SagaIterator {
  yield takeEvery(serviceActions.serviceSetLayers.type, newServiceAddedSaga);
  yield takeEvery(
    serviceActions.mapStoreRemoveService.type,
    layerSelectServiceRemovedSaga,
  );
  yield takeEvery(
    serviceActions.serviceSetLayers.type,
    addServiceToLocalStorageSaga,
  );
  yield takeEvery(
    serviceActions.mapStoreRemoveService.type,
    removeServiceFromLocalStorageSaga,
  );
}

export default rootSaga;
