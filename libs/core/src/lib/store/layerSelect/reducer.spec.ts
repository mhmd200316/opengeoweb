/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { reducer, initialState, layerSelectActions } from './reducer';

describe('store/layerSelect/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  describe('searchFilter', () => {
    it('should update search filter when search filter is passed in', () => {
      const filterText = 'searchText';
      const action = layerSelectActions.setSearchFilter({ filterText });
      expect(reducer(initialState, action)).toEqual({
        filters: {
          searchFilter: filterText,
          keywords: {
            byId: {},
            allIds: [],
          },
          activeServices: {
            byId: {},
            allIds: [],
          },
        },
      });
    });
  });
  describe('keywords', () => {
    it('should calculate the amount of same keywords and create new objects for each new keyword', () => {
      const action = layerSelectActions.addKeywordsAndActiveServices({
        serviceId: 'serviceid_1',
        serviceName: 'FMI',
        serviceUrl: 'https://testservice1',
        keywords: ['keyword1', 'keyword2', 'keyword2'],
        isUserAddedService: false,
      });

      expect(reducer(initialState, action)).toEqual({
        filters: {
          searchFilter: '',
          keywords: {
            byId: {
              keyword1: {
                id: 'keyword1',
                amount: 1,
                amountVisible: 1,
                checked: true,
              },
              keyword2: {
                id: 'keyword2',
                amount: 2,
                amountVisible: 2,
                checked: true,
              },
            },
            allIds: ['keyword1', 'keyword2'],
          },
          activeServices: {
            byId: {
              serviceid_1: {
                serviceName: 'FMI',
                serviceUrl: 'https://testservice1',
                enabled: true,
                keywordsPerService: ['keyword1', 'keyword2', 'keyword2'],
                isUserAddedService: false,
              },
            },
            allIds: ['serviceid_1'],
          },
        },
      });
    });

    it('should add new object to keywords and activeServices, if no keyword found from state', () => {
      const action = layerSelectActions.addKeywordsAndActiveServices({
        serviceId: 'serviceid_1',
        serviceName: 'FMI',
        serviceUrl: 'https://testservice1',
        keywords: ['keyword1', 'keyword2'],
        isUserAddedService: false,
      });

      expect(reducer(initialState, action)).toEqual({
        filters: {
          searchFilter: '',
          keywords: {
            byId: {
              keyword1: {
                id: 'keyword1',
                amount: 1,
                amountVisible: 1,
                checked: true,
              },
              keyword2: {
                id: 'keyword2',
                amount: 1,
                amountVisible: 1,
                checked: true,
              },
            },
            allIds: ['keyword1', 'keyword2'],
          },
          activeServices: {
            byId: {
              serviceid_1: {
                serviceName: 'FMI',
                serviceUrl: 'https://testservice1',
                enabled: true,
                keywordsPerService: ['keyword1', 'keyword2'],
                isUserAddedService: false,
              },
            },
            allIds: ['serviceid_1'],
          },
        },
      });
    });

    it('should increment amount for same keywords and add a new service to activeServices', () => {
      const state = {
        filters: {
          searchFilter: '',
          keywords: {
            byId: {
              keyword1: {
                id: 'keyword1',
                amount: 1,
                amountVisible: 1,
                checked: true,
              },
            },
            allIds: ['keyword1'],
          },
          activeServices: {
            byId: {
              serviceid_1: {
                serviceName: 'FMI',
                serviceUrl: 'https://testservice1',
                enabled: true,
                keywordsPerService: ['keyword1'],
                isUserAddedService: false,
              },
            },
            allIds: ['serviceid_1'],
          },
        },
      };

      const action = layerSelectActions.addKeywordsAndActiveServices({
        serviceId: 'serviceid_2',
        serviceName: 'METNO',
        serviceUrl: 'https://testservice2',
        keywords: ['keyword1'],
        isUserAddedService: false,
      });

      expect(reducer(state, action)).toEqual({
        filters: {
          searchFilter: '',
          keywords: {
            byId: {
              keyword1: {
                id: 'keyword1',
                amount: 2,
                amountVisible: 2,
                checked: true,
              },
            },
            allIds: ['keyword1'],
          },
          activeServices: {
            byId: {
              serviceid_1: {
                serviceName: 'FMI',
                serviceUrl: 'https://testservice1',
                enabled: true,
                keywordsPerService: ['keyword1'],
                isUserAddedService: false,
              },
              serviceid_2: {
                serviceName: 'METNO',
                serviceUrl: 'https://testservice2',
                enabled: true,
                keywordsPerService: ['keyword1'],
                isUserAddedService: false,
              },
            },
            allIds: ['serviceid_1', 'serviceid_2'],
          },
        },
      });
    });

    it('should not add anything if keywords are already added for given service', () => {
      const state = {
        filters: {
          searchFilter: '',
          keywords: {
            byId: {
              keyword1: {
                id: 'keyword1',
                amount: 1,
                amountVisible: 1,
                checked: true,
              },
              keyword2: {
                id: 'keyword2',
                amount: 1,
                amountVisible: 1,
                checked: true,
              },
            },
            allIds: ['keyword1', 'keyword2'],
          },
          activeServices: {
            byId: {
              serviceid_1: {
                serviceName: 'FMI',
                serviceUrl: 'https://testurl1',
                enabled: true,
                keywordsPerService: ['keyword1', 'keyword2'],
                isUserAddedService: false,
              },
            },
            allIds: ['serviceid_1'],
          },
        },
      };

      const action = layerSelectActions.addKeywordsAndActiveServices({
        serviceId: 'serviceid_1',
        serviceName: 'FMI',
        serviceUrl: 'https://testurl1',
        keywords: ['keyword1', 'keyword2'],
        isUserAddedService: false,
      });
      expect(reducer(state, action)).toEqual(state);
    });
  });

  describe('activeServices', () => {
    const stateAllActive = {
      filters: {
        searchFilter: '',
        keywords: {
          byId: {
            keyword1: {
              id: 'keyword1',
              amount: 3,
              amountVisible: 3,
              checked: true,
            },
          },
          allIds: ['keyword1'],
        },
        activeServices: {
          byId: {
            serviceid_1: {
              serviceName: 'FMI',
              serviceUrl: 'https://testurl1',
              enabled: true,
              keywordsPerService: ['keyword1'],
              isUserAddedService: false,
            },
            serviceid_2: {
              serviceName: 'METNO',
              serviceUrl: 'https://testurl2',
              enabled: true,
              keywordsPerService: ['keyword1'],
              isUserAddedService: false,
            },
            serviceid_3: {
              serviceName: 'KNMI',
              serviceUrl: 'https://testurl3',
              enabled: true,
              keywordsPerService: ['keyword1'],
              isUserAddedService: false,
            },
          },
          allIds: ['serviceid_1', 'serviceid_2', 'serviceid_3'],
        },
      },
    };
    const stateAllDisabled = {
      filters: {
        searchFilter: '',
        keywords: {
          byId: {
            keyword1: {
              id: 'keyword1',
              amount: 3,
              amountVisible: 0,
              checked: true,
            },
          },
          allIds: ['keyword1'],
        },
        activeServices: {
          byId: {
            serviceid_1: {
              serviceName: 'FMI',
              serviceUrl: 'https://testurl1',
              enabled: false,
              keywordsPerService: ['keyword1'],
              isUserAddedService: false,
            },
            serviceid_2: {
              serviceName: 'METNO',
              serviceUrl: 'https://testurl2',
              enabled: false,
              keywordsPerService: ['keyword1'],
              isUserAddedService: false,
            },
            serviceid_3: {
              serviceName: 'KNMI',
              serviceUrl: 'https://testurl3',
              enabled: false,
              keywordsPerService: ['keyword1'],
              isUserAddedService: false,
            },
          },
          allIds: ['serviceid_1', 'serviceid_2', 'serviceid_3'],
        },
      },
    };

    it('should add new services to activeServices list and increment the amountVisible for given keywords', () => {
      const action = layerSelectActions.enableActiveService({
        serviceId: 'serviceid_1',
        keywords: ['keyword1'],
      });
      expect(reducer(stateAllDisabled, action)).toEqual({
        filters: {
          searchFilter: '',
          keywords: {
            byId: {
              keyword1: {
                id: 'keyword1',
                amount: 3,
                amountVisible: 1,
                checked: true,
              },
            },
            allIds: ['keyword1'],
          },
          activeServices: {
            byId: {
              serviceid_1: {
                serviceName: 'FMI',
                serviceUrl: 'https://testurl1',
                enabled: true,
                keywordsPerService: ['keyword1'],
                isUserAddedService: false,
              },
              serviceid_2: {
                serviceName: 'METNO',
                serviceUrl: 'https://testurl2',
                enabled: false,
                keywordsPerService: ['keyword1'],
                isUserAddedService: false,
              },
              serviceid_3: {
                serviceName: 'KNMI',
                serviceUrl: 'https://testurl3',
                enabled: false,
                keywordsPerService: ['keyword1'],
                isUserAddedService: false,
              },
            },
            allIds: ['serviceid_1', 'serviceid_2', 'serviceid_3'],
          },
        },
      });
    });

    it('should disable service in activeServices and decrement amountVisible for given keywords', () => {
      const action = layerSelectActions.disableActiveService({
        serviceId: 'serviceid_1',
        keywords: ['keyword1'],
      });
      expect(reducer(stateAllActive, action)).toEqual({
        filters: {
          searchFilter: '',
          keywords: {
            byId: {
              keyword1: {
                id: 'keyword1',
                amount: 3,
                amountVisible: 2,
                checked: true,
              },
            },
            allIds: ['keyword1'],
          },
          activeServices: {
            byId: {
              serviceid_1: {
                serviceName: 'FMI',
                serviceUrl: 'https://testurl1',
                enabled: false,
                keywordsPerService: ['keyword1'],
                isUserAddedService: false,
              },
              serviceid_2: {
                serviceName: 'METNO',
                serviceUrl: 'https://testurl2',
                enabled: true,
                keywordsPerService: ['keyword1'],
                isUserAddedService: false,
              },
              serviceid_3: {
                serviceName: 'KNMI',
                serviceUrl: 'https://testurl3',
                enabled: true,
                keywordsPerService: ['keyword1'],
                isUserAddedService: false,
              },
            },
            allIds: ['serviceid_1', 'serviceid_2', 'serviceid_3'],
          },
        },
      });
    });

    it('shouldnt change anything when enabling already enabled service', () => {
      const action = layerSelectActions.enableActiveService({
        serviceId: 'serviceid_1',
        keywords: [],
      });
      expect(reducer(stateAllActive, action)).toEqual(stateAllActive);
    });

    it('shouldnt change anything when disabling already disabled service', () => {
      const action = layerSelectActions.disableActiveService({
        serviceId: 'serviceid_1',
        keywords: [],
      });
      expect(reducer(stateAllDisabled, action)).toEqual(stateAllDisabled);
    });

    it('should remove service in activeServices', () => {
      const action = layerSelectActions.layerSelectServiceRemoved({
        serviceId: 'serviceid_1',
      });
      expect(reducer(stateAllActive, action)).toEqual({
        filters: {
          searchFilter: '',
          keywords: {
            byId: {
              keyword1: {
                id: 'keyword1',
                amount: 2,
                amountVisible: 2,
                checked: true,
              },
            },
            allIds: ['keyword1'],
          },
          activeServices: {
            byId: {
              serviceid_2: {
                serviceName: 'METNO',
                serviceUrl: 'https://testurl2',
                enabled: true,
                keywordsPerService: ['keyword1'],
                isUserAddedService: false,
              },
              serviceid_3: {
                serviceName: 'KNMI',
                serviceUrl: 'https://testurl3',
                enabled: true,
                keywordsPerService: ['keyword1'],
                isUserAddedService: false,
              },
            },
            allIds: ['serviceid_2', 'serviceid_3'],
          },
        },
      });
    });

    describe('toggleKeywords and enableOnlyOneKeyword', () => {
      const state = {
        filters: {
          searchFilter: '',
          keywords: {
            byId: {
              keyword1: {
                id: 'keyword1',
                amount: 1,
                amountVisible: 1,
                checked: true,
              },
              keyword2: {
                id: 'keyword2',
                amount: 1,
                amountVisible: 1,
                checked: true,
              },
            },
            allIds: ['keyword1', 'keyword2'],
          },
          activeServices: {
            byId: {},
            allIds: [],
          },
        },
      };
      const state2 = {
        filters: {
          searchFilter: '',
          keywords: {
            byId: {
              keyword1: {
                id: 'keyword1',
                amount: 1,
                amountVisible: 1,
                checked: false,
              },
              keyword2: {
                id: 'keyword2',
                amount: 1,
                amountVisible: 1,
                checked: false,
              },
            },
            allIds: ['keyword1', 'keyword2'],
          },
          activeServices: {
            byId: {},
            allIds: [],
          },
        },
      };

      const state3 = {
        filters: {
          searchFilter: '',
          keywords: {
            byId: {
              keyword1: {
                id: 'keyword1',
                amount: 1,
                amountVisible: 1,
                checked: true,
              },
              keyword2: {
                id: 'keyword2',
                amount: 1,
                amountVisible: 1,
                checked: false,
              },
            },
            allIds: ['keyword1', 'keyword2'],
          },
          activeServices: {
            byId: {},
            allIds: [],
          },
        },
      };
      it('should toggle listed keywords', () => {
        const action = layerSelectActions.toggleKeywords({
          keywords: ['keyword1', 'keyword2'],
        });
        expect(reducer(state, action)).toEqual(state2);
        expect(reducer(state2, action)).toEqual(state);
      });

      it('should enable only clicked keyword when clicked only', () => {
        const action = layerSelectActions.enableOnlyOneKeyword({
          keyword: 'keyword1',
        });
        expect(reducer(state, action)).toEqual(state3);
        expect(reducer(state2, action)).toEqual(state3);
      });
    });
  });
});
