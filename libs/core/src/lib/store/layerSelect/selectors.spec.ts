/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as selectors from './selectors';

const mockSearchText = 'searchText';
const mockKeywords = {
  byId: {
    keyword1: { id: 'keyword1', amount: 1, amountVisible: 1, checked: true },
    keyword2: { id: 'keyword2', amount: 1, amountVisible: 1, checked: true },
    keyword3: { id: 'keyword3', amount: 1, amountVisible: 1, checked: false },
  },
  allIds: ['keyword1', 'keyword2', 'keyword3'],
};
const mockActiveServices = {
  byId: {
    service1: { id: 'service1', enabled: true, keywordsPerService: [] },
    service2: { id: 'service2', enabled: true, keywordsPerService: [] },
    service3: { id: 'service3', enabled: false, keywordsPerService: [] },
  },
  allIds: ['service1', 'service2', 'service3'],
};
const mockState = {
  layerSelect: {
    filters: {
      searchFilter: mockSearchText,
      keywords: mockKeywords,
      activeServices: mockActiveServices,
    },
  },
};

describe('store/layerSelect/selectors', () => {
  describe('getSearchFilter', () => {
    it('should return the current search filter', () => {
      expect(selectors.getSearchFilter(mockState)).toBe(mockSearchText);
    });

    it('should return an empty string when the store does not exist', () => {
      expect(selectors.getSearchFilter(null)).toBe('');
    });

    it('should return empty string when search text is empty string', () => {
      expect(
        selectors.getSearchFilter({
          layerSelect: {
            filters: {
              searchFilter: '',
              keywords: {
                byId: {},
                allIds: [],
              },
              activeServices: {
                byId: {},
                allIds: [],
              },
            },
          },
        }),
      ).toBe('');
    });
  });

  describe('getActiveServices', () => {
    it('should return an array of objects of active services', () => {
      expect(selectors.getActiveServices(mockState)).toBe(
        mockActiveServices.byId,
      );
    });
  });

  describe('getEnabledServiceIds', () => {
    it('should return an array of string of enabled services', () => {
      expect(selectors.getEnabledServiceIds(mockState)).toStrictEqual([
        'service1',
        'service2',
      ]);
    });
  });

  describe('getCheckedKeywordIds', () => {
    it('should return an array of string of checked keywords', () => {
      expect(selectors.getCheckedKeywordIds(mockState)).toStrictEqual([
        'keyword1',
        'keyword2',
      ]);
    });
  });

  describe('getAllKeywordIds', () => {
    it('should return an array of string of all keywords', () => {
      expect(selectors.getAllKeywordIds(mockState)).toStrictEqual(
        mockKeywords.allIds,
      );
    });
  });

  describe('isAllKeywordsChecked', () => {
    it('should return false that indicates that all keywords are not checked', () => {
      expect(selectors.isAllKeywordsChecked(mockState)).toStrictEqual(false);
    });

    it('should return boolean that indicates that all keywords are checked', () => {
      mockKeywords.byId['keyword3'].checked = true;
      const mockState2 = {
        layerSelect: {
          filters: {
            searchFilter: mockSearchText,
            keywords: mockKeywords,
            activeServices: mockActiveServices,
          },
        },
      };
      expect(selectors.isAllKeywordsChecked(mockState2)).toStrictEqual(true);
    });
  });

  describe('getKeywordObjectById', () => {
    it('should return a keywordObject', () => {
      expect(
        selectors.getKeywordObjectById(mockState, 'keyword1'),
      ).toStrictEqual(mockKeywords.byId['keyword1']);
    });

    it('should return empty object if keyword not found', () => {
      expect(
        selectors.getKeywordObjectById(mockState, 'keyword4'),
      ).toStrictEqual({});
    });
  });
});
