/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

export interface ActiveServiceObject {
  serviceName?: string;
  serviceUrl?: string;
  enabled?: boolean;
  keywordsPerService?: string[];
  isUserAddedService?: boolean;
}

export interface ActiveServices {
  [key: string]: ActiveServiceObject;
}

export interface ActiveServiceType {
  byId: ActiveServices;
  allIds: string[];
}

export interface KeywordObject {
  id?: string;
  amount?: number;
  amountVisible?: number;
  checked?: boolean;
}

export interface Keywords {
  [key: string]: KeywordObject;
}

export interface KeywordType {
  byId: Keywords;
  allIds: string[];
}
export interface LayerSelectModuleState {
  layerSelect?: LayerSelectStoreType;
}

export interface LayerSelectStoreType {
  filters: FiltersType;
}

export interface FiltersType {
  searchFilter: string;
  activeServices: ActiveServiceType;
  keywords: KeywordType;
}

// actions
export interface SetSearchFilterPayload {
  filterText: string;
}

export interface EnableActiveServicePayload {
  serviceId: string;
  keywords: string[];
}

export interface DisableActiveServicePayload {
  serviceId: string;
  keywords: string[];
}

export interface AddKeywordsAndActiveServicesPayload {
  serviceId: string;
  serviceName: string;
  serviceUrl: string;
  keywords: string[];
  isUserAddedService: boolean;
}

export interface LayerSelectServiceRemovedPayload {
  serviceId: string;
}

export interface ToggleKeywordsPayload {
  keywords: string[];
}

export interface EnableOnlyOneKeywordPayload {
  keyword: string;
}
