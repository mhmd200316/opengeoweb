/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { put, takeEvery } from 'redux-saga/effects';
import {
  rootSaga,
  newServiceAddedSaga,
  layerSelectServiceRemovedSaga,
  addServiceToLocalStorageSaga,
  removeServiceFromLocalStorageSaga,
} from './sagas';
import { layerSelectActions } from './reducer';
import { UserAddedServices } from '../../utils/types';
import { setUserAddedServices } from '../../utils/localStorage';
import { serviceActions } from '../mapStore';

const { mapStoreRemoveService, serviceSetLayers } = serviceActions;
const { addKeywordsAndActiveServices, layerSelectServiceRemoved } =
  layerSelectActions;

beforeEach(() => {
  window.localStorage.clear();
});

describe('store/layerSelect/sagas', () => {
  describe('rootSaga', () => {
    it('should take action and fire correct sagas', () => {
      const generator = rootSaga();
      expect(generator.next().value).toEqual(
        takeEvery(serviceSetLayers.type, newServiceAddedSaga),
      );
    });
  });

  describe('newServiceAddedSaga', () => {
    it('should handle newServiceAdded Saga with keywords and group layers', () => {
      const DummyAction = serviceSetLayers({
        id: 'id_1',
        layers: [
          {
            abstract: '500m CAPPI images from Vihti radar',
            keywords: ['WCS', 'ImageMosaic', 'radar_vihti_dbzh-cappi_3067'],
            leaf: true,
            name: 'Radar:radar_vihti_dbzh-cappi_3067',
            path: ['Radar', 'Vihti'],
            text: '500m CAPPI images from Vihti radar',
          },
        ],
        name: 'FMI',
        serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
        isUserAddedService: false,
      });

      const generator = newServiceAddedSaga(DummyAction);
      const layer = DummyAction.payload.layers[0];
      const keywords = layer.path.concat(layer.keywords);
      const results = generator.next(keywords).value;

      expect(results).toEqual(
        put(
          addKeywordsAndActiveServices({
            serviceId: DummyAction.payload.id,
            serviceName: DummyAction.payload.name,
            serviceUrl: DummyAction.payload.serviceUrl,
            keywords,
            isUserAddedService: false,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  it('should handle newServiceAdded Saga without keywords', () => {
    const DummyAction = serviceSetLayers({
      id: 'id_1',
      layers: [
        {
          abstract: '500m CAPPI images from Vihti radar',
          leaf: true,
          name: 'Radar:radar_vihti_dbzh-cappi_3067',
          path: ['Radar', 'Vihti'],
          text: '500m CAPPI images from Vihti radar',
        },
      ],
      name: 'FMI',
      serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
      isUserAddedService: false,
    });

    const generator = newServiceAddedSaga(DummyAction);
    const layer = DummyAction.payload.layers[0];
    const keywords = layer.path.concat(layer.keywords);
    const results = generator.next(keywords).value;

    expect(results).toEqual(
      put(
        addKeywordsAndActiveServices({
          serviceId: DummyAction.payload.id,
          serviceName: DummyAction.payload.name,
          serviceUrl: DummyAction.payload.serviceUrl,
          keywords,
          isUserAddedService: false,
        }),
      ),
    );
    expect(generator.next().done).toBeTruthy();
  });

  it('should handle newServiceAdded Saga without group layers', () => {
    const DummyAction = serviceSetLayers({
      id: 'id_1',
      layers: [
        {
          abstract: '500m CAPPI images from Vihti radar',
          keywords: ['WCS', 'ImageMosaic', 'radar_vihti_dbzh-cappi_3067'],
          leaf: false,
          name: 'Radar:radar_vihti_dbzh-cappi_3067',
          path: [],
          text: '500m CAPPI images from Vihti radar',
        },
      ],
      name: 'FMI',
      serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
      isUserAddedService: false,
    });

    const generator = newServiceAddedSaga(DummyAction);
    const layer = DummyAction.payload.layers[0];
    const keywords = layer.path.concat(layer.keywords);
    const results = generator.next(keywords).value;

    expect(results).toEqual(
      put(
        addKeywordsAndActiveServices({
          serviceId: DummyAction.payload.id,
          serviceName: DummyAction.payload.name,
          serviceUrl: DummyAction.payload.serviceUrl,
          keywords,
          isUserAddedService: false,
        }),
      ),
    );
    expect(generator.next().done).toBeTruthy();
  });

  it('should handle newServiceAdded Saga without group layers and keywords', () => {
    const DummyAction = serviceSetLayers({
      id: 'id_1',
      layers: [
        {
          abstract: '500m CAPPI images from Vihti radar',
          keywords: null,
          leaf: false,
          name: 'Radar:radar_vihti_dbzh-cappi_3067',
          path: [],
          text: '500m CAPPI images from Vihti radar',
        },
      ],
      name: 'FMI',
      serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
      isUserAddedService: false,
    });

    const generator = newServiceAddedSaga(DummyAction);
    const layer = DummyAction.payload.layers[0];
    const keywords = [].concat(layer.path, layer.keywords);
    const results = generator.next(keywords).value;

    expect(results).toEqual(
      put(
        addKeywordsAndActiveServices({
          serviceId: DummyAction.payload.id,
          serviceName: DummyAction.payload.name,
          serviceUrl: DummyAction.payload.serviceUrl,
          keywords,
          isUserAddedService: false,
        }),
      ),
    );
    expect(generator.next().done).toBeTruthy();
  });

  describe('layerSelectServiceRemovedSaga', () => {
    it('should handle layerSelectServiceRemoved Saga', () => {});
    const DummyAction = mapStoreRemoveService({
      id: 'id_1',
      serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
    });

    const generator = layerSelectServiceRemovedSaga(DummyAction);
    const results = generator.next().value;
    expect(results).toEqual(
      put(
        layerSelectServiceRemoved({
          serviceId: 'id_1',
        }),
      ),
    );
    expect(generator.next().done).toBeTruthy();
  });

  describe('addServiceToLocalStorageSaga', () => {
    it('should not write non-user-added service to localstorage', () => {
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      const actionWithUserAddedService = serviceSetLayers({
        id: 'id_1',
        layers: [
          {
            abstract: '500m CAPPI images from Vihti radar',
            keywords: null,
            leaf: false,
            name: 'Radar:radar_vihti_dbzh-cappi_3067',
            path: [],
            text: '500m CAPPI images from Vihti radar',
          },
        ],
        name: 'FMI',
        serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
        isUserAddedService: false,
      });
      addServiceToLocalStorageSaga(actionWithUserAddedService);
      expect(window.localStorage.setItem).not.toHaveBeenCalled();
    });

    it('should write user-added service to localstorage', () => {
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      const actionWithUserAddedService = serviceSetLayers({
        id: 'id_1',
        layers: [
          {
            abstract: '500m CAPPI images from Vihti radar',
            keywords: null,
            leaf: false,
            name: 'Radar:radar_vihti_dbzh-cappi_3067',
            path: [],
            text: '500m CAPPI images from Vihti radar',
          },
        ],
        name: 'FMI',
        serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
        isUserAddedService: true,
      });
      expect(window.localStorage.setItem).not.toHaveBeenCalled();
      addServiceToLocalStorageSaga(actionWithUserAddedService);
      expect(window.localStorage.setItem).toHaveBeenCalled();
      expect(
        JSON.parse(window.localStorage.getItem('userAddedServices')),
      ).toEqual({
        'https://openwms.fmi.fi/geoserver/wms?': {
          name: 'FMI',
          url: 'https://openwms.fmi.fi/geoserver/wms?',
        },
      });
    });

    it('should correctly write two user-added services to localstorage', () => {
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      const actionWithUserAddedService1 = serviceSetLayers({
        id: 'id_1',
        layers: [
          {
            abstract: '500m CAPPI images from Vihti radar',
            keywords: null,
            leaf: false,
            name: 'Radar:radar_vihti_dbzh-cappi_3067',
            path: [],
            text: '500m CAPPI images from Vihti radar',
          },
        ],
        name: 'FMI',
        serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
        isUserAddedService: true,
      });
      const actionWithUserAddedService2 = serviceSetLayers({
        id: 'id_1',
        layers: [],
        name: 'test name',
        serviceUrl: 'https://testservice.fi/wms?',
        isUserAddedService: true,
      });
      addServiceToLocalStorageSaga(actionWithUserAddedService1);
      addServiceToLocalStorageSaga(actionWithUserAddedService2);
      expect(
        JSON.parse(window.localStorage.getItem('userAddedServices')),
      ).toEqual({
        'https://openwms.fmi.fi/geoserver/wms?': {
          name: 'FMI',
          url: 'https://openwms.fmi.fi/geoserver/wms?',
          isUserAddedService: true,
        },
        'https://testservice.fi/wms?': {
          name: 'test name',
          url: 'https://testservice.fi/wms?',
        },
      });
    });

    it('should not write duplicate user-added services to localstorage', () => {
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      const actionWithUserAddedService = serviceSetLayers({
        id: 'id_1',
        layers: [
          {
            abstract: '500m CAPPI images from Vihti radar',
            keywords: null,
            leaf: false,
            name: 'Radar:radar_vihti_dbzh-cappi_3067',
            path: [],
            text: '500m CAPPI images from Vihti radar',
          },
        ],
        name: 'FMI',
        serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
        isUserAddedService: true,
      });
      // call the function twice with the same action
      addServiceToLocalStorageSaga(actionWithUserAddedService);
      addServiceToLocalStorageSaga(actionWithUserAddedService);

      expect(
        JSON.parse(window.localStorage.getItem('userAddedServices')),
      ).toEqual({
        'https://openwms.fmi.fi/geoserver/wms?': {
          name: 'FMI',
          url: 'https://openwms.fmi.fi/geoserver/wms?',
        },
      });
    });
  });

  describe('deleteServiceFromLocalStorageSaga', () => {
    it('should do nothing when service to delete does not exist', () => {
      const initialUserAddedServices: UserAddedServices = {
        'https://testservice.fi/wms?': {
          name: 'test name',
          url: 'https://testservice.fi/wms?',
        },
      };
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      setUserAddedServices(initialUserAddedServices);

      const deleteServiceAction = mapStoreRemoveService({
        id: 'id_1',
        serviceUrl: 'https://wrongtestservice.fi/wms?',
      });
      expect(window.localStorage.setItem).toHaveBeenCalledTimes(1);
      removeServiceFromLocalStorageSaga(deleteServiceAction);
      expect(window.localStorage.setItem).toHaveBeenCalledTimes(1);

      expect(
        JSON.parse(window.localStorage.getItem('userAddedServices')),
      ).toEqual({
        'https://testservice.fi/wms?': {
          name: 'test name',
          url: 'https://testservice.fi/wms?',
        },
      });
    });
    it('should delete service from localstorage', () => {
      const initialUserAddedServices: UserAddedServices = {
        'https://testservice.fi/wms?': {
          name: 'test name',
          url: 'https://testservice.fi/wms?',
        },
        'https://testservice2.fi/wms?': {
          name: 'test name 2',
          url: 'https://testservice2.fi/wms?',
        },
      };
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      setUserAddedServices(initialUserAddedServices);

      const deleteServiceAction = mapStoreRemoveService({
        id: 'id_1',
        serviceUrl: 'https://testservice.fi/wms?',
      });
      expect(window.localStorage.setItem).toHaveBeenCalledTimes(1);
      removeServiceFromLocalStorageSaga(deleteServiceAction);
      expect(window.localStorage.setItem).toHaveBeenCalledTimes(2);

      expect(
        JSON.parse(window.localStorage.getItem('userAddedServices')),
      ).toEqual({
        'https://testservice2.fi/wms?': {
          name: 'test name 2',
          url: 'https://testservice2.fi/wms?',
          isUserAddedService: true,
        },
      });
    });
  });
});
