/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { takeLatest, select, put, delay } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';

import { SnackbarActions, snackbarActions } from './reducer';
import * as snackbarSelectors from './selectors';

const hideTime = 4000;

export function* openSnackbarSaga(
  capturedAction: SnackbarActions,
): SagaIterator {
  const snackbarMessage = capturedAction.payload.snackbarContent.message;

  const currentSnackbarMessages = yield select(
    snackbarSelectors.getCurrentSnackbarMessages,
  );
  // If there are currently opened snackbars we need to close these first
  if (currentSnackbarMessages.length) {
    yield put(snackbarActions.closeSnackbar());
  }
  // Generate a unique id
  const id = `snackbar${Date.now().toString()}`;

  yield put(
    snackbarActions.triggerOpenSnackbarBySaga({
      snackbarContent: { message: snackbarMessage, id },
    }),
  );

  // If no new snackbar is triggered cancelling this saga, hide the snackbar after X seconds
  yield delay(hideTime);
  const currentSnackbarMessagesOpen = yield select(
    snackbarSelectors.getCurrentSnackbarMessages,
  );
  if (currentSnackbarMessagesOpen.length) {
    yield put(snackbarActions.closeSnackbar());
  }
}

export function* rootSaga(): SagaIterator {
  yield takeLatest(snackbarActions.openSnackbar.type, openSnackbarSaga);
}

export default rootSaga;
