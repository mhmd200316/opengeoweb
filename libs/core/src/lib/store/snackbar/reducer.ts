/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import {
  createEntityAdapter,
  createSlice,
  PayloadAction,
} from '@reduxjs/toolkit';
import {
  OpenSnackbarPayload,
  SnackbarItem,
  SnackbarState,
  TriggerOpenSnackbarBySagaPayload,
} from './types';

export const snackbarAdapter = createEntityAdapter<SnackbarItem>();

export const initialState: SnackbarState = snackbarAdapter.getInitialState();

const slice = createSlice({
  initialState,
  name: 'snackbar',
  reducers: {
    // To open the snackbar call this action
    // eslint-disable-next-line no-unused-vars
    openSnackbar: (draft, action: PayloadAction<OpenSnackbarPayload>) => {},
    // triggerOpenSnackbarBySaga is triggered by the saga to open the snackbar after the current snackbars have all been closed
    // DO NOT CALL THIS ACTION DIRECTLY!
    triggerOpenSnackbarBySaga: (
      draft,
      action: PayloadAction<TriggerOpenSnackbarBySagaPayload>,
    ) => {
      const { snackbarContent } = action.payload;
      // Ensure we have an id before proceeding
      if (snackbarContent.id === undefined) {
        return;
      }
      snackbarAdapter.setOne(draft, snackbarContent);
    },
    closeSnackbar: (draft) => {
      snackbarAdapter.removeAll(draft);
    },
  },
});

export const { reducer } = slice;
export const snackbarActions = slice.actions;

export type SnackbarActions =
  | ReturnType<typeof snackbarActions.openSnackbar>
  | ReturnType<typeof snackbarActions.triggerOpenSnackbarBySaga>
  | ReturnType<typeof snackbarActions.closeSnackbar>;
