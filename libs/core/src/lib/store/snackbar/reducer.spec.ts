/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import {
  reducer as snackbarReducer,
  initialState,
  snackbarActions,
} from './reducer';
import { SnackbarItem } from './types';

describe('store/snackbar/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(snackbarReducer(undefined, {})).toEqual(initialState);
  });

  it('openSnackbar should not yet open a snackbar as that will be done in the saga', () => {
    const snackbarMessage = 'hi, I am a snackbar';
    const result = snackbarReducer(
      undefined,
      snackbarActions.openSnackbar({
        snackbarContent: {
          message: snackbarMessage,
        },
      }),
    );
    expect(Object.keys(result.ids).length).toBe(0);
    expect(result.entities).toStrictEqual({});
  });

  it('triggerOpenSnackbarBySaga should do nothing if no id passed', () => {
    const snackbarMessage = 'hi, I am a snackbar';
    const store = {
      entities: {
        snackbar1: {
          id: 'snackbar1',
          message: 'snackbar message 1',
        },
      },
      ids: ['snackbar1'],
    };
    const result = snackbarReducer(
      store,
      snackbarActions.triggerOpenSnackbarBySaga({
        snackbarContent: {
          message: snackbarMessage,
        } as unknown as SnackbarItem,
      }),
    );

    expect(result).toBe(store);
  });

  it('triggerOpenSnackbarBySaga should open a snackbar', () => {
    const snackbarid = 'snackbar1';
    const snackbarMessage = 'hi, I am a snackbar';
    const result = snackbarReducer(
      undefined,
      snackbarActions.triggerOpenSnackbarBySaga({
        snackbarContent: {
          message: snackbarMessage,
          id: snackbarid,
        },
      }),
    );
    expect(Object.keys(result.ids).length).toBe(1);
    expect(result.entities[snackbarid].message).toBe(snackbarMessage);
  });

  it('should empty the store if closeSnackbar is called', () => {
    const result = snackbarReducer(
      {
        entities: {
          snackbar1: {
            id: 'snackbar1',
            message: 'snackbar message 1',
          },
        },
        ids: ['snackbar1'],
      },
      snackbarActions.closeSnackbar(),
    );
    expect(Object.keys(result.ids).length).toBe(0);
    expect(result.entities).toStrictEqual({});
  });
});
