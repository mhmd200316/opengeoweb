/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { put, select, takeLatest } from 'redux-saga/effects';
import { rootSaga, openSnackbarSaga } from './sagas';
import { snackbarActions } from './reducer';
import * as snackbarSelectors from './selectors';

describe('store/snackbar/sagas', () => {
  describe('rootSaga', () => {
    it('should catch rootSaga actions and fire corresponding sagas', () => {
      const generator = rootSaga();
      expect(generator.next().value).toEqual(
        takeLatest(snackbarActions.openSnackbar.type, openSnackbarSaga),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
});

describe('openSnackbarSaga', () => {
  it('should fire action to close snackbar if one is currently open before opening the new one', () => {
    const mockDate = new Date('2022-01-09T14:00:00Z').valueOf();
    const snackbarId = `snackbar${mockDate}`;
    jest.spyOn(Date, 'now').mockReturnValue(mockDate);
    const generator = openSnackbarSaga({
      payload: {
        snackbarContent: {
          message: 'snackbar message',
          id: 'snackbar1',
        },
      },
      type: snackbarActions.openSnackbar.type,
    });
    expect(generator.next().value).toEqual(
      select(snackbarSelectors.getCurrentSnackbarMessages),
    );
    expect(generator.next(['snackbar1']).value).toEqual(
      put(snackbarActions.closeSnackbar()),
    );
    expect(generator.next().value).toEqual(
      put(
        snackbarActions.triggerOpenSnackbarBySaga({
          snackbarContent: {
            message: 'snackbar message',
            id: snackbarId,
          },
        }),
      ),
    );
    generator.next();
    expect(generator.next().value).toEqual(
      select(snackbarSelectors.getCurrentSnackbarMessages),
    );
    expect(generator.next(['snackbar1']).value).toEqual(
      put(snackbarActions.closeSnackbar()),
    );
  });
});
