/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as snackbarSelectors from './selectors';

describe('store/snackbar/selectors', () => {
  describe('getSnackbarStore', () => {
    it('should return the snackbar store', () => {
      const mockStore = {
        snackbar: {
          entities: {},
          ids: [],
        },
      };

      expect(snackbarSelectors.getSnackbarStore(mockStore)).toEqual(
        mockStore.snackbar,
      );

      expect(snackbarSelectors.getSnackbarStore(null)).toBeNull();
    });
  });

  describe('getCurrentSnackbarMessages', () => {
    it('should return snackbar messages', () => {
      const mockStore = {
        snackbar: {
          entities: {
            snackbar1: {
              id: 'snackbar',
              message: 'snackbar message 1',
            },
          },
          ids: ['snackbar1'],
        },
      };

      expect(snackbarSelectors.getCurrentSnackbarMessages(mockStore)).toEqual(
        mockStore.snackbar.ids.map((id) => mockStore.snackbar.entities[id]),
      );
    });

    it('should return empty list when no snackbar messages in store', () => {
      const mockStore = {
        snackbar: {
          entities: {},
          ids: [],
        },
      };

      expect(snackbarSelectors.getCurrentSnackbarMessages(mockStore)).toEqual(
        [],
      );
    });

    it('should return empty list when no snackbar store', () => {
      const mockStore = {};

      expect(snackbarSelectors.getCurrentSnackbarMessages(mockStore)).toEqual(
        [],
      );
    });
  });

  describe('getFirstSnackbarMessage', () => {
    it('should return first snackbar message', () => {
      const mockStore = {
        snackbar: {
          entities: {
            snackbar1: {
              id: 'snackbar',
              message: 'snackbar message 1',
            },
            snackbar2: {
              id: 'snackbar2',
              message: 'snackbar message 2',
            },
          },
          ids: ['snackbar1', 'snackbar2'],
        },
      };

      expect(snackbarSelectors.getFirstSnackbarMessage(mockStore)).toEqual(
        mockStore.snackbar.entities.snackbar1,
      );
    });

    it('should return undefined when no snackbar messages in store', () => {
      const mockStore = {
        snackbar: {
          entities: {},
          ids: [],
        },
      };

      expect(snackbarSelectors.getFirstSnackbarMessage(mockStore)).toEqual(
        undefined,
      );
    });
  });
});
