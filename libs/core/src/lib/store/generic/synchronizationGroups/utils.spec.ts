/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { layerActions, mapActions } from '../../mapStore';
import {
  LayerState,
  LayerStatus,
  LayerType,
} from '../../mapStore/layers/types';
import { createMap } from '../../mapStore/map/utils';
import { WebMapState } from '../../mapStore/types';
import { SYNCGROUPS_TYPE_SETLAYERACTIONS } from './constants';
import { SyncGroupActionOrigin } from './types';
import {
  findTargets,
  getAddLayerActionsTargets,
  getLayerActionsTargets,
  getLayerDeleteActionsTargets,
  getLayerMoveActionsTargets,
  getSetActiveLayerIdActionsTargets,
  getTargetLayerIdFromPayload,
} from './utils';
import { createSyncGroupMockState } from './__mocks__/mockState';

const { addLayer, layerChangeOpacity, layerDelete } = layerActions;

const layerState: LayerState = {
  byId: {
    layerId1: {
      id: 'layerId1',
      name: 'LAYER_NAME',
      dimensions: [
        { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
      ],
      layerType: LayerType.mapLayer,
      opacity: 0.5,
      enabled: true,
      service: 'test.service.com',
      status: LayerStatus.error,
    },
    layerId2: {
      id: 'layerId2',
      name: 'LAYER_NAME',
      dimensions: [
        { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
      ],
      layerType: LayerType.mapLayer,
      opacity: 0.2,
      enabled: true,
      service: 'test.service.com',
      status: LayerStatus.error,
    },
  },
  allIds: ['layerId1', 'layerId2', 'test-3'],
  availableBaseLayers: null,
};

const webMapState: WebMapState = {
  byId: {
    mapA: {
      ...createMap({ id: 'mapA' }),
      mapLayers: ['layerId1'],
      baseLayers: null,
      overLayers: null,
      activeLayerId: null,
    },
    mapB: {
      ...createMap({ id: 'mapB' }),
      mapLayers: ['layerId2'],
      baseLayers: null,
      overLayers: null,
      activeLayerId: null,
    },
  },
  allIds: ['mapA', 'mapB'],
};

describe('store/generic/synchronizationGroups/utils', () => {
  describe('getLayerDeleteActionsTargets', () => {
    it('It should also delete the layers in the other maps', () => {
      const mockStore = createSyncGroupMockState();
      const action = layerDelete({
        layerId: 'layerId2',
        layerIndex: 0,
        mapId: 'mapB',
      });
      const layerActionTargets = getLayerDeleteActionsTargets(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        action.payload,
        SYNCGROUPS_TYPE_SETLAYERACTIONS,
      );
      expect(layerActionTargets).toEqual([
        {
          layerId: 'layerId1',
          layerIndex: 0,
          mapId: 'mapA',
          origin: SyncGroupActionOrigin.delete,
        },
      ]);
    });
  });

  describe('getLayerActionsTargets', () => {
    it('it should find the other layers in other maps by changing the opacity for this layer', () => {
      const mockStore = createSyncGroupMockState();

      const action = layerChangeOpacity({
        layerId: 'layerId2',
        opacity: 0.46,
      });

      const layerActionTargets = getLayerActionsTargets(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        action.payload,
        SYNCGROUPS_TYPE_SETLAYERACTIONS,
      );
      expect(layerActionTargets).toEqual([
        {
          layerId: 'layerId1',
          opacity: 0.46,
          origin: SyncGroupActionOrigin.layerActions,
        },
      ]);
    });
  });
  describe('getAddLayerActionsTargets', () => {
    it('it should find the other maps where the layer should also be added', () => {
      const mockStore = createSyncGroupMockState();
      const action = addLayer({
        mapId: 'mapB',
        layerId: 'newLayerId',
        layer: {
          service: 'bla',
          name: 'bla',
        },
        origin: 'utils.spec',
      });
      const layerActionTargets = getAddLayerActionsTargets(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        action.payload,
        SYNCGROUPS_TYPE_SETLAYERACTIONS,
      );
      expect(layerActionTargets).toEqual([
        {
          layer: {
            name: 'bla',
            service: 'bla',
          },
          layerId: 'layerid_1',
          mapId: 'mapA',
          origin: SyncGroupActionOrigin.add,
        },
      ]);
    });

    it('it should not pass the id of a layer object as payload', () => {
      const mockStore = createSyncGroupMockState();
      const action = addLayer({
        mapId: 'mapB',
        layerId: 'newLayerId',
        layer: {
          service: 'bla',
          name: 'bla',
          id: 'test-1',
        },
        origin: 'utils.spec',
      });
      const layerActionTargets = getAddLayerActionsTargets(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        action.payload,
        SYNCGROUPS_TYPE_SETLAYERACTIONS,
      );
      expect(layerActionTargets).toEqual([
        {
          layer: {
            name: 'bla',
            service: 'bla',
          },
          layerId: 'layerid_2',
          mapId: 'mapA',
          origin: SyncGroupActionOrigin.add,
        },
      ]);
    });
  });

  describe('getLayerMoveActionsTargets', () => {
    it('by moving a layer in one map it should find the other map targets where the layer should be moved', () => {
      const mockStore = createSyncGroupMockState();
      const action = mapActions.layerMoveLayer({
        mapId: 'mapB',
        oldIndex: 0,
        newIndex: 1,
        origin: 'utils.spec',
      });

      const moveLayerActionTargets = getLayerMoveActionsTargets(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        action.payload,
        SYNCGROUPS_TYPE_SETLAYERACTIONS,
      );
      expect(moveLayerActionTargets).toEqual([
        {
          mapId: 'mapA',
          newIndex: 1,
          oldIndex: 0,
          origin: SyncGroupActionOrigin.move,
        },
      ]);
    });
  });
  describe('getTargetLayerIdFromPayload', () => {
    it('it should find the targetId for layerDelete', () => {
      const mockStore = createSyncGroupMockState();
      const action = layerDelete({
        layerId: 'layerId2',
        layerIndex: 0,
        mapId: 'mapB',
      });
      const id = getTargetLayerIdFromPayload(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        'mapB',
        'mapA',
        action.payload,
      );

      expect(id).toEqual('layerId1');
    });
    it('it should return null if unknown layerIndex is given via layerDelete', () => {
      const mockStore = createSyncGroupMockState();
      const action = layerDelete({
        layerId: 'layerId2',
        layerIndex: 1000,
        mapId: 'mapB',
      });
      const id = getTargetLayerIdFromPayload(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        'mapB',
        'mapA',
        action.payload,
      );

      expect(id).toBeNull();
    });
    it('it should find the targetId for layer change opacity', () => {
      const mockStore = createSyncGroupMockState();
      const action = layerChangeOpacity({
        layerId: 'layerId2',
        opacity: 0.46,
      });
      const id = getTargetLayerIdFromPayload(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        'mapB',
        'mapA',
        action.payload,
      );

      expect(id).toEqual('layerId1');
    });
    it('it should return null if unknown layerid is given', () => {
      const mockStore = createSyncGroupMockState();
      const action = layerChangeOpacity({
        layerId: 'layerId2-notexist',
        opacity: 0.46,
      });
      const id = getTargetLayerIdFromPayload(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        'mapB',
        'mapA',
        action.payload,
      );

      expect(id).toBeNull();
    });
    it('it should return null if target map does not exist', () => {
      const mockStore = createSyncGroupMockState();
      const action = layerChangeOpacity({
        layerId: 'layerId2',
        opacity: 0.46,
      });
      const id = getTargetLayerIdFromPayload(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        'mapB',
        'mapA-doesnotexist',
        action.payload,
      );

      expect(id).toBeNull();
    });
    it('it should return null if source map does not exist', () => {
      const mockStore = createSyncGroupMockState();
      const action = layerChangeOpacity({
        layerId: 'layerId2',
        opacity: 0.46,
      });

      const id = getTargetLayerIdFromPayload(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        'mapB-doesnotexist',
        'mapA',
        action.payload,
      );

      expect(id).toBeNull();
    });
  });
  describe('findTargets', () => {
    it('it should find the other targets', () => {
      const mockStore = createSyncGroupMockState();
      const action = addLayer({
        mapId: 'mapB',
        layerId: 'newLayerId',
        layer: {
          service: 'bla',
          name: 'bla',
        },
        origin: 'utils.spec',
      });
      const layerActionTargets = findTargets(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        action.payload,
        SYNCGROUPS_TYPE_SETLAYERACTIONS,
        'mapA',
      );
      expect(layerActionTargets).toEqual([
        {
          layerId: null,
          payload: {
            layer: {
              name: 'bla',
              service: 'bla',
            },
            layerId: 'newLayerId',
            mapId: 'mapB',
            origin: 'utils.spec',
          },
          targetId: 'mapB',
        },
      ]);
    });
  });

  describe('getSetActiveLayerIdActionsTargets', () => {
    it('should set the active layer in the other maps', () => {
      const mockStore = createSyncGroupMockState();
      const action = mapActions.setActiveLayerId({
        mapId: 'mapB',
        layerId: 'layerId2',
      });

      const targets = getSetActiveLayerIdActionsTargets(
        {
          syncronizationGroupStore: mockStore,
          layers: layerState,
          webmap: webMapState,
        },
        action.payload,
        SYNCGROUPS_TYPE_SETLAYERACTIONS,
      );
      expect(targets).toEqual([
        {
          mapId: 'mapA',
          layerId: 'layerId1',
          origin: SyncGroupActionOrigin.activateLayerId,
        },
      ]);
    });
  });
});
