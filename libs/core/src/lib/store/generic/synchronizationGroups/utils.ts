/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { AppStore } from '../../../types/types';
import { mapSelectors } from '../../mapStore';
import {
  AddLayerPayload,
  DeleteLayerPayload,
  MoveLayerPayload,
  SetBaseLayersPayload,
} from '../../mapStore/types';
import { generateLayerId } from '../../mapStore/utils/helpers';
import { SyncLayerPayloads, LayerActionPayloadsWithLayerIds } from '../types';
import { syncGroupStore } from './selectors';
import { SyncGroupActionOrigin, SyncType } from './types';

const { getLayerByLayerIndex, getMapIdFromLayerId, getLayerIndexByLayerId } =
  mapSelectors;

interface FoundTargets {
  payload: SyncLayerPayloads;
  targetId: string;
  layerId: string;
}

/**
 * Tries to find the layerId's in the other map based on the payload of the action. It works for all layer actions.
 * @param state
 * @param mapId
 * @param targetMapId
 * @param payload
 * @returns
 */
export const getTargetLayerIdFromPayload = (
  state: AppStore,
  mapId: string,
  targetMapId: string,
  payload: SyncLayerPayloads,
): string => {
  if (!payload) return null;

  /* Try to find the layer for the DeleteLayerPayload, it uses layerIndex to reference the layer */
  if ('layerIndex' in (payload as DeleteLayerPayload)) {
    const targetLayer = getLayerByLayerIndex(
      state,
      targetMapId,
      (payload as DeleteLayerPayload).layerIndex,
    );
    return !targetLayer ? null : targetLayer.id;
  }

  /* Try to find the layer for the LayerActionsWithLayerIds, it uses layerId to reference the layer */
  if ('layerId' in (payload as LayerActionPayloadsWithLayerIds)) {
    const targetLayer = getLayerByLayerIndex(
      state,
      targetMapId,
      getLayerIndexByLayerId(
        state,
        mapId,
        (payload as LayerActionPayloadsWithLayerIds).layerId,
      ),
    );
    return !targetLayer ? null : targetLayer.id;
  }
  /* This payload probably does not reference to a layerId */
  return null;
};

/**
 * Local helper function used by getLayerDeleteActionsTargets, getLayerActionsTargets, getAddLayerActionsTargets, getTargetLayerIdFromPayload to find the targets
 * @param state
 * @param payload
 * @param actionType
 * @param sourceMapId
 * @returns The found targets
 */
export const findTargets = (
  state: AppStore,
  payload: SyncLayerPayloads,
  actionType: SyncType,
  sourceMapId: string,
): FoundTargets[] => {
  const actionPayloads: FoundTargets[] = [];
  const targetsInActionPayload = {};
  const syncronizationGroupStore = syncGroupStore(state);

  if (syncronizationGroupStore && payload) {
    syncronizationGroupStore.groups.allIds.forEach((id) => {
      const syncronizationGroup = syncronizationGroupStore.groups.byId[id];
      if (actionType === syncronizationGroup.type) {
        /* Check if the source is in the target list of the synchonizationGroup */
        const source = syncronizationGroup.targets.byId[sourceMapId];
        /* If the source is part of the target list, and is linked, continue syncing the other targets */
        if (source && source.linked) {
          syncronizationGroup.targets.allIds.forEach((targetId) => {
            const target = syncronizationGroup.targets.byId[targetId];
            if (
              targetId !== sourceMapId &&
              target.linked &&
              !targetsInActionPayload[targetId]
            ) {
              /* Remember that we have already added this target in the action payloads, prevents adding it twice */
              targetsInActionPayload[targetId] = true;
              const otherLayerId = getTargetLayerIdFromPayload(
                state,
                sourceMapId,
                targetId,
                payload as SyncLayerPayloads,
              );

              actionPayloads.push({
                payload,
                targetId,
                layerId: otherLayerId,
              });
            }
          });
        }
      }
    });
  }
  return actionPayloads;
};

/**
 * These targets are found for layer actions that work with a layerId like: SetLayerName, SetLayerEnabled, SetLayerOpacity, SetLayerDimension and SetLayerStyle
 * @param state
 * @param payload
 * @param actionType
 * @returns
 */
export const getLayerActionsTargets = (
  state: AppStore,
  payload: LayerActionPayloadsWithLayerIds,
  actionType: SyncType,
): LayerActionPayloadsWithLayerIds[] => {
  const mapId = getMapIdFromLayerId(state, payload.layerId);
  const foundTargets = findTargets(state, payload, actionType, mapId);

  return foundTargets.map((target): LayerActionPayloadsWithLayerIds => {
    const { payload } = target;
    return {
      ...(payload as LayerActionPayloadsWithLayerIds),
      layerId: target.layerId,
      origin: SyncGroupActionOrigin.layerActions,
      mapId: payload.mapId,
    };
  });
};

/**
 * These targets are found for the layer/map action AddLayer. AddLayer does not have a layerId (because it's new)
 * @param state
 * @param payload
 * @param actionType
 * @returns
 */
export const getAddLayerActionsTargets = (
  state: AppStore,
  payload: AddLayerPayload,
  actionType: SyncType,
): AddLayerPayload[] => {
  const foundTargets = findTargets(state, payload, actionType, payload.mapId);

  return foundTargets.map(({ payload, targetId }): AddLayerPayload => {
    const { layer } = payload as AddLayerPayload;
    const { id, ...layerWithoutId } = layer;

    return {
      ...payload,
      layer: layerWithoutId,
      layerId: generateLayerId(),
      mapId: targetId,
      origin: SyncGroupActionOrigin.add,
    };
  });
};

/**
 * These targets are found for the layer/map action DeleteLayer. The layer is already gone, so there is no layerId anymore. Using layerIndex instead.
 * @param state
 * @param payload
 * @param actionType
 * @returns
 */
export const getLayerDeleteActionsTargets = (
  state: AppStore,
  payload: DeleteLayerPayload,
  actionType: SyncType,
): DeleteLayerPayload[] => {
  const foundTargets = findTargets(state, payload, actionType, payload.mapId);

  return foundTargets.map((target): DeleteLayerPayload => {
    return {
      ...(target.payload as DeleteLayerPayload),
      mapId: target.targetId,
      layerId: target.layerId,
      origin: SyncGroupActionOrigin.delete,
    };
  });
};

/**
 * These targets are found for the layer/map action MoveLayer. Here layers are referenced by newIndex and oldIndex.
 * @param state
 * @param payload
 * @param actionType
 * @returns
 */
export const getLayerMoveActionsTargets = (
  state: AppStore,
  payload: MoveLayerPayload,
  actionType: SyncType,
): MoveLayerPayload[] => {
  const foundTargets = findTargets(state, payload, actionType, payload.mapId);

  return foundTargets.map((target): MoveLayerPayload => {
    return {
      ...(target.payload as MoveLayerPayload),
      mapId: target.targetId,
      origin: SyncGroupActionOrigin.move,
    };
  });
};

/**
 * These targets are found for the layer/map action SetActiveLayerId. It needs both a target mapId and layerId
 * @param state
 * @param payload
 * @param actionType
 * @returns
 */
export const getSetActiveLayerIdActionsTargets = (
  state: AppStore,
  payload: LayerActionPayloadsWithLayerIds,
  actionType: SyncType,
): LayerActionPayloadsWithLayerIds[] => {
  const sourceMapId = getMapIdFromLayerId(state, payload.layerId);
  const foundTargets = findTargets(state, payload, actionType, sourceMapId);

  return foundTargets.map((target): LayerActionPayloadsWithLayerIds => {
    return {
      mapId: target.targetId,
      layerId: target.layerId,
      origin: SyncGroupActionOrigin.activateLayerId,
    };
  });
};

/**
 * These targets are found for baselayer actions that work with a mapId like: setBaseLayers
 * @param state
 * @param payload
 * @param actionType
 * @returns
 */
export const getMapBaseLayerActionsTargets = (
  state: AppStore,
  payload: SetBaseLayersPayload,
  actionType: SyncType,
): SetBaseLayersPayload[] => {
  const foundTargets = findTargets(state, payload, actionType, payload.mapId);

  return foundTargets.map((target): SetBaseLayersPayload => {
    return {
      layers: payload.layers.map((layer) => {
        return { ...layer, id: generateLayerId() };
      }),
      mapId: target.targetId,
      origin: SyncGroupActionOrigin.layerActions,
    };
  });
};
