/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { GenericActionPayload } from '../types';

import {
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETTIME,
  SYNCGROUPS_TYPE_SETLAYERACTIONS,
} from './constants';
import { SyncGroupViewState } from '../../../components/SyncGroups/types';

// store
export interface SynchronizationGroup {
  type: SyncType;
  title?: string;
  payloadByType: SynchronizationSourcePayLoadByType;
  targets: {
    byId: Record<string, { linked: boolean }>;
    allIds: string[];
  };
}

export type SynchronizationSourcePayLoadByType = Record<
  string,
  GenericActionPayload
>;
export interface SynchronizationSource {
  types: SyncType[];
  payloadByType: SynchronizationSourcePayLoadByType;
}

export interface SynchronizationSources {
  byId: Record<string, SynchronizationSource>;
  allIds: string[];
}

export interface SynchronizationGroups {
  byId: Record<string, SynchronizationGroup>;
  allIds: string[];
}

export interface SynchronizationGroupState {
  sources: SynchronizationSources;
  groups: SynchronizationGroups;
  viewState: SyncGroupViewState;
}

export interface SynchronizationGroupModuleState {
  syncronizationGroupStore?: SynchronizationGroupState;
}

export type SyncType =
  | typeof SYNCGROUPS_TYPE_SETBBOX
  | typeof SYNCGROUPS_TYPE_SETTIME
  | typeof SYNCGROUPS_TYPE_SETLAYERACTIONS;

export interface SyncGroupsAddSourcePayload {
  id: string;
  type: SyncType[];
  defaultPayload?: GenericActionPayload;
}

export interface SyncGroupRemoveSourcePayload {
  id: string;
}

export interface SyncGroupAddTargetPayload {
  groupId: string;
  targetId: string;
  linked?: boolean;
}

export interface SyncGroupRemoveTargetPayload {
  groupId: string;
  targetId: string;
}

export interface SyncGroupLinkTargetPayload {
  linked: boolean;
  groupId: string;
  targetId: string;
}

export interface SyncGroupAddGroupPayload {
  groupId: string;
  title: string /* Title for sync group */;
  type: SyncType /* SyncGroup type */;
}

export interface SyncGroupRemoveGroupPayload {
  groupId: string;
}

export interface SyncGroupSetViewStatePayload {
  viewState: SyncGroupViewState;
}

export const SyncGroupTypeList = [
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETTIME,
];

export enum SyncGroupActionOrigin {
  delete = 'ORIGIN_GENERIC_SYNCHRONIZATIONGROUP_UTILS_DELETEACTION',
  layerActions = 'ORIGIN_GENERIC_SYNCHRONIZATIONGROUP_UTILS_LAYERACTIONS',
  move = 'ORIGIN_GENERIC_SYNCHRONIZATIONGROUP_UTILS_MOVEACTION',
  add = 'ORIGIN_GENERIC_SYNCHRONIZATIONGROUP_UTILS_ADDACTION',
  activateLayerId = 'ORIGIN_GENERIC_SYNCHRONIZATIONGROUP_UTILS_ACTIVELAYERIDACTION',
}
