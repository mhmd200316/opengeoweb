/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { SagaIterator } from 'redux-saga';
import { takeLatest, select, put, call } from 'redux-saga/effects';
import { setBboxSync, setTimeSync } from '../synchronizationActions/actions';
import { SetBboxPayload, SetTimePayload } from '../types';

import { SYNCGROUPS_TYPE_SETBBOX, SYNCGROUPS_TYPE_SETTIME } from './constants';
import * as synchronizationGroupsSelector from './selectors';
import { createSyncGroupViewStateSelector } from '../../../components/SyncGroups/selector';
import * as synchronizationGroupActions from './reducer';

export function* updateSourceValueWhenLinkingComponentToGroupSaga(
  groupId: string,
  targetToUpdate: string,
): SagaIterator {
  const group = yield select(
    synchronizationGroupsSelector.getSynchronizationGroup,
    groupId,
  );

  if (!group) {
    return;
  }

  switch (group.type) {
    case SYNCGROUPS_TYPE_SETTIME:
      if (!group.payloadByType[SYNCGROUPS_TYPE_SETTIME]) return;
      yield put(
        setTimeSync(
          group.payloadByType[SYNCGROUPS_TYPE_SETTIME] as SetTimePayload,
          [
            {
              targetId: targetToUpdate,
              value: group.payloadByType[SYNCGROUPS_TYPE_SETTIME].value,
            },
          ],
          [groupId],
        ),
      );
      break;
    case SYNCGROUPS_TYPE_SETBBOX:
      if (!group.payloadByType[SYNCGROUPS_TYPE_SETBBOX]) return;
      yield put(
        setBboxSync(
          group.payloadByType[SYNCGROUPS_TYPE_SETBBOX] as SetBboxPayload,
          [
            {
              targetId: targetToUpdate,
              bbox: group.payloadByType[SYNCGROUPS_TYPE_SETBBOX].bbox,
              srs: group.payloadByType[SYNCGROUPS_TYPE_SETBBOX].srs,
            },
          ],
          [groupId],
        ),
      );
      break;
    default:
  }
}

export function* addGroupTargetSaga({
  payload,
}: ReturnType<
  typeof synchronizationGroupActions.syncGroupAddTarget
>): SagaIterator {
  const { groupId, linked, targetId: targetToUpdate } = payload;

  if (!linked) {
    yield call(
      updateSourceValueWhenLinkingComponentToGroupSaga,
      groupId,
      targetToUpdate,
    );
  }
}

export function* linkGroupTargetSaga({
  payload,
}: ReturnType<
  typeof synchronizationGroupActions.syncGroupLinkTarget
>): SagaIterator {
  const { groupId, linked, targetId: targetToUpdate } = payload;

  if (!linked) {
    yield call(
      updateSourceValueWhenLinkingComponentToGroupSaga,
      groupId,
      targetToUpdate,
    );
  }
}

export function* updateViewStateSaga(): SagaIterator {
  const viewState = yield select(createSyncGroupViewStateSelector);
  yield put(synchronizationGroupActions.syncGroupSetViewState({ viewState }));
}

function* rootSaga(): SagaIterator {
  yield takeLatest(
    synchronizationGroupActions.syncGroupAddTarget.type,
    addGroupTargetSaga,
  );
  yield takeLatest(
    synchronizationGroupActions.syncGroupLinkTarget.type,
    linkGroupTargetSaga,
  );
  yield takeLatest(
    synchronizationGroupActions.syncGroupAddGroup.type,
    updateViewStateSaga,
  );
  yield takeLatest(
    synchronizationGroupActions.syncGroupRemoveGroup.type,
    updateViewStateSaga,
  );
  yield takeLatest(
    synchronizationGroupActions.syncGroupAddSource.type,
    updateViewStateSaga,
  );
  yield takeLatest(
    synchronizationGroupActions.syncGroupRemoveSource.type,
    updateViewStateSaga,
  );
  yield takeLatest(
    synchronizationGroupActions.syncGroupAddTarget.type,
    updateViewStateSaga,
  );
  yield takeLatest(
    synchronizationGroupActions.syncGroupRemoveTarget.type,
    updateViewStateSaga,
  );
  yield takeLatest(
    synchronizationGroupActions.syncGroupLinkTarget.type,
    updateViewStateSaga,
  );
}

export default rootSaga;
