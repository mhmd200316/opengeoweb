/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { GenericActionPayload } from '../types';
import { SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX } from './constants';
import { getTargetGroups } from './selectors';
import { AppStore } from '../../../types/types';

const syncronizationGroupTestStore: AppStore = {
  syncronizationGroupStore: {
    sources: {
      byId: {
        mapA: {
          types: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              value: '2020-11-13T01:32:00Z',
            },
            SYNCGROUPS_TYPE_SETBBOX: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              srs: 'EPSG:4326',
              bbox: {
                left: -10,
                right: 10,
                bottom: -10,
                top: 10,
              },
            },
          },
        },
        mapB: {
          types: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              value: '2020-11-13T01:32:00Z',
            },
          },
        },
        mapC: {
          types: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              value: '2020-11-13T01:32:00Z',
            },
          },
        },
      },
      allIds: ['mapA', 'mapB'],
    },
    groups: {
      byId: {
        group1: {
          title: 'Group 1 for time',
          type: SYNCGROUPS_TYPE_SETTIME,
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              value: '2020-11-13T01:32:00Z',
            },
            SYNCGROUPS_TYPE_SETBBOX: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              srs: 'EPSG:4326',
              bbox: {
                left: -10,
                right: 10,
                bottom: -10,
                top: 10,
              },
            },
          },
          targets: {
            allIds: ['mapB', 'mapA', 'mapC'],
            byId: {
              mapB: {
                linked: true,
              },
              mapA: {
                linked: true,
              },
              mapC: {
                linked: false,
              },
            },
          },
        },
        group2: {
          title: 'Group 2 for area',
          type: SYNCGROUPS_TYPE_SETBBOX,
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              value: '2020-11-13T01:32:00Z',
            },
            SYNCGROUPS_TYPE_SETBBOX: {
              sourceId: 'mapA',
              origin: 'sagas.spec.ts',
              srs: 'EPSG:4326',
              bbox: {
                left: -10,
                right: 10,
                bottom: -10,
                top: 10,
              },
            },
          },
          targets: {
            allIds: ['mapB', 'mapA', 'mapC'],
            byId: {
              mapB: {
                linked: true,
              },
              mapA: {
                linked: true,
              },
              mapC: {
                linked: false,
              },
            },
          },
        },
      },
      allIds: ['group1', 'group2'],
    },
    viewState: {
      timeslider: {
        groups: [],
        sourcesById: [],
      },
      zoompane: {
        groups: [],
        sourcesById: [],
      },
      level: {
        groups: [],
        sourcesById: [],
      },
    },
  },
};

describe('src/store/synchronizationGroups/selectors', () => {
  describe('getTargetGroups', () => {
    it('should return the group where source is linked to', () => {
      const payload: GenericActionPayload = {
        sourceId: 'mapA',
        srs: 'EPSG:3857',
        bbox: {
          left: -450651.2255879827,
          bottom: 6490531.093143953,
          right: 1428345.8183648037,
          top: 7438773.776232235,
        },
      };
      expect(
        getTargetGroups(
          syncronizationGroupTestStore,
          payload,
          SYNCGROUPS_TYPE_SETBBOX,
        ),
      ).toEqual(['group2']);
    });
    it('should return an empty array, when source is not linked to any group', () => {
      const payload: GenericActionPayload = {
        sourceId: 'mapC',
        srs: 'EPSG:3857',
        bbox: {
          left: -450651.2255879827,
          bottom: 6490531.093143953,
          right: 1428345.8183648037,
          top: 7438773.776232235,
        },
      };
      expect(
        getTargetGroups(
          syncronizationGroupTestStore,
          payload,
          SYNCGROUPS_TYPE_SETBBOX,
        ),
      ).toEqual([]);
    });
  });
});
