/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { produce, current } from 'immer';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import {
  SynchronizationGroupState,
  SyncGroupAddTargetPayload,
  SyncGroupAddGroupPayload,
  SyncGroupRemoveTargetPayload,
  SyncGroupRemoveGroupPayload,
  SyncGroupSetViewStatePayload,
  SyncGroupLinkTargetPayload,
  SyncGroupRemoveSourcePayload,
  SyncGroupsAddSourcePayload,
} from './types';
import { SYNCGROUPS_TYPE_SETBBOX, SYNCGROUPS_TYPE_SETTIME } from './constants';

import type {
  SetBboxSyncActionPayload,
  SetTimeSyncActionPayload,
} from '../synchronizationActions/types';

import { removeInPlace } from '../utils';
import { setBboxSync, setTimeSync } from '../synchronizationActions/actions';

export const initialState: SynchronizationGroupState = {
  sources: {
    byId: {},
    allIds: [],
  },
  groups: {
    byId: {},
    allIds: [],
  },
  viewState: {
    timeslider: {
      groups: [],
      sourcesById: [],
    },
    zoompane: {
      groups: [],
      sourcesById: [],
    },
    level: {
      groups: [],
      sourcesById: [],
    },
  },
};

export const slice = createSlice({
  initialState,
  name: 'synchronizationGroupsReducer',
  reducers: {
    syncGroupAddSource: (
      draft,
      action: PayloadAction<SyncGroupsAddSourcePayload>,
    ) => {
      const { id, type: payloadType, defaultPayload } = action.payload;

      if (!draft.sources.byId[id]) {
        draft.sources.byId[id] = {
          types: payloadType,
          payloadByType: {
            SYNCGROUPS_TYPE_SETTIME: defaultPayload,
            SYNCGROUPS_TYPE_SETBBOX: defaultPayload,
            SYNCGROUPS_TYPE_SETLAYERACTIONS: defaultPayload,
          },
        };

        draft.sources.allIds.push(id);
      }

      /* Now try to initialize with the correct values from the state */
      draft.groups.allIds.forEach((groupId) => {
        const syncronizationGroup = draft.groups.byId[groupId];
        /* Check if the source is in the target list of the synchonizationGroup */
        const source = syncronizationGroup.targets.byId[id];

        const { type } = syncronizationGroup;
        /* If the source is part of the target list, and is linked, continue syncing the other targets */
        if (source && source.linked) {
          draft.sources.byId[id].payloadByType[type] =
            (syncronizationGroup.payloadByType &&
              syncronizationGroup.payloadByType[type]) ||
            defaultPayload;
        }
      });
    },

    syncGroupRemoveSource: (
      draft,
      action: PayloadAction<SyncGroupRemoveSourcePayload>,
    ) => {
      const { id } = action.payload;
      if (draft.sources.byId[id]) {
        delete draft.sources.byId[id];
        removeInPlace(draft.sources.allIds, (_id) => _id === id);
      }
    },
    syncGroupAddTarget: (
      draft,
      action: PayloadAction<SyncGroupAddTargetPayload>,
    ) => {
      const state = current(draft);
      const { groupId, targetId, linked } = action.payload;
      if (!draft.groups.byId[groupId]) {
        console.warn(`SYNCGROUPS_ADD_TARGET: Group ${groupId} does not exist.`);
        return;
      }
      if (!targetId) {
        console.warn(`SYNCGROUPS_ADD_TARGET: targetId is not defined.`);
        return;
      }
      if (draft.groups.byId[groupId].targets.allIds.includes(targetId)) {
        return;
      }
      draft.groups.byId[groupId].targets.allIds.push(targetId);
      draft.groups.byId[groupId].targets.byId[targetId] = {
        linked: linked !== false,
      };

      /* Now try to initialize with the correct values from the state */
      const syncronizationGroup = state.groups.byId[groupId];
      /* Check if the source is in the target list of the synchonizationGroup */
      const source = state.groups.byId[groupId].targets.byId[targetId];
      /* If the source is part of the target list, and is linked, continue syncing the other targets */
      if (source && source.linked) {
        const { type } = syncronizationGroup;
        draft.sources.byId[targetId].payloadByType[type] =
          syncronizationGroup.payloadByType[type];
      }
    },
    syncGroupRemoveTarget: (
      draft,
      action: PayloadAction<SyncGroupRemoveTargetPayload>,
    ) => {
      const { groupId, targetId } = action.payload;
      if (!draft.groups.byId[groupId]) return;
      if (!draft.groups.byId[groupId].targets.allIds.includes(targetId)) return;
      delete draft.groups.byId[groupId].targets.byId[targetId];
      removeInPlace(
        draft.groups.byId[groupId].targets.allIds,
        (_id) => _id === targetId,
      );
    },
    syncGroupLinkTarget: (
      draft,
      action: PayloadAction<SyncGroupLinkTargetPayload>,
    ) => {
      const { groupId, targetId, linked } = action.payload;
      if (
        !draft.groups.byId[groupId] ||
        !draft.groups.byId[groupId].targets.byId[targetId]
      ) {
        return;
      }
      draft.groups.byId[groupId].targets.byId[targetId].linked = linked;
    },
    syncGroupAddGroup: (
      draft,
      action: PayloadAction<SyncGroupAddGroupPayload>,
    ) => {
      const { groupId, type, title } = action.payload;
      if (draft.groups.byId[groupId]) {
        return;
      }
      draft.groups.byId[groupId] = {
        title,
        type,
        payloadByType: {
          SYNCGROUPS_TYPE_SETTIME: null,
          SYNCGROUPS_TYPE_SETBBOX: null,
        },
        targets: {
          allIds: [],
          byId: {},
        },
      };
      draft.groups.allIds.push(groupId);
    },
    syncGroupRemoveGroup: (
      draft,
      action: PayloadAction<SyncGroupRemoveGroupPayload>,
    ) => {
      const { groupId } = action.payload;
      if (!draft.groups.byId[groupId]) {
        return;
      }
      delete draft.groups.byId[groupId];
      removeInPlace(draft.groups.allIds, (_groupId) => _groupId === groupId);
    },
    syncGroupClear: () => {
      return initialState;
    },
    syncGroupSetViewState: (
      draft,
      action: PayloadAction<SyncGroupSetViewStatePayload>,
    ) => {
      const { viewState } = action.payload;
      draft.viewState = viewState;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(setTimeSync, (draft, action) => {
        return setBboxOrTimeSync(draft, action);
      })
      .addCase(setBboxSync, (draft, action) => {
        return setBboxOrTimeSync(draft, action);
      });
  },
});

const setBboxOrTimeSync = (
  state: SynchronizationGroupState,
  action: PayloadAction<SetBboxSyncActionPayload | SetTimeSyncActionPayload>,
): SynchronizationGroupState => {
  return produce(state, (draft) => {
    const { targets, source, groups } = action.payload;

    const getPayLoadKey = (type): string | null => {
      switch (type) {
        case setTimeSync.type:
          return SYNCGROUPS_TYPE_SETTIME;
        case setBboxSync.type:
          return SYNCGROUPS_TYPE_SETBBOX;
        default:
          return null;
      }
    };
    const payloadKey = getPayLoadKey(action.type);
    if (!source.payload || !payloadKey) {
      console.warn('Wrong sync action: ', action);
      return;
    }

    if (draft.sources.byId[source.payload.sourceId]) {
      if (!draft.sources.byId[source.payload.sourceId].payloadByType) {
        draft.sources.byId[source.payload.sourceId].payloadByType = {};
      }
      draft.sources.byId[source.payload.sourceId].payloadByType[payloadKey] =
        source.payload;
    }
    /* Update all targets */
    targets.forEach((target) => {
      if (draft.sources.byId[target.targetId]) {
        if (!draft.sources.byId[target.targetId].payloadByType) {
          draft.sources.byId[target.targetId].payloadByType = {};
        }
        draft.sources.byId[target.targetId].payloadByType[payloadKey] =
          source.payload;
      }
    });
    /* Set the value in the group */
    groups.forEach((group: string) => {
      if (draft.groups.byId[group]) {
        draft.groups.byId[group].payloadByType[payloadKey] = source.payload;
      }
    });
  });
};

export const {
  syncGroupAddGroup,
  syncGroupAddSource,
  syncGroupAddTarget,
  syncGroupClear,
  syncGroupLinkTarget,
  syncGroupRemoveGroup,
  syncGroupRemoveSource,
  syncGroupRemoveTarget,
  syncGroupSetViewState,
} = slice.actions;

export const { actions, reducer } = slice;
