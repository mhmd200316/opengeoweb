/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  SYNCGROUPS_TYPE_SETTIME,
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETLAYERACTIONS,
} from '../constants';
import { SynchronizationGroupState } from '../types';

export const createSyncGroupMockState = (): SynchronizationGroupState => ({
  sources: {
    byId: {
      mapA: {
        payloadByType: {
          SYNCGROUPS_TYPE_SETTIME: {
            sourceId: 'slider1',
            origin: 'mockState.ts',
            value: '2021-02-22T19:57:00Z',
          },
          SYNCGROUPS_TYPE_SETBBOX: {
            sourceId: 'mapA',
            origin: 'sagas.spec.ts',
            srs: 'EPSG:4326',
            bbox: {
              left: -10,
              right: 10,
              bottom: -10,
              top: 10,
            },
          },
        },
        types: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
      },
      mapB: {
        payloadByType: null,
        types: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
      },
      mapC: {
        payloadByType: null,
        types: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
      },
      mapD: {
        payloadByType: null,
        types: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
      },
    },
    allIds: ['mapA', 'mapB', 'mapC', 'mapD'],
  },
  groups: {
    byId: {
      TimeA: {
        title: 'Group 1 for time',
        type: SYNCGROUPS_TYPE_SETTIME,
        payloadByType: {
          SYNCGROUPS_TYPE_SETTIME: {
            sourceId: 'slider1',
            origin: 'mockState.ts',
            value: '2021-02-22T19:57:00Z',
          },
          SYNCGROUPS_TYPE_SETBBOX: {
            sourceId: 'mapA',
            origin: 'sagas.spec.ts',
            srs: 'EPSG:4326',
            bbox: {
              left: -10,
              right: 10,
              bottom: -10,
              top: 10,
            },
          },
        },
        targets: {
          allIds: ['mapA', 'mapB', 'mapC'],
          byId: {
            mapA: {
              linked: true,
            },
            mapB: {
              linked: true,
            },
            mapC: {
              linked: true,
            },
          },
        },
      },
      AreaA: {
        title: 'Group 2 for area',
        type: SYNCGROUPS_TYPE_SETBBOX,
        payloadByType: {
          SYNCGROUPS_TYPE_SETTIME: {
            sourceId: 'slider1',
            origin: 'mockState.ts',
            value: '2021-02-22T19:57:00Z',
          },
          SYNCGROUPS_TYPE_SETBBOX: {
            sourceId: 'mapA',
            origin: 'sagas.spec.ts',
            srs: 'EPSG:4326',
            bbox: {
              left: -10,
              right: 10,
              bottom: -10,
              top: 10,
            },
          },
        },
        targets: {
          allIds: ['mapA', 'mapB', 'mapC'],
          byId: {
            mapA: {
              linked: true,
            },
            mapB: {
              linked: true,
            },
            mapC: {
              linked: true,
            },
          },
        },
      },
      group1: {
        title: 'Group 1 for time',
        type: SYNCGROUPS_TYPE_SETTIME,
        payloadByType: {
          SYNCGROUPS_TYPE_SETTIME: {
            sourceId: 'slider1',
            origin: 'mockState.ts',
            value: '2021-02-22T19:57:00Z',
          },
          SYNCGROUPS_TYPE_SETBBOX: {
            sourceId: 'mapA',
            origin: 'sagas.spec.ts',
            srs: 'EPSG:4326',
            bbox: {
              left: -10,
              right: 10,
              bottom: -10,
              top: 10,
            },
          },
        },
        targets: {
          allIds: [],
          byId: {},
        },
      },
      group2: {
        title: 'Group 2 for area',
        type: SYNCGROUPS_TYPE_SETBBOX,
        payloadByType: {
          SYNCGROUPS_TYPE_SETTIME: {
            sourceId: 'slider1',
            origin: 'mockState.ts',
            value: '2021-02-22T19:57:00Z',
          },
          SYNCGROUPS_TYPE_SETBBOX: {
            sourceId: 'mapA',
            origin: 'sagas.spec.ts',
            srs: 'EPSG:4326',
            bbox: {
              left: -10,
              right: 10,
              bottom: -10,
              top: 10,
            },
          },
        },
        targets: {
          allIds: [],
          byId: {},
        },
      },
      group3: {
        title: 'Group 3 for time',
        type: SYNCGROUPS_TYPE_SETTIME,
        payloadByType: {
          SYNCGROUPS_TYPE_SETTIME: {
            sourceId: 'slider1',
            origin: 'mockState.ts',
            value: '2021-02-22T19:57:00Z',
          },
          SYNCGROUPS_TYPE_SETBBOX: {
            sourceId: 'mapA',
            origin: 'sagas.spec.ts',
            srs: 'EPSG:4326',
            bbox: {
              left: -10,
              right: 10,
              bottom: -10,
              top: 10,
            },
          },
        },
        targets: {
          allIds: [],
          byId: {},
        },
      },
      group4: {
        title: 'Group 4 for area',
        type: SYNCGROUPS_TYPE_SETBBOX,
        payloadByType: {
          SYNCGROUPS_TYPE_SETTIME: {
            sourceId: 'slider1',
            origin: 'mockState.ts',
            value: '2021-02-22T19:57:00Z',
          },
          SYNCGROUPS_TYPE_SETBBOX: {
            sourceId: 'mapA',
            origin: 'sagas.spec.ts',
            srs: 'EPSG:4326',
            bbox: {
              left: -10,
              right: 10,
              bottom: -10,
              top: 10,
            },
          },
        },
        targets: {
          allIds: [],
          byId: {},
        },
      },
      layerGroupA: {
        title: 'Layer group for multimapdemo',
        type: SYNCGROUPS_TYPE_SETLAYERACTIONS,
        payloadByType: {
          SYNCGROUPS_TYPE_SETTIME: null,
          SYNCGROUPS_TYPE_SETBBOX: null,
        },
        targets: {
          allIds: ['mapA', 'mapB'],
          byId: {
            mapA: {
              linked: true,
            },
            mapB: {
              linked: true,
            },
          },
        },
      },
    },
    allIds: [
      'TimeA',
      'AreaA',
      'group1',
      'group2',
      'group3',
      'group4',
      'layerGroupA',
    ],
  },
  viewState: {
    timeslider: {
      groups: [],
      sourcesById: [],
    },
    zoompane: {
      groups: [],
      sourcesById: [],
    },
    level: {
      groups: [],
      sourcesById: [],
    },
  },
});
