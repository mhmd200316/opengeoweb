/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { put, select, takeLatest } from 'redux-saga/effects';
import {
  setTimeSaga,
  setBBoxSaga,
  rootSaga,
  layerActionsSaga,
  deleteLayerActionsSaga,
  moveLayerActionsSaga,
  addLayerActionsSaga,
  setActiveLayerIdActionsSaga,
  mapBaseLayerActionsSaga,
} from './sagas';
import { genericActions } from '.';
import * as synchronizationGroupsSelector from './synchronizationGroups/selectors';
import { mapActions } from '../mapStore/map/reducer';

import {
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETLAYERACTIONS,
  SYNCGROUPS_TYPE_SETTIME,
} from './synchronizationGroups/constants';
import {
  setLayerActionSync,
  setBboxSync,
  setTimeSync,
} from './synchronizationActions/actions';

import {
  AddLayerPayload,
  DeleteLayerPayload,
  SetBaseLayersPayload,
  SetLayerOpacityPayload,
} from '../mapStore/types';
import {
  getAddLayerActionsTargets,
  getLayerActionsTargets,
  getLayerDeleteActionsTargets,
  getLayerMoveActionsTargets,
  getMapBaseLayerActionsTargets,
  getSetActiveLayerIdActionsTargets,
} from './synchronizationGroups/utils';
import { layerActions } from '../mapStore';

const {
  addLayer,
  layerChangeDimension,
  layerChangeEnabled,
  layerChangeName,
  layerChangeOpacity,
  layerDelete,
  setBaseLayers,
} = layerActions;

const { layerMoveLayer, setActiveLayerId } = mapActions;

describe('store/generic/sagas', () => {
  describe('rootSaga', () => {
    it('should take actions and fire correct sagas', () => {
      const generator = rootSaga();

      expect(generator.next().value).toEqual(
        takeLatest(genericActions.setTime.type, setTimeSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(genericActions.setBbox.type, setBBoxSaga),
      );

      expect(generator.next().value).toEqual(
        takeLatest(layerChangeName.type, layerActionsSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(layerChangeEnabled.type, layerActionsSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(layerChangeOpacity.type, layerActionsSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(layerChangeDimension.type, layerActionsSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(layerActions.layerChangeStyle.type, layerActionsSaga),
      );

      expect(generator.next().value).toEqual(
        takeLatest(layerDelete.type, deleteLayerActionsSaga),
      );

      expect(generator.next().value).toEqual(
        takeLatest(addLayer.type, addLayerActionsSaga),
      );

      expect(generator.next().value).toEqual(
        takeLatest(mapActions.layerMoveLayer.type, moveLayerActionsSaga),
      );

      expect(generator.next().value).toEqual(
        takeLatest(
          mapActions.setActiveLayerId.type,
          setActiveLayerIdActionsSaga,
        ),
      );

      expect(generator.next().value).toEqual(
        takeLatest(setBaseLayers.type, mapBaseLayerActionsSaga),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('setTimeSaga', () => {
    it('should handle setTime', () => {
      const payload = {
        sourceId: 'test-1',
        origin: 'saga.spec',
        value: '2020-11-13T01:32:00Z',
      };
      const action = genericActions.setTime(payload);
      const generator = setTimeSaga(action);

      const targets = generator.next().value;
      expect(targets).toEqual(
        select(
          synchronizationGroupsSelector.getTargets,
          payload,
          SYNCGROUPS_TYPE_SETTIME,
        ),
      );

      const groups = generator.next().value;
      expect(groups).toEqual(
        select(
          synchronizationGroupsSelector.getTargetGroups,
          payload,
          SYNCGROUPS_TYPE_SETTIME,
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          setTimeSync(
            payload,
            generator.next(targets).value,
            generator.next(groups).value,
          ),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should ignore handle setTime with invalid values', () => {
      const payload = {
        sourceId: 'test-1',
        origin: 'saga.spec',
        value: '2020-11-13T01:32:00.000Z',
      };
      jest.spyOn(console, 'error').mockImplementation();
      const action = genericActions.setTime(payload);
      const generator = setTimeSaga(action);

      const targets = generator.next().value;
      expect(targets).toBeUndefined();
      expect(console.error).toHaveBeenCalledWith(
        expect.stringContaining(
          'setTime value 2020-11-13T01:32:00.000Z does not conform to format [YYYY-MM-DDThh:mm:ssZ]. It was triggered by saga.spec',
        ),
      );
    });

    it('should handle setBBoxSaga', () => {
      const payload = {
        sourceId: 'test-1',
        bbox: {
          left: -450651.2255879827,
          bottom: 6490531.093143953,
          right: 1428345.8183648037,
          top: 7438773.776232235,
        },
        srs: 'EPSG:3857',
      };
      const action = genericActions.setBbox(payload);
      const generator = setBBoxSaga(action);

      const targets = generator.next().value;
      expect(targets).toEqual(
        select(
          synchronizationGroupsSelector.getTargets,
          payload,
          SYNCGROUPS_TYPE_SETBBOX,
        ),
      );

      const groups = generator.next().value;
      expect(groups).toEqual(
        select(
          synchronizationGroupsSelector.getTargetGroups,
          payload,
          SYNCGROUPS_TYPE_SETBBOX,
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          setBboxSync(
            payload,
            generator.next(targets).value,
            generator.next(groups).value,
          ),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
  describe('layerActionsSaga', () => {
    it('should handle layerActionsSaga', () => {
      const payload: SetLayerOpacityPayload = {
        layerId: 'layerId2',
        opacity: 0.46,
      };
      const action = layerChangeOpacity(payload);

      const generator = layerActionsSaga(action);

      expect(generator.next().value).toEqual(
        select(
          getLayerActionsTargets,
          payload,
          SYNCGROUPS_TYPE_SETLAYERACTIONS,
        ),
      );
      const targets = [{ layerId: 'layerId1', opacity: 0.46 }];
      expect(generator.next(targets).value).toEqual(
        put(setLayerActionSync(payload, targets, layerChangeOpacity.type)),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('addLayerActionsSaga', () => {
    it('should handle addLayerActionsSaga', () => {
      const payload: AddLayerPayload = {
        mapId: 'mapB',
        layerId: 'newLayerId',
        layer: {
          service: 'bla',
          name: 'bla',
        },
        origin: 'utils.spec',
      };
      const action = addLayer(payload);

      const generator = addLayerActionsSaga(action);

      expect(generator.next().value).toEqual(
        select(
          getAddLayerActionsTargets,
          payload,
          SYNCGROUPS_TYPE_SETLAYERACTIONS,
        ),
      );
      const targets = [
        {
          mapId: 'mapA',
          layerId: 'newLayerId',
          layer: {
            service: 'bla',
            name: 'bla',
          },
        },
      ];
      expect(generator.next(targets).value).toEqual(
        put(setLayerActionSync(payload, targets, addLayer.type)),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('deleteLayerActionsSaga', () => {
    it('should handle deleteLayerActionsSaga', () => {
      const payload: DeleteLayerPayload = {
        layerId: 'layerId2',
        layerIndex: 0,
        mapId: 'mapB',
      };
      const action = layerDelete(payload);

      const generator = deleteLayerActionsSaga(action);

      expect(generator.next().value).toEqual(
        select(
          getLayerDeleteActionsTargets,
          payload,
          SYNCGROUPS_TYPE_SETLAYERACTIONS,
        ),
      );

      const targets = [{ mapId: 'mapA', layerId: 'layerId1', layerIndex: 0 }];
      expect(generator.next(targets).value).toEqual(
        put(setLayerActionSync(payload, targets, layerDelete.type)),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
  describe('moveLayerActionsSaga', () => {
    it('should handle moveLayerActionsSaga', () => {
      const payload = {
        mapId: 'mapB',
        oldIndex: 0,
        newIndex: 1,
        origin: 'utils.spec',
      };
      const action = layerMoveLayer(payload);

      const generator = moveLayerActionsSaga(action);

      expect(generator.next().value).toEqual(
        select(
          getLayerMoveActionsTargets,
          payload,
          SYNCGROUPS_TYPE_SETLAYERACTIONS,
        ),
      );
      const targets = [
        { mapId: 'mapA', oldIndex: 0, newIndex: 1, origin: 'utils.spec' },
      ];
      expect(generator.next(targets).value).toEqual(
        put(
          setLayerActionSync(payload, targets, mapActions.layerMoveLayer.type),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('setActiveLayerIdActionsSaga', () => {
    it('should handle setActiveLayerIdActionsSaga', () => {
      const payload = {
        mapId: 'mapB',
        layerId: 'layerId2',
      };
      const action = setActiveLayerId(payload);

      const generator = setActiveLayerIdActionsSaga(action);

      expect(generator.next().value).toEqual(
        select(
          getSetActiveLayerIdActionsTargets,
          payload,
          SYNCGROUPS_TYPE_SETLAYERACTIONS,
        ),
      );
      const targets = [{ mapId: 'mapA', layerId: 'layerId1' }];
      expect(generator.next(targets).value).toEqual(
        put(
          setLayerActionSync(
            payload,
            targets,
            mapActions.setActiveLayerId.type,
          ),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('mapBaseLayerActionsSaga', () => {
    it('should handle mapBaseLayerActionsSaga', () => {
      const payload: SetBaseLayersPayload = {
        mapId: 'mapB',
        layers: [
          {
            service:
              'https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/',
            name: 'arcGisCanvas',
            type: 'twms',
            mapId: 'mapid_1_0',
            dimensions: [],
            id: 'layerid_86',
            opacity: 1,
            enabled: true,
          },
        ],
      };
      const action = setBaseLayers(payload);

      const generator = mapBaseLayerActionsSaga(action);

      expect(generator.next().value).toEqual(
        select(
          getMapBaseLayerActionsTargets,
          payload,
          SYNCGROUPS_TYPE_SETLAYERACTIONS,
        ),
      );
      const targets = [{ mapId: 'mapA', layerId: 'layerId1' }];
      expect(generator.next(targets).value).toEqual(
        put(setLayerActionSync(payload, targets, setBaseLayers.type)),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
});
