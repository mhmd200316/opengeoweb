/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from '@reduxjs/toolkit';
import { AppStore } from '../../types/types';
import { SYNCGROUPS_TYPE_SETTIME } from './synchronizationGroups/constants';
import {
  SynchronizationGroupState,
  SynchronizationSource,
} from './synchronizationGroups/types';
import { SetTimePayload } from './types';

const synchronizationGroupStore = (
  store: AppStore,
): SynchronizationGroupState =>
  store && store.syncronizationGroupStore
    ? store.syncronizationGroupStore
    : null;

export const getSynchronizationGroupStore = createSelector(
  synchronizationGroupStore,
  (store) => store,
);

/**
 * Returns the synchronization source by id
 * @param store The app store
 * @param id The source id to find
 */
const getSyncSourceBySourceId = (
  state: SynchronizationGroupState,
  id: string,
): SynchronizationSource => {
  if (state && state.sources) {
    const { sources } = state;
    if (sources.byId[id]) {
      return sources.byId[id];
    }
  }
  return null;
};

export const getTime = createSelector(getSyncSourceBySourceId, (store) =>
  store && store.payloadByType && store.payloadByType[SYNCGROUPS_TYPE_SETTIME]
    ? (store.payloadByType[SYNCGROUPS_TYPE_SETTIME] as SetTimePayload).value
    : null,
);
