/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { setBbox, setTime } from '../actions';
import { SyncLayerPayloads } from '../types';

/* Generic SYNC action payloads */
export interface SetTimeSyncPayload {
  targetId: string;
  value: string; // ISO 8601 string "like 2010-10-01T00:00:00Z"
}

export interface SetBboxSyncPayload {
  targetId: string;
  bbox: {
    left: number;
    bottom: number;
    right: number;
    top: number;
  };
  srs: string;
}

export interface SetLayerActionsSyncPayload {
  source: {
    type: string;
    payload: SyncLayerPayloads;
  };
  targets: SyncLayerPayloads[];
}

export interface SetTimeSyncActionPayload {
  source: ReturnType<typeof setTime>;
  groups: string[];
  targets: SetTimeSyncPayload[];
}

export interface SetBboxSyncActionPayload {
  source: ReturnType<typeof setBbox>;
  groups: string[];
  targets: SetBboxSyncPayload[];
}

export type GenericSyncActionPayload =
  | SetBboxSyncPayload
  | SetTimeSyncPayload
  | SetLayerActionsSyncPayload;
