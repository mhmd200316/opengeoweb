/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { takeLatest, select, put } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';

import * as synchronizationGroupsSelector from './synchronizationGroups/selectors';
import { LayerActionPayloadsWithLayerIds, SyncLayerPayloads } from './types';
import {
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETLAYERACTIONS,
  SYNCGROUPS_TYPE_SETTIME,
} from './synchronizationGroups/constants';
import {
  setLayerActionSync,
  setBboxSync,
  setTimeSync,
} from './synchronizationActions/actions';
import { mapActions } from '../mapStore';

import {
  getAddLayerActionsTargets,
  getSetActiveLayerIdActionsTargets,
  getLayerActionsTargets,
  getLayerDeleteActionsTargets,
  getLayerMoveActionsTargets,
  getMapBaseLayerActionsTargets,
} from './synchronizationGroups/utils';
import {
  DeleteLayerPayload,
  MoveLayerPayload,
  LayerActionOrigin,
  SetBaseLayersPayload,
} from '../mapStore/types';

import { setBbox, setTime } from './actions';
import { layerActions, LayerActions } from '../mapStore/layers/reducer';

const {
  setBaseLayers,
  addLayer,
  layerChangeDimension,
  layerChangeEnabled,
  layerChangeName,
  layerChangeOpacity,
  layerChangeStyle,
  layerDelete,
} = layerActions;

const setTimeValidatorRexexp = new RegExp(
  '^(19|20)\\d\\d-(0[1-9]|1[012])-([012]\\d|3[01])T([01]\\d|2[0-3]):([0-5]\\d):([0-5]\\d)Z$',
);

export function* setTimeSaga({
  payload,
}: ReturnType<typeof setTime>): SagaIterator {
  const { value, origin } = payload;
  /* Test if the value is according to the expected time format */
  if (!setTimeValidatorRexexp.test(value)) {
    console.error(
      `setTime value ${value} does not conform to format [YYYY-MM-DDThh:mm:ssZ]. It was triggered by ${origin}`,
    );
  } else {
    const targets = yield select(
      synchronizationGroupsSelector.getTargets,
      payload,
      SYNCGROUPS_TYPE_SETTIME,
    );
    const groups = yield select(
      synchronizationGroupsSelector.getTargetGroups,
      payload,
      SYNCGROUPS_TYPE_SETTIME,
    );

    yield put(setTimeSync(payload, targets, groups));
  }
}

export function* setBBoxSaga({
  payload,
}: ReturnType<typeof setBbox>): SagaIterator {
  const targets = yield select(
    synchronizationGroupsSelector.getTargets,
    payload,
    SYNCGROUPS_TYPE_SETBBOX,
  );
  const groups = yield select(
    synchronizationGroupsSelector.getTargetGroups,
    payload,
    SYNCGROUPS_TYPE_SETBBOX,
  );

  yield put(setBboxSync(payload, targets, groups));
}

export function* layerActionsSaga({
  payload,
  type,
}: LayerActions): SagaIterator {
  /* Should not listen to actions from itself */
  if (payload && payload.origin === LayerActionOrigin.ReactMapViewParseLayer)
    return;
  const targets: SyncLayerPayloads[] = yield select(
    getLayerActionsTargets,
    payload as LayerActionPayloadsWithLayerIds,
    SYNCGROUPS_TYPE_SETLAYERACTIONS,
  );
  if (targets && targets.length > 0) {
    yield put(setLayerActionSync(payload as SyncLayerPayloads, targets, type));
  }
}

export function* addLayerActionsSaga({
  payload,
  type,
}: ReturnType<typeof layerActions.addLayer>): SagaIterator {
  /* Should not listen to actions from itself */
  if (payload && payload.origin === LayerActionOrigin.ReactMapViewParseLayer)
    return;
  const targets: SyncLayerPayloads[] = yield select(
    getAddLayerActionsTargets,
    payload,
    SYNCGROUPS_TYPE_SETLAYERACTIONS,
  );
  if (targets && targets.length > 0) {
    yield put(setLayerActionSync(payload, targets, type));
  }
}

export function* deleteLayerActionsSaga({
  payload,
  type,
}: ReturnType<typeof layerActions.layerDelete>): SagaIterator {
  /* Should not listen to actions from itself */
  if (payload && payload.origin === LayerActionOrigin.ReactMapViewParseLayer)
    return;

  const targets: DeleteLayerPayload[] = yield select(
    getLayerDeleteActionsTargets,
    payload,
    SYNCGROUPS_TYPE_SETLAYERACTIONS,
  );

  if (targets && targets.length > 0) {
    yield put(setLayerActionSync(payload, targets, type));
  }
}
export function* moveLayerActionsSaga({
  payload,
  type,
}: ReturnType<typeof mapActions.layerMoveLayer>): SagaIterator {
  /* Should not listen to actions from itself */
  if (payload && payload.origin === LayerActionOrigin.ReactMapViewParseLayer)
    return;

  const targets: MoveLayerPayload[] = yield select(
    getLayerMoveActionsTargets,
    payload,
    SYNCGROUPS_TYPE_SETLAYERACTIONS,
  );

  if (targets && targets.length > 0) {
    yield put(setLayerActionSync(payload, targets, type));
  }
}

export function* setActiveLayerIdActionsSaga({
  payload,
  type,
}: ReturnType<typeof mapActions.setActiveLayerId>): SagaIterator {
  const targets: SyncLayerPayloads[] = yield select(
    getSetActiveLayerIdActionsTargets,
    payload,
    SYNCGROUPS_TYPE_SETLAYERACTIONS,
  );
  if (targets && targets.length > 0) {
    yield put(setLayerActionSync(payload, targets, type));
  }
}

export function* mapBaseLayerActionsSaga({
  payload,
  type,
}: ReturnType<typeof layerActions.setBaseLayers>): SagaIterator {
  /* Should not listen to actions from itself */
  if (payload && payload.origin === LayerActionOrigin.ReactMapViewParseLayer)
    return;
  const targets: SetBaseLayersPayload[] = yield select(
    getMapBaseLayerActionsTargets,
    payload,
    SYNCGROUPS_TYPE_SETLAYERACTIONS,
  );
  if (targets && targets.length > 0) {
    yield put(setLayerActionSync(payload, targets, type));
  }
}

export function* rootSaga(): SagaIterator {
  yield takeLatest(setTime.type, setTimeSaga);
  yield takeLatest(setBbox.type, setBBoxSaga);
  yield takeLatest(layerChangeName.type, layerActionsSaga);
  yield takeLatest(layerChangeEnabled.type, layerActionsSaga);
  yield takeLatest(layerChangeOpacity.type, layerActionsSaga);
  yield takeLatest(layerChangeDimension.type, layerActionsSaga);
  yield takeLatest(layerChangeStyle.type, layerActionsSaga);

  yield takeLatest(layerDelete.type, deleteLayerActionsSaga);

  yield takeLatest(addLayer.type, addLayerActionsSaga);

  yield takeLatest(mapActions.layerMoveLayer.type, moveLayerActionsSaga);

  yield takeLatest(
    mapActions.setActiveLayerId.type,
    setActiveLayerIdActionsSaga,
  );

  yield takeLatest(setBaseLayers.type, mapBaseLayerActionsSaga);
}

export default rootSaga;
