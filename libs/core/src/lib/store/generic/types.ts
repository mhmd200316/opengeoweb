/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  AddLayerPayload,
  DeleteLayerPayload,
  MapActionOrigin,
  MoveLayerPayload,
  SetActiveLayerIdPayload,
  SetBaseLayersPayload,
  SetLayerDimensionPayload,
  SetLayerEnabledPayload,
  SetLayerNamePayload,
  SetLayerOpacityPayload,
  SetLayerStylePayload,
} from '../mapStore/types';

/* Generic Action payloads */

export interface SetTimePayload {
  sourceId: string;
  origin: string;
  value: string; // ISO 8601 string "like 2010-10-01T00:00:00Z"
}

export interface SetBboxPayload {
  sourceId: string;
  bbox: {
    left: number;
    bottom: number;
    right: number;
    top: number;
  };
  srs: string;
  origin?: MapActionOrigin | string;
  mapId?: string;
}

export type GenericActionPayload = SetBboxPayload | SetTimePayload;

export type LayerActionPayloadsWithLayerIds =
  | SetLayerOpacityPayload
  | SetLayerNamePayload
  | SetLayerEnabledPayload
  | SetLayerDimensionPayload
  | SetLayerStylePayload
  | SetActiveLayerIdPayload;

export type SyncLayerPayloads =
  | LayerActionPayloadsWithLayerIds
  | AddLayerPayload
  | MoveLayerPayload
  | DeleteLayerPayload
  | SetBaseLayersPayload;
