/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { WMJSMap, WMLayer } from '@opengeoweb/webmap';
import { Dimension, WMJSDimension } from '../types';

export const dateFormat = 'YYYY-MM-DDTHH:mm:ss[Z]';

let generatedLayerIds = 0;
export const generateLayerId = (): string => {
  generatedLayerIds += 1;
  return `layerid_${generatedLayerIds}`;
};

let generatedMapIds = 0;
export const generateMapId = (): string => {
  generatedMapIds += 1;
  return `mapid_${generatedMapIds}`;
};

let generatedTimesliderIds = 0;
export const generateTimesliderId = (): string => {
  generatedTimesliderIds += 1;
  return `timesliderid_${generatedTimesliderIds}`;
};

/**
 * Map for registering wmlayers with their id's
 */
const registeredWMLayersForReactLayerId = {};

/**
 * Registers a WMJSLayer in a lookuptable with a layerId
 * @param {WMLayer} wmLayer
 * @param {string} layerId
 */
export const registerWMLayer = (wmLayer: WMLayer, layerId: string): void => {
  registeredWMLayersForReactLayerId[layerId] = wmLayer;
};

/**
 * Get the WMLayer from the lookuptable with layerId
 * @param {string} layerId
 */
export const getWMLayerById = (layerId: string): WMLayer => {
  return registeredWMLayersForReactLayerId[layerId];
};

/**
 * Map for registering wmlayers with their id's
 */
const registeredWMMapForReactMapId = {};

/**
 * Registers a WMJSMap in a lookuptable with a wmjsMapId
 * @param {WMJSMap} wmjsMap
 * @param {string} wmjsMapId
 */
export const registerWMJSMap = (wmjsMap: WMJSMap, wmjsMapId: string): void => {
  registeredWMMapForReactMapId[wmjsMapId] = wmjsMap;
};

/**
 * Get the wmjsMap from the lookuptable with wmjsMapId
 * @param {string} wmjsMapId
 */
export const getWMJSMapById = (wmjsMapId: string): WMJSMap => {
  return registeredWMMapForReactMapId[wmjsMapId];
};

/**
 * Returns the WMJSDimension object for given layerId and dimension name
 * @param layerId The layerId
 * @param dimensionName The dimension to lookup
 */
export const getWMJSDimensionForLayerAndDimension = (
  layerId: string,
  dimensionName: string,
): WMJSDimension => {
  const wmLayer = getWMLayerById(layerId);
  if (!wmLayer || !dimensionName) {
    return null;
  }
  const wmjsDimension = wmLayer.getDimension(dimensionName);
  if (!wmjsDimension) {
    return null;
  }
  return wmjsDimension;
};

/**
 * Returns the list of dimensions without the time dimension
 * @param dimensions Dimension[]
 */
export const filterNonTimeDimensions = (
  dimensions: Dimension[],
): Dimension[] => {
  return dimensions && dimensions.length
    ? dimensions.filter((dim) => dim.name !== 'time')
    : [];
};

/**
 * Gets the WMJSTimeDimension for given activeLayerId and dimensions list
 * @param layerId: The layer id to search the WMJSDimension for
 * @return: The WMJSDimension if found, otherwise null
 */
export const getWMJSTimeDimensionForLayerId = (
  layerId: string,
): WMJSDimension => {
  const wmLayer = getWMLayerById(layerId);
  if (!wmLayer) {
    return null;
  }
  return wmLayer.getDimension('time');
};

/**
 * Get the previous step for this layerid
 * @param layerId The layerId to get the previous timestep for
 */
export const getPreviousTimeStepForLayerId = (layerId: string): string => {
  const wmjsTimeDim = getWMJSTimeDimensionForLayerId(layerId);
  if (!wmjsTimeDim) return null;
  const currentIndex = wmjsTimeDim.getIndexForValue(
    wmjsTimeDim.currentValue,
    false,
  );
  if (currentIndex < 0 || currentIndex - 1 < 0)
    return wmjsTimeDim.getClosestValue(wmjsTimeDim.currentValue, true);

  return wmjsTimeDim.getValueForIndex(currentIndex - 1);
};

/**
 * Get the next step for this layerid
 * @param layerId The layerId to get the next timestep for
 */
export const getNextTimeStepForLayerId = (layerId: string): string => {
  const wmjsTimeDim = getWMJSTimeDimensionForLayerId(layerId);
  if (!wmjsTimeDim) return null;
  const currentIndex = wmjsTimeDim.getIndexForValue(
    wmjsTimeDim.currentValue,
    false,
  );
  if (currentIndex < 0 || currentIndex + 1 >= wmjsTimeDim.size())
    return wmjsTimeDim.getClosestValue(wmjsTimeDim.currentValue, true);

  return wmjsTimeDim.getValueForIndex(currentIndex + 1);
};

/**
 * Get the first time step for this layerid
 * @param layerId The layerId to get the first timestep for
 */
export const getFirstTimeStepForLayerId = (layerId: string): string => {
  const wmjsTimeDim = getWMJSTimeDimensionForLayerId(layerId);
  if (!wmjsTimeDim) return null;
  return wmjsTimeDim.getFirstValue();
};

/**
 * Get the last time step for this layerid
 * @param layerId The layerId to get the last timestep for
 */
export const getLastTimeStepForLayerId = (layerId: string): string => {
  const wmjsTimeDim = getWMJSTimeDimensionForLayerId(layerId);
  if (!wmjsTimeDim) return null;
  return wmjsTimeDim.getLastValue();
};
