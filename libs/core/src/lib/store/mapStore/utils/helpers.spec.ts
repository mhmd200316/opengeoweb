/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  getWMJSTimeDimensionForLayerId,
  getWMLayerById,
  registerWMLayer,
  getPreviousTimeStepForLayerId,
  getNextTimeStepForLayerId,
  getFirstTimeStepForLayerId,
  getLastTimeStepForLayerId,
} from './helpers';
import {
  WmMultiDimensionLayer,
  WmLayerWithoutTimeDimension,
} from '../../../utils/defaultTestSettings';

describe('store/mapStore/utils/helpers', () => {
  const layerIdWithTime = 'test_id_1';
  WmMultiDimensionLayer.id = layerIdWithTime;
  registerWMLayer(WmMultiDimensionLayer, WmMultiDimensionLayer.id);

  const layerIdWithoutTime = 'test_id_2';
  WmLayerWithoutTimeDimension.id = layerIdWithoutTime;
  registerWMLayer(WmLayerWithoutTimeDimension, WmLayerWithoutTimeDimension.id);

  describe('getWMLayerById', () => {
    it('should return the WMJSLayer for a layer id', () => {
      const wmLayer = getWMLayerById(layerIdWithTime);

      expect(wmLayer).toBeDefined();
    });
    it('should return the same id for given layer id', () => {
      const wmLayer = getWMLayerById(layerIdWithTime);

      expect(wmLayer.id).toEqual(layerIdWithTime);
    });
    it('should return undefined when layer does not exist', () => {
      const wmLayer = getWMLayerById('hello');
      expect(wmLayer).toBeUndefined();
    });
  });

  describe('getWMJSTimeDimensionForLayerId', () => {
    it('should return the WMJSTimeDimension for a layer id', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension).toBeDefined();
    });

    it('should have the name time', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension.name).toEqual('time');
    });

    it('should have a currentValue', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension.currentValue).toEqual('2020-03-13T14:40:00Z');
    });

    it('should have a size', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      expect(wmjsTimeDimension.size()).toEqual(49);
    });

    it('should return undefined if the layer has no time dimension', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(
        layerIdWithoutTime,
      );
      expect(wmjsTimeDimension).toBeUndefined();
    });

    it('should return null if the layer does not exist', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId('hello');
      expect(wmjsTimeDimension).toBeNull();
    });
  });

  describe('getPreviousTimeStepForLayerId', () => {
    it('should return the previous timestep for a layer id', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      wmjsTimeDimension.setValue('2020-03-13T14:40:00Z');
      const newTimeStep = getPreviousTimeStepForLayerId(layerIdWithTime);
      expect(newTimeStep).toEqual('2020-03-13T14:35:00Z');
    });

    it('should return the same value if already at the beginning of the time dimension for a layer id', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      wmjsTimeDimension.setValue('2020-03-13T10:40:00Z');
      const newTimeStep = getPreviousTimeStepForLayerId(layerIdWithTime);
      expect(newTimeStep).toEqual('2020-03-13T10:40:00Z');
    });

    it('should return null if layer does not exist', () => {
      const result = getPreviousTimeStepForLayerId('hello');
      expect(result).toBeNull();
    });

    it('should return null if layer has no time dimension', () => {
      const result = getPreviousTimeStepForLayerId(layerIdWithoutTime);
      expect(result).toBeNull();
    });
  });

  describe('getNextTimeStepForLayerId', () => {
    it('should return the next timestep for a layer id', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      wmjsTimeDimension.setValue('2020-03-13T13:40:00Z');
      const newTimeStep = getNextTimeStepForLayerId(layerIdWithTime);
      expect(newTimeStep).toEqual('2020-03-13T13:45:00Z');
    });

    it('should return same value if already at the end of the time dimension for a layer id', () => {
      const wmjsTimeDimension = getWMJSTimeDimensionForLayerId(layerIdWithTime);
      wmjsTimeDimension.setValue('2020-03-13T14:40:00Z');
      const newTimeStep = getNextTimeStepForLayerId(layerIdWithTime);
      expect(newTimeStep).toEqual('2020-03-13T14:40:00Z');
    });

    it('should return null if layer does not exist', () => {
      const result = getNextTimeStepForLayerId('hello');
      expect(result).toBeNull();
    });

    it('should return null if layer has no time dimension', () => {
      const result = getNextTimeStepForLayerId(layerIdWithoutTime);
      expect(result).toBeNull();
    });
  });

  describe('getFirstTimeStepForLayerId', () => {
    it('should return the first timestep for a layer id', () => {
      const firstTimeStep = getFirstTimeStepForLayerId(layerIdWithTime);
      expect(firstTimeStep).toEqual('2020-03-13T10:40:00Z');
    });

    it('should return null if layer does not exist', () => {
      const result = getFirstTimeStepForLayerId('hello');
      expect(result).toBeNull();
    });

    it('should return null if layer has no time dimension', () => {
      const result = getFirstTimeStepForLayerId(layerIdWithoutTime);
      expect(result).toBeNull();
    });
  });

  describe('getLastTimeStepForLayerId', () => {
    it('should return the last timestep for a layer id', () => {
      const lastTimeStep = getLastTimeStepForLayerId(layerIdWithTime);
      expect(lastTimeStep).toEqual('2020-03-13T14:40:00Z');
    });

    it('should return null if layer does not exist', () => {
      const result = getLastTimeStepForLayerId('hello');
      expect(result).toBeNull();
    });

    it('should return null if layer has no time dimension', () => {
      const result = getLastTimeStepForLayerId(layerIdWithoutTime);
      expect(result).toBeNull();
    });
  });
});
