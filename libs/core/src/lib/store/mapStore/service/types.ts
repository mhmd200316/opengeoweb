/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Style } from '@opengeoweb/webmap';

export interface Services {
  [key: string]: ReduxService;
}

export interface ReduxService {
  name?: string;
  serviceUrl?: string;
  active?: boolean;
  layers?: ServiceLayer[];
  isUserAddedService?: boolean;
}

/**
 * Used in the services object of webmap to keep a list of available layers in the service
 */
export interface ServiceLayer {
  name: string;
  text: string;
  leaf: boolean;
  path: string[];
  abstract?: string;
  keywords?: string[];
  styles?: Style[];
}

export interface ServiceState {
  byId: Services;
  allIds: string[];
}
export interface SetLayersForServicePayload {
  id: string;
  name: string;
  serviceUrl: string;
  layers: ServiceLayer[];
  isUserAddedService: boolean;
}

export interface MapStoreRemoveServicePayload {
  id: string;
  serviceUrl: string;
}
