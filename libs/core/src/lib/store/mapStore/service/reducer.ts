/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  MapStoreRemoveServicePayload,
  ServiceState,
  SetLayersForServicePayload,
} from './types';
import { layerActions } from '../layers/reducer';

export const initialState: ServiceState = {
  byId: {},
  allIds: [],
};

const slice = createSlice({
  initialState,
  name: 'serviceReducer',
  reducers: {
    serviceSetLayers: (
      draft,
      action: PayloadAction<SetLayersForServicePayload>,
    ) => {
      const { id } = action.payload;
      draft.byId[id] = { ...action.payload };
      if (!draft.allIds.includes(id)) draft.allIds.push(id);
    },
    mapStoreRemoveService: (
      draft,
      action: PayloadAction<MapStoreRemoveServicePayload>,
    ) => {
      const foundServiceIndex = draft.allIds.findIndex((id) => {
        return draft.byId[id].serviceUrl === action.payload.serviceUrl;
      });
      if (foundServiceIndex !== -1) {
        const serviceId = draft.allIds[foundServiceIndex];
        draft.allIds = draft.allIds.filter((id) => id !== serviceId);
        delete draft.byId[serviceId];
      }
    },
  },
  extraReducers: (builder) =>
    builder.addCase(layerActions.onUpdateLayerInformation, (draft, action) => {
      const { serviceLayers } = action.payload;
      if (serviceLayers === null) return draft;
      const mapAction = serviceActions.serviceSetLayers(serviceLayers);
      const newState: ServiceState = reducer(draft, mapAction);
      return newState;
    }),
});

export const { reducer } = slice;
export const serviceActions = slice.actions;

export type ServiceActions =
  | ReturnType<typeof serviceActions.mapStoreRemoveService>
  | ReturnType<typeof serviceActions.serviceSetLayers>
  | ReturnType<typeof layerActions.onUpdateLayerInformation>;
