/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as serviceSelectors from './selectors';
import {
  defaultReduxServices,
  styleListForRADNLOPERR25PCPRRL3KNMILayer,
} from '../../../utils/defaultTestSettings';
import { AppStore } from '../../../types/types';

const fakeState: AppStore = {
  services: {
    byId: defaultReduxServices,
    allIds: ['serviceid_1'],
  },
};

describe('store/mapStore/service/selectors', () => {
  describe('getServices', () => {
    it('should return services', () => {
      expect(serviceSelectors.getServices(fakeState)).toEqual(
        fakeState.services.byId,
      );
      expect(serviceSelectors.getServices({})).toEqual({});
    });
  });

  describe('getServiceIds', () => {
    it('should return array of service ids when existing', () => {
      const result = serviceSelectors.getServiceIds(fakeState);
      expect(result).toEqual(fakeState.services.allIds);
      expect(result).toHaveLength(1);
    });
    it('should return empty list when store does not exist', () => {
      expect(serviceSelectors.getServiceIds(null)).toHaveLength(0);
    });
  });

  describe('getServiceByName', () => {
    it('should return service object by name ', () => {
      const serviceUrl = 'https://testservice';
      const result = serviceSelectors.getServiceByName(fakeState, serviceUrl);
      expect(result).toEqual(fakeState.services.byId['serviceid_1']);
    });
    it('should return null when service store does not exist', () => {
      expect(
        serviceSelectors.getServiceByName(fakeState, 'doesnotexist'),
      ).toBeNull();
    });
  });

  describe('getLayersFromService', () => {
    it('should return services layers when service exists', () => {
      const serviceUrl = 'https://testservice';
      expect(
        serviceSelectors.getLayersFromService(fakeState, serviceUrl),
      ).toEqual(fakeState.services.byId['serviceid_1'].layers);
    });

    it('should return null when service store does not exist', () => {
      expect(
        serviceSelectors.getLayersFromService(fakeState, 'fake-service'),
      ).toBeNull();
    });
  });

  describe('getLayerFromService', () => {
    const serviceUrl = 'https://testservice';
    const layerName = 'RADNL_OPER_R___25PCPRR_L3_KNMI';
    it('should return the layer from the chosen service', () => {
      const result = serviceSelectors.getLayerFromService(
        fakeState,
        serviceUrl,
        layerName,
      );
      expect(result).not.toBeNull();
      expect(result.name).toBe(layerName);
    });
    it('should return null when layer does not exist', () => {
      expect(
        serviceSelectors.getLayerFromService(
          fakeState,
          serviceUrl,
          'testlayer',
        ),
      ).toBeNull();
    });
    it('should return null when the service and layer do not exist', () => {
      expect(
        serviceSelectors.getLayerFromService(
          fakeState,
          'testservice',
          'testlayer',
        ),
      ).toBeNull();
    });
  });
  describe('getLayerStyles', () => {
    const serviceUrl = 'https://testservice';
    const layerName = 'RADNL_OPER_R___25PCPRR_L3_KNMI';
    it('should return array of available layer styles', () => {
      const result = serviceSelectors.getLayerStyles(
        fakeState,
        serviceUrl,
        layerName,
      );

      expect(result).toEqual(styleListForRADNLOPERR25PCPRRL3KNMILayer);
    });
    it('should return empty list when layer does not exist', () => {
      expect(
        serviceSelectors.getLayerStyles(fakeState, serviceUrl, 'testlayer'),
      ).toHaveLength(0);
    });
    it('should return empty list when styles does not exist', () => {
      expect(
        serviceSelectors.getLayerStyles(
          fakeState,
          serviceUrl,
          'RAD_NL25_PCP_CM',
        ),
      ).toHaveLength(0);
    });
  });
});
