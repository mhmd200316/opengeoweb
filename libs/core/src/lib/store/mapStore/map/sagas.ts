/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable require-yield */
import {
  takeLatest,
  select,
  put,
  call,
  takeEvery,
  all,
} from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import moment, { Moment } from 'moment';

import { mapActions, mapSelectors } from '.';

import * as layerSelectors from '../layers/selectors';
import { layerActions } from '../layers';

import { Layer, LayerActionOrigin } from '../layers/types';
import {
  getSpeedDelay,
  roundWithTimeStep,
} from '../../../components/TimeSlider/TimeSliderUtils';
import { dateFormat, generateLayerId, getWMJSMapById } from '../utils/helpers';
import { generateUniqueLayerIds } from '../../../components/SyncGroups/utils';
import { animationIntervalDefault } from '../../../components/TimeSlider/TimeSliderButtons/PlayButton/PlayButtonConnect';
import { filterLayers } from '../../../utils/jsonPresetFilter';
import { baseLayerGrey, overLayer } from '../../../utils/publicLayers';
import { MapActionOrigin } from './types';
import { genericActions } from '../../generic';
import { uiActions } from '../../ui';
import { IS_LEGEND_OPEN_BY_DEFAULT } from '../../../components/Legend/LegendConnect';

// Expects start, end time as moment object and interval in minutes
export const generateTimeList = (
  start: Moment,
  end: Moment,
  interval: number,
): { name: string; value: string }[] => {
  const timeList = [];
  const unixStart = moment(start).utc().unix();
  const unixEnd = moment(end).utc().unix();
  const intervalSeconds = interval * 60;
  for (let j = unixStart; j <= unixEnd; j += intervalSeconds) {
    timeList.push({ name: 'time', value: moment.unix(j).toISOString() });
  }
  return timeList;
};

export function updateMapDraw(mapId: string, draw: unknown): void {
  const webMap = getWMJSMapById(mapId);
  if (webMap) {
    webMap.getListener().suspendEvents();
    webMap.stopAnimating();
    webMap.draw(draw);
    webMap.getListener().resumeEvents();
  }
}

export function* startAnimationSaga({
  payload,
}: ReturnType<typeof mapActions.mapStartAnimation>): Generator {
  const { mapId, start, end, interval, timeList } = payload;

  const roundedStart = roundWithTimeStep(
    start && moment.utc(start).unix(),
    interval,
    'ceil',
  );
  const roundedEnd = roundWithTimeStep(
    end && moment.utc(end).unix(),
    interval,
    'floor',
  );

  const animationList = yield timeList ||
    generateTimeList(
      moment.utc(roundedStart * 1000),
      moment.utc(roundedEnd * 1000),
      interval,
    );

  yield updateMapDraw(mapId, animationList);
}

export function* stopAnimationSaga({
  payload,
}: ReturnType<typeof mapActions.mapStopAnimation>): Generator {
  const { mapId } = payload;
  yield updateMapDraw(mapId, 'opengeoweb-core-map-reducer');
}

export function* deleteLayerSaga({
  payload,
}: ReturnType<typeof layerActions.layerDelete>): SagaIterator {
  const { mapId } = payload;
  const layers = yield select(mapSelectors.getMapLayers, mapId);
  if (!layers.length) {
    yield put(mapActions.mapStopAnimation({ mapId }));
  }
}

export function* updateAnimation(
  mapId: string,
  maxValue: string,
): SagaIterator {
  const animationStart = yield select(
    mapSelectors.getAnimationStartTime,
    mapId,
  );
  const animationStartUnix = moment.utc(animationStart).unix();
  const animationEnd = yield select(mapSelectors.getAnimationEndTime, mapId);
  const animationEndUnix = moment.utc(animationEnd).unix();
  const t = moment.utc(maxValue).unix();
  const deltaT = t - animationEndUnix;

  const start = moment
    .unix(animationStartUnix + deltaT)
    .utc()
    .format(dateFormat);
  const end = moment
    .unix(animationEndUnix + deltaT)
    .utc()
    .format(dateFormat);

  yield put(
    mapActions.setAnimationEndTime({
      mapId,
      animationEndTime: end,
    }),
  );
  yield put(
    mapActions.setAnimationStartTime({
      mapId,
      animationStartTime: start,
    }),
  );

  const isAnimating = yield select(mapSelectors.isAnimating, mapId);
  if (isAnimating) {
    // restart animation if animation was running
    const timeStep = yield select(mapSelectors.getMapTimeStep, mapId);
    yield put(
      mapActions.mapStartAnimation({
        mapId,
        start,
        end,
        interval: timeStep,
      }),
    );
  }
}

export function* setLayerDimensionsSaga({
  payload,
}: ReturnType<typeof layerActions.onUpdateLayerInformation>): SagaIterator {
  try {
    const { layerDimensions } = payload;
    if (!layerDimensions) return;
    const { dimensions, layerId } = layerDimensions;
    const layer = yield select(layerSelectors.getLayerById, layerId);
    if (!layer) return;

    const newTimeDimension = dimensions.find(
      (dimension) => dimension.name === 'time',
    );
    const { mapId } = layer;
    const activeLayerId = yield select(mapSelectors.getActiveLayerId, mapId);
    const shouldAutoUpdate = yield select(mapSelectors.isAutoUpdating, mapId);
    const prevTimeDimension = yield select(
      layerSelectors.getLayerTimeDimension,
      layerId,
    );

    const isAnimating = yield select(mapSelectors.isAnimating, mapId);
    const webmapInstance = getWMJSMapById(mapId);

    if (
      layerId === activeLayerId && // only update the active layer
      shouldAutoUpdate &&
      newTimeDimension &&
      newTimeDimension.maxValue &&
      prevTimeDimension &&
      prevTimeDimension.currentValue &&
      prevTimeDimension.currentValue !== newTimeDimension.maxValue
    ) {
      /* 
        To prevent weird animation time jumping while updating, 
        this should only update the layer dimension if it is not animating, 
        otherwise the animation will jump between max and then back to its own animation time.
        When animating, the animation logic makles sure that the layer is updated to the latest value 
      */
      if (!isAnimating) {
        yield put(
          layerActions.layerChangeDimension({
            layerId,
            origin: LayerActionOrigin.setLayerDimensionSaga,
            dimension: {
              name: 'time',
              currentValue: newTimeDimension.maxValue,
            },
          }),
        );
      }
      yield call(updateAnimation, mapId, newTimeDimension.maxValue);
      // If there is a discrepancy between the redux store and the webmap isAnimating
      // it means the animation hasn't started yet - start it now we have the time dimension
    } else if (
      isAnimating &&
      !webmapInstance.isAnimating &&
      newTimeDimension &&
      newTimeDimension.maxValue
    ) {
      yield call(updateAnimation, mapId, newTimeDimension.maxValue);
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.warn(error);
  }
}

export function* toggleAutoUpdateSaga({
  payload,
}: ReturnType<typeof mapActions.toggleAutoUpdate>): SagaIterator {
  try {
    const { shouldAutoUpdate, mapId } = payload;
    if (shouldAutoUpdate) {
      // go to end of active layer
      const layerId = yield select(mapSelectors.getActiveLayerId, mapId);
      const timeDimension = yield select(
        layerSelectors.getLayerTimeDimension,
        layerId,
      );
      if (timeDimension && timeDimension.maxValue) {
        yield put(
          layerActions.layerChangeDimension({
            layerId,
            origin: LayerActionOrigin.toggleAutoUpdateSaga,
            dimension: {
              name: 'time',
              currentValue: timeDimension.maxValue,
            },
          }),
        );

        yield call(updateAnimation, mapId, timeDimension.maxValue);
      }
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.warn(error);
  }
}

export function* handleBaseLayersSaga(
  mapId: string,
  baseLayers: Layer[],
): SagaIterator {
  const baseLayer = baseLayers.find((layer) => layer.layerType === 'baseLayer');
  const currentAvailableBaseLayers = yield select(
    layerSelectors.getAvailableBaseLayersForMap,
    mapId,
  );

  // find a availableBaseLayer with the same name, and use that id for the active baselayer
  const activeAvailableBaseLayer = currentAvailableBaseLayers.find(
    (availableBaseLayer) => availableBaseLayer.name === baseLayer.name,
  );

  const activeBaseLayerId = activeAvailableBaseLayer
    ? activeAvailableBaseLayer.id
    : generateLayerId();

  // if the baseLayer can't be found in a visible available baseLayer list, add it
  if (!activeAvailableBaseLayer && currentAvailableBaseLayers.length) {
    yield put(
      layerActions.addAvailableBaseLayers({
        // TODO: remove type casting in https://gitlab.com/opengeoweb/opengeoweb/-/issues/1884
        layers: [{ ...baseLayer, mapId, id: activeBaseLayerId }] as Layer[],
      }),
    );
  }

  const baseLayersWithActiveId = baseLayers.map((layer, index) =>
    index === 0
      ? {
          ...layer,
          id: activeBaseLayerId || layer.id,
        }
      : layer,
  );

  yield put(
    layerActions.setBaseLayers({
      mapId,
      // TODO: remove type casting in https://gitlab.com/opengeoweb/opengeoweb/-/issues/1884
      layers: baseLayersWithActiveId as Layer[],
    }),
  );
}

export function* setMapPresetSaga({
  payload,
}: ReturnType<typeof mapActions.setMapPreset>): SagaIterator {
  try {
    const { mapId, initialProps } = payload;
    const { mapPreset } = initialProps;

    if (mapPreset) {
      const {
        layers,
        activeLayerId,
        proj,
        shouldAutoUpdate,
        shouldAnimate,
        animationPayload,
        toggleTimestepAuto,
        showTimeSlider,
        displayMapPin,
        shouldShowZoomControls,
        shouldShowLegend,
      } = mapPreset;

      const { mapLayers, baseLayers, overLayers } = filterLayers(layers);
      if (layers) {
        //  make sure all layers have a unique id before going forward
        const { layersNewIds, activeLayerNewId } = yield call(
          generateUniqueLayerIds,
          mapLayers,
          activeLayerId,
        );

        //  set layers
        yield put(
          layerActions.setLayers({
            mapId,
            layers: layersNewIds,
          }),
        );

        //  set active layer if given otherwise to first layer
        const newActiveLayerId =
          activeLayerNewId ||
          (layersNewIds.length ? layersNewIds[0].id : undefined);

        yield put(
          mapActions.setActiveLayerId({
            mapId,
            layerId: newActiveLayerId,
          }),
        );
      }

      // sets (default) baseLayers
      const baseLayersWithDefaultLayer = baseLayers.length
        ? baseLayers
        : [baseLayerGrey];
      // sets (default) overLayers
      const overLayersWithDefaultLayer = overLayers.length
        ? overLayers
        : [overLayer];

      const allBaseLayers = [
        ...baseLayersWithDefaultLayer,
        ...overLayersWithDefaultLayer,
      ].map((layer) => ({
        ...layer,
        id: generateLayerId(),
      })) as Layer[];

      yield call(handleBaseLayersSaga, mapId, allBaseLayers);

      if (proj) {
        //  set bbox
        yield put(
          mapActions.setBbox({
            mapId,
            bbox: proj?.bbox,
            srs: proj?.srs,
          }),
        );
      }

      if (shouldAutoUpdate !== undefined) {
        //  auto update
        yield put(
          mapActions.toggleAutoUpdate({
            mapId,
            shouldAutoUpdate,
          }),
        );
      }

      if (showTimeSlider !== undefined) {
        // toggle timeslider
        yield put(
          mapActions.toggleTimeSliderIsVisible({
            mapId,
            isTimeSliderVisible: showTimeSlider,
          }),
        );
      }

      if (shouldShowZoomControls !== undefined) {
        // toggle zoom controls
        yield put(
          mapActions.toggleZoomControls({ mapId, shouldShowZoomControls }),
        );
      }

      if (displayMapPin !== undefined) {
        //  display map pin
        yield put(
          mapActions.toggleMapPinIsVisible({
            mapId,
            displayMapPin,
          }),
        );
      }

      // sets timestep by interval of animationPayload
      const interval = animationPayload && animationPayload.interval;
      if (interval) {
        yield put(mapActions.setAnimationInterval({ mapId, interval }));
      }

      // sets animationDelay by speed of animationPayload
      if (animationPayload && animationPayload.speed) {
        yield put(
          mapActions.setAnimationDelay({
            mapId,
            animationDelay: getSpeedDelay(animationPayload.speed),
          }),
        );
      }

      //  turn animation on
      if (shouldAnimate === true) {
        const duration =
          animationPayload && animationPayload.duration
            ? animationPayload.duration
            : 5 * 60; //  set to default of 5 hours
        yield put(
          mapActions.mapStartAnimation({
            mapId,
            start: moment
              .utc()
              .subtract(duration, 'minutes')
              .format(dateFormat),
            end: moment.utc().format(dateFormat),
            interval: interval || animationIntervalDefault,
          }),
        );

        //  If animation interval set, set the timestep auto property to false
        if (interval) {
          yield put(
            mapActions.toggleTimestepAuto({
              mapId,
              timestepAuto: false,
            }),
          );
        }
      }

      //  Set timestep auto based on preset if animation is off
      else if (toggleTimestepAuto !== undefined) {
        yield put(
          mapActions.toggleTimestepAuto({
            mapId,
            timestepAuto: toggleTimestepAuto,
          }),
        );
      }

      // show legend
      const shouldOpenLegend =
        shouldShowLegend !== undefined
          ? shouldShowLegend
          : IS_LEGEND_OPEN_BY_DEFAULT;
      const legendId = yield select(mapSelectors.getLegendId, mapId);
      if (legendId) {
        yield put(
          uiActions.setToggleOpenDialog({
            type: legendId,
            setOpen: shouldOpenLegend,
          }),
        );
      }
    }
  } catch (error) {
    console.error(error);
  }
}

export type SupportedMapPresetChangeActions =
  | ReturnType<typeof layerActions.setBaseLayers>
  | ReturnType<typeof layerActions.layerDelete>
  | ReturnType<typeof layerActions.addLayer>
  | ReturnType<typeof layerActions.layerChangeEnabled>
  | ReturnType<typeof layerActions.layerChangeName>
  | ReturnType<typeof layerActions.layerChangeStyle>
  | ReturnType<typeof layerActions.layerChangeOpacity>
  | ReturnType<typeof layerActions.layerChangeDimension>
  | ReturnType<typeof mapActions.layerMoveLayer>
  | ReturnType<typeof mapActions.setActiveLayerId>
  | ReturnType<typeof mapActions.toggleAutoUpdate>
  | ReturnType<typeof mapActions.mapStartAnimation>
  | ReturnType<typeof mapActions.mapStopAnimation>
  | ReturnType<typeof mapActions.toggleTimeSliderIsVisible>
  | ReturnType<typeof mapActions.setTimeStep>
  | ReturnType<typeof mapActions.setAnimationDelay>
  | ReturnType<typeof mapActions.toggleTimestepAuto>
  | ReturnType<typeof uiActions.setActiveMapIdForDialog>
  | ReturnType<typeof uiActions.setToggleOpenDialog>
  | ReturnType<typeof genericActions.setBbox>;

export const mapPresetChangeActions = [
  layerActions.setBaseLayers.type,
  layerActions.layerDelete.type,
  layerActions.addLayer.type,
  layerActions.layerChangeEnabled.type,
  layerActions.layerChangeName.type,
  layerActions.layerChangeStyle.type,
  layerActions.layerChangeOpacity.type,
  layerActions.layerChangeDimension.type,
  mapActions.layerMoveLayer.type,
  mapActions.setActiveLayerId.type,
  mapActions.toggleAutoUpdate,
  mapActions.mapStartAnimation,
  mapActions.mapStopAnimation,
  mapActions.toggleTimeSliderIsVisible,
  mapActions.setTimeStep,
  mapActions.setAnimationDelay,
  mapActions.toggleTimestepAuto,
  uiActions.setActiveMapIdForDialog,
  uiActions.setToggleOpenDialog,
  genericActions.setBbox.type,
];

type SupportedOrigins = LayerActionOrigin | MapActionOrigin;

const supportedChangeOrigins: SupportedOrigins[] = [
  LayerActionOrigin.layerManager,
  LayerActionOrigin.wmsLoader,
  MapActionOrigin.map,
];

export function* checkMapPresetForChangesSaga(
  action: SupportedMapPresetChangeActions,
): SagaIterator {
  const {
    payload: { mapId, origin },
  } = action;
  if (!origin) {
    return;
  }

  const isChangeOriginSupported =
    supportedChangeOrigins.indexOf(origin as SupportedOrigins) >= 0;

  if (isChangeOriginSupported) {
    const hasMapPresetChanges = yield select(
      mapSelectors.getHasMapPresetChanges,
      mapId,
    );
    // don't fire action if map preset already has changes
    if (!hasMapPresetChanges) {
      yield put(
        mapActions.setHasMapPresetChanges({
          mapId,
          hasChanges: true,
        }),
      );
    }
  }
}

export function* rootSaga(): SagaIterator {
  // resets WMJSMap state
  yield all([
    takeLatest(mapActions.mapStopAnimation.type, stopAnimationSaga),
    takeLatest(mapActions.setActiveMapPresetId.type, stopAnimationSaga),
  ]);
  yield takeLatest(mapActions.mapStartAnimation.type, startAnimationSaga);
  yield takeLatest(layerActions.layerDelete.type, deleteLayerSaga);
  yield takeLatest(
    layerActions.onUpdateLayerInformation.type,
    setLayerDimensionsSaga,
  );
  yield takeLatest(mapActions.toggleAutoUpdate.type, toggleAutoUpdateSaga);
  yield takeEvery(mapActions.setMapPreset.type, setMapPresetSaga);

  yield all(
    mapPresetChangeActions.map((action) =>
      takeEvery(action, checkMapPresetForChangesSaga),
    ),
  );
}

export default rootSaga;
