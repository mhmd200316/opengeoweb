/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import produce from 'immer';
import { mapSelectors } from '.';
import { createMap } from './utils';
import { LayerType, Scale } from '../types';
import {
  defaultAnimationDelayAtStart,
  defaultDataScaleToSecondsPerPx,
  defaultTimeStep,
  getSpeedFactor,
} from '../../../components/TimeSlider/TimeSliderUtils';
import { dateFormat } from '../utils/helpers';

const mapId = 'map-1';
const mockMap = createMap({ id: mapId });
const mockStoreDefaultMap = {
  webmap: {
    byId: {
      [mapId]: mockMap,
    },
    allIds: [mapId],
  },
};
const layerId1 = 'layer-1';
const layerId2 = 'layer-2';
const layerId3 = 'layer-3';
const mockStoreDefaultWithPinLocation = {
  webmap: {
    byId: {
      [mapId]: {
        ...mockMap,
        mapPinLocation: { lat: 52.0, lon: 5.0 },
        disableMapPin: true,
        displayMapPin: true,
      },
    },
    allIds: [mapId],
  },
};
const mockStoreMapWithLayers = {
  webmap: {
    byId: {
      [mapId]: {
        ...mockMap,
        mapLayers: [layerId1],
        baseLayers: [layerId2],
        overLayers: [layerId3],
        activeLayerId: layerId1,
      },
    },
    allIds: [mapId],
  },
  layers: {
    byId: {
      [layerId1]: {
        mapId,
        service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
        name: 'RAD_NL25_PCP_CM',
        format: 'image/png',
        style: 'knmiradar/nearest',
        id: layerId1,
        opacity: 1,
        enabled: true,
        layerType: LayerType.mapLayer,
      },
      [layerId2]: {
        mapId,
        name: 'arcGisSat',
        title: 'arcGisSat',
        type: 'twms',
        id: layerId2,
        opacity: 1,
        enabled: true,
        layerType: LayerType.baseLayer,
      },
      [layerId3]: {
        mapId,
        name: 'someOverLayer',
        title: 'someOverLayer',
        type: 'twms',
        id: layerId3,
        opacity: 1,
        enabled: true,
        layerType: LayerType.overLayer,
      },
    },
    allIds: [layerId1, layerId2, layerId3],
    availableBaseLayers: { allIds: [], byId: {} },
  },
};

describe('store/mapStore/map/selectors', () => {
  describe('getMapById', () => {
    it('should return the map when it exists', () => {
      expect(mapSelectors.getMapById(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId],
      );
    });
    it('should return null when the map does not exist', () => {
      expect(mapSelectors.getMapById(mockStoreDefaultMap, 'fake-id')).toEqual(
        null,
      );
    });
    it('should return null when the store does not exist', () => {
      expect(mapSelectors.getMapById(null, 'fake-id')).toEqual(null);
    });
  });

  describe('getAllMapIds', () => {
    it('should return all existing map ids in store', () => {
      expect(mapSelectors.getAllMapIds(mockStoreDefaultMap)).toEqual(
        mockStoreDefaultMap.webmap.allIds,
      );
    });
    it('should return empty array when no maps exist', () => {
      expect(
        mapSelectors.getAllMapIds({
          webmap: {
            byId: {},
            allIds: [],
          },
        }),
      ).toEqual([]);
    });
    it('should return empty array when the store does not exist', () => {
      expect(mapSelectors.getAllMapIds(null)).toEqual([]);
    });
  });

  describe('getFirstMap', () => {
    it('should return the map when it exists', () => {
      expect(mapSelectors.getFirstMap(mockStoreDefaultMap)).toEqual(
        mockStoreDefaultMap.webmap.byId[mockStoreDefaultMap.webmap.allIds[0]],
      );
    });
    it('should return null when no map exists', () => {
      expect(
        mapSelectors.getFirstMap({
          webmap: {
            byId: {},
            allIds: ['mapId1'],
          },
        }),
      ).toEqual(null);
    });

    it('should return null when  map exists', () => {
      expect(
        mapSelectors.getFirstMap({
          webmap: {
            byId: {},
            allIds: [],
          },
        }),
      ).toEqual(null);
    });
    it('should return null when the store does not exist', () => {
      expect(mapSelectors.getFirstMap(null)).toEqual(null);
    });
  });

  describe('getFirstMap', () => {
    it('should return the map id when it exists', () => {
      expect(mapSelectors.getFirstMapId(mockStoreDefaultMap)).toEqual(
        mockStoreDefaultMap.webmap.allIds[0],
      );
    });
    it('should return empty string when no map exists', () => {
      expect(
        mapSelectors.getFirstMapId({
          webmap: {
            byId: {},
            allIds: [],
          },
        }),
      ).toEqual('');
    });
    it('should return null when the store does not exist', () => {
      expect(mapSelectors.getFirstMap(null)).toEqual(null);
    });
  });

  describe('getIsMapPresent', () => {
    it('should return true when it exists', () => {
      expect(mapSelectors.getIsMapPresent(mockStoreDefaultMap, mapId)).toEqual(
        true,
      );
    });
    it('should return false when the map does not exist', () => {
      expect(
        mapSelectors.getIsMapPresent(mockStoreDefaultMap, 'fake-id'),
      ).toEqual(false);
    });
    it('should return null when the store does not exist', () => {
      expect(mapSelectors.getIsMapPresent(null, 'fake-id')).toEqual(false);
    });
  });

  describe('getLayerIds', () => {
    it('should return all layer ids for the map', () => {
      expect(mapSelectors.getLayerIds(mockStoreMapWithLayers, mapId)).toEqual(
        mockStoreMapWithLayers.webmap.byId[mapId].mapLayers,
      );
    });
    it('should return an empty list if the map has no layers', () => {
      expect(mapSelectors.getLayerIds(mockStoreDefaultMap, mapId)).toHaveLength(
        0,
      );
    });
    it('should return an empty list if the map does not exist', () => {
      expect(
        mapSelectors.getLayerIds(mockStoreDefaultMap, 'fake-id'),
      ).toHaveLength(0);
    });
  });

  describe('getLayers', () => {
    it('should return the list of layer objects for the map', () => {
      expect(mapSelectors.getMapLayers(mockStoreMapWithLayers, mapId)).toEqual([
        {
          mapId,
          service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
          name: 'RAD_NL25_PCP_CM',
          format: 'image/png',
          style: 'knmiradar/nearest',
          id: layerId1,
          opacity: 1,
          enabled: true,
          layerType: LayerType.mapLayer,
        },
      ]);
    });
    it('should return an empty list when the map has no layers', () => {
      expect(
        mapSelectors.getMapLayers(mockStoreDefaultMap, mapId),
      ).toHaveLength(0);
    });
  });

  describe('getMapBaseLayersIds', () => {
    it('should return a list of baselayer ids for the map', () => {
      const result = mapSelectors.getMapBaseLayersIds(
        mockStoreMapWithLayers,
        mapId,
      );
      expect(result).toHaveLength(1);
      expect(result).toEqual(
        mockStoreMapWithLayers.webmap.byId[mapId].baseLayers,
      );
    });
    it('should return an empty list when store does not exist', () => {
      const result = mapSelectors.getMapBaseLayersIds(null, mapId);
      expect(result).toHaveLength(0);
    });
  });

  describe('getMapBaseLayers', () => {
    it('should return a list of baseLayer objects for the map', () => {
      expect(
        mapSelectors.getMapBaseLayers(mockStoreMapWithLayers, mapId),
      ).toEqual([
        {
          mapId,
          name: 'arcGisSat',
          title: 'arcGisSat',
          type: 'twms',
          id: layerId2,
          layerType: LayerType.baseLayer,
          opacity: 1,
          enabled: true,
        },
      ]);
    });
    it('should return an empty list when the map has no baselayers', () => {
      expect(
        mapSelectors.getMapBaseLayers(mockStoreDefaultMap, mapId),
      ).toHaveLength(0);
    });
  });

  describe('getMapOverLayersIds', () => {
    it('should return a list of overlayer ids for the map', () => {
      const result = mapSelectors.getMapOverLayersIds(
        mockStoreMapWithLayers,
        mapId,
      );
      expect(result).toHaveLength(1);
      expect(result).toEqual(
        mockStoreMapWithLayers.webmap.byId[mapId].overLayers,
      );
    });
    it('should return an empty list when store does not exist', () => {
      const result = mapSelectors.getMapOverLayersIds(null, mapId);
      expect(result).toHaveLength(0);
    });
  });

  describe('getMapOverLayers', () => {
    it('should return a list of overLayer objects for the map', () => {
      expect(
        mapSelectors.getMapOverLayers(mockStoreMapWithLayers, mapId),
      ).toEqual([
        {
          mapId,
          name: 'someOverLayer',
          title: 'someOverLayer',
          type: 'twms',
          id: layerId3,
          opacity: 1,
          enabled: true,
          layerType: LayerType.overLayer,
        },
      ]);
    });
    it('should return an empty list when the map has no overLayers', () => {
      expect(
        mapSelectors.getMapOverLayers(mockStoreDefaultMap, mapId),
      ).toHaveLength(0);
    });
  });

  describe('getMapDimensions', () => {
    const mockStoreMapWithDimensions = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            dimensions: [
              {
                name: 'time',
                units: 'ISO8601',
                currentValue: '2020-03-13T13:30:00Z',
              },
            ],
            isAnimating: true,
          },
        },
        allIds: [mapId],
      },
    };

    it('should return the dimensions for the map', () => {
      expect(
        mapSelectors.getMapDimensions(mockStoreMapWithDimensions, mapId),
      ).toEqual(mockStoreMapWithDimensions.webmap.byId[mapId].dimensions);
    });
    it('should return an empty list when store does not exist', () => {
      expect(mapSelectors.getMapDimensions(null, mapId)).toHaveLength(0);
    });
  });

  describe('getMapDimension', () => {
    const mockStoreMapWithDimensions = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            dimensions: [
              {
                name: 'time',
                units: 'ISO8601',
                currentValue: '2020-03-13T13:30:00Z',
              },
            ],
            isAnimating: true,
          },
        },
        allIds: [mapId],
      },
    };

    it('should return the requested dimension for the map', () => {
      expect(
        mapSelectors.getMapDimension(mockStoreMapWithDimensions, mapId, 'time'),
      ).toEqual(mockStoreMapWithDimensions.webmap.byId[mapId].dimensions[0]);
    });
    it('should return undefined when store does not exist', () => {
      expect(
        mapSelectors.getMapDimension(null, mapId, 'dimname-does-not-exist'),
      ).toEqual(undefined);
    });
    it('should return undefined when dimension does not exist for the map', () => {
      expect(
        mapSelectors.getMapDimension(
          mockStoreMapWithDimensions,
          mapId,
          'elevation',
        ),
      ).toEqual(undefined);
    });
  });

  describe('getSrs', () => {
    it('should return the Srs for the map', () => {
      expect(mapSelectors.getSrs(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId].srs,
      );
    });
    it('should return an empty string when store does not exist', () => {
      expect(mapSelectors.getSrs(null, mapId)).toEqual('');
    });
  });

  describe('getBbox', () => {
    it('should return the Bbox for the map', () => {
      expect(mapSelectors.getBbox(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId].bbox,
      );
    });
    it('should return an empty object when store does not exist', () => {
      expect(mapSelectors.getBbox(null, mapId)).toMatchObject({});
    });
  });

  describe('isAnimating', () => {
    it('should return isAnimating for the map', () => {
      expect(mapSelectors.isAnimating(mockStoreDefaultMap, mapId)).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId].isAnimating,
      );
    });
    it('should return false when store does not exist', () => {
      expect(mapSelectors.isAnimating(null, mapId)).toEqual(false);
    });
  });

  describe('getAnimationStartTime', () => {
    it('should return map animation start time', () => {
      const mockStoreMapAnimationTimes = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              animationStartTime: moment.utc().subtract(6, 'h').toISOString(),
              animationEndTime: moment.utc().subtract(10, 'm').toISOString(),
            },
          },
          allIds: [mapId],
        },
      };
      expect(
        mapSelectors.getAnimationStartTime(mockStoreMapAnimationTimes, mapId),
      ).toBe(mockStoreMapAnimationTimes.webmap.byId[mapId].animationStartTime);
    });
    it('should return utc time - 6 hours when map does not exist', () => {
      expect(mapSelectors.getAnimationStartTime(null, mapId)).toBe(
        moment.utc().subtract(6, 'h').format(dateFormat),
      );
    });
  });

  describe('getAnimationEndTime', () => {
    it('should return map animation end time', () => {
      const mockStoreMapAnimationTimes = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              animationStartTime: moment.utc().subtract(6, 'h').toISOString(),
              animationEndTime: moment.utc().subtract(10, 'm').toISOString(),
            },
          },
          allIds: [mapId],
        },
      };
      expect(
        mapSelectors.getAnimationEndTime(mockStoreMapAnimationTimes, mapId),
      ).toBe(mockStoreMapAnimationTimes.webmap.byId[mapId].animationEndTime);
    });
    it('should return utc time - 10 minutes when map does not exist', () => {
      expect(mapSelectors.getAnimationEndTime(null, mapId)).toBe(
        moment.utc().subtract(10, 'm').format(dateFormat),
      );
    });
  });

  describe('isAutoUpdating', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isAutoUpdating: true },
        },
        allIds: [mapId],
      },
    };

    it('should return map is auto updating', () => {
      expect(
        mapSelectors.isAutoUpdating(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].isAutoUpdating);
    });
    it('should return an false when map does not exist', () => {
      expect(mapSelectors.isAutoUpdating(null, mapId)).toBeFalsy();
    });
  });

  describe('getActiveLayerId', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, activeLayerId: layerId2 },
        },
        allIds: [mapId],
      },
    };

    it('should return the active layer id for the map', () => {
      expect(
        mapSelectors.getActiveLayerId(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].activeLayerId);
    });
    it('should return an empty string when store does not exist', () => {
      expect(mapSelectors.getActiveLayerId(null, mapId)).toEqual('');
    });
  });

  describe('getMaptimeSliderScale', () => {
    const mockStoreMapTimeSliderScale = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, timeSliderScale: Scale.Hour },
        },
        allIds: [mapId],
      },
    };

    it('should return map time step', () => {
      expect(
        mapSelectors.getMapTimeSliderScale(mockStoreMapTimeSliderScale, mapId),
      ).toEqual(mockStoreMapTimeSliderScale.webmap.byId[mapId].timeSliderScale);
    });
    it('should return Scale.Hour when map does not exist', () => {
      expect(mapSelectors.getMapTimeSliderScale(null, mapId)).toEqual(
        Scale.Hour,
      );
    });
  });

  describe('getMaptimeStep', () => {
    const mockStoreMapTimeStep = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, timeStep: 10 },
        },
        allIds: [mapId],
      },
    };

    it('should return map time step', () => {
      expect(mapSelectors.getMapTimeStep(mockStoreMapTimeStep, mapId)).toEqual(
        mockStoreMapTimeStep.webmap.byId[mapId].timeStep,
      );
    });
    it('should return 5 when map does not exist', () => {
      expect(mapSelectors.getMapTimeStep(null, mapId)).toEqual(5);
    });
  });

  describe('getMapTimeSliderCenterTime', () => {
    const time = moment().unix();
    const storeWithCurrentTime = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            timeSliderCenterTime: time,
          },
        },
        allIds: [mapId],
      },
    };
    it('should return the current map time slider center time', () => {
      expect(
        mapSelectors.getMapTimeSliderCenterTime(storeWithCurrentTime, mapId),
      ).toEqual(time);
    });

    it('should return the current time when store does not exist', () => {
      const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
      jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
      expect(mapSelectors.getMapTimeSliderCenterTime(null, mapId)).toEqual(
        moment(now).unix(),
      );
    });
  });

  describe('getMapTimeSliderSecondsPerPx', () => {
    it('should return the map time slider secondsPerPx value', () => {
      expect(
        mapSelectors.getMapTimeSliderSecondsPerPx(mockStoreDefaultMap, mapId),
      ).toEqual(mockStoreDefaultMap.webmap.byId[mapId].timeSliderSecondsPerPx);
    });

    it('should return 60 when map does not exist', () => {
      expect(mapSelectors.getMapTimeSliderSecondsPerPx(null, mapId)).toEqual(
        60,
      );
    });
  });

  describe('getMapTimeSliderDataScaleToSecondsPerPx', () => {
    it('should return the map time slider DataScaleToSecondsPerPx value', () => {
      expect(
        mapSelectors.getMapTimeSliderDataScaleToSecondsPerPx(
          mockStoreDefaultMap,
          mapId,
        ),
      ).toEqual(
        mockStoreDefaultMap.webmap.byId[mapId]
          .timeSliderDataScaleToSecondsPerPx,
      );
    });

    it('should return defaultDataScaleToSecondsPerPx when map does not exist', () => {
      expect(
        mapSelectors.getMapTimeSliderDataScaleToSecondsPerPx(null, mapId),
      ).toEqual(defaultDataScaleToSecondsPerPx);
    });
  });

  describe('isTimestepAuto', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isTimestepAuto: true },
        },
        allIds: [mapId],
      },
    };
    it('should return map is timestep auto mode', () => {
      expect(
        mapSelectors.isTimestepAuto(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].isTimestepAuto);
    });
    it('should return an false when map does not exist', () => {
      expect(mapSelectors.isTimestepAuto(null, mapId)).toBeFalsy();
    });
  });

  describe('use time slider hover', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isTimeSliderHoverOn: true },
        },
        allIds: [mapId],
      },
    };

    it('should return is map using time slider hover', () => {
      expect(
        mapSelectors.isTimeSliderHoverOn(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].isTimeSliderHoverOn);
    });
    it('should return an false when map does not exist', () => {
      expect(mapSelectors.isTimeSliderHoverOn(null, mapId)).toBeFalsy();
    });
  });

  describe('isZoomControlsVisible', () => {
    const mockStore = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, shouldShowZoomControls: true },
        },
        allIds: [mapId],
      },
    };

    it('should return if zoom controls on map are visible', () => {
      expect(mapSelectors.isZoomControlsVisible(mockStore, mapId)).toEqual(
        mockStore.webmap.byId[mapId].shouldShowZoomControls,
      );
    });
    it('should return an true when map does not exist', () => {
      expect(mapSelectors.isZoomControlsVisible(null, mapId)).toBeTruthy();
    });
  });

  describe('isTimeSliderVisible', () => {
    const mockStoreMapActiveLayer = {
      webmap: {
        byId: {
          [mapId]: { ...mockMap, isTimeSliderVisible: false },
        },
        allIds: [mapId],
      },
    };

    it('should return if timeslider on map is visible', () => {
      expect(
        mapSelectors.isTimeSliderVisible(mockStoreMapActiveLayer, mapId),
      ).toEqual(mockStoreMapActiveLayer.webmap.byId[mapId].isTimeSliderVisible);
    });
    it('should return an true when map does not exist', () => {
      expect(mapSelectors.isTimeSliderVisible(null, mapId)).toBeTruthy();
    });
  });

  describe('getIsLayerActiveLayer', () => {
    it('should return true when layer is active layer', () => {
      expect(
        mapSelectors.getIsLayerActiveLayer(
          mockStoreMapWithLayers,
          mapId,
          layerId1,
        ),
      ).toBeTruthy();
      expect(
        mapSelectors.getIsLayerActiveLayer(
          mockStoreMapWithLayers,
          mapId,
          layerId2,
        ),
      ).toBeFalsy();
      expect(
        mapSelectors.getIsLayerActiveLayer(
          mockStoreMapWithLayers,
          mapId,
          layerId3,
        ),
      ).toBeFalsy();
    });
    it('should return false when not providing mapId or layerId', () => {
      expect(mapSelectors.getIsLayerActiveLayer(null, null, null)).toBeFalsy();
    });
  });

  describe('getMapIdFromLayerId', () => {
    it('should return mapId for given layerId', () => {
      expect(
        mapSelectors.getMapIdFromLayerId(mockStoreMapWithLayers, layerId1),
      ).toBe(mapId);
      expect(
        mapSelectors.getMapIdFromLayerId(
          mockStoreMapWithLayers,
          'non-existing-layer-id',
        ),
      ).toBe(null);
    });
  });

  describe('getLayerIdByLayerName', () => {
    it('should return layerId for given layerName', () => {
      expect(
        mapSelectors.getLayerIdByLayerName(
          mockStoreMapWithLayers,
          mapId,
          'RAD_NL25_PCP_CM',
        ),
      ).toBe(layerId1);
      expect(
        mapSelectors.getLayerIdByLayerName(
          mockStoreMapWithLayers,
          mapId,
          'non-existing-layer-name',
        ),
      ).toBe(null);
      expect(
        mapSelectors.getLayerIdByLayerName(
          mockStoreMapWithLayers,
          'non-existing-mapid',
          'RAD_NL25_PCP_CM',
        ),
      ).toBe(null);
    });
  });

  describe('getLayerIndexByLayerId', () => {
    it('should return layeIndex for given layerId', () => {
      expect(
        mapSelectors.getLayerIndexByLayerId(
          mockStoreMapWithLayers,
          mapId,
          layerId1,
        ),
      ).toBe(0);
      expect(
        mapSelectors.getLayerIndexByLayerId(
          mockStoreMapWithLayers,
          mapId,
          'non-existing-layer-id',
        ),
      ).toBe(-1);

      expect(
        mapSelectors.getLayerIndexByLayerId(
          mockStoreMapWithLayers,
          'non-existing-mapid',
          'non-existing-layer-id',
        ),
      ).toBe(-1);
    });
  });

  describe('getLayerByLayerIndex', () => {
    it('should return the Layer in the map for given layerIndex', () => {
      const firstLayer = mapSelectors.getMapLayers(
        mockStoreMapWithLayers,
        mapId,
      )[0];
      expect(
        mapSelectors.getLayerByLayerIndex(mockStoreMapWithLayers, mapId, 0),
      ).toEqual(firstLayer);
      expect(
        mapSelectors.getLayerByLayerIndex(mockStoreMapWithLayers, mapId, 10),
      ).toEqual(null);
      expect(
        mapSelectors.getLayerByLayerIndex(
          mockStoreMapWithLayers,
          'non-existing-mapid',
          10,
        ),
      ).toEqual(null);
    });
  });

  describe('getAllUniqueDimensions', () => {
    const mapId2 = 'map-2';
    const mockMap2 = createMap({ id: mapId2 });
    const mockStoreMapWithDimensions = {
      webmap: {
        byId: {
          [mapId]: {
            ...mockMap,
            dimensions: [
              {
                name: 'time',
                currentValue: '2021-11-11T12:50:00Z',
              },
              {
                name: 'reference_time',
                currentValue: '2021-11-11T09:00:00Z',
              },
              {
                name: 'elevation',
                currentValue: '850',
              },
            ],
          },
          [mapId2]: {
            ...mockMap2,
            dimensions: [
              {
                name: 'time',
                currentValue: '2021-11-11T12:50:00Z',
              },
              {
                name: 'reference_time',
                currentValue: '2021-11-11T09:00:00Z',
              },
              {
                name: 'ensemble_member',
                currentValue: '0',
              },
            ],
          },
        },
        allIds: [mapId, mapId2],
      },
    };
    it('should return all unique dimension names across all maps or an empty array if no store', () => {
      expect(
        mapSelectors.getAllUniqueDimensions(mockStoreMapWithDimensions),
      ).toEqual(['time', 'reference_time', 'elevation', 'ensemble_member']);
      expect(mapSelectors.getAllUniqueDimensions(null)).toEqual([]);
    });
  });

  describe('getPinLocation', () => {
    it('should return pin location for map', () => {
      expect(
        mapSelectors.getPinLocation(mockStoreDefaultWithPinLocation, mapId),
      ).toEqual({ lat: 52.0, lon: 5.0 });
    });

    it('no store', () => {
      expect(mapSelectors.getPinLocation(null, mapId)).toBeUndefined();
    });

    it('map pin not set', () => {
      const storeWithoutMapPin = produce(mockStoreDefaultMap, (draftState) => {
        // eslint-disable-next-line no-param-reassign
        draftState.webmap.byId[mapId].mapPinLocation = undefined;
      });
      expect(mapSelectors.getPinLocation(storeWithoutMapPin, mapId)).toEqual(
        undefined,
      );
    });
  });

  describe('getDisableMapPin', () => {
    it('no store', () => {
      expect(mapSelectors.getDisableMapPin(null, mapId)).toEqual(false);
    });

    it('map pin not set', () => {
      expect(mapSelectors.getDisableMapPin(mockStoreDefaultMap, mapId)).toEqual(
        false,
      );
    });

    it('disabled map pin set to true', () => {
      expect(
        mapSelectors.getDisableMapPin(mockStoreDefaultWithPinLocation, mapId),
      ).toEqual(true);
    });
  });

  describe('getDisplayMapPin', () => {
    it('no store', () => {
      expect(mapSelectors.getDisplayMapPin(null, mapId)).toBeFalsy();
    });

    it('map pin not set', () => {
      expect(
        mapSelectors.getDisplayMapPin(mockStoreDefaultMap, mapId),
      ).toBeFalsy();
    });

    it('disabled map pin set to true', () => {
      expect(
        mapSelectors.getDisplayMapPin(mockStoreDefaultWithPinLocation, mapId),
      ).toEqual(true);
    });
  });

  describe('getSelectedFeatureIndex', () => {
    it('should return undefined if no store', () => {
      expect(mapSelectors.getSelectedFeatureIndex(null, mapId)).toBeUndefined();
    });

    it('should return undefined if map does not exist', () => {
      expect(
        mapSelectors.getSelectedFeatureIndex(
          mockStoreDefaultMap,
          'nonExistingMapId',
        ),
      ).toEqual(undefined);
    });

    it('should return selected feature index', () => {
      const mockStoreWithFeature = produce(mockStoreDefaultMap, (draft) => {
        draft.webmap.byId[mapId].selectedFeatureIndex = 1;
      });
      expect(
        mapSelectors.getSelectedFeatureIndex(mockStoreWithFeature, mapId),
      ).toEqual(1);
    });
  });
  describe('getActiveMapPresetId', () => {
    it('no store', () => {
      expect(mapSelectors.getActiveMapPresetId(null, mapId)).toBeUndefined();
    });

    it('active preset has been set', () => {
      const activeMapPresetId = 'test-preset-1';
      const mockStoreWithActivePreset = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              activeMapPresetId,
            },
          },
          allIds: [mapId],
        },
      };

      expect(
        mapSelectors.getActiveMapPresetId(mockStoreWithActivePreset, mapId),
      ).toEqual(activeMapPresetId);
    });

    it('active preset has not been set', () => {
      expect(
        mapSelectors.getActiveMapPresetId(mockStoreDefaultMap, mapId),
      ).toBeNull();
    });
  });

  describe('getLegendId', () => {
    it('no store', () => {
      expect(mapSelectors.getLegendId(null, mapId)).toBeUndefined();
    });

    it('legend has been set', () => {
      const legendId = 'test-legend-1';
      const mockStoreWithActivePreset = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              legendId,
            },
          },
          allIds: [mapId],
        },
      };

      expect(
        mapSelectors.getLegendId(mockStoreWithActivePreset, mapId),
      ).toEqual(legendId);
    });

    it('legend has not been set', () => {
      expect(
        mapSelectors.getLegendId(mockStoreDefaultMap, mapId),
      ).toBeUndefined();
    });
  });

  describe('getMapPreset', () => {
    it('no store', () => {
      expect(mapSelectors.getMapPreset(null, mapId)).toEqual({
        layers: [],
        proj: {
          bbox: {},
          srs: '',
        },
        activeLayerId: '',
        shouldAnimate: false,
        shouldAutoUpdate: false,
        showTimeSlider: true,
        displayMapPin: false,
        shouldShowZoomControls: true,
        animationPayload: {
          interval: defaultTimeStep,
          speed: getSpeedFactor(defaultAnimationDelayAtStart),
        },
        toggleTimestepAuto: false,
      });
    });

    it('mapPreset has been set', () => {
      const expectedLayersResult = [
        mockStoreMapWithLayers.layers.byId[layerId2],
        mockStoreMapWithLayers.layers.byId[layerId3],
        mockStoreMapWithLayers.layers.byId[layerId1],
      ].map(({ mapId, ...layer }) => layer);
      expect(mapSelectors.getMapPreset(mockStoreMapWithLayers, mapId)).toEqual({
        layers: expectedLayersResult,
        proj: {
          bbox: mockStoreMapWithLayers.webmap.byId[mapId].bbox,
          srs: mockStoreMapWithLayers.webmap.byId[mapId].srs,
        },
        activeLayerId: mockStoreMapWithLayers.webmap.byId[mapId].activeLayerId,
        shouldAnimate: false,
        shouldAutoUpdate: false,
        showTimeSlider: true,
        displayMapPin: false,
        shouldShowZoomControls: true,
        animationPayload: {
          interval: defaultTimeStep,
          speed: getSpeedFactor(defaultAnimationDelayAtStart),
        },
        toggleTimestepAuto: true,
      });
    });

    it('mapPreset has been set with custom vars', () => {
      const legendId = 'legend-1';
      const mockStoreWithAnimateAutoUpdate = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              isAnimating: true,
              isAutoUpdating: true,
              displayMapPin: true,
              shouldShowZoomControls: false,
              timeStep: 1234,
              animationDelay: 250,
              isTimestepAuto: false,
              legendId,
            },
          },
          allIds: [mapId],
        },
        ui: {
          dialogs: {
            [legendId]: {
              isOpen: false,
              type: legendId,
              activeMapId: mapId,
            },
          },
          order: [legendId],
          activeWindowId: 'test1',
        },
      };
      const result = mapSelectors.getMapPreset(
        mockStoreWithAnimateAutoUpdate,
        mapId,
      );
      expect(result.shouldAnimate).toEqual(
        mockStoreWithAnimateAutoUpdate.webmap.byId[mapId].isAnimating,
      );
      expect(result.shouldAutoUpdate).toEqual(
        mockStoreWithAnimateAutoUpdate.webmap.byId[mapId].isAutoUpdating,
      );
      expect(result.displayMapPin).toBeTruthy();
      expect(result.shouldShowZoomControls).toEqual(
        mockStoreWithAnimateAutoUpdate.webmap.byId[mapId]
          .shouldShowZoomControls,
      );
      expect(result.animationPayload.interval).toEqual(
        mockStoreWithAnimateAutoUpdate.webmap.byId[mapId].timeStep,
      );
      expect(result.animationPayload.speed).toEqual(
        getSpeedFactor(
          mockStoreWithAnimateAutoUpdate.webmap.byId[mapId].animationDelay,
        ),
      );
      expect(result.toggleTimestepAuto).toEqual(
        mockStoreWithAnimateAutoUpdate.webmap.byId[mapId].isTimestepAuto,
      );
      expect(result.shouldShowLegend).toBeFalsy();
    });

    it('should remove time and reference time dimension from layers but leave other dimensions', () => {
      const timeDims = [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2022-07-28T06:00:00Z',
          minValue: '2022-07-26T06:00:00Z',
          maxValue: '2022-07-28T06:00:00Z',
          synced: false,
        },
        {
          name: 'reference_time',
          units: 'ISO8601',
          currentValue: '2022-07-26T06:00:00Z',
          minValue: '2022-07-19T00:00:00Z',
          maxValue: '2022-07-26T06:00:00Z',
          timeInterval: null,
          synced: false,
        },
      ];
      const layer1 = {
        mapId,
        service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
        name: 'RAD_NL25_PCP_CM',
        format: 'image/png',
        style: 'knmiradar/nearest',
        id: layerId1,
        opacity: 1,
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'elevation',
            units: 'hPa',
            currentValue: '200',
            minValue: '200',
            maxValue: '1000',
            timeInterval: null,
            synced: false,
          },
        ],
      };
      const mockStoreMapWithLayersWithDim = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              mapLayers: [layerId1],
              baseLayers: [layerId2],
              overLayers: [layerId3],
              activeLayerId: layerId1,
            },
          },
          allIds: [mapId],
        },
        layers: {
          byId: {
            [layerId1]: {
              ...layer1,
              dimensions: [...layer1.dimensions, ...timeDims],
            },
            [layerId2]: {
              mapId,
              name: 'arcGisSat',
              title: 'arcGisSat',
              type: 'twms',
              id: layerId2,
              opacity: 1,
              enabled: true,
              layerType: LayerType.baseLayer,
            },
            [layerId3]: {
              mapId,
              name: 'someOverLayer',
              title: 'someOverLayer',
              type: 'twms',
              id: layerId3,
              opacity: 1,
              enabled: true,
              layerType: LayerType.overLayer,
            },
          },
          allIds: [layerId1, layerId2, layerId3],
          availableBaseLayers: { allIds: [], byId: {} },
        },
      };
      const expectedLayersResult = [
        mockStoreMapWithLayersWithDim.layers.byId[layerId2],
        mockStoreMapWithLayersWithDim.layers.byId[layerId3],
        layer1,
      ].map(({ mapId, ...layer }) => layer);
      expect(
        mapSelectors.getMapPreset(mockStoreMapWithLayersWithDim, mapId),
      ).toEqual({
        layers: expectedLayersResult,
        proj: {
          bbox: mockStoreMapWithLayersWithDim.webmap.byId[mapId].bbox,
          srs: mockStoreMapWithLayersWithDim.webmap.byId[mapId].srs,
        },
        activeLayerId:
          mockStoreMapWithLayersWithDim.webmap.byId[mapId].activeLayerId,
        shouldAnimate: false,
        shouldAutoUpdate: false,
        showTimeSlider: true,
        displayMapPin: false,
        shouldShowZoomControls: true,
        animationPayload: {
          interval: defaultTimeStep,
          speed: getSpeedFactor(defaultAnimationDelayAtStart),
        },
        toggleTimestepAuto: true,
      });
    });
    it('mapPreset has not been set', () => {
      expect(mapSelectors.getMapPreset(mockStoreDefaultMap, mapId)).toEqual({
        layers: [],
        proj: {
          bbox: mockStoreDefaultMap.webmap.byId[mapId].bbox,
          srs: mockStoreDefaultMap.webmap.byId[mapId].srs,
        },
        activeLayerId: '',
        shouldAnimate: false,
        shouldAutoUpdate: false,
        showTimeSlider: true,
        displayMapPin: false,
        shouldShowZoomControls: true,
        animationPayload: {
          interval: defaultTimeStep,
          speed: getSpeedFactor(defaultAnimationDelayAtStart),
        },
        toggleTimestepAuto: true,
      });
    });

    it('should not return map id of layers', () => {
      const layer1 = {
        mapId,
        service: 'https://geoservices.knmi.nl/adagucserver?dataset=RADAR&',
        name: 'RAD_NL25_PCP_CM',
        format: 'image/png',
        style: 'knmiradar/nearest',
        id: layerId1,
        opacity: 1,
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'elevation',
            units: 'hPa',
            currentValue: '200',
            minValue: '200',
            maxValue: '1000',
            timeInterval: null,
            synced: false,
          },
        ],
      };
      const mockStoreWithMapIdOnLayer = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              mapLayers: [layerId1],
              baseLayers: [layerId2],
              overLayers: [layerId3],
              activeLayerId: layerId1,
            },
          },
          allIds: [mapId],
        },
        layers: {
          byId: {
            [layerId1]: {
              ...layer1,
            },
            [layerId2]: {
              mapId,
              name: 'arcGisSat',
              title: 'arcGisSat',
              type: 'twms',
              id: layerId2,
              opacity: 1,
              enabled: true,
              layerType: LayerType.baseLayer,
            },
            [layerId3]: {
              mapId,
              name: 'someOverLayer',
              title: 'someOverLayer',
              type: 'twms',
              id: layerId3,
              opacity: 1,
              enabled: true,
              layerType: LayerType.overLayer,
            },
          },
          allIds: [layerId1, layerId2, layerId3],
          availableBaseLayers: { allIds: [], byId: {} },
        },
      };
      const result = mapSelectors.getMapPreset(
        mockStoreWithMapIdOnLayer,
        mapId,
      );

      expect(result.layers);

      result.layers.forEach((layer) => expect(layer.mapId).toBeUndefined());
    });
  });

  describe('getIsMapPresetLoading', () => {
    it('should return undefined if no store', () => {
      expect(mapSelectors.getActiveMapPresetId(null, mapId)).toBeUndefined();
    });

    it('should return isMapPresetLoading', () => {
      const mockStoreWithPresetLoading = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              isMapPresetLoading: true,
            },
          },
          allIds: [mapId],
        },
      };

      expect(
        mapSelectors.getIsMapPresetLoading(mockStoreWithPresetLoading, mapId),
      ).toEqual(true);
    });

    it('should return false if isMapPresetLoading is not available', () => {
      expect(
        mapSelectors.getIsMapPresetLoading(mockStoreDefaultMap, mapId),
      ).toEqual(false);
    });
  });

  describe('getHasMapPresetChanges', () => {
    it('should return false if no store', () => {
      expect(mapSelectors.getHasMapPresetChanges(null, mapId)).toBeFalsy();
    });

    it('should return getHasMapPresetChanges', () => {
      const mockStoreWithPresetLoading = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              hasMapPresetChanges: true,
            },
          },
          allIds: [mapId],
        },
      };

      expect(
        mapSelectors.getHasMapPresetChanges(mockStoreWithPresetLoading, mapId),
      ).toEqual(true);
    });

    it('should return false if getHasMapPresetChanges is not available', () => {
      expect(
        mapSelectors.getHasMapPresetChanges(mockStoreDefaultMap, mapId),
      ).toEqual(false);
    });
  });

  describe('getMapPresetError', () => {
    it('should return undefined if no store', () => {
      expect(mapSelectors.getActiveMapPresetId(null, mapId)).toBeUndefined();
    });

    it('should return mapPresetError', () => {
      const mockStoreWithError = {
        webmap: {
          byId: {
            [mapId]: {
              ...mockMap,
              mapPresetError: 'test error',
            },
          },
          allIds: [mapId],
        },
      };

      expect(mapSelectors.getMapPresetError(mockStoreWithError, mapId)).toEqual(
        'test error',
      );
    });

    it('should return undefined if mapPresetError is not available', () => {
      expect(
        mapSelectors.getMapPresetError(mockStoreDefaultMap, mapId),
      ).toEqual(undefined);
    });
  });
});
