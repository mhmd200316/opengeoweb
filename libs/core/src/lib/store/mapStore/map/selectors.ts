/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from '@reduxjs/toolkit';

import moment from 'moment';
import { Bbox, MapPreset, Scale, WebMap, WebMapState } from './types';
import { AppStore } from '../../../types/types';
import { layerSelectors } from '../layers';
import { uiSelectors } from '../../ui';
import { genericSelectors } from '../../generic';
import {
  defaultDataScaleToSecondsPerPx,
  defaultAnimationDelayAtStart,
  defaultTimeStep,
  getSpeedFactor,
} from '../../../components/TimeSlider/TimeSliderUtils';
import { Layer } from '../types';
import { findMapIdFromLayerId } from './utils';
import { dateFormat } from '../utils/helpers';
import { selectorMemoizationOptions } from '../../utils';
import { UIStoreType } from '../../ui/types';

/**
 * Gets the map state by mapId
 *
 * Example: getMapById(store, 'mapid_1')
 * @param {object} store store object from which the map state wll be extracted
 * @param {string} mapId Id of the map
 * @returns {object} object containing map state (isAnimating, bbox, baseLayers, layers etc.)
 */
export const getMapById = (store: AppStore, mapId: string): WebMap => {
  if (store && store.webmap && store.webmap.byId[mapId]) {
    return store.webmap.byId[mapId];
  }
  return null;
};

/**
 * Gets all mapIds
 *
 * Example: getAllMapIds(store)
 * @param {object} store store object from which the map state wll be extracted
 * @returns {array} array containing all map ids
 */
export const getAllMapIds = (store: AppStore): string[] => {
  if (store && store.webmap && store.webmap.allIds) {
    return store.webmap.allIds;
  }
  return [];
};

/**
 * Gets the map state of the first map in the store
 *
 * Example: getFirstMap(store)
 * @param {object} store store object from which the map state wll be extracted
 * @returns {object} object containing map state (isAnimating, bbox, baseLayers, layers etc.)
 */
export const getFirstMap = createSelector(
  getAllMapIds,
  (store) =>
    store && store.webmap && store.webmap.byId ? store.webmap.byId : {},
  (allMapIds, mapsById) =>
    allMapIds[0] && mapsById[allMapIds[0]] ? mapsById[allMapIds[0]] : null,
  selectorMemoizationOptions,
);

/**
 * Gets the id of first map in store
 *
 * Example: getFirstMapId(store)
 * @param {object} store store: object from which the map state wll be extracted
 * @returns {string} returnType:string - map id
 */
export const getFirstMapId = createSelector(getFirstMap, (map) =>
  map ? map.id : '',
);

/**
 * Determines if map is present
 *
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType:boolean - true if map is present
 */
export const getIsMapPresent = createSelector(
  (store: AppStore, mapId: string): boolean => {
    if (store && store.webmap && store.webmap.byId[mapId]) {
      return true;
    }
    return false;
  },
  (isPresent) => isPresent,
);

/**
 * Gets all layerIds for a map that aren't baselayers or overlayers
 *
 * Example: layerIds = getLayerIds(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing all layerIds
 */
export const getLayerIds = createSelector(
  getMapById,
  (store) => (store ? store.mapLayers : []),
  selectorMemoizationOptions,
);

/**
 * Gets all layer states for a map
 *
 * Example: layers = getMapLayers(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing all layer states for the map
 */
export const getMapLayers = createSelector(
  getLayerIds,
  layerSelectors.getLayersById,
  (layerIds, layers) => layerIds.map((layerId) => layers[layerId]),
  selectorMemoizationOptions,
);

/**
 * Gets an array of baselayers ids for a map
 *
 * Example: baseLayersId = getMapBaseLayersIds(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing an array of baselayers ids
 */
export const getMapBaseLayersIds = createSelector(
  getMapById,
  (store) => (store ? store.baseLayers : []),
  selectorMemoizationOptions,
);

/**
 * Gets all baselayers for a map
 *
 * Example: baseLayers = getMapBaseLayers(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing all baselayers for the map
 */
export const getMapBaseLayers = createSelector(
  getMapBaseLayersIds,
  layerSelectors.getLayersById,
  (layerIds, layers) => layerIds.map((layerId) => layers[layerId]),
  selectorMemoizationOptions,
);

/**
 * Gets and array of overLayers ids for a map
 *
 * Example: overLayersId = getMapOverLayersIds(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing an array of overLayers ids
 */
export const getMapOverLayersIds = createSelector(
  getMapById,
  (store) => (store ? store.overLayers : []),
  selectorMemoizationOptions,
);

/**
 * Gets all overLayers for a map
 *
 * Example: overLayers = getMapOverLayers(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array -  array containing all overLayers for the map
 */
export const getMapOverLayers = createSelector(
  getMapOverLayersIds,
  layerSelectors.getLayersById,
  (layerIds, layers) => layerIds.map((layerId) => layers[layerId]),
  selectorMemoizationOptions,
);

/**
 * Gets map dimensions
 *
 * Example: mapDimensions = getMapDimensions(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {array} returnType: array - array containing map dimensions
 */
export const getMapDimensions = createSelector(
  getMapById,
  (store) => (store ? store.dimensions : []),
  selectorMemoizationOptions,
);

/**
 * Gets the map dimension requested
 *
 * Example: mapDimensions = getMapDimension(store, 'mapid_1', 'elevation')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @param {string} dimensionName dimensionName: string - name of the dimension
 * @returns {object} returnType: object - object containing the map dimension details
 */
export const getMapDimension = createSelector(
  getMapById,
  (_store, _mapId, dimensionName): string => dimensionName,
  (store, dimensionName) => {
    if (store && store.dimensions) {
      return store.dimensions.find((dim) => dim.name === dimensionName);
    }
    return undefined;
  },
  selectorMemoizationOptions,
);

/**
 * Gets map srs
 *
 * Example: mapSrs = getSrs(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {string} returnType: string -  string containing srs
 */
export const getSrs = createSelector(getMapById, (store) =>
  store ? store.srs : '',
);

/**
 * Gets map bounding box
 *
 * Example: mapBbox = getBbox(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {object} returnType: object -  boundingbox object {left: number, bottom: number, right:number, top: number}
 */
export const getBbox = createSelector(
  getMapById,
  (store) => (store ? store.bbox : ({} as Bbox)),
  selectorMemoizationOptions,
);

/**
 * Gets if map is animating
 *
 * Example: mapIsAnimating = isAnimating(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const isAnimating = createSelector(getMapById, (store) =>
  store ? store.isAnimating : false,
);

/**
 * Gets if any linked map is animating
 *
 * Example: linkedMapIsAnimating = linkedMapAnimationInfo(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {object} returnType: object - object containing isAnimating boolean and id string
 */
export const linkedMapAnimationInfo = createSelector(
  (store: AppStore, mapId: string) => {
    const animationInfo = {
      isAnimating: isAnimating(store, mapId),
      id: mapId,
    };
    genericSelectors
      .getSynchronizationGroupStore(store)
      .groups.allIds.forEach((groupId) => {
        if (
          genericSelectors.getSynchronizationGroupStore(store).groups.byId[
            groupId
          ].targets.byId[animationInfo.id] &&
          genericSelectors.getSynchronizationGroupStore(store).groups.byId[
            groupId
          ].targets.byId[animationInfo.id].linked
        ) {
          genericSelectors
            .getSynchronizationGroupStore(store)
            .groups.byId[groupId].targets.allIds.forEach((id) => {
              if (isAnimating(store, id) === true) {
                animationInfo.isAnimating = true;
                animationInfo.id = id;
              }
            });
        }
      });
    return animationInfo;
  },
  (animationInfo) => animationInfo,
);

/**
 * Gets start time of animation
 *
 * Example: endTimeOfAnimetion = getAnimationStartTime(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {string} returnType: string
 */
export const getAnimationStartTime = createSelector(getMapById, (store) =>
  store
    ? store.animationStartTime
    : moment.utc().subtract(6, 'h').format(dateFormat),
);

/**
 * Gets end time of animation
 *
 * Example: endTimeOfAnimation = getAnimationEndTime(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {string} returnType: string
 */
export const getAnimationEndTime = createSelector(getMapById, (store) =>
  store
    ? store.animationEndTime
    : moment.utc().subtract(10, 'm').format(dateFormat),
);

/**
 * Returns map is auto updating
 *
 * Example: isAutoUpdating = isAutoUpdating(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const isAutoUpdating = createSelector(getMapById, (store) =>
  store ? store.isAutoUpdating : false,
);

/**
 * Gets activeLayerId for map
 *
 * example: activeLayerId = getActiveLayerId(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 */
export const getActiveLayerId = createSelector(getMapById, (store) =>
  store ? store.activeLayerId : '',
);

/**
 * Gets scale of a time slider of a map
 *
 * Example: scale = getScale(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {Scale} returnType: Scale -  scale as an enum
 */
export const getMapTimeSliderScale = createSelector(getMapById, (store) =>
  store ? store.timeSliderScale : Scale.Hour,
);

/**
 * Gets time step of a map
 *
 * Example: timeStep = getTimeStep(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {number} returnType: number - time step as a number
 */
export const getMapTimeStep = createSelector(getMapById, (store) =>
  store ? store.timeStep : defaultTimeStep,
);

/**
 * Returns the speed of animation
 *
 * Example: speed = getSpeed(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {number} returnType: number - speed as a number
 */
export const getMapAnimationDelay = createSelector(getMapById, (store) =>
  store ? store.animationDelay : defaultAnimationDelayAtStart,
);

/**
 * Returns the center time of time slider
 *
 * Example: centerTime = getCenterTime(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {number} returnType: number - center time as a unix timestamp
 */
export const getMapTimeSliderCenterTime = createSelector(getMapById, (store) =>
  store ? store.timeSliderCenterTime : moment().unix(),
);

/**
 * Returns the number of seconds per pixel on the time slider
 *
 * Example: secondsPerPx = getSecondsPerPx(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {number} returnType: number - the number of seconds per pixel
 */
export const getMapTimeSliderSecondsPerPx = createSelector(
  getMapById,
  (store) => (store ? store.timeSliderSecondsPerPx : 60),
);

/**
 * Returns the number of scale to seconds pixel on the time slider
 *
 * Example: dataScaleToSecondsPerPx = getMapTimeSliderDataScaleToSecondsPerPx(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {number} returnType: number - the number of scaleto seconds pixel
 */
export const getMapTimeSliderDataScaleToSecondsPerPx = createSelector(
  getMapById,
  (store) =>
    store
      ? store.timeSliderDataScaleToSecondsPerPx
      : defaultDataScaleToSecondsPerPx,
);

/**
 * Returns map is timestep auto
 *
 * Example: isTimestepAuto = isTimestepAuto(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const isTimestepAuto = createSelector(getMapById, (store) =>
  store ? store.isTimestepAuto : false,
);

/**
 * Returns map is time slider hover
 *
 * Example: isTimeSliderHoverOn = isTimeSliderHoverOn(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const isTimeSliderHoverOn = createSelector(getMapById, (store) =>
  store ? store.isTimeSliderHoverOn : false,
);

/**
 * Returns map if zoomcontrols are visible
 *
 * Example: isZoomControlsVisible = isZoomControlsVisible(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const isZoomControlsVisible = createSelector(getMapById, (store) =>
  store ? store.shouldShowZoomControls : true,
);

/**
 * Returns map is time slider visible
 *
 * Example: isTimeSliderHoverOn = isTimeSliderVisible(store, 'mapid_1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const isTimeSliderVisible = createSelector(getMapById, (store) =>
  store ? store.isTimeSliderVisible : true,
);

/**
 * Returns is layer is active layer
 *
 * Example: isLayerActiveLayer = getIsLayerActiveLayer(store, 'mapid_1', 'layer_1)
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @param {string} mapId layerId: string - Id of the layer
 * @returns {boolean} returnType: boolean
 */
export const getIsLayerActiveLayer = createSelector(
  getActiveLayerId,
  (_store, _mapId, layerId) => layerId,
  (activeLayerId, layerId) => activeLayerId === layerId,
);

/**
 * Returns the mapId for given layerId
 *
 * Example const mapId = getMapIdFromLayerId(store 'layerId-A');
 * @param {object} store store: object - store object
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {string} returnType: string - the mapId, or null if not found
 */
export const getMapIdFromLayerId = createSelector(
  (store: AppStore): WebMapState =>
    store && store.webmap ? store.webmap : null,
  (_store: AppStore, layerId: string): string => layerId,
  findMapIdFromLayerId,
  selectorMemoizationOptions,
);

/**
 * Returns the layerId for given layerName
 *
 * Example const layerId = getLayerIdByLayerName(store, 'mapid-1', 'precipitation');
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {string} returnType: string - the layerId, or null if not found
 */
export const getLayerIdByLayerName = createSelector(
  getMapLayers,
  (_store, _mapId, layerName): string => layerName,
  (layers: Layer[], layerName: string): string => {
    const layer: Layer = layers.find((layer) => layer.name === layerName);
    if (layer) {
      return layer.id;
    }
    return null;
  },
  selectorMemoizationOptions,
);

/**
 * Returns the layerIndex in the map for given layerId
 *
 * Example const layerIndex = getLayerIndexByLayerId(store, 'mapid-1', 'precipitation');
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {number} returnType: index number or -1 if not found
 */
export const getLayerIndexByLayerId = createSelector(
  getMapLayers,
  (_store, _mapId, layerId): string => layerId,
  (layers: Layer[], layerId: string): number => {
    return layers.findIndex((layer) => layer.id === layerId);
  },
  selectorMemoizationOptions,
);

/**
 * Returns the Layer in the map for given layerIndex
 *
 * Example const layer = getLayerByLayerIndex(store, 'mapid-1', 0);
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @param {number} layerIndex layerId: number - Index of the layer in the map
 * @returns {object} returnType: layer, or null if not found
 */
export const getLayerByLayerIndex = createSelector(
  getMapLayers,
  (_store, _mapId, layerIndex): number => layerIndex,
  (layers: Layer[], layerIndex: number): Layer => {
    if (layerIndex >= 0 && layerIndex < layers.length) {
      return layers[layerIndex];
    }
    return null;
  },
  selectorMemoizationOptions,
);

/**
 * Returns name of all unique dimensions present in all maps
 *
 * Example getAllUniqueDimensions(store);
 * @param {object} store store: object - store object
 * @returns {array} returnType: array of dimension names
 */
export const getAllUniqueDimensions = createSelector(
  getAllMapIds,
  (store): AppStore => store,
  (mapIds: string[], store: AppStore): string[] => {
    return mapIds.reduce((list, mapId): string[] => {
      const mapDimensions = getMapDimensions(store, mapId);
      const uniqueMapDims = mapDimensions.reduce(
        (array, dimension): string[] => {
          if (
            dimension &&
            dimension.name &&
            array.indexOf(dimension.name) === -1
          ) {
            return array.concat(dimension.name);
          }
          return array;
        },
        list,
      );
      return uniqueMapDims;
    }, []);
  },
  selectorMemoizationOptions,
);

/**
 * Returns the mapPinLocation for the current map
 *
 * Example getPinLocation(store);
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {object} returnType: latitude and longitude of pin
 */
export const getPinLocation = createSelector(
  getMapById,
  (store) => (store ? store.mapPinLocation : undefined),
  selectorMemoizationOptions,
);

/**
 * Returns the disable map pin boolean for the current map
 *
 * Example getDisableMapPin(store);
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const getDisableMapPin = createSelector(getMapById, (store) =>
  store ? store.disableMapPin : false,
);

/**
 * Returns the diplay map pin boolean for the current map
 *
 * Example getDisplayMapPin(store);
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const getDisplayMapPin = createSelector(getMapById, (store) =>
  store ? store.displayMapPin : false,
);

/**
 * Returns the selected geojson feature for the given map
 *
 * Example const selectedFeature = getSelectedFeature(store, 'mapId1')
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {number} selectedFeatureIndex: the index of the selected geojson feature
 */
export const getSelectedFeatureIndex = createSelector(
  getMapById,
  (store) => store?.selectedFeatureIndex,
);
/**
 * Returns the active map preset id
 *
 * Example getActiveMapPresetId(store, mapId);
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean or undefined
 */
export const getActiveMapPresetId = createSelector(getMapById, (store) =>
  store ? store.activeMapPresetId : undefined,
);

/**
 * Returns the legend id
 *
 * Example getLegendId(store, mapId);
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: id or undefined
 */
export const getLegendId = createSelector(getMapById, (store) =>
  store ? store.legendId : undefined,
);

/**
 * Creates a MapPreset from mapId
 *
 * Example getMapPreset(store, mapId);
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {MapPreset} returnType: MapPreset
 */
export const getMapPreset = createSelector(
  getMapLayers,
  getMapBaseLayers,
  getMapOverLayers,
  getBbox,
  getSrs,
  getActiveLayerId,
  isAnimating,
  isAutoUpdating,
  isTimeSliderVisible,
  getDisplayMapPin,
  isZoomControlsVisible,
  getMapTimeStep,
  getMapAnimationDelay,
  isTimestepAuto,
  getLegendId,
  uiSelectors.getUiStore,
  (
    mapLayers: Layer[],
    baseLayers: Layer[],
    overLayers: Layer[],
    bbox: Bbox,
    srs: string,
    activeLayerId: string,
    isAnimating: boolean,
    isAutoUpdating: boolean,
    isTimeSliderVisible: boolean,
    displayMapPin: boolean,
    isZoomControlsVisible: boolean,
    mapTimeStep: number,
    mapAnimationDelay: number,
    isTimestepAuto: boolean,
    legendId: string,
    uiStore: UIStoreType,
  ): MapPreset => {
    const allLayers = [...baseLayers, ...overLayers, ...mapLayers].map(
      ({ mapId, ...layer }) => {
        if (layer.dimensions !== undefined) {
          // Filter out (reference) time dimension as these should not be part of the preset for now
          const dimensions = layer.dimensions.filter(
            (dimension) =>
              dimension.name !== 'time' && dimension.name !== 'reference_time',
          );

          return {
            ...layer,
            dimensions,
          };
        }
        return layer;
      },
    );

    const animationPayload = {
      interval: mapTimeStep,
      speed: getSpeedFactor(mapAnimationDelay),
    };

    const shouldShowLegend = uiStore?.dialogs[legendId]?.isOpen;

    return {
      layers: allLayers,
      activeLayerId,
      proj: {
        bbox,
        srs,
      },
      shouldAnimate: isAnimating,
      shouldAutoUpdate: isAutoUpdating,
      showTimeSlider: isTimeSliderVisible,
      displayMapPin,
      shouldShowZoomControls: isZoomControlsVisible,
      animationPayload,
      toggleTimestepAuto: isTimestepAuto,
      ...(shouldShowLegend !== undefined && { shouldShowLegend }),
    };
  },
  selectorMemoizationOptions,
);

/**
 * Returns the loading state of the map preset
 *
 * Example getIsMapPresetLoading(store, mapId);
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const getIsMapPresetLoading = createSelector(getMapById, (store) =>
  store && store.isMapPresetLoading ? store.isMapPresetLoading : false,
);

/**
 * Returns the has changes state of the map preset
 *
 * Example getHasMapPresetChanges(store, mapId);
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {boolean} returnType: boolean
 */
export const getHasMapPresetChanges = createSelector(getMapById, (store) =>
  store && store.hasMapPresetChanges ? store.hasMapPresetChanges : false,
);

/**
 * Returns the error state of the map preset
 *
 * Example getMapPresetError(store, mapId);
 * @param {object} store store: object - store object
 * @param {string} mapId mapId: string - Id of the map
 * @returns {Error} returnType: string
 */
export const getMapPresetError = createSelector(
  getMapById,
  (store) => store?.mapPresetError,
);
