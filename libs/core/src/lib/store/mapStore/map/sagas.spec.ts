/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  takeLatest,
  takeEvery,
  put,
  select,
  call,
  all,
} from 'redux-saga/effects';
import moment from 'moment';
import configureStore from 'redux-mock-store';
import createSagaMiddleware from 'redux-saga';

import { WMJSMap } from '@opengeoweb/webmap';
import { mapSelectors, mapActions } from '.';
import { layerActions, layerSelectors } from '../layers';
import {
  stopAnimationSaga,
  startAnimationSaga,
  rootSaga,
  updateMapDraw,
  generateTimeList,
  deleteLayerSaga,
  setLayerDimensionsSaga,
  toggleAutoUpdateSaga,
  updateAnimation,
  setMapPresetSaga,
  handleBaseLayersSaga,
  checkMapPresetForChangesSaga,
  mapPresetChangeActions,
} from './sagas';
import { TimeListType } from './types';
import { Layer, LayerActionOrigin, LayerType } from '../layers/types';
import {
  mockStateMapWithLayer,
  mockStateMapWithMultipleLayers,
} from '../../../utils/testUtils';
import * as helpers from '../utils/helpers';
import { defaultReduxServices } from '../../../utils/defaultTestSettings';
import { generateUniqueLayerIds } from '../../../components/SyncGroups/utils';
import { animationIntervalDefault } from '../../../components/TimeSlider/TimeSliderButtons/PlayButton/PlayButtonConnect';
import { dateFormat } from '../utils/helpers';
import { getSpeedDelay } from '../../../components/TimeSlider/TimeSliderUtils';
import { baseLayerGrey, overLayer } from '../../../utils/publicLayers';
import { uiActions } from '../../ui';
import { IS_LEGEND_OPEN_BY_DEFAULT } from '../../../components/Legend/LegendConnect';

describe('store/mapStore/map/sagas', () => {
  it('should catch rootSaga actions and fire corresponding sagas', () => {
    const generator = rootSaga();
    expect(generator.next().value).toEqual(
      all([
        takeLatest(mapActions.mapStopAnimation.type, stopAnimationSaga),
        takeLatest(mapActions.setActiveMapPresetId.type, stopAnimationSaga),
      ]),
    );
    expect(generator.next().value).toEqual(
      takeLatest(mapActions.mapStartAnimation.type, startAnimationSaga),
    );
    expect(generator.next().value).toEqual(
      takeLatest(layerActions.layerDelete.type, deleteLayerSaga),
    );
    expect(generator.next().value).toEqual(
      takeLatest(
        layerActions.onUpdateLayerInformation.type,
        setLayerDimensionsSaga,
      ),
    );
    expect(generator.next().value).toEqual(
      takeLatest(mapActions.toggleAutoUpdate.type, toggleAutoUpdateSaga),
    );
    expect(generator.next().value).toEqual(
      takeEvery(mapActions.setMapPreset.type, setMapPresetSaga),
    );
    expect(generator.next().value).toEqual(
      all(
        mapPresetChangeActions.map((action) =>
          takeEvery(action, checkMapPresetForChangesSaga),
        ),
      ),
    );
    expect(generator.next().done).toBeTruthy();
  });

  describe('stopAnimationSaga', () => {
    it('should stop animation', () => {
      const mapId = 'map-test';
      const action = mapActions.mapStopAnimation({
        mapId,
      });
      const generator = stopAnimationSaga(action);

      expect(generator.next().value).toEqual(
        updateMapDraw(mapId, 'opengeoweb-core-map-reducer'),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('startAnimationSaga', () => {
    it('should start animation with timelist', () => {
      const mapId = 'map-test';
      const start = '2021-01-01T11:59:30.000Z';
      const end = '2021-01-01T12:15:30.000Z';
      const interval = 5;
      const timeList = [
        { name: 'time', value: '2020-03-24T08:45:40.000Z' },
        { name: 'time', value: '2020-03-24T08:50:40.000Z' },
        { name: 'time', value: '2020-03-24T08:55:40.000Z' },
      ] as TimeListType[];

      const action = mapActions.mapStartAnimation({
        mapId,
        timeList,
        start,
        end,
        interval,
      });

      const generator = startAnimationSaga(action);
      const result = generator.next().value;
      expect(result).toEqual(timeList);
      expect(generator.next().value).toEqual(updateMapDraw(mapId, result));
    });

    it('should start animation and create timeList', () => {
      const mapId = 'map-test';

      const start = '2021-01-01T11:59:30.000Z';
      const end = '2021-01-01T12:15:30.000Z';
      const interval = 1;
      const action = mapActions.mapStartAnimation({
        mapId,
        start,
        end,
        interval,
      });

      const generator = startAnimationSaga(action);
      const result = generator.next().value;
      expect(result).toEqual(
        generateTimeList(
          moment.utc('2021-01-01T12:00:00.000Z'),
          moment.utc('2021-01-01T12:15:00.000Z'),
          interval,
        ),
      );
      expect(generator.next().value).toEqual(updateMapDraw(mapId, result));
    });
  });

  describe('updateAnimation', () => {
    const mapId = 'map-test';
    const maxValue = '2021-01-01T12:00:00Z';
    const animationStart = moment.utc('2021-01-01T10:00:00Z');
    const animationEnd = moment.utc('2021-01-01T11:00:00Z');

    it('should set animation end to max value', () => {
      const generator = updateAnimation(mapId, maxValue);
      generator.next();
      generator.next(animationStart);

      const setAnimationendAction = generator.next(animationEnd).value;
      expect(
        setAnimationendAction.payload.action.payload.animationEndTime,
      ).toEqual(maxValue);
    });

    it('should keep animation length fixed', () => {
      const generator = updateAnimation(mapId, maxValue);
      generator.next();
      generator.next(animationStart);
      generator.next(animationEnd);

      const length = animationEnd.unix() - animationStart.unix();
      const setAnimationStartAction = generator.next().value;
      const newAnimationStart =
        setAnimationStartAction.payload.action.payload.animationStartTime;
      expect(
        moment.utc(maxValue).unix() - moment.utc(newAnimationStart).unix(),
      ).toEqual(length);
    });

    it('should restart the animation', () => {
      const generator = updateAnimation(mapId, maxValue);
      generator.next();
      generator.next(animationStart);
      generator.next(animationEnd);
      generator.next();
      generator.next();
      generator.next(true);
      expect(generator.next(20).value.payload.action.type).toEqual(
        put(mapActions.mapStartAnimation({ mapId })).payload.action.type,
      );
    });
  });

  describe('deleteLayerSaga', () => {
    it('should stop animation after deleting the last layer', () => {
      const mapId = 'map-test';
      const layerId = 'layer-1';
      const action = layerActions.layerDelete({
        mapId,
        layerId,
        layerIndex: 0,
      });
      const generator = deleteLayerSaga(action);

      expect(generator.next().value).toEqual(
        select(mapSelectors.getMapLayers, mapId),
      );
      expect(generator.next([]).value).toEqual(
        put(mapActions.mapStopAnimation({ mapId })),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should not stop animation after deleting when there are still layers', () => {
      const mapId = 'map-test';
      const layerId = 'layer-1';
      const action = layerActions.layerDelete({
        mapId,
        layerId,
        layerIndex: 0,
      });
      const generator = deleteLayerSaga(action);

      expect(generator.next().value).toEqual(
        select(mapSelectors.getMapLayers, mapId),
      );
      expect(generator.next(['layer-2']).done).toBeTruthy();
    });
  });

  describe('toggleAutoUpdateSaga', () => {
    it('should update active layer to latest timedimension', () => {
      const mapId = 'map-test';
      const layerId = 'test-layer';
      const timeDimension = {
        name: 'time',
        currentValue: 'xxx',
        maxValue: 'yyy',
      };

      const action = mapActions.toggleAutoUpdate({
        mapId,
        shouldAutoUpdate: true,
      });

      const generator = toggleAutoUpdateSaga(action);

      expect(generator.next().value).toEqual(
        select(mapSelectors.getActiveLayerId, mapId),
      );

      expect(generator.next(layerId).value).toEqual(
        select(layerSelectors.getLayerTimeDimension, layerId),
      );

      expect(generator.next(timeDimension).value).toEqual(
        put(
          layerActions.layerChangeDimension({
            layerId,
            origin: LayerActionOrigin.toggleAutoUpdateSaga,
            dimension: {
              name: 'time',
              currentValue: timeDimension.maxValue,
            },
          }),
        ),
      );

      generator.next();
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('setLayerDimensionsSaga', () => {
    it('should update the active layer and animation to new time dimension if there is new data and autoupdating is true', () => {
      const mapId = 'map-test';
      const layer = {
        service: 'https://testservice',
        name: 'test-name',
        title: 'test-title',
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2021-11-18T12:45:00Z',
            maxValue: '2021-11-18T12:45:00Z',
            minValue: '2021-11-11T00:00:00Z',
          },
        ],
        id: 'test-layer',
      };
      const saga = createSagaMiddleware();
      const mockState = mockStateMapWithLayer(layer, mapId);

      expect(mockState.webmap.byId[mapId].activeLayerId).toEqual(layer.id);
      mockState.webmap.byId[mapId].isAutoUpdating = true;
      const mockStore = configureStore([saga]);
      const store = mockStore(mockState);
      const timeDimension = {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-11-18T12:45:00Z',
        maxValue: '2021-11-18T12:45:00Z',
        minValue: '2021-11-11T00:00:00Z',
      };
      const timeDimension2 = {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-11-18T12:45:00Z',
        maxValue: '2021-11-18T12:50:00Z',
        minValue: '2021-11-11T00:00:00Z',
      };

      const action = layerActions.onUpdateLayerInformation({
        origin: 'test',
        layerDimensions: {
          layerId: layer.id,
          dimensions: [timeDimension],
          origin: 'sagas.spec',
        },
      });

      const action2 = layerActions.onUpdateLayerInformation({
        origin: 'test',
        layerDimensions: {
          layerId: layer.id,
          dimensions: [timeDimension2],
          origin: 'sagas.spec',
        },
      });

      const actionNotToPut = layerActions.layerChangeDimension({
        layerId: layer.id,
        origin: LayerActionOrigin.setLayerDimensionSaga,
        dimension: {
          name: 'time',
          currentValue: timeDimension.maxValue,
        },
      });

      const actionToPut = layerActions.layerChangeDimension({
        layerId: layer.id,
        origin: LayerActionOrigin.setLayerDimensionSaga,
        dimension: {
          name: 'time',
          currentValue: timeDimension2.maxValue,
        },
      });

      const actionEndTime = mapActions.setAnimationEndTime({
        animationEndTime: timeDimension2.maxValue,
        mapId,
      });

      const actionStartTime = mapActions.setAnimationStartTime({
        animationStartTime: '2021-11-18T08:00:00Z',
        mapId,
      });

      const actionStartAnimation = mapActions.mapStartAnimation({
        mapId: 'map-test',
        start: actionStartTime.payload.animationStartTime,
        end: actionEndTime.payload.animationEndTime,
        interval: 300,
      });

      let actionsToCheck;
      store.subscribe(() => {
        const actions = store.getActions();
        if (actions.length >= 3) {
          actionsToCheck = actions;
        }
      });

      saga.run(setLayerDimensionsSaga, action);
      saga.run(setLayerDimensionsSaga, action2);

      expect(actionsToCheck.length).toEqual(3);
      expect(actionsToCheck).not.toContainEqual(actionNotToPut);
      expect(actionsToCheck).toContainEqual(actionToPut);
      expect(actionsToCheck).toContainEqual(actionEndTime);
      expect(actionsToCheck).toContainEqual(actionStartTime);
      expect(actionsToCheck).not.toContainEqual(actionStartAnimation);
    });

    it('should restart the animation if animation was running and there is new time dimension data', () => {
      jest
        .spyOn(helpers, 'getWMJSMapById')
        .mockReturnValueOnce({ isAnimating: true } as WMJSMap);
      const mapId = 'map-test';
      const layer = {
        service: 'https://testservice',
        name: 'test-name',
        title: 'test-title',
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2021-11-18T12:45:00Z',
            maxValue: '2021-11-18T12:45:00Z',
            minValue: '2021-11-11T00:00:00Z',
          },
        ],
        id: 'test-layer',
      };
      const saga = createSagaMiddleware();
      const mockState = mockStateMapWithLayer(layer, mapId);
      expect(mockState.webmap.byId[mapId].activeLayerId).toEqual(layer.id);
      mockState.webmap.byId[mapId].isAutoUpdating = true;
      mockState.webmap.byId[mapId].isAnimating = true;
      const mockStore = configureStore([saga]);
      const store = mockStore(mockState);
      const timeDimension = {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-11-18T12:45:00Z',
        maxValue: '2021-11-18T12:45:00Z',
        minValue: '2021-11-11T00:00:00Z',
      };
      const timeDimension2 = {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-11-18T12:45:00Z',
        maxValue: '2021-11-18T12:50:00Z',
        minValue: '2021-11-11T00:00:00Z',
      };

      const action = layerActions.onUpdateLayerInformation({
        origin: 'test',
        layerDimensions: {
          layerId: layer.id,
          dimensions: [timeDimension],
          origin: 'sagas.spec',
        },
      });

      const action2 = layerActions.onUpdateLayerInformation({
        origin: 'test',
        layerDimensions: {
          layerId: layer.id,
          dimensions: [timeDimension2],
          origin: 'sagas.spec',
        },
      });

      const actionNotToPut = {
        payload: {
          layerId: layer.id,
          origin: LayerActionOrigin.setLayerDimensionSaga,
          dimension: {
            name: 'time',
            currentValue: timeDimension.maxValue,
          },
        },
        type: 'WEBMAP_LAYER_CHANGE_DIMENSION',
      };

      const actionEndTime = mapActions.setAnimationEndTime({
        animationEndTime: timeDimension2.maxValue,
        mapId,
      });

      const actionStartTime = mapActions.setAnimationStartTime({
        animationStartTime: '2021-11-18T08:00:00Z',
        mapId,
      });

      const actionStartAnimation = mapActions.mapStartAnimation({
        mapId: 'map-test',
        start: actionStartTime.payload.animationStartTime,
        end: actionEndTime.payload.animationEndTime,
        interval: 5,
      });

      let actionsToCheck;
      store.subscribe(() => {
        const actions = store.getActions();
        if (actions.length >= 3) {
          actionsToCheck = actions;
        }
      });

      saga.run(setLayerDimensionsSaga, action);
      saga.run(setLayerDimensionsSaga, action2);
      expect(actionsToCheck.length).toEqual(3);
      expect(actionsToCheck).not.toContainEqual(actionNotToPut);
      expect(actionsToCheck).toContainEqual(actionEndTime);
      expect(actionsToCheck).toContainEqual(actionStartTime);
      expect(actionsToCheck).toContainEqual(actionStartAnimation);
    });
    it('should start the animation if animation is set in redux store but not running yet on the map even if already have the latest dimension', () => {
      jest
        .spyOn(helpers, 'getWMJSMapById')
        .mockReturnValueOnce({ isAnimating: false } as WMJSMap);
      const mapId = 'map-test';
      const layer = {
        service: 'https://testservice',
        name: 'test-name',
        title: 'test-title',
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2021-11-18T12:45:00Z',
            maxValue: '2021-11-18T12:45:00Z',
            minValue: '2021-11-11T00:00:00Z',
          },
        ],
        id: 'test-layer',
      };
      const saga = createSagaMiddleware();
      const mockState = mockStateMapWithLayer(layer, mapId);
      expect(mockState.webmap.byId[mapId].activeLayerId).toEqual(layer.id);
      mockState.webmap.byId[mapId].isAutoUpdating = true;
      mockState.webmap.byId[mapId].isAnimating = true;
      const mockStore = configureStore([saga]);
      const store = mockStore(mockState);
      const timeDimension = {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-11-18T12:45:00Z',
        maxValue: '2021-11-18T12:45:00Z',
        minValue: '2021-11-11T00:00:00Z',
      };

      const action = layerActions.onUpdateLayerInformation({
        origin: 'test',
        layerDimensions: {
          layerId: layer.id,
          dimensions: [timeDimension],
          origin: 'sagas.spec',
        },
      });

      const actionNotToPut = layerActions.layerChangeDimension({
        layerId: layer.id,
        origin: LayerActionOrigin.setLayerDimensionSaga,
        dimension: {
          name: 'time',
          currentValue: timeDimension.maxValue,
        },
      });

      const actionEndTime = mapActions.setAnimationEndTime({
        animationEndTime: timeDimension.maxValue,
        mapId,
      });

      const actionStartTime = mapActions.setAnimationStartTime({
        animationStartTime: '2021-11-18T07:55:00Z',
        mapId,
      });

      const actionStartAnimation = mapActions.mapStartAnimation({
        mapId: 'map-test',
        start: actionStartTime.payload.animationStartTime,
        end: actionEndTime.payload.animationEndTime,
        interval: 5,
      });

      let actionsToCheck;
      store.subscribe(() => {
        const actions = store.getActions();
        if (actions.length >= 3) {
          actionsToCheck = actions;
        }
      });

      saga.run(setLayerDimensionsSaga, action);
      expect(actionsToCheck.length).toEqual(3);
      expect(actionsToCheck).not.toContainEqual(actionNotToPut);
      expect(actionsToCheck).toContainEqual(actionEndTime);
      expect(actionsToCheck).toContainEqual(actionStartTime);
      expect(actionsToCheck).toContainEqual(actionStartAnimation);
    });

    it('should only update the active layer', () => {
      const mapId = 'map-test';
      const layer = {
        service: 'https://testservice',
        name: 'test-name',
        title: 'test-title',
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2021-11-18T12:45:00Z',
            maxValue: '2021-11-18T12:45:00Z',
            minValue: '2021-11-11T00:00:00Z',
          },
        ],
        id: 'test-layer',
      };
      const layer2 = {
        ...layer,
        id: 'test-layer-2',
      };
      const saga = createSagaMiddleware();
      const mockState = mockStateMapWithMultipleLayers([layer, layer2], mapId);
      expect(mockState.webmap.byId[mapId].activeLayerId).toEqual(layer.id);
      mockState.webmap.byId[mapId].isAutoUpdating = true;
      const mockStore = configureStore([saga]);
      const store = mockStore(mockState);
      const timeDimension = {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-11-18T12:45:00Z',
        maxValue: '2021-11-18T12:45:00Z',
        minValue: '2021-11-11T00:00:00Z',
      };
      const timeDimension2 = {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-11-18T12:45:00Z',
        maxValue: '2021-11-18T12:50:00Z',
        minValue: '2021-11-11T00:00:00Z',
      };

      const action = layerActions.onUpdateLayerInformation({
        origin: 'test',
        layerDimensions: {
          layerId: layer.id,
          dimensions: [timeDimension],
          origin: 'sagas.spec',
        },
      });
      const action2 = layerActions.onUpdateLayerInformation({
        origin: 'test',
        layerDimensions: {
          layerId: layer.id,
          dimensions: [timeDimension2],
          origin: 'sagas.spec',
        },
      });

      const actionNotToPut = layerActions.layerChangeDimension({
        layerId: layer2.id,
        origin: LayerActionOrigin.setLayerDimensionSaga,
        dimension: {
          name: 'time',
          currentValue: timeDimension2.maxValue,
        },
      });

      const actionToPut = layerActions.layerChangeDimension({
        layerId: layer.id,
        origin: LayerActionOrigin.setLayerDimensionSaga,
        dimension: {
          name: 'time',
          currentValue: timeDimension2.maxValue,
        },
      });

      const actionEndTime = mapActions.setAnimationEndTime({
        animationEndTime: timeDimension2.maxValue,
        mapId,
      });

      const actionStartTime = mapActions.setAnimationStartTime({
        animationStartTime: '2021-11-18T08:00:00Z',
        mapId,
      });

      const actionStartAnimation = mapActions.mapStartAnimation({
        mapId: 'map-test',
        start: actionStartTime.payload.animationStartTime,
        end: actionEndTime.payload.animationEndTime,
        interval: 300,
      });
      let actionsToCheck;
      store.subscribe(() => {
        const actions = store.getActions();
        if (actions.length >= 3) {
          actionsToCheck = actions;
        }
      });

      saga.run(setLayerDimensionsSaga, action);
      saga.run(setLayerDimensionsSaga, action2);

      expect(actionsToCheck.length).toEqual(3);
      expect(actionsToCheck).not.toContainEqual(actionNotToPut);
      expect(actionsToCheck).toContainEqual(actionToPut);
      expect(actionsToCheck).toContainEqual(actionEndTime);
      expect(actionsToCheck).toContainEqual(actionStartTime);
      expect(actionsToCheck).not.toContainEqual(actionStartAnimation);
    });

    it('should not update the active layer and animation to new time dimension if there is new data but autoupdating is false', () => {
      const mapId = 'map-test';
      const layer = {
        service: 'https://testservice',
        name: 'test-name',
        title: 'test-title',
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2021-11-18T12:45:00Z',
            maxValue: '2021-11-18T12:45:00Z',
            minValue: '2021-11-11T00:00:00Z',
          },
        ],
        id: 'test-layer',
      };
      const saga = createSagaMiddleware();
      const mockState = mockStateMapWithLayer(layer, mapId);
      expect(mockState.webmap.byId[mapId].activeLayerId).toEqual(layer.id);
      mockState.webmap.byId[mapId].isAutoUpdating = false;
      const mockStore = configureStore([saga]);
      const store = mockStore(mockState);
      const timeDimension = {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-11-18T12:45:00Z',
        maxValue: '2021-11-18T12:45:00Z',
        minValue: '2021-11-11T00:00:00Z',
      };
      const timeDimension2 = {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-11-18T12:45:00Z',
        maxValue: '2021-11-18T12:50:00Z',
        minValue: '2021-11-11T00:00:00Z',
      };

      const action = layerActions.onUpdateLayerInformation({
        origin: 'test',
        layerDimensions: {
          layerId: layer.id,
          dimensions: [timeDimension],
          origin: 'sagas.spec',
        },
      });
      const action2 = layerActions.onUpdateLayerInformation({
        origin: 'test',
        layerDimensions: {
          layerId: layer.id,
          dimensions: [timeDimension2],
          origin: 'sagas.spec',
        },
      });

      const actionNotToPut = layerActions.layerChangeDimension({
        layerId: layer.id,
        origin: LayerActionOrigin.setLayerDimensionSaga,
        dimension: {
          name: 'time',
          currentValue: timeDimension.maxValue,
        },
      });

      const actionToPut = layerActions.layerChangeDimension({
        layerId: layer.id,
        origin: LayerActionOrigin.setLayerDimensionSaga,
        dimension: {
          name: 'time',
          currentValue: timeDimension2.maxValue,
        },
      });

      const actionEndTime = mapActions.setAnimationEndTime({
        animationEndTime: timeDimension2.maxValue,
        mapId,
      });

      const actionStartTime = mapActions.setAnimationStartTime({
        animationStartTime: '2021-11-18T08:00:00Z',
        mapId,
      });

      const actionStartAnimation = mapActions.mapStartAnimation({
        mapId: 'map-test',
        start: actionStartTime.payload.animationStartTime,
        end: actionEndTime.payload.animationEndTime,
        interval: 300,
      });
      store.subscribe(() => {
        const actions = store.getActions();
        expect(actions).not.toContainEqual(actionNotToPut);
        expect(actions).not.toContainEqual(actionToPut);
        expect(actions).not.toContainEqual(actionEndTime);
        expect(actions).not.toContainEqual(actionStartTime);
        expect(actions).not.toContainEqual(actionStartAnimation);
      });

      saga.run(setLayerDimensionsSaga, action);
      saga.run(setLayerDimensionsSaga, action2);
    });

    it('should auto update the active layer for multiple maps if there is new data', () => {
      const mapId = 'map-test';
      const mapId2 = 'map-test-2';
      const layer = {
        mapId,
        service: 'https://testservice',
        name: 'test-name',
        title: 'test-title',
        enabled: true,
        layerType: LayerType.mapLayer,
        dimensions: [
          {
            name: 'time',
            units: 'ISO8601',
            currentValue: '2021-11-18T12:45:00Z',
            maxValue: '2021-11-18T12:45:00Z',
            minValue: '2021-11-11T00:00:00Z',
          },
        ],
        id: 'test-layer',
      };
      const layer2 = {
        ...layer,
        mapId: mapId2,
        id: 'test-layer-2',
      };

      const saga = createSagaMiddleware();
      const mockState = {
        webmap: {
          byId: {
            [mapId]: {
              id: mapId,
              isAnimating: false,
              animationStartTime: '2021-11-18T09:15:00Z',
              animationEndTime: '2021-11-18T14:05:00Z',
              isAutoUpdating: true,
              srs: 'EPSG:3857',
              bbox: {
                left: -450651.2255879827,
                bottom: 6393842.957153378,
                right: 1428345.8183648037,
                top: 7342085.640241,
              },
              baseLayers: ['baseLayer-screenA'],
              overLayers: ['layerid_92'],
              mapLayers: [layer.id],
              featureLayers: [],
              dimensions: [
                {
                  name: 'time',
                  currentValue: '2021-11-18T12:45:00Z',
                },
              ],
              activeLayerId: layer.id,
              timeSliderScale: 1,
              timeStep: 5,
              animationDelay: 250,
              timeSliderCenterTime: 1637236303,
              timeSliderSecondsPerPx: 30,
              timeSliderDataScaleToSecondsPerPx: 43200,
              isTimestepAuto: true,
              isTimeSliderHoverOn: false,
            },
            [mapId2]: {
              id: mapId2,
              isAnimating: false,
              animationStartTime: '2021-11-18T09:15:00Z',
              animationEndTime: '2021-11-18T14:05:00Z',
              isAutoUpdating: true,
              srs: 'EPSG:3857',
              bbox: {
                left: -450651.2255879827,
                bottom: 6490531.093143953,
                right: 1428345.8183648037,
                top: 7438773.776232235,
              },
              baseLayers: ['baseLayer-screen_1'],
              overLayers: ['layerid_96'],
              mapLayers: [layer2.id],
              featureLayers: [],
              dimensions: [
                {
                  name: 'time',
                  currentValue: '2021-11-18T12:45:00Z',
                },
              ],
              activeLayerId: layer2.id,
              timeSliderScale: 1,
              timeStep: 5,
              animationDelay: 250,
              timeSliderCenterTime: 1637236770,
              timeSliderSecondsPerPx: 30,
              timeSliderDataScaleToSecondsPerPx: 43200,
              isTimestepAuto: true,
              isTimeSliderHoverOn: false,
            },
          },
          allIds: [mapId, mapId2],
        },
        services: {
          byId: defaultReduxServices,
          allIds: ['serviceid_1'],
        },
        layers: {
          byId: {
            [layer.id]: layer,
            [layer2.id]: layer2,
          },
          allIds: [layer.id, layer2.id],
          availableBaseLayers: { allIds: [], byId: {} },
        },
      };

      const mockStore = configureStore([saga]);
      const store = mockStore(mockState);

      const timeDimension = {
        name: 'time',
        units: 'ISO8601',
        currentValue: '2021-11-18T12:45:00Z',
        maxValue: '2021-11-18T12:50:00Z',
        minValue: '2021-11-11T00:00:00Z',
      };

      const action = layerActions.onUpdateLayerInformation({
        origin: 'test',
        layerDimensions: {
          layerId: layer.id,
          dimensions: [timeDimension],
          origin: 'sagas.spec',
        },
      });
      const action2 = layerActions.onUpdateLayerInformation({
        origin: 'test',
        layerDimensions: {
          layerId: layer2.id,
          dimensions: [timeDimension],
          origin: 'sagas.spec',
        },
      });

      const actionToPut = layerActions.layerChangeDimension({
        layerId: layer.id,
        origin: LayerActionOrigin.setLayerDimensionSaga,
        dimension: {
          name: 'time',
          currentValue: timeDimension.maxValue,
        },
      });

      const action2ToPut = layerActions.layerChangeDimension({
        layerId: layer2.id,
        origin: LayerActionOrigin.setLayerDimensionSaga,
        dimension: {
          name: 'time',
          currentValue: timeDimension.maxValue,
        },
      });

      const actionEndTime = mapActions.setAnimationEndTime({
        animationEndTime: timeDimension.maxValue,
        mapId,
      });

      const actionStartTime = mapActions.setAnimationStartTime({
        animationStartTime: '2021-11-18T08:00:00Z',
        mapId,
      });

      const actionEndTime2 = mapActions.setAnimationEndTime({
        animationEndTime: timeDimension.maxValue,
        mapId: mapId2,
      });

      const actionStartTime2 = mapActions.setAnimationStartTime({
        animationStartTime: '2021-11-18T08:00:00Z',
        mapId: mapId2,
      });

      let actionsToCheck;
      store.subscribe(() => {
        const actions = store.getActions();
        if (actions.length >= 1) {
          actionsToCheck = actions;
        }
      });

      saga.run(setLayerDimensionsSaga, action);
      saga.run(setLayerDimensionsSaga, action2);

      expect(actionsToCheck.length).toEqual(6);
      expect(actionsToCheck).toContainEqual(actionToPut);
      expect(actionsToCheck).toContainEqual(actionEndTime);
      expect(actionsToCheck).toContainEqual(actionStartTime);

      expect(actionsToCheck).toContainEqual(action2ToPut);
      expect(actionsToCheck).toContainEqual(actionEndTime2);
      expect(actionsToCheck).toContainEqual(actionStartTime2);
    });
  });

  describe('handleBaseLayersSaga', () => {
    it('should set baseLayers', () => {
      const mapId = 'test-1';
      const baseLayers = [baseLayerGrey];
      const generator = handleBaseLayersSaga(mapId, baseLayers);

      expect(generator.next().value).toEqual(
        select(layerSelectors.getAvailableBaseLayersForMap, mapId),
      );

      expect(generator.next([]).value).toEqual(
        put(
          layerActions.setBaseLayers({
            mapId,
            layers: [
              { ...baseLayers[0], id: expect.stringContaining('layerid_') },
            ],
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should set baseLayers including overLayers', () => {
      const mapId = 'test-1';
      const baseLayers = [
        baseLayerGrey,
        { id: 'overlayer', layerType: 'overLayer' as LayerType },
        { id: 'overlayer-2', layerType: 'overLayer' as LayerType },
      ];
      const generator = handleBaseLayersSaga(mapId, baseLayers);

      expect(generator.next().value).toEqual(
        select(layerSelectors.getAvailableBaseLayersForMap, mapId),
      );

      expect(generator.next([]).value).toEqual(
        put(
          layerActions.setBaseLayers({
            mapId,
            layers: baseLayers.map((layer) => ({
              ...layer,
              id: expect.anything(),
            })),
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should set a baseLayer id if the name exist in the available baselayer list', () => {
      const mapId = 'test-1';
      const testExistingId = 'test-id-2';
      const baseLayers = [{ ...baseLayerGrey, name: 'OpenStreetMap number 2' }];
      const generator = handleBaseLayersSaga(mapId, baseLayers);

      expect(generator.next().value).toEqual(
        select(layerSelectors.getAvailableBaseLayersForMap, mapId),
      );

      expect(
        generator.next([
          {
            id: 'test-id-1',
            name: 'OpenStreetMap',
          },
          {
            id: testExistingId,
            name: 'OpenStreetMap number 2',
          },
        ]).value,
      ).toEqual(
        put(
          layerActions.setBaseLayers({
            mapId,
            layers: [{ ...baseLayers[0], id: testExistingId }],
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should add a baseLayer to the available baselayer list if it does not exist in the existing list', () => {
      const mapId = 'test-1';

      const baseLayers = [
        {
          ...baseLayerGrey,
          name: 'New one that does not exist',
        },
      ];
      const generator = handleBaseLayersSaga(mapId, baseLayers);

      expect(generator.next().value).toEqual(
        select(layerSelectors.getAvailableBaseLayersForMap, mapId),
      );

      expect(
        generator.next([
          {
            id: 'test-id-1',
            name: 'OpenStreetMap',
          },
          {
            id: 'test-id-2',
            name: 'OpenStreetMap number 2',
          },
        ]).value,
      ).toEqual(
        put(
          layerActions.addAvailableBaseLayers({
            layers: [
              {
                ...baseLayers[0],
                mapId,
                id: expect.stringContaining('layerid'),
              },
            ],
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          layerActions.setBaseLayers({
            mapId,
            layers: [
              { ...baseLayers[0], id: expect.stringContaining('layerid') },
            ],
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('setMapPresetSaga', () => {
    const now = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(now).valueOf());
    const expectedDefaultBaseLayers = [baseLayerGrey, overLayer].map(
      (layer) => ({
        ...layer,
        id: expect.stringContaining('layerid_'),
      }),
    );
    it('should not do anyting if mapPreset has no props', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {},
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);
      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should fire setLayers and setActiveLayerId when layers given', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'layer-1',
                layerType: LayerType.mapLayer,
              },
              {
                id: 'layer-2',
                layerType: LayerType.mapLayer,
              },
            ],
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          generateUniqueLayerIds as any,
          payload.initialProps.mapPreset.layers,
          undefined,
        ),
      );

      const newTestIds = generateUniqueLayerIds(
        payload.initialProps.mapPreset.layers as Layer[],
        'layer_1',
      );

      expect(generator.next(newTestIds).value).toEqual(
        put(
          layerActions.setLayers({
            mapId: payload.mapId,
            layers: newTestIds.layersNewIds,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          mapActions.setActiveLayerId({
            mapId: payload.mapId,
            layerId: newTestIds.activeLayerNewId,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should fire setLayers and setActiveLayerId when layers and activeLayerId given', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            activeLayerId: 'layerid_2',
            layers: [
              {
                id: 'layerid_1',
                layerType: LayerType.mapLayer,
              },
              {
                id: 'layerid_2',
                layerType: LayerType.mapLayer,
              },
            ],
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          generateUniqueLayerIds as any,
          payload.initialProps.mapPreset.layers,
          payload.initialProps.mapPreset.activeLayerId,
        ),
      );

      const newTestIds = generateUniqueLayerIds(
        payload.initialProps.mapPreset.layers as Layer[],
        'layerid_2',
      );

      expect(generator.next(newTestIds).value).toEqual(
        put(
          layerActions.setLayers({
            mapId: payload.mapId,
            layers: newTestIds.layersNewIds,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          mapActions.setActiveLayerId({
            mapId: payload.mapId,
            layerId: newTestIds.activeLayerNewId,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should fire setLayers and setActiveLayerId when empty layers given', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            layers: [],
            activeLayerId: undefined,
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          generateUniqueLayerIds as any,
          payload.initialProps.mapPreset.layers,
          payload.initialProps.mapPreset.activeLayerId,
        ),
      );

      const newTestIds = generateUniqueLayerIds(
        payload.initialProps.mapPreset.layers as Layer[],
        undefined,
      );

      expect(generator.next(newTestIds).value).toEqual(
        put(
          layerActions.setLayers({
            mapId: payload.mapId,
            layers: payload.initialProps.mapPreset.layers as Layer[],
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          mapActions.setActiveLayerId({
            mapId: payload.mapId,
            layerId: undefined,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should fire handleBaseLayersSaga with baseLayer', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'base-layer-1',
                name: 'WorldMap_Light_Grey_Canvas',
                type: 'twms',
                layerType: LayerType.baseLayer,
              },
            ],
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(generateUniqueLayerIds, [], undefined),
      );

      expect(
        generator.next({ layersNewIds: [], activeLayerNewId: undefined }).value,
      ).toEqual(
        put(
          layerActions.setLayers({
            mapId: payload.mapId,
            layers: [],
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          mapActions.setActiveLayerId({
            mapId: payload.mapId,
            layerId: undefined,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, [
          {
            ...payload.initialProps.mapPreset.layers[0],
            id: expect.stringContaining('layerid_'),
          } as Layer,
          { ...overLayer, id: expect.stringContaining('layerid_') },
        ]),
      );
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should fire handleBaseLayersSaga with overlayers when baseLayer and overlayers are given', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'base-layer-1',
                name: 'WorldMap_Light_Grey_Canvas',
                type: 'twms',
                layerType: LayerType.baseLayer,
              },
              {
                id: 'over-layer-1',
                name: 'OverlayWorld',
                type: 'twms',
                layerType: LayerType.overLayer,
              },
            ],
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(generateUniqueLayerIds, [], undefined),
      );

      expect(
        generator.next({ layersNewIds: [], activeLayerNewId: undefined }).value,
      ).toEqual(
        put(
          layerActions.setLayers({
            mapId: payload.mapId,
            layers: [],
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          mapActions.setActiveLayerId({
            mapId: payload.mapId,
            layerId: undefined,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, [
          {
            ...payload.initialProps.mapPreset.layers[0],
            id: expect.stringContaining('layerid_'),
          } as Layer,
          {
            ...payload.initialProps.mapPreset.layers[1],
            id: expect.stringContaining('layerid_'),
          } as Layer,
        ]),
      );
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should fire shouldAutoUpdate when given', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            shouldAutoUpdate: true,
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );
      expect(generator.next().value).toEqual(
        put(
          mapActions.toggleAutoUpdate({
            mapId: payload.mapId,
            shouldAutoUpdate: payload.initialProps.mapPreset.shouldAutoUpdate,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should fire showTimeSlider when given', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            showTimeSlider: true,
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );
      expect(generator.next().value).toEqual(
        put(
          mapActions.toggleTimeSliderIsVisible({
            mapId: payload.mapId,
            isTimeSliderVisible: payload.initialProps.mapPreset.showTimeSlider,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should fire bbox when given', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            proj: {
              bbox: {
                left: -7529663.50832266,
                bottom: 308359.5390525013,
                right: 7493930.85787452,
                top: 11742807.68245839,
              },
              srs: 'EPSG:3857',
            },
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );
      expect(generator.next().value).toEqual(
        put(
          mapActions.setBbox({
            mapId: payload.mapId,
            bbox: payload.initialProps.mapPreset.proj.bbox,
            srs: payload.initialProps.mapPreset.proj.srs,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should fire show zoomControls when given ', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            shouldShowZoomControls: false,
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );

      expect(generator.next().value).toEqual(
        put(
          mapActions.toggleZoomControls({
            mapId: payload.mapId,
            shouldShowZoomControls:
              payload.initialProps.mapPreset.shouldShowZoomControls,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );
      expect(generator.next().done).toBeTruthy();
    });
    it('should fire show displayMapPin when given ', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            displayMapPin: true,
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );

      expect(generator.next().value).toEqual(
        put(
          mapActions.toggleMapPinIsVisible({
            mapId: payload.mapId,
            displayMapPin: payload.initialProps.mapPreset.displayMapPin,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should fire mapStartAnimation when shouldAnimate is given ', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            shouldAnimate: true,
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );

      const duration = 5 * 60; // sets a default animation when none is given
      expect(generator.next().value).toEqual(
        put(
          mapActions.mapStartAnimation({
            mapId: payload.mapId,
            start: moment(now).subtract(duration, 'minutes').format(dateFormat),
            end: now,
            interval: animationIntervalDefault,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should fire mapStartAnimation with duration ', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            shouldAnimate: true,
            animationPayload: {
              duration: 99999,
            },
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );

      expect(generator.next().value).toEqual(
        put(
          mapActions.mapStartAnimation({
            mapId: payload.mapId,
            start: moment(now)
              .subtract(
                payload.initialProps.mapPreset.animationPayload.duration,
                'minutes',
              )
              .format(dateFormat),
            end: now,
            interval: animationIntervalDefault,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should fire mapStartAnimation with duration and interval and should toggleTimestepAuto false', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            shouldAnimate: true,
            animationPayload: {
              duration: 99999,
              interval: 88888,
            },
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );

      expect(generator.next().value).toEqual(
        put(
          mapActions.setAnimationInterval({
            mapId: payload.mapId,
            interval: payload.initialProps.mapPreset.animationPayload.interval,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          mapActions.mapStartAnimation({
            mapId: payload.mapId,
            start: moment(now)
              .subtract(
                payload.initialProps.mapPreset.animationPayload.duration,
                'minutes',
              )
              .format(dateFormat),
            end: now,
            interval: payload.initialProps.mapPreset.animationPayload.interval,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          mapActions.toggleTimestepAuto({
            mapId: payload.mapId,
            timestepAuto: false,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should fire mapStartAnimation with speed', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            shouldAnimate: true,
            animationPayload: {
              duration: 99999,
              speed: 16,
            },
          },
        },
      } as const;
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );

      expect(generator.next().value).toEqual(
        put(
          mapActions.setAnimationDelay({
            mapId: payload.mapId,
            animationDelay: getSpeedDelay(
              payload.initialProps.mapPreset.animationPayload.speed,
            ),
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          mapActions.mapStartAnimation({
            mapId: payload.mapId,
            start: moment(now)
              .subtract(
                payload.initialProps.mapPreset.animationPayload.duration,
                'minutes',
              )
              .format(dateFormat),
            end: now,
            interval: animationIntervalDefault,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should fire toggleTimestepAuto', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            toggleTimestepAuto: true,
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );

      expect(generator.next().value).toEqual(
        put(
          mapActions.toggleTimestepAuto({
            mapId: payload.mapId,
            timestepAuto: payload.initialProps.mapPreset.toggleTimestepAuto,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should set timestep by passing interval with animationPayload', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            animationPayload: {
              interval: 5,
            },
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );

      expect(generator.next().value).toEqual(
        put(
          mapActions.setAnimationInterval({
            mapId: payload.mapId,
            interval: payload.initialProps.mapPreset.animationPayload.interval,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should open legend when shouldShowLegend is given', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            shouldShowLegend: true,
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );

      const legendId = 'legend1-1';
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );

      expect(generator.next(legendId).value).toEqual(
        put(
          uiActions.setToggleOpenDialog({
            type: legendId,
            setOpen: payload.initialProps.mapPreset.shouldShowLegend,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should hide legend when shouldShowLegend is given', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {
            shouldShowLegend: false,
          },
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );

      const legendId = 'legend1-1';
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );

      expect(generator.next(legendId).value).toEqual(
        put(
          uiActions.setToggleOpenDialog({
            type: legendId,
            setOpen: payload.initialProps.mapPreset.shouldShowLegend,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should show legend when shouldShowLegend is not given', () => {
      const payload = {
        mapId: 'test-1',
        initialProps: {
          mapPreset: {},
        },
      };
      const action = mapActions.setMapPreset(payload);
      const generator = setMapPresetSaga(action);

      expect(generator.next().value).toEqual(
        call(handleBaseLayersSaga, payload.mapId, expectedDefaultBaseLayers),
      );

      const legendId = 'legend1-1';
      expect(generator.next().value).toEqual(
        select(mapSelectors.getLegendId, payload.mapId),
      );

      expect(generator.next(legendId).value).toEqual(
        put(
          uiActions.setToggleOpenDialog({
            type: legendId,
            setOpen: IS_LEGEND_OPEN_BY_DEFAULT,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('checkMapPresetForChangesSaga', () => {
    it('should fire setHasMapPresetChanges', () => {
      const mapId = 'map-test';
      const action = layerActions.setBaseLayers({
        mapId,
        layers: [
          {
            service:
              'https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/',
            name: 'arcGisCanvas',
            type: 'twms',
            mapId: 'mapid_1',
            dimensions: [],
            id: 'layerid_41',
            opacity: 1,
            enabled: true,
            layerType: LayerType.baseLayer,
          },
        ],
        origin: LayerActionOrigin.layerManager,
      });
      const generator = checkMapPresetForChangesSaga(action);

      expect(generator.next().value).toEqual(
        select(mapSelectors.getHasMapPresetChanges, mapId),
      );

      expect(generator.next().value).toEqual(
        put(
          mapActions.setHasMapPresetChanges({
            mapId,
            hasChanges: true,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('not fire setHasMapPresetChanges if there are already changes', () => {
      const mapId = 'map-test';
      const action = layerActions.setBaseLayers({
        mapId,
        layers: [
          {
            service:
              'https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/',
            name: 'arcGisCanvas',
            type: 'twms',
            mapId: 'mapid_1',
            dimensions: [],
            id: 'layerid_41',
            opacity: 1,
            enabled: true,
            layerType: LayerType.baseLayer,
          },
        ],
        origin: LayerActionOrigin.layerManager,
      });
      const generator = checkMapPresetForChangesSaga(action);

      expect(generator.next().value).toEqual(
        select(mapSelectors.getHasMapPresetChanges, mapId),
      );

      expect(generator.next(true).done).toBeTruthy();
    });

    it('not fire setHasMapPresetChanges if origin is not given', () => {
      const mapId = 'map-test';
      const action = layerActions.setBaseLayers({
        mapId,
        layers: [
          {
            service:
              'https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/',
            name: 'arcGisCanvas',
            type: 'twms',
            mapId: 'mapid_1',
            dimensions: [],
            id: 'layerid_41',
            opacity: 1,
            enabled: true,
            layerType: LayerType.baseLayer,
          },
        ],
      });
      const generator = checkMapPresetForChangesSaga(action);

      expect(generator.next(true).done).toBeTruthy();
    });

    it('not fire setHasMapPresetChanges if origin is not supported', () => {
      const mapId = 'map-test';
      const action = layerActions.setBaseLayers({
        mapId,
        layers: [
          {
            service:
              'https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/',
            name: 'arcGisCanvas',
            type: 'twms',
            mapId: 'mapid_1',
            dimensions: [],
            id: 'layerid_41',
            opacity: 1,
            enabled: true,
            layerType: LayerType.baseLayer,
          },
        ],
        origin: LayerActionOrigin.ReactMapViewParseLayer,
      });
      const generator = checkMapPresetForChangesSaga(action);

      expect(generator.next(true).done).toBeTruthy();
    });
  });
});
