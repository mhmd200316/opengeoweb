/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import moment from 'moment';
import { Bbox, Dimension, Scale } from './types';
import { WebMapState, WebMap } from '../types';
import { Layer } from '../layers/types';
import {
  dateFormat,
  getWMJSDimensionForLayerAndDimension,
} from '../utils/helpers';
import {
  defaultDataScaleToSecondsPerPx,
  defaultAnimationDelayAtStart,
} from '../../../components/TimeSlider/TimeSliderUtils';

interface CreateMapProps {
  id: string;
  isAnimating?: boolean;
  animationStartTime?: string;
  animationEndTime?: string;
  isAutoUpdating?: boolean;
  srs?: string;
  bbox?: Bbox;
  mapLayers?: string[];
  baseLayers?: string[];
  overLayers?: string[];
  featureLayers?: string[];
  dimensions?: Dimension[];
  activeLayerId?: string;
  timeSliderScale?: Scale;
  timeStep?: number;
  animationDelay?: number;
  timeSliderCenterTime?: number;
  timeSliderSecondsPerPx?: number;
  timeSliderDataScaleToSecondsPerPx?: number;
  isTimestepAuto?: boolean;
  isTimeSliderHoverOn?: boolean;
  isTimeSliderVisible?: boolean;
  activeMapPresetId?: string;
  displayMapPin?: boolean;
  disableMapPin?: boolean;
  shouldShowZoomControls?: boolean;
}

export const createMap = ({
  id,
  isAnimating = false,
  animationStartTime = moment
    .utc(moment().unix() * 1000)
    .subtract(5, 'h')
    .format(dateFormat),
  animationEndTime = moment
    .utc(moment().unix() * 1000)
    .subtract(10, 'm')
    .format(dateFormat),
  isAutoUpdating = false,
  bbox = {
    left: -19000000,
    bottom: -19000000,
    right: 19000000,
    top: 19000000,
  },
  srs = 'EPSG:3857',
  baseLayers = [],
  overLayers = [],
  mapLayers = [],
  featureLayers = [],
  dimensions = [],
  activeLayerId = '',
  timeSliderScale = Scale.Hour,
  timeStep = 5,
  animationDelay = defaultAnimationDelayAtStart,
  timeSliderCenterTime = moment.utc().unix(),
  timeSliderSecondsPerPx = 30,
  timeSliderDataScaleToSecondsPerPx = defaultDataScaleToSecondsPerPx,
  isTimestepAuto = true,
  isTimeSliderHoverOn = false,
  isTimeSliderVisible = true,
  activeMapPresetId = null,
  displayMapPin = false,
  disableMapPin = false,
  shouldShowZoomControls = true,
}: CreateMapProps): WebMap => ({
  id,
  isAnimating,
  animationStartTime,
  animationEndTime,
  isAutoUpdating,
  srs,
  bbox,
  baseLayers,
  overLayers,
  mapLayers,
  featureLayers,
  dimensions,
  activeLayerId,
  timeSliderScale,
  timeStep,
  animationDelay,
  timeSliderCenterTime,
  timeSliderSecondsPerPx,
  timeSliderDataScaleToSecondsPerPx,
  isTimestepAuto,
  isTimeSliderHoverOn,
  isTimeSliderVisible,
  activeMapPresetId,
  displayMapPin,
  disableMapPin,
  shouldShowZoomControls,
});

export const checkValidLayersPayload = (
  layers: Layer[],
  mapId: string,
): boolean => {
  /* Check for duplicate ids */
  const layerIds = [];
  for (let i = 0; i < layers.length; i += 1) {
    if (layers[i].id) {
      /* Check if layer is already added to a different map */
      if (layers[i].mapId && mapId && layers[i].mapId !== mapId) {
        return false;
      }
      /* Check duplicate */
      if (!layerIds[layers[i].id]) {
        layerIds[layers[i].id] = true;
      } else {
        return false;
      }
    }
  }
  return true;
};

/**
 * This will get the map from the map draftstate.
 * If the mapId is not found, it registers one and returns it.
 * @param mapId The mapID
 * @param draft Draft map state
 */
export const getDraftMapById = (mapId: string, draft: WebMapState): WebMap => {
  const map = draft.byId[mapId];
  if (map) return map;
  if (!draft.allIds.includes(mapId)) {
    draft.byId[mapId] = createMap({ id: mapId } as WebMap);
    draft.allIds.push(mapId);
  }
  return draft.byId[mapId];
};

/**
 * Sets the map dimension in the state.
 * It will add dimensions to the map if they are missing.
 * If will update the existing dimensions if overwriteCurrentValue is set to true
 * @param draft The map draft state
 * @param mapId The mapId to update the dimensions for
 * @param dimensionFromAction  The dimension from the action
 * @param overwriteCurrentValue True to overwrite existing value. False to add a new dimension if one is not there yet.
 */
export const produceDraftStateSetWebMapDimension = (
  draft: WebMapState,
  mapId: string,
  dimensionFromAction: Dimension,
  overwriteCurrentValue: boolean,
): void => {
  const map = getDraftMapById(mapId, draft);
  if (dimensionFromAction) {
    if (!map.dimensions) map.dimensions = [];
    const { dimensions } = map;
    const mapDim = dimensions.find(
      (dim) => dim.name === dimensionFromAction.name,
    );
    if (mapDim) {
      if (overwriteCurrentValue) {
        mapDim.currentValue = dimensionFromAction.currentValue;
      }
    } else {
      dimensions.push({
        name: dimensionFromAction.name,
        currentValue: dimensionFromAction.currentValue,
      });
    }
  }
};

/**
 * Find the mapId belonging to a layerId
 * @param draft The WebMapState containing the state of all maps.
 * @param layerId The layer Id to find in the maps
 */
export const findMapIdFromLayerId = (
  draft: WebMapState,
  layerId: string,
): string => {
  for (let i = 0; i < draft.allIds.length; i += 1) {
    const mapId = draft.allIds[i];
    const layerIds = draft.byId[mapId].mapLayers;
    for (let l = 0; l < layerIds.length; l += 1) {
      const layerIdFromMap = layerIds[l];
      if (layerIdFromMap === layerId) {
        return mapId;
      }
    }
  }
  return null;
};

/* 
    When a layer dimension is changed, it can affect the map dimension if the layer dimension is linked with the map. 
    We need to find out from the layerId to which map it is coupled, and then adjust the map dimension 
  */

export const produceDraftStateSetMapDimensionFromLayerChangeDimension = (
  draft: WebMapState,
  layerId: string,
  dimension: Dimension,
): void => {
  const wmjsDimension = getWMJSDimensionForLayerAndDimension(
    layerId,
    dimension.name,
  );
  if (!wmjsDimension) {
    return;
  }
  /* If the layer dimension is not linked with the map, we should not update the map dimension */
  if (!wmjsDimension.linked) {
    return;
  }
  const mapId = findMapIdFromLayerId(draft, layerId);
  if (!mapId) {
    return;
  }

  produceDraftStateSetWebMapDimension(draft, mapId, dimension, true);
};

/**
 * Returns array with new order of swapped elements
 * @param array Array with ids
 * @param oldIndex Old index of element in array
 * @param newIndex New index of element in array
 */
export function moveArrayElements(
  array: string[],
  oldIndex: number,
  newIndex: number,
): string[] {
  const newArray = [...array];
  const indexNew = newIndex >= newArray.length ? newArray.length - 1 : newIndex;
  newArray.splice(indexNew, 0, newArray.splice(oldIndex, 1)[0]);
  return newArray;
}
