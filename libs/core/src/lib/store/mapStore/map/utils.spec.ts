/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { moveArrayElements } from './utils';

describe('store/mapStore/map/utils', () => {
  describe('moveArrayElements', () => {
    it('should move array elements', () => {
      const testArray = ['test-1', 'test-2', 'test-3'];
      expect(moveArrayElements(testArray, 0, 1)).toEqual([
        'test-2',
        'test-1',
        'test-3',
      ]);
      expect(moveArrayElements(testArray, 0, 2)).toEqual([
        'test-2',
        'test-3',
        'test-1',
      ]);
      expect(moveArrayElements(testArray, 1, 0)).toEqual([
        'test-2',
        'test-1',
        'test-3',
      ]);
      expect(moveArrayElements(testArray, 1, 2)).toEqual([
        'test-1',
        'test-3',
        'test-2',
      ]);
      expect(moveArrayElements(testArray, 2, 0)).toEqual([
        'test-3',
        'test-1',
        'test-2',
      ]);
      expect(moveArrayElements(testArray, 2, 1)).toEqual([
        'test-1',
        'test-3',
        'test-2',
      ]);
    });
  });
});
