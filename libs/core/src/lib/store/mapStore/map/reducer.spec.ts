/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import { WMJSMap, WMLayer } from '@opengeoweb/webmap';
import { createMap } from './utils';
import {
  reducer as mapReducer,
  initialState,
  checkIfMapLayerIdIsAlreadyTaken,
  mapActions,
} from './reducer';
import { layerActions } from '../layers/reducer';
import * as syncActions from '../../generic/synchronizationActions/actions';
import { LayerType, Scale, WebMap, WebMapState } from '../types';
import { LayerActionOrigin } from '../layers/types';
import {
  createWebmapState,
  createMapDimensionsState,
} from '../../../utils/testUtils';
import { dateFormat, registerWMJSMap, registerWMLayer } from '../utils/helpers';
import {
  scaleToSecondsPerPx,
  defaultDataScaleToSecondsPerPx,
  defaultAnimationDelayAtStart,
  defaultTimeStep,
} from '../../../components/TimeSlider/TimeSliderUtils';
import { animationIntervalDefault } from '../../../components/TimeSlider/TimeSliderButtons/PlayButton/PlayButtonConnect';
import { uiActions } from '../../ui';

describe('store/mapStore/map/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(mapReducer(undefined, {})).toEqual(initialState);
  });

  describe('registerMap', () => {
    const date = `2022-01-20T14:30:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(date).valueOf());
    it('should register a map if no state is passed', () => {
      const mapId = 'test-1';
      const result = mapReducer(undefined, mapActions.registerMap({ mapId }));
      const map = createMap({ id: mapId });

      expect(result.allIds).toHaveLength(1);
      expect(result.byId[mapId]).toEqual(map);
      expect(result.allIds.includes(mapId)).toBeTruthy();
    });

    it('should register a map  if current state passed', () => {
      const mapId1 = 'test-1';
      const mapIdMock = 'test-2';
      const mockState1 = createWebmapState(mapIdMock);

      // initial state
      expect(mockState1.allIds).toHaveLength(1);
      const map = createMap({ id: mapIdMock });

      expect(mockState1.byId[mapIdMock]).toEqual(map);
      expect(mockState1.allIds.includes(mapIdMock)).toBeTruthy();

      const result = mapReducer(
        mockState1,
        mapActions.registerMap({ mapId: mapId1 }),
      );

      expect(result.allIds).toHaveLength(2);
      expect(result.byId[mapIdMock]).toEqual(map);
      expect(result.allIds.includes(mapId1)).toBeTruthy();
    });
  });

  describe('unregisterMap', () => {
    it('should return empty map state if no state is passed', () => {
      const mapId = 'test-1';
      const result = mapReducer(undefined, mapActions.unregisterMap({ mapId }));

      expect(result.allIds).toHaveLength(0);
      expect(result.allIds.includes(mapId)).toBeFalsy();
      expect(result.byId[mapId]).toBeUndefined();
      expect(result.byId).toEqual({});
    });

    it('should unregister a map  if current state passed', () => {
      const date = `2022-01-15T14:30:00Z`;
      jest.spyOn(Date, 'now').mockReturnValue(new Date(date).valueOf());
      const mapId1 = 'test-1';
      const mapId2 = 'test-2';
      const mockState = createWebmapState(mapId1, mapId2);

      // inital state
      expect(mockState.allIds).toHaveLength(2);
      const map1 = createMap({ id: mapId1 });
      const map2 = createMap({ id: mapId2 });

      expect(mockState.byId[mapId2]).toEqual(map2);
      expect(mockState.byId[mapId1]).toEqual(map1);
      expect(mockState.allIds.includes(mapId2)).toBeTruthy();
      expect(mockState.allIds.includes(mapId1)).toBeTruthy();

      const result = mapReducer(
        mockState,
        mapActions.unregisterMap({ mapId: mapId2 }),
      );

      expect(result.allIds).toHaveLength(1);
      expect(result.allIds.includes(mapId2)).toBeFalsy();
      expect(result.allIds.includes(mapId1)).toBeTruthy();
      expect(result.byId[mapId2]).toBeUndefined();

      // remove the last map - ensure empty state
      const result2 = mapReducer(
        result,
        mapActions.unregisterMap({ mapId: mapId1 }),
      );

      expect(result2.allIds).toHaveLength(0);
      expect(result2.allIds.includes(mapId1)).toBeFalsy();
      expect(result2.byId[mapId1]).toBeUndefined();
    });
  });

  describe('addLayer', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const layer = { name: 'testLayer', layerType: LayerType.mapLayer };
    const layerId1 = 'test-layer-1';
    const layerId2 = 'test-layer-2';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        layerActions.addLayer({
          layerId: layerId1,
          mapId: mapId1,
          layer,
          origin: 'reducer.spec',
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        layerActions.addLayer({
          layerId: layerId1,
          mapId: 'invalidMapId',
          layer,
          origin: 'reducer.spec',
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should return passed state if layerId already exists', () => {
      jest.spyOn(console, 'warn').mockImplementation();
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].mapLayers.push(layerId1);

      const result = mapReducer(
        mockState,
        layerActions.addLayer({
          layerId: layerId1,
          mapId: mapId1,
          layer,
          origin: 'reducer.spec',
        }),
      );

      expect(result).toEqual(mockState);
      expect(console.warn).toHaveBeenCalled();
    });

    it('should add layer to mapId passed', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].mapLayers.push(layerId1);

      // initial state
      expect(mockState.allIds).toHaveLength(2);
      expect(mockState.byId[mapId1].mapLayers.includes(layerId1)).toBeTruthy();
      expect(mockState.byId[mapId1].mapLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].mapLayers).toHaveLength(0);

      const result = mapReducer(
        mockState,
        layerActions.addLayer({
          layerId: layerId2,
          mapId: mapId1,
          layer,
          origin: 'reducer.spec',
        }),
      );

      expect(result.byId[mapId1].mapLayers.includes(layerId1)).toBeTruthy();
      expect(result.byId[mapId1].mapLayers.includes(layerId2)).toBeTruthy();
      // ensure it has been added to the top of the layers array
      expect(result.byId[mapId1].mapLayers[0]).toEqual(layerId2);
      // make sure it has only been added to the correct map
      expect(result.byId[mapId2].mapLayers).toHaveLength(0);
    });

    it('should set activeLayerId if it is first layer', () => {
      const mockState = createWebmapState(mapId1);
      const result = mapReducer(
        mockState,
        layerActions.addLayer({
          layerId: layerId1,
          mapId: mapId1,
          layer,
          origin: 'reducer.spec',
        }),
      );

      expect(result.byId[mapId1].activeLayerId).toEqual(layerId1);
    });

    it('should not update activeLayerId if already active layer on map', () => {
      const mockState = mapReducer(
        createWebmapState(mapId1),
        layerActions.addLayer({
          layerId: layerId1,
          mapId: mapId1,
          layer,
          origin: 'reducer.spec',
        }),
      );

      const result = mapReducer(
        mockState,
        layerActions.addLayer({
          layerId: layerId2,
          mapId: mapId1,
          layer,
          origin: 'reducer.spec',
        }),
      );

      expect(result.byId[mapId1].activeLayerId).toEqual(layerId1);
    });
  });

  describe('setBbox', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const bbox1 = { left: 1, right: 1, top: 1, bottom: 1 };
    const srs1 = 'srs-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setBbox({
          mapId: mapId1,
          bbox: bbox1,
          srs: srs1,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setBbox({
          mapId: 'fakeMap',
          bbox: bbox1,
          srs: srs1,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should not update srs if not passed', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setBbox({
          mapId: mapId1,
          bbox: bbox1,
        }),
      );

      expect(result.byId[mapId1].srs).toEqual(mockState.byId[mapId1].srs);
    });

    it('should update srs if passed', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setBbox({
          mapId: mapId1,
          bbox: bbox1,
          srs: srs1,
        }),
      );

      expect(result.byId[mapId1].srs).toEqual(srs1);
      expect(result.byId[mapId2].srs).toEqual(mockState.byId[mapId2].srs);
    });

    it('should set bbox', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setBbox({
          mapId: mapId1,
          bbox: bbox1,
          srs: srs1,
        }),
      );

      expect(result.byId[mapId1].bbox).toEqual(bbox1);
      expect(result.byId[mapId1].srs).toEqual(srs1);
      expect(result.byId[mapId2].bbox).toEqual(mockState.byId[mapId2].bbox);
      expect(result.byId[mapId2].srs).toEqual(mockState.byId[mapId2].srs);
    });
  });

  describe('mapUpdateAllMapDimensions', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const dimensions1 = [
      {
        name: 'nameDim1',
        units: 'unitDim1',
        currentValue: 'curValueDim',
      },
      {
        name: 'nameDim2',
        units: 'unitDim2',
        currentValue: 'curValueDim2',
      },
    ];

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.mapUpdateAllMapDimensions({
          origin: 'reducer.spec.ts',
          mapId: mapId1,
          dimensions: dimensions1,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.mapUpdateAllMapDimensions({
          origin: 'reducer.spec.ts',
          mapId: 'invalidMapId',
          dimensions: dimensions1,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should update all dimensions', () => {
      const dimensionsNew = [
        {
          name: 'nameDimNew',
          units: 'unitDimNew',
          currentValue: 'curValueDimNew',
        },
      ];

      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].dimensions.push(dimensions1[0]);
      mockState.byId[mapId1].dimensions.push(dimensions1[1]);

      // initial state
      expect(mockState.byId[mapId1].dimensions).toHaveLength(2);
      expect(
        mockState.byId[mapId1].dimensions.includes(dimensions1[0]),
      ).toBeTruthy();
      expect(
        mockState.byId[mapId1].dimensions.includes(dimensions1[1]),
      ).toBeTruthy();
      expect(mockState.byId[mapId2].dimensions).toHaveLength(0);

      const result = mapReducer(
        mockState,
        mapActions.mapUpdateAllMapDimensions({
          origin: 'reducer.spec.ts',
          mapId: mapId1,
          dimensions: dimensionsNew,
        }),
      );

      expect(result.byId[mapId1].dimensions).toHaveLength(1);
      expect(
        result.byId[mapId1].dimensions.find(
          (dim) => dim.name === dimensionsNew[0].name,
        ).currentValue === dimensionsNew[0].currentValue,
      ).toBeTruthy();
      expect(result.byId[mapId2].dimensions).toEqual(
        mockState.byId[mapId2].dimensions,
      );
    });
  });

  describe('mapChangeDimension', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const dimensions1 = [
      {
        name: 'nameDim1',
        units: 'unitDim1',
        currentValue: 'curValueDim',
      },
      {
        name: 'nameDim2',
        units: 'unitDim2',
        currentValue: 'curValueDim2',
      },
    ];
    const dimensionsNew = {
      name: 'nameDim1',
      units: 'unitDim1',
      currentValue: 'curValueDimNew',
    };

    it('should register map if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.mapChangeDimension({
          origin: 'reducer.spec.ts',
          mapId: mapId1,
          dimension: dimensionsNew,
        }),
      );
      expect(result.allIds.includes(mapId1)).toBeTruthy();
    });

    it('should register map if mapId is unknown', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      const result = mapReducer(
        mockState,
        mapActions.mapChangeDimension({
          origin: 'reducer.spec.ts',
          mapId: 'unknownMapId',
          dimension: dimensionsNew,
        }),
      );
      expect(result.allIds.includes('unknownMapId')).toBeTruthy();
    });

    it('should update dimension for passed mapId', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].dimensions.push(dimensions1[0]);
      mockState.byId[mapId1].dimensions.push(dimensions1[1]);

      // initial state
      expect(mockState.byId[mapId1].dimensions).toHaveLength(2);
      expect(
        mockState.byId[mapId1].dimensions.includes(dimensions1[0]),
      ).toBeTruthy();
      expect(
        mockState.byId[mapId1].dimensions.includes(dimensions1[1]),
      ).toBeTruthy();
      expect(mockState.byId[mapId2].dimensions).toHaveLength(0);

      const result = mapReducer(
        mockState,
        mapActions.mapChangeDimension({
          origin: 'reducer.spec.ts',
          mapId: mapId1,
          dimension: dimensionsNew,
        }),
      );

      expect(mockState.byId[mapId1].dimensions).toHaveLength(2);
      expect(result.byId[mapId1].dimensions[0]).toEqual(dimensionsNew);
      expect(result.byId[mapId1].dimensions[1]).toEqual(dimensions1[1]);
      expect(result.byId[mapId2].dimensions).toEqual(
        mockState.byId[mapId2].dimensions,
      );
    });

    it('should not update dimension units for passed dimension and mapId', () => {
      const dimensionsNew2 = {
        name: 'nameDim1',
        units: 'unitDimNew',
        currentValue: 'curValueDimNew',
      };
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].dimensions.push(dimensions1[0]);
      mockState.byId[mapId1].dimensions.push(dimensions1[1]);

      // initial state
      expect(mockState.byId[mapId1].dimensions).toHaveLength(2);
      expect(
        mockState.byId[mapId1].dimensions.includes(dimensions1[0]),
      ).toBeTruthy();
      expect(
        mockState.byId[mapId1].dimensions.includes(dimensions1[1]),
      ).toBeTruthy();
      expect(mockState.byId[mapId2].dimensions).toHaveLength(0);

      const result = mapReducer(
        mockState,
        mapActions.mapChangeDimension({
          origin: 'reducer.spec.ts',
          mapId: mapId1,
          dimension: dimensionsNew2,
        }),
      );

      expect(mockState.byId[mapId1].dimensions).toHaveLength(2);
      expect(result.byId[mapId1].dimensions[0].units).toEqual(
        dimensions1[0].units,
      );
      expect(result.byId[mapId1].dimensions[0].currentValue).toEqual(
        dimensionsNew2.currentValue,
      );
      expect(result.byId[mapId1].dimensions[1]).toEqual(dimensions1[1]);
      expect(result.byId[mapId2].dimensions).toEqual(
        mockState.byId[mapId2].dimensions,
      );
    });
  });

  describe('mapStartAnimation', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';

    const startTime = moment.utc().format(dateFormat);
    const endTime = moment.utc().add('1', 'hour').format(dateFormat);

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.mapStartAnimation({
          mapId: mapId1,
          start: startTime,
          end: endTime,
          interval: 1000,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.mapStartAnimation({
          mapId: 'invalidMapId',
          start: startTime,
          end: endTime,
          interval: 1000,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set isAnimating to true', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.mapStartAnimation({
          mapId: mapId1,
          start: startTime,
          end: endTime,
          interval: 1000,
        }),
      );

      expect(result.byId[mapId1].isAnimating).toBeTruthy();
      expect(result.byId[mapId2].isAnimating).toBeFalsy();
    });

    it('should set a time dimension of map to a start value correctly', () => {
      const startTime2 = moment.utc().subtract(2, 'hours').format(dateFormat);
      const dimensions = [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: startTime2,
        },
      ];
      const mockState = createMapDimensionsState(dimensions, mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.mapStartAnimation({
          mapId: mapId1,
          start: startTime2,
          end: endTime,
          interval: 1000,
        }),
      );
      const currentTimeVal = result.byId[mapId1].dimensions.find(
        (dim) => dim.name === 'time',
      ).currentValue;
      expect(currentTimeVal).toEqual(startTime2);
    });
  });

  describe('mapStopAnimation', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.mapStopAnimation({
          mapId: mapId1,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.mapStopAnimation({
          mapId: 'invalidMapId',
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set isAnimating to false', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].isAnimating = true;

      // initial state
      expect(mockState.byId[mapId1].isAnimating).toBeTruthy();

      const result = mapReducer(
        mockState,
        mapActions.mapStopAnimation({
          mapId: mapId1,
        }),
      );

      expect(result.byId[mapId1].isAnimating).toBeFalsy();
      expect(result.byId[mapId2].isAnimating).toBeFalsy();
    });
  });

  describe('addBaseLayer', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const overlayer = { name: 'testLayer1', layerType: LayerType.overLayer };
    const overlayerId1 = 'test-layer-1';
    const baselayer = { name: 'testLayer2', layerType: LayerType.baseLayer };
    const baselayerId1 = 'test-layer-2';
    const overlayer2 = { name: 'testLayer3', layerType: LayerType.overLayer };
    const overlayerId2 = 'test-layer-3';
    const baselayer2 = { name: 'testLayer4', layerType: LayerType.baseLayer };
    const baselayerId2 = 'test-layer-4';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        layerActions.addBaseLayer({
          layerId: overlayerId1,
          mapId: mapId1,
          layer: overlayer,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        layerActions.addBaseLayer({
          layerId: overlayerId1,
          mapId: 'invalidMapId',
          layer: overlayer,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should return passed state if overlayer layerId already exists', () => {
      jest.spyOn(console, 'warn').mockImplementation();

      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].overLayers.push(overlayerId1);

      const result = mapReducer(
        mockState,
        layerActions.addBaseLayer({
          layerId: overlayerId1,
          mapId: mapId1,
          layer: overlayer,
        }),
      );

      expect(result).toEqual(mockState);
      expect(console.warn).toHaveBeenCalled();
    });

    it('should return passed state if baselayer layerId already exists', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].baseLayers.push(baselayerId1);

      const result = mapReducer(
        mockState,
        layerActions.addBaseLayer({
          layerId: baselayerId1,
          mapId: mapId1,
          layer: baselayer,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should add overlayer to mapId passed', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].overLayers.push(overlayerId1);

      // initial state
      expect(mockState.allIds).toHaveLength(2);
      expect(
        mockState.byId[mapId1].overLayers.includes(overlayerId1),
      ).toBeTruthy();
      expect(mockState.byId[mapId1].overLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].overLayers).toHaveLength(0);

      const result = mapReducer(
        mockState,
        layerActions.addBaseLayer({
          layerId: overlayerId2,
          mapId: mapId1,
          layer: overlayer2,
        }),
      );

      expect(
        result.byId[mapId1].overLayers.includes(overlayerId2),
      ).toBeTruthy();
      expect(
        result.byId[mapId1].overLayers.includes(overlayerId2),
      ).toBeTruthy();
      // ensure it has been added to the top of the layers array
      expect(result.byId[mapId1].overLayers[0]).toEqual(overlayerId2);
      // make sure it has only been added to the correct map
      expect(result.byId[mapId2].overLayers).toHaveLength(0);
    });

    it('should add baselayer to mapId passed', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].baseLayers.push(baselayerId1);

      // initial state
      expect(mockState.allIds).toHaveLength(2);
      expect(
        mockState.byId[mapId1].baseLayers.includes(baselayerId1),
      ).toBeTruthy();
      expect(mockState.byId[mapId1].baseLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].baseLayers).toHaveLength(0);

      const result = mapReducer(
        mockState,
        layerActions.addBaseLayer({
          layerId: baselayerId2,
          mapId: mapId1,
          layer: baselayer2,
        }),
      );

      expect(
        result.byId[mapId1].baseLayers.includes(baselayerId2),
      ).toBeTruthy();
      expect(
        result.byId[mapId1].baseLayers.includes(baselayerId2),
      ).toBeTruthy();
      // ensure it has been added to the top of the layers array
      expect(result.byId[mapId1].baseLayers[0]).toEqual(baselayerId2);
      // make sure it has only been added to the correct map
      expect(result.byId[mapId2].baseLayers).toHaveLength(0);
    });

    it('should return state if wrong layerType passed in', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].baseLayers.push(baselayerId1);
      mockState.byId[mapId1].overLayers.push(overlayerId1);

      // initial state
      expect(mockState.allIds).toHaveLength(2);
      expect(
        mockState.byId[mapId1].baseLayers.includes(baselayerId1),
      ).toBeTruthy();
      expect(mockState.byId[mapId1].baseLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].baseLayers).toHaveLength(0);
      expect(
        mockState.byId[mapId1].overLayers.includes(overlayerId1),
      ).toBeTruthy();
      expect(mockState.byId[mapId1].overLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].overLayers).toHaveLength(0);

      const result = mapReducer(
        mockState,
        layerActions.addBaseLayer({
          layerId: 'newLayerId',
          mapId: mapId1,
          layer: { layerType: LayerType.mapLayer },
        }),
      );

      expect(result.allIds).toHaveLength(2);
      expect(
        result.byId[mapId1].baseLayers.includes(baselayerId1),
      ).toBeTruthy();
      expect(result.byId[mapId1].baseLayers).toHaveLength(1);
      expect(result.byId[mapId2].baseLayers).toHaveLength(0);
      expect(
        result.byId[mapId1].overLayers.includes(overlayerId1),
      ).toBeTruthy();
      expect(result.byId[mapId1].overLayers).toHaveLength(1);
      expect(result.byId[mapId2].overLayers).toHaveLength(0);
    });
  });

  describe('setLayers', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const layerId1 = 'layer-origin';
    const layers = [
      { name: 'layer-2-name', id: 'layer-2-id', layerType: LayerType.mapLayer },
      { name: 'layer-3-name', id: 'layer-3-id', layerType: LayerType.mapLayer },
    ];

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        layerActions.setLayers({
          mapId: mapId1,
          layers,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        layerActions.setLayers({
          mapId: 'invalidMapId',
          layers,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set layers in layerId array', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].mapLayers.push(layerId1);

      // initial state
      expect(mockState.byId[mapId1].mapLayers.includes(layerId1)).toBeTruthy();
      expect(mockState.byId[mapId1].mapLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].mapLayers).toHaveLength(0);

      const result = mapReducer(
        mockState,
        layerActions.setLayers({
          mapId: mapId1,
          layers,
        }),
      );

      expect(result.byId[mapId1].mapLayers.includes(layers[0].id)).toBeTruthy();
      expect(result.byId[mapId1].mapLayers.includes(layers[1].id)).toBeTruthy();
      expect(result.byId[mapId1].mapLayers.includes(layerId1)).toBeFalsy();
      expect(result.byId[mapId1].mapLayers).toHaveLength(2);
      expect(result.byId[mapId2].mapLayers).toHaveLength(0);
    });

    it('should generate layerIds if not passed', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].mapLayers.push(layerId1);

      // initial state
      expect(mockState.byId[mapId1].mapLayers.includes(layerId1)).toBeTruthy();
      expect(mockState.byId[mapId1].mapLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].mapLayers).toHaveLength(0);

      const result = mapReducer(
        mockState,
        layerActions.setLayers({
          mapId: mapId1,
          layers: [
            { name: 'layer-3-name', layerType: LayerType.mapLayer },
            { name: 'layer-4-name', layerType: LayerType.mapLayer },
          ],
        }),
      );

      expect(result.byId[mapId1].mapLayers.includes(layerId1)).toBeFalsy();
      expect(result.byId[mapId1].mapLayers[0]).not.toEqual(
        result.byId[mapId1].mapLayers[1],
      );
      expect(result.byId[mapId1].mapLayers).toHaveLength(2);
      expect(result.byId[mapId2].mapLayers).toHaveLength(0);
    });

    it('should set activeLayerId on the first layer', () => {
      const mockState = createWebmapState(mapId1);
      const mockLayers = [
        { name: 'layer-1-name', id: 'layer-1', layerType: LayerType.mapLayer },
        { name: 'layer-2-name', id: 'layer-2', layerType: LayerType.mapLayer },
      ];

      const result = mapReducer(
        mockState,
        layerActions.setLayers({
          mapId: mapId1,
          layers: mockLayers,
        }),
      );

      expect(result.byId[mapId1].activeLayerId).toEqual(mockLayers[0].id);
    });

    it('should set the map dimensions', () => {
      const mockState = createWebmapState(mapId1);
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      const mockDimension = {
        name: 'namedim1',
        currentValue: 'curValueDim1',
      };

      wmjsmap.setDimension(
        mockDimension.name,
        mockDimension.currentValue,
        false,
      );
      registerWMJSMap(wmjsmap, mapId1);

      // initial state
      expect(mockState.byId[mapId1].dimensions).toEqual([]);

      const mockLayers = [
        {
          name: 'layer-1-name',
          id: 'layer-1-id',
          layerType: LayerType.mapLayer,
        },
      ];

      const result = mapReducer(
        mockState,
        layerActions.setLayers({
          mapId: mapId1,
          layers: mockLayers,
        }),
      );

      expect(result.byId[mapId1].dimensions).toEqual([
        {
          ...mockDimension,
          units: undefined,
        },
      ]);
    });
  });

  describe('setBaseLayers', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const baseLayerId1 = 'baseLayer-origin';
    const overLayerId1 = 'overLayer-origin';
    const overLayerId2 = 'overLayer-origin-2';
    const baseLayers = [
      {
        name: 'baseLayer-2-name',
        id: 'baseLayer-2-id',
        layerType: LayerType.baseLayer,
      },
      {
        name: 'baseLayer-3-name',
        id: 'baseLayer-3-id',
        layerType: LayerType.baseLayer,
      },
      {
        name: 'overLayer-2-name',
        id: 'overLayer-2-id',
        layerType: LayerType.overLayer,
      },
    ];

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        layerActions.setBaseLayers({
          mapId: mapId1,
          layers: baseLayers,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        layerActions.setBaseLayers({
          mapId: 'invalidMapId',
          layers: baseLayers,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set baselayers in baselayerId and overlayerId array', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].baseLayers.push(baseLayerId1);
      mockState.byId[mapId1].overLayers.push(overLayerId1);
      mockState.byId[mapId1].overLayers.push(overLayerId2);

      // initial state
      expect(
        mockState.byId[mapId1].baseLayers.includes(baseLayerId1),
      ).toBeTruthy();
      expect(mockState.byId[mapId1].baseLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].baseLayers).toHaveLength(0);
      expect(
        mockState.byId[mapId1].overLayers.includes(overLayerId1),
      ).toBeTruthy();
      expect(
        mockState.byId[mapId1].overLayers.includes(overLayerId2),
      ).toBeTruthy();
      expect(mockState.byId[mapId1].overLayers).toHaveLength(2);
      expect(mockState.byId[mapId2].overLayers).toHaveLength(0);

      const result = mapReducer(
        mockState,
        layerActions.setBaseLayers({
          mapId: mapId1,
          layers: baseLayers,
        }),
      );

      expect(
        result.byId[mapId1].baseLayers.includes(baseLayers[0].id),
      ).toBeTruthy();
      expect(
        result.byId[mapId1].baseLayers.includes(baseLayers[1].id),
      ).toBeTruthy();
      expect(
        result.byId[mapId1].overLayers.includes(baseLayers[2].id),
      ).toBeTruthy();
      expect(result.byId[mapId1].baseLayers.includes(baseLayerId1)).toBeFalsy();
      expect(result.byId[mapId1].overLayers.includes(overLayerId1)).toBeFalsy();
      expect(result.byId[mapId1].baseLayers).toHaveLength(2);
      expect(result.byId[mapId2].baseLayers).toHaveLength(0);
      expect(result.byId[mapId1].overLayers).toHaveLength(1);
      expect(result.byId[mapId2].overLayers).toHaveLength(0);
    });

    it('should generate layerIds if not passed', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].baseLayers.push(baseLayerId1);
      mockState.byId[mapId1].overLayers.push(overLayerId1);
      mockState.byId[mapId1].overLayers.push(overLayerId2);

      const result = mapReducer(
        mockState,
        layerActions.setBaseLayers({
          mapId: mapId1,
          layers: [
            { name: 'layer-3-name', layerType: LayerType.baseLayer },
            { name: 'layer-4-name', layerType: LayerType.baseLayer },
            { name: 'layer-5-name', layerType: LayerType.overLayer },
          ],
        }),
      );

      expect(result.byId[mapId1].baseLayers.includes(baseLayerId1)).toBeFalsy();
      expect(result.byId[mapId1].baseLayers[0]).not.toEqual(
        result.byId[mapId1].baseLayers[1],
      );
      expect(result.byId[mapId1].overLayers.includes(overLayerId1)).toBeFalsy();
      expect(result.byId[mapId1].overLayers.includes(overLayerId2)).toBeFalsy();
      expect(result.byId[mapId1].baseLayers).toHaveLength(2);
      expect(result.byId[mapId2].baseLayers).toHaveLength(0);
      expect(result.byId[mapId1].overLayers).toHaveLength(1);
      expect(result.byId[mapId2].overLayers).toHaveLength(0);
    });

    it('should generate ignore any layers passed that have a different layer type than baseLayer and overLayer', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].baseLayers.push(baseLayerId1);
      mockState.byId[mapId1].overLayers.push(overLayerId1);
      mockState.byId[mapId1].overLayers.push(overLayerId2);

      const result = mapReducer(
        mockState,
        layerActions.setBaseLayers({
          mapId: mapId1,
          layers: [{ name: 'layer-3-name', layerType: LayerType.mapLayer }],
        }),
      );

      expect(
        result.byId[mapId1].baseLayers.includes(baseLayerId1),
      ).toBeTruthy();
      expect(result.byId[mapId1].baseLayers).toHaveLength(1);
      expect(result.byId[mapId2].baseLayers).toHaveLength(0);
      expect(
        result.byId[mapId1].overLayers.includes(overLayerId1),
      ).toBeTruthy();
      expect(
        result.byId[mapId1].overLayers.includes(overLayerId2),
      ).toBeTruthy();
      expect(result.byId[mapId2].mapLayers).toHaveLength(0);
    });
  });

  describe('layerDelete', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const layerId1 = 'layer-1';
    const layerId2 = 'layer-2';
    const layerId3 = 'layer-3';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        layerActions.layerDelete({
          mapId: mapId1,
          layerId: layerId1,
          layerIndex: 0,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        layerActions.layerDelete({
          mapId: 'invalidMapId',
          layerId: layerId1,
          layerIndex: 0,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should delete the correct layers', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].mapLayers.push(layerId1);
      mockState.byId[mapId1].mapLayers.push(layerId2);
      mockState.byId[mapId2].mapLayers.push(layerId3);

      // initial state
      expect(mockState.byId[mapId1].mapLayers.includes(layerId1)).toBeTruthy();
      expect(mockState.byId[mapId1].mapLayers.includes(layerId2)).toBeTruthy();
      expect(mockState.byId[mapId2].mapLayers.includes(layerId3)).toBeTruthy();
      expect(mockState.byId[mapId1].mapLayers).toHaveLength(2);
      expect(mockState.byId[mapId2].mapLayers).toHaveLength(1);

      const result = mapReducer(
        mockState,
        layerActions.layerDelete({
          mapId: mapId1,
          layerId: layerId2,
          layerIndex: 0,
        }),
      );

      expect(result.byId[mapId1].mapLayers.includes(layerId1)).toBeTruthy();
      expect(result.byId[mapId1].mapLayers.includes(layerId2)).toBeFalsy();
      expect(result.byId[mapId1].mapLayers.includes(layerId3)).toBeFalsy();
      expect(result.byId[mapId1].mapLayers).toHaveLength(1);
      expect(result.byId[mapId2].mapLayers).toHaveLength(1);
    });

    it('should update activeLayerId if layer with activeLayerId is deleted', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].mapLayers.push(layerId1);
      mockState.byId[mapId1].mapLayers.push(layerId2);
      mockState.byId[mapId1].mapLayers.push(layerId3);

      mockState.byId[mapId1].activeLayerId = layerId2;

      const result = mapReducer(
        mockState,
        layerActions.layerDelete({
          mapId: mapId1,
          layerId: layerId2,
          layerIndex: 0,
        }),
      );

      expect(result.byId[mapId1].activeLayerId).toEqual(layerId1);
    });

    it('should reset activeLayerId if layer with activeLayerId is deleted', () => {
      const mockState = createWebmapState(mapId1);
      mockState.byId[mapId1].mapLayers.push(layerId1);
      mockState.byId[mapId1].activeLayerId = layerId1;

      const result = mapReducer(
        mockState,
        layerActions.layerDelete({
          mapId: mapId1,
          layerId: layerId1,
          layerIndex: 0,
        }),
      );

      expect(result.byId[mapId1].activeLayerId).toEqual('');
    });
  });

  describe('baseLayerDelete', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const layerId1 = 'baseLayer-1';
    const layerId2 = 'baseLayer-2';
    const layerId3 = 'baseLayer-3';
    const layerId4 = 'overLayer-2';
    const layerId5 = 'overLayer-3';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        layerActions.baseLayerDelete({
          mapId: mapId1,
          layerId: layerId1,
          layerIndex: 0,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        layerActions.baseLayerDelete({
          mapId: 'invalidMapId',
          layerId: layerId1,
          layerIndex: 0,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should delete the correct layers', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].baseLayers.push(layerId1);
      mockState.byId[mapId1].baseLayers.push(layerId2);
      mockState.byId[mapId2].baseLayers.push(layerId3);
      mockState.byId[mapId1].overLayers.push(layerId4);
      mockState.byId[mapId2].overLayers.push(layerId5);

      // initial state
      expect(mockState.byId[mapId1].baseLayers.includes(layerId1)).toBeTruthy();
      expect(mockState.byId[mapId1].baseLayers.includes(layerId2)).toBeTruthy();
      expect(mockState.byId[mapId2].baseLayers.includes(layerId3)).toBeTruthy();
      expect(mockState.byId[mapId1].overLayers.includes(layerId4)).toBeTruthy();
      expect(mockState.byId[mapId2].overLayers.includes(layerId5)).toBeTruthy();
      expect(mockState.byId[mapId1].baseLayers).toHaveLength(2);
      expect(mockState.byId[mapId2].baseLayers).toHaveLength(1);
      expect(mockState.byId[mapId1].overLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].overLayers).toHaveLength(1);

      const result = mapReducer(
        mockState,
        layerActions.baseLayerDelete({
          mapId: mapId1,
          layerId: layerId2,
          layerIndex: 0,
        }),
      );

      expect(result.byId[mapId1].baseLayers.includes(layerId1)).toBeTruthy();
      expect(result.byId[mapId1].baseLayers.includes(layerId2)).toBeFalsy();
      expect(result.byId[mapId1].baseLayers.includes(layerId3)).toBeFalsy();
      expect(mockState.byId[mapId1].overLayers.includes(layerId4)).toBeTruthy();
      expect(mockState.byId[mapId2].overLayers.includes(layerId5)).toBeTruthy();
      expect(result.byId[mapId1].baseLayers).toHaveLength(1);
      expect(result.byId[mapId2].baseLayers).toHaveLength(1);
      expect(mockState.byId[mapId1].overLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].overLayers).toHaveLength(1);
    });

    it('should delete the correct layers', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].baseLayers.push(layerId2);
      mockState.byId[mapId2].baseLayers.push(layerId3);
      mockState.byId[mapId1].overLayers.push(layerId4);
      mockState.byId[mapId2].overLayers.push(layerId5);

      // initial state

      expect(mockState.byId[mapId1].baseLayers.includes(layerId2)).toBeTruthy();
      expect(mockState.byId[mapId2].baseLayers.includes(layerId3)).toBeTruthy();
      expect(mockState.byId[mapId1].overLayers.includes(layerId4)).toBeTruthy();
      expect(mockState.byId[mapId2].overLayers.includes(layerId5)).toBeTruthy();
      expect(mockState.byId[mapId1].baseLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].baseLayers).toHaveLength(1);
      expect(mockState.byId[mapId1].overLayers).toHaveLength(1);
      expect(mockState.byId[mapId2].overLayers).toHaveLength(1);

      const result = mapReducer(
        mockState,
        layerActions.baseLayerDelete({
          mapId: mapId1,
          layerId: layerId4,
          layerIndex: 0,
        }),
      );

      expect(result.byId[mapId1].baseLayers.includes(layerId2)).toBeTruthy();
      expect(result.byId[mapId2].baseLayers.includes(layerId3)).toBeTruthy();
      expect(result.byId[mapId1].overLayers.includes(layerId4)).toBeFalsy();
      expect(result.byId[mapId2].overLayers.includes(layerId5)).toBeTruthy();
      expect(result.byId[mapId1].baseLayers).toHaveLength(1);
      expect(result.byId[mapId2].baseLayers).toHaveLength(1);
      expect(result.byId[mapId1].overLayers).toHaveLength(0);
      expect(result.byId[mapId2].overLayers).toHaveLength(1);
    });
  });

  describe('layerMoveLayer', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.layerMoveLayer({
          mapId: mapId1,
          oldIndex: 0,
          newIndex: 1,
          origin: 'reducer.spec',
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.layerMoveLayer({
          mapId: 'invalidMapId',
          oldIndex: 0,
          newIndex: 1,
          origin: 'reducer.spec',
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should move a layer', () => {
      const layerId1 = 'layer-1';
      const layerId2 = 'layer-2';
      const layerId3 = 'layer-3';
      const layerId4 = 'layer-4';
      const layerId5 = 'layer-5';
      const layerId6 = 'layer-6';
      const layerId7 = 'layer-7';

      const mockState = createWebmapState(mapId1, mapId2);
      mockState.byId[mapId1].mapLayers.push(layerId1);
      mockState.byId[mapId1].mapLayers.push(layerId2);
      mockState.byId[mapId1].mapLayers.push(layerId3);
      mockState.byId[mapId1].mapLayers.push(layerId4);
      mockState.byId[mapId1].mapLayers.push(layerId5);
      mockState.byId[mapId2].mapLayers.push(layerId6);
      mockState.byId[mapId2].mapLayers.push(layerId7);

      // initial state
      expect(mockState.byId[mapId1].mapLayers[0]).toEqual(layerId1);
      expect(mockState.byId[mapId1].mapLayers[1]).toEqual(layerId2);
      expect(mockState.byId[mapId1].mapLayers[2]).toEqual(layerId3);
      expect(mockState.byId[mapId1].mapLayers[3]).toEqual(layerId4);
      expect(mockState.byId[mapId1].mapLayers[4]).toEqual(layerId5);
      expect(mockState.byId[mapId1].mapLayers).toHaveLength(5);

      expect(mockState.byId[mapId2].mapLayers[0]).toEqual(layerId6);
      expect(mockState.byId[mapId2].mapLayers[1]).toEqual(layerId7);
      expect(mockState.byId[mapId2].mapLayers).toHaveLength(2);

      // moving layerId3 to the next index
      const result = mapReducer(
        mockState,
        mapActions.layerMoveLayer({
          mapId: mapId1,
          oldIndex: 2,
          newIndex: 3,
          origin: 'reducer.spec',
        }),
      );

      // check that layerId3 got moved
      expect(result.byId[mapId1].mapLayers[0]).toEqual(layerId1);
      expect(result.byId[mapId1].mapLayers[1]).toEqual(layerId2);
      expect(result.byId[mapId1].mapLayers[2]).toEqual(layerId4);
      expect(result.byId[mapId1].mapLayers[3]).toEqual(layerId3);
      expect(result.byId[mapId1].mapLayers[4]).toEqual(layerId5);
      expect(result.byId[mapId1].mapLayers).toHaveLength(5);

      // check that the layers of mapId2 didn't change
      expect(result.byId[mapId2].mapLayers[0]).toEqual(layerId6);
      expect(result.byId[mapId2].mapLayers[1]).toEqual(layerId7);
      expect(result.byId[mapId2].mapLayers).toHaveLength(2);

      // move first layer with last
      const result2 = mapReducer(
        mockState,
        mapActions.layerMoveLayer({
          mapId: mapId1,
          oldIndex: 0,
          newIndex: 4,
          origin: 'reducer.spec',
        }),
      );
      expect(result2.byId[mapId1].mapLayers[0]).toEqual(layerId2);
      expect(result2.byId[mapId1].mapLayers[1]).toEqual(layerId3);
      expect(result2.byId[mapId1].mapLayers[2]).toEqual(layerId4);
      expect(result2.byId[mapId1].mapLayers[3]).toEqual(layerId5);
      expect(result2.byId[mapId1].mapLayers[4]).toEqual(layerId1);
    });
  });

  describe('setActiveLayerId', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const layerId1 = 'layer-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setActiveLayerId({
          mapId: mapId1,
          layerId: layerId1,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setActiveLayerId({
          mapId: 'invalidMapId',
          layerId: layerId1,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set the correct layer to active', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      // initial state
      expect(mockState.byId[mapId1].activeLayerId).toEqual('');
      expect(mockState.byId[mapId2].activeLayerId).toEqual('');

      const result = mapReducer(
        mockState,
        mapActions.setActiveLayerId({
          mapId: mapId1,
          layerId: layerId1,
        }),
      );

      expect(result.byId[mapId1].activeLayerId).toEqual(layerId1);
      expect(result.byId[mapId2].activeLayerId).toEqual('');
    });
  });

  describe('toggleAutoUpdate', () => {
    const mapId1 = 'test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.toggleAutoUpdate({
          mapId: mapId1,
          shouldAutoUpdate: true,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should toggle auto update', () => {
      const mockState = createWebmapState(mapId1);

      // initial state
      expect(mockState.byId[mapId1].isAutoUpdating).toBeFalsy();

      // toggle on
      const result = mapReducer(
        mockState,
        mapActions.toggleAutoUpdate({
          mapId: mapId1,
          shouldAutoUpdate: true,
        }),
      );

      expect(result.byId[mapId1].isAutoUpdating).toBeTruthy();

      // toggle off
      expect(
        mapReducer(
          result,
          mapActions.toggleAutoUpdate({
            mapId: mapId1,
            shouldAutoUpdate: false,
          }),
        ).byId[mapId1].isAutoUpdating,
      ).toBeFalsy();
    });
  });

  describe('setTimeSliderScale', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const timeSliderScale1 = Scale.Hour;

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setTimeSliderScale({
          mapId: mapId1,
          timeSliderScale: timeSliderScale1,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setTimeSliderScale({
          mapId: 'invalidMapId',
          timeSliderScale: timeSliderScale1,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set the timeslider scale and secondsPerPx value', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      // initial state
      expect(mockState.byId[mapId1].timeSliderScale).toEqual(Scale.Hour);

      const result = mapReducer(
        mockState,
        mapActions.setTimeSliderScale({
          mapId: mapId1,
          timeSliderScale: Scale.Minutes5,
        }),
      );

      expect(result.byId[mapId1].timeSliderScale).toEqual(Scale.Minutes5);
      expect(result.byId[mapId1].timeSliderSecondsPerPx).toEqual(
        scaleToSecondsPerPx[Scale.Minutes5],
      );
    });

    it('should set the timeslider scale and secondsPerPx value with one week scale', () => {
      const mockState = createWebmapState(mapId1);

      // initial state
      expect(mockState.byId[mapId1].timeSliderScale).toEqual(Scale.Hour);

      const result = mapReducer(
        mockState,
        mapActions.setTimeSliderScale({
          mapId: mapId1,
          timeSliderScale: Scale.Week,
        }),
      );

      expect(result.byId[mapId1].timeSliderScale).toEqual(Scale.Week);
      expect(result.byId[mapId1].timeSliderSecondsPerPx).toEqual(
        scaleToSecondsPerPx[Scale.Week],
      );
    });

    it('should set the timeslider scale with a data scale and secondsPerPx', () => {
      const mockState = createWebmapState(mapId1);

      // initial state
      expect(mockState.byId[mapId1].timeSliderScale).toEqual(Scale.Hour);

      const result = mapReducer(
        mockState,
        mapActions.setTimeSliderScale({
          mapId: mapId1,
          timeSliderScale: Scale.DataScale,
        }),
      );

      expect(result.byId[mapId1].timeSliderScale).toEqual(Scale.DataScale);
      expect(result.byId[mapId1].timeSliderSecondsPerPx).toEqual(
        defaultDataScaleToSecondsPerPx,
      );
    });
  });

  describe('setTimeStep', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const timeStep1 = 5;

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setTimeStep({
          mapId: mapId1,
          timeStep: timeStep1,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setTimeStep({
          mapId: 'invalidMapId',
          timeStep: timeStep1,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set the timestep', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      // initial state
      expect(mockState.byId[mapId1].timeStep).toEqual(5);

      const result = mapReducer(
        mockState,
        mapActions.setTimeStep({
          mapId: mapId1,
          timeStep: 60,
        }),
      );

      expect(result.byId[mapId1].timeStep).toEqual(60);
    });
  });
  describe('toggleTimestepAuto', () => {
    const mapId1 = 'test-1';

    it('should toggle timestep auto', () => {
      const mockState = createWebmapState(mapId1);

      // initial state
      expect(mockState.byId[mapId1].isTimestepAuto).toBeTruthy();

      // toggle off
      const result = mapReducer(
        mockState,
        mapActions.toggleTimestepAuto({
          mapId: mapId1,
          timestepAuto: false,
        }),
      );

      expect(result.byId[mapId1].isTimestepAuto).toBeFalsy();

      // toggle on
      expect(
        mapReducer(
          result,
          mapActions.toggleTimestepAuto({
            mapId: mapId1,
            timestepAuto: true,
          }),
        ).byId[mapId1].isTimestepAuto,
      ).toBeTruthy();
    });
  });

  describe('toggleTimestepAuto', () => {
    const mapId1 = 'test-1';

    it('should toggle timestep auto', () => {
      const mockState = createWebmapState(mapId1);

      // initial state
      expect(mockState.byId[mapId1].isTimestepAuto).toBeTruthy();

      // toggle off
      const result = mapReducer(
        mockState,
        mapActions.toggleTimestepAuto({
          mapId: mapId1,
          timestepAuto: false,
        }),
      );

      expect(result.byId[mapId1].isTimestepAuto).toBeFalsy();

      // toggle on
      expect(
        mapReducer(
          result,
          mapActions.toggleTimestepAuto({
            mapId: mapId1,
            timestepAuto: true,
          }),
        ).byId[mapId1].isTimestepAuto,
      ).toBeTruthy();
    });
  });

  describe('setAnimationStartTime', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const startTime = moment.utc().hours(12).minutes(27).format(dateFormat);

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setAnimationStartTime({
          mapId: mapId1,
          animationStartTime: startTime,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setAnimationStartTime({
          mapId: 'invalidMapId',
          animationStartTime: startTime,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set the animationStartTime', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      const expectedStartTime = moment
        .utc(moment().unix() * 1000)
        .subtract(5, 'h')
        .format(dateFormat);
      // initial state is five hours ago
      expect(mockState.byId[mapId1].animationStartTime).toEqual(
        expectedStartTime,
      );

      const result = mapReducer(
        mockState,
        mapActions.setAnimationStartTime({
          mapId: mapId1,
          animationStartTime: startTime,
        }),
      );

      expect(result.byId[mapId1].animationStartTime).toEqual(startTime);
    });
  });

  describe('setAnimationEndTime', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const endTime = moment.utc().hours(14).minutes(14).format(dateFormat);

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setAnimationEndTime({
          mapId: mapId1,
          animationEndTime: endTime,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setAnimationEndTime({
          mapId: 'invalidMapId',
          animationEndTime: endTime,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set the animationEndTime', () => {
      const mockState = createWebmapState(mapId1, mapId2);
      const expectedEndTime = moment
        .utc(moment().unix() * 1000)
        .subtract(10, 'm')
        .format(dateFormat);
      // initial state is ten minutes ago
      expect(mockState.byId[mapId1].animationEndTime).toEqual(expectedEndTime);

      const result = mapReducer(
        mockState,
        mapActions.setAnimationEndTime({
          mapId: mapId1,
          animationEndTime: endTime,
        }),
      );

      expect(result.byId[mapId1].animationEndTime).toEqual(endTime);
    });
  });

  describe('setAnimationDelay', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setAnimationDelay({
          mapId: mapId1,
          animationDelay: 300,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setAnimationDelay({
          mapId: 'invalidMapId',
          animationDelay: 300,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set the animationDelay', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      // initial state
      expect(mockState.byId[mapId1].animationDelay).toEqual(
        defaultAnimationDelayAtStart,
      );

      const result = mapReducer(
        mockState,
        mapActions.setAnimationDelay({
          mapId: mapId1,
          animationDelay: 300,
        }),
      );

      expect(result.byId[mapId1].animationDelay).toEqual(300);
    });
  });

  describe('setAnimationInterval', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setAnimationInterval({
          mapId: mapId1,
          interval: 180,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setAnimationInterval({
          mapId: 'invalidMapId',
          interval: 180,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set the animationInterval', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      // initial state
      expect(mockState.byId[mapId1].timeStep).toEqual(animationIntervalDefault);

      const result = mapReducer(
        mockState,
        mapActions.setAnimationInterval({
          mapId: mapId1,
          interval: 180,
        }),
      );

      expect(result.byId[mapId1].timeStep).toEqual(180);
    });
  });

  describe('setSelectedFeatureIndex', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setSelectedFeature({
          mapId: mapId1,
          selectedFeatureIndex: 1,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setSelectedFeature({
          mapId: 'invalidMapId',
          selectedFeatureIndex: 1,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set the selected feature index', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        mapActions.setSelectedFeature({
          mapId: mapId1,
          selectedFeatureIndex: 1,
        }),
      );

      expect(result.byId[mapId1].selectedFeatureIndex).toEqual(1);
    });
  });

  describe('layerChangeDimension', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const layerId1 = 'layer-1';
    const mockDimension = {
      name: 'nameDim1',
      currentValue: 'curValueDim1',
      unit: 'unitDim1',
    };

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        layerActions.layerChangeDimension({
          layerId: layerId1,
          dimension: mockDimension,
          origin: LayerActionOrigin.layerManager,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if layerId passed is invalid', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      const result = mapReducer(
        mockState,
        layerActions.layerChangeDimension({
          layerId: 'invalid',
          dimension: mockDimension,
          origin: LayerActionOrigin.layerManager,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should update the linked map dimension', () => {
      const dimension = {
        name: 'namedim1',
        currentValue: 'curValueDim1',
      };
      const layer = {
        id: 'linkedDimensionTest',
        name: 'testLayer',
        service: 'test',
        layerType: LayerType.mapLayer,
        dimensions: [dimension],
      };
      const mockState = createWebmapState(mapId1, mapId2);
      const wmLayer = new WMLayer(layer);
      const mockStateWithLayer = mapReducer(
        mockState,
        layerActions.addLayer({
          layerId: layer.id,
          mapId: mapId1,
          layer,
          origin: 'reducer.spec',
        }),
      );
      registerWMLayer(wmLayer, layer.id);

      // initial state
      expect(mockStateWithLayer.byId[mapId1].dimensions).toEqual([]);
      expect(mockStateWithLayer.byId[mapId1].mapLayers).toEqual([layer.id]);

      const result = mapReducer(
        mockStateWithLayer,
        layerActions.layerChangeDimension({
          layerId: layer.id,
          dimension: { ...dimension, currentValue: 'newValue' },
          origin: LayerActionOrigin.layerManager,
        }),
      );

      expect(result.byId[mapId1].dimensions).toEqual([
        { ...dimension, currentValue: 'newValue' },
      ]);
    });
  });

  describe('genericSyncSetTime', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const newTime = '2021-02-17T12:35:24Z';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        syncActions.setTimeSync(
          {
            sourceId: mapId1,
            origin: 'reducer.spec.ts',
            value: newTime,
          },
          [{ targetId: mapId2, value: newTime }],
          [],
        ),
      );

      expect(result).toEqual(initialState);
    });

    it('should sync time', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      // initial state
      expect(mockState.byId[mapId2].dimensions).toEqual([]);

      const result = mapReducer(
        mockState,
        syncActions.setTimeSync(
          {
            sourceId: mapId1,
            origin: 'reducer.spec.ts',
            value: newTime,
          },
          [{ targetId: mapId2, value: newTime }],
          [],
        ),
      );

      expect(result.byId[mapId2].dimensions).toEqual([
        { name: 'time', currentValue: newTime },
      ]);
    });
  });

  describe('genericSyncSetBBox', () => {
    const mapId1 = 'test-1';
    const mapId2 = 'test-2';
    const newBBox = {
      left: 11,
      bottom: 11,
      right: 11,
      top: 11,
    };

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        syncActions.setBboxSync(
          {
            sourceId: mapId1,
            bbox: newBBox,
            srs: 'new',
          },
          [{ targetId: mapId2, bbox: newBBox, srs: 'new' }],
          [],
        ),
      );

      expect(result).toEqual(initialState);
    });

    it('should sync BBox', () => {
      const mockState = createWebmapState(mapId1, mapId2);

      // initial state
      expect(mockState.byId[mapId1].bbox).toEqual({
        left: -19000000,
        bottom: -19000000,
        right: 19000000,
        top: 19000000,
      });
      expect(mockState.byId[mapId1].srs).toEqual('EPSG:3857');
      expect(mockState.byId[mapId2].bbox).toEqual({
        left: -19000000,
        bottom: -19000000,
        right: 19000000,
        top: 19000000,
      });
      expect(mockState.byId[mapId2].srs).toEqual('EPSG:3857');

      const result = mapReducer(
        mockState,
        syncActions.setBboxSync(
          {
            sourceId: mapId1,
            bbox: newBBox,
            srs: 'new',
          },
          [{ targetId: mapId2, bbox: newBBox, srs: 'new' }],
          [],
        ),
      );
      expect(result.byId[mapId1].bbox).toEqual(newBBox);
      expect(result.byId[mapId1].srs).toEqual('new');
      expect(result.byId[mapId2].bbox).toEqual(newBBox);
      expect(result.byId[mapId2].srs).toEqual('new');
    });
  });

  describe('toggleTimeSliderHover', () => {
    const mapId1 = 'test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.toggleTimeSliderHover({
          mapId: mapId1,
          isTimeSliderHoverOn: true,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should toggle time slider hover', () => {
      const mockState = createWebmapState(mapId1);

      // initial state
      expect(mockState.byId[mapId1].isTimeSliderHoverOn).toBeFalsy();

      // toggle on
      const result = mapReducer(
        mockState,
        mapActions.toggleTimeSliderHover({
          mapId: mapId1,
          isTimeSliderHoverOn: true,
        }),
      );

      expect(result.byId[mapId1].isTimeSliderHoverOn).toBeTruthy();

      // toggle off
      expect(
        mapReducer(
          result,
          mapActions.toggleTimeSliderHover({
            mapId: mapId1,
            isTimeSliderHoverOn: false,
          }),
        ).byId[mapId1].isTimeSliderHoverOn,
      ).toBeFalsy();
    });
  });

  describe('setTimeSliderCenterTime', () => {
    const mapId = 'map-1';
    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setTimeSliderCenterTime({
          mapId,
          timeSliderCenterTime: moment().unix(),
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.setTimeSliderCenterTime({
          mapId: 'invalidMapId',
          timeSliderCenterTime: moment().unix(),
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set new map time slider center time', () => {
      const stateWithRecentTime = createWebmapState(mapId);
      const currentTime = moment().unix();
      const stateWithCurrentTime = {
        ...stateWithRecentTime,
        byId: {
          [mapId]: {
            ...stateWithRecentTime.byId[mapId],
            timeSliderCenterTime: currentTime,
          },
        },
        allIds: [mapId],
      };
      expect(stateWithCurrentTime.byId[mapId].timeSliderCenterTime).toEqual(
        currentTime,
      );

      const time = moment.utc().hours(14).unix();
      const result = mapReducer(
        stateWithCurrentTime,
        mapActions.setTimeSliderCenterTime({
          mapId,
          timeSliderCenterTime: time,
        }),
      );
      expect(result.byId[mapId].timeSliderCenterTime).toEqual(time);
    });
  });

  describe('setTimeSliderSecondsPerPx', () => {
    const mapId = 'map-1';
    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setTimeSliderSecondsPerPx({
          mapId,
          timeSliderSecondsPerPx: 60,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.setTimeSliderSecondsPerPx({
          mapId: 'invalidMapId',
          timeSliderSecondsPerPx: 60,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set new map time slider secondsPerPx value', () => {
      const mockState = createWebmapState(mapId);
      expect(mockState.byId[mapId].timeSliderSecondsPerPx).toEqual(30);

      const secondsPerPx = 5;
      const result = mapReducer(
        mockState,
        mapActions.setTimeSliderSecondsPerPx({
          mapId,
          timeSliderSecondsPerPx: secondsPerPx,
        }),
      );
      expect(result.byId[mapId].timeSliderSecondsPerPx).toEqual(secondsPerPx);
    });

    it('should set new map time slider secondsPerPx value', () => {
      const mockState = createWebmapState(mapId);
      expect(mockState.byId[mapId].timeSliderSecondsPerPx).toEqual(30);

      const secondsPerPx = defaultDataScaleToSecondsPerPx;
      const result = mapReducer(
        mockState,
        mapActions.setTimeSliderSecondsPerPx({
          mapId,
          timeSliderSecondsPerPx: secondsPerPx,
        }),
      );
      expect(result.byId[mapId].timeSliderSecondsPerPx).toEqual(
        defaultDataScaleToSecondsPerPx,
      );
      expect(result.byId[mapId].timeSliderScale).toEqual(Scale.DataScale);
    });
  });

  describe('setTimeSliderDataScaleToSecondsPerPx', () => {
    const mapId = 'map-1';
    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setTimeSliderDataScaleToSecondsPerPx({
          mapId,
          timeSliderDataScaleToSecondsPerPx: defaultDataScaleToSecondsPerPx,
        }),
      );

      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.setTimeSliderDataScaleToSecondsPerPx({
          mapId: 'invalidMapId',
          timeSliderDataScaleToSecondsPerPx: defaultDataScaleToSecondsPerPx,
        }),
      );

      expect(result).toEqual(mockState);
    });

    it('should set new map time slider dataScaleToSecondsPerP value', () => {
      const mockState = createWebmapState(mapId);
      expect(mockState.byId[mapId].timeSliderSecondsPerPx).toEqual(30);

      const dataScaleToSecondsPerPx = defaultDataScaleToSecondsPerPx;
      const result = mapReducer(
        mockState,
        mapActions.setTimeSliderDataScaleToSecondsPerPx({
          mapId,
          timeSliderDataScaleToSecondsPerPx: dataScaleToSecondsPerPx,
        }),
      );
      expect(result.byId[mapId].timeSliderDataScaleToSecondsPerPx).toEqual(
        dataScaleToSecondsPerPx,
      );
    });
  });
  describe('Check checkIfMapLayerIdIsAlreadyTaken', () => {
    it('should return true if id is already taken', () => {
      const state: WebMapState = {
        byId: {
          mapid_1: {
            mapLayers: ['layerId_A', 'layerId_B'],
            baseLayers: ['layerId_C', 'layerId_D'],
            overLayers: ['layerId_E', 'layerId_F'],
            featureLayers: ['layerId_G', 'layerId_H'],
          },
          mapid_2: {
            mapLayers: ['layerId_I', 'layerId_J'],
            baseLayers: ['layerId_K', 'layerId_L'],
            overLayers: ['layerId_M', 'layerId_N'],
            featureLayers: ['layerId_O', 'layerId_P'],
          },
        },
        allIds: ['mapid_1', 'mapid_2'],
      } as unknown as WebMapState;
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_A')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_B')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_C')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_D')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_E')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_F')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_G')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_H')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_I')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_J')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_K')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_L')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_M')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_N')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_O')).toBeTruthy();
      expect(checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_P')).toBeTruthy();
      expect(
        checkIfMapLayerIdIsAlreadyTaken(state, 'layerId_NonYetThere'),
      ).toBeFalsy();
    });
  });

  describe('toggleTimeSliderIsVisible', () => {
    const mapId = 'test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.toggleTimeSliderIsVisible({
          mapId,
          isTimeSliderVisible: true,
        }),
      );
      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.toggleTimeSliderIsVisible({
          mapId: 'undef',
          isTimeSliderVisible: true,
        }),
      );
      expect(result).toEqual(mockState);
    });

    it('should set toggle TimeSlider', () => {
      const mockState = createWebmapState(mapId);
      expect(mockState.byId[mapId].isTimeSliderVisible).toBeTruthy();

      const result = mapReducer(
        mockState,
        mapActions.toggleTimeSliderIsVisible({
          mapId,
          isTimeSliderVisible: false,
        }),
      );

      expect(result.byId[mapId].isTimeSliderVisible).toBeFalsy();
    });
  });

  describe('toggleZoomControls', () => {
    const mapId = 'test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.toggleZoomControls({
          mapId,
          shouldShowZoomControls: true,
        }),
      );
      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.toggleZoomControls({
          mapId: 'undef',
          shouldShowZoomControls: true,
        }),
      );
      expect(result).toEqual(mockState);
    });

    it('should set toggle zoom controls', () => {
      const mockState = createWebmapState(mapId);
      expect(mockState.byId[mapId].shouldShowZoomControls).toBeTruthy();

      const result = mapReducer(
        mockState,
        mapActions.toggleZoomControls({
          mapId,
          shouldShowZoomControls: false,
        }),
      );

      expect(result.byId[mapId].shouldShowZoomControls).toBeFalsy();
    });
  });

  describe('setMapPinLocation', () => {
    const mapId = 'test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setMapPinLocation({
          mapId,
          mapPinLocation: { lat: 52.0, lon: 5.0 },
        }),
      );
      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.setMapPinLocation({
          mapId: 'undef',
          mapPinLocation: { lat: 52.0, lon: 5.0 },
        }),
      );
      expect(result).toEqual(mockState);
    });

    it('should set mapPinLocation', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.setMapPinLocation({
          mapId,
          mapPinLocation: { lat: 52.0, lon: 5.0 },
        }),
      );

      expect(result.byId[mapId].mapPinLocation.lat).toEqual(52.0);
      expect(result.byId[mapId].mapPinLocation.lon).toEqual(5.0);
    });
  });

  describe('DisableMapPin', () => {
    const mapId = 'test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setDisableMapPin({
          mapId,
          disableMapPin: true,
        }),
      );
      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.setDisableMapPin({
          mapId: 'undef',
          disableMapPin: true,
        }),
      );
      expect(result).toEqual(mockState);
    });

    it('should disable the map pin', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.setDisableMapPin({
          mapId,
          disableMapPin: true,
        }),
      );
      expect(result.byId[mapId].disableMapPin).toBeTruthy();

      const enabledResult = mapReducer(
        mockState,
        mapActions.setDisableMapPin({
          mapId,
          disableMapPin: false,
        }),
      );
      expect(enabledResult.byId[mapId].disableMapPin).toBeFalsy();
    });
  });

  describe('toggleMapPinIsVisible', () => {
    const mapId = 'test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.toggleMapPinIsVisible({
          mapId,
          displayMapPin: true,
        }),
      );
      expect(result).toEqual(initialState);
    });

    it('should return passed state if mapId passed is invalid', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.toggleMapPinIsVisible({
          mapId: 'undef',
          displayMapPin: true,
        }),
      );
      expect(result).toEqual(mockState);
    });

    it('should toggle display the map pin', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.toggleMapPinIsVisible({
          mapId,
          displayMapPin: true,
        }),
      );
      expect(result.byId[mapId].displayMapPin).toBeTruthy();

      const enabledResult = mapReducer(
        mockState,
        mapActions.toggleMapPinIsVisible({
          mapId,
          displayMapPin: false,
        }),
      );
      expect(enabledResult.byId[mapId].displayMapPin).toBeFalsy();
    });
  });

  describe('setMapPreset', () => {
    const mapId = 'test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setMapPreset({
          mapId,
          initialProps: {
            mapPreset: {},
          },
        }),
      );
      expect(result).toEqual(initialState);
    });

    it('should return reset state of a map when setting a mapPreset', () => {
      const mockState = {
        byId: {
          map1: {
            baseLayers: ['base-1'],
            overLayers: ['over-1'],
            mapLayers: ['map-1'],
            hasMapPresetChanges: true,
            isAutoUpdating: true,
            isAnimating: true,
            isTimeSliderVisible: false,
            isTimestepAuto: false,
            displayMapPin: true,
            shouldShowZoomControls: false,
            timeStep: 1,
            animationDelay: 10000,
          } as WebMap,
          map2: {
            baseLayers: ['base-2'],
            overLayers: ['over-2'],
            mapLayers: ['map-2'],
          } as WebMap,
        },
        allIds: ['map1', 'map2'],
      };

      const result = mapReducer(
        mockState,
        mapActions.setMapPreset({
          mapId: 'map1',
          initialProps: {
            mapPreset: {},
          },
        }),
      );
      expect(result.byId.map1.baseLayers).toHaveLength(0);
      expect(result.byId.map1.overLayers).toHaveLength(0);
      expect(result.byId.map1.mapLayers).toHaveLength(0);
      expect(result.byId.map1.hasMapPresetChanges).toEqual(false);
      expect(result.byId.map1.isAutoUpdating).toEqual(false);
      expect(result.byId.map1.isAnimating).toEqual(false);
      expect(result.byId.map1.isTimeSliderVisible).toEqual(true);
      expect(result.byId.map1.isTimestepAuto).toEqual(true);
      expect(result.byId.map1.displayMapPin).toEqual(false);
      expect(result.byId.map1.shouldShowZoomControls).toEqual(true);
      expect(result.byId.map1.timeStep).toEqual(defaultTimeStep);
      expect(result.byId.map1.animationDelay).toEqual(
        defaultAnimationDelayAtStart,
      );

      expect(result.byId.map2.baseLayers).toHaveLength(1);
      expect(result.byId.map2.overLayers).toHaveLength(1);
      expect(result.byId.map2.mapLayers).toHaveLength(1);
      expect(result.byId.map2.hasMapPresetChanges).toBeUndefined();
      expect(result.byId.map2.isAutoUpdating).toBeUndefined();
      expect(result.byId.map2.isAnimating).toBeUndefined();
      expect(result.byId.map2.isTimeSliderVisible).toBeUndefined();
      expect(result.byId.map2.isTimestepAuto).toBeUndefined();
      expect(result.byId.map2.displayMapPin).toBeUndefined();
      expect(result.byId.map2.shouldShowZoomControls).toBeUndefined();
      expect(result.byId.map2.timeStep).toBeUndefined();
      expect(result.byId.map2.animationDelay).toBeUndefined();
    });
  });

  describe('setActiveMapPresetId', () => {
    const mapId = 'test-1';
    const presetId = 'preset-test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setActiveMapPresetId({
          mapId,
          presetId,
        }),
      );
      expect(result).toEqual(initialState);
    });

    it('should reset active map preset id', () => {
      const mockState = {
        ...createWebmapState(mapId),
        byId: {
          [mapId]: {
            ...createWebmapState(mapId).byId[mapId],
            activeMapPresetId: null,
          },
        },
        allIds: [mapId],
      };
      expect(mockState.byId[mapId].activeMapPresetId).toBeNull();

      const result = mapReducer(
        mockState,
        mapActions.setActiveMapPresetId({
          mapId,
          presetId,
        }),
      );
      expect(result.byId[mapId].activeMapPresetId).toEqual(presetId);
    });

    it('should set active map preset id', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.setActiveMapPresetId({
          mapId,
          presetId,
        }),
      );
      expect(result.byId[mapId].activeMapPresetId).toEqual(presetId);
    });
  });

  describe('setIsMapPresetLoading', () => {
    const mapId = 'test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setIsMapPresetLoading({
          mapId,
          isLoading: true,
        }),
      );
      expect(result).toEqual(initialState);
    });

    it('should set isMapPresetLoading', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.setIsMapPresetLoading({
          mapId,
          isLoading: true,
        }),
      );
      expect(result.byId[mapId].isMapPresetLoading).toEqual(true);
      const result2 = mapReducer(
        mockState,
        mapActions.setIsMapPresetLoading({
          mapId,
          isLoading: false,
        }),
      );
      expect(result2.byId[mapId].isMapPresetLoading).toEqual(false);
    });
  });

  describe('setHasMapPresetChanges', () => {
    const mapId = 'test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setHasMapPresetChanges({
          mapId,
          hasChanges: true,
        }),
      );
      expect(result).toEqual(initialState);
    });

    it('should set hasMapPresetChanges', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.setHasMapPresetChanges({
          mapId,
          hasChanges: true,
        }),
      );
      expect(result.byId[mapId].hasMapPresetChanges).toEqual(true);
      const result2 = mapReducer(
        mockState,
        mapActions.setHasMapPresetChanges({
          mapId,
          hasChanges: false,
        }),
      );
      expect(result2.byId[mapId].hasMapPresetChanges).toEqual(false);
    });
  });

  describe('setMapPresetError', () => {
    const mapId = 'test-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        mapActions.setMapPresetError({
          mapId,
          error: 'test error',
        }),
      );
      expect(result).toEqual(initialState);
    });

    it('should set mapPresetError', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        mapActions.setMapPresetError({
          mapId,
          error: 'test error',
        }),
      );
      expect(result.byId[mapId].mapPresetError).toEqual('test error');
      const result2 = mapReducer(
        mockState,
        mapActions.setMapPresetError({
          mapId,
          error: null,
        }),
      );
      expect(result2.byId[mapId].mapPresetError).toEqual(null);
    });
  });

  describe('registerDialog', () => {
    const mapId = 'test-1';
    const legendId = 'legend-1';

    it('should return initial state if no state is passed', () => {
      const result = mapReducer(
        undefined,
        uiActions.registerDialog({
          mapId,
          type: legendId,
        }),
      );
      expect(result).toEqual(initialState);
    });

    it('should register legend on map', () => {
      const mockState = createWebmapState(mapId);

      const result = mapReducer(
        mockState,
        uiActions.registerDialog({
          mapId,
          type: legendId,
        }),
      );
      expect(result.byId[mapId].legendId).toEqual(legendId);
    });
  });
});
