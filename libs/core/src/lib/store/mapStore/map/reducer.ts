/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-console */
import { produce } from 'immer';

import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { generateLayerId, getWMJSMapById } from '../utils/helpers';

import {
  WebMapState,
  SetActiveLayerIdPayload,
  SetBboxPayload,
  SetMapAnimationStartPayload,
  SetMapAnimationStopPayload,
  SetTimeSliderScalePayload,
  SetTimeStepPayload,
  SetAnimationStartTimePayload,
  SetAnimationEndTimePayload,
  SetSelectedFeaturePayload,
  SetAnimationDelayPayload,
  MoveLayerPayload,
  SetTimeSliderCenterTimePayload,
  SetTimeSliderDataScaleToSecondsPerPxPayload,
  ToggleAutoUpdatePayload,
  ToggleTimestepAutoPayload,
  ToggleTimeSliderHoverPayload,
  MapPinLocationPayload,
  DisableMapPinPayload,
  SetActiveMapPresetPayload,
  SetTimeSliderSecondsPerPxPayload,
  UpdateAllMapDimensionsPayload,
  SetIsMapPresetLoadingPayload,
  SetIsMapPresetHasChangesPayload,
  SetMapPresetErrorPayload,
  ToggleTimeSliderIsVisiblePayload,
  SetAnimationIntervalPayload,
} from './types';

import {
  Scale,
  Dimension,
  Layer,
  LayerType,
  UpdateLayerInfoPayload,
  ToggleMapPinIsVisiblePayload,
  ToggleZoomControlsPayload,
} from '../types';

import {
  produceDraftStateSetWebMapDimension,
  produceDraftStateSetMapDimensionFromLayerChangeDimension,
  createMap,
  moveArrayElements,
} from './utils';
import {
  defaultAnimationDelayAtStart,
  defaultTimeStep,
  scaleToSecondsPerPx,
  secondsPerPxToScale,
} from '../../../components/TimeSlider/TimeSliderUtils';

import { SyncLayerPayloads } from '../../generic/types';
import {
  setBboxSync,
  setLayerActionSync,
  setTimeSync,
} from '../../generic/synchronizationActions/actions';
import { mapChangeDimension, setMapPreset } from './actions';
import { layerActions } from '../layers/reducer';
import { uiActions } from '../../ui/reducer';

const {
  addBaseLayer,
  addLayer,
  baseLayerDelete,
  layerChangeDimension,
  layerDelete,
  onUpdateLayerInformation,
  setBaseLayers,
  setLayers,
} = layerActions;

/**
 * Checks if the layer id is already taken in one of the maps.
 * @param state The WebMapState
 * @param layerIdToCheck The layerId to check
 * @returns true if the layerId is already taken somewhere else.
 */
export const checkIfMapLayerIdIsAlreadyTaken = (
  state: WebMapState,
  layerId: string,
): boolean => {
  return state.allIds.some((mapId: string) => {
    const map = state.byId[mapId];
    const doesLayeridExist = (layers: string[]): boolean =>
      layers && layers.some((id: string): boolean => id === layerId);
    return (
      doesLayeridExist(map.mapLayers) ||
      doesLayeridExist(map.overLayers) ||
      doesLayeridExist(map.baseLayers) ||
      doesLayeridExist(map.featureLayers)
    );
  });
};

/**
 * Addes id's to the layer object. All layers need to have an id for referring.
 * @param layers
 */
const createLayersWithIds = (state: WebMapState, layers: Layer[]): Layer[] => {
  if (!layers) return layers;
  return produce(layers, (draft) => {
    for (let j = 0; j < draft.length; j += 1) {
      const layer = draft[j];
      if (!layer.id) {
        layer.id = generateLayerId();
      } else if (checkIfMapLayerIdIsAlreadyTaken(state, layer.id)) {
        console.warn(
          `Warning: Layer id ${layer.id} was already taken: Generating new one.`,
        );
        layer.id = generateLayerId();
      }
    }
  });
};

export const initialState: WebMapState = {
  byId: {},
  allIds: [],
};

export const slice = createSlice({
  initialState,
  name: 'mapReducer',
  reducers: {
    registerMap: (draft, action: PayloadAction<{ mapId: string }>) => {
      const { mapId } = action.payload;
      if (!draft.allIds.includes(mapId)) {
        draft.byId[mapId] = createMap({ id: mapId });
        draft.allIds.push(mapId);
      }
    },
    unregisterMap: (draft, action: PayloadAction<{ mapId: string }>) => {
      const { mapId } = action.payload;
      const mapIndex = draft.allIds.indexOf(mapId);
      if (mapIndex !== -1) {
        delete draft.byId[mapId];
        draft.allIds.splice(mapIndex, 1);
      }
    },
    setBbox: (draft, action: PayloadAction<SetBboxPayload>) => {
      const { mapId, bbox } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      const srs = action.payload.srs || null;
      draft.byId[mapId].bbox = bbox;
      if (srs) draft.byId[mapId].srs = srs;
    },
    mapUpdateAllMapDimensions: (
      draft,
      action: PayloadAction<UpdateAllMapDimensionsPayload>,
    ) => {
      const { mapId, dimensions } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].dimensions.length = 0;
      dimensions.forEach((dimension) =>
        produceDraftStateSetWebMapDimension(draft, mapId, dimension, true),
      );
    },
    mapStartAnimation: (
      draft,
      action: PayloadAction<SetMapAnimationStartPayload>,
    ) => {
      const { mapId } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].isAnimating = true;
      if (action.payload.start) {
        draft.byId[mapId].animationStartTime = action.payload.start;
      }
      if (action.payload.end) {
        draft.byId[mapId].animationEndTime = action.payload.end;
      }
      if (
        action.payload.interval &&
        action.payload.interval !== draft.byId[mapId].timeStep
      ) {
        draft.byId[mapId].timeStep = action.payload.interval;
        draft.byId[mapId].isTimestepAuto = false;
      }
    },
    mapStopAnimation: (
      draft,
      action: PayloadAction<SetMapAnimationStopPayload>,
    ) => {
      const { mapId } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].isAnimating = false;
    },
    setTimeSliderScale: (
      draft,
      action: PayloadAction<SetTimeSliderScalePayload>,
    ) => {
      const { mapId, timeSliderScale } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].timeSliderScale = timeSliderScale;
      const scaleToSecPerPx =
        timeSliderScale === Scale.DataScale
          ? draft.byId[mapId].timeSliderDataScaleToSecondsPerPx
          : scaleToSecondsPerPx[timeSliderScale];
      draft.byId[mapId].timeSliderSecondsPerPx = scaleToSecPerPx;
    },
    setTimeStep: (draft, action: PayloadAction<SetTimeStepPayload>) => {
      const { mapId, timeStep } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].timeStep = timeStep;
    },
    setAnimationStartTime: (
      draft,
      action: PayloadAction<SetAnimationStartTimePayload>,
    ) => {
      const { mapId, animationStartTime } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].animationStartTime = animationStartTime;
    },
    setAnimationEndTime: (
      draft,
      action: PayloadAction<SetAnimationEndTimePayload>,
    ) => {
      const { mapId, animationEndTime } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].animationEndTime = animationEndTime;
    },
    setSelectedFeature: (
      draft,
      action: PayloadAction<SetSelectedFeaturePayload>,
    ) => {
      const { mapId, selectedFeatureIndex } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }

      draft.byId[mapId].selectedFeatureIndex = selectedFeatureIndex;
    },
    setAnimationDelay: (
      draft,
      action: PayloadAction<SetAnimationDelayPayload>,
    ) => {
      const { mapId, animationDelay } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].animationDelay = animationDelay;
    },
    setAnimationInterval: (
      draft,
      action: PayloadAction<SetAnimationIntervalPayload>,
    ) => {
      const { mapId, interval } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].timeStep = interval;
    },
    layerMoveLayer: (draft, action: PayloadAction<MoveLayerPayload>) => {
      const { oldIndex, newIndex, mapId } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }

      draft.byId[mapId].mapLayers = moveArrayElements(
        draft.byId[mapId].mapLayers,
        oldIndex,
        newIndex,
      );
    },
    setActiveLayerId: (
      draft,
      action: PayloadAction<SetActiveLayerIdPayload>,
    ) => {
      const { mapId, layerId } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].activeLayerId = layerId;
    },
    setTimeSliderCenterTime: (
      draft,
      action: PayloadAction<SetTimeSliderCenterTimePayload>,
    ) => {
      const { mapId, timeSliderCenterTime } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].timeSliderCenterTime = timeSliderCenterTime;
    },
    setTimeSliderSecondsPerPx: (
      draft,
      action: PayloadAction<SetTimeSliderSecondsPerPxPayload>,
    ) => {
      const { mapId, timeSliderSecondsPerPx } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }

      const secondsPerPxToScaleValue = secondsPerPxToScale.get(
        timeSliderSecondsPerPx,
      );
      const scale =
        secondsPerPxToScaleValue === undefined
          ? Scale.DataScale
          : secondsPerPxToScale.get(timeSliderSecondsPerPx);
      draft.byId[mapId].timeSliderSecondsPerPx = timeSliderSecondsPerPx;
      draft.byId[mapId].timeSliderScale = scale;
    },
    setTimeSliderDataScaleToSecondsPerPx: (
      draft,
      action: PayloadAction<SetTimeSliderDataScaleToSecondsPerPxPayload>,
    ) => {
      const { mapId, timeSliderDataScaleToSecondsPerPx } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].timeSliderDataScaleToSecondsPerPx =
        timeSliderDataScaleToSecondsPerPx;
    },
    toggleAutoUpdate: (
      draft,
      action: PayloadAction<ToggleAutoUpdatePayload>,
    ) => {
      const { mapId, shouldAutoUpdate } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].isAutoUpdating = shouldAutoUpdate;
    },
    toggleTimestepAuto: (
      draft,
      action: PayloadAction<ToggleTimestepAutoPayload>,
    ) => {
      const { mapId, timestepAuto } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].isTimestepAuto = timestepAuto;
    },
    toggleTimeSliderHover: (
      draft,
      action: PayloadAction<ToggleTimeSliderHoverPayload>,
    ) => {
      const { mapId, isTimeSliderHoverOn } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].isTimeSliderHoverOn = isTimeSliderHoverOn;
    },
    toggleTimeSliderIsVisible: (
      draft,
      action: PayloadAction<ToggleTimeSliderIsVisiblePayload>,
    ) => {
      const { mapId, isTimeSliderVisible } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].isTimeSliderVisible = isTimeSliderVisible;
    },
    toggleZoomControls: (
      draft,
      action: PayloadAction<ToggleZoomControlsPayload>,
    ) => {
      const { mapId, shouldShowZoomControls } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].shouldShowZoomControls = shouldShowZoomControls;
    },
    setMapPinLocation: (
      draft,
      action: PayloadAction<MapPinLocationPayload>,
    ) => {
      const { mapId, mapPinLocation } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].mapPinLocation = mapPinLocation;
    },
    setDisableMapPin: (draft, action: PayloadAction<DisableMapPinPayload>) => {
      const { mapId, disableMapPin } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].disableMapPin = disableMapPin;
    },
    toggleMapPinIsVisible: (
      draft,
      action: PayloadAction<ToggleMapPinIsVisiblePayload>,
    ) => {
      const { mapId, displayMapPin } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].displayMapPin = displayMapPin;
    },
    setActiveMapPresetId: (
      draft,
      action: PayloadAction<SetActiveMapPresetPayload>,
    ) => {
      const { mapId, presetId } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].activeMapPresetId = presetId;
    },
    setIsMapPresetLoading: (
      draft,
      action: PayloadAction<SetIsMapPresetLoadingPayload>,
    ) => {
      const { mapId, isLoading } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].isMapPresetLoading = isLoading;
    },
    setHasMapPresetChanges: (
      draft,
      action: PayloadAction<SetIsMapPresetHasChangesPayload>,
    ) => {
      const { mapId, hasChanges } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].hasMapPresetChanges = hasChanges;
    },
    setMapPresetError: (
      draft,
      action: PayloadAction<SetMapPresetErrorPayload>,
    ) => {
      const { mapId, error } = action.payload;
      if (!draft.byId[mapId]) {
        return;
      }
      draft.byId[mapId].mapPresetError = error;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(addLayer, (draft, action) => {
        const { layerId, mapId } = action.payload;
        if (!draft.byId[mapId]) {
          return;
        }

        if (checkIfMapLayerIdIsAlreadyTaken(draft, layerId)) {
          console.warn(`Warning: Layer id ${layerId} was already taken`);
          return;
        }
        if (!draft.byId[mapId].mapLayers.length) {
          draft.byId[mapId].activeLayerId = layerId;
        }
        draft.byId[mapId].mapLayers.unshift(layerId);
      })
      .addCase(addBaseLayer, (draft, action) => {
        const { layer, layerId, mapId } = action.payload;
        if (!draft.byId[mapId]) {
          return;
        }
        if (checkIfMapLayerIdIsAlreadyTaken(draft, layerId)) {
          console.warn(`Warning: Layer id ${layerId} was already taken`);
          return;
        }
        if (layer.layerType === LayerType.baseLayer) {
          draft.byId[mapId].baseLayers.unshift(layerId);
          return;
        }
        if (layer.layerType === LayerType.overLayer) {
          draft.byId[mapId].overLayers.unshift(layerId);
        }
      })
      .addCase(setLayers, (draft, action) => {
        const { mapId } = action.payload;
        const layersWithIds = createLayersWithIds(draft, action.payload.layers);
        const layerIds = layersWithIds.map(({ id }) => id);
        const [activeLayerId] = layerIds;

        if (draft.byId[mapId]) {
          draft.byId[mapId].mapLayers = layerIds;
          draft.byId[mapId].activeLayerId = activeLayerId;
          const webmapInstance = getWMJSMapById(mapId);
          if (webmapInstance) {
            const mapDims = webmapInstance.getDimensionList();
            if (mapDims) {
              draft.byId[mapId].dimensions = mapDims.map((dim) => ({
                name: dim.name,
                currentValue: dim.currentValue,
                units: dim.units,
              }));
            }
          }
        }
      })
      .addCase(setBaseLayers, (draft, action) => {
        const { layers, mapId } = action.payload;
        // Split into base and overlayers
        const baseLayers = [];
        const overLayers = [];
        layers.forEach((layer) => {
          if (layer.layerType === LayerType.overLayer) {
            overLayers.push(layer);
          } else if (layer.layerType === LayerType.baseLayer) {
            baseLayers.push(layer);
          }
        });

        const baseLayersWithIds = createLayersWithIds(draft, baseLayers);
        const overLayersWithIds = createLayersWithIds(draft, overLayers);

        const baseLayerIds = baseLayersWithIds.map(({ id }) => id);
        const overLayerIds = overLayersWithIds.map(({ id }) => id);

        if (draft.byId[mapId]) {
          if (baseLayerIds.length !== 0)
            draft.byId[mapId].baseLayers = baseLayerIds;
          if (overLayerIds.length !== 0)
            draft.byId[mapId].overLayers = overLayerIds;
        }
      })
      .addCase(layerDelete, (draft, action) => {
        const { mapId, layerId } = action.payload;
        if (!draft.byId[mapId]) {
          return;
        }
        draft.byId[mapId].mapLayers = draft.byId[mapId].mapLayers.filter(
          (id) => id !== layerId,
        );
        // if activeLayer is deleted, change activeLayerId
        if (
          draft.byId[mapId].activeLayerId === layerId &&
          draft.byId[mapId].mapLayers.length
        ) {
          const [activeLayerId] = draft.byId[mapId].mapLayers;
          draft.byId[mapId].activeLayerId = activeLayerId;
        }
        if (!draft.byId[mapId].mapLayers.length) {
          draft.byId[mapId].activeLayerId = '';
        }
      })
      .addCase(baseLayerDelete, (draft, action) => {
        const { mapId, layerId } = action.payload;
        if (!draft.byId[mapId]) {
          return;
        }
        draft.byId[mapId].baseLayers = draft.byId[mapId].baseLayers.filter(
          (id) => id !== layerId,
        );
        draft.byId[mapId].overLayers = draft.byId[mapId].overLayers.filter(
          (id) => id !== layerId,
        );
      })
      .addCase(layerChangeDimension, (draft, action) => {
        const { layerId, dimension } = action.payload;
        produceDraftStateSetMapDimensionFromLayerChangeDimension(
          draft,
          layerId,
          dimension,
        );
      })
      .addCase(setTimeSync, (draft, action) => {
        const { targets: targetsFromAction, source } = action.payload;
        /* Because we want backwards compatibility with the previous code, we also need to listen to the original source action */
        const targets = [
          {
            targetId: source.payload.sourceId,
            value: source.payload.value,
          },
        ];
        /* And then append the targets form the action */
        targets.push(...targetsFromAction);
        targets.forEach((target) => {
          const { targetId, value } = target;
          if (draft.byId[targetId]) {
            const dimension: Dimension = {
              name: 'time',
              currentValue: value,
            };
            produceDraftStateSetWebMapDimension(
              draft,
              targetId,
              dimension,
              true,
            );
            produceDraftStateSetMapDimensionFromLayerChangeDimension(
              draft,
              targetId,
              dimension,
            );
          }
        });
      })
      .addCase(setBboxSync, (draft, action) => {
        const { targets: targetsFromAction, source } = action.payload;
        /* Because we want backwards compatibility with the previous code, we also need to listen to the original source action */
        const targets = [
          {
            targetId: source.payload.sourceId,
            bbox: source.payload.bbox,
            srs: source.payload.srs,
          },
        ];
        /* And then append the targets form the action */
        targets.push(...targetsFromAction);
        targets.forEach((payload) => {
          const { targetId, bbox, srs } = payload;
          if (draft.byId[targetId]) {
            if (bbox) {
              draft.byId[targetId].bbox = bbox;
            }
            if (srs) draft.byId[targetId].srs = srs;
          }
        });
      })
      .addCase(setLayerActionSync, (draft, action) => {
        /*
         * This GENERIC_SYNC_SETLAYERACTIONS action is generated by the syncgroup saga.
         * It has multiple targets (Layers) in its payload.
         * These targets can be used as payloads in new Layer actions.
         * These actions are here handled via the layer reducer, as it is the same logic
         */
        const { targets, source } = action.payload;

        return targets.reduce(
          (prevState: WebMapState, target: SyncLayerPayloads): WebMapState => {
            const action = {
              payload: target,
              type: source.type,
            };
            /* Handle the Layer action with the same logic, using the same reducer */
            return reducer(prevState, action);
          },
          draft,
        );
      })
      .addCase(
        onUpdateLayerInformation,
        (draft, action: PayloadAction<UpdateLayerInfoPayload>) => {
          const { mapDimensions } = action.payload;
          if (mapDimensions === null) return draft;
          const mapAction = mapActions.mapUpdateAllMapDimensions(mapDimensions);
          return reducer(draft, mapAction);
        },
      )
      .addCase(mapChangeDimension, (draft, action) => {
        const { mapId, dimension: dimensionFromAction } = action.payload;
        produceDraftStateSetWebMapDimension(
          draft,
          mapId,
          dimensionFromAction,
          true,
        );
      })
      .addCase(setMapPreset, (draft, action) => {
        const { mapId } = action.payload;
        if (!draft.byId[mapId]) {
          return;
        }
        // reset preset state (currently setMapPreset saga is firing actions and these are setting values)
        draft.byId[mapId].baseLayers = [];
        draft.byId[mapId].mapLayers = [];
        draft.byId[mapId].overLayers = [];
        draft.byId[mapId].hasMapPresetChanges = false;
        draft.byId[mapId].isAutoUpdating = false;
        draft.byId[mapId].isAnimating = false;
        draft.byId[mapId].isTimeSliderVisible = true;
        draft.byId[mapId].isTimestepAuto = true;
        draft.byId[mapId].displayMapPin = false;
        draft.byId[mapId].shouldShowZoomControls = true;
        draft.byId[mapId].timeStep = defaultTimeStep;
        draft.byId[mapId].animationDelay = defaultAnimationDelayAtStart;
      })
      .addCase(uiActions.registerDialog, (draft, action) => {
        const { mapId, type } = action.payload;
        if (!draft.byId[mapId]) {
          return;
        }
        draft.byId[mapId].legendId = type;
      });
  },
});

export const mapActions = {
  ...slice.actions,
  setMapPreset,
  mapChangeDimension,
};

export const { reducer } = slice;
