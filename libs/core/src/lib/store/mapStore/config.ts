/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { ISagaModule } from 'redux-dynamic-modules-saga';

import { reducer as webmap } from './map/reducer';
import { reducer as services } from './service/reducer';
import { reducer as layers } from './layers/reducer';

import mapSaga from './map/sagas';

import { WebMapStateModuleState } from './types';

const moduleConfig: ISagaModule<WebMapStateModuleState> = {
  id: 'webmap-module',
  reducerMap: {
    webmap,
    services,
    layers,
  },
  sagas: [mapSaga],
};

export default moduleConfig;
