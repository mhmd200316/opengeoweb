/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as layerSelectors from './selectors';
import { LayerType, LayerStatus } from './types';

const testState = {
  layers: {
    byId: {
      'test-1': {
        id: 'test-1',
        name: 'LAYER_NAME',
        dimensions: [
          { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
          { name: 'elevation', units: 'm', currentValue: '100' },
        ],
        layerType: LayerType.mapLayer,
        opacity: 0.5,
        enabled: true,
        service: 'test.service.com',
        status: LayerStatus.error,
        style: 'knmiradar/nearest',
        styles: [
          {
            title: 'knmiradar/nearest',
            name: 'knmiradar/nearest',
            legendURL: 'someURL',
            abstract: 'No abstract available',
          },
          {
            title: 'precip/nearest',
            name: 'precip/nearest',
            legendURL: 'someURL2',
            abstract: 'No abstract available',
          },
          {
            title: 'precip-transparent/nearest',
            name: 'precip-transparent/nearest',
            legendURL: 'someURL3',
            abstract: 'No abstract available',
          },
        ],
      },
      'test-2': {
        id: 'test-2',
        layerType: LayerType.baseLayer,
      },
      'test-3': {
        id: 'test-3',
        dimensions: [
          { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
        ],
        layerType: LayerType.overLayer,
      },
    },
    allIds: ['test-1', 'test-2', 'test-3'],
    availableBaseLayers: {
      byId: {
        'base_layer-1': {
          id: 'base_layer-1',
          name: 'BASELAYER_NAME',
          layerType: LayerType.baseLayer,
          opacity: 1,
          enabled: true,
          mapId: 'map1',
        },
        'base_layer-2': {
          id: 'base_layer-2',
          name: 'BASELAYER_NAME',
          layerType: LayerType.baseLayer,
          opacity: 1,
          enabled: true,
          mapId: 'map1',
        },
        'base_layer-3': {
          id: 'base_layer-1',
          name: 'BASELAYER_NAME',
          layerType: LayerType.baseLayer,
          opacity: 1,
          enabled: true,
          mapId: 'map2',
        },
        'base_layer-4': {
          id: 'base_layer-2',
          name: 'BASELAYER_NAME',
          layerType: LayerType.baseLayer,
          opacity: 1,
          enabled: true,
          mapId: 'map2',
        },
      },
      allIds: ['base_layer-1', 'base_layer-2'],
    },
  },
};

describe('store/mapStore/layers/selectors', () => {
  describe('getLayerById', () => {
    it('should return the layer when it exists', () => {
      expect(layerSelectors.getLayerById(testState, 'test-1')).toEqual(
        testState.layers.byId['test-1'],
      );
    });
    it('should return null when layerId does not exist', () => {
      expect(layerSelectors.getLayerById(testState, 'testing')).toBeNull();
    });
  });

  describe('getLayersById', () => {
    it('should return the layers when they exists', () => {
      const result = layerSelectors.getLayersById(testState);
      expect(result).toEqual(testState.layers.byId);
      expect(Object.keys(result)).toHaveLength(3);
    });
    it('should return null when store does not exist', () => {
      expect(layerSelectors.getLayersById(null)).toBeNull();
    });
  });

  describe('getLayersIds', () => {
    it('should return array of layer ids when existing', () => {
      const result = layerSelectors.getLayersIds(testState);
      expect(result).toEqual(testState.layers.allIds);
      expect(result).toHaveLength(3);
    });
    it('should return empty list when store does not exist', () => {
      expect(layerSelectors.getLayersIds(null)).toHaveLength(0);
    });
  });

  describe('getAllLayers', () => {
    it('should return array of all layer objects including baselayers', () => {
      const result = layerSelectors.getAllLayers(testState);
      expect(result).toEqual(
        testState.layers.allIds.map(
          (layerId) => testState.layers.byId[layerId],
        ),
      );
      expect(result).toHaveLength(3);
    });
    it('should return empty list when store does not exist', () => {
      expect(layerSelectors.getAllLayers(null)).toHaveLength(0);
    });
  });

  describe('getLayers', () => {
    it('should return array of layer objects that arent baselayers', () => {
      const result = layerSelectors.getLayers(testState);
      const nonBaselayerIds = testState.layers.allIds.filter(
        (layerId) =>
          testState.layers.byId[layerId].layerType !== LayerType.baseLayer &&
          testState.layers.byId[layerId].layerType !== LayerType.overLayer,
      );

      expect(result).toEqual(
        nonBaselayerIds.map((layerId) => testState.layers.byId[layerId]),
      );
      expect(result).toHaveLength(1);
    });
    it('should return empty list when store does not exist', () => {
      expect(layerSelectors.getLayers(null)).toHaveLength(0);
    });
  });

  describe('getBaseLayers', () => {
    it('should return array of baselayer objects', () => {
      const result = layerSelectors.getBaseLayers(testState);
      const baseLayerIds = testState.layers.allIds.filter(
        (layerId) =>
          testState.layers.byId[layerId].layerType === LayerType.baseLayer,
      );
      expect(result).toEqual(
        baseLayerIds.map((layerId) => testState.layers.byId[layerId]),
      );
      expect(result).toHaveLength(1);
    });
    it('should return empty list when store does not exist', () => {
      expect(layerSelectors.getBaseLayers(null)).toHaveLength(0);
    });
  });

  describe('getOverLayers', () => {
    it('should return array of overlayer objects', () => {
      const result = layerSelectors.getOverLayers(testState);
      const overLayerIds = testState.layers.allIds.filter(
        (layerId) =>
          testState.layers.byId[layerId].layerType === LayerType.overLayer,
      );
      expect(result).toEqual(
        overLayerIds.map((layerId) => testState.layers.byId[layerId]),
      );
      expect(result).toHaveLength(1);
    });
    it('should return empty list when store does not exist', () => {
      expect(layerSelectors.getOverLayers(null)).toHaveLength(0);
    });
  });

  describe('getLayerDimensions', () => {
    it('should return array of layer dimensions', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerDimensions(testState, layerId);

      expect(result).toEqual(testState.layers.byId[layerId].dimensions);
    });
    it('should return empty list when layer does not exist', () => {
      expect(
        layerSelectors.getLayerDimensions(testState, 'testlayer'),
      ).toHaveLength(0);
    });
    it('should return empty list when dimensions does not exist', () => {
      expect(
        layerSelectors.getLayerDimensions(testState, 'test-2'),
      ).toHaveLength(0);
    });
  });

  describe('getLayerTimeDimension', () => {
    it('should return a time dimension', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerTimeDimension(testState, layerId);

      expect(result).toEqual(testState.layers.byId[layerId].dimensions[0]);
    });
    it('should return empty object when layer does not exist', () => {
      expect(
        layerSelectors.getLayerTimeDimension(testState, 'no-exist'),
      ).toStrictEqual({});
    });
    it('should return empty object when dimensions does not exist', () => {
      expect(
        layerSelectors.getLayerTimeDimension(testState, 'test-2'),
      ).toStrictEqual({});
    });
  });

  describe('getLayerDimension', () => {
    it('should return a time dimension', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerDimension(
        testState,
        layerId,
        'elevation',
      );

      expect(result).toEqual(testState.layers.byId[layerId].dimensions[1]);
    });
    it('should return empty object when layer does not exist', () => {
      expect(
        layerSelectors.getLayerDimension(
          testState,
          'no-exist',
          'dimname-does-not-exist',
        ),
      ).toStrictEqual({});
    });
    it('should return empty object when dimension does not exist', () => {
      expect(
        layerSelectors.getLayerDimension(
          testState,
          'test-2',
          'dimname-does-not-exist',
        ),
      ).toStrictEqual({});
    });
  });

  describe('getLayerNonTimeDimensions', () => {
    it('should return array of layer dimensions without time', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerNonTimeDimensions(
        testState,
        layerId,
      );
      expect(result.length).toEqual(1);
      expect(result[0].name).toEqual('elevation');
    });
    it('should return empty list when layer does not exist', () => {
      expect(
        layerSelectors.getLayerNonTimeDimensions(testState, 'testlayer'),
      ).toHaveLength(0);
    });
    it('should return empty list when dimensions does not exist', () => {
      expect(
        layerSelectors.getLayerNonTimeDimensions(testState, 'test-2'),
      ).toHaveLength(0);
    });
    it('should return empty list when only time dimension exist', () => {
      expect(
        layerSelectors.getLayerNonTimeDimensions(testState, 'test-3'),
      ).toHaveLength(0);
    });
  });

  describe('getLayerOpacity', () => {
    it('should return the layer opacity', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerOpacity(testState, layerId);
      expect(result).toEqual(testState.layers.byId[layerId].opacity);
    });
    it('should return 0 when the layer has no opacity defined', () => {
      const layerId = 'test-2';
      const result = layerSelectors.getLayerOpacity(testState, layerId);
      expect(result).toEqual(0);
    });
    it('should return 0 when the layer does not exist', () => {
      const result = layerSelectors.getLayerOpacity(testState, 'fake-id');
      expect(result).toEqual(0);
    });
  });

  describe('getLayerEnabled', () => {
    it('should return the value of the layer enabled property', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerEnabled(testState, layerId);
      expect(result).toEqual(testState.layers.byId[layerId].enabled);
    });
    it('should return false when the layer has no enabled property', () => {
      const layerId = 'test-2';
      const result = layerSelectors.getLayerEnabled(testState, layerId);
      expect(result).toEqual(false);
    });
    it('should return null when the layer does not exist', () => {
      const result = layerSelectors.getLayerEnabled(testState, 'fake-id');
      expect(result).toEqual(false);
    });
  });

  describe('getLayerName', () => {
    it('should return the name of the layer', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerName(testState, layerId);
      expect(result).toEqual(testState.layers.byId[layerId].name);
    });
    it('should return an empty string when the layer has no name', () => {
      const layerId = 'test-2';
      const result = layerSelectors.getLayerName(testState, layerId);
      expect(result).toEqual('');
    });
    it('should return an empty string when the layer does not exist', () => {
      const result = layerSelectors.getLayerName(testState, 'fake-id');
      expect(result).toEqual('');
    });
  });

  describe('getLayerService', () => {
    it('should return the service name of the layer', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerService(testState, layerId);
      expect(result).toEqual(testState.layers.byId[layerId].service);
    });
    it('should return an empty string when the layer has no service', () => {
      const layerId = 'test-2';
      const result = layerSelectors.getLayerService(testState, layerId);
      expect(result).toEqual('');
    });
    it('should return an empty string when the layer does not exist', () => {
      const result = layerSelectors.getLayerService(testState, 'fake-id');
      expect(result).toEqual('');
    });
  });

  describe('getLayerStyle', () => {
    it('should return the layer style', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerStyle(testState, layerId);
      expect(result).toEqual(testState.layers.byId[layerId].style);
    });
    it('should return "" when the layer has no style defined', () => {
      const layerId = 'test-2';
      const result = layerSelectors.getLayerStyle(testState, layerId);
      expect(result).toEqual('');
    });
    it('should return 0 when the layer does not exist', () => {
      const result = layerSelectors.getLayerStyle(testState, 'fake-id');
      expect(result).toEqual('');
    });
  });

  describe('getLayerStatus', () => {
    it('should return the status the layer', () => {
      const layerId = 'test-1';
      const result = layerSelectors.getLayerStatus(testState, layerId);
      expect(result).toEqual(testState.layers.byId[layerId].status);
    });
    it('should return a default status when the layer has no status specified', () => {
      const layerId = 'test-2';
      const result = layerSelectors.getLayerStatus(testState, layerId);
      expect(result).toEqual(LayerStatus.default);
    });
  });

  describe('getAvailableBaseLayersForMap', () => {
    it('should get a list of the available base layers for the map passed in', () => {
      const result = layerSelectors.getAvailableBaseLayersForMap(
        testState,
        'map1',
      );
      expect(result).toEqual(
        Object.values([
          testState.layers.availableBaseLayers.byId['base_layer-1'],
          testState.layers.availableBaseLayers.byId['base_layer-2'],
        ]),
      );
    });
    it('should return an empty array if no available base layers', () => {
      const emptyAvailBaseLayersState = {
        layers: {
          byId: {
            'test-1': {
              id: 'test-1',
              name: 'LAYER_NAME',
              dimensions: [
                { name: 'time', units: 'ISO8601', currentValue: 'someTimeVal' },
              ],
              layerType: LayerType.mapLayer,
              opacity: 0.5,
              enabled: true,
              service: 'test.service.com',
              status: LayerStatus.error,
            },
            'test-2': {
              id: 'test-2',
              layerType: LayerType.baseLayer,
            },
            'test-3': {
              id: 'test-3',
              layerType: LayerType.overLayer,
            },
          },
          allIds: ['test-1', 'test-2', 'test-3'],
          availableBaseLayers: {
            byId: {},
            allIds: [],
          },
        },
      };
      const result = layerSelectors.getAvailableBaseLayersForMap(
        emptyAvailBaseLayersState,
        'map2',
      );
      expect(result).toEqual([]);
    });
  });
});
