/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { createSelector } from '@reduxjs/toolkit';
import { AppStore } from '../../../types/types';
import { selectorMemoizationOptions } from '../../utils';
import { Dimension } from '../types';
import { filterNonTimeDimensions } from '../utils/helpers';
import { LayerState, Layer, LayerStatus, LayerType } from './types';

const layerStore = (store: AppStore): LayerState =>
  store && store.layers ? store.layers : null;

/**
 * Gets a layer from the layers part of the store by its Id
 *
 * Example: layer = getLayerById(store, 'layerId')
 * @param {object} store object from which the layer state will be extracted
 * @param {string} layerId Id of the layer
 * @returns {object} object containing layer information (service, name, style, enabled etc.)
 */
export const getLayerById = (store: AppStore, layerId: string): Layer => {
  if (store && store.layers && store.layers.byId[layerId]) {
    return store.layers.byId[layerId];
  }
  return null;
};

/**
 * Retrieves all layers indexed by layerId
 *
 * Example: layers = getLayersById(store)
 * @param {object} store store: object - object from which the layers state will be extracted
 * @returns {object} returnType: object - an object of all layers containing layer information (service, name, style, enabled etc.) indexed by layerId
 */
export const getLayersById = createSelector(
  layerStore,
  (store) => (store ? store.byId : null),
  selectorMemoizationOptions,
);

/**
 * Retrieves all layerIds
 *
 * Example: layerIds = getLayersIds(store)
 * @param {object} store store: object - object from which the layers state will be extracted
 * @returns {array} returnType: array - an array of all layerIds
 */
export const getLayersIds = createSelector(
  layerStore,
  (store) => (store ? store.allIds : []),
  selectorMemoizationOptions,
);

/**
 * Retrieves all layers (including base layers and overlayers)
 *
 * Example: layers = getAllLayers(store)
 * @param {object} store store: object - object from which the layers state will be extracted
 * @returns {array} returnType: array - an array of all layers containing layer information (service, name, style, enabled etc.)
 */
export const getAllLayers = createSelector(
  getLayersIds,
  getLayersById,
  (ids, layers) => ids.map((layerId) => layers[layerId]),
  selectorMemoizationOptions,
);

/**
 * Retrieves all layers that aren't base layers and aren't overlayers
 *
 * Example: layers = getLayers(store)
 * @param {object} store store: object - object from which the layers state will be extracted
 * @returns {array} returnType: array - an array of all non-baselayers containing layer information (service, name, style, enabled etc.)
 */
export const getLayers = createSelector(
  getAllLayers,
  (layers) =>
    layers.filter(
      (layer) =>
        layer.layerType !== LayerType.baseLayer &&
        layer.layerType !== LayerType.overLayer,
    ),
  selectorMemoizationOptions,
);

/**
 * Retrieves all baselayers
 *
 * Example: layers = getBaseLayers(store)
 * @param {object} store store: object - object from which the layers state will be extracted
 * @returns {array} returnType: array - an array of all baselayers containing layer information (service, name, style, enabled etc.)
 */
export const getBaseLayers = createSelector(
  getAllLayers,
  (layers) => layers.filter((layer) => layer.layerType === LayerType.baseLayer),
  selectorMemoizationOptions,
);

/**
 * Retrieves all overLayers
 *
 * Example: layers = getOverLayers(store)
 * @param {object} store store: object - object from which the layers state will be extracted
 * @returns {array} returnType: array - an array of all overLayers containing layer information (service, name, style, enabled etc.)
 */
export const getOverLayers = createSelector(
  getAllLayers,
  (layers) => layers.filter((layer) => layer.layerType === LayerType.overLayer),
  selectorMemoizationOptions,
);

/**
 * Gets dimensions of the passed layer
 *
 * Example: layerDimensions = getLayerDimensions(store, 'layerId_1')
 * @param {object} store store: object - object from which the layers state will be extracted
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {array} returnType: Dimension[] - array containing layer dimensions
 */
export const getLayerDimensions = createSelector(
  getLayerById,
  (layer) => (layer && layer.dimensions ? layer.dimensions : []),
  selectorMemoizationOptions,
);

/**
 * Gets dimensions of the passed layer, excluding the time dimension
 *
 * Example: layerNonTimeDimensions = getLayerNonTimeDimensions(store, 'layerId_1')
 * @param {object} store store: object - object from which the layers state will be extracted
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {array} returnType: Dimension[] - array containing layer dimensions, excluding the time dimension
 */
export const getLayerNonTimeDimensions = createSelector(
  getLayerDimensions,
  (dimensions) => filterNonTimeDimensions(dimensions),
  selectorMemoizationOptions,
);

/**
 * Gets time dimension of the passed layer
 *
 * Example: timeDimension = getLayerTimeDimension(store, 'layerId_1')
 * @param {object} store store: object - object from which the layers state will be extracted
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {object} returnType: object - object of layer time dimension
 */
export const getLayerTimeDimension = createSelector(
  getLayerDimensions,
  (dimensions) =>
    dimensions && dimensions.length
      ? dimensions.find((dimension) => dimension.name === 'time')
      : ({} as Dimension),
  selectorMemoizationOptions,
);

/**
 * Gets specified dimension of the passed layer
 *
 * Example: dimension = getLayerDimension(store, 'layerId_1', 'elevation')
 * @param {object} store store: object - object from which the layers state will be extracted
 * @param {string} layerId layerId: string - Id of the layer
 * @param {string} dimensionName dimensionName: string - name of dimension you want to retrieve the dimension data for
 * @returns {object} returnType: object - object of layer dimension
 */
export const getLayerDimension = createSelector(
  getLayerDimensions,
  (_store, _layerId: string, dimensionName: string): string => dimensionName,
  (dimensions, dimensionName): Dimension =>
    dimensions && dimensions.length
      ? dimensions.find((dimension) => dimension.name === dimensionName)
      : ({} as Dimension),
  selectorMemoizationOptions,
);

/**
 * Gets opacity of the passed layer
 *
 * Example: layerOpacity = getLayerOpacity(store, 'layerId_1')
 * @param {object} store store: object - object from which the layers state will be extracted
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {number} returnType: number - opacity as a number (between 0 and 1)
 */
export const getLayerOpacity = createSelector(getLayerById, (layer) =>
  layer && layer.opacity ? layer.opacity : 0,
);

/**
 * Gets whether a layer is enabled or disabled
 *
 * Example: isLayerEnabled = getLayerEnabled(store, 'layerId_1')
 * @param {object} store store: object - object from which the layers state will be extracted
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {boolean} returnType: boolean -  true if enabled, false if disabled
 */
export const getLayerEnabled = createSelector(getLayerById, (layer) =>
  layer && layer.enabled ? layer.enabled : false,
);

/**
 * Gets layer name
 *
 * Example: layerName = getLayerName(store, 'layerId_1')
 * @param {object} store store: object - object from which the layers state will be extracted
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {string} returnType: string -  layer name
 */
export const getLayerName = createSelector(getLayerById, (layer) =>
  layer && layer.name && layer.name.length > 0 ? layer.name : '',
);

/**
 * Gets layer service
 *
 * Example: layerService = getLayerService(store, 'layerId_1')
 * @param {object} store store: object - object from which the layers state will be extracted
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {string} returnType: string -  layer service
 */
export const getLayerService = createSelector(getLayerById, (layer) =>
  layer && layer.service ? layer.service : '',
);

/**
 * Gets selected style of the passed layer
 *
 * Example: layerStyle = getLayerStyle(store, 'layerId_1')
 * @param {object} store store: object - object from which the layers state will be extracted
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {string} returnType: string - style that is currently selected
 */
export const getLayerStyle = createSelector(getLayerById, (layer) =>
  layer && layer.style ? layer.style : '',
);

/**
 * Gets layer status
 *
 * Example: layerService = getLayerStatus(store, 'layerId_1')
 * @param {object} store store: object - object from which the layers state will be extracted
 * @param {string} layerId layerId: string - Id of the layer
 * @returns {string} returnType: LayerStatus
 */
export const getLayerStatus = createSelector(getLayerById, (layer) =>
  layer && layer.status ? layer.status : LayerStatus.default,
);

/**
 * Gets all available base layers for a map
 *
 * Example: availableBaseLayers = getAvailableBaseLayersForMap(store)
 * @param {object} store store: object - object from which the layer state will be extracted
 * @param {string} mapId mapId: string - Id of the map we want to retrieve the available baselayers for
 * @returns {array} returnType: array - array containing all available base layers
 */
export const getAvailableBaseLayersForMap = createSelector(
  layerStore,
  (_store, _mapId: string): string => _mapId,
  (layerStore, mapId: string): Layer[] => {
    if (layerStore && layerStore.availableBaseLayers) {
      const { availableBaseLayers } = layerStore;
      return availableBaseLayers.allIds.reduce((list, layerId) => {
        if (availableBaseLayers.byId[layerId].mapId === mapId) {
          return list.concat(availableBaseLayers.byId[layerId]);
        }
        return list;
      }, []);
    }
    return [];
  },
  selectorMemoizationOptions,
);
