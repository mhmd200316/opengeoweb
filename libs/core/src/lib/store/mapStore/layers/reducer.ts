/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { current } from 'immer';
import { WMJSDimension } from '@opengeoweb/webmap';

import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import {
  LayerState,
  Layer,
  LayerType,
  LayerStatus,
  AddLayerPayload,
  SetLayerDimensionPayload,
  SetLayerEnabledPayload,
  SetLayerOpacityPayload,
  SetLayerStylePayload,
  SetLayerNamePayload,
  SetLayerGeojsonPayload,
  DeleteLayerPayload,
  ErrorLayerPayload,
  SetLayersPayload,
  SetBaseLayersPayload,
  AddBaseLayerPayload,
  SetLayerDimensionsPayload,
  AddAvailableBaseLayerPayload,
  AddAvailableBaseLayersPayload,
  UpdateLayerInfoPayload,
  SetAvailableBaseLayersPayload,
} from './types';
import { Dimension } from '../map/types';
import { checkValidLayersPayload } from '../map/utils';
import { produceDraftStateForAllLayersForDimensionWithinMap } from './utils';
import { getWMLayerById } from '../utils/helpers';
import { SyncLayerPayloads } from '../../generic/types';
import { mapChangeDimension, setMapPreset } from '../map/actions';
import {
  setLayerActionSync,
  setTimeSync,
} from '../../generic/synchronizationActions/actions';

export const createLayer = ({
  id,
  opacity = 1,
  enabled = true,
  layerType = 'mapLayer' as LayerType,
  status = 'default' as LayerStatus,
  ...props
}: Layer): Layer => {
  const wmjsLayer = getWMLayerById(id);
  const dimensions =
    props.dimensions ||
    (wmjsLayer ? wmjsLayer.getDimensions() : []).map((dim: WMJSDimension) => ({
      name: dim.name,
      currentValue: dim.currentValue,
      units: dim.units,
    }));

  return {
    ...props,
    dimensions,
    id,
    opacity,
    enabled,
    layerType,
    status,
  };
};

export const initialState: LayerState = {
  byId: {},
  allIds: [],
  availableBaseLayers: { byId: {}, allIds: [] },
};

export const slice = createSlice({
  initialState,
  name: 'layerReducer',
  reducers: {
    addLayer: (draft, action: PayloadAction<AddLayerPayload>) => {
      const { layer, layerId, mapId } = action.payload;
      if (!checkValidLayersPayload([layer], mapId)) {
        return;
      }

      if (!draft.byId[layerId]) {
        // TODO: (Sander de Snaijer, 2020-03-19) remove layerIds from the layer utils
        draft.byId[layerId] = createLayer({ ...layer, id: layerId, mapId });
        draft.allIds.push(layerId);
      }
    },
    layerChangeDimension: (
      draft,
      action: PayloadAction<SetLayerDimensionPayload>,
    ) => {
      const { layerId: layerIdFromAction, dimension } = action.payload;
      const layerFromAction = draft.byId[layerIdFromAction];
      if (!layerFromAction) return;

      const { mapId } = layerFromAction;

      produceDraftStateForAllLayersForDimensionWithinMap(
        draft,
        dimension,
        mapId,
        layerIdFromAction,
      );
    },
    layerChangeEnabled: (
      draft,
      action: PayloadAction<SetLayerEnabledPayload>,
    ) => {
      const { layerId, enabled } = action.payload;
      if (draft.byId[layerId]) {
        draft.byId[layerId].enabled = enabled;
      }
    },
    layerChangeOpacity: (
      draft,
      action: PayloadAction<SetLayerOpacityPayload>,
    ) => {
      const { layerId, opacity } = action.payload;
      if (draft.byId[layerId]) {
        draft.byId[layerId].opacity = opacity;
      }
    },
    layerChangeStyle: (draft, action: PayloadAction<SetLayerStylePayload>) => {
      const { layerId, style } = action.payload;
      if (draft.byId[layerId]) {
        draft.byId[layerId].style = style;
      }
    },
    layerChangeName: (draft, action: PayloadAction<SetLayerNamePayload>) => {
      const { layerId, name } = action.payload;
      if (draft.byId[layerId]) {
        draft.byId[layerId].name = name;
      }
    },
    layerChangeGeojson: (
      draft,
      action: PayloadAction<SetLayerGeojsonPayload>,
    ) => {
      const { layerId, geojson } = action.payload;
      if (draft.byId[layerId]) {
        draft.byId[layerId].geojson = geojson;
      }
    },
    layerDelete: (draft, action: PayloadAction<DeleteLayerPayload>) => {
      const { layerId } = action.payload;
      if (draft.byId[layerId]) {
        draft.allIds = draft.allIds.filter((id) => id !== layerId);
        delete draft.byId[layerId];
      }
    },
    layerError: (draft, action: PayloadAction<ErrorLayerPayload>) => {
      const { layerId } = action.payload;
      if (draft.byId[layerId]) {
        draft.byId[layerId].status = LayerStatus.error;
      }
    },
    baseLayerDelete: (draft, action: PayloadAction<DeleteLayerPayload>) => {
      const { layerId } = action.payload;
      if (
        draft.byId[layerId].layerType !== LayerType.baseLayer &&
        draft.byId[layerId].layerType !== LayerType.overLayer
      )
        return;

      if (draft.byId[layerId]) {
        draft.allIds = draft.allIds.filter((id) => id !== layerId);
        delete draft.byId[layerId];
      }
    },
    setLayers: (draft, action: PayloadAction<SetLayersPayload>) => {
      const { layers, mapId } = action.payload;
      if (!checkValidLayersPayload(layers, mapId)) {
        return;
      }

      /* 
          All layer id's for the specified mapId should be removed. 
          This is done by clearing the allId object, and then adding back the layer ids for the other maps we want to keep.
          At the same time the byId object is synchronized. 
        */
      const state = current(draft);
      draft.allIds = [];
      state.allIds.forEach((layerId) => {
        if (
          state.byId[layerId] &&
          state.byId[layerId].layerType !== LayerType.baseLayer &&
          state.byId[layerId].layerType !== LayerType.overLayer &&
          state.byId[layerId].mapId === mapId
        ) {
          delete draft.byId[layerId];
        } else {
          draft.allIds.push(layerId);
        }
      });

      /*
          Here we set the layers for the mapId from the action. byId and allIds is updated. 
        */
      layers.forEach((layer) => {
        draft.byId[layer.id] = createLayer({ id: layer.id, mapId, ...layer });
        draft.allIds.push(layer.id);
      });
    },
    setBaseLayers: (draft, action: PayloadAction<SetBaseLayersPayload>) => {
      const { layers, mapId } = action.payload;
      const filtererdBaseLayers = layers.filter(
        (layer) =>
          layer.layerType === LayerType.baseLayer ||
          layer.layerType === LayerType.overLayer,
      );

      // filter for unique layerTypes
      const layerTypes = [];
      filtererdBaseLayers.forEach((layer) => {
        if (
          layer.layerType !== LayerType.baseLayer &&
          layer.layerType !== LayerType.overLayer
        )
          return;
        if (!layerTypes.includes(layer.layerType))
          layerTypes.push(layer.layerType);
      });

      const state = current(draft);
      // remove current layers with same type as one of the passed layers
      state.allIds.forEach((layerId) => {
        if (
          state.byId[layerId] &&
          layerTypes.includes(state.byId[layerId].layerType) &&
          state.byId[layerId].mapId === mapId
        ) {
          delete draft.byId[layerId];
          const index = draft.allIds.indexOf(layerId);
          draft.allIds.splice(index, 1);
        }
      });
      // set over and base layers
      filtererdBaseLayers.forEach((layer) => {
        draft.byId[layer.id] = createLayer({
          id: layer.id,
          layerType: layer.layerType,
          mapId,
          ...layer,
        });
        draft.allIds.push(layer.id);
      });
    },
    addBaseLayer: (draft, action: PayloadAction<AddBaseLayerPayload>) => {
      const { layer } = action.payload;
      if (
        layer.layerType !== LayerType.baseLayer &&
        layer.layerType !== LayerType.overLayer
      )
        return;

      if (!draft.byId[layer.id]) {
        draft.byId[layer.id] = createLayer({
          id: layer.id,
          layerType: layer.layerType,
          ...layer,
        });
        draft.allIds.push(layer.id);
      }
    },
    layerSetDimensions: (
      draft,
      action: PayloadAction<SetLayerDimensionsPayload>,
    ) => {
      const { dimensions, layerId } = action.payload;

      if (draft.byId[layerId]) {
        draft.byId[layerId].dimensions = dimensions;
      }
    },
    addAvailableBaseLayer: (
      draft,
      action: PayloadAction<AddAvailableBaseLayerPayload>,
    ) => {
      const { layer } = action.payload;
      if (
        (layer.layerType !== LayerType.baseLayer &&
          layer.layerType !== LayerType.overLayer) ||
        !layer.mapId
      )
        return;

      if (!draft.availableBaseLayers.byId[layer.id]) {
        draft.availableBaseLayers.byId[layer.id] = createLayer({
          id: layer.id,
          layerType: layer.layerType,
          ...layer,
        });
        draft.availableBaseLayers.allIds.push(layer.id);
      }
    },
    addAvailableBaseLayers: (
      draft,
      action: PayloadAction<AddAvailableBaseLayersPayload>,
    ) => {
      const { layers } = action.payload;

      // add new available baselayers
      layers.forEach((layer) => {
        if (
          layer.layerType !== LayerType.baseLayer ||
          draft.availableBaseLayers.byId[layer.id] ||
          !layer.mapId
        )
          return;

        draft.availableBaseLayers.byId[layer.id] = createLayer({
          id: layer.id,
          layerType: layer.layerType,
          ...layer,
        });
        draft.availableBaseLayers.allIds.push(layer.id);
      });
    },
    setAvailableBaseLayers: (
      draft,
      action: PayloadAction<SetAvailableBaseLayersPayload>,
    ) => {
      const { layers } = action.payload;

      draft.availableBaseLayers.byId = {};
      draft.availableBaseLayers.allIds = [];

      // add new available baselayers
      layers.forEach((layer) => {
        if (layer.layerType !== LayerType.baseLayer || !layer.mapId) return;

        draft.availableBaseLayers.byId[layer.id] = createLayer({
          ...layer,
        });
        draft.availableBaseLayers.allIds.push(layer.id);
      });
    },
    onUpdateLayerInformation: (
      draft,
      action: PayloadAction<UpdateLayerInfoPayload>,
    ) => {
      const { layerStyle, layerDimensions } = action.payload;
      const layerStyleAction = layerActions.layerChangeStyle(layerStyle);
      const state: LayerState = current(draft);
      const intermediateState: LayerState =
        layerStyle === null ? state : reducer(state, layerStyleAction);

      const dimensionsAction = layerActions.layerSetDimensions(layerDimensions);

      const newState: LayerState =
        layerDimensions === null
          ? intermediateState
          : reducer(intermediateState, dimensionsAction);
      return newState;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(setTimeSync, (draft, action) => {
        const { targets: targetsFromAction, source } = action.payload;
        /* Because we want backwards compatibility with the previous code, we also need to listen to the original source action */
        const targets = [
          {
            targetId: source.payload.sourceId,
            value: source.payload.value,
          },
        ];
        /* And then append the targets form the action */
        targets.push(...targetsFromAction);
        targets.forEach((payload) => {
          const { targetId, value } = payload;
          const dimension: Dimension = {
            name: 'time',
            currentValue: value,
          };
          produceDraftStateForAllLayersForDimensionWithinMap(
            draft,
            dimension,
            targetId,
            null,
          );
        });
      })
      .addCase(setLayerActionSync, (draft, action) => {
        /*
         * This GENERIC_SYNC_SETLAYERACTIONS action is generated by the syncgroup saga.
         * It has multiple targets (Layers) in its payload.
         * These targets can be used as payloads in new Layer actions.
         * These actions are here handled via the layer reducer, as it is the same logic
         */
        const { targets, source } = action.payload;
        const state = current(draft);
        return targets.reduce(
          (prevState: LayerState, target: SyncLayerPayloads): LayerState => {
            const action = {
              payload: target,
              type: source.type,
            };
            /* Handle the Layer action with the same logic, using the same reducer */
            return reducer(prevState, action);
          },
          state,
        );
      })
      .addCase(setMapPreset, (draft, action) => {
        const { mapId } = action.payload;
        const layers = draft.allIds
          .map((id) => draft.byId[id])
          .filter((layer) => layer && layer.mapId === mapId);

        layers.forEach((layer) => {
          delete draft.byId[layer.id];
          const index = draft.allIds.indexOf(layer.id);
          draft.allIds.splice(index, 1);
        });
      })
      .addCase(mapChangeDimension, (draft, action) => {
        const { mapId } = action.payload;
        produceDraftStateForAllLayersForDimensionWithinMap(
          draft,
          action.payload.dimension,
          mapId,
          null,
        );
      });
  },
});

export const { reducer } = slice;
export const layerActions = slice.actions;

export type LayerActions =
  | ReturnType<typeof layerActions.layerSetDimensions>
  | ReturnType<typeof layerActions.layerChangeStyle>
  | ReturnType<typeof layerActions.addAvailableBaseLayer>
  | ReturnType<typeof layerActions.addAvailableBaseLayers>
  | ReturnType<typeof layerActions.addBaseLayer>
  | ReturnType<typeof layerActions.addLayer>
  | ReturnType<typeof layerActions.baseLayerDelete>
  | ReturnType<typeof layerActions.onUpdateLayerInformation>
  | ReturnType<typeof layerActions.layerChangeDimension>
  | ReturnType<typeof layerActions.layerChangeEnabled>
  | ReturnType<typeof layerActions.layerChangeGeojson>
  | ReturnType<typeof layerActions.layerChangeName>
  | ReturnType<typeof layerActions.layerChangeOpacity>
  | ReturnType<typeof layerActions.layerDelete>
  | ReturnType<typeof layerActions.layerError>
  | ReturnType<typeof layerActions.setBaseLayers>
  | ReturnType<typeof layerActions.setLayers>;
