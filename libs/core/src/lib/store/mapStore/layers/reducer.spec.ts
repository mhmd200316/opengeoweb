/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { WMLayer } from '@opengeoweb/webmap';
import { FeatureCollection } from 'geojson';
import { AnyAction } from '@reduxjs/toolkit';
import { reducer as layerReducer, initialState, createLayer } from './reducer';
import { layerActions } from '.';
import { LayerType, LayerStatus, LayerActionOrigin } from './types';
import {
  createLayersState,
  createMultipleLayersState,
} from '../../../utils/testUtils';
import {
  WmMultiDimensionLayer,
  WmMultiDimensionLayer2,
} from '../../../utils/defaultTestSettings';
import { registerWMLayer } from '../utils/helpers';
import { mapActions } from '../../..';

describe('store/mapStore/layers/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(layerReducer(undefined, {})).toEqual(initialState);
  });

  it('should add layer', () => {
    const layer = {
      name: 'test',
      layerType: LayerType.mapLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addLayer({
        layerId,
        mapId: 'map-1',
        layer,
        origin: LayerActionOrigin.layerManager,
      }),
    );

    expect(result.byId[layerId]).toEqual(
      createLayer({ ...layer, id: layerId, mapId: 'map-1' }),
    );
    expect(result.allIds.includes(layerId)).toBeTruthy();
  });

  it('should add baselayer', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.baseLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addBaseLayer({
        layerId,
        mapId: 'map-1',
        layer,
      }),
    );

    expect(result.byId[layerId]).toEqual({
      ...layer,
      id: layerId,
      dimensions: [],
      layerType: LayerType.baseLayer,
      enabled: true,
      opacity: 1,
      status: LayerStatus.default,
    });
    expect(result.allIds.includes(layerId)).toBeTruthy();
  });

  it('should add overlayer', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.overLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addBaseLayer({
        layerId,
        mapId: 'map-1',
        layer,
      }),
    );

    expect(result.byId[layerId]).toEqual({
      ...layer,
      id: layerId,
      dimensions: [],
      layerType: LayerType.overLayer,
      enabled: true,
      opacity: 1,
      status: LayerStatus.default,
    });
    expect(result.allIds.includes(layerId)).toBeTruthy();
  });

  it('addBaseLayer should only accept over and base layers', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.mapLayer,
    };
    const layerId = 'test-1';
    const result = layerReducer(
      undefined,
      layerActions.addBaseLayer({
        layerId,
        mapId: 'map-1',
        layer,
      }),
    );

    expect(result).toEqual(initialState);
  });

  it('should clear all layers for a map when setting a mapPreset', () => {
    const initialMockState = {
      byId: {
        layer_1: {
          id: 'layer_1',
          mapId: 'map-1',
        },
        layer_2: {
          id: 'layer_2',
          mapId: 'map-1',
        },
        layer_3: {
          id: 'layer_3',
          mapId: 'map-2',
        },
      },
      allIds: ['layer_1', 'layer_2', 'layer_3'],
      availableBaseLayers: {
        byId: {},
        allIds: [],
      },
    };

    const result = layerReducer(
      initialMockState,
      mapActions.setMapPreset({
        mapId: 'map-1',
        initialProps: {
          mapPreset: {},
        },
      }),
    );

    expect(result.byId.layer_1).toBeUndefined();
    expect(result.byId.layer_2).toBeUndefined();
    expect(result.byId.layer_3).toEqual(initialMockState.byId.layer_3);
    expect(result.allIds.length).toEqual(1);
  });

  it('should change dimensions', () => {
    const layerId = 'test-1';
    const initialDimension = { name: 'test-dim', currentValue: 'initialvalue' };
    const initialMockState = createLayersState(layerId, {
      mapId: 'mapid1',
      dimensions: [initialDimension],
    });

    // initial value
    expect(initialMockState.byId[layerId].dimensions).toEqual([
      initialDimension,
    ]);

    // change dimensions
    const newDimension = { name: 'test-dim', currentValue: 'newvalue' };
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId,
        dimension: newDimension,
      }),
    );

    expect(result.byId[layerId].dimensions).toEqual([
      { ...newDimension, validSyncSelection: true },
    ]);
  });

  it('should change dimension sync state if passed', () => {
    const layerId = 'test-1';
    const initialDimension = {
      name: 'test-dim',
      currentValue: 'initialvalue',
      synced: false,
    };
    const initialMockState = createLayersState(layerId, {
      mapId: 'mapid1',
      dimensions: [initialDimension],
    });

    // initial value
    expect(initialMockState.byId[layerId].dimensions).toEqual([
      initialDimension,
    ]);

    // change dimensions
    const newDimension = {
      name: 'test-dim',
      currentValue: 'initialvalue',
      synced: true,
    };
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId,
        dimension: newDimension,
      }),
    );

    expect(result.byId[layerId].dimensions).toEqual([
      { ...newDimension, validSyncSelection: true },
    ]);

    const newDimension2 = {
      name: 'test-dim',
      currentValue: 'initialvalue',
      synced: false,
    };
    const result2 = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId,
        dimension: newDimension2,
      }),
    );

    expect(result2.byId[layerId].dimensions).toEqual([
      { ...newDimension2, validSyncSelection: true },
    ]);
  });

  it('should extend dimensions', () => {
    const layerId = 'test-1';
    const initialDimension = { name: 'test-start', currentValue: 'test-start' };
    const initialMockState = createLayersState(layerId, {
      mapId: 'mapid1',
      dimensions: [initialDimension],
    });

    // initial value
    expect(initialMockState.byId[layerId].dimensions).toEqual([
      initialDimension,
    ]);

    // change dimensions
    const newDimension = { name: 'test-end', currentValue: 'test-end' };
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId,
        dimension: newDimension,
      }),
    );

    expect(result.byId[layerId].dimensions).toEqual([
      initialDimension,
      { ...newDimension, validSyncSelection: true },
    ]);
  });

  it('should update the dimension for other layers on the map that have synced: true, layers on other maps should not change', () => {
    const dimensionLayer1 = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
          synced: true,
        },
      ],
    };
    const dimensionLayer2 = {
      service: 'https://testservicedimensions',
      id: 'multiDimensionLayerMock2',
      name: 'netcdf_5dims',
      layerType: LayerType.mapLayer,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
          synced: true,
        },
      ],
    };
    const dimensionLayer3 = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock3',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
          synced: true,
        },
      ],
    };
    const layers = [dimensionLayer1, dimensionLayer2, dimensionLayer3];
    registerWMLayer(WmMultiDimensionLayer2, 'multiDimensionLayerMock2');
    registerWMLayer(WmMultiDimensionLayer, 'multiDimensionLayerMock');
    const mapId = 'mapId_1';
    const initialMockState = createMultipleLayersState(layers, mapId);
    // Change mapId for third layer
    initialMockState.byId[dimensionLayer3.id].mapId = 'mapId_2';

    // initial value
    expect(initialMockState.byId[dimensionLayer1.id].dimensions).toEqual(
      dimensionLayer1.dimensions,
    );
    expect(initialMockState.byId[dimensionLayer2.id].dimensions).toEqual(
      dimensionLayer2.dimensions,
    );

    // change dimension
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId: dimensionLayer1.id,
        dimension: {
          name: 'elevation',
          units: 'meters',
          currentValue: '5000',
        },
      }),
    );

    expect(result.byId[dimensionLayer1.id].dimensions[0].currentValue).toEqual(
      '5000',
    );
    expect(result.byId[dimensionLayer2.id].dimensions[0].currentValue).toEqual(
      '5000',
    );
    expect(result.byId[dimensionLayer3.id].dimensions[0].currentValue).toEqual(
      '9000',
    );
  });

  it('should not update the dimension for other layers on the map that have synced: false', () => {
    const dimensionLayer1 = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
          synced: true,
        },
      ],
    };
    const dimensionLayer2 = {
      service: 'https://testservicedimensions',
      id: 'multiDimensionLayerMock2',
      name: 'netcdf_5dims',
      layerType: LayerType.mapLayer,
      dimensions: [
        {
          name: 'elevation',
          units: 'meters',
          currentValue: '9000',
          values: '1000,5000,9000',
          synced: false,
        },
      ],
    };
    const layers = [dimensionLayer1, dimensionLayer2];
    registerWMLayer(WmMultiDimensionLayer2, 'multiDimensionLayerMock2');
    registerWMLayer(WmMultiDimensionLayer, 'multiDimensionLayerMock');
    const mapId = 'mapId_1';
    const initialMockState = createMultipleLayersState(layers, mapId);

    // initial value
    expect(initialMockState.byId[dimensionLayer1.id].dimensions).toEqual(
      dimensionLayer1.dimensions,
    );
    expect(initialMockState.byId[dimensionLayer2.id].dimensions).toEqual(
      dimensionLayer2.dimensions,
    );

    // change dimension
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId: dimensionLayer1.id,
        dimension: {
          name: 'elevation',
          units: 'meters',
          currentValue: '5000',
        },
      }),
    );

    expect(result.byId[dimensionLayer1.id].dimensions[0].currentValue).toEqual(
      '5000',
    );
    expect(result.byId[dimensionLayer2.id].dimensions[0].currentValue).toEqual(
      '9000',
    );
  });

  it('should also update linked dimensions', () => {
    const dimensionLayer1 = {
      service: 'https://testservice',
      id: 'multiDimensionLayerMock',
      name: 'MULTI_DIMENSION_LAYER',
      title: 'MULTI_DIMENSION_LAYER',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2017-01-01T00:25:00Z',
          values:
            '2017-01-01T00:25:00Z,2017-01-01T00:30:00Z,2017-01-01T00:35:00Z',
        },
      ],
    };
    const dimensionLayer2 = {
      service: 'https://testservicedimensions',
      id: 'multiDimensionLayerMock2',
      name: 'netcdf_5dims',
      layerType: LayerType.mapLayer,
      dimensions: [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2017-01-01T00:25:00Z',
          values:
            '2017-01-01T00:25:00Z,2017-01-01T00:30:00Z,2017-01-01T00:35:00Z',
        },
      ],
    };
    const layers = [dimensionLayer1, dimensionLayer2];
    registerWMLayer(new WMLayer(dimensionLayer2), 'multiDimensionLayerMock2');
    registerWMLayer(new WMLayer(dimensionLayer1), 'multiDimensionLayerMock');
    const mapId = 'mapId_1';
    const initialMockState = createMultipleLayersState(layers, mapId);

    // initial value
    expect(initialMockState.byId[dimensionLayer1.id].dimensions).toEqual(
      dimensionLayer1.dimensions,
    );
    expect(initialMockState.byId[dimensionLayer2.id].dimensions).toEqual(
      dimensionLayer2.dimensions,
    );

    // change dimension
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId: dimensionLayer1.id,
        dimension: {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2017-01-01T00:35:00Z',
        },
      }),
    );

    expect(result.byId[dimensionLayer1.id].dimensions[0].currentValue).toEqual(
      '2017-01-01T00:35:00Z',
    );
    expect(result.byId[dimensionLayer2.id].dimensions[0].currentValue).toEqual(
      '2017-01-01T00:35:00Z',
    );
  });

  it('should enable layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      enabled: false,
    });

    // initial value
    expect(initialMockState.byId[layerId].enabled).toBeFalsy();

    // enable layer
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeEnabled({
        layerId,
        enabled: true,
      }),
    );

    expect(result.byId[layerId].enabled).toBeTruthy();
  });

  it('should disable layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      enabled: true,
    });

    // initial value
    expect(initialMockState.byId[layerId].enabled).toBeTruthy();

    // disable layer
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeEnabled({
        layerId,
        enabled: false,
      }),
    );

    expect(result.byId[layerId].enabled).toBeFalsy();
  });

  it('should change opacity of a layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      opacity: 0.3,
    });

    // initial state
    expect(initialMockState.byId[layerId].opacity).toEqual(0.3);

    // change opacity
    const opacity = 0.5;
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeOpacity({
        layerId,
        opacity,
      }),
    );

    expect(result.byId[layerId].opacity).toEqual(opacity);
  });

  it('should change style of a layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      style: 'initial-style',
    });

    // initial state
    expect(initialMockState.byId[layerId].style).toEqual('initial-style');

    // change style
    const style = 'new-style';
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeStyle({
        layerId,
        style,
      }),
    );

    expect(result.byId[layerId].style).toEqual(style);
  });

  it('should change name of a layer', () => {
    const layerId = 'test-1';
    const initialMockState = createLayersState(layerId, {
      name: 'initial-name',
    });

    // initial state
    expect(initialMockState.byId[layerId].name).toEqual('initial-name');

    // change name
    const name = 'new-name';
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeName({
        layerId,
        name,
      }),
    );

    expect(result.byId[layerId].name).toEqual(name);
  });

  it('should change geojson of layer', () => {
    const layerId = 'layerId';
    const geojsonBefore: FeatureCollection = {
      type: 'FeatureCollection',
      features: [],
    };
    const initialMockState = createLayersState(layerId, {
      geojson: geojsonBefore,
    });
    expect(initialMockState.byId[layerId].geojson).toEqual(geojsonBefore);

    const geojsonAfter: FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: { type: 'Point', coordinates: [1, 1] },
          properties: { name: 'name' },
        },
      ],
    };
    const result = layerReducer(
      initialMockState,
      layerActions.layerChangeGeojson({ layerId, geojson: geojsonAfter }),
    );

    expect(result.byId[layerId].geojson).toEqual(geojsonAfter);
  });

  it('should set layers', () => {
    const initialMockState = {
      byId: {
        'layer-1-map-1': createLayer({ id: 'layer-1-map-1', mapId: 'map-1' }),
        'layer-2-map-2': createLayer({ id: 'layer-2-map-2', mapId: 'map-2' }),
        'base-layer-1-map-1': createLayer({
          id: 'base-layer-1-map-1',
          mapId: 'map-1',
          layerType: LayerType.baseLayer,
        }),
        'base-layer-2-map-1': createLayer({
          id: 'base-layer-2-map-1',
          mapId: 'map-1',
          layerType: LayerType.baseLayer,
        }),
        'base-layer-1-map-2': createLayer({
          id: 'base-layer-1',
          mapId: 'map-2',
          layerType: LayerType.baseLayer,
        }),
      },
      allIds: [
        'layer-1-map-1',
        'layer-2-map-2',
        'base-layer-1-map-1',
        'base-layer-2-map-1',
        'base-layer-1-map-2',
      ],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(5);
    expect(initialMockState.allIds.includes('layer-1-map-1')).toBeTruthy();
    expect(initialMockState.byId['layer-1-map-1']).toBeDefined();
    expect(initialMockState.allIds.includes('layer-2-map-2')).toBeTruthy();
    expect(initialMockState.byId['layer-2-map-2']).toBeDefined();
    expect(initialMockState.allIds.includes('base-layer-1-map-1')).toBeTruthy();
    expect(initialMockState.byId['base-layer-1-map-1']).toBeDefined();

    // set layers
    const layers = [{ id: 'layer-3-id', layerType: LayerType.mapLayer }];

    const result = layerReducer(
      initialMockState,
      layerActions.setLayers({ mapId: 'map-1', layers }),
    );

    expect(result.allIds).toHaveLength(5);
    expect(result.allIds.includes('layer-1')).toBeFalsy();
    expect(result.byId['layer-1']).toBeUndefined();

    layers.forEach((layer) => {
      expect(result.allIds.includes(layer.id));
      expect(result.byId[layer.id]).toBeDefined();
    });

    // layers with different mapIds should be preserved
    expect(result.allIds.includes('layer-2-map-2')).toBeTruthy();
    expect(result.byId['layer-2-map-2']).toBeDefined();
    // baselayers should be preserved
    expect(result.allIds.includes('base-layer-1-map-1')).toBeTruthy();
    expect(result.byId['base-layer-1-map-1']).toBeDefined();
    expect(result.allIds.includes('base-layer-2-map-1')).toBeTruthy();
    expect(result.byId['base-layer-2-map-1']).toBeDefined();

    // Change multiple layers for map should preserve mapids
    const multipleLayers = [
      { id: 'layer-4-id', layerType: LayerType.mapLayer },
      { id: 'layer-5-id', layerType: LayerType.mapLayer },
      { id: 'layer-6-id', layerType: LayerType.mapLayer },
    ];

    const resultMultiple = layerReducer(
      result,
      layerActions.setLayers({ mapId: 'map-1', layers: multipleLayers }),
    );

    expect(resultMultiple.allIds).toHaveLength(7);
    expect(resultMultiple.allIds.includes('layer-3-id')).toBeFalsy();
    expect(resultMultiple.byId['layer-3-id']).toBeUndefined();

    multipleLayers.forEach((layer) => {
      expect(resultMultiple.allIds.includes(layer.id));
      expect(resultMultiple.byId[layer.id]).toBeDefined();
    });

    // layers with different mapIds should be preserved
    expect(resultMultiple.allIds.includes('layer-2-map-2')).toBeTruthy();
    expect(resultMultiple.byId['layer-2-map-2']).toBeDefined();
    // baselayers should be preserved
    expect(resultMultiple.allIds.includes('base-layer-1-map-1')).toBeTruthy();
    expect(resultMultiple.byId['base-layer-1-map-1']).toBeDefined();

    // Change multiple layers to less for map should preserve mapids
    const multipleLayersLess = [
      { id: 'layer-6-id', layerType: LayerType.mapLayer },
    ];

    const resultMultipleLess = layerReducer(
      resultMultiple,
      layerActions.setLayers({ mapId: 'map-1', layers: multipleLayersLess }),
    );

    multipleLayersLess.forEach((layer) => {
      expect(resultMultipleLess.allIds.includes(layer.id));
      expect(resultMultipleLess.byId[layer.id]).toBeDefined();
    });

    // Setting duplicate Id should do nothing
    const duplicateLayers = [
      { id: 'layer-5-id', layerType: LayerType.mapLayer },
      { id: 'layer-6-id', layerType: LayerType.mapLayer },
      { id: 'layer-5-id', layerType: LayerType.mapLayer },
    ];

    const resultduplicateLayers = layerReducer(
      initialMockState,
      layerActions.setLayers({ mapId: 'map-1', layers: duplicateLayers }),
    );
    duplicateLayers.forEach((layer) => {
      expect(resultduplicateLayers.byId[layer.id]).toBeUndefined();
    });

    // Setting duplicate Id should do nothing
    const layerAlreadyUsed = [
      { id: 'layer-1-map-1', mapId: 'map-2', layerType: LayerType.mapLayer },
    ];

    layerAlreadyUsed.forEach((layer) => {
      expect(initialMockState.byId[layer.id]).toBeDefined();
      expect(initialMockState.byId[layer.id].mapId).toBe('map-1');
    });

    const resultAlreadyUsedLayers = layerReducer(
      initialMockState,
      layerActions.setLayers({ mapId: 'map-1', layers: layerAlreadyUsed }),
    );
    layerAlreadyUsed.forEach((layer) => {
      expect(resultAlreadyUsedLayers.byId[layer.id]).toBeDefined();
      expect(resultAlreadyUsedLayers.byId[layer.id].mapId).toBe('map-1');
    });
  });

  it('should set baselayers', () => {
    const initialMockState = {
      byId: {
        'base-layer-1': createLayer({
          id: 'layer-1',
          mapId: 'map-1',
          layerType: LayerType.baseLayer,
        }),
        'over-layer-1': createLayer({
          id: 'over-layer-1',
          mapId: 'map-1',
          layerType: LayerType.overLayer,
        }),
        'base-layer-2': createLayer({
          id: 'layer-2',
          mapId: 'map-2',
          layerType: LayerType.baseLayer,
        }),
        'over-layer-2': createLayer({
          id: 'over-layer-2',
          mapId: 'map-2',
          layerType: LayerType.overLayer,
        }),
        'layer-1': createLayer({
          id: 'layer-1',
          mapId: 'map-3',
        }),
      },
      allIds: [
        'base-layer-1',
        'over-layer-1',
        'base-layer-2',
        'over-layer-2',
        'layer-1',
      ],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(5);
    expect(initialMockState.allIds.includes('base-layer-1')).toBeTruthy();
    expect(initialMockState.byId['over-layer-1']).toBeDefined();
    expect(initialMockState.allIds.includes('over-layer-1')).toBeTruthy();
    expect(initialMockState.byId['base-layer-1']).toBeDefined();
    expect(initialMockState.allIds.includes('base-layer-2')).toBeTruthy();
    expect(initialMockState.byId['base-layer-2']).toBeDefined();
    expect(initialMockState.allIds.includes('over-layer-2')).toBeTruthy();
    expect(initialMockState.byId['over-layer-2']).toBeDefined();
    expect(initialMockState.allIds.includes('layer-1')).toBeTruthy();
    expect(initialMockState.byId['layer-1']).toBeDefined();

    // set baselayers
    const layers = [
      { id: 'base-layer-4', layerType: LayerType.baseLayer },
      { id: 'base-layer-3', layerType: LayerType.baseLayer },
      { id: 'over-layer-3', layerType: LayerType.overLayer },
    ];

    const result = layerReducer(
      initialMockState,
      layerActions.setBaseLayers({ mapId: 'map-1', layers }),
    );

    expect(result.allIds).toHaveLength(6);
    expect(result.allIds.includes('base-layer-1')).toBeFalsy();
    expect(result.byId['base-layer-1']).toBeUndefined();
    expect(result.allIds.includes('over-layer-1')).toBeFalsy();
    expect(result.byId['over-layer-1']).toBeUndefined();

    layers.forEach((layer) => {
      expect(result.allIds.includes(layer.id));
      expect(result.byId[layer.id]).toBeDefined();
    });

    // baseLayers with different mapIds should be preserved
    expect(result.allIds.includes('base-layer-2')).toBeTruthy();
    expect(result.byId['base-layer-2']).toBeDefined();
    expect(result.allIds.includes('over-layer-2')).toBeTruthy();
    expect(result.byId['over-layer-2']).toBeDefined();
    expect(result.allIds.includes('base-layer-3')).toBeTruthy();
    expect(result.byId['base-layer-3']).toBeDefined();
    // layers should be preserved
    expect(result.allIds.includes('layer-1')).toBeTruthy();
    expect(result.byId['layer-1']).toBeDefined();

    // if wrong layerType is passed in, nothing should be done for that layer
    const result2 = layerReducer(
      initialMockState,
      layerActions.setBaseLayers({
        mapId: 'map-1',
        layers: [
          {
            id: 'faultyLayer1',
            name: 'faultyLayer1',
            layerType: LayerType.mapLayer,
          },
        ],
      }),
    );

    expect(result2.allIds).toHaveLength(5);
    expect(result2.allIds.includes('base-layer-1')).toBeTruthy();
    expect(result2.byId['over-layer-1']).toBeDefined();
    expect(result2.allIds.includes('over-layer-1')).toBeTruthy();
    expect(result2.byId['base-layer-1']).toBeDefined();
    expect(result2.allIds.includes('base-layer-2')).toBeTruthy();
    expect(result2.byId['base-layer-2']).toBeDefined();
    expect(result2.allIds.includes('over-layer-2')).toBeTruthy();
    expect(result2.byId['over-layer-2']).toBeDefined();
    expect(result2.allIds.includes('layer-1')).toBeTruthy();
    expect(result2.byId['layer-1']).toBeDefined();
  });

  it('should delete a layer', () => {
    const initialMockState = {
      byId: {
        'test-1': {
          id: 'test-1',
          layerType: LayerType.mapLayer,
        },
        'test-2': {
          id: 'test-2',
          layerType: LayerType.mapLayer,
        },
      },
      allIds: ['test-1', 'test-2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete layer
    const layerId = 'test-1';
    const result = layerReducer(
      initialMockState,
      layerActions.layerDelete({ layerId, mapId: 'test-1', layerIndex: 0 }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-2')).toBeTruthy();
    expect(result.byId['test-2']).toBeDefined();
  });

  it('should delete a baselayer', () => {
    const initialMockState = {
      byId: {
        'test-BaseLayer1': {
          id: 'test-BaseLayer1',
          layerType: LayerType.baseLayer,
        },
        'test-BaseLayer2': {
          id: 'test-BaseLayer2',
          layerType: LayerType.baseLayer,
        },
      },
      allIds: ['test-BaseLayer1', 'test-BaseLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete baselayer
    const layerId = 'test-BaseLayer1';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({
        layerId,
        mapId: 'test-BaseLayer1',
        layerIndex: 0,
      }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-BaseLayer2')).toBeTruthy();
    expect(result.byId['test-BaseLayer2']).toBeDefined();
  });

  it('should error a layer', () => {
    const initialMockState = {
      byId: {
        'test-1': {
          id: 'test-1',
          layerType: LayerType.mapLayer,
          status: LayerStatus.default,
        },
      },
      allIds: ['test-1'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.byId['test-1'].status).toEqual(LayerStatus.default);

    const result = layerReducer(
      initialMockState,
      layerActions.layerError({ layerId: 'test-1', error: new Error('error') }),
    );

    expect(result.byId['test-1'].status).toEqual(LayerStatus.error);
  });

  it('should delete a layer', () => {
    const initialMockState = {
      byId: {
        'test-1': {
          id: 'test-1',
          layerType: LayerType.mapLayer,
        },
        'test-2': {
          id: 'test-2',
          layerType: LayerType.mapLayer,
        },
      },
      allIds: ['test-1', 'test-2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete layer
    const layerId = 'test-1';
    const result = layerReducer(
      initialMockState,
      layerActions.layerDelete({ layerId, mapId: 'test-1', layerIndex: 0 }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-2')).toBeTruthy();
    expect(result.byId['test-2']).toBeDefined();
  });

  it('should delete a baselayer', () => {
    const initialMockState = {
      byId: {
        'test-BaseLayer1': {
          id: 'test-BaseLayer1',
          layerType: LayerType.baseLayer,
        },
        'test-BaseLayer2': {
          id: 'test-BaseLayer2',
          layerType: LayerType.baseLayer,
        },
      },
      allIds: ['test-BaseLayer1', 'test-BaseLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete baselayer
    const layerId = 'test-BaseLayer1';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({
        layerId,
        mapId: 'test-BaseLayer1',
        layerIndex: 0,
      }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-BaseLayer2')).toBeTruthy();
    expect(result.byId['test-BaseLayer2']).toBeDefined();
  });

  it('should delete an overlayer', () => {
    const initialMockState = {
      byId: {
        'test-overLayer1': {
          id: 'test-overLayer1',
          layerType: LayerType.overLayer,
        },
        'test-overLayer2': {
          id: 'test-overLayer2',
          layerType: LayerType.overLayer,
        },
      },
      allIds: ['test-overLayer1', 'test-overLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete overlayer
    const layerId = 'test-overLayer1';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({
        layerId,
        mapId: 'test-map1',
        layerIndex: 0,
      }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes(layerId)).toBeFalsy();
    expect(result.byId[layerId]).toBeUndefined();

    expect(result.allIds.includes('test-overLayer2')).toBeTruthy();
    expect(result.byId['test-overLayer2']).toBeDefined();
  });

  it('baseLayerDelete should return state if no overlayer or baselayer is passed', () => {
    const initialMockState = {
      byId: {
        'test-overLayer1': {
          id: 'test-overLayer1',
          layerType: LayerType.overLayer,
        },
        'test-normalLayer2': {
          id: 'test-normalLayer2',
          layerType: LayerType.mapLayer,
        },
      },
      allIds: ['test-overLayer1', 'test-normalLayer2'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    // initial state
    expect(initialMockState.allIds).toHaveLength(2);

    // delete overlayer
    const layerId = 'test-normalLayer2';
    const result = layerReducer(
      initialMockState,
      layerActions.baseLayerDelete({
        layerId,
        mapId: 'test-map1',
        layerIndex: 0,
      }),
    );

    expect(result.allIds).toHaveLength(2);
    expect(result.allIds.includes('test-normalLayer2')).toBeTruthy();
    expect(result.byId['test-normalLayer2']).toBeDefined();

    expect(result.allIds.includes('test-overLayer1')).toBeTruthy();
    expect(result.byId['test-overLayer1']).toBeDefined();
  });

  it('should add an available baselayer', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.baseLayer,
      mapId: 'mapid1',
    };
    const result = layerReducer(
      undefined,
      layerActions.addAvailableBaseLayer({
        layer,
      }),
    );

    expect(result.availableBaseLayers.byId[layer.id]).toEqual({
      ...layer,
      id: layer.id,
      dimensions: [],
      layerType: LayerType.baseLayer,
      enabled: true,
      opacity: 1,
      status: LayerStatus.default,
    });
    expect(result.availableBaseLayers.allIds.includes(layer.id)).toBeTruthy();

    // Adding the same base layer again should not result in any changes
    const result2 = layerReducer(
      result,
      layerActions.addAvailableBaseLayer({
        layer,
      }),
    );

    expect(result2.availableBaseLayers.allIds).toHaveLength(1);
    expect(result2.availableBaseLayers.byId[layer.id]).toEqual({
      ...layer,
      id: layer.id,
      dimensions: [],
      layerType: LayerType.baseLayer,
      enabled: true,
      opacity: 1,
      status: LayerStatus.default,
    });
    expect(result2.availableBaseLayers.allIds.includes(layer.id)).toBeTruthy();

    // Adding a new baselayer should not affect the exiting available base layers
    const layer3 = {
      name: 'test3',
      id: 'test-3',
      layerType: LayerType.baseLayer,
      mapId: 'mapid1',
    };

    const result3 = layerReducer(
      result2,
      layerActions.addAvailableBaseLayer({
        layer: layer3,
      }),
    );

    expect(result3.availableBaseLayers.allIds).toHaveLength(2);
    expect(result3.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result3.availableBaseLayers.allIds.includes('test-3')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-3']).toBeDefined();

    // Adding a new baselayer without mapId should not be added
    const layer4 = {
      name: 'test4',
      id: 'test-4',
      layerType: LayerType.baseLayer,
    };

    const result4 = layerReducer(
      result3,
      layerActions.addAvailableBaseLayer({
        layer: layer4,
      }),
    );

    expect(result4.availableBaseLayers.allIds).toHaveLength(2);
    expect(result4.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result4.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result4.availableBaseLayers.allIds.includes('test-3')).toBeTruthy();
    expect(result4.availableBaseLayers.byId['test-3']).toBeDefined();
  });

  it('should add multiple available baselayers', () => {
    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.baseLayer,
      mapId: 'mapid1',
    };
    const layer2 = {
      name: 'test2',
      id: 'test-2',
      layerType: LayerType.baseLayer,
      mapId: 'mapid1',
    };
    const result = layerReducer(
      undefined,
      layerActions.addAvailableBaseLayers({
        layers: [layer, layer2],
      }),
    );

    expect(result.availableBaseLayers.allIds).toHaveLength(2);
    expect(result.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result.availableBaseLayers.allIds.includes('test-2')).toBeTruthy();
    expect(result.availableBaseLayers.byId['test-2']).toBeDefined();

    // Adding the same baselayer should not result in any changes
    const result2 = layerReducer(
      result,
      layerActions.addAvailableBaseLayers({
        layers: [layer2],
      }),
    );

    expect(result2.availableBaseLayers.allIds).toHaveLength(2);
    expect(result2.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result2.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result2.availableBaseLayers.allIds.includes('test-2')).toBeTruthy();
    expect(result2.availableBaseLayers.byId['test-2']).toBeDefined();

    // Adding a new baselayer should not affect the exiting available base layers
    const layer3 = {
      name: 'test3',
      id: 'test-3',
      mapId: 'mapid1',
      layerType: LayerType.baseLayer,
    };

    const result3 = layerReducer(
      result2,
      layerActions.addAvailableBaseLayers({
        layers: [layer3],
      }),
    );
    expect(result3.availableBaseLayers.allIds).toHaveLength(3);
    expect(result3.availableBaseLayers.allIds.includes('test-1')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-1']).toBeDefined();
    expect(result3.availableBaseLayers.allIds.includes('test-2')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-2']).toBeDefined();
    expect(result3.availableBaseLayers.allIds.includes('test-3')).toBeTruthy();
    expect(result3.availableBaseLayers.byId['test-3']).toBeDefined();
  });

  it('should ignore a layer with already existing id', () => {
    const initialMockState = {
      byId: {
        'test-1': {
          id: 'test-1',
          layerType: LayerType.mapLayer,
        },
      },
      allIds: ['test-1'],
      availableBaseLayers: { byId: {}, allIds: [] },
    };

    const layer = {
      name: 'test',
      id: 'test-1',
      layerType: LayerType.baseLayer,
      mapId: 'mapid1',
    };

    expect(initialMockState.allIds).toHaveLength(1);
    expect(initialMockState.allIds.includes('test-1')).toBeTruthy();

    const result = layerReducer(
      initialMockState,
      layerActions.addLayer({
        layer: { ...layer },
        layerId: 'test-1',
        mapId: 'mapid1',
        origin: LayerActionOrigin.layerManager,
      }),
    );

    expect(result.allIds).toHaveLength(1);
    expect(result.allIds.includes('test-1')).toBeTruthy();
  });

  describe('setAvailableBaseLayers', () => {
    it('should set avaialable baseLayers', () => {
      const baseLayers = [
        {
          name: 'test',
          id: 'test-1',
          layerType: LayerType.baseLayer,
          mapId: 'mapid1',
        },
        {
          name: 'test2',
          id: 'test-2',
          layerType: LayerType.baseLayer,
          mapId: 'mapid1',
        },
        {
          name: 'test3',
          id: 'test-2',
          layerType: LayerType.overLayer,
          mapId: 'mapid1',
        },
      ];

      const result = layerReducer(
        undefined,
        layerActions.setAvailableBaseLayers({
          layers: baseLayers,
        }),
      );

      expect(result.availableBaseLayers.allIds).toHaveLength(2);
      expect(result.availableBaseLayers.allIds).toEqual([
        baseLayers[0].id,
        baseLayers[1].id,
      ]);
      expect(result.availableBaseLayers.byId[baseLayers[0].id]).toEqual(
        createLayer(baseLayers[0]),
      );
    });

    it('should overwrite existing baseLayers', () => {
      const baseLayers = [
        {
          name: 'test',
          id: 'test-1',
          layerType: LayerType.baseLayer,
          mapId: 'mapid1',
        },
        {
          name: 'test2',
          id: 'test-2',
          layerType: LayerType.baseLayer,
          mapId: 'mapid1',
        },
      ];

      const initialState = {
        byId: {},
        allIds: [],
        availableBaseLayers: {
          byId: {
            test_3: {
              name: 'WorldMap_Light_Grey_Canvas',
              type: 'twms',
              mapId: 'emptyMapView',
              dimensions: [],
              id: 'test_3',
              opacity: 1,
              enabled: true,
              layerType: LayerType.baseLayer,
            },
          },
          allIds: ['test_3'],
        },
      };

      const result = layerReducer(initialState, {} as AnyAction);
      expect(result.availableBaseLayers.byId['test_3']).toEqual(
        initialState.availableBaseLayers.byId['test_3'],
      );
      expect(result.availableBaseLayers.allIds).toHaveLength(1);

      const result2 = layerReducer(
        result,
        layerActions.setAvailableBaseLayers({ layers: baseLayers }),
      );
      expect(result2.availableBaseLayers.byId['test_3']).toBeUndefined();
      expect(result2.availableBaseLayers.byId['test-1']).toEqual(
        createLayer(baseLayers[0]),
      );
      expect(result2.availableBaseLayers.byId['test-2']).toEqual(
        createLayer(baseLayers[1]),
      );
      expect(result2.availableBaseLayers.allIds).toEqual([
        baseLayers[0].id,
        baseLayers[1].id,
      ]);
    });
  });

  describe('layerSetDimensions', () => {
    const layerId = 'test-1';
    const dimensions = [{ name: 'test-dim', currentValue: 'initialvalue' }];

    it('should set dimensions', () => {
      const initialMockState = createLayersState(layerId, {});
      const result = layerReducer(
        initialMockState,
        layerActions.layerSetDimensions({
          layerId,
          origin: LayerActionOrigin.layerManager,
          dimensions,
        }),
      );
      expect(result.byId[layerId].dimensions).toEqual(dimensions);
    });

    it('should not set dimensions if no state passed', () => {
      const result = layerReducer(
        undefined,
        layerActions.layerSetDimensions({
          layerId,
          origin: LayerActionOrigin.layerManager,
          dimensions,
        }),
      );
      expect(result.byId[layerId]).toBeFalsy();
    });
  });
});
