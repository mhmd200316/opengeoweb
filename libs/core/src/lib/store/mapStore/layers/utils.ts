/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { WritableDraft } from 'immer/dist/types/types-external';

import { getWMJSDimensionForLayerAndDimension } from '../utils/helpers';
import { Dimension } from '../types';
import { LayerState, WMJSDimension } from './types';

/**
 * Sets a dimension value for a layer in the state. If the dimension does not exist it is added to the layer.
 * @param draft The draft layerstate
 * @param layerId The layerId
 * @param dimensionValueToUpdate The dimension with values to update in the layer.
 */
const produceLayerDimensionDraftState = (
  draft: WritableDraft<LayerState>,
  layerId: string,
  dimensionValueToUpdate: Dimension,
): void => {
  if (!draft.byId[layerId].dimensions) draft.byId[layerId].dimensions = [];
  const existingLayerDimIndex = draft.byId[layerId].dimensions.findIndex(
    (layerDim) => layerDim.name === dimensionValueToUpdate.name,
  );
  if (existingLayerDimIndex < 0) {
    draft.byId[layerId].dimensions.push({ ...dimensionValueToUpdate });
  } else {
    if (dimensionValueToUpdate.currentValue) {
      draft.byId[layerId].dimensions[existingLayerDimIndex].currentValue =
        dimensionValueToUpdate.currentValue;
    }
    if (dimensionValueToUpdate.synced !== undefined) {
      draft.byId[layerId].dimensions[existingLayerDimIndex].synced =
        dimensionValueToUpdate.synced;
    }
    if (dimensionValueToUpdate.validSyncSelection !== undefined) {
      draft.byId[layerId].dimensions[existingLayerDimIndex].validSyncSelection =
        dimensionValueToUpdate.validSyncSelection;
    }
  }
};

export const isActionLayerSynced = (
  state: LayerState,
  layerIdFromAction: string,
  dimension: Dimension,
): boolean => {
  if (dimension.synced !== undefined) return dimension.synced;
  const actionLayer = state.byId[layerIdFromAction];
  if (!actionLayer) return false;
  const layerDimensionIndex = actionLayer.dimensions.findIndex(
    (layerDim) => layerDim.name === dimension.name,
  );
  if (layerDimensionIndex < 0) return false;
  return actionLayer.dimensions[layerDimensionIndex].synced === true;
};

export const produceDimensionActionForMapLayer = (
  layerDim: Dimension,
  wmjsDimension: WMJSDimension,
  newDimension: Dimension,
): Dimension => {
  // If new value does not occur in the wmjsdimension - do not update the value but set the validSyncSelection to false
  if (wmjsDimension.getIndexForValue(newDimension.currentValue) === -1) {
    return {
      ...layerDim,
      name: newDimension.name,
      units: newDimension.units,
      currentValue: layerDim.currentValue,
      validSyncSelection: false,
    };
  }
  // Otherwise, update to new value and set validSyncSelection to true
  return {
    ...layerDim,
    name: newDimension.name,
    units: newDimension.units,
    currentValue: newDimension.currentValue,
    validSyncSelection: true,
  };
};

/**
 * For the given dimension, it creates a new draft state for all other (linked or synced) layers inside the map.
 * @param draft
 * @param dimension
 * @param mapId
 */
export const produceDraftStateForAllLayersForDimensionWithinMap = (
  draft: WritableDraft<LayerState>,
  dimension: Dimension,
  mapId: string,
  layerIdFromAction: string /* Layer ID or null if not known */,
): void => {
  if (!mapId) return;

  const actionLayerSynced = isActionLayerSynced(
    draft,
    layerIdFromAction,
    dimension,
  );
  for (let li = 0; li < draft.allIds.length; li += 1) {
    const layerId = draft.allIds[li];
    const layer = draft.byId[layerId];

    if (layer && layer.mapId === mapId) {
      const wmjsDimension = getWMJSDimensionForLayerAndDimension(
        layerId,
        dimension.name,
      );
      if (
        wmjsDimension &&
        wmjsDimension.linked &&
        wmjsDimension.name === 'reference_time'
      ) {
        wmjsDimension.linked = false;
      }
      if (
        layerIdFromAction === layerId ||
        (wmjsDimension && wmjsDimension.linked)
      ) {
        const dimensionValueToUpdate = {
          name: dimension.name,
          units: dimension.units,
          currentValue: wmjsDimension
            ? wmjsDimension.getClosestValue(dimension.currentValue, true)
            : dimension.currentValue,
          ...(dimension.synced !== undefined
            ? { synced: dimension.synced }
            : {}),
          validSyncSelection: true,
        };
        /* Set the dimension value for this layer */
        produceLayerDimensionDraftState(draft, layerId, dimensionValueToUpdate);
      } else if (actionLayerSynced) {
        // Check to see if we need to update layer based on being synced
        const layerDimensionIndex = layer.dimensions.findIndex(
          (layerDim) => layerDim.name === dimension.name,
        );
        if (layerDimensionIndex !== -1) {
          const layerDim = layer.dimensions[layerDimensionIndex];
          if (layerDim.synced === true) {
            const dimensionValueToUpdate = produceDimensionActionForMapLayer(
              layerDim,
              wmjsDimension,
              dimension,
            );
            /* Set the dimension value for this layer */
            produceLayerDimensionDraftState(
              draft,
              layerId,
              dimensionValueToUpdate,
            );
          }
        }
      }
    }
  }
};
