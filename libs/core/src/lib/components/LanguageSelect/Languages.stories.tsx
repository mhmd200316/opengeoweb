/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { IntlProvider, FormattedMessage } from 'react-intl';
import translations from '../../utils/languageTranslation';
import { CoreThemeProvider } from '../Providers/Providers';
import LanguageSelect from './LanguageSelect';

export const Languages = (): JSX.Element => {
  const [lang, setLang] = React.useState('en');

  return (
    <CoreThemeProvider>
      <IntlProvider locale={lang} messages={translations[lang]}>
        <LanguageSelect currentLang={lang} onChangeLanguage={setLang} />
        <h1>
          <FormattedMessage id="greeting" defaultMessage="Good morning" />
        </h1>
      </IntlProvider>
    </CoreThemeProvider>
  );
};

Languages.parameters = {
  layout: 'padded',
};
