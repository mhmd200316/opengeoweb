/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import LanguageSelect from './LanguageSelect';
import getLanguages from '../../utils/languagesService';
import { CoreThemeProvider } from '../Providers/Providers';

describe('src/components/LanguageSelect/LanguageSelect', () => {
  const defaultProps = {
    onChangeLanguage: jest.fn(),
    currentLang: 'en',
    defaultLang: 'en',
  };

  it('should show English by default', () => {
    const props = { ...defaultProps };
    const { container } = render(
      <CoreThemeProvider>
        <LanguageSelect {...props} />
      </CoreThemeProvider>,
    );
    expect(container.querySelector('input').value).toBe(props.defaultLang);
  });

  it('should show English if the translation in another language is missed', () => {
    const props = { ...defaultProps, currentLang: 'rus' };
    const { container } = render(
      <CoreThemeProvider>
        <LanguageSelect {...props} />
      </CoreThemeProvider>,
    );
    expect(container.querySelector('input').value).toBe(props.defaultLang);
  });

  it('should show the language that it was initialized with when it exists', () => {
    const props = {
      ...defaultProps,
      currentLang: getLanguages[0].code,
    };
    const { container } = render(
      <CoreThemeProvider>
        <LanguageSelect {...props} />
      </CoreThemeProvider>,
    );
    expect(container.querySelector('input').value).toBe(props.currentLang);
  });

  it('should call onChangeLanguage when a Menu item is clicked', async () => {
    const props = {
      ...defaultProps,
      currentLang: getLanguages[0].code,
    };
    const { findByText, getByTestId } = render(
      <CoreThemeProvider>
        <LanguageSelect {...props} />
      </CoreThemeProvider>,
    );
    const otherLanguage = getLanguages[1].code.toUpperCase();

    fireEvent.mouseDown(getByTestId('select'));

    const menuItem = await findByText(otherLanguage);
    fireEvent.click(menuItem);

    expect(props.onChangeLanguage).toHaveBeenCalledWith(getLanguages[1].code);
    expect(getByTestId('select').textContent).toContain(otherLanguage);
  });

  it('should render a menu-item for each language in the list', () => {
    const props = {
      ...defaultProps,
      currentLang: getLanguages[0].code,
    };
    const { container, getByTestId } = render(
      <CoreThemeProvider>
        <LanguageSelect {...props} />
      </CoreThemeProvider>,
    );

    fireEvent.mouseDown(getByTestId('select'));

    const menuItems = container.parentElement.querySelectorAll('li');
    expect(menuItems.length).toEqual(getLanguages.length);
  });
});
