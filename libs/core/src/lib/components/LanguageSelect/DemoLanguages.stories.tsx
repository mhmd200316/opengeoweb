/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box, IconButton, Tooltip } from '@mui/material';
import { Delete, Clear } from '@opengeoweb/theme';

import { IntlProvider, FormattedMessage } from 'react-intl';
import translations from '../../utils/languageTranslation';
import LanguageSelect from './LanguageSelect';
import { CoreThemeProvider } from '../Providers/Providers';

export const DemoLanguages = (): React.ReactElement => {
  const [isDeleted, setIsDeleted] = React.useState(false);
  const [lang, setLang] = React.useState('en');

  return (
    <CoreThemeProvider>
      <div>
        <Box
          sx={{
            position: 'absolute',
            left: '20px',
            top: '60px',
          }}
        >
          <IntlProvider locale={lang} messages={translations[lang]}>
            <Tooltip
              title={
                <FormattedMessage
                  id="restore"
                  defaultMessage="Press to restore"
                />
              }
            >
              <IconButton
                sx={{
                  position: 'absolute',
                  left: '40px',
                  top: '3px',
                }}
                size="small"
                onClick={(): void => {
                  setIsDeleted(false);
                }}
              >
                <Clear />
              </IconButton>
            </Tooltip>
            <Tooltip
              title={
                <FormattedMessage
                  id="delete"
                  defaultMessage="Press to delete"
                />
              }
              placement="top"
              style={{ verticalAlign: 'baseLine' }}
            >
              <IconButton
                data-testid="deleteButton"
                size="small"
                onClick={(event: React.MouseEvent<HTMLInputElement>): void => {
                  event.stopPropagation();
                  setIsDeleted(true);
                }}
              >
                <Delete />
              </IconButton>
            </Tooltip>

            <Box
              sx={{
                position: 'absolute',
                left: '20px',
                top: '60px',
              }}
            >
              {isDeleted && (
                <FormattedMessage id="deleteOK" defaultMessage="DELETED" />
              )}
            </Box>
          </IntlProvider>
        </Box>
        <Box
          sx={{
            position: 'absolute',
            left: '200px',
            top: '50px',
          }}
        >
          <LanguageSelect currentLang={lang} onChangeLanguage={setLang} />
        </Box>
        <h4>
          This demo demonstrates that the delete button tool-tip language
          corresponds to the language choosen.
        </h4>
      </div>
    </CoreThemeProvider>
  );
};

DemoLanguages.parameters = {
  layout: 'padded',
};
