/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { act, render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import moment from 'moment';
import {
  ConfigurableMapConnect,
  ConfigurableMapConnectProps,
  defaultBbox,
} from './ConfigurableMapConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import {
  baseLayerGrey,
  baseLayerWorldMap,
  overLayer,
  radarLayer,
} from '../../utils/publicLayers';
import * as syncConstants from '../../store/generic/synchronizationGroups/constants';
import {
  mockStateMapWithDimensions,
  mockStateMapWithLayer,
} from '../../utils/testUtils';
import { multiDimensionLayer } from '../../utils/defaultTestSettings';
import { mapActions, uiActions, genericActions } from '../../store';
import { IS_LEGEND_OPEN_BY_DEFAULT } from '../Legend/LegendConnect';

const { registerMap, setMapPreset } = mapActions;
const { syncGroupAddSource } = genericActions;
describe('components/ConfigurableMap/ConfigurableMapConnect', () => {
  const mockState = {
    screenManagerStore: {},
    syncronizationGroupStore: {
      groups: {
        byId: {},
        allIds: [],
      },
      sources: {
        byId: {},
        allIds: [],
      },
    },
  };

  it('should render map with default props', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-1',
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();
    expect(queryByTestId('mapTitle').textContent).toEqual(props.id);
    expect(queryByTestId('layerManagerButton')).toBeTruthy();
    expect(queryByTestId('open-Legend')).toBeTruthy();
    expect(queryByTestId('map-time')).toBeFalsy();
    expect(queryByTestId('zoom-reset')).toBeTruthy();
    expect(queryByTestId('zoom-in')).toBeTruthy();
    expect(queryByTestId('zoom-out')).toBeTruthy();
    expect(queryByTestId('timeSlider')).toBeTruthy();

    const expectedActions = [
      registerMap({ mapId: props.id }),
      syncGroupAddSource({
        id: props.id,
        type: [
          syncConstants.SYNCGROUPS_TYPE_SETTIME,
          syncConstants.SYNCGROUPS_TYPE_SETBBOX,
        ],
      }),
      syncGroupAddSource({
        id: props.id,
        type: [syncConstants.SYNCGROUPS_TYPE_SETTIME],
      }),
      syncGroupAddSource({
        id: props.id,
        type: [syncConstants.SYNCGROUPS_TYPE_SETTIME],
      }),
      uiActions.registerDialog({
        mapId: 'test-map-1',
        setOpen: IS_LEGEND_OPEN_BY_DEFAULT,
        source: 'app',
        type: 'legend-test-map-1',
      }),
      setMapPreset({
        initialProps: {
          mapPreset: {
            layers: [
              { ...baseLayerGrey, id: expect.stringContaining('layerid_') },
              {
                ...overLayer,
                id: expect.stringContaining('layerid_'),
              },
              ...props.layers,
            ],
            proj: {
              bbox: defaultBbox.bbox,
              srs: defaultBbox.srs,
            },
            shouldAutoUpdate: false,
            shouldAnimate: false,
            shouldShowZoomControls: true,
            displayMapPin: false,
            showTimeSlider: true,
          },
        },
        mapId: props.id,
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should disable the timeSlider', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-1',
      disableTimeSlider: true,
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();
    expect(queryByTestId('mapTitle').textContent).toEqual(props.id);
    expect(queryByTestId('layerManagerButton')).toBeTruthy();
    expect(queryByTestId('open-Legend')).toBeTruthy();
    expect(queryByTestId('map-time')).toBeFalsy();
    expect(queryByTestId('timeSlider')).toBeFalsy();
    expect(queryByTestId('zoom-reset')).toBeTruthy();
    expect(queryByTestId('zoom-in')).toBeTruthy();
    expect(queryByTestId('zoom-out')).toBeTruthy();

    const expectedActions = [
      registerMap({ mapId: props.id }),
      syncGroupAddSource({
        id: props.id,
        type: [
          syncConstants.SYNCGROUPS_TYPE_SETTIME,
          syncConstants.SYNCGROUPS_TYPE_SETBBOX,
        ],
      }),
      uiActions.registerDialog({
        mapId: 'test-map-1',
        setOpen: IS_LEGEND_OPEN_BY_DEFAULT,
        source: 'app',
        type: 'legend-test-map-1',
      }),
      setMapPreset({
        initialProps: {
          mapPreset: {
            layers: [
              { ...baseLayerGrey, id: expect.stringContaining('layerid_') },
              {
                ...overLayer,
                id: expect.stringContaining('layerid_'),
              },
              ...props.layers,
            ],
            proj: {
              bbox: defaultBbox.bbox,
              srs: defaultBbox.srs,
            },
            shouldAutoUpdate: false,
            shouldAnimate: false,
            shouldShowZoomControls: true,
            displayMapPin: false,
            showTimeSlider: true,
          },
        },
        mapId: props.id,
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should render map with custom title', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-0',
      title: 'Custom map title',
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();
    expect(queryByTestId('mapTitle').textContent).toEqual(props.title);
  });

  it('should render map with custom bbox', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-2',
      srs: 'EPSG:3575',
      bbox: {
        left: -13000000,
        bottom: -13000000,
        right: 13000000,
        top: 13000000,
      },
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();

    const actionResult = store
      .getActions()
      .find((action) => action.type === setMapPreset.type);

    expect(actionResult.payload.initialProps.mapPreset.proj.bbox).toEqual(
      props.bbox,
    );
    expect(actionResult.payload.initialProps.mapPreset.proj.srs).toEqual(
      props.srs,
    );
  });

  it('should set autoUpdate to false if not passed', async () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      id: 'map1',
      layers: [],
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const actionResult = store
      .getActions()
      .find((action) => action.type === setMapPreset.type);

    expect(
      actionResult.payload.initialProps.mapPreset.shouldAutoUpdate,
    ).toBeFalsy();
  });

  it('should set autoUpdate to true if passed as true', async () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const testShouldAutoUpdate = true;
    const props: ConfigurableMapConnectProps = {
      id: 'map1',
      layers: [],
      shouldAutoUpdate: testShouldAutoUpdate,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const actionResult = store
      .getActions()
      .find((action) => action.type === setMapPreset.type);

    expect(
      actionResult.payload.initialProps.mapPreset.shouldAutoUpdate,
    ).toEqual(testShouldAutoUpdate);
  });

  it('should include zoomControls if shouldShowZoomControls not passed', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
    };

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('ConfigurableMap')).toBeTruthy();
    expect(getByTestId('zoom-reset')).toBeTruthy();
    expect(getByTestId('zoom-in')).toBeTruthy();
    expect(getByTestId('zoom-out')).toBeTruthy();
  });

  it('should not include zoomControls if shouldShowZoomControls is not visible in store', () => {
    const mockStore = configureStore();
    const mapId = 'test';
    const store = mockStore({
      ...mockState,
      webmap: {
        byId: {
          [mapId]: {
            shouldShowZoomControls: false,
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
    });
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      shouldShowZoomControls: false,
      id: mapId,
    };

    const { getByTestId, queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('ConfigurableMap')).toBeTruthy();
    expect(queryByTestId('zoom-reset')).toBeFalsy();
    expect(queryByTestId('zoom-in')).toBeFalsy();
    expect(queryByTestId('zoom-out')).toBeFalsy();
  });

  it('should include timeSlider if showTimeSlider not passed', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
    };

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('ConfigurableMap')).toBeTruthy();
    expect(getByTestId('timeSlider')).toBeTruthy();
  });

  it('should not include timeSlider is not visible', async () => {
    const mapId = 'test-1';
    const newMockState = {
      ...mockState,
      webmap: {
        byId: {
          [mapId]: {
            isTimeSliderVisible: false,
            mapLayers: [],
            overLayers: [],
            baseLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
    };
    const mockStore = configureStore();
    const store = mockStore(newMockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      showTimeSlider: false,
      id: mapId,
    };

    const { getByTestId, queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(getByTestId('ConfigurableMap')).toBeTruthy();
    await waitFor(() => {
      expect(queryByTestId('timeSlider')).toBeFalsy();
    });
  });

  it('should render map with autoanimate enabled', () => {
    const end = `2021-12-20T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(end).valueOf());
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
      shouldAnimate: true,
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();

    const actionResult = store
      .getActions()
      .find((action) => action.type === setMapPreset.type);

    expect(actionResult.payload.initialProps.mapPreset.shouldAnimate).toEqual(
      props.shouldAnimate,
    );
  });

  it('should render map with autoanimate disabled when not given', () => {
    const end = `2021-12-20T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(end).valueOf());
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();

    const actionResult = store
      .getActions()
      .find((action) => action.type === setMapPreset.type);

    expect(
      actionResult.payload.initialProps.mapPreset.shouldAnimate,
    ).toBeFalsy();
  });

  it('should render map with autoanimate enabled and animationPayload if passed', () => {
    const end = `${moment.utc().format('YYYY-MM-DD')}T14:00:00Z`;
    jest.spyOn(Date, 'now').mockReturnValue(new Date(end).valueOf());
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
      shouldAnimate: true,
      animationPayload: { interval: 15, duration: 2 * 60 },
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();

    const actionResult = store
      .getActions()
      .find((action) => action.type === setMapPreset.type);

    expect(
      actionResult.payload.initialProps.mapPreset.animationPayload,
    ).toEqual(props.animationPayload);
    expect(actionResult.payload.initialProps.mapPreset.shouldAnimate).toEqual(
      props.shouldAnimate,
    );
  });

  it('should render map with autoanimate enabled and pass speed if passed in payload', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'test-map-3',
      shouldAnimate: true,
      animationPayload: { speed: 8 },
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();

    const actionResult = store
      .getActions()
      .find((action) => action.type === setMapPreset.type);

    expect(
      actionResult.payload.initialProps.mapPreset.animationPayload,
    ).toEqual(props.animationPayload);
    expect(actionResult.payload.initialProps.mapPreset.shouldAnimate).toEqual(
      props.shouldAnimate,
    );
  });

  it('should render map with custom layers', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [{ ...radarLayer, id: 'active' }, baseLayerWorldMap, overLayer],
      id: 'test-map-4',
      activeLayerId: 'active',
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();
    expect(queryByTestId('mapTitle').textContent).toEqual(props.id);

    const expectedAction = {
      type: setMapPreset.type,
      payload: {
        mapId: props.id,
        initialProps: {
          mapPreset: {
            activeLayerId: props.activeLayerId,
            displayMapPin: false,
            layers: props.layers,
            proj: {
              bbox: defaultBbox.bbox,
              srs: defaultBbox.srs,
            },
            shouldAnimate: false,
            shouldAutoUpdate: false,
            shouldShowZoomControls: true,
            showTimeSlider: true,
          },
        },
      },
    };
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should render map with maptime visible', () => {
    const mapId = 'test-map-5';
    const layer = {
      ...radarLayer,
      dimensions: [{ name: 'time', currentValue: '2021-12-07T13:30:00Z' }],
    };
    const customMockState = mockStateMapWithLayer(layer, mapId);
    const mockStore = configureStore();
    const store = mockStore(customMockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: mapId,
      displayTimeInMap: true,
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();
    expect(queryByTestId('map-time')).toBeTruthy();
  });

  it('should render map with LayerManagerButton and legend button', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      displayLayerManagerAndLegendButtonInMap: true,
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();
    expect(queryByTestId('layerManagerButton')).toBeTruthy();
    expect(queryByTestId('open-Legend')).toBeTruthy();
  });

  it('should render map without LayerManagerButton or legend button', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      displayLayerManagerAndLegendButtonInMap: false,
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();
    expect(queryByTestId('layerManagerButton')).toBeFalsy();
    expect(queryByTestId('open-Legend')).toBeFalsy();
  });

  it('should render map with DimensionSelectButton', () => {
    const mapId = 'test-map-7';
    const customMockState = mockStateMapWithDimensions(
      multiDimensionLayer,
      mapId,
    );
    const mockStore = configureStore();
    const store = mockStore({ ...mockState, ...customMockState });
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [multiDimensionLayer],
      id: mapId,
      displayDimensionSelectButtonInMap: true,
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();
    expect(queryByTestId('dimensionMapBtn-elevation')).toBeTruthy();
  });

  it('should render map without DimensionSelectButton', () => {
    jest.spyOn(console, 'warn').mockImplementation();
    const mapId = 'test-map-8';
    const customMockState = mockStateMapWithDimensions(
      multiDimensionLayer,
      mapId,
    );
    const mockStore = configureStore();
    const store = mockStore({ ...mockState, ...customMockState });
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [multiDimensionLayer],
      id: mapId,
      displayDimensionSelectButtonInMap: false,
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('ConfigurableMap')).toBeTruthy();
    expect(queryByTestId('dimensionMapBtn-elevation')).toBeFalsy();
  });

  it('should render map with toggleTimestepAuto using preset values', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'radarView',
      displayDimensionSelectButtonInMap: false,
      toggleTimestepAuto: false,
    };

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const actionResult = store
      .getActions()
      .find((action) => action.type === setMapPreset.type);

    expect(
      actionResult.payload.initialProps.mapPreset.toggleTimestepAuto,
    ).toEqual(props.toggleTimestepAuto);
    expect(queryByTestId('dimensionMapBtn-elevation')).toBeNull();
  });

  it('should render map with mapid in legend type', async () => {
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'radarView',
      displayDimensionSelectButtonInMap: false,
      toggleTimestepAuto: false,
      multiLegend: true,
    };

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();

    await act(async () => {
      render(
        <CoreThemeStoreProvider store={store}>
          <ConfigurableMapConnect {...props} />
        </CoreThemeStoreProvider>,
      );
      await waitFor(() => {
        const expectedAction = uiActions.registerDialog({
          mapId: props.id,
          setOpen: IS_LEGEND_OPEN_BY_DEFAULT,
          source: 'app',
          type: `legend-${props.id}`,
        });

        expect(store.getActions()).toContainEqual(expectedAction);
      });
    });
  });

  it('should pass setTimestep value using preset animation interval values', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      displayDimensionSelectButtonInMap: false,
      id: 'radarView',
      animationPayload: {
        interval: 5,
      },
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const actionResult = store
      .getActions()
      .find((action) => action.type === setMapPreset.type);

    expect(
      actionResult.payload.initialProps.mapPreset.animationPayload.interval,
    ).toEqual(props.animationPayload.interval);
  });

  it('should show layermanager', () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();
    const props: ConfigurableMapConnectProps = {
      layers: [],
      id: 'radarView',
      shouldShowLayerManager: true,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <ConfigurableMapConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const actionResult = store
      .getActions()
      .find(
        (action) => action.type === uiActions.setActiveMapIdForDialog.type,
      ).payload;

    expect(actionResult.type).toEqual('layerManager');
    expect(actionResult.mapId).toEqual(props.id);
    expect(actionResult.setOpen).toEqual(props.shouldShowLayerManager);
    expect(actionResult.source).toEqual('app');
  });
});
