/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Box, SxProps, Theme, Typography } from '@mui/material';
import { useDispatch } from 'react-redux';

import { generateMapId } from '../../store/mapStore/utils/helpers';
import {
  AnimationPayloadType,
  Bbox,
  Layer,
  LayerType,
  MapPresetInitialProps,
} from '../../store/mapStore/types';
import { MapControls, ZoomControlConnect } from '../MapControls';
import { TimeSliderConnect } from '../TimeSlider';
import { MapViewConnect } from '../MapView';
import { LegendConnect, LegendMapButtonConnect } from '../Legend';
import { LayerManagerMapButtonConnect } from '../LayerManager';
import { MultiDimensionSelectMapButtonsConnect } from '../MultiMapDimensionSelect';
import { baseLayerGrey, overLayer } from '../../utils/publicLayers';
import { mapActions, uiActions } from '../../store';

const titleStyle = (theme: Theme): SxProps<Theme> => ({
  position: 'absolute',
  padding: '5px',
  zIndex: 50,
  color: theme.palette.common.black,
  whiteSpace: 'nowrap',
});

export const defaultBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: 58703.6377,
    bottom: 6408480.4514,
    right: 3967387.5161,
    top: 11520588.9031,
  },
};

export interface ConfigurableMapConnectProps {
  id?: string;
  shouldAutoUpdate?: boolean;
  shouldAnimate?: boolean;
  title?: string;
  layers: Layer[];
  activeLayerId?: string;
  bbox?: Bbox;
  srs?: string;
  animationPayload?: AnimationPayloadType;
  shouldShowZoomControls?: boolean;
  displayMapPin?: boolean;
  showTimeSlider?: boolean; // used for map preset action
  disableTimeSlider?: boolean; // used by multimap to disable timeslider completely
  toggleTimestepAuto?: boolean;
  displayTimeInMap?: boolean;
  displayLayerManagerAndLegendButtonInMap?: boolean;
  displayDimensionSelectButtonInMap?: boolean;
  multiLegend?: boolean;
  shouldShowLayerManager?: boolean;
}

export const ConfigurableMapConnect: React.FC<ConfigurableMapConnectProps> = ({
  id,
  title,
  layers = [],
  shouldAutoUpdate = false,
  shouldAnimate = false,
  bbox = defaultBbox.bbox,
  srs = defaultBbox.srs,
  shouldShowZoomControls = true,
  displayMapPin = false,
  showTimeSlider = true,
  disableTimeSlider = false,
  displayTimeInMap = false,
  displayLayerManagerAndLegendButtonInMap = true,
  displayDimensionSelectButtonInMap = true,
  multiLegend = true,
  shouldShowLayerManager,
  ...props
}: ConfigurableMapConnectProps) => {
  const dispatch = useDispatch();
  const mapId = React.useRef(id || generateMapId()).current;

  React.useEffect(() => {
    const layersWithDefaultBaseLayer = layers.find(
      (layer) => layer.layerType === LayerType.baseLayer,
    )
      ? layers
      : [...layers, baseLayerGrey];

    const layersWithDefaultOverLayer = layersWithDefaultBaseLayer.find(
      (layer) => layer.layerType === LayerType.overLayer,
    )
      ? layersWithDefaultBaseLayer
      : [...layersWithDefaultBaseLayer, overLayer];

    const mapPreset = {
      layers: layersWithDefaultOverLayer,
      proj: {
        bbox,
        srs,
      },
      shouldAutoUpdate,
      shouldAnimate,
      shouldShowZoomControls,
      displayMapPin,
      showTimeSlider,
      ...props,
    };

    const initialProps: MapPresetInitialProps = { mapPreset };

    dispatch(mapActions.setMapPreset({ mapId, initialProps }));

    if (shouldShowLayerManager !== undefined) {
      dispatch(
        uiActions.setActiveMapIdForDialog({
          type: 'layerManager',
          mapId,
          setOpen: shouldShowLayerManager,
          source: 'app',
        }),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Box
      sx={{
        width: '100%',
        height: '100%',
        position: 'relative',
        overflow: 'hidden',
      }}
      data-testid="ConfigurableMap"
    >
      <Typography data-testid="mapTitle" sx={titleStyle as SxProps<Theme>}>
        {title || mapId}
      </Typography>

      <MapControls data-testid="mapControls" style={{ top: 24 }}>
        <ZoomControlConnect mapId={id} />
        {displayLayerManagerAndLegendButtonInMap && (
          <LayerManagerMapButtonConnect mapId={mapId} />
        )}
        {displayLayerManagerAndLegendButtonInMap && (
          <LegendMapButtonConnect mapId={mapId} multiLegend={multiLegend} />
        )}
        {displayDimensionSelectButtonInMap && (
          <MultiDimensionSelectMapButtonsConnect mapId={mapId} />
        )}
      </MapControls>

      {!disableTimeSlider && (
        <Box
          sx={{
            position: 'absolute',
            left: '0px',
            bottom: '0px',
            zIndex: 1000,
            width: '100%',
          }}
        >
          <TimeSliderConnect mapId={id} sourceId={id} />
        </Box>
      )}

      <MapViewConnect
        controls={{}}
        displayTimeInMap={displayTimeInMap}
        showScaleBar={false}
        mapId={mapId}
      />
      {multiLegend && (
        <LegendConnect showMapId mapId={mapId} multiLegend={multiLegend} />
      )}
    </Box>
  );
};
