/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */
import * as React from 'react';

import CanvasComponent from './CanvasComponent';

export const DynamicCanvas: React.FC = () => {
  let loopTimer = 0;
  let mouseX = 0;
  let mouseY = 0;
  return (
    <div style={{ width: '100%', height: '600px' }}>
      <CanvasComponent
        loop
        onMouseMove={(x, y): void => {
          mouseX = x;
          mouseY = y;
        }}
        onRenderCanvas={(
          ctx: CanvasRenderingContext2D,
          width: number,
          height: number,
        ): void => {
          loopTimer += 0.05;

          /* Clear canvas */
          ctx.beginPath();
          ctx.fillStyle = 'white';
          ctx.fillRect(0, 0, width, height);
          ctx.fillStyle = 'red';
          ctx.fillRect(mouseX - 20, mouseY - 20, 40, 40);

          /* Draw moving line */
          const x1 = Math.sin(loopTimer) * (height / 2) + width / 2;
          const y1 = Math.cos(loopTimer) * (height / 2) + height / 2;
          const x2 = Math.sin(loopTimer + Math.PI) * (height / 2) + width / 2;
          const y2 = Math.cos(loopTimer + Math.PI) * (height / 2) + height / 2;

          ctx.moveTo(x1, y1);
          ctx.lineTo(x2, y2);
          ctx.strokeStyle = 'blue';
          ctx.lineWidth = 10;
          ctx.stroke();
        }}
      />
    </div>
  );
};
