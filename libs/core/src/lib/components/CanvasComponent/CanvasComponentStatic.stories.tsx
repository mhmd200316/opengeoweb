/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable no-param-reassign */
import * as React from 'react';

import CanvasComponent from './CanvasComponent';

export const StaticCanvas: React.FC = () => (
  <div style={{ width: '100%', height: '600px' }}>
    <CanvasComponent
      onRenderCanvas={(
        ctx: CanvasRenderingContext2D,
        width: number,
        height: number,
      ): void => {
        ctx.moveTo(0, 0);
        ctx.lineTo(width, height);
        ctx.strokeStyle = 'blue';
        ctx.lineWidth = 2;
        ctx.stroke();
        for (let i = 0; i < 16; i += 1) {
          for (let j = 0; j < 16; j += 1) {
            ctx.strokeStyle = `rgb(
                0,
                ${Math.floor(255 - 16 * i)},
                ${Math.floor(255 - 16 * j)})`;
            ctx.beginPath();
            ctx.arc(12.5 + j * 25, 12.5 + i * 25, 10, 0, Math.PI * 2, true);
            ctx.stroke();
          }
        }
      }}
    />
  </div>
);
