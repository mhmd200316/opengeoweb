/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import * as publicLayers from '../../../utils/publicLayers';
import { makeMapPreset, ModelRunInterval } from './ModelRunInterval';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import { LayerType } from '../../../store/mapStore/types';

describe('components/MultiMapView/ModelRunInterval', () => {
  describe('makeMapPreset', () => {
    it('should return correct global props if no layers passed in', () => {
      expect(
        makeMapPreset(
          6,
          'unique-id',
          '2020-12-11T12:00:00Z',
          [],
          ['layerGroupA'],
          true,
          true,
        ),
      ).toEqual({
        title: `FC +6`,
        displayTimeInMap: true,
        syncGroupsIds: ['layerGroupA'],
        id: 'unique-id',
        bbox: {
          left: -129849.7860570465,
          bottom: 6180677.243149376,
          right: 1215891.3289626688,
          top: 7540256.313079321,
        },
        displayLayerManagerAndLegendButtonInMap: true,
        displayDimensionSelectButtonInMap: true,
        srs: 'EPSG:3857',
        layers: [],
      });
    });
    it('should return correct global props + layer props if one layer passed in', () => {
      expect(
        makeMapPreset(
          6,
          'unique-id',
          '2020-12-11T12:00:00Z',
          [publicLayers.harmoniePressure],
          ['layerGroupA'],
          true,
          true,
        ),
      ).toEqual({
        title: `FC +6`,
        displayTimeInMap: true,
        id: 'unique-id',
        syncGroupsIds: ['layerGroupA'],
        bbox: {
          left: -129849.7860570465,
          bottom: 6180677.243149376,
          right: 1215891.3289626688,
          top: 7540256.313079321,
        },
        displayLayerManagerAndLegendButtonInMap: true,
        displayDimensionSelectButtonInMap: true,
        srs: 'EPSG:3857',
        layers: [
          {
            ...publicLayers.harmoniePressure,
            id: 'unique-id-0',
            dimensions: [
              {
                name: 'reference_time',
                currentValue: '2020-12-11T12:00:00Z',
              },
              {
                name: 'time',
                currentValue: '2020-12-11T18:00:00Z',
              },
            ],
          },
        ],
      });
    });
    it('should return correct global props + layer props if multiple layers passed in', () => {
      expect(
        makeMapPreset(
          8,
          'unique-id',
          '2020-12-11T12:00:00Z',
          [
            publicLayers.harmoniePressure,
            publicLayers.harmoniePrecipitation,
            publicLayers.harmonieWindFlags,
          ],
          ['layerGroupA'],
          true,
          true,
        ),
      ).toEqual({
        title: `FC +8`,
        displayTimeInMap: true,
        syncGroupsIds: ['layerGroupA'],
        id: 'unique-id',
        bbox: {
          left: -129849.7860570465,
          bottom: 6180677.243149376,
          right: 1215891.3289626688,
          top: 7540256.313079321,
        },
        displayLayerManagerAndLegendButtonInMap: true,
        displayDimensionSelectButtonInMap: true,
        srs: 'EPSG:3857',
        layers: [
          {
            ...publicLayers.harmoniePressure,
            id: 'unique-id-0',
            dimensions: [
              {
                name: 'reference_time',
                currentValue: '2020-12-11T12:00:00Z',
              },
              {
                name: 'time',
                currentValue: '2020-12-11T20:00:00Z',
              },
            ],
          },
          {
            ...publicLayers.harmoniePrecipitation,
            id: 'unique-id-1',
            dimensions: [
              {
                name: 'reference_time',
                currentValue: '2020-12-11T12:00:00Z',
              },
              {
                name: 'time',
                currentValue: '2020-12-11T20:00:00Z',
              },
            ],
          },
          {
            ...publicLayers.harmonieWindFlags,
            id: 'unique-id-2',
            dimensions: [
              {
                name: 'reference_time',
                currentValue: '2020-12-11T12:00:00Z',
              },
              {
                name: 'time',
                currentValue: '2020-12-11T20:00:00Z',
              },
            ],
          },
        ],
      });
    });
  });

  describe('ModelRunInterval', () => {
    const storedFetch = global['fetch'];

    beforeEach(() => {
      const headers = new Headers();
      window.fetch = jest.fn().mockResolvedValue({
        text: () =>
          Promise.resolve(
            `<?xml version="1.0" encoding="UTF-8"?>
              <WMS_Capabilities
                    version="1.3.0"
                    updateSequence="0"
                    xmlns="http://www.opengis.net/wms"
                    xmlns:xlink="http://www.w3.org/1999/xlink"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsi:schemaLocation="http://www.opengis.net/wms http://schemas.opengis.net/wms/1.3.0/capabilities_1_3_0.xsd"
                 >
                <Service>
                    <Name>WMS</Name>
                    <Title>HARM_N25</Title>
                </Service>
                <Capability>
                  <Layer>
                    <Title>WMS of HARM_N25</Title>
                    <Layer queryable="1" opaque="1" cascaded="0">
                      <Name>ref_times_layer</Name>
                      <Title>ref_times_layer</Title>
                      <Dimension name="time" units="ISO8601" default="2022-06-05T00:00:00Z" multipleValues="1" nearestValue="0" current="1">2022-06-05T00:00:00Z/2022-06-09T06:00:00Z/PT1H</Dimension>
                      <Dimension name="reference_time" units="ISO8601" default="2022-06-07T06:00:00Z" multipleValues="1" nearestValue="0" current="1">2022-06-05T00:00:00Z,2022-06-05T03:00:00Z,2022-06-05T06:00:00Z,2022-06-05T09:00:00Z,2022-06-05T12:00:00Z,2022-06-05T15:00:00Z,2022-06-05T18:00:00Z,2022-06-05T21:00:00Z,2022-06-06T00:00:00Z,2022-06-06T03:00:00Z,2022-06-06T06:00:00Z,2022-06-06T09:00:00Z,2022-06-06T12:00:00Z,2022-06-06T15:00:00Z,2022-06-06T18:00:00Z,2022-06-06T21:00:00Z,2022-06-07T00:00:00Z,2022-06-07T03:00:00Z,2022-06-07T06:00:00Z</Dimension>
                    </Layer>
                  </Layer>
                </Capability>
              </WMS_Capabilities>
            `,
          ),
        headers,
      });
    });

    afterEach(() => {
      global['fetch'] = storedFetch;
    });

    const layerWithRefTimes = {
      service: 'https://testservice',
      id: 'refTimesLayer',
      name: 'ref_times_layer',
      title: 'ref_times_layer',
      layerType: LayerType.mapLayer,
      enabled: true,
      dimensions: [
        {
          values: '2022-06-07T06:00:00Z,2022-06-07T05:00:00Z',
          currentValue: '2022-06-07T06:00:00Z',
          name: 'reference_time',
          units: 'ISO8601',
        },
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: '2022-06-07T06:00:00Z',
        },
      ],
      styles: [],
    };

    it('should render 15 maps with default intervals', async () => {
      const mockStore = configureStore();
      const store = mockStore({});
      store.addModules = jest.fn(); // mocking the dynamic module loader

      const { findByTestId, getAllByTestId } = render(
        <CoreThemeStoreProvider store={store}>
          <ModelRunInterval syncGroupsIds={[]} layers={[layerWithRefTimes]} />
        </CoreThemeStoreProvider>,
      );
      expect(await findByTestId('MultiMapViewSliderConnect')).toBeTruthy();
      const mapTitles = getAllByTestId('mapTitle');
      expect(mapTitles.length).toEqual(15);
      expect(mapTitles[0].textContent).toEqual('FC +3');
      expect(mapTitles[mapTitles.length - 1].textContent).toEqual('FC +45');
    });

    it('should render 15 maps with custom interval and starting increment', async () => {
      const mockStore = configureStore();
      const store = mockStore({});
      store.addModules = jest.fn(); // mocking the dynamic module loader

      const { findByTestId, getAllByTestId } = render(
        <CoreThemeStoreProvider store={store}>
          <ModelRunInterval
            syncGroupsIds={[]}
            layers={[layerWithRefTimes]}
            interval={1}
            startTimeIncrement={0}
          />
        </CoreThemeStoreProvider>,
      );
      expect(await findByTestId('MultiMapViewSliderConnect')).toBeTruthy();
      const mapTitles = getAllByTestId('mapTitle');
      expect(mapTitles.length).toEqual(15);
      expect(mapTitles[0].textContent).toEqual('FC +0');
      expect(mapTitles[mapTitles.length - 1].textContent).toEqual('FC +14');
    });
  });
});
