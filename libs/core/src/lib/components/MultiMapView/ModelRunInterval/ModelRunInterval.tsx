/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { WMLayer } from '@opengeoweb/webmap';
import moment from 'moment';
import { range } from 'lodash';
import { Layer } from '../../../store/mapStore/types';
import { MultiMapPreset, MultiMapViewConnect } from '../MultiMapViewConnect';
import * as publicLayers from '../../../utils/publicLayers';

const useGetLatestReferenceTime = (inputLayer): string => {
  const [latestReferenceTime, setLatestReferenceTime] =
    React.useState<string>('');
  React.useEffect(() => {
    const wmLayer = new WMLayer({ ...inputLayer });
    wmLayer.parseLayerPromise().then((layer: WMLayer) => {
      const refTimeDim = layer.getDimension('reference_time');
      const lastRefTime = refTimeDim.getLastValue();
      setLatestReferenceTime(lastRefTime);
    });
  }, [inputLayer]);
  return latestReferenceTime;
};

/* Helper to make a map layer preset */
export const makeMapPreset = (
  timeIncrement: number,
  uniqueId: string,
  referenceTime: string,
  layers: Layer[],
  syncGroupsIds: string[],
  displayLayerManagerAndLegendButtonInMap: boolean,
  displayDimensionSelectButtonInMap: boolean,
): MultiMapPreset => {
  const mapTime = moment
    .utc(referenceTime)
    .add(timeIncrement, 'h')
    .format('YYYY-MM-DDTHH:mm:ss[Z]');
  return {
    id: uniqueId,
    syncGroupsIds,
    title: `FC +${timeIncrement}`,
    displayTimeInMap: true,
    bbox: {
      left: -129849.7860570465,
      bottom: 6180677.243149376,
      right: 1215891.3289626688,
      top: 7540256.313079321,
    },
    srs: 'EPSG:3857',
    layers: layers.map((layer, index) => ({
      ...layer,
      id: `${uniqueId}-${index}`,
      dimensions: [
        {
          name: 'reference_time',
          currentValue: referenceTime,
        },
        {
          name: 'time',
          currentValue: mapTime,
        },
      ],
    })),
    displayLayerManagerAndLegendButtonInMap,
    displayDimensionSelectButtonInMap,
  };
};

export interface ModelRunIntervalProps {
  layers?: Layer[];
  syncGroupsIds: string[];
  interval?: number;
  startTimeIncrement?: number;
  multiLegend?: boolean;
}

export const ModelRunInterval: React.FC<ModelRunIntervalProps> = ({
  layers = [
    publicLayers.harmoniePressure,
    publicLayers.harmonieWindFlags,
    publicLayers.harmoniePrecipitation,
  ],
  syncGroupsIds,
  interval = 3,
  startTimeIncrement = 3,
  multiLegend = false,
}: ModelRunIntervalProps) => {
  /* Get latest reference time for this layer */
  const latestReferenceTime = useGetLatestReferenceTime(layers[0]);
  if (latestReferenceTime === '') return <div>Loading...</div>;
  /* Calculate the last time increment based on 15 maps */
  const lastTimeIncrement = startTimeIncrement + 15 * interval;
  /* Make the preset */
  const timeIncrements = [
    ...range(startTimeIncrement, lastTimeIncrement, interval),
  ];

  /* Add the layers with the different map times */
  const maps = timeIncrements.map((increment, index) => {
    return makeMapPreset(
      increment,
      `harm_precip${increment}`,
      latestReferenceTime,
      layers,
      syncGroupsIds,
      index === 0 || !syncGroupsIds,
      index === 0 || !syncGroupsIds,
    );
  });
  return (
    <MultiMapViewConnect
      rows={3}
      cols={5}
      maps={maps}
      syncTime={false}
      showTimeSlider={false}
      multiLegend={multiLegend}
    />
  );
};
