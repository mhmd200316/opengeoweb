/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { store } from '../../storybookUtils/store';
import { MultiMapPreset, MultiMapViewConnect } from './MultiMapViewConnect';
import { LayerManagerConnect } from '../LayerManager';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { klimaatAtlasTG3 } from '../../utils/publicLayers';
import { MultiMapDimensionSelectConnect } from '../MultiMapDimensionSelect';

export const MultiMapViewStory10x10 = (): React.ReactElement => {
  const maps: MultiMapPreset[] = [];
  const zeroPad = (num: number, places: number): string =>
    String(num).padStart(places, '0');
  while (maps.length < 10 * 10) {
    const monthNumber = zeroPad((maps.length % 12) + 1, 2);
    const newTimeValue = `2020-${monthNumber}-01T00:00:00Z`;
    const title = `Longterm temperature average for month ${monthNumber}`;
    maps.push({
      id: `MultiMapViewStory10x10_${maps.length}`,
      title,
      bbox: {
        left: 348802.7893321724,
        bottom: 6436679.408988602,
        right: 884445.0192972326,
        top: 7310776.894151365,
      },
      layers: [
        {
          ...klimaatAtlasTG3,
          id: `templayer${maps.length}`,
          dimensions: [
            {
              name: 'time',
              currentValue: newTimeValue,
            },
          ],
        },
      ],
    });
  }

  return (
    <CoreThemeStoreProvider store={store}>
      <div style={{ width: '100%', height: '100vh' }}>
        <MultiMapViewConnect
          rows={10}
          cols={10}
          maps={maps}
          syncTime={false}
          showTimeSlider={false}
        />
        <MultiMapDimensionSelectConnect />
        <LayerManagerConnect showTitle />
      </div>
    </CoreThemeStoreProvider>
  );
};
