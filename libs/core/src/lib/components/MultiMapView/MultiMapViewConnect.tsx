/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import makeStyles from '@mui/styles/makeStyles';
import React from 'react';
import { useDispatch } from 'react-redux';
import { Theme } from '@mui/material';
import { TimeSliderConnect } from '../TimeSlider';
import { ConfigurableMapConnect } from '../ConfigurableMap';
import { Bbox, Layer } from '../../store/mapStore/types';
import { SyncGroupAddTargetPayload } from '../../store/generic/synchronizationGroups/types';
import { LegendConnect } from '../Legend';
import { genericActions } from '../../store';

export const useStyles = makeStyles((theme: Theme) => ({
  box: {
    boxSizing: 'border-box',
    float: 'left',
    '& .MapViewComponent': {
      border: `1px solid ${theme.palette.geowebColors.typographyAndIcons.iconLinkDisabled}`,
    },
  },
}));

export interface MultiMapPreset {
  id: string;
  title?: string;
  layers: Layer[];
  activeLayerId?: string;
  bbox?: Bbox;
  srs?: string;
  displayTimeInMap?: boolean;
  displayLayerManagerAndLegendButtonInMap?: boolean;
  displayDimensionSelectButtonInMap?: boolean;
  syncGroupsIds?: string[];
}
export interface MultiMapViewProps {
  rows: number;
  cols: number;
  maps: MultiMapPreset[];
  syncArea?: boolean;
  syncTime?: boolean;
  showTimeSlider?: boolean;
  showZoomControls?: boolean;
  multiLegend?: boolean;
}

export const MultiMapViewConnect: React.FC<MultiMapViewProps> = ({
  maps,
  rows,
  cols,
  showTimeSlider = true,
  showZoomControls = true,
  multiLegend = false,
}: MultiMapViewProps) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const syncGroupAddTarget = React.useCallback(
    (payload: SyncGroupAddTargetPayload): void => {
      dispatch(genericActions.syncGroupAddTarget(payload));
    },
    [dispatch],
  );
  React.useEffect(() => {
    maps.forEach((map) => {
      map.syncGroupsIds &&
        map.syncGroupsIds.forEach((syncGroupsId) => {
          syncGroupAddTarget({
            groupId: syncGroupsId,
            targetId: map.id,
            linked: true,
          });
        });
    });
  }, [maps, syncGroupAddTarget]);

  const mapStyle = {
    width: `${Math.round(100 / cols)}%`,
    height: `${Math.round(100 / rows)}%`,
  };
  const firstMapId = maps[0].id;

  return (
    <div
      style={{ height: '100%', position: 'relative' }}
      data-testid="MultiMapViewSliderConnect"
    >
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 1000,
          width: '100%',
        }}
      >
        {showTimeSlider && (
          <TimeSliderConnect
            sourceId={firstMapId}
            mapId={firstMapId}
            isAlwaysVisible
          />
        )}
      </div>
      {!multiLegend && (
        <LegendConnect showMapId mapId={firstMapId} multiLegend={multiLegend} />
      )}
      {maps.map((map: MultiMapPreset, mapIndex) => {
        const mapId = map.id;
        return (
          <div className={classes.box} key={mapId} style={mapStyle}>
            <ConfigurableMapConnect
              title={map.title}
              id={mapId}
              layers={map.layers || []}
              activeLayerId={map.activeLayerId || undefined}
              bbox={map.bbox}
              srs={map.srs}
              displayTimeInMap={map.displayTimeInMap || false}
              displayLayerManagerAndLegendButtonInMap={
                map.displayLayerManagerAndLegendButtonInMap
              }
              displayDimensionSelectButtonInMap={
                map.displayDimensionSelectButtonInMap
              }
              shouldShowZoomControls={showZoomControls && mapIndex === 0}
              showTimeSlider={false}
              disableTimeSlider
              multiLegend={multiLegend}
            />
          </div>
        );
      })}
    </div>
  );
};
