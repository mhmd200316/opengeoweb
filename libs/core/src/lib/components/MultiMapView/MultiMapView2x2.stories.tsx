/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { store } from '../../storybookUtils/store';
import { obsAirTemperature, radarLayer } from '../../utils/publicLayers';
import { MultiMapPreset, MultiMapViewConnect } from './MultiMapViewConnect';
import {
  mapPresetEumetsat,
  mapPresetHarmoniePrecipitationFlux,
} from './storyUtils/mapPresets';
import { LayerManagerConnect } from '../LayerManager';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { MultiMapDimensionSelectConnect } from '../MultiMapDimensionSelect';

export const MultiMapViewStory2x2 = (): React.ReactElement => {
  const maps: MultiMapPreset[] = [
    {
      id: 'precipradarnl',
      title: 'Precipitation Radar NL',
      layers: [radarLayer],
    },
    {
      id: 'temperatureobs',
      title: 'Temperature Observations',
      layers: [obsAirTemperature],
    },
    {
      id: 'harmonieprecip',
      title: 'Harmonie precipitation',
      layers: mapPresetHarmoniePrecipitationFlux.layers,
    },
    {
      id: 'msgnaturalview',
      title: 'EumetSat MSG natural view',
      layers: mapPresetEumetsat.layers,
    },
  ];

  return (
    <CoreThemeStoreProvider store={store}>
      <div style={{ width: '100%', height: '100vh' }}>
        <MultiMapViewConnect rows={2} cols={2} maps={maps} />
        <MultiMapDimensionSelectConnect />
        <LayerManagerConnect showTitle />
      </div>
    </CoreThemeStoreProvider>
  );
};
