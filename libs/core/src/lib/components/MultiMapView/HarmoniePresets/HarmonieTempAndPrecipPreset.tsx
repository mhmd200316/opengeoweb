/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { WMLayer } from '@opengeoweb/webmap';
import { MultiMapPreset, MultiMapViewConnect } from '../MultiMapViewConnect';
import { Layer } from '../../../store/mapStore/types';

const useGetReferenceTimes = (inputLayer): string[] => {
  const [referenceTimes, setReferenceTimes] = React.useState<string[]>([]);
  React.useEffect(() => {
    const referenceTimes: string[] = [];
    const wmLayer = new WMLayer({ ...inputLayer });
    wmLayer.parseLayerPromise().then((layer: WMLayer) => {
      const refTimeDim = layer.getDimension('reference_time');
      /* Loop through all values of the dimension.
         A map cannot be used for iterating,  as the dimension values are not in an array (it is an API). */
      for (let j = 0; j < refTimeDim.size(); j += 1) {
        const referenceTimeValue = refTimeDim.getValueForIndex(j);
        referenceTimes.push(referenceTimeValue);
      }
      setReferenceTimes(referenceTimes);
    });
  }, [inputLayer]);
  return referenceTimes;
};

export interface InitialHarmTempAndPrecipProps {
  layers: {
    topRow: Layer;
    bottomRow: Layer;
    topRowSyncGroups?: string[];
    bottomRowSyncGroups?: string[];
  };
  multiLegend?: boolean;
}

export const HarmonieTempAndPrecipPreset: React.FC<InitialHarmTempAndPrecipProps> =
  ({ layers, multiLegend = false }: InitialHarmTempAndPrecipProps) => {
    /* Get all the reference times for this layer */
    const referenceTimes = useGetReferenceTimes(layers.topRow);
    if (referenceTimes.length === 0) return <div>Loading...</div>;

    /* Helper to make a map layer prest */
    const makeMapPreset = (
      referenceTime: string,
      uniqueId: string,
      layer: Layer,
      syncGroupsIds: string[],
      col: number,
    ): MultiMapPreset => {
      return {
        id: uniqueId,
        title: `Harmonie run ${`${referenceTime
          .substring(8, 13)
          .replace('T', ' / ')}`}`,
        bbox: {
          left: -450651.2255879827,
          bottom: 6490531.093143953,
          right: 1428345.8183648037,
          top: 7438773.776232235,
        },
        srs: 'EPSG:3857',
        layers: [
          {
            ...layer,
            id: uniqueId,
            dimensions: [
              {
                name: 'reference_time',
                currentValue: referenceTime,
              },
            ],
          },
        ],
        syncGroupsIds,
        displayLayerManagerAndLegendButtonInMap: col === 0 || !syncGroupsIds,
        displayDimensionSelectButtonInMap: col === 0 || !syncGroupsIds,
      };
    };

    /* Make the preset */
    const nCols = 5;

    /* Add the harmonie air temperature layers with the different referencetimes */
    const mapsTemperature = [...Array(nCols)].map((_, index) => {
      const refTime = referenceTimes[referenceTimes.length - nCols + index];
      return makeMapPreset(
        refTime,
        `harm_air${index}`,
        layers.topRow,
        layers.topRowSyncGroups,
        index,
      );
    });

    /* Add the harmonie precipitation layers with the different referencetimes */
    const mapsPrecipitation = [...Array(nCols)].map((_, index) => {
      const refTime = referenceTimes[referenceTimes.length - nCols + index];
      return makeMapPreset(
        refTime,
        `harm_precip${index}`,
        layers.bottomRow,
        layers.bottomRowSyncGroups,
        index,
      );
    });
    const maps = mapsTemperature.concat(mapsPrecipitation);

    return (
      <MultiMapViewConnect
        rows={2}
        cols={5}
        maps={maps}
        multiLegend={multiLegend}
      />
    );
  };
