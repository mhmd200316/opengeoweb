/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import {
  obsGlobalSolarRadiation,
  obsAirPressureAtSeaLevel,
  obsRelativeHumidity,
  obsAirTemperature,
  obsWind,
  radarLayer,
  harmoniePrecipitation,
  harmonieAirTemperature,
  harmoniePressure,
  msgNaturalenhncdEUMETSAT,
} from '../../../utils/publicLayers';

export const mapPresetRadar = {
  layers: [radarLayer],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetObsTA = {
  layers: [obsAirTemperature],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetObsWind = {
  layers: [obsWind],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetObsPP = {
  layers: [obsAirPressureAtSeaLevel],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetObsRH = {
  layers: [obsRelativeHumidity],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetObsQG = {
  layers: [obsGlobalSolarRadiation],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetEumetsat = {
  layers: [msgNaturalenhncdEUMETSAT],
  proj: {
    bbox: {
      left: -7529663.50832266,
      bottom: 308359.5390525013,
      right: 7493930.85787452,
      top: 11742807.68245839,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetHarmoniePrecipitationFlux = {
  layers: [harmoniePrecipitation],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetHarmonieTemperature2m = {
  layers: [harmonieAirTemperature],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetHarmoniePressureMSL = {
  layers: [harmoniePressure],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};
