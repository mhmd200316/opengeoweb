/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import * as helpers from '../../store/mapStore/utils/helpers';

import ZoomControlConnect from './ZoomControlsConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';

jest.mock('../../store/mapStore/utils/helpers', () => ({
  getWMJSMapById: jest.fn(),
  generateLayerId: jest.fn(),
}));

describe('src/components/ZoomControls/ZoomControlsConnect', () => {
  it('should render zoom controls', () => {
    const mapId = 'test-1';
    const props = {
      mapId,
    };

    const mockStore = configureStore();
    const mockState = {};
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ZoomControlConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(helpers.getWMJSMapById).toHaveBeenCalledWith(props.mapId);

    expect(getByTestId('zoom-reset')).toBeTruthy();
    expect(getByTestId('zoom-in')).toBeTruthy();
    expect(getByTestId('zoom-out')).toBeTruthy();
  });

  it('should render zoom controls if visible in store', () => {
    const mapId = 'test-1';
    const props = {
      mapId,
    };

    const mockStore = configureStore();
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            shouldShowZoomControls: true,
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ZoomControlConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('zoom-reset')).toBeTruthy();
    expect(getByTestId('zoom-in')).toBeTruthy();
    expect(getByTestId('zoom-out')).toBeTruthy();
  });

  it('should hide zoom controls if not visible in store', () => {
    const mapId = 'test-1';
    const props = {
      mapId,
    };

    const mockStore = configureStore();
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            shouldShowZoomControls: false,
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ZoomControlConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(queryByTestId('zoom-reset')).toBeFalsy();
    expect(queryByTestId('zoom-in')).toBeFalsy();
    expect(queryByTestId('zoom-out')).toBeFalsy();
  });
});
