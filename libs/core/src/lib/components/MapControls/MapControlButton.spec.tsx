/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import MapControlButton from './MapControlButton';
import { CoreThemeProvider } from '../Providers/Providers';

describe('src/components/MapControls/MapControlButton', () => {
  it('work with default props', async () => {
    const props = {
      title: 'test tooltip',
      onClick: jest.fn(),
    };

    const { container } = render(
      <CoreThemeProvider>
        <MapControlButton {...props}>
          <span>button content</span>
        </MapControlButton>
      </CoreThemeProvider>,
    );
    const button = container.querySelector('[type="button"]');

    fireEvent.click(button);

    expect(props.onClick).toHaveBeenCalled();
    expect(
      container.querySelector(`[aria-label="${props.title}"]`),
    ).toBeTruthy();
  });

  it('should show/hide correct tooltip on hover', async () => {
    const props = {
      title: 'test tooltip',
      onClick: jest.fn(),
    };

    render(
      <CoreThemeProvider>
        <MapControlButton {...props}>
          <span data-testid="icon">button content</span>
        </MapControlButton>
      </CoreThemeProvider>,
    );

    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(screen.queryByText(props.title)).toBeFalsy();

    const button = screen.getByTestId('icon');

    fireEvent.mouseOver(button);
    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeTruthy();
      expect(screen.queryByText(props.title)).toBeTruthy();
    });

    fireEvent.mouseOut(button);
    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeFalsy();
      expect(screen.queryByText(props.title)).toBeFalsy();
    });
  });
});
