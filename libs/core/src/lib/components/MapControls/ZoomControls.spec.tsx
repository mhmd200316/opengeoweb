/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';

import ZoomControls from './ZoomControls';
import { CoreThemeProvider } from '../Providers/Providers';

describe('src/components/ZoomControls/ZoomControls', () => {
  it('should render ZoomControls and trigger correct callbacks', () => {
    const props = {
      onZoomIn: jest.fn(),
      onZoomOut: jest.fn(),
      onZoomReset: jest.fn(),
    };
    const { getByTestId } = render(
      <CoreThemeProvider>
        <ZoomControls {...props} />
      </CoreThemeProvider>,
    );

    fireEvent.click(getByTestId('zoom-out'));
    expect(props.onZoomOut).toHaveBeenCalled();

    fireEvent.click(getByTestId('zoom-in'));
    expect(props.onZoomIn).toHaveBeenCalled();

    fireEvent.click(getByTestId('zoom-reset'));
    expect(props.onZoomReset).toHaveBeenCalled();
  });
});
