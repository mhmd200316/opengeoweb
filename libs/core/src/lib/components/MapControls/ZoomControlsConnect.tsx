/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useSelector } from 'react-redux';
import { mapSelectors } from '../../store';

import { getWMJSMapById } from '../../store/mapStore/utils/helpers';
import { AppStore } from '../../types/types';

import ZoomControls from './ZoomControls';

interface ZoomControlConnectProps {
  mapId?: string;
}

const ZoomControlConnect: React.FC<ZoomControlConnectProps> = ({
  mapId,
}: ZoomControlConnectProps) => {
  const isVisible = useSelector((store: AppStore) =>
    mapSelectors.isZoomControlsVisible(store, mapId),
  );
  const adagucRef = React.useRef(null);

  React.useEffect(() => {
    adagucRef.current = getWMJSMapById(mapId);
  }, [mapId]);

  const onZoomIn = (): void => adagucRef.current.zoomIn();
  const onZoomOut = (): void => adagucRef.current.zoomOut();
  const onZoomReset = (): void =>
    adagucRef.current.zoomToLayer(adagucRef.current.getActiveLayer());

  if (!isVisible) {
    return null;
  }

  return (
    <ZoomControls
      onZoomIn={onZoomIn}
      onZoomOut={onZoomOut}
      onZoomReset={onZoomReset}
    />
  );
};

export default ZoomControlConnect;
