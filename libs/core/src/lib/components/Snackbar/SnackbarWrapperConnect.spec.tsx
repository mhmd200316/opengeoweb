/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import { SnackbarWrapperConnect } from './SnackbarWrapperConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { snackbarActions } from '../../store';

describe('src/lib/components/Snackbar', () => {
  it('should render children even if snackbar closed', () => {
    const mockStore = configureStore();
    const storeNoMessage = mockStore({
      snackbar: {
        entities: {},
        ids: [],
      },
    });
    storeNoMessage.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByText, queryByTestId } = render(
      <CoreThemeStoreProvider store={storeNoMessage}>
        <SnackbarWrapperConnect>
          <div>I am a child</div>
        </SnackbarWrapperConnect>
      </CoreThemeStoreProvider>,
    );

    expect(getByText('I am a child')).toBeTruthy();
    expect(queryByTestId('snackbarComponent')).toBeFalsy();
  });

  it('should render children when snackbar opened', () => {
    const mockStore = configureStore();
    const storeOneMessage = mockStore({
      snackbar: {
        entities: {
          snackbar1: {
            id: 'snackbar1',
            message: 'snackbar message 1',
          },
        },
        ids: ['snackbar1'],
      },
    });
    storeOneMessage.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByText, getByTestId } = render(
      <CoreThemeStoreProvider store={storeOneMessage}>
        <SnackbarWrapperConnect>
          <div>I am a child</div>
        </SnackbarWrapperConnect>
      </CoreThemeStoreProvider>,
    );

    expect(getByText('I am a child')).toBeTruthy();
    expect(getByTestId('snackbarComponent')).toBeTruthy();
  });

  it('should call close when clicked on the close button', () => {
    const mockStore = configureStore();
    const storeOneMessage = mockStore({
      snackbar: {
        entities: {
          snackbar1: {
            id: 'snackbar1',
            message: 'snackbar message 1',
          },
        },
        ids: ['snackbar1'],
      },
    });
    storeOneMessage.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByText, getByTestId } = render(
      <CoreThemeStoreProvider store={storeOneMessage}>
        <SnackbarWrapperConnect>
          <div>I am a child</div>
        </SnackbarWrapperConnect>
      </CoreThemeStoreProvider>,
    );

    expect(getByText('I am a child')).toBeTruthy();
    expect(getByTestId('snackbarComponent')).toBeTruthy();
    const expectedAction = [snackbarActions.closeSnackbar()];
    fireEvent.click(getByTestId('snackbarCloseButton'));
    expect(storeOneMessage.getActions()).toEqual(expectedAction);
  });
});
