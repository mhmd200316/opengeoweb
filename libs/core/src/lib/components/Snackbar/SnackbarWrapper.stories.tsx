/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Button, Typography } from '@mui/material';
import * as React from 'react';
import { useDispatch } from 'react-redux';
import { CoreThemeStoreProvider, store } from '../..';
import { snackbarActions } from '../../store/snackbar';
import { CoreThemeProvider } from '../Providers/Providers';
import { SnackbarWrapper } from './SnackbarWrapper';
import { SnackbarWrapperConnect } from './SnackbarWrapperConnect';

export const SnackbarDemo = (): React.ReactElement => {
  const [open, setOpen] = React.useState(true);

  const handleToggle = (): void => {
    setOpen(!open);
  };

  const handleClose = (): void => {
    setOpen(false);
  };

  return (
    <CoreThemeProvider>
      <SnackbarWrapper
        isOpen={open}
        onCloseSnackbar={handleClose}
        message="This is a snackbar notification"
        id="snackbar1"
      >
        <>
          <Typography variant="body2"> We are snackbar children </Typography>
          <Button variant="contained" onClick={handleToggle}>
            Toggle snackbar
          </Button>
        </>
      </SnackbarWrapper>
    </CoreThemeProvider>
  );
};

const TwoSnackbarConnectedDemoComponent = (): React.ReactElement => {
  const dispatch = useDispatch();

  const handleToggle = (): void => {
    dispatch(
      snackbarActions.openSnackbar({
        snackbarContent: {
          message: 'I am the first snackbar',
        },
      }),
    );
    setTimeout(() => {
      return dispatch(
        snackbarActions.openSnackbar({
          snackbarContent: {
            message: 'I am the second snackbar',
          },
        }),
      );
    }, 2000);
  };

  return (
    <SnackbarWrapperConnect>
      <>
        <Typography variant="body2">
          Pressing the below button will trigger a snackbar to appear. A second
          snackbar will be triggered 2 sec later.
        </Typography>
        <Button variant="contained" onClick={handleToggle}>
          Toggle two snackbars
        </Button>
      </>
    </SnackbarWrapperConnect>
  );
};

export const TwoSnackbarsDemo = (): React.ReactElement => {
  return (
    <CoreThemeStoreProvider store={store}>
      <TwoSnackbarConnectedDemoComponent />
    </CoreThemeStoreProvider>
  );
};

export default { title: 'components/SnackbarWrapper' };
