/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { IconButton, Snackbar, SnackbarProps } from '@mui/material';

import { Close } from '@opengeoweb/theme';

interface SnackBarWrapperProps extends SnackbarProps {
  isOpen: boolean;
  id: string;
  onCloseSnackbar: () => void;
}

export const SnackbarWrapper: React.FC<SnackBarWrapperProps> = ({
  children,
  isOpen,
  onCloseSnackbar,
  id,
  ...props
}: SnackBarWrapperProps) => {
  return (
    <>
      {children}
      <Snackbar
        data-testid="snackbarComponent"
        open={isOpen}
        onClose={onCloseSnackbar}
        autoHideDuration={null}
        key={id}
        ClickAwayListenerProps={{ mouseEvent: false }}
        action={
          <IconButton
            aria-label="close"
            data-testid="snackbarCloseButton"
            onClick={onCloseSnackbar}
            sx={{
              color: 'geowebColors.snackbar.action',
              '&:hover': {
                backgroundColor: 'geowebColors.snackbar.actionHover',
              },
            }}
          >
            <Close />
          </IconButton>
        }
        {...props}
      />
    </>
  );
};
