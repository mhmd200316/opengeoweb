/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, findAllByRole, fireEvent } from '@testing-library/react';
import WMSServerList from './WMSServerList';
import { preloadedDefaultMapServices } from '../../../utils/defaultConfigurations';

describe('src/components/WMSLoader/WMSServerList/WMSServerList', () => {
  it('should show a menu item for each service in the list', async () => {
    const mockProps = {
      availableServices: preloadedDefaultMapServices,
      handleChangeService: jest.fn(),
      service: preloadedDefaultMapServices[0],
    };
    const { container } = render(<WMSServerList {...mockProps} />);
    const list = await findAllByRole(container, 'button');

    expect(list).toBeTruthy();
    expect(list.length).toEqual(preloadedDefaultMapServices.length);
    list.forEach((li) =>
      expect(
        preloadedDefaultMapServices.find(
          (service) => service.name === li.textContent,
        ),
      ).toBeTruthy(),
    );
  });

  it('should trigger handleChangeService when clicking a service in the list', async () => {
    const mockProps = {
      availableServices: preloadedDefaultMapServices,
      handleChangeService: jest.fn(),
      service: preloadedDefaultMapServices[0],
    };
    const { container } = render(<WMSServerList {...mockProps} />);
    const list = await findAllByRole(container, 'button');

    expect(list).toBeTruthy();

    const newServiceIndex = 2;
    fireEvent.click(list[newServiceIndex]);

    expect(mockProps.handleChangeService).toHaveBeenCalledWith(
      mockProps.availableServices[2],
    );
  });

  it('should sort the services alphabetically', async () => {
    const mockProps = {
      availableServices: preloadedDefaultMapServices,
      handleChangeService: jest.fn(),
      service: preloadedDefaultMapServices[0],
    };
    const { container } = render(<WMSServerList {...mockProps} />);
    const list = await findAllByRole(container, 'button');

    expect(list).toBeTruthy();
    expect(list.length).toEqual(preloadedDefaultMapServices.length);
    expect(list[0].textContent).toEqual('DWD');
    expect(list[1].textContent).toEqual('ECMWF');
    expect(list[2].textContent).toEqual('FMIopenwms');
  });
});
