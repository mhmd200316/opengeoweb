/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Paper,
  List,
  ListItem,
  ListItemText,
  InputLabel,
  Grid,
} from '@mui/material';
import { Service } from '../services';

interface WMSServerListProps {
  availableServices: Service[];
  handleChangeService: (service: Service) => void;
  service: Service;
}

const WMSServerList: React.FC<WMSServerListProps> = ({
  availableServices,
  handleChangeService,
  service: activeService,
}: WMSServerListProps) => {
  const handleListItemClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    service: Service,
  ): void => {
    handleChangeService(service);
  };

  const sortedServices = [...availableServices].sort((a, b) =>
    a.name.localeCompare(b.name),
  );

  return (
    <>
      <Grid container direction="row" spacing={1} style={{ height: '34px' }}>
        <Grid item>
          <InputLabel shrink htmlFor="select-multiple-native">
            Available services
          </InputLabel>
        </Grid>
      </Grid>
      <Paper style={{ height: 200, overflow: 'auto' }}>
        <List dense>
          {sortedServices.map((service) => (
            <ListItem
              data-testid="service-item"
              key={service.url}
              selected={service === activeService}
              button
              onClick={(event): void => {
                handleListItemClick(event, service);
              }}
            >
              <ListItemText primary={service.name} />
            </ListItem>
          ))}
        </List>
      </Paper>
    </>
  );
};

export default WMSServerList;
