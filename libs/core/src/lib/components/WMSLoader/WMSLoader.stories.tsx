/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { Typography, Paper, Grid } from '@mui/material';

import { MapViewConnect } from '../MapView';
import { store } from '../../storybookUtils/store';

import WMSLoaderConnect from './WMSLoaderConnect';

import { initialBbox } from '../../storybookUtils/defaultStorySettings';
import { mapActions, layerActions } from '../../store';
import * as publicLayers from '../../utils/publicLayers';
import { KNMImsgcpp, MeteoCanada } from '../../utils/publicServices';
import { LayerType } from '../../store/mapStore/types';
import { CoreThemeStoreProvider } from '../Providers/Providers';

export default { title: 'components/WMSLoader' };

const services = [MeteoCanada, KNMImsgcpp];
interface WMSLoadProps {
  setBbox: typeof mapActions.setBbox;
  setBaseLayers: typeof layerActions.setBaseLayers;
}

const connectRedux = connect(null, {
  setLayers: layerActions.setLayers,
  setBbox: mapActions.setBbox,
  setBaseLayers: layerActions.setBaseLayers,
});

// --- example 1
const WMSLoad = connectRedux(({ setBbox, setBaseLayers }: WMSLoadProps) => {
  React.useEffect(() => {
    setBbox({
      bbox: initialBbox.bbox,
      srs: initialBbox.srs,
      mapId: 'mapid_1',
    });
    setBaseLayers({
      mapId: 'mapid_1',
      layers: [publicLayers.baseLayerGrey, publicLayers.overLayer],
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div>
      <CoreThemeStoreProvider store={store}>
        <div style={{ height: '100vh' }}>
          <MapViewConnect mapId="mapid_1" />
        </div>
        <div
          style={{
            position: 'absolute',
            left: '50px',
            top: '20px',
            zIndex: 10000,
            right: '200px',
          }}
        >
          <Paper
            style={{
              padding: '5px',
              width: '400px',
            }}
          >
            <Grid container>
              <Grid item xs={6}>
                <Typography>Load WMS Layer:</Typography>
                <WMSLoaderConnect mapId="mapid_1" />
              </Grid>
              <Grid item xs={6}>
                <Typography>Load WMS BaseLayer:</Typography>
                <WMSLoaderConnect
                  mapId="mapid_1"
                  layerType={LayerType.baseLayer}
                />
              </Grid>
              <Grid item xs={12}>
                <Typography>
                  Load WMS Layer with preconfigured services:
                </Typography>
                <WMSLoaderConnect
                  mapId="mapid_1"
                  preloadedServices={services}
                />
              </Grid>
            </Grid>
          </Paper>
        </div>
      </CoreThemeStoreProvider>
    </div>
  );
});

export const DemoWMSLoad: React.FC = () => (
  <CoreThemeStoreProvider store={store}>
    <WMSLoad />
  </CoreThemeStoreProvider>
);
