/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Grid, InputLabel, Tooltip, IconButton } from '@mui/material';
import { Cached } from '@opengeoweb/theme';

interface WMSLayerTreeHeaderProps {
  serviceName: string;
  onClickReload?: () => void;
}

const WMSLayerTreeHeader: React.FC<WMSLayerTreeHeaderProps> = ({
  serviceName,
  onClickReload,
}: WMSLayerTreeHeaderProps) => {
  return (
    <Grid container direction="row" spacing={1} justifyContent="space-between">
      <Grid item>
        <InputLabel shrink htmlFor="select-multiple-native">
          Available layers for {serviceName}
        </InputLabel>
      </Grid>
      {onClickReload && (
        <Grid item>
          <Tooltip title="Reload layers from service" placement="top">
            <IconButton
              data-testid="reloadLayers"
              color="primary"
              style={{ padding: 3 }}
              onClick={onClickReload}
              size="large"
            >
              <Cached />
            </IconButton>
          </Tooltip>
        </Grid>
      )}
    </Grid>
  );
};

export default WMSLayerTreeHeader;
