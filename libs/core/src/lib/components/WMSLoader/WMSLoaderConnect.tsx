/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import WMSLoader from './WMSLoader';
import WMSLayerTreeConnect from './WMSLayerTree/WMSLayerTreeConnect';
import { Service } from './services';
import { LayerType } from '../../store/mapStore/types';
import { preloadedDefaultMapServices } from '../../utils/defaultConfigurations';

interface WMSLoaderConnectProps {
  mapId: string;
  preloadedServices?: Service[];
  layerType?: LayerType;
  tooltip?: string;
}

/**
 * A pop-up allowing you to add new WMS sources which can then be displayed on the map.
 * A list of preconfigured sources can be given to the component which will always be availble to retrieve layers from
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {Service[]} [preloadedServices] **optional** preloadedServices: array of Service objects - contains an array of Service objects that are to be loaded as preconfigured WMS services
 * @example
 * ``` <WMSLoaderConnect mapId="mapid_1" preloadedServices = {preloadedServicesList} /> ```
 */
const WMSLoaderConnect: React.FC<WMSLoaderConnectProps> = ({
  mapId,
  layerType = LayerType.mapLayer,
  preloadedServices = preloadedDefaultMapServices,
  tooltip = '',
}: WMSLoaderConnectProps) => (
  <WMSLoader
    preloadedServices={preloadedServices}
    tooltip={tooltip}
    onRenderTree={(service): React.ReactChild => (
      <WMSLayerTreeConnect
        mapId={mapId}
        service={service}
        layerType={layerType}
      />
    )}
  />
);

export default WMSLoaderConnect;
