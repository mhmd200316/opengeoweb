/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  fireEvent,
  waitForElementToBeRemoved,
  waitFor,
} from '@testing-library/react';
import WMSLoader from './WMSLoader';
import {
  MOCK_URL_WITH_CHILDREN,
  MOCK_URL_INVALID,
  mockLayersWithChildren,
  MOCK_URL_SLOW,
  MOCK_URL_WITH_SUBCATEGORY,
  mockLayersWithSubcategory,
  MOCK_URL_SLOW_FAILS,
  mockLayersWithNoTitle,
  MOCK_URL_WITH_NO_TITLE,
  MOCK_URL_WITH_NO_TITLE_OR_NAME,
  MOCK_URL_HTTP,
} from '../../utils/__mocks__/getCapabilities';
import { defaultTestServices } from '../../utils/defaultTestSettings';

jest.mock('../../utils/getCapabilities');

describe('src/components/WMSLoader/WMSLoader', () => {
  it('should open and close the dialog when clicking the corresponding buttons', async () => {
    const { getByRole, getByText, queryByRole } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );

    const openDialogButton = getByRole('button');
    fireEvent.click(openDialogButton);

    const dialog = getByRole('dialog');
    expect(dialog).toBeTruthy();

    const closeButton = getByText('Close');
    fireEvent.click(closeButton);

    await waitForElementToBeRemoved(() => queryByRole('dialog'));
  });

  it('should add a new service to the list when entering a valid service url and pressing the add button', async () => {
    const { getAllByTestId, getByRole, findByText, getByTestId } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url in the inputfield
    const inputServiceUrl = getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: MOCK_URL_WITH_CHILDREN },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        MOCK_URL_WITH_CHILDREN,
      ),
    );

    // add the service to the list by clicking the add button
    fireEvent.click(getByTestId('add-service'));
    expect(await findByText(mockLayersWithChildren.title)).toBeTruthy();

    // the service should be added to the list in alphabetical order
    const serviceList = getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(mockLayersWithChildren.title);
  });

  it('should add a new service to the list when entering a valid service url and using the enter key', async () => {
    const { getAllByTestId, getByRole, findByText } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url in the inputfield
    const inputServiceUrl = getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: MOCK_URL_WITH_CHILDREN },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        MOCK_URL_WITH_CHILDREN,
      ),
    );

    // add the service to the list by pressing the Enter key
    fireEvent.keyPress(inputServiceUrl, {
      key: 'Enter',
      code: 'Enter',
      charCode: 13,
    });

    expect(await findByText(mockLayersWithChildren.title)).toBeTruthy();

    // the service should be added to the list in alphabetical order
    const serviceList = getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(mockLayersWithChildren.title);
  });

  it('should add a new service to the list using name if no title available', async () => {
    const { getAllByTestId, getByRole, findByText } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url in the inputfield
    const inputServiceUrl = getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: MOCK_URL_WITH_NO_TITLE },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        MOCK_URL_WITH_NO_TITLE,
      ),
    );

    // add the service to the list by pressing the Enter key
    fireEvent.keyPress(inputServiceUrl, {
      key: 'Enter',
      code: 'Enter',
      charCode: 13,
    });

    expect(await findByText(mockLayersWithNoTitle.name)).toBeTruthy();

    // the service should be added to the list in alphabetical order
    const serviceList = getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(mockLayersWithNoTitle.name);
  });

  it('should add a new service to the list using url if no title or name available', async () => {
    const { getAllByTestId, getByRole, findByText } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url in the inputfield
    const inputServiceUrl = getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: MOCK_URL_WITH_NO_TITLE_OR_NAME },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        MOCK_URL_WITH_NO_TITLE_OR_NAME,
      ),
    );

    // add the service to the list by pressing the Enter key
    fireEvent.keyPress(inputServiceUrl, {
      key: 'Enter',
      code: 'Enter',
      charCode: 13,
    });

    const shortenedURL = MOCK_URL_WITH_NO_TITLE_OR_NAME.substring(
      MOCK_URL_WITH_NO_TITLE_OR_NAME.indexOf('//') + 2,
      MOCK_URL_WITH_NO_TITLE_OR_NAME.indexOf('?'),
    );

    expect(await findByText(shortenedURL)).toBeTruthy();

    // the service should be added to the list in alphabetical order
    const serviceList = getAllByTestId('service-item');
    expect(serviceList[2].textContent).toEqual(shortenedURL);
  });

  it('should update the active service after clicking a service', async () => {
    const { findAllByTestId, getByRole } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);
    const serviceList = await findAllByTestId('service-item');

    // first one should be active
    expect(serviceList[0].getAttribute('class')).toContain('Mui-selected');

    // click the second one
    fireEvent.click(serviceList[1]);

    // now second one should be active
    await waitFor(() =>
      expect(serviceList[1].getAttribute('class')).toContain('Mui-selected'),
    );
  });

  it('should show an error message when the given service url is invalid', async () => {
    const { getByTestId, getByRole, findByText } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing an invalid service url in the inputfield
    const inputServiceUrl = getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: 'some-test-url' },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual('some-test-url'),
    );

    // try to add the service to the list
    fireEvent.click(getByTestId('add-service'));

    // error message should be displayed
    expect(await findByText('Please enter a valid URL')).toBeTruthy();
  });

  it('should show an error message when the given service url is not a wms service', async () => {
    const { getByTestId, getByRole, findByText } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url that is not a wms service
    const inputServiceUrl = getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: MOCK_URL_INVALID },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(MOCK_URL_INVALID),
    );

    // try to add the service to the list
    fireEvent.click(getByTestId('add-service'));

    // error message should be displayed
    expect(await findByText('Please enter a valid WMS service')).toBeTruthy();
  });

  it('should show an error message when the given service url starts with http instead of https', async () => {
    const { getByTestId, getByRole, findByText } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    // simulate typing a service url with http protocol
    const inputServiceUrl = getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: MOCK_URL_HTTP },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(MOCK_URL_HTTP),
    );

    // try to add the service to the list
    fireEvent.click(getByTestId('add-service'));

    // error message should be displayed
    expect(
      await findByText(
        'Use https or allow Mixed Content in your browser to use this WMS service',
      ),
    ).toBeTruthy();
  });

  it('should set the service as active when trying to add a service to the list twice', async () => {
    const { getAllByTestId, getByRole, findByText, getByTestId } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    // add a service to the list
    const inputServiceUrl = getByRole('textbox');
    fireEvent.change(inputServiceUrl, {
      target: { value: MOCK_URL_WITH_CHILDREN },
    });
    await waitFor(() =>
      expect(inputServiceUrl.getAttribute('value')).toEqual(
        MOCK_URL_WITH_CHILDREN,
      ),
    );
    fireEvent.click(getByTestId('add-service'));
    expect(await findByText(mockLayersWithChildren.title)).toBeTruthy();

    // check that it is not active yet
    const serviceList = getAllByTestId('service-item');
    expect(serviceList[2].getAttribute('class')).not.toContain('Mui-selected');
    const expectedListLength = serviceList.length;

    // add the same service again
    fireEvent.change(inputServiceUrl, {
      target: { value: MOCK_URL_WITH_CHILDREN },
    });
    expect(inputServiceUrl.getAttribute('value')).toEqual(
      MOCK_URL_WITH_CHILDREN,
    );
    fireEvent.click(getByTestId('add-service'));

    // check that it is active now
    expect(serviceList[2].getAttribute('class')).toContain('Mui-selected');
    expect(
      await findByText(`Available layers for ${serviceList[2].textContent}`),
    ).toBeTruthy();

    // check that it wasn't added twice
    expect(getAllByTestId('service-item').length).toEqual(expectedListLength);
  });

  it('should be able to pass renderProp onRenderTree', async () => {
    const props = {
      onRenderTree: jest.fn(),
      preloadedServices: defaultTestServices,
    };
    const { getByRole } = render(<WMSLoader {...props} />);
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    expect(props.onRenderTree).toHaveBeenCalledWith(defaultTestServices[0]);
  });

  it('should use the preselected list of services if none passed in', async () => {
    const { getByRole, findAllByTestId, getByText } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    const list = await findAllByTestId('service-item');
    expect(list.length).toEqual(defaultTestServices.length);

    for (let i = 0; i < defaultTestServices.length; i += 1) {
      expect(getByText(defaultTestServices[i].name)).toBeTruthy();
    }
  });

  it('should use the passed in list of services', async () => {
    const { getByRole, findAllByTestId, getByText } = render(
      <WMSLoader preloadedServices={defaultTestServices} />,
    );
    const openDialogButton = getByRole('button');
    fireEvent.click(openDialogButton);

    const list = await findAllByTestId('service-item');
    expect(list.length).toEqual(defaultTestServices.length);

    for (let i = 0; i < defaultTestServices.length; i += 1) {
      expect(getByText(defaultTestServices[i].name)).toBeTruthy();
    }
  });

  it('should show the layers of the new service after selecting a new service and the previous service loads successfully in the background', async () => {
    jest.useFakeTimers();

    const services = [
      {
        name: 'Meteo',
        url: MOCK_URL_WITH_SUBCATEGORY,
        id: 'Meteo',
      },
      {
        name: 'Slow service',
        url: MOCK_URL_SLOW,
        id: 'SlowService',
      },
    ];

    const { getByRole, findAllByTestId, getByTestId, queryByRole } = render(
      <WMSLoader preloadedServices={services} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    const serviceList = await findAllByTestId('service-item');

    // click the second service, for which loading is slow
    fireEvent.click(serviceList[1]);
    expect(getByRole('progressbar')).toBeTruthy();

    // click the first service, for which loading is fast
    fireEvent.click(serviceList[0]);

    // wait for both services to finish loading
    jest.advanceTimersByTime(500);
    await waitForElementToBeRemoved(() => queryByRole('progressbar'));

    // the content of the layer list should correspond to the first service
    const layerList = getByTestId('layer-list');

    expect(layerList.textContent).toContain(
      mockLayersWithSubcategory.children[0].title,
    );
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should show the layers of the new service after selecting a new service and the previous service gives an error in the background', async () => {
    jest.useFakeTimers();

    const services = [
      {
        name: 'Meteo',
        url: MOCK_URL_WITH_SUBCATEGORY,
        id: 'Meteo',
      },
      {
        name: 'Slow service',
        url: MOCK_URL_SLOW_FAILS,
        id: 'SlowService',
      },
    ];

    const { getByRole, findAllByTestId, getByTestId, queryByRole } = render(
      <WMSLoader preloadedServices={services} />,
    );
    const openDialogButton = getByRole('button');

    fireEvent.click(openDialogButton);

    const serviceList = await findAllByTestId('service-item');

    // click the second service, for which loading is slow
    fireEvent.click(serviceList[1]);
    expect(getByRole('progressbar')).toBeTruthy();

    // click the first service, for which loading is fast
    fireEvent.click(serviceList[0]);

    // wait for both services to finish loading
    jest.advanceTimersByTime(500);
    await waitForElementToBeRemoved(() => queryByRole('progressbar'));

    // the content of the layer list should correspond to the first service
    const layerList = getByTestId('layer-list');

    expect(layerList.textContent).toContain(
      mockLayersWithSubcategory.children[0].title,
    );
    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
