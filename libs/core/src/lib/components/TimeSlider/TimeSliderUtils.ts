/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import moment from 'moment';
import React from 'react';

import { DateInterval } from '@opengeoweb/webmap';
import {
  Dimension,
  Layer,
  Scale,
  SpeedFactorType,
} from '../../store/mapStore/types';
import { getWMJSTimeDimensionForLayerId } from '../../store/mapStore/utils/helpers';
import CanvasComponent from '../CanvasComponent/CanvasComponent';

export const defaultAnimationDelayAtStart = 250;
export const defaultDelay = 1000; // [ms]
export const defaultTimeStep = 5;
export const speedFactors = [
  0.1, 0.2, 0.5, 1, 2, 4, 8, 16,
] as SpeedFactorType[]; // Declares available animation speed multipliers for default delay

export interface TimeBounds {
  selectedTime: number;
  startTime: number;
  endTime: number;
}

/**
 * Returns speed delay for given speedFactor. For options, see defined above in "speedFactors"
 */
export const getSpeedDelay = (speedFactor: SpeedFactorType): number => {
  return defaultDelay / speedFactor;
};

export const getSpeedFactor = (speedDelay: number): SpeedFactorType => {
  return (defaultDelay / speedDelay) as SpeedFactorType;
};

/**
 * Returns time bounds from the given dimension. If no time dimension given, current time is returned as default
 */
export const getTimeBounds = (dimensions: Dimension[]): TimeBounds => {
  const defaultStartEnd = moment
    .utc()
    .set({ second: 0, millisecond: 0 })
    .unix();
  const timeDimension = dimensions
    ? dimensions.find((dim) => dim.name === 'time')
    : null;
  const startTime =
    timeDimension && timeDimension.minValue
      ? moment.utc(timeDimension.minValue).unix()
      : defaultStartEnd;
  const endTime =
    timeDimension && timeDimension.maxValue
      ? moment.utc(timeDimension.maxValue).unix()
      : defaultStartEnd;
  const currentValueIsValid =
    timeDimension &&
    timeDimension.currentValue &&
    moment.utc(timeDimension.currentValue).isValid();
  const selectedTime = currentValueIsValid
    ? moment.utc(timeDimension.currentValue).unix()
    : moment.utc().unix();
  return {
    selectedTime,
    startTime,
    endTime,
  };
};

export interface MomentTimeBounds {
  selectedTime: moment.Moment;
  startTime: moment.Moment;
  endTime: moment.Moment;
}

/**
 * Returns time bounds as *Moment* time instances from the given dimension. If no time dimension given, current time is returned as default
 */
export const getMomentTimeBounds = (
  dimensions: Dimension[],
): MomentTimeBounds => {
  const { selectedTime, startTime, endTime }: TimeBounds =
    getTimeBounds(dimensions);

  return {
    selectedTime: moment.unix(selectedTime),
    startTime: moment.unix(startTime),
    endTime: moment.unix(endTime),
  };
};

export const scalingCoefficient = (
  end: number,
  start: number,
  width: number,
): number => width / (end - start);

export const timestampToPixelEdges = (
  timestamp: number,
  start: number,
  end: number,
  width: number,
): number => (timestamp - start) * scalingCoefficient(end, start, width);

export const timestampToPixel = (
  timestamp: number,
  centerTime: number,
  widthPx: number,
  secondsPerPx: number,
): number => widthPx / 2 - (centerTime - timestamp) / secondsPerPx;

export const pixelToTimestamp = (
  timePx: number,
  centerTime: number,
  widthPx: number,
  secondsPerPx: number,
): number => centerTime - (widthPx / 2 - timePx) * secondsPerPx;

let onSetNewDateDebouncer;

export const onsetNewDateDebounced = (
  dateToSet: string,
  onSetNewDate: (newDate: string) => void,
): void => {
  /* Debounce the dateToSet with a simple timeout */
  if (onSetNewDateDebouncer === undefined) {
    onSetNewDateDebouncer = dateToSet;
    onSetNewDate(dateToSet);
    window.setTimeout(() => {
      if (
        onSetNewDateDebouncer !== undefined &&
        onSetNewDateDebouncer !== dateToSet
      ) {
        onSetNewDate(onSetNewDateDebouncer);
      }
      onSetNewDateDebouncer = undefined;
    }, 10);
  } else {
    onSetNewDateDebouncer = dateToSet;
  }
};

export const roundWithTimeStep = (
  unixTime: number,
  timeStep: number,
  type?: string,
): number => {
  const adjustedTimeStep = timeStep * 60;
  if (!type || type === 'round') {
    return Math.round(unixTime / adjustedTimeStep) * adjustedTimeStep;
  }
  if (type === 'floor') {
    return Math.floor(unixTime / adjustedTimeStep) * adjustedTimeStep;
  }
  if (type === 'ceil') {
    return Math.ceil(unixTime / adjustedTimeStep) * adjustedTimeStep;
  }
  return undefined;
};

export const setNewRoundedTime = (
  x: number,
  centerTime: number,
  width: number,
  secondsPerPx: number,
  timeStep: number,
  dataStartTime: number,
  dataEndTime: number,
  onSetNewDate: (newDate: string) => void,
): void => {
  const unixTime = pixelToTimestamp(x, centerTime, width, secondsPerPx);
  const roundedTime = roundWithTimeStep(unixTime, timeStep);
  const selectedTimeString = moment
    .unix(Math.min(Math.max(roundedTime, dataStartTime), dataEndTime))
    .toISOString();
  onsetNewDateDebounced(selectedTimeString, onSetNewDate);
};

export const getDataLimitsFromLayers = (layers: Layer[]): Array<number> =>
  layers.reduce(
    ([start, end], layer) => {
      const dimension = getWMJSTimeDimensionForLayerId(layer.id);
      if (dimension) {
        const lastValue = moment(dimension.getLastValue()).unix();
        const firstValue = moment(dimension.getFirstValue()).unix();

        const newLast = lastValue > end ? lastValue : end;
        const newFirst = firstValue < start ? firstValue : start;

        return [newFirst, newLast];
      }
      return [start, end];
    },
    /* Using the maximum 32-bit value and 0 as starting points
     * bigger values like Number.MAX_VALUE or Number.MAX_SAFE_INTEGER
     * cause weird behaviour as timestamps break at 32bit limit (year 2038)
     */
    [2147483647, 0],
  );

export const getNextTimeStepvalue = (value: number, max: number): number => {
  const newValue = value + 1;
  return newValue > max ? max : newValue;
};

export const getPreviousTimeStepvalue = (
  value: number,
  min: number,
): number => {
  const newValue = value - 1;
  return newValue < min ? min : newValue;
};

export const getValueFromKeyboardEvent = (
  event: React.KeyboardEvent<HTMLInputElement>,
  value: number,
  min: number,
  max: number,
  supportFourDirectionNavigation = false,
): number => {
  if (!supportFourDirectionNavigation) {
    if (event.ctrlKey || event.metaKey) {
      switch (event.key) {
        case 'ArrowDown':
          return getPreviousTimeStepvalue(value, min);
        case 'ArrowUp':
          return getNextTimeStepvalue(value, max);
        default:
          return null;
      }
    }
  } else {
    // currently supportFourDirectionNavigation (left/right/up/down) is only used in horizontal slider TimeSliderScaleSlider
    switch (event.key) {
      case 'ArrowDown':
      case 'ArrowLeft':
        return getPreviousTimeStepvalue(value, min);
      case 'ArrowUp':
      case 'ArrowRight':
        return getNextTimeStepvalue(value, max);
      default:
        return null;
    }
  }

  return null;
};

export const getTimeStepFromDataInterval = (
  timeInterval: DateInterval,
): number => {
  switch (timeInterval.isRegularInterval) {
    case false:
      switch (timeInterval.year) {
        case 0: // month
          return 30 * 24 * 60;
        default:
          return timeInterval.year * 365 * 24 * 60;
      }
    case true:
      if (timeInterval.day !== 0) {
        return timeInterval.day * 24 * 60;
      }
      if (timeInterval.hour !== 0) {
        return timeInterval.hour * 60;
      }
      if (timeInterval.minute !== 0) {
        return timeInterval.minute;
      }
      if (timeInterval.second !== 0) {
        return timeInterval.second / 60.0;
      }
      return 5;
    default:
      return 5;
  }
};

export const getActiveLayerTimeStep = (
  timeDimension: Dimension,
): number | null =>
  timeDimension?.timeInterval
    ? getTimeStepFromDataInterval(timeDimension.timeInterval)
    : null;

/**
 * This function calculates a suitable secondsPerpx value,
 * when a data scale is selected. secondsPerPx is different for every data set
 * and is calculated here with a function getDataLimitsfromLayers.
 * There in the function the largest time range of all layers is selected.
 * @param layers
 * @returns secondsPerPx for data sets of layers calculated 60*2*3,
 * where 3 is found by trying how to get the whole data range
 * to fit in the time slider
 */

export const getScaleToSecondsPerPxForDataScale = (layers: Layer[]): number => {
  const [dataStartTime, dataEndTime] = getDataLimitsFromLayers(layers);
  const dataRegionInSeconds = dataEndTime - dataStartTime;
  return Math.floor(dataRegionInSeconds / 360); // 60 * 2 * 3, 3 is a hand shake value
};

/**
 * In this Map collection all fundamental scales
 * (Scale.Minutes5 ... Scale.Year) are mapped with their corresponding
 * secondsPerPx numbers. A Scale.DataScale is not included since its
 * secondsPerPx number is different for every data set.
 * @returns a Map including information explained above
 */

export const secondsPerPxToScale: Map<number, Scale> = new Map([
  [2.5, Scale.Minutes5],
  [30, Scale.Hour],
  [3 * 30, Scale.Hours3],
  [6 * 30, Scale.Hours6],
  [24 * 30, Scale.Day],
  [7 * 24 * 30, Scale.Week],
  [30 * 24 * 30, Scale.Month],
  [365 * 24 * 30, Scale.Year],
]);

/**
 * Here secondsPerPxToScale is converted to an iterator object and
 * then keys are turned to values and values turned to keys
 * to make it easier in some parts GeoWeb to fetch a secondsPerpx for
 * a certain fundamental scale (Scale.Minutes5 ... Scale.year)
 * @returns an Object, where names are fundamental scale values (0 - 7) and
 * values are their corresponding secondsPerPx numbers.
 */

export const scaleToSecondsPerPx: {
  [x in Scale]: number;
} = [...secondsPerPxToScale.entries()].reduce(
  (acc, [k, v]) => ({
    ...acc,
    [v]: Number(k),
  }),
  {} as { [x in Scale]: number },
);

/**
 * This function creates and returns an array where there is secondsPerPx
 * number of the data set added for a data scale (dataScaleToSecondsPerPx)
 * to secondsPerPxToScale.
 * @param dataScaleToSecondsPerPx
 * @returns an array containing secondsPerPx numbers.
 * The secondsPerPx number for a data scale is included
 * contrary to secondsPerPxToScale.
 */

export const secondsPerPxValues = (
  dataScaleToSecondsPerPx: number,
): number[] => {
  const sortedSecondsPerPx = [...secondsPerPxToScale].map(
    (value: [number, Scale]) => Number(value[0]),
  );
  const full = [...sortedSecondsPerPx, dataScaleToSecondsPerPx];
  return full;
};

export const defaultDataScaleToSecondsPerPx = 43200;

export const getNewCenterOfFixedPointZoom = (
  fixedTimePoint: number,
  oldSecondsPerPx: number,
  newSecondsPerPx: number,
  oldCenterTime: number,
): number => {
  const centerToFixedPointPx =
    (fixedTimePoint - oldCenterTime) / oldSecondsPerPx;
  return fixedTimePoint - centerToFixedPointPx * newSecondsPerPx;
};

/**
 * Move the time slider such that a given time point (timePoint)
 * is at a given pixel width (timePointPx).
 * @returns a new center time for the resulting position
 */
export const moveRelativeToTimePoint = (
  timePoint: number,
  timePointPx: number,
  secondsPerPx: number,
  canvasWidth: number,
): number => timePoint - secondsPerPx * (timePointPx - canvasWidth / 2);

/** This reusable custom hook tells whether given pointer event targets current canvas node.
 * Example:
 *
 * const [isAllowedCanvasNodePointed, nodeRef] = useCanvasTarget('mousedown', false);
 *  ...
 * return isAllowedCanvasNodePointed ? (<CanvasComponent ref={nodeRef}>Component with ref</CanvasComponent>) : (<CanvasComponent>Component with NO ref</CanvasComponent>);
 *  ...
 */

export const useCanvasTarget = (
  eventType: string,
): [boolean, React.RefObject<CanvasComponent>] => {
  const nodeRef = React.useRef<CanvasComponent>(null);
  const [isTargetNode, setTargetNode] = React.useState(false);

  // Check if pointer event targets current node element
  const pointerEventListener = React.useCallback(
    (e) => {
      setTargetNode(
        nodeRef.current.canvas
          ? nodeRef.current.canvas.isEqualNode(e.target)
          : false,
      );
    },
    [nodeRef],
  );

  React.useEffect(() => {
    document.addEventListener(eventType, pointerEventListener);
    return (): void =>
      document.removeEventListener(eventType, pointerEventListener);
  }, [eventType, pointerEventListener]); // Only add/remove event listener when listener really changes, that is, NOT necessarily between every re-render.

  return [isTargetNode, nodeRef];
};

export const needleGeom = {
  smallWidth: 126,
  largeWidth: 167,
  height: 24,
  cornerRadius: 5,
  lineWidth: 1,
};

export const AUTO_MOVE_AREA_PADDING = 1;
export const getAutoMoveAreaWidth = (scale: Scale): number =>
  (scale === Scale.Year ? needleGeom.largeWidth : needleGeom.smallWidth) / 2 +
  AUTO_MOVE_AREA_PADDING;
