/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  mapActions,
  mapSelectors,
  genericActions,
  genericSelectors,
  uiSelectors,
} from '../../../store';
import { AppStore } from '../../../types/types';
import TimeSliderRail from './TimeSliderRail';
import { Dimension, Layer, Scale } from '../../../store/mapStore/types';
import {
  getDataLimitsFromLayers,
  getMomentTimeBounds,
  MomentTimeBounds,
} from '../TimeSliderUtils';

import { SYNCGROUPS_TYPE_SETTIME } from '../../../store/generic/synchronizationGroups/constants';
import { handleMomentISOString } from '../../../utils/dimensionUtils';

interface TimeSliderRailConnectComponentProps {
  sourceId: string;
  mapId: string;
  dimensions?: Dimension[];
  timeStep?: number;
  layers?: Layer[];
  animationStartTime?: string;
  animationEndTime?: string;
  isAnimating?: boolean;
  centerTime?: number;
  secondsPerPx?: number;
  isTimeSliderHoverOn?: boolean;
  scale?: Scale;
  activeWindowId?: string;
  setAnimationStartTime?: typeof mapActions.setAnimationStartTime;
  setAnimationEndTime?: typeof mapActions.setAnimationEndTime;
  stopMapAnimation?: typeof mapActions.mapStopAnimation;
  setTime?: typeof genericActions.setTime;
  syncGroupAddSource?: typeof genericActions.syncGroupAddSource;
  syncGroupRemoveSource?: typeof genericActions.syncGroupRemoveSource;
  mapSetTimeSliderCenterTime?: typeof mapActions.setTimeSliderCenterTime;
}

const connectRedux = connect(
  (store: AppStore, props: TimeSliderRailConnectComponentProps) => ({
    timeStep: mapSelectors.getMapTimeStep(store, props.mapId),
    layers: mapSelectors.getMapLayers(store, props.mapId),
    dimensions: mapSelectors.getMapDimensions(store, props.mapId),
    animationStartTime: mapSelectors.getAnimationStartTime(store, props.mapId),
    animationEndTime: mapSelectors.getAnimationEndTime(store, props.mapId),
    isAnimating: mapSelectors.isAnimating(store, props.mapId),
    centerTime: mapSelectors.getMapTimeSliderCenterTime(store, props.mapId),
    secondsPerPx: mapSelectors.getMapTimeSliderSecondsPerPx(store, props.mapId),
    isTimeSliderHoverOn: mapSelectors.isTimeSliderHoverOn(store, props.mapId),
    scale: mapSelectors.getMapTimeSliderScale(store, props.mapId),
    timeValue: genericSelectors.getTime(
      genericSelectors.getSynchronizationGroupStore(store),
      props.sourceId,
    ),
    activeWindowId: uiSelectors.getActiveWindowId(store),
  }),
  {
    setAnimationStartTime: mapActions.setAnimationStartTime,
    setAnimationEndTime: mapActions.setAnimationEndTime,
    setTime: genericActions.setTime,
    stopMapAnimation: mapActions.mapStopAnimation,
    syncGroupAddSource: genericActions.syncGroupAddSource,
    syncGroupRemoveSource: genericActions.syncGroupRemoveSource,
    mapSetTimeSliderCenterTime: mapActions.setTimeSliderCenterTime,
  },
);

const TimeSliderRailConnectComponent: React.FC<TimeSliderRailConnectComponentProps> =
  ({
    sourceId,
    mapId,
    dimensions,
    timeStep,
    layers,
    animationStartTime,
    animationEndTime,
    isAnimating,
    centerTime,
    secondsPerPx,
    isTimeSliderHoverOn,
    scale,
    activeWindowId,
    setAnimationStartTime,
    setAnimationEndTime,
    stopMapAnimation,
    setTime,
    syncGroupAddSource,
    syncGroupRemoveSource,
    mapSetTimeSliderCenterTime,
  }: TimeSliderRailConnectComponentProps) => {
    const { selectedTime }: MomentTimeBounds = getMomentTimeBounds(dimensions);
    const currentTime = moment.utc().unix();
    const [dataStartTime, dataEndTime] = getDataLimitsFromLayers(layers);

    const mapIsActive = activeWindowId === mapId;

    React.useEffect(() => {
      syncGroupAddSource({
        id: sourceId,
        type: [SYNCGROUPS_TYPE_SETTIME],
      });
      return (): void => {
        syncGroupRemoveSource({
          id: sourceId,
        });
      };
    }, [sourceId, syncGroupAddSource, syncGroupRemoveSource]);

    return (
      <TimeSliderRail
        centerTime={centerTime}
        secondsPerPx={secondsPerPx}
        timeStep={timeStep}
        selectedTime={selectedTime}
        animationStartTime={animationStartTime}
        animationEndTime={animationEndTime}
        currentTime={currentTime}
        dataStartTime={dataStartTime}
        dataEndTime={dataEndTime}
        scale={scale}
        onSetAnimationStartTime={(animationStartTime: string): void => {
          setAnimationStartTime({
            mapId,
            animationStartTime,
          });
        }}
        onSetAnimationEndTime={(animationEndTime: string): void => {
          setAnimationEndTime({
            mapId,
            animationEndTime,
          });
        }}
        onSetNewDate={(newDate): void => {
          if (isAnimating) {
            stopMapAnimation({ mapId });
          }
          setTime({
            sourceId,
            origin: 'TimeSliderRailConnect, 153',
            value: handleMomentISOString(newDate),
          });
        }}
        isTimeSliderHoverOn={isTimeSliderHoverOn}
        onSetCenterTime={(newTime: number): void => {
          mapSetTimeSliderCenterTime({ mapId, timeSliderCenterTime: newTime });
        }}
        mapIsActive={mapIsActive}
      />
    );
  };

const TimeSliderRailConnect = connectRedux(TimeSliderRailConnectComponent);

export default TimeSliderRailConnect;
