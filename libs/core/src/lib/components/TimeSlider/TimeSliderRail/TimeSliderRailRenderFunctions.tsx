/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import { Theme } from '@mui/material';
import moment from 'moment';
import { Scale } from '../../../store/mapStore/types';

import { needleGeom, timestampToPixel } from '../TimeSliderUtils';

/* eslint-disable no-param-reassign */

const drawRoundedRectangle = (
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  width: number,
  height: number,
  cornerRadius: number,
  theme: Theme,
): void => {
  ctx.save();
  ctx.translate(x, y);

  const { playerTimeMarkers } = theme.palette.geowebColors.timeSlider;

  ctx.beginPath();
  ctx.strokeStyle = playerTimeMarkers.fill;
  ctx.fillStyle = playerTimeMarkers.fill;
  ctx.lineWidth = needleGeom.lineWidth;
  const halfLineWidth = needleGeom.lineWidth / 2;

  // Top left corner
  ctx.moveTo(halfLineWidth, height / 2);
  ctx.arcTo(
    halfLineWidth,
    halfLineWidth,
    width / 2,
    halfLineWidth,
    cornerRadius,
  );

  // Top right corner
  ctx.arcTo(
    width - halfLineWidth,
    halfLineWidth,
    width - halfLineWidth,
    height / 2,
    cornerRadius,
  );

  // Bottom right corner
  ctx.arcTo(
    width - halfLineWidth,
    height - halfLineWidth,
    width / 2,
    height - halfLineWidth,
    cornerRadius,
  );

  // Arrow down
  ctx.lineTo(width / 2 + 5, height);
  ctx.lineTo(width / 2, height + 5);
  ctx.lineTo(width / 2 - 5, height);

  // Bottom left corner
  ctx.arcTo(
    halfLineWidth,
    height - halfLineWidth,
    halfLineWidth,
    height / 2,
    cornerRadius,
  );

  ctx.lineTo(halfLineWidth, height / 2);
  ctx.fill();
  ctx.stroke();

  ctx.restore();
};

const drawTextAnnotation = (
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  text: string,
  theme: Theme,
): void => {
  const { playerTimeText } = theme.palette.geowebColors.timeSlider;
  ctx.save();
  ctx.translate(x, y);

  ctx.fillStyle = playerTimeText.color;
  ctx.font = `${playerTimeText.fontSize}px ${playerTimeText.fontFamily}`;
  const textMetrics = ctx.measureText(text);

  const actualTextWidth =
    textMetrics.actualBoundingBoxRight - textMetrics.actualBoundingBoxLeft;
  const actualTextHeight = ctx.measureText('O').width;
  ctx.fillText(text, -actualTextWidth / 2, actualTextHeight / 2);

  ctx.restore();
};

const drawPlayerLoopIcon = (
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  theme: Theme,
): void => {
  const { playerTimeMarkers } = theme.palette.geowebColors.timeSlider;
  ctx.save();

  const qAndDTranslateXSvg = -12;
  const qAndDTranslateYSvg = 0;
  ctx.translate(x + qAndDTranslateXSvg, y + qAndDTranslateYSvg);

  const svgIconPath = new Path2D(
    'M8.843 1.029c-.56 0-.814.362-.814.699 0 .179.065.36.19.54l3.06 4.34c.065.093.14.171.221.234v8.373c-.082.064-.156.142-.22.234L8.219 19.79c-.125.18-.19.361-.19.54 0 .337.255.699.814.699h6.373c.56 0 .813-.361.813-.7 0-.178-.064-.36-.19-.538l-3.06-4.343c-.08-.112-.174-.204-.278-.273V6.883c.104-.069.199-.16.278-.273l3.06-4.343c.126-.179.19-.36.19-.539 0-.338-.254-.7-.813-.7z',
  );

  ctx.fillStyle = playerTimeMarkers.fill;
  ctx.fill(svgIconPath);

  ctx.strokeStyle =
    theme.palette.geowebColors.timeSlider.playerNeedlePlayer.rgba;
  ctx.lineWidth = 1;
  ctx.stroke(svgIconPath);

  ctx.restore();
};

const drawNewNeedleLabel = (
  ctx: CanvasRenderingContext2D,
  selectedTime: number,
  selectedPx: number,
  animationStartPx: number,
  animationEndPx: number,
  scale: Scale,
  theme: Theme,
): void => {
  const { smallWidth, largeWidth, height } = needleGeom;
  const needleWidth = scale === Scale.Year ? largeWidth : smallWidth;
  const needleX = selectedPx - needleWidth / 2;
  const needleY = needleGeom.lineWidth;

  drawRoundedRectangle(
    ctx,
    needleX,
    needleY,
    needleWidth,
    height,
    needleGeom.cornerRadius,
    theme,
  );

  const timeFormat =
    scale === Scale.Year ? 'Y ddd DD MMM HH:mm' : 'ddd DD MMM HH:mm';
  const timeText = moment
    .utc(selectedTime * 1000)
    .format(timeFormat)
    .toString();

  drawTextAnnotation(
    ctx,
    needleX + needleWidth / 2,
    needleY + height / 2,
    timeText,
    theme,
  );

  if (!animationStartPx || !animationEndPx) {
    drawPlayerLoopIcon(
      ctx,
      selectedPx,
      needleY + height - needleGeom.lineWidth,
      theme,
    );
    return;
  }

  drawPlayerLoopIcon(
    ctx,
    animationStartPx,
    needleY + height - needleGeom.lineWidth,
    theme,
  );

  drawPlayerLoopIcon(
    ctx,
    animationEndPx,
    needleY + height - needleGeom.lineWidth,
    theme,
  );
};

// top spacing
const y = 31;

const drawRoundedRailMask = (
  ctx: CanvasRenderingContext2D,
  width: number,
  height: number,
): void => {
  const r = 4;

  ctx.beginPath();
  ctx.moveTo(r, y);
  ctx.lineTo(width - r, y);
  // top right corner
  ctx.quadraticCurveTo(width, y, width, r + y);
  ctx.lineTo(width, height - r + y);
  // bottom right corner
  ctx.quadraticCurveTo(width, height + y, width - r, height + y);
  ctx.lineTo(r, height + y);
  // bottom left corner
  ctx.quadraticCurveTo(0, height + y, 0, height - r + y);
  ctx.lineTo(0, r + y);
  // top left corner
  ctx.quadraticCurveTo(0, y, r, y);
  ctx.closePath();
  // the mask color is not visible but needs to be completely opaque
  ctx.fillStyle = '#ffffff';
  ctx.fill();
};

const drawSelectedTimeLine = (
  ctx: CanvasRenderingContext2D,
  selectedPx: number,
  railHeight: number,
  theme: Theme,
): void => {
  const { playerNeedlePlayer, playerNeedlePlayerTop } =
    theme.palette.geowebColors.timeSlider;
  ctx.beginPath();
  // Move 1 pixel line by 0.5 pixels to prevent it from being shown as blurry: https://usefulangle.com/post/17/html5-canvas-drawing-1px-crisp-straight-lines
  ctx.moveTo(Math.floor(selectedPx) + 0.5, y);
  ctx.lineTo(Math.floor(selectedPx) + 0.5, y + railHeight);
  ctx.lineWidth = 3;
  ctx.strokeStyle = playerNeedlePlayer.rgba;
  ctx.stroke();
  ctx.lineWidth = 1;
  ctx.strokeStyle = playerNeedlePlayerTop.rgba;
  ctx.stroke();
};

const drawCurrentTimeLine = (
  ctx: CanvasRenderingContext2D,
  currentTimePx: number,
  railHeight: number,
  theme: Theme,
): void => {
  const { playerNeedleTime, playerNeedleTimeTop } =
    theme.palette.geowebColors.timeSlider;
  ctx.beginPath();
  // Move 1 pixel line by 0.5 pixels to prevent it from being shown as blurry: https://usefulangle.com/post/17/html5-canvas-drawing-1px-crisp-straight-lines
  ctx.moveTo(Math.floor(currentTimePx) + 0.5, y);
  ctx.lineTo(Math.floor(currentTimePx) + 0.5, y + railHeight);
  ctx.lineWidth = 3;
  ctx.strokeStyle = playerNeedleTime.rgba;
  ctx.stroke();
  ctx.lineWidth = 1;
  ctx.strokeStyle = playerNeedleTimeTop.rgba;
  ctx.stroke();
};

const drawRailColors = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  width: number,
  railHeight: number,
  selectedPx: number,
  currentTimePx: number,
  animationStartPx?: number,
  animationEndPx?: number,
  dataStartPx?: number,
  dataEndPx?: number,
): void => {
  const {
    playerRailSelection,
    playerRailForecastNoData,
    playerRailObservation,
    playerRailForecastData,
  } = theme.palette.geowebColors.timeSlider;

  ctx.fillStyle = playerRailForecastNoData.rgba;
  ctx.fillRect(0, y, width, railHeight);
  if (dataStartPx < currentTimePx) {
    ctx.fillStyle = playerRailObservation.rgba;
    ctx.fillRect(
      Math.max(dataStartPx, 0),
      y,
      Math.min(currentTimePx, dataEndPx) - Math.max(dataStartPx, 0),
      railHeight,
    );
  }
  if (dataEndPx > currentTimePx) {
    ctx.fillStyle = playerRailForecastData.rgba;
    ctx.fillRect(
      Math.max(currentTimePx, dataStartPx),
      y,
      Math.min(width, dataEndPx) - Math.max(currentTimePx, dataStartPx),
      railHeight,
    );
  }
  if (animationStartPx && animationEndPx) {
    const start = Math.max(animationStartPx, dataStartPx);
    const end = Math.min(animationEndPx, dataEndPx);
    ctx.fillStyle = playerRailSelection.rgba;
    ctx.fillRect(
      start,
      y,
      Math.max(end - start, 0), // no negative widths
      railHeight,
    );
  }

  drawSelectedTimeLine(ctx, selectedPx, railHeight, theme);
  drawCurrentTimeLine(ctx, currentTimePx, railHeight, theme);
};

export const renderTimeSliderRailWithNeedle = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  width: number,
  height: number,
  centerTime: number,
  selected: number,
  secondsPerPx: number,
  scale: Scale,
  animationStart?: number,
  animationEnd?: number,
  dataStartTime?: number,
  dataEndTime?: number,
  currentTime?: number,
): void => {
  // map unix timestamps to pixel values
  const [
    selectedPx,
    animationStartPx,
    animationEndPx,
    currentTimePx,
    dataStartPx,
    dataEndPx,
  ] = [
    selected,
    animationStart,
    animationEnd,
    currentTime,
    dataStartTime,
    dataEndTime,
  ].map((t) => timestampToPixel(t, centerTime, width, secondsPerPx));
  ctx.clearRect(0, 0, width, height);

  drawRailColors(
    ctx,
    theme,
    width,
    8,
    selectedPx,
    currentTimePx,
    animationStartPx,
    animationEndPx,
    dataStartPx,
    dataEndPx,
  );
  // destination-in only leaves those areas visible that overlap with the mask
  ctx.globalCompositeOperation = 'destination-in';
  drawRoundedRailMask(ctx, width, 8);
  // source-over is the default
  ctx.globalCompositeOperation = 'source-over';
  drawNewNeedleLabel(
    ctx,
    selected,
    selectedPx,
    animationStartPx,
    animationEndPx,
    scale,
    theme,
  );
};
