/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';
import { darkTheme, lightTheme, ThemeWrapper } from '@opengeoweb/theme';

import TimeSliderRail, { TimeSliderRailProps } from './TimeSliderRail';
import { Scale } from '../../../store/mapStore/types';
import { dateFormat } from '../../../store/mapStore/utils/helpers';

export default { title: 'components/TimeSlider/TimeSliderRail' };

const TimeSliderRailDisplay = (): React.ReactElement => {
  const t = moment.utc('2021-06-23');
  const simpleProps: TimeSliderRailProps = {
    selectedTime: moment.utc(t).startOf('day').hours(12),
    timeStep: 5,
    centerTime: moment.utc(t).startOf('day').hours(12).unix(),
    secondsPerPx: 120,
    mapIsActive: true,
  };
  const day = moment.utc(t).startOf('day');
  const propsWithAnimationTimes: TimeSliderRailProps = {
    ...simpleProps,
    animationStartTime: moment.utc(day).hours(10).format(dateFormat),
    animationEndTime: moment.utc(day).hours(14).format(dateFormat),
    currentTime: moment.utc(day).hours(13).unix(),
    dataStartTime: moment.utc(day).hours(2).unix(),
    dataEndTime: moment.utc(day).hours(22).unix(),
  };
  return (
    <div>
      <TimeSliderRail {...simpleProps} />
      <TimeSliderRail {...simpleProps} scale={Scale.Year} />

      <TimeSliderRail
        {...propsWithAnimationTimes}
        selectedTime={moment.utc(day).hours(6)}
      />
      <TimeSliderRail {...propsWithAnimationTimes} />
      <TimeSliderRail
        {...propsWithAnimationTimes}
        selectedTime={moment.utc(day).hours(18)}
      />
      <TimeSliderRail
        {...propsWithAnimationTimes}
        animationStartTime={moment.utc(day).hours(7).format(dateFormat)}
        animationEndTime={moment.utc(day).hours(11).format(dateFormat)}
      />
      <TimeSliderRail
        {...propsWithAnimationTimes}
        animationStartTime={moment.utc(day).hours(14).format(dateFormat)}
        animationEndTime={moment.utc(day).hours(18).format(dateFormat)}
      />
      <TimeSliderRail
        {...propsWithAnimationTimes}
        animationStartTime={moment.utc(day).hours(0).format(dateFormat)}
        animationEndTime={moment.utc(day).hours(24).format(dateFormat)}
      />
      <TimeSliderRail
        {...propsWithAnimationTimes}
        animationStartTime={moment
          .utc(day)
          .subtract(1, 'day')
          .hours(20)
          .format(dateFormat)}
        animationEndTime={moment.utc(day).hours(1).format(dateFormat)}
      />
      <TimeSliderRail
        {...propsWithAnimationTimes}
        animationStartTime={moment.utc(day).hours(23).format(dateFormat)}
        animationEndTime={moment
          .utc(day)
          .add(1, 'day')
          .hours(4)
          .format(dateFormat)}
      />
    </div>
  );
};

export const TimeSliderRailDisplayLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <TimeSliderRailDisplay />
  </ThemeWrapper>
);

export const TimeSliderRailDisplayDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <div style={{ background: '#FFFFFF' }}>
      <TimeSliderRailDisplay />
    </div>
  </ThemeWrapper>
);

TimeSliderRailDisplayLight.storyName =
  'Time Slider Rail Light Theme (takeSnapshot)';
TimeSliderRailDisplayDark.storyName =
  'Time Slider Rail Dark Theme (takeSnapshot)';
