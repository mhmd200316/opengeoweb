/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';
import { useTheme } from '@mui/material';

import makeStyles from '@mui/styles/makeStyles';

import CanvasComponent from '../../CanvasComponent/CanvasComponent';
import { renderTimeSliderRailWithNeedle } from './TimeSliderRailRenderFunctions';
import {
  onsetNewDateDebounced,
  timestampToPixel,
  pixelToTimestamp,
  setNewRoundedTime,
  useCanvasTarget,
  moveRelativeToTimePoint,
  getAutoMoveAreaWidth,
} from '../TimeSliderUtils';
import { Scale } from '../../../store/mapStore/types';
import { dateFormat } from '../../../store/mapStore/utils/helpers';

const useStyles = makeStyles({
  timeSliderRail: {
    '& div': {
      outline: 'none',
    },
    height: '45px',
    '& canvas': {
      display: 'block',
    },
  },
});

export interface TimeSliderRailProps {
  centerTime: number;
  secondsPerPx: number;
  selectedTime?: moment.Moment;
  timeStep?: number;
  animationStartTime?: string;
  animationEndTime?: string;
  currentTime?: number;
  dataStartTime?: number;
  dataEndTime?: number;
  isTimeSliderHoverOn?: boolean;
  scale?: Scale;
  mapIsActive: boolean;
  onSetAnimationStartTime?: (time: string) => void;
  onSetAnimationEndTime?: (time: string) => void;
  onSetNewDate?: (newDate: string) => void;
  onSetCenterTime?: (newTime: number) => void;
}

const setPreviousTimeStep = (
  timeStep,
  curTime,
  dataStartTime,
  onSetNewDate,
): void => {
  const prevTimeStep = moment
    .utc(curTime)
    .subtract(timeStep, 'm')
    .toISOString();
  if (!prevTimeStep) return;
  if (moment.utc(prevTimeStep) < moment.utc(dataStartTime * 1000)) return;
  onsetNewDateDebounced(prevTimeStep, onSetNewDate);
};

const setNextTimeStep = (
  timeStep,
  curTime,
  dataEndTime,
  onSetNewDate,
): void => {
  const nextTimeStep = moment.utc(curTime).add(timeStep, 'm').toISOString();
  if (!nextTimeStep) return;
  if (moment.utc(nextTimeStep) > moment.utc(dataEndTime * 1000)) return;
  onsetNewDateDebounced(nextTimeStep, onSetNewDate);
};

const DRAG_AREA_WIDTH = 10;
const DRAG_HANDLER_AREA_WIDTH = 30;

const TimeSliderRail: React.FC<TimeSliderRailProps> = ({
  centerTime = moment.utc().unix(),
  secondsPerPx = 50,
  selectedTime,
  timeStep,
  animationStartTime,
  animationEndTime,
  currentTime,
  dataStartTime,
  dataEndTime,
  isTimeSliderHoverOn,
  scale = Scale.Hour,
  mapIsActive,
  onSetAnimationStartTime,
  onSetAnimationEndTime,
  onSetNewDate,
  onSetCenterTime,
}: TimeSliderRailProps) => {
  const classes = useStyles();
  const ZOOM_RATIO = 1.1;
  const zoomOut = (relativeX): void => {
    const su = moment.utc(animationStartTime).unix();
    const eu = moment.utc(animationEndTime).unix();
    const targetTime = su + (eu - su) * relativeX;
    onSetAnimationStartTime(
      moment
        .utc(((su - targetTime) * ZOOM_RATIO + targetTime) * 1000)
        .format(dateFormat),
    );
    onSetAnimationEndTime(
      moment
        .utc(((eu - targetTime) * ZOOM_RATIO + targetTime) * 1000)
        .format(dateFormat),
    );
  };

  const zoomIn = (relativeX): void => {
    const su = moment.utc(animationStartTime).unix();
    const eu = moment.utc(animationEndTime).unix();
    const targetTime = su + (eu - su) * relativeX;
    onSetAnimationStartTime(
      moment
        .utc(((su - targetTime) / ZOOM_RATIO + targetTime) * 1000)
        .format(dateFormat),
    );
    onSetAnimationEndTime(
      moment
        .utc(((eu - targetTime) / ZOOM_RATIO + targetTime) * 1000)
        .format(dateFormat),
    );
  };

  const curTime =
    selectedTime !== undefined
      ? selectedTime.toISOString()
      : moment.utc().toISOString();

  const [localAnimationStartTime, setLocalAnimationStartTime] = React.useState(
    animationStartTime && moment.utc(animationStartTime).unix(),
  );
  const [localAnimationEndTime, setLocalAnimationEndTime] = React.useState(
    animationEndTime && moment.utc(animationEndTime).unix(),
  );
  const [isLeftMarkerDragging, setLeftMarkerDragging] = React.useState(false);
  const [isRightMarkerDragging, setRightMarkerDragging] = React.useState(false);
  const [cursorStyle, setCursorStyle] = React.useState('auto');
  const [isAnimationDragged, setAnimationDragged] = React.useState(false);

  const onWheel = ({ event, deltaY, canvasWidth, mouseX }): void => {
    if (event.altKey) {
      const relativeX = mouseX / canvasWidth;
      if (deltaY > 0) {
        zoomIn(relativeX);
      } else {
        zoomOut(relativeX);
      }
    } else if (deltaY > 0) {
      setPreviousTimeStep(timeStep, curTime, dataStartTime, onSetNewDate);
    } else {
      setNextTimeStep(timeStep, curTime, dataEndTime, onSetNewDate);
    }
  };

  const onMouseDownTouchStartActions = (
    x: number,
    y: number,
    width: number,
  ): void => {
    if (
      !animationStartTime ||
      !animationEndTime ||
      !onSetAnimationStartTime ||
      !onSetAnimationEndTime
    )
      return;

    const [leftMarkerPx, rightMarkerPx] = [
      localAnimationStartTime,
      localAnimationEndTime,
    ].map((timestamp) =>
      timestampToPixel(timestamp, centerTime, width, secondsPerPx),
    );

    // start dragging either marker
    if (
      x > leftMarkerPx - DRAG_HANDLER_AREA_WIDTH &&
      x < leftMarkerPx + DRAG_HANDLER_AREA_WIDTH &&
      !isRightMarkerDragging
    ) {
      setLeftMarkerDragging(true);
    }
    if (
      x > rightMarkerPx - DRAG_HANDLER_AREA_WIDTH &&
      x < rightMarkerPx + DRAG_HANDLER_AREA_WIDTH &&
      !isLeftMarkerDragging
    ) {
      setRightMarkerDragging(true);
    }
  };

  const onMouseDown = (x: number, y: number, width: number): void => {
    onMouseDownTouchStartActions(x, y, width);
  };

  const onTouchStart = (event, width: number): void => {
    const x = event.touches[0].clientX - event.touches[0].target.offsetLeft;
    const y = event.touches[0].clientY;
    event.preventDefault();
    onMouseDownTouchStartActions(x, y, width);
  };

  const onMouseUpTouchEnd = (): void => {
    // Dispatch changes when releasing mouse
    if (isLeftMarkerDragging) {
      onSetAnimationStartTime(
        moment.utc(localAnimationStartTime * 1000).format(dateFormat),
      );
      setAnimationDragged(true);
    }
    if (isRightMarkerDragging) {
      onSetAnimationEndTime(
        moment.utc(localAnimationEndTime * 1000).format(dateFormat),
      );
      setAnimationDragged(true);
    }
    setLeftMarkerDragging(false);
    setRightMarkerDragging(false);
  };

  const onMouseTouchMoveActions = (x, y, width): void => {
    if (!localAnimationStartTime || !localAnimationEndTime) return;

    // change local time bounds according to either dragged marker
    const [mouseTimeUnix, startTimeUnix, endTimeUnix] = [x, 0, width].map((t) =>
      pixelToTimestamp(t, centerTime, width, secondsPerPx),
    );
    if (isLeftMarkerDragging) {
      // Prevent dragging if it would cause markers to cross over, except if the markers
      // are moved away from being crossed over
      if (
        mouseTimeUnix >= localAnimationEndTime ||
        mouseTimeUnix <= startTimeUnix
      )
        return;
      setLocalAnimationStartTime(mouseTimeUnix);
      return;
    }
    if (isRightMarkerDragging) {
      // Similar condition for other marker
      if (
        mouseTimeUnix <= localAnimationStartTime ||
        mouseTimeUnix >= endTimeUnix
      )
        return;
      setLocalAnimationEndTime(mouseTimeUnix);
    }
  };

  const onMouseMove = (x, y, event, width): void => {
    const [leftMarkerPx, rightMarkerPx] = [
      localAnimationStartTime,
      localAnimationEndTime,
    ].map((timestamp) =>
      timestampToPixel(timestamp, centerTime, width, secondsPerPx),
    );
    // Adjust mouse cursor while hovering on either marker,
    // or while dragging (even when the cursor is not above a marker)
    if (
      (x > leftMarkerPx - DRAG_AREA_WIDTH &&
        x < leftMarkerPx + DRAG_AREA_WIDTH) ||
      (x > rightMarkerPx - DRAG_AREA_WIDTH &&
        x < rightMarkerPx + DRAG_AREA_WIDTH) ||
      isLeftMarkerDragging ||
      isRightMarkerDragging
    ) {
      setCursorStyle('ew-resize');
    } else {
      setCursorStyle('auto');
    }
    const isDragging = isLeftMarkerDragging || isRightMarkerDragging;
    if (isTimeSliderHoverOn && !isDragging) {
      setNewRoundedTime(
        x,
        centerTime,
        width,
        secondsPerPx,
        timeStep,
        dataStartTime,
        dataEndTime,
        onSetNewDate,
      );
    }
  };

  React.useEffect(() => {
    setLocalAnimationStartTime(
      animationStartTime && moment.utc(animationStartTime).unix(),
    );
    setLocalAnimationEndTime(
      animationEndTime && moment.utc(animationEndTime).unix(),
    );
  }, [animationStartTime, animationEndTime]);

  const handleKeyDown = React.useCallback(
    (event): void => {
      if (event.ctrlKey && dataStartTime && dataEndTime && mapIsActive) {
        switch (event.code) {
          case 'ArrowLeft':
            setPreviousTimeStep(timeStep, curTime, dataStartTime, onSetNewDate);
            break;
          case 'ArrowRight':
            setNextTimeStep(timeStep, curTime, dataEndTime, onSetNewDate);
            break;
          default:
        }
      }
    },
    [curTime, onSetNewDate, timeStep, dataStartTime, dataEndTime, mapIsActive],
  );

  React.useEffect(() => {
    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [handleKeyDown]); // Only add/remove event listener when listener changes, that is, NOT necessarily between every re-render.

  const [isAllowedCanvasNodePointed, node] = useCanvasTarget('mousedown');

  const [width, setWidth] = React.useState(-1);

  // Move the time slider if selected time has moved too close to the edge
  const selectedTimeUnix = selectedTime.unix();
  const autoMoveAreaWidth = getAutoMoveAreaWidth(scale);

  React.useLayoutEffect(() => {
    if (width === -1) return;
    const selectedTimePx = timestampToPixel(
      selectedTimeUnix,
      centerTime,
      width,
      secondsPerPx,
    );
    if (selectedTimePx <= autoMoveAreaWidth) {
      onSetCenterTime(
        moveRelativeToTimePoint(
          selectedTimeUnix,
          autoMoveAreaWidth,
          secondsPerPx,
          width,
        ),
      );
    }
    if (width - selectedTimePx <= autoMoveAreaWidth) {
      onSetCenterTime(
        moveRelativeToTimePoint(
          selectedTimeUnix,
          width - autoMoveAreaWidth,
          secondsPerPx,
          width,
        ),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedTimeUnix]);

  const theme = useTheme();

  return (
    <div
      className={classes.timeSliderRail}
      style={{ cursor: cursorStyle }}
      data-testid="timeSliderRail"
    >
      <CanvasComponent
        ref={node}
        onMouseDown={onMouseDown}
        onMouseMove={onMouseMove}
        onMouseUp={onMouseUpTouchEnd}
        onTouchStart={onTouchStart}
        onTouchEnd={onMouseUpTouchEnd}
        onWheel={onWheel}
        resizeCallback={(newWidth): void => setWidth(newWidth)}
        onCanvasClick={(x, y, width): void => {
          const isCanvasArea = x >= 0 && x < width;
          const isDragging = isLeftMarkerDragging || isRightMarkerDragging;
          if (isCanvasArea) {
            if (!isDragging) {
              const [leftMarkerPx, rightMarkerPx] = [
                localAnimationStartTime,
                localAnimationEndTime,
              ].map((timestamp) =>
                timestampToPixel(timestamp, centerTime, width, secondsPerPx),
              );
              const isAnimationDraggingArea =
                (x > leftMarkerPx - DRAG_AREA_WIDTH &&
                  x < leftMarkerPx + DRAG_AREA_WIDTH) ||
                (x > rightMarkerPx - DRAG_AREA_WIDTH &&
                  x < rightMarkerPx + DRAG_AREA_WIDTH);
              if (
                !isAnimationDraggingArea &&
                !isAnimationDragged &&
                isAllowedCanvasNodePointed
              ) {
                setNewRoundedTime(
                  x,
                  centerTime,
                  width,
                  secondsPerPx,
                  timeStep,
                  dataStartTime,
                  dataEndTime,
                  onSetNewDate,
                );
              }
              setAnimationDragged(false);
            }

            if (isDragging) {
              onMouseTouchMoveActions(x, y, width);
            }
          }
        }}
        onRenderCanvas={(
          ctx: CanvasRenderingContext2D,
          width: number,
          height: number,
        ): void => {
          renderTimeSliderRailWithNeedle(
            ctx,
            theme,
            width,
            height,
            centerTime,
            selectedTime.unix(),
            secondsPerPx,
            scale,
            localAnimationStartTime,
            localAnimationEndTime,
            dataStartTime,
            dataEndTime,
            currentTime,
          );
        }}
      />
    </div>
  );
};

export default TimeSliderRail;
