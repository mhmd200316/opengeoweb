/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import TimeSliderRailConnect from './TimeSliderRailConnect';
import {
  mockStateMapWithDimensions,
  mockStateMapWithLayer,
} from '../../../utils/testUtils';
import {
  defaultReduxLayerRadarKNMI,
  layerWithoutTimeDimension,
} from '../../../utils/defaultTestSettings';
import { CoreThemeStoreProvider } from '../../Providers/Providers';

describe('src/components/TimeSlider/TimeSliderRail/TimeSliderRailConnect', () => {
  it('should render the component when the map has a time dimension', () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithDimensions(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { container } = render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderRailConnect mapId={mapId} sourceId="timeSlider_A" />
      </CoreThemeStoreProvider>,
    );

    expect(container.querySelector('canvas')).toBeTruthy();
  });

  it('should render the component when the map does not have a time dimension', () => {
    const mapId = 'mapid_1';
    const layer = layerWithoutTimeDimension;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { container } = render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderRailConnect mapId={mapId} sourceId="timeSlider_A" />
      </CoreThemeStoreProvider>,
    );

    expect(container.querySelector('canvas')).toBeTruthy();
  });
});
