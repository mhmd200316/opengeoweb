/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import moment from 'moment';

import TimeSliderRail, { TimeSliderRailProps } from './TimeSliderRail';
import { Scale } from '../../../store/mapStore/types';
import { CoreThemeProvider } from '../../Providers/Providers';
import { dateFormat } from '../../../store/mapStore/utils/helpers';

const date = moment.utc('20200101', 'YYYYMMDD');
const props: TimeSliderRailProps = {
  scale: Scale.Hour,
  timeStep: 5,
  dataStartTime: moment.utc('20100101', 'YYYYMMDD').unix(),
  dataEndTime: moment.utc('20300101', 'YYYYMMDD').unix(),
  centerTime: moment.utc(date).startOf('day').hours(12).unix(),
  secondsPerPx: 50,
  onSetNewDate: jest.fn(),
  onSetAnimationStartTime: jest.fn(),
  onSetAnimationEndTime: jest.fn(),
  onSetCenterTime: jest.fn(),
  mapIsActive: true,
};
const startTime = '2020-01-01T09:58:00Z';
const endTime = '2020-01-01T14:02:00Z';

describe('src/components/TimeSlider/TimeSliderRail', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should render a canvas with start, end and selected time props', () => {
    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderRail
          {...props}
          animationStartTime={startTime}
          animationEndTime={endTime}
          selectedTime={moment.utc(date).startOf('day').hours(12)}
        />
      </CoreThemeProvider>,
    );
    expect(container.querySelector('canvas')).toBeTruthy();
  });

  it('should render a canvas with animation start and end times added to props', () => {
    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderRail
          {...props}
          animationStartTime="2020-01-01T10:00:00Z"
          animationEndTime="2020-01-01T14:00:00Z"
          selectedTime={moment.utc(date).startOf('day').hours(12)}
        />
      </CoreThemeProvider>,
    );
    expect(container.querySelector('canvas')).toBeTruthy();
  });
  it(
    'rail slider value can be changed with a scroll with deltaY > 0 ' +
      'and change animation interval with alt key',
    async () => {
      const date1 = moment.utc().subtract(6, 'h');
      const wheeledDate = date1.clone().subtract(5, 'm').toISOString();

      const { container } = render(
        <CoreThemeProvider>
          <TimeSliderRail
            {...props}
            selectedTime={moment.utc(date1)}
            animationStartTime={startTime}
            animationEndTime={endTime}
          />
        </CoreThemeProvider>,
      );
      expect(container).toBeTruthy();
      const canvas = container.querySelector('canvas');
      canvas.focus();
      fireEvent.wheel(canvas, { deltaY: 1 });
      // run timers set by wheel to avoid side effects to the next test
      jest.runOnlyPendingTimers();

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props.onSetNewDate).toHaveBeenCalledWith(wheeledDate);

      fireEvent.wheel(canvas, {
        altKey: true,
        deltaY: 1,
      });
      expect(props.onSetAnimationStartTime).toHaveBeenCalledTimes(1);
      expect(props.onSetAnimationEndTime).toHaveBeenCalledTimes(1);
    },
  );
  it(
    'railslider value can be changed with a scroll and with deltaY < 0  ' +
      'and change animation interval with alt key',
    async () => {
      const date2 = moment.utc();
      const wheeledDate = date2.clone().add(5, 'm').toISOString();

      const { container } = render(
        <CoreThemeProvider>
          <TimeSliderRail
            {...props}
            selectedTime={moment.utc(date2)}
            animationStartTime={startTime}
            animationEndTime={endTime}
          />
        </CoreThemeProvider>,
      );
      expect(container).toBeTruthy();
      const canvas = container.querySelector('canvas');
      canvas.focus();

      fireEvent.wheel(canvas, { deltaY: -1 });

      // run timers set by wheel to avoid side effects to the next test
      jest.runOnlyPendingTimers();

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props.onSetNewDate).toHaveBeenCalledWith(wheeledDate);

      fireEvent.wheel(canvas, { altKey: true, deltaY: -1 });
      expect(props.onSetAnimationStartTime).toHaveBeenCalledTimes(1);
      expect(props.onSetAnimationEndTime).toHaveBeenCalledTimes(1);
    },
  );
  describe('change rail slider value with keyboard', () => {
    function changeRailSliderValueWithKeyboard(
      date: moment.Moment,
      arrowKeyDirection: 'left' | 'right',
      mapIsActive: boolean,
    ): void {
      const { container, queryByRole } = render(
        <CoreThemeProvider>
          <TimeSliderRail
            {...props}
            selectedTime={moment.utc(date)}
            animationStartTime={moment
              .utc(date)
              .startOf('day')
              .hours(10)
              .format(dateFormat)}
            animationEndTime={moment
              .utc(date)
              .startOf('day')
              .hours(14)
              .format(dateFormat)}
            mapIsActive={mapIsActive}
          />
        </CoreThemeProvider>,
      );
      expect(container).toBeTruthy();
      const button = queryByRole('button');
      button.focus();
      const canvas = container.querySelector('canvas');
      canvas.focus();
      fireEvent.keyDown(
        canvas,
        arrowKeyDirection === 'left'
          ? {
              ctrlKey: true,
              key: 'ArrowLeft',
              code: 'ArrowLeft',
              keyCode: 37,
            }
          : {
              ctrlKey: true,
              key: 'ArrowRight',
              code: 'ArrowRight',
              keyCode: 39,
            },
      );
      jest.runOnlyPendingTimers();
    }

    it('rail slider value can be changed with an right arrow key and a ctrl key if map is active', async () => {
      const date3 = moment.utc().subtract(6, 'h');
      const wheeledDate = date3.clone().add(5, 'm').toISOString();

      changeRailSliderValueWithKeyboard(date3, 'right', true);

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props.onSetNewDate).toHaveBeenCalledWith(wheeledDate);
    });
    it('rail slider value can not be changed with an right arrow key and a ctrl key if map is inactive', async () => {
      const date3 = moment.utc().subtract(6, 'h');

      changeRailSliderValueWithKeyboard(date3, 'right', false);

      expect(props.onSetNewDate).not.toBeCalled();
    });
    it('rail slider value can be changed with an left arrow key and a ctrl key if map is active', async () => {
      const date4 = moment.utc().subtract(6, 'h');
      const wheeledDate = date4.clone().subtract(5, 'm').toISOString();

      changeRailSliderValueWithKeyboard(date4, 'left', true);

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props.onSetNewDate).toHaveBeenCalledWith(wheeledDate);
    });

    it('rail slider value can not be changed with an left arrow key and a ctrl key if map is inactive', async () => {
      const date4 = moment.utc().subtract(6, 'h');

      changeRailSliderValueWithKeyboard(date4, 'left', false);

      expect(props.onSetNewDate).not.toBeCalled();
    });
  });
  it('should follow selected time', () => {
    const { container, rerender } = render(
      <CoreThemeProvider>
        <TimeSliderRail
          {...props}
          selectedTime={moment.unix(props.centerTime).utc()}
        />
      </CoreThemeProvider>,
    );
    const canvasContainerDiv = container.getElementsByTagName('div')[2];
    jest.spyOn(canvasContainerDiv, 'clientWidth', 'get').mockReturnValue(1000);
    fireEvent(window, new Event('resize'));
    expect(props.onSetCenterTime).toHaveBeenCalledTimes(0);
    // move selection inside visible area - rail should stay in place
    rerender(
      <CoreThemeProvider>
        <TimeSliderRail
          {...props}
          selectedTime={moment.unix(props.centerTime).utc().add(10, 'minutes')}
        />
      </CoreThemeProvider>,
    );
    expect(props.onSetCenterTime).toHaveBeenCalledTimes(0);
    // move selection outside visible area - rail should follow the selection
    rerender(
      <CoreThemeProvider>
        <TimeSliderRail
          {...props}
          selectedTime={moment.unix(props.centerTime).utc().add(10, 'hours')}
        />
      </CoreThemeProvider>,
    );
    expect(props.onSetCenterTime).toHaveBeenCalledTimes(1);
  });
});
