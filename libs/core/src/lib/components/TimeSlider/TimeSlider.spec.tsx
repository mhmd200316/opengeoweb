/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import configureStore from 'redux-mock-store';
import TimeSlider from './TimeSlider';

import { mockStateMapWithAnimationDelayWithoutLayers } from '../../utils/testUtils';
import {
  CoreThemeProvider,
  CoreThemeStoreProvider,
} from '../Providers/Providers';

describe('src/components/TimeSlider/TimeSlider', () => {
  it('should render the wrapped components', () => {
    const props = {};
    const { getByTestId } = render(
      <CoreThemeProvider>
        <TimeSlider {...props} />
      </CoreThemeProvider>,
    );
    expect(getByTestId('timeSliderButtons')).toBeTruthy();
    expect(getByTestId('timeSliderRail')).toBeTruthy();
    expect(getByTestId('scaleSlider')).toBeTruthy();
    expect(getByTestId('timeSliderLegend')).toBeTruthy();
  });

  it('should hide the timeslider', () => {
    const props = {
      isVisible: false,
      onToggleTimeSlider: jest.fn(),
    };
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <TimeSlider {...props} />
      </CoreThemeProvider>,
    );
    expect(queryByTestId('timeSliderButtons')).toBeFalsy();
    expect(queryByTestId('timeSliderRail')).toBeFalsy();
    expect(queryByTestId('scaleSlider')).toBeFalsy();
    expect(queryByTestId('timeSliderLegend')).toBeFalsy();

    expect(props.onToggleTimeSlider).not.toHaveBeenCalled();
    fireEvent.keyDown(document, {
      key: 't',
      code: 'KeyT',
    });
    expect(props.onToggleTimeSlider).toHaveBeenCalledWith(true);
  });

  it('should be able to hide the timeslider with t key when map is active', async () => {
    const props = {
      onToggleTimeSlider: jest.fn(),
    };
    const { container, queryByTestId } = render(
      <CoreThemeProvider>
        <TimeSlider {...props} />
      </CoreThemeProvider>,
    );
    expect(queryByTestId('timeSlider')).toBeTruthy();
    expect(props.onToggleTimeSlider).not.toHaveBeenCalled();
    fireEvent.keyDown(container, {
      key: 't',
      code: 'KeyT',
    });
    expect(props.onToggleTimeSlider).toHaveBeenCalledWith(false);
  });

  it('should be able to show the timeslider with t key when map is active', async () => {
    const props = {
      onToggleTimeSlider: jest.fn(),
      isVisible: false,
    };
    const { container, queryByTestId } = render(
      <CoreThemeProvider>
        <TimeSlider {...props} />
      </CoreThemeProvider>,
    );
    expect(queryByTestId('timeSlider')).toBeNull();
    expect(props.onToggleTimeSlider).not.toHaveBeenCalled();
    fireEvent.keyDown(container, {
      key: 't',
      code: 'KeyT',
    });
    expect(props.onToggleTimeSlider).toHaveBeenCalledWith(true);
  });
  it('timeslider cannot be removed and fetched back with t key when map is inactive', async () => {
    const props = { mapIsActive: false };
    const { container, queryByTestId } = render(
      <CoreThemeProvider>
        <TimeSlider {...props} />
      </CoreThemeProvider>,
    );
    expect(queryByTestId('timeSlider')).toBeTruthy();
    fireEvent.keyDown(container, {
      key: 't',
      code: 'KeyT',
    });
    expect(queryByTestId('timeSlider')).toBeTruthy();
  });
  it(
    'timeslider cannot be removed and fetched back with t key in an input field,' +
      'but can be done elsewhere',
    async () => {
      const props = {
        mapId: 'mapId_1',
        onToggleTimeSlider: jest.fn(),
      };
      const props2 = {
        onToggleTimeSlider: jest.fn(),
      };
      const mockStore = configureStore();
      const mockState = mockStateMapWithAnimationDelayWithoutLayers(
        props.mapId,
      );
      const store = mockStore(mockState);
      store.addModules = jest.fn(); // mocking the dynamic module loader
      const { queryByTestId, getByTestId } = render(
        <CoreThemeStoreProvider store={store}>
          <input data-testid="fakeTestField" />
          <div data-testid="testDiv" />
          <TimeSlider {...props2} />
        </CoreThemeStoreProvider>,
      );
      expect(queryByTestId('timeSlider')).toBeTruthy();
      const inputField = getByTestId('fakeTestField');
      expect(inputField).toBeTruthy();
      const testDiv = getByTestId('testDiv');
      expect(testDiv).toBeTruthy();
      expect(queryByTestId('timeSlider')).toBeTruthy();
      expect(props2.onToggleTimeSlider).not.toHaveBeenCalled();

      fireEvent.keyDown(inputField, {
        key: 't',
        code: 'KeyT',
      });
      expect(props2.onToggleTimeSlider).not.toHaveBeenCalled();

      await waitFor(() => {
        fireEvent.keyDown(testDiv, {
          key: 't',
          code: 'KeyT',
        });
      });
      await waitFor(() =>
        expect(props2.onToggleTimeSlider).toHaveBeenCalledWith(false),
      );
    },
  );
});
