/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useTheme } from '@mui/material';
import { throttle } from 'lodash';
import makeStyles from '@mui/styles/makeStyles';

import moment from 'moment';
import { renderTimeSliderLegend } from './TimeSliderLegendRenderFunctions';
import CanvasComponent from '../../CanvasComponent/CanvasComponent';
import {
  getNewCenterOfFixedPointZoom,
  pixelToTimestamp,
  secondsPerPxValues,
  setNewRoundedTime,
  useCanvasTarget,
  defaultDataScaleToSecondsPerPx,
  roundWithTimeStep,
  timestampToPixel,
} from '../TimeSliderUtils';
import { Scale } from '../../../store/mapStore/types';

const useStyles = makeStyles({
  timeSliderLegend: {
    height: '50px',
  },
});

export interface TimeSliderLegendProps {
  centerTime: number;
  secondsPerPx: number;
  dataScaleToSecondsPerPx?: number;
  selectedTime?: number;
  scale?: Scale;
  currentTime?: number;
  dataStartTime?: number;
  dataEndTime?: number;
  animationStartTime?: string;
  animationEndTime?: string;
  isTimeSliderHoverOn?: boolean;
  timeStep?: number;
  mapIsActive?: boolean;
  onSetNewDate?: (newDate: string) => void;
  onSetCenterTime?: (newTime: number) => void;
  onZoom?: (newSecondsPerPx: number, newCenterTime: number) => void;
}

const getNextSecondsPerPxValue = (
  direction,
  oldSecondsPerPx,
  dataScaleToSecondsPerPx,
): number => {
  const fullSecondsPerPxValues = secondsPerPxValues(dataScaleToSecondsPerPx);
  const ind = fullSecondsPerPxValues.indexOf(oldSecondsPerPx);
  const newInd = ind + direction;
  const clampedInd = Math.max(
    0,
    Math.min(fullSecondsPerPxValues.length - 1, newInd),
  );
  return fullSecondsPerPxValues[clampedInd];
};

const NEEDLE_HANDLE_WIDTH = 10;
const LEFT_MOUSE_BTN = 1;
const THROTTLE_WAIT_TIME = 500; // [ms]
const THROTTLE_OPTIONS = {
  trailing: false, // Invoke function when called, but not more than once every <THROTTLE_WAIT_TIME>.
};

const TimeSliderLegend: React.FC<TimeSliderLegendProps> = ({
  centerTime,
  secondsPerPx,
  dataScaleToSecondsPerPx = defaultDataScaleToSecondsPerPx,
  selectedTime,
  scale = Scale.Hour,
  currentTime,
  dataStartTime,
  dataEndTime,
  animationStartTime,
  animationEndTime,
  isTimeSliderHoverOn,
  timeStep,
  mapIsActive = true,
  onSetNewDate,
  onSetCenterTime,
  onZoom,
}) => {
  const classes = useStyles();
  const [isAllowedCanvasNodePointed, node] = useCanvasTarget('mousedown');
  const [isDragged, setDragged] = React.useState(false);
  const [isReleased, setReleased] = React.useState(false);
  const [isNewSelectionTimeRequested, setNewSelectionTimeRequested] =
    React.useState(false);

  const [localAnimationStartTime, setLocalAnimationStartTime] = React.useState(
    animationStartTime && moment.utc(animationStartTime).unix(),
  );
  const [localAnimationEndTime, setLocalAnimationEndTime] = React.useState(
    animationEndTime && moment.utc(animationEndTime).unix(),
  );
  React.useEffect(() => {
    setLocalAnimationStartTime(
      animationStartTime && moment.utc(animationStartTime).unix(),
    );
    setLocalAnimationEndTime(
      animationEndTime && moment.utc(animationEndTime).unix(),
    );
  }, [animationStartTime, animationEndTime]);

  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent): void => {
      if (event.key === 'Home' && mapIsActive) {
        if (dataStartTime < dataEndTime) {
          if (dataEndTime < currentTime) {
            onSetNewDate(moment.utc(dataEndTime * 1000).toISOString());
            onSetCenterTime(dataEndTime);
          } else if (dataStartTime > currentTime) {
            onSetNewDate(moment.utc(dataStartTime * 1000).toISOString());
            onSetCenterTime(dataStartTime);
          } else {
            const closestToCurrent = roundWithTimeStep(currentTime, timeStep);
            onSetNewDate(moment.utc(closestToCurrent * 1000).toISOString());
            onSetCenterTime(closestToCurrent);
          }
        }
      }
    };
    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [
    timeStep,
    isTimeSliderHoverOn,
    currentTime,
    dataStartTime,
    dataEndTime,
    mapIsActive,
    onSetCenterTime,
    onSetNewDate,
  ]);

  const setSelectedTime = (x: number, width: number): void => {
    setNewRoundedTime(
      x,
      centerTime,
      width,
      secondsPerPx,
      timeStep,
      dataStartTime,
      dataEndTime,
      onSetNewDate,
    );
  };

  const theme = useTheme();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const throttledZoom = React.useCallback(
    throttle(
      (newSecondsPerPx, newCenterTime) => {
        onZoom(newSecondsPerPx, newCenterTime);
      },
      THROTTLE_WAIT_TIME,
      THROTTLE_OPTIONS,
    ),
    [],
  );

  const zoom = (deltaY, deltaX, canvasWidth, mouseX): void => {
    if (!onZoom) return;
    const direction = deltaY || deltaX;
    const newSecondsPerPx = getNextSecondsPerPxValue(
      Math.sign(direction),
      secondsPerPx,
      dataScaleToSecondsPerPx,
    );
    const mouseTime = pixelToTimestamp(
      mouseX,
      centerTime,
      canvasWidth,
      secondsPerPx,
    );
    const newCenterTime = getNewCenterOfFixedPointZoom(
      mouseTime,
      secondsPerPx,
      newSecondsPerPx,
      centerTime,
    );
    throttledZoom(newSecondsPerPx, newCenterTime);
  };

  return (
    <div className={classes.timeSliderLegend} data-testid="timeSliderLegend">
      <CanvasComponent
        ref={node}
        onMouseMove={(
          x: number,
          y: number,
          event: MouseEvent,
          width: number,
        ): void => {
          if (isTimeSliderHoverOn && dataStartTime && dataEndTime) {
            // Sets new selection time when moving and mouse was down when close enough the time slider needle. This makes needle draggable while leaving time slider legend in rest.
            setSelectedTime(x, width);
            return;
          }

          if (isAllowedCanvasNodePointed && event.buttons === LEFT_MOUSE_BTN) {
            // Sets new center time while dragging. This allows the whole time slider legend [as well as the time slider needle] to be mouse dragged as one.
            const dragDistanceX = event.movementX;
            setDragged(dragDistanceX !== 0);

            if (isNewSelectionTimeRequested) {
              setSelectedTime(x, width);
            } else {
              const dragDistanceTime = dragDistanceX * secondsPerPx;
              const newCenterTime = centerTime - dragDistanceTime;
              onSetCenterTime && onSetCenterTime(newCenterTime);
            }
          }
        }}
        onWheel={({ deltaY, deltaX, canvasWidth, mouseX }): void => {
          zoom(deltaY, deltaX, canvasWidth, mouseX);
        }}
        onMouseDown={(x, y, width): void => {
          setDragged(false);
          setReleased(false);

          const selectedTimeX = timestampToPixel(
            selectedTime,
            centerTime,
            width,
            secondsPerPx,
          );

          setNewSelectionTimeRequested(
            Math.abs(x - selectedTimeX) < NEEDLE_HANDLE_WIDTH,
          );
        }}
        onMouseUp={(): void => {
          setReleased(true);
          setNewSelectionTimeRequested(false);
        }}
        onCanvasClick={(x, y, width): void => {
          const isCanvasArea = x >= 0 && x < width;
          if (isCanvasArea) {
            if (isReleased && !isDragged) {
              setSelectedTime(x, width);
            }
          }
        }}
        onRenderCanvas={(
          ctx: CanvasRenderingContext2D,
          width: number,
          height: number,
        ): void => {
          renderTimeSliderLegend(
            ctx,
            theme,
            width,
            height,
            centerTime,
            secondsPerPx,
            dataScaleToSecondsPerPx,
            selectedTime,
            scale,
            currentTime,
            localAnimationStartTime,
            localAnimationEndTime,
          );
        }}
      />
    </div>
  );
};

export default TimeSliderLegend;
