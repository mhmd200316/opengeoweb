/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import moment from 'moment';
import { Dimension, Layer, Scale } from '../../../store/mapStore/types';
import { AppStore } from '../../../types/types';
import {
  mapActions,
  mapSelectors,
  genericSelectors,
  genericActions,
} from '../../../store';

import TimeSliderLegend from './TimeSliderLegend';
import {
  getDataLimitsFromLayers,
  getTimeBounds,
  TimeBounds,
} from '../TimeSliderUtils';

import { SYNCGROUPS_TYPE_SETTIME } from '../../../store/generic/synchronizationGroups/constants';
import { handleMomentISOString } from '../../../utils/dimensionUtils';
import { getActiveWindowId } from '../../../store/ui/selectors';

interface TimeSliderLegendConnectProps {
  sourceId?: string;
  mapId: string;
  dimensions?: Dimension[];
  scale?: Scale;
  centerTime?: number;
  secondsPerPx?: number;
  dataScaleToSecondsPerPx?: number;
  layers?: Layer[];
  isAnimating?: boolean;
  timeStep?: number;
  isTimeSliderHoverOn?: boolean;
  animationStartTime?: string;
  animationEndTime?: string;
  activeWindowId?: string;
  stopMapAnimation?: typeof mapActions.mapStopAnimation;
  setTime?: typeof genericActions.setTime;
  syncGroupAddSource?: typeof genericActions.syncGroupAddSource;
  syncGroupRemoveSource?: typeof genericActions.syncGroupRemoveSource;
  mapSetTimeSliderCenterTime?: typeof mapActions.setTimeSliderCenterTime;
  mapSetTimeSliderSecondsPerPx?: typeof mapActions.setTimeSliderSecondsPerPx;
}

const connectRedux = connect(
  (store: AppStore, props: TimeSliderLegendConnectProps) => ({
    layers: mapSelectors.getMapLayers(store, props.mapId),
    dimensions: mapSelectors.getMapDimensions(store, props.mapId),
    scale: mapSelectors.getMapTimeSliderScale(store, props.mapId),
    centerTime: mapSelectors.getMapTimeSliderCenterTime(store, props.mapId),
    secondsPerPx: mapSelectors.getMapTimeSliderSecondsPerPx(store, props.mapId),
    dataScaleToSecondsPerPx:
      mapSelectors.getMapTimeSliderDataScaleToSecondsPerPx(store, props.mapId),
    isAnimating: mapSelectors.isAnimating(store, props.mapId),
    timeStep: mapSelectors.getMapTimeStep(store, props.mapId),
    isTimeSliderHoverOn: mapSelectors.isTimeSliderHoverOn(store, props.mapId),
    timeValue: genericSelectors.getTime(
      genericSelectors.getSynchronizationGroupStore(store),
      props.sourceId,
    ),
    animationStartTime: mapSelectors.getAnimationStartTime(store, props.mapId),
    animationEndTime: mapSelectors.getAnimationEndTime(store, props.mapId),
    activeWindowId: getActiveWindowId(store),
  }),
  {
    stopMapAnimation: mapActions.mapStopAnimation,
    setTime: genericActions.setTime,
    syncGroupAddSource: genericActions.syncGroupAddSource,
    syncGroupRemoveSource: genericActions.syncGroupRemoveSource,
    mapSetTimeSliderCenterTime: mapActions.setTimeSliderCenterTime,
    mapSetTimeSliderSecondsPerPx: mapActions.setTimeSliderSecondsPerPx,
  },
);

const TimeSliderLegendConnect: React.FC<TimeSliderLegendConnectProps> = ({
  sourceId,
  mapId,
  dimensions,
  scale,
  centerTime,
  secondsPerPx,
  dataScaleToSecondsPerPx,
  layers,
  isAnimating,
  timeStep,
  isTimeSliderHoverOn,
  animationStartTime,
  animationEndTime,
  activeWindowId,
  stopMapAnimation,
  setTime,
  syncGroupAddSource,
  syncGroupRemoveSource,
  mapSetTimeSliderCenterTime,
  mapSetTimeSliderSecondsPerPx,
}: TimeSliderLegendConnectProps) => {
  const { selectedTime }: TimeBounds = getTimeBounds(dimensions);
  const currentTime = moment.utc().unix();
  const [dataStartTime, dataEndTime] = getDataLimitsFromLayers(layers);

  const mapIsActive = mapId === activeWindowId;

  React.useEffect(() => {
    syncGroupAddSource({
      id: sourceId,
      type: [SYNCGROUPS_TYPE_SETTIME],
    });
    return (): void => {
      syncGroupRemoveSource({
        id: sourceId,
      });
    };
  }, [sourceId, syncGroupAddSource, syncGroupRemoveSource]);

  return (
    <TimeSliderLegend
      centerTime={centerTime}
      secondsPerPx={secondsPerPx}
      dataScaleToSecondsPerPx={dataScaleToSecondsPerPx}
      selectedTime={selectedTime}
      scale={scale}
      currentTime={currentTime}
      isTimeSliderHoverOn={isTimeSliderHoverOn}
      dataStartTime={dataStartTime}
      dataEndTime={dataEndTime}
      animationStartTime={animationStartTime}
      animationEndTime={animationEndTime}
      timeStep={timeStep}
      mapIsActive={mapIsActive}
      onSetNewDate={(newDate): void => {
        if (isAnimating) {
          stopMapAnimation({ mapId });
        }
        setTime({
          sourceId,
          origin: 'TimeSliderConnect, 139',
          value: handleMomentISOString(newDate),
        });
      }}
      onSetCenterTime={(newTime: number): void => {
        mapSetTimeSliderCenterTime({ mapId, timeSliderCenterTime: newTime });
      }}
      onZoom={(newSecondsPerPx, newCenterTime): void => {
        mapSetTimeSliderSecondsPerPx({
          mapId,
          timeSliderSecondsPerPx: newSecondsPerPx,
        });
        mapSetTimeSliderCenterTime({
          mapId,
          timeSliderCenterTime: newCenterTime,
        });
      }}
    />
  );
};

/**
 * TimeSliderLegend component connected to the store displaying a legend of the time slider
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <TimeSliderLegendConnect mapId={mapId} />```
 */

export default connectRedux(TimeSliderLegendConnect);
