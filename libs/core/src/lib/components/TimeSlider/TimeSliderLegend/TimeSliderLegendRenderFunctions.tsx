/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Theme } from '@mui/material';
import moment from 'moment';
import { Scale } from '../../../store/mapStore/types';
import {
  pixelToTimestamp,
  timestampToPixelEdges,
  scaleToSecondsPerPx,
  timestampToPixel,
} from '../TimeSliderUtils';

const scaleToUnitSeconds = {
  [Scale.Minutes5]: 5 * 60,
  [Scale.Hour]: 60 * 60,
  [Scale.Hours3]: 3 * 60 * 60,
  [Scale.Hours6]: 6 * 60 * 60,
  [Scale.Day]: 24 * 60 * 60,
  [Scale.Week]: 7 * 24 * 60 * 60,
};

const drawNeedle = (
  context: CanvasRenderingContext2D,
  theme: Theme,
  width: number,
  height: number,
  startTime: number,
  endTime: number,
  selectedTime: number,
): void => {
  if (!selectedTime) return;
  const ctx = context;
  const { playerNeedlePlayer, playerNeedlePlayerTop } =
    theme.palette.geowebColors.timeSlider;
  const selectedPx = timestampToPixelEdges(
    selectedTime,
    startTime,
    endTime,
    width,
  );

  // Draw a high vertical line (needle) indicating the current or selected time
  ctx.beginPath();
  // Move 1 pixel line by 0.5 pixels to prevent it from being shown as blurry: https://usefulangle.com/post/17/html5-canvas-drawing-1px-crisp-straight-lines
  ctx.moveTo(Math.floor(selectedPx) + 0.5, height - 50);
  ctx.lineTo(Math.floor(selectedPx) + 0.5, height);
  ctx.lineWidth = 3;
  ctx.strokeStyle = playerNeedlePlayer.rgba;
  ctx.stroke();
  ctx.lineWidth = 1;
  ctx.strokeStyle = playerNeedlePlayerTop.rgba;
  ctx.stroke();
};

const drawCurrentTime = (
  context: CanvasRenderingContext2D,
  theme: Theme,
  width: number,
  height: number,
  startTime: number,
  endTime: number,
  currentTime: number,
): void => {
  const ctx = context;
  const { playerNeedleTime, playerNeedleTimeTop } =
    theme.palette.geowebColors.timeSlider;
  const current = currentTime;
  const currentPx = current
    ? timestampToPixelEdges(current, startTime, endTime, width)
    : 0;
  // Draw a high vertical line (needle) indicating the current or selected time
  ctx.beginPath();
  // Move 1 pixel line by 0.5 pixels to prevent it from being shown as blurry: https://usefulangle.com/post/17/html5-canvas-drawing-1px-crisp-straight-lines
  ctx.moveTo(Math.floor(currentPx) + 0.5, height - 50);
  ctx.lineTo(Math.floor(currentPx) + 0.5, height);
  ctx.lineWidth = 3;
  ctx.strokeStyle = playerNeedleTime.rgba;
  ctx.stroke();
  ctx.lineWidth = 1;
  ctx.strokeStyle = playerNeedleTimeTop.rgba;
  ctx.stroke();
};

const TICK_MARK_HEIGHT_PRIMARY = 40;
const TICK_MARK_HEIGHT_SECONDARY = 10;
const TICK_MARK_HEIGHT_TERTIARY = 5;
const getStepPxLineHeight = (scale: Scale, timestep: number): number => {
  if ([Scale.Hour, Scale.Hours3, Scale.Hours6, Scale.Day].includes(scale)) {
    const scaleUnitSeconds = scaleToUnitSeconds[scale];
    const rem = timestep % scaleUnitSeconds;
    switch (scale) {
      case Scale.Hour: {
        if (rem === 0) return TICK_MARK_HEIGHT_PRIMARY;
        // 15 min subdivisions
        if (rem % (15 * 60) === 0) return TICK_MARK_HEIGHT_SECONDARY;
        return TICK_MARK_HEIGHT_TERTIARY;
      }
      case Scale.Hours3: {
        if (rem === 0) return TICK_MARK_HEIGHT_PRIMARY;
        return TICK_MARK_HEIGHT_TERTIARY;
      }
      case Scale.Hours6: {
        if (rem === 0) return TICK_MARK_HEIGHT_PRIMARY;
        // 3 hour subdivisions
        if (rem % (3 * 60 * 60) === 0) return TICK_MARK_HEIGHT_SECONDARY;
        return TICK_MARK_HEIGHT_TERTIARY;
      }
      default: {
        // Scale.Day
        if (rem === 0) return TICK_MARK_HEIGHT_PRIMARY;
        // 6 hour subdivisions
        if (rem % (6 * 60 * 60) === 0) return TICK_MARK_HEIGHT_SECONDARY;
        return TICK_MARK_HEIGHT_TERTIARY;
      }
    }
  } else {
    switch (scale) {
      case Scale.Week: {
        const weekday = moment.unix(timestep).utc().isoWeekday();
        // momentjs isoWeekdays range from 1(Monday) to 7(Sunday)
        if (weekday === 1) return TICK_MARK_HEIGHT_PRIMARY;
        return TICK_MARK_HEIGHT_TERTIARY;
      }
      case Scale.Year: {
        const month = moment.unix(timestep).utc().month();
        // momentjs months are zero indexed
        if (month === 0) return TICK_MARK_HEIGHT_PRIMARY;
        if (month % 3 === 0) return TICK_MARK_HEIGHT_SECONDARY;
        return TICK_MARK_HEIGHT_TERTIARY;
      }
      case Scale.Minutes5: {
        // add primary line on day change
        if (timestep % (60 * 60 * 24) === 0) return TICK_MARK_HEIGHT_PRIMARY;
        return TICK_MARK_HEIGHT_TERTIARY;
      }
      default:
        // Scale.Month
        return TICK_MARK_HEIGHT_TERTIARY;
    }
  }
};

const getStepPxLineHeightForDataScale = (
  scale: Scale,
  timestep: number,
): number => {
  const scaleUnitSeconds = scaleToUnitSeconds[scale];
  const rem = timestep % scaleUnitSeconds;
  switch (scale) {
    case Scale.Hour: {
      if (rem === 0) return TICK_MARK_HEIGHT_PRIMARY;
      // 15 min subdivisions
      if (rem % (15 * 60) === 0) return TICK_MARK_HEIGHT_SECONDARY;
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case Scale.Hours3: {
      if (rem === 0) return TICK_MARK_HEIGHT_PRIMARY;
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case Scale.Hours6: {
      if (rem === 0) return TICK_MARK_HEIGHT_PRIMARY;
      // 3 hour subdivisions
      if (rem % (3 * 60 * 60) === 0) return TICK_MARK_HEIGHT_SECONDARY;
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case Scale.Day: {
      if (rem === 0) return TICK_MARK_HEIGHT_PRIMARY;
      // 6 hour subdivisions
      if (rem % (6 * 60 * 60) === 0) return TICK_MARK_HEIGHT_SECONDARY;
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case Scale.Week: {
      const weekday = moment.unix(timestep).utc().isoWeekday();
      // momentjs isoWeekdays range from 1(Monday) to 7(Sunday)
      if (weekday === 1) return TICK_MARK_HEIGHT_PRIMARY;
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case Scale.Year: {
      const month = moment.unix(timestep).utc().month();
      // momentjs months are zero indexed
      if (month === 0) return TICK_MARK_HEIGHT_PRIMARY;
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    case Scale.Minutes5: {
      // add primary line on day change
      if (timestep % (60 * 60 * 24) === 0) return TICK_MARK_HEIGHT_PRIMARY;
      return TICK_MARK_HEIGHT_TERTIARY;
    }
    default:
      // Scale.Month
      return TICK_MARK_HEIGHT_TERTIARY;
  }
};

const drawStepPxLine = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  stepPx: number,
  height: number,
  scale: Scale,
  fundamentalScale: Scale,
  timestep: number,
): void => {
  const ctx2 = ctx;
  ctx2.strokeStyle =
    theme.palette.geowebColors.timeSlider.timelineTimeScale.fill;
  ctx2.lineWidth = 1;
  const stepPxLineHeight =
    scale !== Scale.DataScale
      ? getStepPxLineHeight(scale, timestep)
      : getStepPxLineHeightForDataScale(fundamentalScale, timestep);

  ctx2.beginPath();
  ctx2.moveTo(stepPx, height - stepPxLineHeight);
  ctx2.lineTo(stepPx, height);
  ctx2.stroke();
};

const drawTimeText = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  stepPx: number,
  height: number,
  timeText: string,
  scale: Scale,
): void => {
  const ctx2 = ctx;
  const { fontFamily, color } =
    theme.palette.geowebColors.timeSlider.timelineText;
  ctx2.fillStyle = color;
  // time text has smaller font than other timeline text
  ctx2.font = `12px ${fontFamily}`;
  ctx2.textAlign = 'left';
  if (
    scale === Scale.Year ||
    scale === Scale.Month ||
    scale === Scale.Week ||
    scale === Scale.Day
  ) {
    ctx2.fillText(timeText, stepPx, height - 12);
  } else {
    ctx2.fillText(timeText, stepPx - 15, height - 12);
  }
};

const drawUpperText = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  newDate: string,
  stepPx: number,
  height: number,
  minOffset: number,
): void => {
  if (stepPx <= 0) return;
  const ctx2 = ctx;
  const { fontSize, fontFamily, color } =
    theme.palette.geowebColors.timeSlider.timelineText;
  ctx2.fillStyle = color;
  ctx2.font = `${fontSize}px ${fontFamily}`;
  ctx2.textAlign = 'left';
  ctx2.fillText(newDate, Math.max(minOffset, stepPx + 2), height - 30);
};

const drawYearScaleText = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  newDate: string,
  stepPx: number,
  height: number,
): void => {
  const ctx2 = ctx;
  const { fontSize, fontFamily, color } =
    theme.palette.geowebColors.timeSlider.timelineText;
  ctx2.fillStyle = color;
  ctx2.font = `${fontSize}px ${fontFamily}`;
  ctx2.textAlign = 'left';
  ctx2.fillText(newDate, stepPx + 2, height - 30);
};

const drawLeftSideDateText = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  newDate: string,
  height: number,
): void => {
  const ctx2 = ctx;
  const { fontSize, fontFamily, color } =
    theme.palette.geowebColors.timeSlider.timelineText;
  ctx2.fillStyle = color;
  ctx2.font = `${fontSize}px ${fontFamily}`;
  ctx2.textAlign = 'left';
  ctx2.fillText(newDate, 2, height - 30);
};

const getTimeFormat = (scale: number): string => {
  switch (scale) {
    case Scale.Year:
      return 'Y';
    case Scale.Month:
      return 'MMM';
    case Scale.Week:
      return 'DD';
    case Scale.Day:
      return 'ddd DD';
    default:
      return 'HH:mm';
  }
};

const drawDateChangeLine = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  stepPx: number,
  height: number,
): void => {
  const ctx2 = ctx;
  ctx2.strokeStyle =
    theme.palette.geowebColors.timeSlider.timelineTimeScale.fill;
  ctx2.lineWidth = 1;
  ctx2.beginPath();
  ctx2.moveTo(stepPx, height - 40);
  ctx2.lineTo(stepPx, height);
  ctx2.stroke();
};

const drawDashedMonthChangeLine = (
  ctx: CanvasRenderingContext2D,
  theme: Theme,
  stepPx: number,
  height: number,
): void => {
  const ctx2 = ctx;
  ctx2.strokeStyle =
    theme.palette.geowebColors.timeSlider.timelineMonthChangeDash.rgba;
  ctx2.lineWidth = 3;
  ctx2.setLineDash([5, 5]);
  ctx2.beginPath();
  ctx2.moveTo(stepPx, 0);
  ctx2.lineTo(stepPx, height);
  ctx2.stroke();
  ctx2.setLineDash([]);
};

const roundByUnitOfTime = (
  timeUnix: number,
  scaleType: moment.unitOfTime.DurationConstructor,
  roundUp = false,
): number => {
  const unitOfTimeStart = moment.unix(timeUnix).utc().startOf(scaleType);
  if (roundUp) {
    return unitOfTimeStart.add(1, scaleType).unix();
  }
  return unitOfTimeStart.unix();
};

const roundToNearestMultiple = (
  timeUnix: number,
  scale: Scale,
  roundUp = false,
): number => {
  const stepSeconds = scaleToUnitSeconds[scale];
  const roundFn = roundUp ? Math.ceil : Math.floor;
  return roundFn(timeUnix / stepSeconds) * stepSeconds;
};

/**
 * In this script starting and ending times of the time slider are rounded.
 * Here it is defined what is a time unit to a start of which a rounding is
 * done for each scale.
 * @returns an object where there are rounding units of each scale
 */

const scaleToRoundableUnit = {
  [Scale.Hour]: 'hour',
  [Scale.Day]: 'day',
  [Scale.Week]: 'isoWeek',
  [Scale.Month]: 'isoWeek',
  [Scale.Year]: 'year',
  [Scale.DataScale]: 'year',
};

export const getRoundedStartAndEnd = (
  visibleTimeStart: number,
  visibleTimeEnd: number,
  scale: Scale,
): [number, number] => {
  if (
    scale === Scale.Minutes5 ||
    scale === Scale.Hours3 ||
    scale === Scale.Hours6
  ) {
    return [
      roundToNearestMultiple(visibleTimeStart, scale, false),
      roundToNearestMultiple(visibleTimeEnd, scale, true),
    ];
  }
  const momentUnit = scaleToRoundableUnit[
    scale
  ] as moment.unitOfTime.DurationConstructor;
  return [
    roundByUnitOfTime(visibleTimeStart, momentUnit, false),
    roundByUnitOfTime(visibleTimeEnd, momentUnit, true),
  ];
};

export const getCustomRoundedStartAndEnd = (
  visibleTimeStart: number,
  visibleTimeEnd: number,
  unit: string,
): [number, number] => {
  return [
    roundByUnitOfTime(
      visibleTimeStart,
      unit as moment.unitOfTime.DurationConstructor,
      false,
    ),
    roundByUnitOfTime(
      visibleTimeEnd,
      unit as moment.unitOfTime.DurationConstructor,
      true,
    ),
  ];
};

const incrementTimestep = (
  timeAtTimeStep: moment.Moment,
  fundamentalScale: Scale,
): moment.Moment => {
  switch (fundamentalScale) {
    case Scale.Minutes5:
      return timeAtTimeStep.add(1, 'minute');
    case Scale.Hour:
      return timeAtTimeStep.add(5, 'minutes');
    case Scale.Hours3:
    case Scale.Hours6:
      return timeAtTimeStep.add(1, 'hour');
    case Scale.Day:
      return timeAtTimeStep.add(3, 'hours');
    case Scale.Week:
      return timeAtTimeStep.add(1, 'day');
    case Scale.Month:
      return timeAtTimeStep.add(1, 'week');
    case Scale.Year:
      return timeAtTimeStep.add(1, 'month');
    default:
      return timeAtTimeStep.add(1, 'month');
  }
};

const getTimesteps = (
  roundedStart,
  roundedEnd,
  fundamentalScaleType,
): Array<number> => {
  const timesteps = [];
  let timestep = roundedStart;
  while (timestep <= roundedEnd) {
    timesteps.push(timestep);
    timestep = incrementTimestep(
      moment.unix(timestep).utc(),
      fundamentalScaleType,
    ).unix();
  }
  return timesteps;
};

const getDateChangeTimestepUnit = (scale: Scale): string => {
  switch (scale) {
    case Scale.DataScale:
      return 'year';
    case Scale.Month:
      return 'year';
    case Scale.Week:
    case Scale.Day:
      return 'month';
    default:
      return 'day';
  }
};

const getMinOffset = (scale: Scale): number => {
  switch (scale) {
    case Scale.Month:
      return 42;
    case Scale.Week:
    case Scale.Day:
      return 70;
    default:
      return 115;
  }
};

const getCustomTimesteps = (
  roundedStart,
  roundedEnd,
  increment,
): Array<number> => {
  const momentIncrement = increment as moment.unitOfTime.DurationConstructor;
  const timesteps = [];
  let timestep = roundedStart;
  while (timestep <= roundedEnd) {
    timesteps.push(timestep);
    timestep = moment.unix(timestep).utc().add(1, momentIncrement).unix();
  }
  return timesteps;
};

const getLeftSideDateText = (timestamp, scale): string => {
  const timeMoment = moment.unix(timestamp).utc();
  switch (scale) {
    case Scale.Minutes5:
    case Scale.Hour:
    case Scale.Hours3:
    case Scale.Hours6:
      return timeMoment.format('Y MMM ddd DD');
    case Scale.Day:
    case Scale.Week:
      return timeMoment.format('Y MMM');
    case Scale.Month:
      return timeMoment.format('Y');
    default:
      return '';
  }
};

const getLeftSideDateTextForDataScale = (
  timestamp,
  fundamentalScale,
): string => {
  const timeMoment = moment.unix(timestamp).utc();
  switch (fundamentalScale) {
    case Scale.Minutes5:
    case Scale.Hour:
    case Scale.Hours3:
    case Scale.Hours6:
      return timeMoment.format('Y MMM ddd DD');
    case Scale.Day:
    case Scale.Week:
      return timeMoment.format('Y MMM');
    case Scale.Month:
      return timeMoment.format('Y');
    case Scale.Year:
      return timeMoment.format('Y');
    default:
      return '';
  }
};

const getDateChangeFormat = (
  timestamp: number,
  timestepUnit: string,
): string => {
  const momentTime = moment.unix(timestamp).utc();
  if (timestepUnit === 'year') {
    return momentTime.format('Y');
  }
  if (timestepUnit === 'month') {
    if (momentTime.month() === 0) {
      return momentTime.format('Y MMM');
    }
    return momentTime.format('MMM');
  }
  if (momentTime.month() === 0 && momentTime.date() === 1) {
    return momentTime.format('Y MMM ddd DD');
  }
  if (momentTime.date() === 1) {
    return momentTime.format('MMM ddd DD');
  }
  return momentTime.format('ddd DD');
};

// For a data scale an appropriate fundamental scale is chosen
// depending on dataScaleToSecondsPerPx of data. When a time range of
// data is for example many months. the fundamental scale Scale.Year
// is chosen here and the legend of the time slider is drawn in the
// same as if otiginally a year scale would have been chose.
//
// If a  time range of data is a few weeks, Scale.Month is chosen. So, all
// the time range of data can be shown in the window.

const getFundamentalScaleForDataScale = (
  dataScaleToSecondsPerPx: number,
): Scale => {
  if (dataScaleToSecondsPerPx >= scaleToSecondsPerPx[Scale.Month]) {
    return Scale.Year;
  }
  if (dataScaleToSecondsPerPx >= scaleToSecondsPerPx[Scale.Week]) {
    return Scale.Month;
  }
  if (dataScaleToSecondsPerPx >= scaleToSecondsPerPx[Scale.Day]) {
    return Scale.Week;
  }
  if (dataScaleToSecondsPerPx >= scaleToSecondsPerPx[Scale.Hour]) {
    return Scale.Day;
  }
  return Scale.Hour;
};

const getColorChangeTimestepUnit = (scale: Scale): string => {
  switch (scale) {
    case Scale.Year:
      return 'year';
    case Scale.Month:
      return 'month';
    case Scale.Week:
      return 'isoWeek';
    default:
      return 'day';
  }
};

const isColorIntervalEven = (scale: Scale, timestep: number): boolean => {
  const momentTime = moment.unix(timestep).utc();
  switch (scale) {
    case Scale.Year:
      return momentTime.year() % 2 === 0;
    case Scale.Month:
      return momentTime.month() % 2 === 0;
    case Scale.Week:
      return momentTime.isoWeek() % 2 === 0;
    default:
      return (timestep / (60 * 60 * 24)) % 2 === 0;
  }
};

const drawBackground = (
  context: CanvasRenderingContext2D,
  theme: Theme,
  visibleTimeStart: number,
  visibleTimeEnd: number,
  animationStartPx: number,
  animationEndPx: number,
  canvasWidth: number,
  height: number,
  scale: Scale,
): void => {
  const ctx = context;
  const { timelineTimelineSurface, timelineNightTime } =
    theme.palette.geowebColors.timeSlider;

  ctx.fillStyle = timelineTimelineSurface.fill;
  ctx.fillRect(0, 0, canvasWidth, height);
  ctx.fillStyle =
    theme.palette.geowebColors.timeSlider.timelineSelectionBackground.rgba;
  ctx.fillRect(
    animationStartPx,
    0,
    Math.max(animationEndPx - animationStartPx, 0),
    height,
  );

  const colorChangeUnit = getColorChangeTimestepUnit(scale);
  const [colorChangeStart, colorChangeEnd] = getCustomRoundedStartAndEnd(
    visibleTimeStart,
    visibleTimeEnd,
    colorChangeUnit,
  );
  const colorChangeTimesteps = getCustomTimesteps(
    colorChangeStart,
    colorChangeEnd,
    colorChangeUnit,
  );
  colorChangeTimesteps.forEach((timestep, i, arr) => {
    if (i === 0) return;
    const [prevPx, currPx] = [arr[i - 1], timestep].map((timestep) =>
      timestampToPixelEdges(
        timestep,
        visibleTimeStart,
        visibleTimeEnd,
        canvasWidth,
      ),
    );
    if (!isColorIntervalEven(scale, timestep)) {
      ctx.fillStyle = timelineNightTime.rgba;
      ctx.fillRect(prevPx, 0, currPx - prevPx, height);
    }
  });
};

const drawTimeScale = (
  context: CanvasRenderingContext2D,
  theme: Theme,
  visibleTimeStart: number,
  visibleTimeEnd: number,
  canvasWidth: number,
  height: number,
  scale: Scale,
  dataScaleToSecondsPerPx: number,
): void => {
  const fundamentalScale =
    scale === Scale.DataScale
      ? getFundamentalScaleForDataScale(dataScaleToSecondsPerPx)
      : scale;

  const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
    visibleTimeStart,
    visibleTimeEnd,
    fundamentalScale,
  );

  // draw regular tickmarks
  const ctx = context;
  getTimesteps(roundedStart, roundedEnd, fundamentalScale).forEach(
    (timestep) => {
      const stepPx = timestampToPixelEdges(
        timestep,
        visibleTimeStart,
        visibleTimeEnd,
        canvasWidth,
      );
      // Move 1 pixel line by 0.5 pixels to prevent it from being shown as blurry: https://usefulangle.com/post/17/html5-canvas-drawing-1px-crisp-straight-lines
      drawStepPxLine(
        ctx,
        theme,
        Math.floor(stepPx) + 0.5,
        height,
        scale,
        fundamentalScale,
        timestep,
      );

      // Draw the time step time as text
      const timeFormat = getTimeFormat(fundamentalScale);
      const timeText = moment
        .unix(timestep)
        .utc()
        .format(timeFormat)
        .toString();
      if (
        ([
          Scale.Minutes5,
          Scale.Hour,
          Scale.Hours3,
          Scale.Hours6,
          Scale.Day,
        ].includes(fundamentalScale) &&
          timestep % scaleToUnitSeconds[fundamentalScale] === 0) ||
        (fundamentalScale === Scale.Week &&
          moment.unix(timestep).isoWeekday() === 1)
      ) {
        drawTimeText(ctx, theme, stepPx, height, timeText, fundamentalScale);
      }

      if (
        fundamentalScale === Scale.Year &&
        moment.unix(timestep).month() === 0
      ) {
        drawYearScaleText(ctx, theme, timeText, stepPx, height);
      }
    },
  );

  // draw month overlay lines
  if (
    fundamentalScale === Scale.Week ||
    fundamentalScale === Scale.Month ||
    scale === Scale.DataScale
  ) {
    const [roundedStart, roundedEnd] = getCustomRoundedStartAndEnd(
      visibleTimeStart,
      visibleTimeEnd,
      'month',
    );
    const monthTimesteps = getCustomTimesteps(
      roundedStart,
      roundedEnd,
      'month',
    );
    monthTimesteps.forEach((timestep) => {
      const stepPx = timestampToPixelEdges(
        timestep,
        visibleTimeStart,
        visibleTimeEnd,
        canvasWidth,
      );
      if (
        fundamentalScale === Scale.Month ||
        (scale === Scale.DataScale && fundamentalScale !== Scale.Day)
      ) {
        const momentTime = moment.unix(timestep).utc();
        // Move 1 pixel line by 0.5 pixels to prevent it from being shown as blurry: https://usefulangle.com/post/17/html5-canvas-drawing-1px-crisp-straight-lines
        drawDateChangeLine(ctx, theme, Math.floor(stepPx) + 0.5, height);
        drawTimeText(
          ctx,
          theme,
          stepPx,
          height,
          momentTime.format('MMM'),
          fundamentalScale,
        );
        return;
      }
      // adding 0.5 to make line not so blurred
      drawDashedMonthChangeLine(ctx, theme, Math.floor(stepPx) + 0.5, height);
    });
  }
  // draw date change texts
  if (scale === Scale.Year && fundamentalScale === Scale.Year) return;
  const timestepUnit = getDateChangeTimestepUnit(fundamentalScale);
  const [dateChangeStart, dateChangeEnd] = getCustomRoundedStartAndEnd(
    visibleTimeStart,
    visibleTimeEnd,
    timestepUnit,
  );
  const dateChangeTimesteps = getCustomTimesteps(
    dateChangeStart,
    dateChangeEnd,
    timestepUnit,
  );
  dateChangeTimesteps.forEach((timestep) => {
    const stepPx = timestampToPixelEdges(
      timestep,
      visibleTimeStart,
      visibleTimeEnd,
      canvasWidth,
    );
    const textFormat = getDateChangeFormat(timestep, timestepUnit);
    const minOffset = getMinOffset(fundamentalScale);
    if (scale !== Scale.DataScale) {
      drawUpperText(ctx, theme, textFormat, stepPx, height, minOffset);
    }
  });
  const dateText =
    scale === Scale.DataScale
      ? getLeftSideDateTextForDataScale(visibleTimeStart, fundamentalScale)
      : getLeftSideDateText(visibleTimeStart, scale);
  drawLeftSideDateText(ctx, theme, dateText, height);
};

export const renderTimeSliderLegend = (
  context: CanvasRenderingContext2D,
  theme: Theme,
  canvasWidth: number,
  height: number,
  centerTime: number,
  secondsPerPx: number,
  dataScaleToSecondsPerPx: number,
  selectedTimeUnix: number,
  scale: Scale,
  currentTimeUnix: number,
  animationStartTime: number,
  animationEndTime: number,
): void => {
  const ctx = context;
  const [visibleTimeStart, visibleTimeEnd] = [0, canvasWidth].map((px) =>
    pixelToTimestamp(px, centerTime, canvasWidth, secondsPerPx),
  );

  const [animationStartPx, animationEndPx] = [
    animationStartTime,
    animationEndTime,
  ].map((time) => {
    return timestampToPixel(time, centerTime, canvasWidth, secondsPerPx);
  });
  drawBackground(
    ctx,
    theme,
    visibleTimeStart,
    visibleTimeEnd,
    animationStartPx,
    animationEndPx,
    canvasWidth,
    height,
    scale,
  );
  drawTimeScale(
    ctx,
    theme,
    visibleTimeStart,
    visibleTimeEnd,
    canvasWidth,
    height,
    scale,
    dataScaleToSecondsPerPx,
  );
  drawNeedle(
    ctx,
    theme,
    canvasWidth,
    height,
    visibleTimeStart,
    visibleTimeEnd,
    selectedTimeUnix,
  );
  drawCurrentTime(
    ctx,
    theme,
    canvasWidth,
    height,
    visibleTimeStart,
    visibleTimeEnd,
    currentTimeUnix,
  );
};
