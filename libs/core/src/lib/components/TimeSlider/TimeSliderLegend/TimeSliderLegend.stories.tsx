/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { lightTheme, darkTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Box } from '@mui/material';
import moment from 'moment';
import { Scale } from '../../../store/mapStore/types';
import TimeSliderLegend from './TimeSliderLegend';

export default { title: 'components/TimeSlider/TimeSliderLegend' };

const commonStoryProps = {
  name: 'time',
  units: 'ISO8601',
  currentValue: '2020-10-30T18:00:00.000Z',
  centerTime: moment.utc('2020-10-30T18:00:00.000Z').unix(),
  animationStartTime: '2020-10-30T17:00:00.000Z',
  animationEndTime: '2020-10-30T19:00:00.000Z',
};

const TimeSliderLegendDisplay = (): React.ReactElement => {
  return (
    <div>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          secondsPerPx={3}
          scale={Scale.Minutes5}
          currentTime={moment.utc('2020-10-30').hours(19).unix()}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          scale={Scale.Hour}
          secondsPerPx={60}
          currentTime={moment.utc('2020-10-31').hours(19).unix()}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          scale={Scale.Hours3}
          secondsPerPx={60}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          scale={Scale.Hours6}
          secondsPerPx={60}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          scale={Scale.Day}
          secondsPerPx={300}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          scale={Scale.Week}
          secondsPerPx={7000}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          scale={Scale.Month}
          secondsPerPx={7000}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
        />
      </Box>
      <Box m={2}>
        <TimeSliderLegend
          {...commonStoryProps}
          scale={Scale.Year}
          secondsPerPx={100000}
          currentTime={moment.utc('2020-10-29').hours(19).unix()}
        />
      </Box>
    </div>
  );
};

export const TimeSliderLegendDemoLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <TimeSliderLegendDisplay />
  </ThemeWrapper>
);

export const TimeSliderLegendDemoDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <TimeSliderLegendDisplay />
  </ThemeWrapper>
);

TimeSliderLegendDemoLight.storyName =
  'Time Slider Legend Light Theme (takeSnapshot)';

TimeSliderLegendDemoDark.storyName =
  'Time Slider Legend Dark Theme (takeSnapshot)';
