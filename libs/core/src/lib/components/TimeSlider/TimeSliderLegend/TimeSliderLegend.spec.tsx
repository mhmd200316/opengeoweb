/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import moment from 'moment';

import TimeSliderLegend, { TimeSliderLegendProps } from './TimeSliderLegend';
import { roundWithTimeStep } from '../TimeSliderUtils';
import { CoreThemeProvider } from '../../Providers/Providers';

describe('src/lib/components/TimeSlider/TimeSliderLegend/TimeSliderLegend', () => {
  const date = moment.utc('20200101', 'YYYYMMDD');
  const props: TimeSliderLegendProps = {
    scale: 5,
    timeStep: 5,
    centerTime: moment.utc(date).startOf('day').hours(12).unix(),
    currentTime: moment.utc(date).startOf('day').hours(12).unix(),
    secondsPerPx: 5,
    onZoom: jest.fn(),
    onSetNewDate: jest.fn(),
    onSetCenterTime: jest.fn(),
  };

  it('should render the component with canvas', () => {
    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderLegend {...props} />
      </CoreThemeProvider>,
    );
    expect(container.querySelector('canvas')).toBeTruthy();
  });

  it('should call onZoom when scroll wheel is applied', () => {
    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderLegend {...props} />
      </CoreThemeProvider>,
    );
    expect(container).toBeTruthy();
    const canvas = container.querySelector('canvas');
    canvas.focus();
    fireEvent.wheel(canvas, { deltaY: 1 });
    // run timers set by wheel to avoid side effects to the next test
    expect(props.onZoom).toHaveBeenCalledTimes(1);
  });

  const renderTimeSliderLegendAndClickHome = (
    props: TimeSliderLegendProps,
  ): void => {
    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderLegend {...props} />
      </CoreThemeProvider>,
    );
    expect(container).toBeTruthy();
    const canvas = container.querySelector('canvas');
    canvas.focus();

    fireEvent.keyDown(canvas, {
      key: 'Home',
      code: 'Home',
      keyCode: 36,
    });
  };

  it('should call onSetNewDate and onSetCenterTime with correct parameters when no data is available in the future and home button is pressed', () => {
    const props2: TimeSliderLegendProps = {
      ...props,
      dataStartTime: moment.utc(date).startOf('day').unix(),
      dataEndTime: moment.utc(date).startOf('day').hours(10).unix(),
    };

    renderTimeSliderLegendAndClickHome(props2);

    expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
    expect(props.onSetNewDate).toHaveBeenCalledWith(
      moment.utc(props2.dataEndTime * 1000).toISOString(),
    );
    expect(props.onSetCenterTime).toHaveBeenCalledTimes(1);
    expect(props.onSetCenterTime).toHaveBeenCalledWith(props2.dataEndTime);
  });

  it('should call onSetNewDate and onSetCenterTime with correct parameters when observation and forecast data are available and home button is pressed', () => {
    const props2: TimeSliderLegendProps = {
      ...props,
      dataStartTime: moment.utc(date).startOf('day').unix(),
      dataEndTime: moment.utc(date).endOf('day').unix(),
    };

    renderTimeSliderLegendAndClickHome(props2);

    expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
    expect(props.onSetNewDate).toHaveBeenCalledWith(
      moment
        .utc(roundWithTimeStep(props.currentTime, props.timeStep) * 1000)
        .toISOString(),
    );
    expect(props.onSetCenterTime).toHaveBeenCalledTimes(1);
    expect(props.onSetCenterTime).toHaveBeenCalledWith(
      roundWithTimeStep(props.currentTime, props.timeStep),
    );
  });

  it('should call onSetNewDate and onSetCenterTime with correct parameters when no data is available in the past and home button is pressed', () => {
    const props2: TimeSliderLegendProps = {
      ...props,
      dataStartTime: moment.utc(date).startOf('day').hours(14).unix(),
      dataEndTime: moment.utc(date).endOf('day').unix(),
    };

    renderTimeSliderLegendAndClickHome(props2);

    expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
    expect(props.onSetNewDate).toHaveBeenCalledWith(
      moment.utc(props2.dataStartTime * 1000).toISOString(),
    );
    expect(props.onSetCenterTime).toHaveBeenCalledTimes(1);
    expect(props.onSetCenterTime).toHaveBeenCalledWith(props2.dataStartTime);
  });

  it('should not call onSetNewDate and onSetCenterTime when map is inactive and home button is pressed', () => {
    const props2: TimeSliderLegendProps = {
      ...props,
      dataStartTime: moment.utc(date).startOf('day').unix(),
      dataEndTime: moment.utc(date).startOf('day').hours(10).unix(),
      mapIsActive: false,
    };

    renderTimeSliderLegendAndClickHome(props2);

    expect(props.onSetNewDate).not.toBeCalled();
    expect(props.onSetCenterTime).not.toBeCalled();
  });

  it('should follow selected time when hovering mouse on the legend while not changing center time', () => {
    const props = {
      scale: 5,
      timeStep: 5,
      centerTime: moment.utc(date).startOf('day').hours(12).unix(),
      currentTime: moment.utc(date).startOf('day').hours(12).unix(),
      selectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      dataStartTime: moment.utc(date).startOf('day').unix(),
      dataEndTime: moment.utc(date).endOf('day').unix(),
      isTimeSliderHoverOn: true,
      secondsPerPx: 30,
      onZoom: jest.fn(),
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
    };

    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderLegend {...props} />
      </CoreThemeProvider>,
    );
    expect(container).toBeTruthy();

    const canvas = container.querySelector('canvas');
    expect(canvas).toBeTruthy();

    canvas.focus();
    fireEvent.mouseMove(canvas);

    // When hover is on, time instant should get selected by just moving mouse on legend
    expect(props.onSetNewDate).toHaveBeenCalled();

    // Center time should stay intact
    expect(props.onSetCenterTime).not.toHaveBeenCalled();
  });

  it('should not do any time selections at all when just moving mouse on the legend with no hover on', () => {
    const props = {
      scale: 5,
      timeStep: 5,
      centerTime: moment.utc(date).startOf('day').hours(12).unix(),
      currentTime: moment.utc(date).startOf('day').hours(12).unix(),
      selectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      dataStartTime: moment.utc(date).startOf('day').unix(),
      dataEndTime: moment.utc(date).endOf('day').unix(),
      isTimeSliderHoverOn: false,
      secondsPerPx: 30,
      onZoom: jest.fn(),
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
    };

    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderLegend {...props} />
      </CoreThemeProvider>,
    );

    expect(container).toBeTruthy();

    const canvas = container.querySelector('canvas');
    expect(canvas).toBeTruthy();

    canvas.focus();
    fireEvent.mouseMove(canvas);

    expect(props.onSetNewDate).not.toHaveBeenCalled();
    expect(props.onSetCenterTime).not.toHaveBeenCalled();
  });

  it('should move legend only (changing center time, not selected time) when mouse is dragged far away from needle', async () => {
    const props = {
      scale: 5,
      timeStep: 5,
      centerTime: moment.utc(date).startOf('day').hours(12).unix(),
      currentTime: moment.utc(date).startOf('day').hours(12).unix(),
      selectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      dataStartTime: moment.utc(date).startOf('day').unix(),
      dataEndTime: moment.utc(date).endOf('day').unix(),
      isTimeSliderHoverOn: false,
      secondsPerPx: 30,
      onZoom: jest.fn(),
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
    };

    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderLegend {...props} />
      </CoreThemeProvider>,
    );
    expect(container).toBeTruthy();

    const canvas = container.getElementsByTagName('canvas')[0];
    expect(canvas).toBeTruthy();
    canvas.focus();

    fireEvent.mouseDown(canvas, {
      x: 0,
      y: 0,
    });

    fireEvent.mouseMove(canvas, {
      buttons: 1, // Dragging ~ mouse down while mouse moving
      x: 0,
      y: 0,
      width: canvas.width,
    });

    expect(props.onSetCenterTime).toHaveBeenCalled();
    expect(props.onSetNewDate).not.toHaveBeenCalled();
  });
});
