/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import { Scale } from '../../../store/mapStore/types';

import { getRoundedStartAndEnd } from './TimeSliderLegendRenderFunctions';

describe('src/components/TimeSlider/TimeSliderLegendRenderFunctions', () => {
  describe('getRoundedStartAndEnd', () => {
    it('should round the times up/down to nearest 5 minutes', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:27:27').unix();
      const endTimeVisible = moment.utc('2021-05-05 12:51:27').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Minutes5,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 12:25:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-05 12:55:00').unix());
    });

    it('should round the times up/down to nearest 3 hours', () => {
      const startTimeVisible = moment.utc('2021-05-05 13:27:27').unix();
      const endTimeVisible = moment.utc('2021-05-05 14:27:27').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Hours3,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 12:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-05 15:00:00').unix());
    });

    it('should round the times up/down to nearest 6 hours', () => {
      const startTimeVisible = moment.utc('2021-05-05 17:27:27').unix();
      const endTimeVisible = moment.utc('2021-05-05 20:27:27').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Hours6,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 12:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-06 00:00:00').unix());
    });

    it('should not round if the times are already correct multiples', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:00:00').unix();
      const endTimeVisible = moment.utc('2021-05-05 15:00:00').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Hours3,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 12:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-05 15:00:00').unix());
    });

    it('should round the to up/down by one hour', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:15:00').unix();
      const endTimeVisible = moment.utc('2021-05-05 12:45:00').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Hour,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 12:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-05 13:00:00').unix());
    });

    it('should round up/down by one day', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:00:00').unix();
      const endTimeVisible = moment.utc('2021-05-05 13:00:00').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Day,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-05 00:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-06 00:00:00').unix());
    });

    it('should round up/down by one week', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:00:00').unix();
      const endTimeVisible = moment.utc('2021-05-08 12:00:00').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Week,
      );
      expect(roundedStart).toEqual(moment.utc('2021-05-03 00:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2021-05-10 00:00:00').unix());
    });

    it('should round up/down by one year', () => {
      const startTimeVisible = moment.utc('2021-05-05 12:00:00').unix();
      const endTimeVisible = moment.utc('2021-10-05 12:00:00').unix();
      const [roundedStart, roundedEnd] = getRoundedStartAndEnd(
        startTimeVisible,
        endTimeVisible,
        Scale.Year,
      );
      expect(roundedStart).toEqual(moment.utc('2021-01-01 00:00:00').unix());
      expect(roundedEnd).toEqual(moment.utc('2022-01-01 00:00:00').unix());
    });
  });
});
