/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import moment from 'moment';
import matchMediaPolyfill from 'mq-polyfill';

import TimeSliderScaleSlider from './TimeSliderScaleSlider';
import { Scale } from '../../../store/mapStore/types';

import { multiDimensionLayer3 } from '../../../utils/defaultTestSettings';
import { CoreThemeProvider } from '../../Providers/Providers';

describe('src/components/TimeSlider/TimeSliderScaleSlider', () => {
  const props = {
    timeSliderScale: Scale.Hour,
    centerTime: moment.utc('2021-01-01 12:00:00').unix(),
    secondsPerPx: 60,
    selectedTime: moment.utc('2021-01-01 12:15:00').unix(),
    onChangeTimeSliderScale: jest.fn(),
    layers: [multiDimensionLayer3],
  };

  it('should at start in tests render the component with a scale text', () => {
    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderScaleSlider {...props} />
      </CoreThemeProvider>,
    );

    const componentText = container.querySelector('div').textContent;
    expect(componentText).toContain('1 h scale');
  });

  it('should render the component with a text, when a width is 585', () => {
    matchMediaPolyfill(window);
    window.resizeTo = function resizeTo(width, height): void {
      Object.assign(this, {
        innerWidth: width,
        innerHeight: height,
        outerWidth: width,
        outerHeight: height,
      }).dispatchEvent(new this.Event('resize'));
    };
    window.resizeTo(585, 50);
    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderScaleSlider {...props} />
      </CoreThemeProvider>,
    );

    const componentText = container.querySelector('div').textContent;
    expect(componentText).toContain('1 h scale');
  });
  it('should render the component with a texttext, when a width is 584', () => {
    matchMediaPolyfill(window);
    window.resizeTo = function resizeTo(width, height): void {
      Object.assign(this, {
        innerWidth: width,
        innerHeight: height,
        outerWidth: width,
        outerHeight: height,
      }).dispatchEvent(new this.Event('resize'));
    };
    window.resizeTo(584, 50);
    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderScaleSlider {...props} />
      </CoreThemeProvider>,
    );

    const componentText = container.querySelector('div').textContent;
    expect(componentText).toContain('1 h scale');
  });

  it('should render a slider as enabled', () => {
    const { getByRole, getByTestId } = render(
      <CoreThemeProvider>
        <TimeSliderScaleSlider {...props} />,
      </CoreThemeProvider>,
    );
    expect(getByRole('slider')).toBeTruthy();

    // slider should be enabled
    expect(
      getByTestId('scaleSlider').classList.contains('Mui-disabled'),
    ).toBeFalsy();
  });

  it('should render the slider correctly with 9 marks and the second value also as active', () => {
    const { container } = render(
      <CoreThemeProvider>
        <TimeSliderScaleSlider {...props} />
      </CoreThemeProvider>,
    );

    const marks = container.getElementsByClassName('MuiSlider-mark');
    expect(marks.length === 9).toBeTruthy();
    // Confirms that the value (1 h) is marked active
    expect(marks[1].classList).toContain('MuiSlider-markActive');
  });

  it('handleChange is called to increase/decrease scale', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <TimeSliderScaleSlider {...props} />
      </CoreThemeProvider>,
    );
    const slider = getByTestId('scaleSliderSlider');
    expect(slider).toBeTruthy();
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientX: 1 });
    fireEvent.mouseUp(slider);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledWith(
      Scale.DataScale,
      expect.anything(),
      expect.anything(),
    );

    fireEvent.mouseDown(slider, { clientX: 0 });
    fireEvent.mouseUp(slider);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledTimes(2);
    expect(props.onChangeTimeSliderScale).toHaveBeenLastCalledWith(
      Scale.Minutes5,
      expect.anything(),
      expect.anything(),
    );
  });

  it('scale slider can be correctly called with other scale value than 5', async () => {
    const props2 = {
      mapId: 'map_id',
      timeSliderScale: Scale.Week,
      centerTime: moment.utc('2021-01-01 12:00:00').unix(),
      secondsPerPx: 60,
      selectedTime: moment.utc('2021-01-01 12:15:00').unix(),
      onChangeTimeSliderScale: jest.fn(),
    };
    const { getByTestId } = render(
      <CoreThemeProvider>
        <TimeSliderScaleSlider {...props2} />
      </CoreThemeProvider>,
    );
    const slider = getByTestId('scaleSliderSlider');
    expect(slider).toBeTruthy();

    const marks = slider.getElementsByClassName('MuiSlider-mark');
    // Confirms that the value (week) is marked active
    expect(marks[5].classList).toContain('MuiSlider-markActive');

    const componentText = getByTestId('scaleSliderText').textContent;
    expect(componentText).toContain('week');
  });
  it('scale slider value can be changed with an arrow right key', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <TimeSliderScaleSlider {...props} />
      </CoreThemeProvider>,
    );
    const slider = getByTestId('scaleSliderSlider');

    expect(slider).toBeTruthy();
    const thumb = getByTestId('scaleSliderSliderThumb');
    expect(thumb).toBeTruthy();
    thumb.focus();
    fireEvent.keyDown(thumb, { key: 'ArrowRight', code: 'ArrowRight' });
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledWith(
      Scale.Hours3,
      expect.anything(),
      90,
    );
  });

  it('scale slider value should not go above max with arrow right key', async () => {
    const testProps = {
      ...props,
      timeSliderScale: Scale.DataScale,
    };
    const { getByTestId } = render(
      <CoreThemeProvider>
        <TimeSliderScaleSlider {...testProps} />
      </CoreThemeProvider>,
    );
    const slider = getByTestId('scaleSliderSlider');

    expect(slider).toBeTruthy();
    const thumb = getByTestId('scaleSliderSliderThumb');
    expect(thumb).toBeTruthy();
    thumb.focus();
    fireEvent.keyDown(thumb, { key: 'ArrowRight', code: 'ArrowRight' });
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledWith(
      Scale.DataScale,
      expect.anything(),
      expect.anything(),
    );
  });

  it('scale slider value can be changed with an arrow left key', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <TimeSliderScaleSlider {...props} />
      </CoreThemeProvider>,
    );
    const slider = getByTestId('scaleSliderSlider');

    expect(slider).toBeTruthy();
    const thumb = getByTestId('scaleSliderSliderThumb');
    expect(thumb).toBeTruthy();
    thumb.focus();
    fireEvent.keyDown(thumb, { key: 'ArrowLeft', code: 'ArrowLeft' });
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledWith(
      Scale.Minutes5,
      expect.anything(),
      2.5,
    );
  });

  it('scale slider value should not go below min with arrow left key', async () => {
    const testProps = {
      ...props,
      timeSliderScale: Scale.Minutes5,
    };
    const { getByTestId } = render(
      <CoreThemeProvider>
        <TimeSliderScaleSlider {...testProps} />
      </CoreThemeProvider>,
    );
    const slider = getByTestId('scaleSliderSlider');

    expect(slider).toBeTruthy();
    const thumb = getByTestId('scaleSliderSliderThumb');
    expect(thumb).toBeTruthy();
    thumb.focus();
    fireEvent.keyDown(thumb, { key: 'ArrowLeft', code: 'ArrowLeft' });
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeSliderScale).toHaveBeenCalledWith(
      Scale.Minutes5,
      expect.anything(),
      expect.anything(),
    );
  });
});
