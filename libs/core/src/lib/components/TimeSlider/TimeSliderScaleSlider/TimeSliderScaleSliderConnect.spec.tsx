/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, getByRole } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import TimeSliderScaleSliderConnect, {
  ConnectedScaleSliderProps,
} from './TimeSliderScaleSliderConnect';
import {
  mockStateMapWithTimeSliderScaleWithoutLayers,
  mockStateMapWithLayer,
} from '../../../utils/testUtils';

import { Scale } from '../../../store/mapStore/types';

import { defaultReduxLayerRadarColor } from '../../../utils/defaultTestSettings';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import { mapActions } from '../../../store';

const { setTimeSliderScale } = mapActions;
describe('src/components/TimeSlider/TimeSliderScaleSlider/TimeSliderScaleSliderConnect', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
    layers: defaultReduxLayerRadarColor,
  } as unknown as ConnectedScaleSliderProps;

  it('should render an enabled slider and show a correct scale text', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithTimeSliderScaleWithoutLayers(
      mapId,
      Scale.Hour,
    );
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { container, getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderScaleSliderConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const slider = getByRole(container, 'slider');
    expect(slider).toBeTruthy();

    expect(
      getByTestId('scaleSlider').classList.contains('Mui-disabled'),
    ).toBeFalsy();

    expect(getByTestId('scaleSlider').querySelector('div').textContent).toEqual(
      '1 h scale',
    );
  });

  it('timeSliderScaleSlider renders and should change slider scale in store', async () => {
    const mockStore = configureStore();

    const mockState = mockStateMapWithLayer(defaultReduxLayerRadarColor, mapId);
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderScaleSliderConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const slider = getByTestId('scaleSliderSlider');
    expect(slider).toBeTruthy();
    // a line below moves a scale slider value to max = 10080
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientX: 1 });
    fireEvent.mouseUp(slider);

    const expectedAction = setTimeSliderScale({
      mapId,
      timeSliderScale: Scale.DataScale,
    });

    expect(store.getActions()).toContainEqual(expectedAction);

    // a line below moves a scale value to min = 1
    fireEvent.mouseDown(slider, { clientX: 0 });
    fireEvent.mouseUp(slider);

    const expectedAction2 = setTimeSliderScale({
      mapId,
      timeSliderScale: Scale.DataScale,
    });
    const expectedAction3 = setTimeSliderScale({
      mapId,
      timeSliderScale: Scale.Minutes5,
    });

    expect(store.getActions()).toContainEqual(expectedAction2);
    expect(store.getActions()).toContainEqual(expectedAction3);
  });
});
