/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import { mapActions, mapSelectors } from '../../../store';
import TimeSliderScaleSlider from './TimeSliderScaleSlider';
import { AppStore } from '../../../types/types';
import { Layer, Dimension, Scale } from '../../../store/mapStore/types';
import { getTimeBounds, TimeBounds } from '../TimeSliderUtils';

export interface ConnectedScaleSliderProps {
  timeSliderScale?: Scale;
  mapId: string;
  layers?: Layer[];
  centerTime?: number;
  secondsPerPx?: number;
  dimensions?: Dimension[];
  mapSetTimeSliderScale?: typeof mapActions.setTimeSliderScale;
  mapSetTimeSliderDataScaleToSecondsPerPx?: typeof mapActions.setTimeSliderDataScaleToSecondsPerPx;
  mapSetTimeSliderCenterTime?: typeof mapActions.setTimeSliderCenterTime;
}

const connectReduxTimeSliderScaleSlider = connect(
  (store: AppStore, props: ConnectedScaleSliderProps) => ({
    layers: mapSelectors.getMapLayers(store, props.mapId),
    timeSliderScale: mapSelectors.getMapTimeSliderScale(store, props.mapId),
    centerTime: mapSelectors.getMapTimeSliderCenterTime(store, props.mapId),
    secondsPerPx: mapSelectors.getMapTimeSliderSecondsPerPx(store, props.mapId),
    dimensions: mapSelectors.getMapDimensions(store, props.mapId),
  }),
  {
    mapSetTimeSliderScale: mapActions.setTimeSliderScale,
    mapSetTimeSliderDataScaleToSecondsPerPx:
      mapActions.setTimeSliderDataScaleToSecondsPerPx,
    mapSetTimeSliderCenterTime: mapActions.setTimeSliderCenterTime,
  },
);

const TimeSliderScaleSliderComponent: React.FC<ConnectedScaleSliderProps> = ({
  timeSliderScale,
  mapId,
  layers,
  centerTime,
  secondsPerPx,
  dimensions,
  mapSetTimeSliderScale,
  mapSetTimeSliderDataScaleToSecondsPerPx,
  mapSetTimeSliderCenterTime,
}: ConnectedScaleSliderProps) => {
  const onSetTimeSliderScale = (
    scale: Scale,
    newCenterTime: number,
    scaleToSecPerPx: number,
  ): void => {
    if (scale === Scale.DataScale) {
      mapSetTimeSliderDataScaleToSecondsPerPx({
        mapId,
        timeSliderDataScaleToSecondsPerPx: scaleToSecPerPx,
      });
    }
    mapSetTimeSliderScale({
      mapId,
      timeSliderScale: scale,
    });
    mapSetTimeSliderCenterTime({ mapId, timeSliderCenterTime: newCenterTime });
  };

  const { selectedTime }: TimeBounds = getTimeBounds(dimensions);

  return (
    <TimeSliderScaleSlider
      layers={layers}
      timeSliderScale={timeSliderScale}
      onChangeTimeSliderScale={onSetTimeSliderScale}
      secondsPerPx={secondsPerPx}
      centerTime={centerTime}
      selectedTime={selectedTime}
    />
  );
};

/**
 * TimeSliderScaleSlider component connected to the store displaying a scale slider of the time slider
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <TimeSliderScaleSliderConnect mapId={mapId} />```
 */

const TimeSliderScaleSliderConnect = connectReduxTimeSliderScaleSlider(
  TimeSliderScaleSliderComponent,
);

export default TimeSliderScaleSliderConnect;
