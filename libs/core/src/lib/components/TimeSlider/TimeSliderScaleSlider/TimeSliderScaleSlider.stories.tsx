/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch } from 'react-redux';

import { darkTheme, lightTheme } from '@opengeoweb/theme';
import TimeSliderScaleSliderConnect from './TimeSliderScaleSliderConnect';
import { store } from '../../../storybookUtils/store';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import { mapActions } from '../../../store';

export default { title: 'components/TimeSlider/TimeSliderScaleSlider' };

interface TimeSliderScaleDemoConnectProps {
  mapId: string;
}

const TimeSliderScaleConnectDisplay: React.FC<TimeSliderScaleDemoConnectProps> =
  ({ mapId }: TimeSliderScaleDemoConnectProps) => {
    const dispatch = useDispatch();
    dispatch(mapActions.registerMap({ mapId }));
    return <TimeSliderScaleSliderConnect mapId={mapId} />;
  };

export const TimeSliderScaleLightDemo = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store} theme={lightTheme}>
    <TimeSliderScaleConnectDisplay mapId="map_id_1" />
  </CoreThemeStoreProvider>
);

export const TimeSliderScaleDarkDemo = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store} theme={darkTheme}>
    <TimeSliderScaleConnectDisplay mapId="map_id_1" />
  </CoreThemeStoreProvider>
);

TimeSliderScaleLightDemo.storyName =
  'Time Slider Scale Slider Light Theme (takeSnapshot)';
TimeSliderScaleDarkDemo.storyName =
  'Time Slider Scale Slider Dark Theme (takeSnapshot)';
