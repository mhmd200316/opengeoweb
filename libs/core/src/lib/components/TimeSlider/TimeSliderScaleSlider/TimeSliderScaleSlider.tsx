/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Slider, SliderThumb, Theme } from '@mui/material';
import withStyles from '@mui/styles/withStyles';
import makeStyles from '@mui/styles/makeStyles';
import { SliderThumb as SliderThumbIcon } from '@opengeoweb/theme';
import { Layer, Scale } from '../../../store/mapStore/types';

import {
  getNewCenterOfFixedPointZoom,
  scaleToSecondsPerPx,
  getScaleToSecondsPerPxForDataScale,
  getValueFromKeyboardEvent,
} from '../TimeSliderUtils';

const scaleSliderOtherSettings = {
  LEFT_PADDING: '10%',
};

const useStyles = makeStyles((theme: Theme) => ({
  Background: {
    backgroundColor: theme.palette.geowebColors.background.surface,
    width: '229px',
    height: '40px',
    paddingTop: '4px',
    paddingBottom: '7px',
    boxSizing: 'content-box',
  },
  ScaleText: {
    width: '67px',
    height: '15px',
    padding: '0px 10px 0 8px',
    color: theme.palette.geowebColors.typographyAndIcons.text,
    font: `${theme.palette.geowebColors.timeSlider.timeScaleText.fontSize}px Roboto`,
  },
}));

const thumbComponent: React.FC<unknown> = (props) => {
  const { children, ...other } = props;
  return (
    <SliderThumb {...other} data-testid="scaleSliderSliderThumb" tabIndex={0}>
      {children}
      <SliderThumbIcon />
    </SliderThumb>
  );
};

const marks = [
  {
    text: '5 min scale',
    paddingLeft: '2px',
    value: Scale.Minutes5,
  },
  {
    text: '1 h scale',
    paddingLeft: '25px',
    value: Scale.Hour,
  },
  {
    text: '3 h scale',
    paddingLeft: '45px',
    value: Scale.Hours3,
  },
  {
    text: '6 h scale',
    paddingLeft: '65px',
    value: Scale.Hours6,
  },
  {
    text: 'day scale',
    paddingLeft: '80px',
    value: Scale.Day,
  },
  {
    text: 'week scale',
    paddingLeft: '92px',
    value: Scale.Week,
  },
  {
    text: 'month scale',
    paddingLeft: '107px',
    value: Scale.Month,
  },
  {
    text: 'year scale',
    paddingLeft: '130px',
    value: Scale.Year,
  },
  {
    text: 'data scale',
    paddingLeft: '140px',
    value: Scale.DataScale,
  },
];

const valueLabelFormat = (value: number): string => {
  const valueNew = value.toString();
  return valueNew;
};

const GeoWebScaleSlider = withStyles((theme: Theme) => ({
  root: {
    height: 4,
    width: 167,
    margin: `0 0 18px ${scaleSliderOtherSettings.LEFT_PADDING}`,
  },
  thumb: {
    backgroundColor: 'rgba(0,0,0,0)',
    marginTop: 0,
    width: 12,
    height: 12,
    color: theme.palette.geowebColors.timeSlider.timeScalePointer.fill,
    '& svg polygon': {
      filter:
        theme.palette.geowebColors.timeSlider.timeScaleShadowButtonScale.filter,
    },
    '&:before': {
      display: 'none',
    },
  },
  valueLabel: {
    left: '20px',
  },
  rail: {
    color: theme.palette.geowebColors.timeSlider.timeScaleHorizontalScale.fill,
    height: 4,
    width: 170,
    borderRadius: 4.5,
    opacity:
      theme.palette.geowebColors.timeSlider.timeScaleHorizontalScale.opacity,
  },
  mark: {
    backgroundColor:
      theme.palette.geowebColors.timeSlider.timeScaleTimeIndicators.fill,
    height: 6,
    width: 2,
    marginTop: -9,
  },
  markActive: {
    backgroundColor:
      theme.palette.geowebColors.timeSlider.timeScaleTimeIndicatorsActive.fill,
    height: 6,
    width: 2,
    marginTop: -9,
  },
  '@media (max-width: 585px)': {
    root: {
      width: 110,
    },
    rail: {
      width: 110,
    },
  },
  '@media (max-width: 395px)': {
    root: {
      width: 60,
    },
    rail: {
      width: 60,
    },
  },
}))(Slider);

interface TimeSliderScaleSliderProps {
  layers?: Layer[];
  timeSliderScale: Scale;
  centerTime: number;
  secondsPerPx: number;
  selectedTime: number;
  onChangeTimeSliderScale: (
    scale: number,
    newCenterTime: number,
    dataScalePerPx: number,
  ) => void;
}

const RAIL_GUTTER_OFFSET = 0.4;

const TimeSliderScaleSlider: React.FC<TimeSliderScaleSliderProps> = ({
  layers,
  timeSliderScale = Scale.Hour,
  centerTime,
  secondsPerPx,
  selectedTime,
  onChangeTimeSliderScale,
}: TimeSliderScaleSliderProps) => {
  const classes = useStyles();
  const min = 0;
  const max = marks.length - 1;
  const value = marks.findIndex((mark) => mark.value === timeSliderScale);

  const onChangeSliderValue = (newSliderValue: number): void => {
    const newValue = newSliderValue ? Math.round(newSliderValue) : 0;
    const newScale = marks.find((el) => el.value === newValue).value;
    const scaleToSecPerPx =
      newScale === Scale.DataScale
        ? getScaleToSecondsPerPxForDataScale(layers)
        : scaleToSecondsPerPx[newScale];

    const newCenterTime = getNewCenterOfFixedPointZoom(
      selectedTime,
      secondsPerPx,
      scaleToSecPerPx,
      centerTime,
    );
    onChangeTimeSliderScale(newScale, newCenterTime, scaleToSecPerPx);
  };

  const onChange = (event: Event, value: number): void => {
    if (event.type !== 'keydown') {
      onChangeSliderValue(value);
    }
  };

  const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>): void => {
    // disable default Slider behavior
    event.preventDefault();
    event.stopPropagation();
    // custom logic
    const newValue = getValueFromKeyboardEvent(event, value, min, max, true);
    if (newValue !== null) {
      onChangeSliderValue(newValue);
    }
  };

  const { paddingLeft, text } = marks.find((mark) => mark.value === value);

  const scaleTextPadding = {
    paddingLeft,
  };

  return (
    <div className={classes.Background} data-testid="scaleSlider">
      <div>
        <div
          data-testid="scaleSliderText"
          className={classes.ScaleText}
          style={scaleTextPadding}
        >
          {text}
        </div>
      </div>
      <div>
        <GeoWebScaleSlider
          data-testid="scaleSliderSlider"
          defaultValue={Scale.Hour}
          value={value}
          min={min - RAIL_GUTTER_OFFSET}
          step={1}
          max={max + RAIL_GUTTER_OFFSET}
          getAriaValueText={valueLabelFormat}
          onChange={onChange}
          onKeyDown={onKeyDown}
          aria-labelledby="scale-slider"
          components={{ Thumb: thumbComponent }}
          marks={marks}
          track={false}
        />
      </div>
    </div>
  );
};

export default TimeSliderScaleSlider;
