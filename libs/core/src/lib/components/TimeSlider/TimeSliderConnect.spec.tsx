/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import TimeSliderConnect from './TimeSliderConnect';
import { mockStateMapWithLayer } from '../../utils/testUtils';
import {
  defaultReduxLayerRadarKNMI,
  layerWithoutTimeDimension,
} from '../../utils/defaultTestSettings';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { mapActions } from '../../store';

describe('src/components/TimeSlider/TimeSliderConnect', () => {
  it('should render the component when the map has a layer with time dimension', () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('timeSliderButtons')).toBeTruthy();
    expect(getByTestId('timeSliderRail')).toBeTruthy();
    expect(getByTestId('scaleSlider')).toBeTruthy();
    expect(getByTestId('timeSliderLegend')).toBeTruthy();
    expect(getByTestId('timeSlider')).toBeTruthy();
  });

  it('should render the component when the layer on the map does not have a time dimension', () => {
    const mapId = 'mapid_2';
    const layer = layerWithoutTimeDimension;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('timeSliderButtons')).toBeTruthy();
    expect(getByTestId('timeSliderRail')).toBeTruthy();
    expect(getByTestId('scaleSlider')).toBeTruthy();
    expect(getByTestId('timeSliderLegend')).toBeTruthy();
    expect(getByTestId('timeSlider')).toBeTruthy();
  });

  it('should toggle time slider hover when ctrl + alt + h is pressed', async () => {
    const mapId = 'mapid_3';
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </CoreThemeStoreProvider>,
    );
    const timeSlider = getByTestId('timeSlider');
    expect(timeSlider).toBeTruthy();
    timeSlider.focus();

    fireEvent.keyDown(timeSlider, {
      ctrlKey: true,
      altKey: true,
      key: 'h',
      code: 'KeyH',
    });

    const expectedActions = mapActions.toggleTimeSliderHover({
      mapId,
      isTimeSliderHoverOn: true,
    });
    expect(store.getActions()).toContainEqual(expectedActions);
  });
  it('should be visible even when isTimeSliderVisible is true', () => {
    const mapId = 'mapid_3';
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            baseLayers: [],
            mapLayers: [],
            overLayers: [],
            isTimeSliderVisible: true,
          },
        },
        allIds: [mapId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </CoreThemeStoreProvider>,
    );
    const timeSlider = getByTestId('timeSlider');
    expect(timeSlider).toBeTruthy();
  });

  it('should be hidden even when isTimeSliderVisible is false', () => {
    const mapId = 'mapid_3';
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            baseLayers: [],
            mapLayers: [],
            overLayers: [],
            isTimeSliderVisible: false,
          },
        },
        allIds: [mapId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </CoreThemeStoreProvider>,
    );
    const timeSlider = queryByTestId('timeSlider');
    expect(timeSlider).toBeFalsy();
  });

  it('should be visible even when isTimeSliderVisible is false but has isAlwaysVisible as prop', () => {
    const mapId = 'mapid_3';
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            baseLayers: [],
            mapLayers: [],
            overLayers: [],
            isTimeSliderVisible: false,
          },
        },
        allIds: [mapId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <TimeSliderConnect
          sourceId="timeslider-1"
          mapId={mapId}
          isAlwaysVisible
        />
      </CoreThemeStoreProvider>,
    );
    const timeSlider = getByTestId('timeSlider');
    expect(timeSlider).toBeTruthy();
  });
});
