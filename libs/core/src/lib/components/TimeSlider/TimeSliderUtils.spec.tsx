/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import moment from 'moment';
import { renderHook } from '@testing-library/react-hooks';
import React from 'react';
import { render } from '@testing-library/react';
import { DateInterval } from '@opengeoweb/webmap';
import {
  AUTO_MOVE_AREA_PADDING,
  defaultDelay,
  getActiveLayerTimeStep,
  getAutoMoveAreaWidth,
  getNewCenterOfFixedPointZoom,
  getNextTimeStepvalue,
  getPreviousTimeStepvalue,
  getSpeedDelay,
  getSpeedFactor,
  getTimeBounds,
  getTimeStepFromDataInterval,
  getValueFromKeyboardEvent,
  moveRelativeToTimePoint,
  needleGeom,
  pixelToTimestamp,
  scalingCoefficient,
  timestampToPixel,
  timestampToPixelEdges,
  useCanvasTarget,
} from './TimeSliderUtils';
import CanvasComponent from '../CanvasComponent/CanvasComponent';
import { Scale } from '../../store/mapStore/types';

describe('src/components/TimeSlider/TimeSliderUtils', () => {
  describe('getTimeBounds', () => {
    it('should return the proper time bounds', () => {
      const dimensions = [
        {
          name: 'time',
          currentValue: '2020-07-01T10:00:00Z',
          maxValue: '2020-07-01T08:00:00Z',
          minValue: '2020-07-02T10:00:00Z',
        },
        {
          name: 'someOtherDim',
          currentValue: '12',
          maxValue: 'gigantisch',
          minValue: 'anders',
        },
      ];
      expect(getTimeBounds(dimensions)).toStrictEqual({
        selectedTime: moment.utc(dimensions[0].currentValue).unix(),
        startTime: moment.utc(dimensions[0].minValue).unix(),
        endTime: moment.utc(dimensions[0].maxValue).unix(),
      });
    });
    it('should return the default time bounds if no time dimension passed', () => {
      const currentTime = moment
        .utc()
        .set({ second: 0, millisecond: 0 })
        .unix();
      const dimensions = [
        {
          name: 'someOtherDim',
          currentValue: '12',
          maxValue: 'gigantisch',
          minValue: 'anders',
        },
      ];
      expect(getTimeBounds(dimensions)).toStrictEqual({
        selectedTime: moment.utc().unix(),
        startTime: currentTime,
        endTime: currentTime,
      });
    });
  });

  describe('scalingCoefficient', () => {
    const timeStart = moment.utc('2020-07-01T10:00:00Z').unix();
    const timeEnd = moment.utc('2020-07-01T10:10:00Z').unix();
    it('should return the proper time bounds', () => {
      expect(scalingCoefficient(timeEnd, timeStart, 600)).toBe(1);
      expect(scalingCoefficient(timeEnd, timeStart, 30)).toBe(0.05);
    });
  });
  describe('timestampToPixelEdges', () => {
    const timeStamp = moment.utc('2020-07-01T10:05:00Z').unix();
    const timeStart = moment.utc('2020-07-01T10:00:00Z').unix();
    const timeEnd = moment.utc('2020-07-01T10:10:00Z').unix();
    it('should return the proper time bounds', () => {
      expect(timestampToPixelEdges(timeStamp, timeStart, timeEnd, 600)).toBe(
        300,
      );
      expect(
        timestampToPixelEdges(
          moment.utc('2020-07-01T10:02:00Z').unix(),
          timeStart,
          timeEnd,
          30,
        ),
      ).toBe(6);
    });
  });

  describe('timestampToPixel', () => {
    const centerTime = moment.utc('2020-07-01T10:00:00Z').unix();
    it('should return the proper pixel position', () => {
      expect(
        timestampToPixel(
          moment.utc('2020-07-01T09:50:00Z').unix(),
          centerTime,
          1000,
          10,
        ),
      ).toBe(440);
      expect(
        timestampToPixel(
          moment.utc('2020-07-01T10:15:00Z').unix(),
          centerTime,
          1000,
          10,
        ),
      ).toBe(590);
    });
  });

  describe('pixelToTimestamp', () => {
    const centerTime = moment.utc('2020-07-01T10:00:00Z').unix();
    it('should return the proper timestamp', () => {
      expect(pixelToTimestamp(620, centerTime, 1000, 10)).toBe(
        moment.utc('2020-07-01T10:20:00Z').unix(),
      );
      expect(pixelToTimestamp(560, centerTime, 1000, 10)).toBe(
        moment.utc('2020-07-01T10:10:00Z').unix(),
      );
    });
  });

  // Test inverse with 10 random numbers
  const centerTime = moment.utc('2020-07-01T10:00:00Z').unix();
  const testPixelWidths = new Array(10)
    .fill(0)
    // eslint-disable-next-line no-unused-vars
    .map((_) => Math.random() * 1000);
  describe.each(testPixelWidths)(
    'timestampToPixel(pixelToTimestamp(%d, ...), ...)',
    (t) => {
      it(`should return ${t}`, () => {
        expect(
          timestampToPixel(
            pixelToTimestamp(t, centerTime, 1000, 10),
            centerTime,
            1000,
            10,
          ),
        ).toBeCloseTo(t);
      });
    },
  );

  describe('getNewCenterOfFixedPointZoom', () => {
    it('should return new center time', () => {
      const secondsPerPxStart = 2;
      const secondsPerPxEnd = 1;
      const fixedTimePoint = moment.utc('2021-01-01 18:00:00').unix();
      const oldCenterTime = moment.utc('2021-01-01 12:00:00').unix();
      const newCenterTime = getNewCenterOfFixedPointZoom(
        fixedTimePoint,
        secondsPerPxStart,
        secondsPerPxEnd,
        oldCenterTime,
      );
      expect(newCenterTime).toEqual(moment.utc('2021-01-01 15:00:00').unix());
    });

    it('should keep the fixed point at same pixel location', () => {
      const canvasWidth = 1000;
      const fixedPointPx = 750;
      const secondsPerPxStart = 10;
      const secondsPerPxEnd = 17;
      const oldCenterTime = moment.utc('2021-01-01 12:00:00').unix();

      const fixedPointUnix = pixelToTimestamp(
        fixedPointPx,
        oldCenterTime,
        canvasWidth,
        secondsPerPxStart,
      );
      const newCenterTime = getNewCenterOfFixedPointZoom(
        fixedPointUnix,
        secondsPerPxStart,
        secondsPerPxEnd,
        oldCenterTime,
      );
      const zoomedFixedPointPx = timestampToPixel(
        fixedPointUnix,
        newCenterTime,
        canvasWidth,
        secondsPerPxEnd,
      );
      expect(zoomedFixedPointPx).toEqual(fixedPointPx);
    });
  });

  describe('custom hook "useCanvasTarget"', () => {
    it('should not be a hooked component if ref does not exist on component', () => {
      const { result } = renderHook(() => useCanvasTarget('mousedown'));
      expect(result.current).toEqual([false, { current: null }]);
    });

    it('should use "useCallback" hook', () => {
      const useCallbackSpy = jest
        .spyOn(React, 'useCallback')
        .mockReturnValueOnce(expect.any(Function));

      const { result } = renderHook(() => useCanvasTarget('mousedown'));
      expect(useCallbackSpy).toBeCalledTimes(1);
      expect(result.current).toEqual([false, { current: null }]);
    });

    it('should use "useRef" hook', () => {
      const useRefSpy = jest
        .spyOn(React, 'useRef')
        .mockReturnValueOnce({ current: null });

      renderHook(() => useCanvasTarget('mousedown'));

      expect(useRefSpy).toBeCalledTimes(1);
      expect(useRefSpy).toBeCalledWith(null);
    });

    it('should be called with "mousedown" event when adding document listener', () => {
      jest.spyOn(document, 'addEventListener');

      renderHook(() => useCanvasTarget('mousedown'));
      expect(document.addEventListener).toBeCalled();
      expect(document.addEventListener).toBeCalledWith(
        'mousedown',
        expect.any(Function),
      );
    });

    it('should remove window listener', () => {
      jest.spyOn(window, 'removeEventListener');

      renderHook(() => useCanvasTarget('mousedown'));
      expect(window.removeEventListener).toBeCalled();
    });

    it('should be able to render canvas component', () => {
      let testRef;

      const TestComponent: React.FC = () => {
        const [isAllowedCanvasNodePointed, nodeRef] =
          useCanvasTarget('mousedown');
        testRef = nodeRef;

        return isAllowedCanvasNodePointed && nodeRef && nodeRef.current ? (
          <CanvasComponent ref={nodeRef}>Component with ref</CanvasComponent>
        ) : (
          <CanvasComponent>Component with NO ref</CanvasComponent>
        );
      };

      const { container } = render(<TestComponent />);
      const canvas = container.getElementsByTagName('canvas')[0];
      expect(canvas).toBeDefined();
      expect(testRef.current).toBeNull();

      const { queryByText } = render(<TestComponent />);
      expect(queryByText('Component with ref')).toBeFalsy();
    });
  });

  describe('moveRelativeToTimePoint', () => {
    it('should move the given time point to given position', () => {
      const timePoint = moment.utc('2021-01-01 18:00:00').unix();
      const secondsPerPx = 10;
      const canvasWidth = 1200;
      const timePointPx = 1200 / 4;
      const newCenterTime = moveRelativeToTimePoint(
        timePoint,
        timePointPx,
        secondsPerPx,
        canvasWidth,
      );
      expect(newCenterTime).toEqual(moment.utc('2021-01-01 18:50:00').unix());
      expect(
        timestampToPixel(timePoint, newCenterTime, canvasWidth, secondsPerPx),
      ).toBeCloseTo(timePointPx);
    });
  });

  describe('getNextTimeStepvalue', () => {
    it('should get next timestep value', () => {
      expect(getNextTimeStepvalue(0, 10)).toEqual(1);
      expect(getNextTimeStepvalue(9, 10)).toEqual(10);
      expect(getNextTimeStepvalue(10, 10)).toEqual(10);
      expect(getNextTimeStepvalue(11, 10)).toEqual(10);
    });
  });

  describe('getPreviousTimeStepvalue', () => {
    it('should get previous timestep value', () => {
      expect(getPreviousTimeStepvalue(10, 0)).toEqual(9);
      expect(getPreviousTimeStepvalue(9, 0)).toEqual(8);
      expect(getPreviousTimeStepvalue(0, 0)).toEqual(0);
      expect(getPreviousTimeStepvalue(-1, 0)).toEqual(0);
    });
  });

  describe('getValueFromKeyboardEvent', () => {
    it('return correct value when ctrl or cmd and ArrowDown are pressed', () => {
      const fakeEvent = {
        ctrlKey: true,
        key: 'ArrowDown',
      } as React.KeyboardEvent<HTMLInputElement>;

      expect(getValueFromKeyboardEvent(fakeEvent, 10, 0, 10)).toEqual(9);
      expect(getValueFromKeyboardEvent(fakeEvent, 9, 0, 10)).toEqual(8);
      expect(getValueFromKeyboardEvent(fakeEvent, 0, 0, 10)).toEqual(0);
      expect(getValueFromKeyboardEvent(fakeEvent, -1, 0, 10)).toEqual(0);
    });
    it('return correct value when ctrl or cmd and ArrowUp are pressed', () => {
      const fakeEvent = {
        ctrlKey: true,
        key: 'ArrowUp',
      } as React.KeyboardEvent<HTMLInputElement>;

      expect(getValueFromKeyboardEvent(fakeEvent, 0, 0, 10)).toEqual(1);
      expect(getValueFromKeyboardEvent(fakeEvent, 9, 0, 10)).toEqual(10);
      expect(getValueFromKeyboardEvent(fakeEvent, 10, 0, 10)).toEqual(10);
      expect(getValueFromKeyboardEvent(fakeEvent, 11, 0, 10)).toEqual(10);
    });
    it('return return null when no valid keys are pressed', () => {
      const fakeEvent = {
        ctrlKey: true,
      } as React.KeyboardEvent<HTMLInputElement>;

      expect(getValueFromKeyboardEvent(fakeEvent, 0, 0, 10)).toBeNull();
      expect(getValueFromKeyboardEvent(fakeEvent, 9, 0, 10)).toBeNull();
      expect(getValueFromKeyboardEvent(fakeEvent, 10, 0, 10)).toBeNull();
      expect(getValueFromKeyboardEvent(fakeEvent, 11, 0, 10)).toBeNull();
    });
    it('should return previous value for ArrowDown and ArrowLeft key', () => {
      const arrowDownEvent = {
        key: 'ArrowDown',
      } as React.KeyboardEvent<HTMLInputElement>;
      const arrowLeftEvent = {
        key: 'ArrowLeft',
      } as React.KeyboardEvent<HTMLInputElement>;

      expect(
        getValueFromKeyboardEvent(arrowDownEvent, 10, 0, 10, true),
      ).toEqual(9);
      expect(getValueFromKeyboardEvent(arrowDownEvent, 0, 0, 10, true)).toEqual(
        0,
      );
      expect(
        getValueFromKeyboardEvent(arrowLeftEvent, 10, 0, 10, true),
      ).toEqual(9);
      expect(getValueFromKeyboardEvent(arrowLeftEvent, 0, 0, 10, true)).toEqual(
        0,
      );
    });
    it('should return next value for ArrowUp and ArrowRight key', () => {
      const arrowUpEvent = {
        key: 'ArrowUp',
      } as React.KeyboardEvent<HTMLInputElement>;
      const arrowRightEvent = {
        key: 'ArrowRight',
      } as React.KeyboardEvent<HTMLInputElement>;

      expect(getValueFromKeyboardEvent(arrowUpEvent, 10, 0, 10, true)).toEqual(
        10,
      );
      expect(getValueFromKeyboardEvent(arrowUpEvent, 0, 0, 10, true)).toEqual(
        1,
      );
      expect(
        getValueFromKeyboardEvent(arrowRightEvent, 10, 0, 10, true),
      ).toEqual(10);
      expect(
        getValueFromKeyboardEvent(arrowRightEvent, 0, 0, 10, true),
      ).toEqual(1);
    });
  });

  describe('getAutoMoveAreaWidth', () => {
    const smallWidth = needleGeom.smallWidth / 2 + AUTO_MOVE_AREA_PADDING;
    const largeWidth = needleGeom.largeWidth / 2 + AUTO_MOVE_AREA_PADDING;
    it('should return small width for a non-year scale', () => {
      expect(getAutoMoveAreaWidth(Scale.Day)).toEqual(smallWidth);
    });
    it('should return large width for year scale', () => {
      expect(getAutoMoveAreaWidth(Scale.Year)).toEqual(largeWidth);
    });
  });

  const ti1 = new DateInterval('1', '0', '0', '0', '0', '0');
  const ti2 = new DateInterval('0', '1', '0', '0', '0', '0');
  const ti3 = new DateInterval('0', '0', '1', '0', '0', '0');
  const ti4 = new DateInterval('0', '0', '0', '1', '0', '0');
  const ti5 = new DateInterval('0', '0', '0', '0', '1', '0');
  const ti6 = new DateInterval('0', '0', '0', '0', '0', '1');
  describe.each([
    [ti1, 365 * 24 * 60],
    [ti2, 30 * 24 * 60],
    [ti3, 24 * 60],
    [ti4, 60],
    [ti5, 1],
    [ti6, 1 / 60],
  ])('getTimeStepFromDataInterval', (interval, expected) => {
    it(`should return ${expected} for ${JSON.stringify(interval)}`, () => {
      expect(getTimeStepFromDataInterval(interval)).toBeCloseTo(expected);
    });
  });

  describe('getActiveLayerTimeStep', () => {
    it('should return truthy value for time dimension with time interval', () => {
      expect(
        getActiveLayerTimeStep({ currentValue: '', timeInterval: ti1 }),
      ).toBeTruthy();
    });
    it('should return falsy value for time dimension without time interval', () => {
      expect(getActiveLayerTimeStep({ currentValue: '' })).toBeFalsy();
    });
  });

  describe('getSpeedDelay', () => {
    it('should return correct delay for the passed speedFactor', () => {
      expect(getSpeedDelay(0.1)).toEqual(defaultDelay / 0.1);
      expect(getSpeedDelay(0.2)).toEqual(defaultDelay / 0.2);
      expect(getSpeedDelay(0.5)).toEqual(defaultDelay / 0.5);
      expect(getSpeedDelay(1)).toEqual(defaultDelay / 1);
      expect(getSpeedDelay(2)).toEqual(defaultDelay / 2);
      expect(getSpeedDelay(4)).toEqual(defaultDelay / 4);
      expect(getSpeedDelay(8)).toEqual(defaultDelay / 8);
      expect(getSpeedDelay(16)).toEqual(defaultDelay / 16);
    });
  });
});

describe('getSpeedFactor', () => {
  it('should return correct speedFactor for the passed delay', () => {
    expect(getSpeedFactor(62.5)).toEqual(16);
    expect(getSpeedFactor(125)).toEqual(8);
    expect(getSpeedFactor(250)).toEqual(4);
    expect(getSpeedFactor(500)).toEqual(2);
    expect(getSpeedFactor(1000)).toEqual(1);
    expect(getSpeedFactor(2000)).toEqual(0.5);
    expect(getSpeedFactor(5000)).toEqual(0.2);
    expect(getSpeedFactor(10000)).toEqual(0.1);
  });
});
