/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { store } from '../../storybookUtils/store';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { MapViewConnect } from '../MapView';
import { radarLayer, baseLayerGrey, overLayer } from '../../utils/publicLayers';
import TimeSliderConnect from './TimeSliderConnect';

import { LegendConnect, LegendMapButtonConnect } from '../Legend';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { MapControls } from '../MapControls';

export default { title: 'components/TimeSlider/TimeSliderConnect' };

interface ExampleComponentProps {
  mapId: string;
}

const ExampleComponent: React.FC<ExampleComponentProps> = ({
  mapId,
}: ExampleComponentProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [
      {
        ...radarLayer,
        id: `radar-${mapId}`,
      },
    ],
    baseLayers: [
      {
        ...baseLayerGrey,
        id: `baseGrey-${mapId}`,
      },
      overLayer,
    ],
  });
  return (
    <div style={{ height: '100vh' }}>
      <LegendConnect mapId={mapId} />
      <MapControls>
        <LegendMapButtonConnect mapId={mapId} />
      </MapControls>
      <div style={{ height: '100vh' }}>
        <MapViewConnect mapId={mapId} />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 10,
          right: '0px',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
    </div>
  );
};

export const DemoTimeSliderConnect = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <ExampleComponent mapId="mapid_1" />
  </CoreThemeStoreProvider>
);

DemoTimeSliderConnect.storyName = 'Time Slider Connect';
