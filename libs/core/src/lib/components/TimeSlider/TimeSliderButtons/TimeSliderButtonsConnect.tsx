/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import AutoUpdateButtonConnect from './AutoUpdateButton/AutoUpdateButtonConnect';
import OptionsMenuButtonConnect from './OptionsMenuButton/OptionsMenuButtonConnect';
import PlayButtonConnect from './PlayButton/PlayButtonConnect';
import SpeedButtonConnect from './SpeedButton/SpeedButtonConnect';
import TimeSliderButtons from './TimeSliderButtons';
import TimeStepButtonConnect from './TimeStepButton/TimeStepButtonConnect';

interface TimeSliderButtonsConnectProps {
  mapId: string;
}

/**
 * TimeSliderButtons component with components connected to the store displaying the time slider buttons
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <TimeSliderButtonsConnect mapId={mapId} />```
 */

const TimeSliderButtonsConnect: React.FC<TimeSliderButtonsConnectProps> = ({
  mapId,
}: TimeSliderButtonsConnectProps) => (
  <TimeSliderButtons
    autoUpdateBtn={<AutoUpdateButtonConnect mapId={mapId} />}
    optionsMenuBtn={<OptionsMenuButtonConnect mapId={mapId} />}
    playBtn={<PlayButtonConnect mapId={mapId} />}
    speedBtn={<SpeedButtonConnect mapId={mapId} />}
    timeStepBtn={<TimeStepButtonConnect mapId={mapId} />}
  />
);

export default TimeSliderButtonsConnect;
