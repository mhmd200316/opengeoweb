/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Popover, Grid, Tooltip, Theme } from '@mui/material';
import withStyles from '@mui/styles/withStyles';

import { ToolButton } from '@opengeoweb/shared';
import { Options } from '@opengeoweb/theme';
import AutoUpdateButton from '../AutoUpdateButton/AutoUpdateButton';
import SpeedButton from '../SpeedButton/SpeedButton';
import TimeStepButton from '../TimeStepButton/TimeStepButton';
import { defaultAnimationDelayAtStart } from '../../TimeSliderUtils';

const OptionsPopOver = withStyles((theme: Theme) => ({
  paper: {
    height: '24px',
    padding: '8px',
    borderRadius: '0',
    overflow: 'visible',
    backgroundColor: theme.palette.geowebColors.background.surface,
  },
}))(Popover);

interface OptionsMenuButtonProps {
  autoUpdateBtn?: React.ReactChild;
  speedBtn?: React.ReactChild;
  timeStepBtn?: React.ReactChild;
}

const OptionsMenuButton: React.FC<OptionsMenuButtonProps> = ({
  autoUpdateBtn,
  speedBtn,
  timeStepBtn,
}: OptionsMenuButtonProps) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const onClickButton = (event): void => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <div>
      <Tooltip title="Options">
        <ToolButton
          onClick={onClickButton}
          data-testid="optionsMenuButton"
          active={open}
        >
          <Options data-testid="optionsIcon" />
        </ToolButton>
      </Tooltip>
      <OptionsPopOver
        id={id}
        open={open}
        data-testid="optionsMenuPopOver"
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <Grid container spacing={1}>
          <Grid item xs="auto">
            {timeStepBtn || (
              <TimeStepButton
                timeStep={0}
                onChangeTimeStep={(): void => null}
              />
            )}
          </Grid>
          <Grid item xs="auto">
            {speedBtn || (
              <SpeedButton
                animationDelay={defaultAnimationDelayAtStart}
                setMapAnimationDelay={(): void => null}
              />
            )}
          </Grid>
          <Grid item xs="auto">
            {autoUpdateBtn || <AutoUpdateButton />}
          </Grid>
        </Grid>
      </OptionsPopOver>
    </div>
  );
};

export default OptionsMenuButton;
