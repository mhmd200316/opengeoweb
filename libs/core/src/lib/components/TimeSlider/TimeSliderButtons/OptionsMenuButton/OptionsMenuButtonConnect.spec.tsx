/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import OptionsMenuButtonConnect from './OptionsMenuButtonConnect';
import { mockStateMapWithLayer } from '../../../../utils/testUtils';
import {
  defaultReduxLayerRadarKNMI,
  layerWithoutTimeDimension,
} from '../../../../utils/defaultTestSettings';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';

describe('src/components/TimeSlider/TimeSliderButtons/OptionsMenuButtonConnect', () => {
  it('should render the component when the map has a layer with time dimension', () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <OptionsMenuButtonConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    expect(queryByTestId('optionsMenuButton')).toBeTruthy();
  });

  it('should render the component when the layer on the map does not have a time dimension', () => {
    const mapId = 'mapid_1';
    const layer = layerWithoutTimeDimension;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <OptionsMenuButtonConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    expect(queryByTestId('optionsMenuButton')).toBeTruthy();
  });
});
