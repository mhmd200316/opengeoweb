/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Tooltip } from '@mui/material';
import { ToolButton } from '@opengeoweb/shared';
import { Play, Pause } from '@opengeoweb/theme';

export interface PlayButtonProps {
  isAnimating?: boolean;
  isDisabled?: boolean;
  onTogglePlayButton?: () => void;
}

const PlayButton: React.FC<PlayButtonProps> = ({
  isAnimating,
  isDisabled,
  onTogglePlayButton = (): void => null,
}: PlayButtonProps) => {
  const onTogglePlay = (): void => {
    onTogglePlayButton();
  };

  return (
    <Tooltip title={isAnimating ? 'Pause' : 'Play'}>
      <span>
        <ToolButton
          onClick={onTogglePlay}
          disabled={isDisabled}
          data-testid="playButton"
          active={isAnimating}
        >
          {isAnimating ? (
            <Pause data-testid="pause-svg-path" />
          ) : (
            <Play data-testid="play-svg-path" />
          )}
        </ToolButton>
      </span>
    </Tooltip>
  );
};

export default PlayButton;
