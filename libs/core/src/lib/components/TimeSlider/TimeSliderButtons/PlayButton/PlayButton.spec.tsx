/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { findByRole, fireEvent, render } from '@testing-library/react';
import PlayButton from './PlayButton';
import { CoreThemeProvider } from '../../../Providers/Providers';

describe('src/components/TimeSlider/TimeSliderButtons/PlayButton', () => {
  it('should display a proper Svg path for icon', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <PlayButton />
      </CoreThemeProvider>,
    );

    // Should display two Svg paths for "play" icon by default
    const playButton = getByTestId('play-svg-path');
    expect(playButton).toBeTruthy();
  });

  it('should display a proper Svg path for icon when hovering while animating, that is, when "isAnimating" is false in props', () => {
    const props = {
      mapId: 'map_id1',
      isAnimating: false,
      animationInterval: 300,
    };

    const { getByTestId } = render(
      <CoreThemeProvider>
        <PlayButton {...props} />
      </CoreThemeProvider>,
    );

    // Should display a proper Svg path for "play" icon
    const playButton = getByTestId('play-svg-path');
    expect(playButton).toBeTruthy();
  });

  it('should display a proper Svg path for icon when hovering while animating, that is, when "isAnimating" is true in props', () => {
    const props = {
      mapId: 'map_id1',
      isAnimating: true,
      animationInterval: 300,
    };

    const { getByTestId } = render(
      <CoreThemeProvider>
        <PlayButton {...props} />
      </CoreThemeProvider>,
    );

    // Should display a proper Svg path for "pause" icon
    const pauseButton = getByTestId('pause-svg-path');
    expect(pauseButton).toBeTruthy();
  });

  it('should display a "loop-off" Svg path for icon while animating', () => {
    const props = {
      isAnimating: true,
    };

    const { getByTestId } = render(
      <CoreThemeProvider>
        <PlayButton {...props} />
      </CoreThemeProvider>,
    );

    // Should display "pause" Svg path
    const pauseButton = getByTestId('pause-svg-path');
    expect(pauseButton).toBeTruthy();
  });

  it('should show a title text "Play" in tooltip when hovering while not looping', async () => {
    const { getByTestId, container } = render(
      <CoreThemeProvider>
        <PlayButton />
      </CoreThemeProvider>,
    );

    const playButton = getByTestId('play-svg-path');
    expect(playButton).toBeTruthy();

    fireEvent.mouseOver(playButton);
    // Wait until tooltip appears
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain('Play');
  });

  it('should show a title text "Pause" in tooltip when hovering while animating', async () => {
    const props = {
      isAnimating: true,
      onTogglePlayButton: jest.fn(),
    };
    const { getByTestId, container } = render(
      <CoreThemeProvider>
        <PlayButton {...props} />
      </CoreThemeProvider>,
    );

    const pauseButton = getByTestId('pause-svg-path');
    expect(pauseButton).toBeTruthy();

    fireEvent.mouseOver(pauseButton);
    // Wait until tooltip appears
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain('Pause');
  });

  it('should render button as enabled when it is neither animating nor disabled in props', () => {
    const props = {
      isAnimating: false,
      isDisabled: false,
      onTogglePlayButton: jest.fn(),
    };

    const { getByTestId } = render(
      <CoreThemeProvider>
        <PlayButton {...props} />
      </CoreThemeProvider>,
    );

    // Should display a proper Svg path for "play" icon (default)
    const playButton = getByTestId('play-svg-path');
    expect(playButton).toBeTruthy();

    // An enabled play button should show up
    expect(playButton.classList.contains('Mui-disabled')).toBeFalsy();
  });

  it('should render button as disabled when it is not playing and is disabled in props', async () => {
    const props = {
      isAnimating: false,
      isDisabled: true,
      onTogglePlayButton: jest.fn(),
    };

    const { getByTestId } = render(
      <CoreThemeProvider>
        <PlayButton {...props} />
      </CoreThemeProvider>,
    );

    // Should display "play" icon (default)
    const button = getByTestId('playButton');
    expect(button).toBeTruthy();

    // A disabled play button should show up
    expect(button.classList.contains('Mui-disabled')).toBeTruthy();
  });

  it('should render button as disabled when it is not animating and is not disabled in props', () => {
    const props = {
      isAnimating: false,
      isDisabled: false,
      onTogglePlayButton: jest.fn(),
    };

    const { getByTestId } = render(
      <CoreThemeProvider>
        <PlayButton {...props} />
      </CoreThemeProvider>,
    );

    // A disabled button should show up
    const button = getByTestId('playButton');
    expect(button).toBeTruthy();

    // A disabled play button should show up
    expect(button.classList.contains('Mui-disabled')).toBeFalsy();
  });

  it('should render button as disabled when it is animating and is disabled in props', async () => {
    const props = {
      isAnimating: true,
      isDisabled: true,
      onTogglePlayButton: jest.fn(),
    };

    const { getByTestId } = render(
      <CoreThemeProvider>
        <PlayButton {...props} />
      </CoreThemeProvider>,
    );

    // Should display "play" icon (default)
    const button = getByTestId('playButton');
    expect(button).toBeTruthy();

    // A disabled play button should show up
    expect(button.classList.contains('Mui-disabled')).toBeTruthy();
  });

  it('should render button as enabled when it is animating and not disabled in props', async () => {
    const props = {
      isAnimating: true,
      isDisabled: false,
      onTogglePlayButton: jest.fn(),
    };

    const { getByTestId } = render(
      <CoreThemeProvider>
        <PlayButton {...props} />
      </CoreThemeProvider>,
    );

    // Should display "play" icon (default)
    const button = getByTestId('playButton');
    expect(button).toBeTruthy();

    // A disabled play button should show up
    expect(button.classList.contains('Mui-disabled')).toBeFalsy();
  });

  it('should call "onTogglePlayButton()" for each click of play or pause button', async () => {
    const props = {
      isAnimating: false,
      isDisabled: false,
      onTogglePlayButton: jest.fn(),
    };

    const { getByTestId } = render(
      <CoreThemeProvider>
        <PlayButton {...props} />
      </CoreThemeProvider>,
    );

    // "onTogglePlayButton()" should have been called once after first click
    const button = getByTestId('playButton');
    expect(button).toBeTruthy();

    fireEvent.click(button);
    expect(props.onTogglePlayButton).toHaveBeenCalledTimes(1);

    //  "onClickButton()" should have been called once again after second click, making up a total of two calls
    fireEvent.click(button);
    expect(props.onTogglePlayButton).toHaveBeenCalledTimes(2);
  });
});
