/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import moment from 'moment';
import { MapActionOrigin } from '../../../../store/mapStore/types';
import { dateFormat } from '../../../../store/mapStore/utils/helpers';
import PlayButtonConnect from './PlayButtonConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { mapActions } from '../../../../store';

describe('src/components/TimeSlider/TimeSliderButtons/PlayButton/PlayButtonConnect', () => {
  const dimensions = [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ];

  const mapId = 'map-1';
  it('should start playing animation with the correct start and end times when clicked while map is not yet animating', () => {
    const props = {
      mapId,
      isAnimating: false,
    };

    const mockStore = configureStore();
    const startValue = moment.utc().subtract(6, 'h').format(dateFormat);
    const endValue = moment.utc().subtract(10, 'm').format(dateFormat);
    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            isAnimating: false,
            timeStep: 15,
            animationStartTime: startValue,
            animationEndTime: endValue,
            dimensions,
          },
        },
      },
      syncronizationGroupStore: {
        groups: {
          byId: {
            Time_A: {
              targets: {
                byId: {
                  [mapId]: {
                    linked: false,
                  },
                },
                allIds: [mapId],
              },
            },
          },
          allIds: ['Time_A'],
        },
      },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { container } = render(
      <CoreThemeStoreProvider store={store}>
        <PlayButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const button = getByRole(container, 'button');
    expect(button).toBeTruthy();

    fireEvent.click(button);

    const expectedAction = mapActions.mapStartAnimation({
      mapId,
      start: startValue,
      end: endValue,
      interval: 15,
      origin: MapActionOrigin.map,
    });

    expect(store.getActions()).toEqual([expectedAction]);
  });

  it('should stop playing animation when clicked while map is animating', () => {
    const props = {
      mapId,
      isAnimating: true,
    };

    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            isAnimating: true,
            timeStep: 15,
            dimensions,
          },
        },
      },
      syncronizationGroupStore: {
        groups: {
          byId: {
            Time_A: {
              targets: {
                byId: {
                  [mapId]: {
                    linked: false,
                  },
                },
                allIds: [mapId],
              },
            },
          },
          allIds: ['Time_A'],
        },
      },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { container } = render(
      <CoreThemeStoreProvider store={store}>
        <PlayButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const button = getByRole(container, 'button');
    expect(button).toBeTruthy();

    fireEvent.click(button);

    const expectedAction = mapActions.mapStopAnimation({
      mapId,
      origin: MapActionOrigin.map,
    });

    expect(store.getActions()).toEqual([expectedAction]);
  });
});
