/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import SpeedButtonConnect from './SpeedButtonConnect';
import { mockStateMapWithAnimationDelayWithoutLayers } from '../../../../utils/testUtils';
import {
  defaultDelay,
  getSpeedDelay,
  speedFactors,
} from '../../TimeSliderUtils';
import { CoreThemeProvider } from '../../../Providers/Providers';
import {
  MapActionOrigin,
  SpeedFactorType,
} from '../../../../store/mapStore/types';
import { mapActions } from '../../../../store';

const { setAnimationDelay } = mapActions;
describe('src/components/TimeSlider/TimeSliderButtons/SpeedButton/SpeedButtonConnect', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };
  it('should render an enabled speed slider and how a correct speed text', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(mapId);
    const store = mockStore(mockState);

    const { container, getByTestId } = render(
      <CoreThemeProvider>
        <Provider store={store}>
          <SpeedButtonConnect {...props} />
        </Provider>
      </CoreThemeProvider>,
    );
    const speedButton = getByRole(container, 'button');
    expect(speedButton).toBeTruthy();

    expect(
      getByTestId('speedButton').classList.contains('Mui-disabled'),
    ).toBeFalsy();

    expect(getByTestId('speedButton').textContent).toContain('4x');
  });

  it('speedSlider renders, when clicked open and should change speed delay value in store', async () => {
    const mockStore = configureStore();

    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            animationDelay: defaultDelay,
          },
        },
      },
    });

    const { getByTestId } = render(
      <CoreThemeProvider>
        <Provider store={store}>
          <SpeedButtonConnect {...props} />
        </Provider>
      </CoreThemeProvider>,
    );
    const button = getByTestId('speedButton');
    await fireEvent.click(button);
    const slider = getByTestId('speedSlider');
    expect(slider).toBeTruthy();

    await fireEvent.mouseDown(slider, { clientY: 1 });
    await fireEvent.mouseUp(slider);

    const expectedAction = setAnimationDelay({
      mapId,
      animationDelay: getSpeedDelay(
        speedFactors[speedFactors.length - 1] as SpeedFactorType,
      ),
      origin: MapActionOrigin.map,
    });

    expect(store.getActions()).toEqual([expectedAction]);

    await fireEvent.mouseDown(slider, { clientY: 0 });
    await fireEvent.mouseUp(slider);

    const expectedActions2 = [
      setAnimationDelay({
        mapId,
        animationDelay: getSpeedDelay(
          speedFactors[speedFactors.length - 1] as SpeedFactorType,
        ),
        origin: MapActionOrigin.map,
      }),
      setAnimationDelay({
        mapId,
        animationDelay: getSpeedDelay(speedFactors[0] as SpeedFactorType),
        origin: MapActionOrigin.map,
      }),
    ];

    expect(store.getActions()).toEqual(expectedActions2);
  });
});
