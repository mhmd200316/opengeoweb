/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Tooltip, Theme, Box } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

import {
  CustomSlider,
  sliderHeaderStyle,
  ToolButton,
} from '@opengeoweb/shared';
import { TextIcon } from '@opengeoweb/theme';
import {
  getSpeedDelay,
  getValueFromKeyboardEvent,
  speedFactors,
} from '../../TimeSliderUtils';

const useStyles = makeStyles((theme: Theme) => ({
  speedButtonDiv: {
    height: '24px',
    width: '24px',
    position: 'relative',
  },
  sliderBackground: {
    padding: '0 0 60px 0',
    backgroundColor: theme.palette.geowebColors.background.surface,
    width: '95px',
    height: '150px',
    position: 'absolute',
    bottom: '34px',
    left: '-18px',
  },
}));

export const speedTooltipTitle = 'Speed';

interface SpeedInfoType {
  delay: number;
  markPos: number;
  text: string;
}

interface SpeedButtonProps {
  animationDelay: number;
  setMapAnimationDelay: (delay: number) => void;
}

const SpeedButton: React.FC<SpeedButtonProps> = ({
  animationDelay,
  setMapAnimationDelay,
}: SpeedButtonProps) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const marks: SpeedInfoType[] = speedFactors.map((speedFactor, i) => ({
    delay: getSpeedDelay(speedFactor),
    markPos: i,
    text: `${speedFactor}x`,
  }));

  const max = marks.length - 1;
  const min = 0;
  const currentValue = marks.findIndex((info) => info.delay === animationDelay);

  const getAnimationDelayValue = (markPos: number): number => {
    return marks[markPos].delay || marks[0].delay;
  };

  const onClickButton = (): void => {
    setOpen(!open);
  };

  const onChangeSlider = (event, value): void => {
    const newValue = getAnimationDelayValue(value);

    if (event.type !== 'keydown') {
      setMapAnimationDelay(newValue);
    }
  };

  const onKeyDown = (event): void => {
    // disable default Slider behavior
    event.stopPropagation();
    event.preventDefault();
    // custom logic
    if (event.key === 'Tab') {
      setOpen(!open);
    }

    const newValue = getValueFromKeyboardEvent(event, currentValue, min, max);
    if (newValue !== null) {
      setMapAnimationDelay(getAnimationDelayValue(newValue));
    }
  };

  const { text } = marks[currentValue];
  return (
    <div className={classes.speedButtonDiv}>
      <Tooltip title={speedTooltipTitle}>
        <ToolButton onClick={onClickButton} active data-testid="speedButton">
          <TextIcon text={text} />
        </ToolButton>
      </Tooltip>
      {open ? (
        <div className={classes.sliderBackground}>
          <Box sx={sliderHeaderStyle}>Speed</Box>
          <CustomSlider
            data-testid="speedSlider"
            orientation="vertical"
            value={currentValue}
            step={null}
            max={max}
            min={min}
            marks={marks.map(({ markPos, text }) => ({
              value: markPos,
              label: text,
            }))}
            onChange={onChangeSlider}
            onKeyDown={onKeyDown}
          />
        </div>
      ) : null}
    </div>
  );
};

export default SpeedButton;
