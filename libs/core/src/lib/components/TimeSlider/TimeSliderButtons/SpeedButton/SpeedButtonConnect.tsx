/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { MapActionOrigin } from '../../../../store/mapStore/types';

import { mapActions, mapSelectors } from '../../../../store';
import { AppStore } from '../../../../types/types';
import SpeedButton from './SpeedButton';

interface SpeedButtonConnectProps {
  animationDelay?: number;
  mapId: string;
  setMapAnimationDelay?: typeof mapActions.setAnimationDelay;
}

const connectRedux = connect(
  (store: AppStore, props: SpeedButtonConnectProps) => ({
    animationDelay: mapSelectors.getMapAnimationDelay(store, props.mapId),
  }),
  {
    setMapAnimationDelay: mapActions.setAnimationDelay,
  },
);

const SpeedButtonConnect = connectRedux(
  ({
    animationDelay,
    mapId,
    setMapAnimationDelay,
  }: SpeedButtonConnectProps) => {
    const onSetAnimationDelay = (_animationDelay: number): void => {
      setMapAnimationDelay({
        mapId,
        animationDelay: _animationDelay,
        origin: MapActionOrigin.map,
      });
    };
    return (
      <SpeedButton
        animationDelay={animationDelay}
        setMapAnimationDelay={onSetAnimationDelay}
      />
    );
  },
);

export default SpeedButtonConnect;
