/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';

import {
  defaultAnimationDelayAtStart,
  getSpeedDelay,
  speedFactors,
} from '../../TimeSliderUtils';
import { CoreThemeProvider } from '../../../Providers/Providers';
import SpeedButton, { speedTooltipTitle } from './SpeedButton';
import { SpeedFactorType } from '../../../../store/mapStore/types';

describe('src/components/TimeSlider/SpeedButton', () => {
  const props = {
    animationDelay: defaultAnimationDelayAtStart,
    setMapAnimationDelay: jest.fn(),
  };
  it('should render the button with text', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <SpeedButton {...props} />
      </CoreThemeProvider>,
    );

    const buttonText = getByTestId('speedButton').textContent;
    expect(buttonText).toEqual('4x');
  });

  it('should show and hide tooltip', async () => {
    render(
      <CoreThemeProvider>
        <SpeedButton {...props} />
      </CoreThemeProvider>,
    );

    const button = screen.queryByTestId('speedButton');
    expect(button).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(screen.queryByText(speedTooltipTitle)).toBeFalsy();

    fireEvent.mouseOver(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.queryByText(speedTooltipTitle)).toBeTruthy();

    fireEvent.mouseOut(button);
    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeFalsy();
      expect(screen.queryByText(speedTooltipTitle)).toBeFalsy();
    });
  });

  it('should toggle a slider on click', () => {
    const { getByTestId, queryByRole } = render(
      <CoreThemeProvider>
        <SpeedButton {...props} />
      </CoreThemeProvider>,
    );

    expect(queryByRole('slider')).toBeFalsy();
    fireEvent.click(getByTestId('speedButton'));
    expect(queryByRole('slider')).toBeTruthy();
    fireEvent.click(getByTestId('speedButton'));
    expect(queryByRole('slider')).toBeFalsy();
  });

  it('should render the slider correctly with 8 marks and first one active', () => {
    const { container, getByTestId } = render(
      <CoreThemeProvider>
        <SpeedButton {...props} />
      </CoreThemeProvider>,
    );

    const buttonText = getByTestId('speedButton').textContent;
    expect(buttonText).toEqual('4x');

    fireEvent.click(getByTestId('speedButton'));
    const marks = container.getElementsByClassName('MuiSlider-mark');
    expect(marks.length === 8).toBeTruthy();
    const activeMarks = container.getElementsByClassName(
      'MuiSlider-markActive',
    );
    expect(activeMarks.length > 0).toBeTruthy();
    // Confirms that the first mark is active
    expect(marks[0].classList).toContain('MuiSlider-markActive');
  });

  it('should render a slider as enabled', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <SpeedButton {...props} />
      </CoreThemeProvider>,
    );
    fireEvent.click(getByTestId('speedButton'));
    expect(getByTestId('speedSlider')).toBeTruthy();

    // slider should be enabled
    expect(
      getByTestId('speedSlider').classList.contains('Mui-disabled'),
    ).toBeFalsy();
  });

  it('speedButton slider can be opened and closed and handleChange is called correctly', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <SpeedButton {...props} />
      </CoreThemeProvider>,
    );
    fireEvent.click(getByTestId('speedButton'));
    const slider = getByTestId('speedSlider');
    expect(slider).toBeTruthy();
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);
    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(1);
    expect(props.setMapAnimationDelay).toHaveBeenCalledWith(
      getSpeedDelay(speedFactors[speedFactors.length - 1] as SpeedFactorType),
    );

    fireEvent.mouseDown(slider, { clientY: 0 });
    fireEvent.mouseUp(slider);
    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(2);
    expect(props.setMapAnimationDelay).toHaveBeenLastCalledWith(
      getSpeedDelay(speedFactors[0] as SpeedFactorType),
    );
  });

  it('speedButton slider can be correctly called with other delay than default delay', async () => {
    const props2 = {
      animationDelay: 1000,
      setMapAnimationDelay: jest.fn(),
    };
    const { getByTestId } = render(
      <CoreThemeProvider>
        <SpeedButton {...props2} />
      </CoreThemeProvider>,
    );
    fireEvent.click(getByTestId('speedButton'));
    const slider = getByTestId('speedSlider');
    expect(slider).toBeTruthy();

    const activeMarks = slider.getElementsByClassName('MuiSlider-markActive');
    const marks = slider.getElementsByClassName('MuiSlider-mark');
    expect(activeMarks.length > 0).toBeTruthy();
    // Confirms that the value 5 (4x) is marked active
    expect(marks[5].classList).not.toContain('MuiSlider-markActive');
    // Confirms that the value 3 (1000 => 1x) is marked active
    expect(marks[3].classList).toContain('MuiSlider-markActive');

    const buttonText = getByTestId('speedButton').textContent;
    expect(buttonText).toEqual('1x');
  });
  it('speed slider value can be changed with an arrow up and down key when a ctrl key is pressed', async () => {
    const { getByTestId, getByRole } = render(
      <CoreThemeProvider>
        <SpeedButton {...props} />
      </CoreThemeProvider>,
    );
    fireEvent.click(getByTestId('speedButton'));
    const slider = getByTestId('speedSlider');

    expect(slider).toBeTruthy();
    const thumb = getByRole('slider');
    expect(thumb).toBeTruthy();
    thumb.focus();
    fireEvent.keyDown(thumb, {
      ctrlKey: true,
      key: 'ArrowUp',
      code: 'ArrowUp',
    });
    fireEvent.keyUp(thumb, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(1);
    expect(props.setMapAnimationDelay).toHaveBeenCalledWith(125);
    fireEvent.keyDown(thumb, {
      ctrlKey: true,
      key: 'ArrowDown',
      code: 'ArrowDown',
    });
    fireEvent.keyUp(thumb, { key: 'ArrowDown', code: 'ArrowDown' });
    expect(props.setMapAnimationDelay).toHaveBeenCalledTimes(2);
    expect(props.setMapAnimationDelay).toHaveBeenCalledWith(125);
    expect(props.setMapAnimationDelay).toHaveBeenCalledWith(500);
  });
});
