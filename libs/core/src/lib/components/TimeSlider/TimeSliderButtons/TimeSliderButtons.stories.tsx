/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import TimeSliderButtons from './TimeSliderButtons';
import { CoreThemeProvider } from '../../Providers/Providers';

export const TimeSliderButtonsDemo = (): React.ReactElement => {
  return (
    <CoreThemeProvider>
      <div
        style={{
          marginTop: '500px',
          marginLeft: '50px',
        }}
      >
        <TimeSliderButtons />
      </div>
    </CoreThemeProvider>
  );
};

TimeSliderButtonsDemo.storyName = 'Time Slider Buttons';
