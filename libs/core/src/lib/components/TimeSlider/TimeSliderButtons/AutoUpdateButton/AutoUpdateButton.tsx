/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Tooltip } from '@mui/material';
import { ToolButton } from '@opengeoweb/shared';
import { AutoUpdateActive, AutoUpdateInActive } from '@opengeoweb/theme';

export const titleOn = 'Auto update on';
export const titleOff = 'Auto update off';
interface AutoUpdateButtonProps {
  toggleAutoUpdate?: () => void;
  disabled?: boolean;
  isAutoUpdating?: boolean;
}

const AutoUpdateButton: React.FC<AutoUpdateButtonProps> = ({
  toggleAutoUpdate = (): void => null,
  isAutoUpdating = false,
  disabled = false,
}: AutoUpdateButtonProps) => {
  return (
    <Tooltip
      title={isAutoUpdating ? titleOn : titleOff}
      data-testid="autoUpdateButtonTooltip"
    >
      <span>
        <ToolButton
          active={isAutoUpdating}
          disabled={disabled}
          onClick={(): void => toggleAutoUpdate()}
          data-testid="autoUpdateButton"
        >
          {isAutoUpdating ? (
            <AutoUpdateActive data-testid="auto-update-svg-on" />
          ) : (
            <AutoUpdateInActive data-testid="auto-update-svg-off" />
          )}
        </ToolButton>
      </span>
    </Tooltip>
  );
};

export default AutoUpdateButton;
