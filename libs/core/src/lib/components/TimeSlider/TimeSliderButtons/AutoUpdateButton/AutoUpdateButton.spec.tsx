/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';

import AutoUpdateButton, { titleOn } from './AutoUpdateButton';
import { CoreThemeProvider } from '../../../Providers/Providers';

describe('src/components/TimeSlider/AutoUpdateButton', () => {
  it('should render the component with text', () => {
    const props = {
      isAutoUpdating: false,
      toggleAutoUpdate: jest.fn(),
      disabled: false,
    };

    const { container } = render(
      <CoreThemeProvider>
        <AutoUpdateButton {...props} />
      </CoreThemeProvider>,
    );
    const componentText = container.querySelector('svg text').textContent;
    expect(componentText).toContain('AUTO');
  });

  it('should show and hide tooltip', async () => {
    const props = {
      isAutoUpdating: true,
      toggleAutoUpdate: jest.fn(),
      disabled: false,
    };

    render(
      <CoreThemeProvider>
        <AutoUpdateButton {...props} />
      </CoreThemeProvider>,
    );
    const button = screen.queryByTestId('autoUpdateButton');
    expect(button).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(screen.queryByText(titleOn)).toBeFalsy();

    fireEvent.mouseOver(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.queryByText(titleOn)).toBeTruthy();

    fireEvent.mouseOut(button);
    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeFalsy();
      expect(screen.queryByText(titleOn)).toBeFalsy();
    });
  });
});
