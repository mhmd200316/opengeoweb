/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { Button, ButtonGroup } from '@mui/material';

import { AppStore } from '../../../../types/types';
import { layerActions, mapActions, mapSelectors } from '../../../../store';
import { MapViewConnect } from '../../../MapView';
import { store } from '../../../../storybookUtils/store';
import { useDefaultMapSettings } from '../../../../storybookUtils/defaultStorySettings';
import {
  radarLayer,
  overLayer,
  baseLayerGrey,
  baseLayerOpenStreetMapNL,
  harmonieRelativeHumidityPl,
} from '../../../../utils/publicLayers';

import { TimeSliderConnect } from '../..';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../../../LayerManager';

import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { MapControls } from '../../../MapControls';

export default {
  title: 'components/AutoUpdate',
};

interface RadarPresetProps {
  setLayers?: typeof layerActions.setLayers;
  toggleAutoUpdate?: typeof mapActions.toggleAutoUpdate;
  setActiveLayerId?: typeof mapActions.setActiveLayerId;
  mapId: string;
}

const enhance = connect(
  (state: AppStore, props: RadarPresetProps) => ({
    layers: mapSelectors.getMapLayers(state, props.mapId),
  }),
  {
    setLayers: layerActions.setLayers,
    toggleAutoUpdate: mapActions.toggleAutoUpdate,
    setActiveLayerId: mapActions.setActiveLayerId,
  },
);

const RadarPresetWithAutoUpdate: React.FC<RadarPresetProps> = ({
  setLayers,
  mapId,
  toggleAutoUpdate,
  setActiveLayerId,
}: RadarPresetProps) => {
  useDefaultMapSettings({
    mapId,
    baseLayers: [
      { ...baseLayerGrey, id: `baseGrey-${mapId}` },
      {
        ...overLayer,
        id: `overLayer-1-${mapId}`,
      },
    ],
  });

  return (
    <ButtonGroup
      variant="contained"
      color="primary"
      aria-label="outlined primary button group"
    >
      <Button
        onClick={(): void => {
          setLayers({
            layers: [radarLayer],
            mapId,
          });
          toggleAutoUpdate({ mapId, shouldAutoUpdate: true });
        }}
      >
        Radar with auto-update
      </Button>
      <Button
        onClick={(): void => {
          setLayers({
            layers: [radarLayer, harmonieRelativeHumidityPl],
            mapId,
          });
          setActiveLayerId({ mapId, layerId: harmonieRelativeHumidityPl.id });
          toggleAutoUpdate({ mapId, shouldAutoUpdate: true });
        }}
      >
        Multiple layers with auto-update
      </Button>
    </ButtonGroup>
  );
};
const ConnectedRadarPresetWithAutoUpdate = enhance(RadarPresetWithAutoUpdate);

export const SetLayersWithAutoUpdate = (): React.ReactElement => {
  const mapId = 'mapid_1';
  const preloadedAvailableBaseLayers = [
    { ...baseLayerGrey, id: `baseGrey-${mapId}` },
    baseLayerOpenStreetMapNL,
  ];

  return (
    <CoreThemeStoreProvider store={store}>
      <div style={{ height: '100vh' }}>
        <MapViewConnect mapId={mapId} />
      </div>

      <MapControls>
        <LayerManagerMapButtonConnect mapId={mapId} />
      </MapControls>
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 500,
          width: '100%',
        }}
      >
        <TimeSliderConnect mapId={mapId} sourceId="timeslider_1" />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '12px',
          top: '50px',
          zIndex: 500,
        }}
      >
        <LayerManagerConnect
          preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 50,
        }}
      >
        <ConnectedRadarPresetWithAutoUpdate mapId={mapId} />
      </div>
    </CoreThemeStoreProvider>
  );
};
