/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import { MapActionOrigin } from '../../../../store/mapStore/types';
import AutoUpdateButtonConnect from './AutoUpdateButtonConnect';

import { mockStateMapWithTimeStepWithoutLayers } from '../../../../utils/testUtils';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { mapActions } from '../../../../store';

describe('src/components/TimeSlider/TimeSliderButtons/AutoUpdateButton/AutoUpdateButtonConnect.tsx', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };
  it('should render inactive auto update button by default', async () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithTimeStepWithoutLayers(mapId, 5);
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { container, getByTestId, findByText } = render(
      <CoreThemeStoreProvider store={store}>
        <AutoUpdateButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const autoUpdateButton = getByRole(container, 'button');
    expect(autoUpdateButton).toBeTruthy();
    expect(getByTestId('autoUpdateButton').textContent).toContain('AUTO');
    expect(
      getByTestId('autoUpdateButton').classList.contains('Mui-disabled'),
    ).toBeFalsy();

    fireEvent.focus(getByTestId('autoUpdateButton'));
    expect(await findByText('Auto update off')).toBeTruthy();
  });

  it('autoupdate button is clicked, the autoupdate state should change in the store', async () => {
    const mockStore = configureStore();

    const store = mockStore({
      webmap: {
        byId: {
          [mapId]: {
            shouldAutoUpdate: false,
          },
        },
      },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { container } = render(
      <CoreThemeStoreProvider store={store}>
        <AutoUpdateButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const button = getByRole(container, 'button');
    const expectedAction1 = [
      mapActions.toggleAutoUpdate({
        mapId: 'mapid_1',
        shouldAutoUpdate: true,
        origin: MapActionOrigin.map,
      }),
    ];
    await fireEvent.click(button);
    expect(store.getActions()).toEqual(expectedAction1);
  });
});
