/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, getByRole, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import { MapActionOrigin } from '../../../../store/mapStore/types';
import TimeStepButtonConnect from './TimeStepButtonConnect';
import { mockStateMapWithMultipleLayers } from '../../../../utils/testUtils';
import { multiDimensionLayer3 } from '../../../../utils/defaultTestSettings';
import { CoreThemeProvider } from '../../../Providers/Providers';
import { mapActions } from '../../../../store';

const { setTimeStep } = mapActions;
describe('src/components/TimeSlider/TimeSliderButtons/TimeStepButton/TimeStepButtonConnect', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };

  it('should render an enabled time step slider and show a correct time step text', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithMultipleLayers(
      [multiDimensionLayer3],
      mapId,
    );
    const store = mockStore(mockState);

    const { container, getByTestId } = render(
      <CoreThemeProvider>
        <Provider store={store}>
          <TimeStepButtonConnect {...props} />
        </Provider>
      </CoreThemeProvider>,
    );
    const timeStepButton = getByRole(container, 'button');
    expect(timeStepButton).toBeTruthy();

    expect(
      getByTestId('timeStepButton').classList.contains('Mui-disabled'),
    ).toBeFalsy();

    expect(getByTestId('timeStepButton').textContent).toContain('5m');
  });

  it('timeStepslider renders, when clicked open and should change time step in store', async () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithMultipleLayers(
      [multiDimensionLayer3],
      mapId,
    );
    const store = mockStore(mockState);
    const { getByTestId } = render(
      <CoreThemeProvider>
        <Provider store={store}>
          <TimeStepButtonConnect {...props} />
        </Provider>
      </CoreThemeProvider>,
    );
    const button = getByTestId('timeStepButton');
    fireEvent.click(button);
    const slider = getByTestId('timeStepButtonSlider');
    expect(slider).toBeTruthy();
    // a line below moves timeStep value to max = 5256000
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);

    const expectedAction = [
      setTimeStep({
        mapId,
        timeStep: 5,
      }),
      setTimeStep({
        mapId,
        timeStep: 5256000,
        origin: MapActionOrigin.map,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);

    // a line below moves timeStep value to min = 1
    fireEvent.mouseDown(slider, { clientY: 0 });
    fireEvent.mouseUp(slider);

    const expectedActions2 = [
      setTimeStep({
        mapId,
        timeStep: 5,
      }),
      setTimeStep({
        mapId,
        timeStep: 5256000,
        origin: MapActionOrigin.map,
      }),
      setTimeStep({
        mapId,
        timeStep: 1,
        origin: MapActionOrigin.map,
      }),
    ];

    expect(store.getActions()).toEqual(expectedActions2);
  });
});
