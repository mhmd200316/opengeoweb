/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Tooltip, Switch, FormControlLabel, Theme, Box } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';

import {
  CustomSlider,
  sliderHeaderStyle,
  ToolButton,
} from '@opengeoweb/shared';
import { FastForward, TextIcon } from '@opengeoweb/theme';
import { Dimension, MapActionOrigin } from '../../../../store/mapStore/types';
import {
  getActiveLayerTimeStep,
  getValueFromKeyboardEvent,
} from '../../TimeSliderUtils';

const timestepAutoSwitchActive = {
  title: 'Timestep Auto on',
};

const timestepAutoSwitchInactive = {
  title: 'Timestep Auto off',
};

const timeStepMarks = [
  {
    value: 1,
    label: '1 min',
    timeStepValue: 1,
    text: '1m',
  },
  {
    value: 2,
    label: '2 min',
    timeStepValue: 2,
    text: '2m',
  },
  {
    value: 3,
    label: '5 min',
    timeStepValue: 5,
    text: '5m',
  },
  {
    value: 4,
    label: '10 min',
    timeStepValue: 10,
    text: '10m',
  },
  {
    value: 5,
    label: '15 min',
    timeStepValue: 15,
    text: '15m',
  },
  {
    value: 6,
    label: '30 min',
    timeStepValue: 30,
    text: '30m',
  },
  {
    value: 7,
    label: '1 h',
    timeStepValue: 60,
    text: '1h',
  },
  {
    value: 8,
    label: '2 h',
    timeStepValue: 120,
    text: '2h',
  },
  {
    value: 9,
    label: '3 h',
    timeStepValue: 180,
    text: '3h',
  },
  {
    value: 10,
    label: '6 h',
    timeStepValue: 360,
    text: '6h',
  },
  {
    value: 11,
    label: '12 h',
    timeStepValue: 720,
    text: '12h',
  },
  {
    value: 12,
    label: '24 h',
    timeStepValue: 1440,
    text: '24h',
  },
  {
    value: 13,
    label: '48 h',
    timeStepValue: 2880,
    text: '48h',
  },
  {
    value: 14,
    label: '72 h',
    timeStepValue: 4320,
    text: '72h',
  },
  {
    value: 15,
    label: '1 week',
    timeStepValue: 10080,
    text: '1w',
  },
  {
    value: 16,
    label: '2 weeks',
    timeStepValue: 20160,
    text: '2w',
  },
  {
    value: 17,
    label: '1 month',
    timeStepValue: 40320,
    text: '4w',
  },
  {
    value: 18,
    label: '2 months',
    timeStepValue: 80640,
    text: '8w',
  },
  {
    value: 19,
    label: '6 months',
    timeStepValue: 241920,
    text: '24w',
  },
  {
    value: 20,
    label: '1 year',
    timeStepValue: 525600,
    text: '1y',
  },
  {
    value: 21,
    label: '10 years',
    timeStepValue: 5256000,
    text: '10y',
  },
];

const useStyles = makeStyles((theme: Theme) => ({
  timeStepDiv: {
    width: '58px',
    height: '24px',
    position: 'relative',
  },
  sliderBackground: {
    backgroundColor: theme.palette.geowebColors.background.surface,
    width: '110px',
    height: '350px',
    padding: '0 0 83px 0',
    position: 'absolute',
    bottom: '34px',
  },
  timestepAutoText: {
    fontFamily: 'Roboto-Black, Roboto',
    fontSize: 12,
    color: theme.palette.geowebColors.typographyAndIcons.text,
  },
  timestepAutoTextOff: {
    fontFamily: 'Roboto-Black, Roboto',
    fontSize: 12,
    color: theme.palette.geowebColors.typographyAndIcons.text,
  },
  disabled: {},
}));

export const timeStepTitle = 'Time step';
interface TimeStepProps {
  timeStep: number;
  disabled?: boolean;
  isTimestepAuto?: boolean;
  timeDimension?: Dimension;
  onChangeTimeStep: (scale: number, origin?: MapActionOrigin) => void;
  onToggleTimestepAuto?: () => void;
}

const TimeStepButton: React.FC<TimeStepProps> = ({
  timeStep,
  disabled = false,
  isTimestepAuto = false,
  timeDimension,
  onChangeTimeStep,
  onToggleTimestepAuto = (): void => null,
}: TimeStepProps) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const onClickButton = (): void => {
    setOpen(!open);
  };

  const timestepAutoSwitch = !isTimestepAuto
    ? timestepAutoSwitchInactive
    : timestepAutoSwitchActive;

  const timestepAutoTextStyle = !isTimestepAuto
    ? classes.timestepAutoText
    : classes.timestepAutoTextOff;

  const disabledSlider = disabled || isTimestepAuto;

  const timeStepFromLayer = getActiveLayerTimeStep(timeDimension) || timeStep;

  const currentMark = timeStepMarks.find(
    (mark) => mark.timeStepValue === timeStep,
  );

  const value = currentMark && currentMark.value ? currentMark.value : 0;
  const min = timeStepMarks[0].value;
  const max = timeStepMarks[timeStepMarks.length - 1].value;

  const getTimeStepValue = (value: number): number =>
    timeStepMarks[Math.round(value - 1)].timeStepValue;

  const onChangeSlider = (event, value): void => {
    const newValue = getTimeStepValue(value);
    if (event.type !== 'keydown') {
      onChangeTimeStep(newValue, MapActionOrigin.map);
    }
  };

  const onKeyDown = (event): void => {
    // disable default Slider behavior
    event.stopPropagation();
    event.preventDefault();
    // custom logic
    if (event.key === 'Tab') {
      setOpen(!open);
    }

    const newValue = getValueFromKeyboardEvent(event, value, min, max);
    if (newValue !== null) {
      onChangeTimeStep(getTimeStepValue(newValue), MapActionOrigin.map);
    }
  };

  React.useEffect(() => {
    if (isTimestepAuto) {
      onChangeTimeStep(timeStepFromLayer);
    }
  }, [timeStepFromLayer, isTimestepAuto, onChangeTimeStep]);

  return (
    <div className={classes.timeStepDiv}>
      <Tooltip title={timeStepTitle}>
        <ToolButton
          active
          onClick={onClickButton}
          data-testid="timeStepButton"
          width="58px"
        >
          <FastForward />
          <TextIcon
            text={(currentMark && currentMark.text) || ''}
            data-testid="timeStepButtonText"
          />
        </ToolButton>
      </Tooltip>
      {open ? (
        <div className={classes.sliderBackground}>
          <Box sx={sliderHeaderStyle}>Time step</Box>
          <CustomSlider
            data-testid="timeStepButtonSlider"
            orientation="vertical"
            value={value}
            step={null}
            min={min}
            max={max}
            marks={timeStepMarks}
            disabled={disabledSlider}
            onChange={onChangeSlider}
            onKeyDown={onKeyDown}
            shouldAutoFocus
          />
          <Tooltip title={timestepAutoSwitch.title}>
            <span data-testid="timestepAutoSwitchTooltip">
              <FormControlLabel
                control={
                  <Switch
                    checked={isTimestepAuto}
                    onChange={(): void => onToggleTimestepAuto()}
                    disabled={disabled}
                    id="timestepAutoSwitch"
                    disableRipple
                    sx={{
                      marginLeft: '18px',
                    }}
                  />
                }
                label="Auto"
                labelPlacement="end"
                classes={{
                  label: timestepAutoTextStyle,
                }}
              />
            </span>
          </Tooltip>
        </div>
      ) : null}
    </div>
  );
};

export default TimeStepButton;
