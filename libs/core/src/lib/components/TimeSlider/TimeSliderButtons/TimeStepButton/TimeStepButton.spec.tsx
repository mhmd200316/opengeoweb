/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';

import { MapActionOrigin } from '../../../../store/mapStore/types';
import TimeStepButton, { timeStepTitle } from './TimeStepButton';
import { multiDimensionLayer3 } from '../../../../utils/defaultTestSettings';
import { CoreThemeProvider } from '../../../Providers/Providers';

describe('src/components/TimeSlider/TimeStepButton', () => {
  const props = {
    layers: [multiDimensionLayer3],
    timeStep: 5,
    onChangeTimeStep: jest.fn(),
  };

  it('should render the component with text', () => {
    const { container } = render(
      <CoreThemeProvider>
        <TimeStepButton {...props} />
      </CoreThemeProvider>,
    );
    const componentText = container.querySelector('div').textContent;

    expect(componentText).toContain('5m');
  });

  it('should show and hide tooltip', async () => {
    render(
      <CoreThemeProvider>
        <TimeStepButton {...props} />
      </CoreThemeProvider>,
    );

    const button = screen.queryByTestId('timeStepButton');
    expect(button).toBeTruthy();
    expect(screen.queryByRole('tooltip')).toBeFalsy();
    expect(screen.queryByText(timeStepTitle)).toBeFalsy();

    fireEvent.mouseOver(button);
    expect(await screen.findByRole('tooltip')).toBeTruthy();
    expect(screen.queryByText(timeStepTitle)).toBeTruthy();

    fireEvent.mouseOut(button);
    await waitFor(() => {
      expect(screen.queryByRole('tooltip')).toBeFalsy();
      expect(screen.queryByText(timeStepTitle)).toBeFalsy();
    });
  });

  it('should toggle a slider on click', () => {
    const { queryByRole } = render(
      <CoreThemeProvider>
        <TimeStepButton {...props} />
      </CoreThemeProvider>,
    );

    expect(queryByRole('slider')).toBeFalsy();

    fireEvent.click(queryByRole('button'));
    expect(queryByRole('slider')).toBeTruthy();

    fireEvent.click(queryByRole('button'));
    expect(queryByRole('slider')).toBeFalsy();
  });

  it('should render the slider correctly with 21 marks and first one active', () => {
    const { container, queryByRole } = render(
      <CoreThemeProvider>
        <TimeStepButton {...props} />
      </CoreThemeProvider>,
    );
    const buttonText = queryByRole('button').textContent;

    expect(buttonText).toContain('5m');

    fireEvent.click(queryByRole('button'));
    const activeMark = container.getElementsByClassName('MuiSlider-markActive');
    const marks = container.getElementsByClassName('MuiSlider-mark');

    expect(marks.length === 21).toBeTruthy();
    expect(activeMark.length === 3).toBeTruthy();
    // Confirms that the first mark is active
    expect(marks[0].classList).toContain('MuiSlider-markActive');
  });

  it('should render a slider as enabled', () => {
    const { queryByRole } = render(
      <CoreThemeProvider>
        <TimeStepButton {...props} />
      </CoreThemeProvider>,
    );

    fireEvent.click(queryByRole('button'));
    expect(queryByRole('slider')).toBeTruthy();
    // slider should be enabled
    expect(
      queryByRole('slider').classList.contains('Mui-disabled'),
    ).toBeFalsy();
  });

  it('should have focus when slider is first rendered', () => {
    const { queryByRole, container } = render(
      <CoreThemeProvider>
        <TimeStepButton {...props} />
      </CoreThemeProvider>,
    );

    fireEvent.click(queryByRole('button'));
    expect(queryByRole('slider')).toBeTruthy();
    expect(
      queryByRole('slider').classList.contains('Mui-disabled'),
    ).toBeFalsy();
    // slider should have focus
    expect(
      container
        .querySelector('.MuiSlider-thumb')
        .classList.contains('Mui-focusVisible'),
    ).toBeTruthy();
  });

  it('should render a slider as disabled if passed in as props', () => {
    const props2 = { disabled: true, ...props };
    const { queryByRole, getByTestId } = render(
      <CoreThemeProvider>
        <TimeStepButton {...props2} />
      </CoreThemeProvider>,
    );

    // button should be enabled
    expect(
      queryByRole('button').classList.contains('Mui-disabled'),
    ).toBeFalsy();
    // slider should be disabled
    fireEvent.click(queryByRole('button'));
    expect(getByTestId('timeStepButtonSlider')).toBeTruthy();
    expect(
      getByTestId('timeStepButtonSlider').classList.contains('Mui-disabled'),
    ).toBeTruthy();
  });

  it('timeStepButton slider can be opened and closed and handleChange is called correctly', async () => {
    const { getByTestId, queryByRole } = render(
      <CoreThemeProvider>
        <TimeStepButton {...props} />
      </CoreThemeProvider>,
    );

    fireEvent.click(queryByRole('button'));
    const slider = getByTestId('timeStepButtonSlider');
    expect(slider).toBeTruthy();

    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeStep).toHaveBeenCalledWith(
      5256000,
      MapActionOrigin.map,
    );

    fireEvent.mouseDown(slider, { clientY: 0 });
    fireEvent.mouseUp(slider);
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(2);
    expect(props.onChangeTimeStep).toHaveBeenLastCalledWith(
      1,
      MapActionOrigin.map,
    );
  });

  it('timeStepButton slider can be correctly called with other timeStep than 5', async () => {
    const props2 = {
      mapId: 'map_id',
      timeStep: 2880,
      layers: [multiDimensionLayer3],
      onChangeTimeStep: jest.fn(),
    };
    const { getByTestId, queryByRole } = render(
      <CoreThemeProvider>
        <TimeStepButton {...props2} />
      </CoreThemeProvider>,
    );

    fireEvent.click(queryByRole('button'));
    const slider = getByTestId('timeStepButtonSlider');
    expect(slider).toBeTruthy();

    const activeMark = slider.getElementsByClassName('MuiSlider-markActive');
    const marks = slider.getElementsByClassName('MuiSlider-mark');
    expect(activeMark.length === 13).toBeTruthy();
    // Confirms that 13th mark (48h) is marked active
    expect(marks[12].classList).toContain('MuiSlider-markActive');

    const componentText = getByTestId('timeStepButtonText').textContent;
    expect(componentText).toContain('48h');
  });

  it('timeStepButton slider value can be changed with an arrow up and down key when a ctrl key is pressed', async () => {
    const { getByTestId, queryByRole, container } = render(
      <CoreThemeProvider>
        <TimeStepButton {...props} />
      </CoreThemeProvider>,
    );

    fireEvent.click(queryByRole('button'));
    const slider = getByTestId('timeStepButtonSlider');
    expect(slider).toBeTruthy();

    const thumb = container.querySelector(
      '.MuiSlider-thumb',
    ) as HTMLSpanElement;
    expect(thumb).toBeTruthy();

    thumb.focus();
    fireEvent.keyDown(thumb, {
      ctrlKey: true,
      key: 'ArrowUp',
      code: 'ArrowUp',
    });
    fireEvent.keyUp(thumb, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeStep).toHaveBeenCalledWith(
      10,
      MapActionOrigin.map,
    );

    fireEvent.keyDown(thumb, {
      ctrlKey: true,
      key: 'ArrowDown',
      code: 'ArrowDown',
    });
    fireEvent.keyUp(thumb, { key: 'ArrowDown', code: 'ArrowDown' });
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(2);
    expect(props.onChangeTimeStep).toHaveBeenCalledWith(2, MapActionOrigin.map);
  });
});
