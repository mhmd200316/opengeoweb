/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { MapActionOrigin } from '../../../../store/mapStore/types';
import { layerSelectors, mapActions, mapSelectors } from '../../../../store';
import TimeStepButton from './TimeStepButton';
import { AppStore } from '../../../../types/types';

interface ConnectedTimeStepProps {
  mapId: string;
}

const TimeStepButtonComponent: React.FC<ConnectedTimeStepProps> = ({
  mapId,
}: ConnectedTimeStepProps) => {
  const dispatch = useDispatch();

  const timeStep = useSelector((store: AppStore) =>
    mapSelectors.getMapTimeStep(store, mapId),
  );

  const isAnimating = useSelector((store: AppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );
  const isTimestepAuto = useSelector((store: AppStore) =>
    mapSelectors.isTimestepAuto(store, mapId),
  );

  const activeLayerId = useSelector((store: AppStore) =>
    mapSelectors.getActiveLayerId(store, mapId),
  );

  const activeLayerTimeDimension = useSelector((store: AppStore) =>
    layerSelectors.getLayerTimeDimension(store, activeLayerId),
  );

  const onToggleTimestepAuto = React.useCallback((): void => {
    dispatch(
      mapActions.toggleTimestepAuto({
        mapId,
        timestepAuto: !isTimestepAuto,
        origin: MapActionOrigin.map,
      }),
    );
  }, [dispatch, isTimestepAuto, mapId]);

  const onSetTimeStep = React.useCallback(
    (timeStep: number, origin: MapActionOrigin): void => {
      dispatch(
        mapActions.setTimeStep({
          mapId,
          timeStep,
          ...(origin && { origin }),
        }),
      );
    },
    [dispatch, mapId],
  );

  return (
    <TimeStepButton
      timeStep={timeStep}
      onChangeTimeStep={onSetTimeStep}
      disabled={isAnimating}
      isTimestepAuto={isTimestepAuto}
      onToggleTimestepAuto={onToggleTimestepAuto}
      timeDimension={activeLayerTimeDimension}
    />
  );
};

/**
 * TimeStepButton component connected to the store displaying a time step of the time slider
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @example
 * ``` <TimeStepButtonConnect mapId={mapId} />```
 */

export default TimeStepButtonComponent;
