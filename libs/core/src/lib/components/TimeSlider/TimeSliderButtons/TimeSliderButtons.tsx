/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Grid, useMediaQuery } from '@mui/material';

import makeStyles from '@mui/styles/makeStyles';

import AutoUpdateButton from './AutoUpdateButton/AutoUpdateButton';
import OptionsMenuButton from './OptionsMenuButton/OptionsMenuButton';
import PlayButton from './PlayButton/PlayButton';
import SpeedButton from './SpeedButton/SpeedButton';
import TimeStepButton from './TimeStepButton/TimeStepButton';
import { defaultAnimationDelayAtStart } from '../TimeSliderUtils';

const useStyles = makeStyles({
  buttonContainerBig: {
    padding: '20px 0px 0px 12px',
    width: '210px',
  },
  buttonContainerSmall: {
    padding: '20px 0px 0px 12px',
    width: '80px',
  },
});
interface TimeSliderButtonsProps {
  autoUpdateBtn?: React.ReactChild;
  optionsMenuBtn?: React.ReactChild;
  playBtn?: React.ReactChild;
  speedBtn?: React.ReactChild;
  timeStepBtn?: React.ReactChild;
}

const TimeSliderButtons: React.FC<TimeSliderButtonsProps> = ({
  autoUpdateBtn,
  optionsMenuBtn,
  playBtn,
  speedBtn,
  timeStepBtn,
}: TimeSliderButtonsProps) => {
  const classes = useStyles();
  const matches = useMediaQuery('(min-width:600px)');

  return (
    <div data-testid="timeSliderButtons">
      {matches ? (
        <Grid container spacing={2} className={classes.buttonContainerBig}>
          <Grid item>
            {timeStepBtn || (
              <TimeStepButton
                timeStep={5}
                onChangeTimeStep={(): void => null}
              />
            )}
          </Grid>
          <Grid item>
            {speedBtn || (
              <SpeedButton
                animationDelay={defaultAnimationDelayAtStart}
                setMapAnimationDelay={(): void => null}
              />
            )}
          </Grid>
          <Grid item>{autoUpdateBtn || <AutoUpdateButton />}</Grid>
          <Grid item>{playBtn || <PlayButton />}</Grid>
        </Grid>
      ) : (
        <Grid container spacing={1} className={classes.buttonContainerSmall}>
          <Grid item>{optionsMenuBtn || <OptionsMenuButton />}</Grid>
          <Grid item>{playBtn || <PlayButton />}</Grid>
        </Grid>
      )}
    </div>
  );
};

export default TimeSliderButtons;
