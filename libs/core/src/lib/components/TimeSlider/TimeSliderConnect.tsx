/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import TimeSlider from './TimeSlider';
import TimeSliderButtonsConnect from './TimeSliderButtons/TimeSliderButtonsConnect';
import TimeSliderLegendConnect from './TimeSliderLegend/TimeSliderLegendConnect';
import TimeSliderRailConnect from './TimeSliderRail/TimeSliderRailConnect';
import TimeSliderScaleSliderConnect from './TimeSliderScaleSlider/TimeSliderScaleSliderConnect';
import { AppStore } from '../../types/types';
import { mapSelectors, mapActions, uiSelectors } from '../../store';
import { MapActionOrigin } from '../../store/mapStore/types';

interface TimeSliderConnectProps {
  sourceId: string;
  mapId: string;
  isTimeSliderHoverOn?: boolean;
  isTimeSliderVisible?: boolean;
  isAlwaysVisible?: boolean;
  toggleTimeSliderHover?: typeof mapActions.toggleTimeSliderHover;
  toggleTimeSliderIsVisible?: typeof mapActions.toggleTimeSliderIsVisible;
  activeWindowId?: string;
}

const connectRedux = connect(
  (store: AppStore, props: TimeSliderConnectProps) => ({
    isTimeSliderHoverOn: mapSelectors.isTimeSliderHoverOn(store, props.mapId),
    activeWindowId: uiSelectors.getActiveWindowId(store),
    isTimeSliderVisible: mapSelectors.isTimeSliderVisible(store, props.mapId),
  }),
  {
    toggleTimeSliderHover: mapActions.toggleTimeSliderHover,
    toggleTimeSliderIsVisible: mapActions.toggleTimeSliderIsVisible,
  },
);

const ConnectedTimeSlider: React.FC<TimeSliderConnectProps> = ({
  sourceId,
  mapId,
  isTimeSliderHoverOn,
  toggleTimeSliderHover,
  activeWindowId,
  isTimeSliderVisible,
  toggleTimeSliderIsVisible,
  isAlwaysVisible = false,
}: TimeSliderConnectProps) => {
  // don't use redux state if isAlwaysVisible
  const isVisible = isAlwaysVisible || isTimeSliderVisible;
  const mapIsActive = mapId === activeWindowId;

  // TODO: move keyboard logic to TimeSlider.tsx
  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent): void => {
      if (event.ctrlKey && event.altKey && event.key === 'h') {
        toggleTimeSliderHover({
          mapId,
          isTimeSliderHoverOn: !isTimeSliderHoverOn,
        });
      }
    };
    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [mapId, isTimeSliderHoverOn, toggleTimeSliderHover]);

  const onToggleTimeSliderVisibility = (isTimeSliderVisible: boolean): void => {
    toggleTimeSliderIsVisible({
      mapId,
      isTimeSliderVisible,
      origin: MapActionOrigin.map,
    });
  };

  return (
    <TimeSlider
      buttons={<TimeSliderButtonsConnect mapId={mapId} />}
      rail={<TimeSliderRailConnect mapId={mapId} sourceId={sourceId} />}
      scaleSlider={<TimeSliderScaleSliderConnect mapId={mapId} />}
      legend={<TimeSliderLegendConnect mapId={mapId} sourceId={sourceId} />}
      mapIsActive={mapIsActive}
      onToggleTimeSlider={onToggleTimeSliderVisibility}
      isVisible={isVisible}
    />
  );
};

/**
 * TimeSlider component with components connected to the store displaying the time slider
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {string} sourceId sourceId: string - Source Id of the timeslider
 * @example
 * ``` <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />```
 */
const TimeSliderConnect = connectRedux(ConnectedTimeSlider);

export default TimeSliderConnect;
