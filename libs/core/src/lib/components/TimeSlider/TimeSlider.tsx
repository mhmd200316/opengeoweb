/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';
import { Grid } from '@mui/material';

import makeStyles from '@mui/styles/makeStyles';

import TimeSliderButtons from './TimeSliderButtons/TimeSliderButtons';
import TimeSliderLegend from './TimeSliderLegend/TimeSliderLegend';
import TimeSliderScaleSlider from './TimeSliderScaleSlider/TimeSliderScaleSlider';
import TimeSliderRail from './TimeSliderRail/TimeSliderRail';
import { defaultDataScaleToSecondsPerPx } from './TimeSliderUtils';
import { Scale } from '../../store/mapStore/types';

const useStyles = makeStyles({
  container: {
    display: 'grid',
    gridTemplateColumns: 'minmax(100px, 210px) minmax(66%, 1fr)',
    gridTemplateRows: '50px',
  },
  scaleSlider: {
    height: '50px',
  },
});

const useTimeSliderKeyBoardControl = (
  mapIsActive: boolean,
  isVisible: boolean,
  onToggleTimeSlider: (isVisible: boolean) => void,
): void => {
  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent): void => {
      const target = event.target as Element;
      if (target.tagName !== 'INPUT' && event.key === 't' && mapIsActive) {
        onToggleTimeSlider(!isVisible);
      }
    };

    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, [mapIsActive, isVisible, onToggleTimeSlider]);
};

export interface TimeSliderProps {
  buttons?: React.ReactChild;
  rail?: React.ReactChild;
  scaleSlider?: React.ReactChild;
  legend?: React.ReactChild;
  mapIsActive?: boolean;
  onToggleTimeSlider?: (isVisible: boolean) => void;
  isVisible?: boolean;
}

const defaultProps = {
  centerTime: moment.utc().startOf('day').hours(12).unix(),
  secondsPerPx: 5,
  selectedTime: moment.utc().startOf('day').hours(12),
  dataScaleToSecondsPerPx: defaultDataScaleToSecondsPerPx,
};

const TimeSlider: React.FC<TimeSliderProps> = ({
  buttons,
  rail,
  scaleSlider,
  legend,
  mapIsActive = true,
  isVisible = true,
  onToggleTimeSlider = (): void => {},
}: TimeSliderProps) => {
  const styles = useStyles();
  useTimeSliderKeyBoardControl(mapIsActive, isVisible, onToggleTimeSlider);

  return isVisible ? (
    <Grid container className={styles.container} data-testid="timeSlider">
      <Grid item> {buttons || <TimeSliderButtons />} </Grid>
      <Grid item>
        {rail || (
          <TimeSliderRail
            {...defaultProps}
            selectedTime={defaultProps.selectedTime}
            mapIsActive={mapIsActive}
          />
        )}{' '}
      </Grid>
      <Grid item className={styles.scaleSlider}>
        {scaleSlider || (
          <TimeSliderScaleSlider
            {...defaultProps}
            selectedTime={defaultProps.selectedTime.unix()}
            timeSliderScale={Scale.Hour}
            onChangeTimeSliderScale={(): void => null}
          />
        )}
      </Grid>
      <Grid item>
        {legend || (
          <TimeSliderLegend
            {...defaultProps}
            selectedTime={defaultProps.selectedTime.unix()}
            mapIsActive={mapIsActive}
          />
        )}{' '}
      </Grid>
    </Grid>
  ) : null;
};

export default TimeSlider;
