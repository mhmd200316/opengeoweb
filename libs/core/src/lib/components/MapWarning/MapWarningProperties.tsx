/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import moment from 'moment';

interface WarningListProps {
  selectedFeatureProperties: {
    identifier?: string;
    areaDesc?: string;
    severity?: string;
    certainty?: string;
    onset?: string;
    expires?: string;
    languages?: Language[];
  };
}

interface Language {
  language: string;
  event: string;
  senderName: string;
  headline: string;
  description: string;
}

export const MapWarningProperties: React.FC<WarningListProps> = ({
  selectedFeatureProperties,
}) => {
  return (
    <div
      data-testid="map-warning-properties"
      style={{
        width: '100%',
        height: '15%',
        marginTop: '30px',
        marginLeft: '100px',
        position: 'relative',
        zIndex: 10000,
        backgroundColor: '#CCCCCCC0',
        padding: '0px 20px 20px',
        overflow: 'auto',
        fontSize: '11px',
      }}
    >
      <h1 data-testid="map-warning-properties-title">
        {selectedFeatureProperties.areaDesc}
      </h1>

      {selectedFeatureProperties.areaDesc && (
        <pre data-testid="map-warning-properties-row">
          event: {selectedFeatureProperties.languages[2].event}
        </pre>
      )}
      {selectedFeatureProperties.onset && (
        <pre data-testid="map-warning-properties-row">
          Sent:{' '}
          {moment
            .utc(selectedFeatureProperties.onset)
            .format('DD MMM YYYY, HH:mm')}{' '}
          UTC
        </pre>
      )}
      {selectedFeatureProperties.expires && (
        <pre data-testid="map-warning-properties-row">
          Expires:{' '}
          {moment
            .utc(selectedFeatureProperties.expires)
            .format('DD MMM YYYY, HH:mm')}{' '}
          UTC
        </pre>
      )}
      {selectedFeatureProperties.languages[2].senderName && (
        <pre data-testid="map-warning-properties-row">
          Sender: {selectedFeatureProperties.languages[2].senderName}
        </pre>
      )}
      {selectedFeatureProperties.identifier && (
        <pre data-testid="map-warning-properties-row">
          Identifier: {selectedFeatureProperties.identifier}
        </pre>
      )}
    </div>
  );
};
