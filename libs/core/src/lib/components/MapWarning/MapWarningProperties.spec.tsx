/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import { MapWarningProperties } from './MapWarningProperties';

describe('src/components/MapWarning/MapWarningProperties', () => {
  const props = {
    details: {
      identifier: '2.49.0.1.578.0.220406064209738.1298',
      areaDesc: 'Andenes - Hekkingen',
      severity: 'Severe',
      certainty: 'Certain',
      event: 'Gale',
      onset: '2022-04-06T06:42:09+00:00',
      expires: '2022-04-06T14:00:00+00:00',
      languages: [
        {
          language: 'en-GB',
          event: 'UV advisory',
          senderName: 'Finnish Meteorological Institute',
          headline:
            'Yellow UV advisory: Locally in the southern part of the country, Wed 11.00 - 15.00',
          description:
            "UV advisory: In Wednesday noon, probability for strong Sun's ultraviolet radiation is 40%. The UV index is 6.",
        },
        {
          language: 'en-GB',
          event: 'UV advisory',
          senderName: 'Finnish Meteorological Institute',
          headline:
            'Yellow UV advisory: Locally in the southern part of the country, Wed 11.00 - 15.00',
          description:
            "UV advisory: In Wednesday noon, probability for strong Sun's ultraviolet radiation is 40%. The UV index is 6.",
        },
        {
          language: 'en-GB',
          event: 'UV advisory',
          senderName: 'Finnish Meteorological Institute',
          headline:
            'Yellow UV advisory: Locally in the southern part of the country, Wed 11.00 - 15.00',
          description:
            "UV advisory: In Wednesday noon, probability for strong Sun's ultraviolet radiation is 40%. The UV index is 6.",
        },
      ],
    },
  };

  it('should render successfully', async () => {
    const { findByTestId } = render(
      <MapWarningProperties selectedFeatureProperties={props.details} />,
    );

    expect(await findByTestId('map-warning-properties')).toBeTruthy();
  });

  it('should show location as title', () => {
    const { getByTestId } = render(
      <MapWarningProperties selectedFeatureProperties={props.details} />,
    );

    expect(getByTestId('map-warning-properties-title')).toBeTruthy();

    expect(getByTestId('map-warning-properties-title').textContent).toEqual(
      props.details.areaDesc,
    );
  });

  it('should show details', () => {
    const { getAllByTestId } = render(
      <MapWarningProperties selectedFeatureProperties={props.details} />,
    );

    expect(getAllByTestId('map-warning-properties-row')).toHaveLength(5);
  });

  it('should not show empty fields', () => {
    const propsEithEmptyDetails = {
      details: {
        identifier: '',
        areaDesc: '',
        severity: '',
        certainty: '',
        event: '',
        onset: '',
        expires: '',
        languages: [
          {
            language: '',
            event: '',
            senderName: '',
            headline: '',
            description: '',
          },
          {
            language: '',
            event: '',
            senderName: '',
            headline: '',
            description: '',
          },
          {
            language: '',
            event: '',
            senderName: '',
            headline: '',
            description: '',
          },
        ],
      },
    };
    const { queryByTestId } = render(
      <MapWarningProperties
        selectedFeatureProperties={propsEithEmptyDetails.details}
      />,
    );

    expect(queryByTestId('map-warning-properties-row')).toBeFalsy();
  });
});
