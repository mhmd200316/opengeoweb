/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { MapPreset, MapPresetInitialProps } from '../../store/mapStore/types';
import {
  ConfigurableMapConnect,
  ConfigurableMapConnectProps,
} from '../ConfigurableMap';
import {
  HarmonieTempAndPrecipPreset,
  InitialHarmTempAndPrecipProps,
} from '../MultiMapView/HarmoniePresets';
import {
  ModelRunIntervalProps,
  ModelRunInterval,
} from '../MultiMapView/ModelRunInterval';
import {
  MultiMapPreset,
  MultiMapViewConnect,
} from '../MultiMapView/MultiMapViewConnect';
import { TimeSliderConnect } from '../TimeSlider';

interface InitialMultiMapProps {
  mapPreset: MapPreset[];
  syncGroupsIds: string[];
  shouldShowZoomControls?: boolean;
}

export interface InitialMapProps {
  mapPreset: MapPreset;
  syncGroupsIds: string[];
}
interface InitialSliderProps {
  sliderPreset: { mapId: string };
  syncGroupsIds: string[];
}
export type InitialProps =
  | InitialMapProps
  | InitialMultiMapProps
  | InitialSliderProps
  | InitialHarmTempAndPrecipProps
  | ModelRunIntervalProps
  | MapPresetInitialProps;

/**
 * The lookup table is for registering your own components with the screenmanager.
 * @param payload
 * @returns
 */

export type SupportedComponentTypes =
  | 'Map'
  | 'MultiMap'
  | 'ModelRunInterval'
  | 'HarmonieTempAndPrecipPreset'
  | 'TimeSlider';

export interface ComponentsLookUpPayload {
  componentType: SupportedComponentTypes;
  id: string;
  title?: string;
  initialProps: InitialProps;
}

export const componentsLookUp = ({
  componentType,
  initialProps,
  id,
}: ComponentsLookUpPayload): React.ReactElement => {
  switch (componentType) {
    case 'Map': {
      const { mapPreset } = initialProps as InitialMapProps;
      return (
        <ConfigurableMapConnect
          {...(mapPreset as ConfigurableMapConnectProps)}
          id={id}
          data-testid="coreConfigurableMapConnect"
        />
      );
    }
    case 'MultiMap': {
      const { mapPreset, shouldShowZoomControls, syncGroupsIds } =
        initialProps as InitialMultiMapProps;

      if (!mapPreset || mapPreset.length === 0) return null;

      return (
        <MultiMapViewConnect
          rows={2}
          cols={2}
          showZoomControls={shouldShowZoomControls}
          maps={mapPreset.map((preset, index) => {
            return {
              id: `${id}_${index}`,
              syncGroupsIds,
              layers: preset.layers,
              activeLayerId: preset.activeLayerId,
              bbox: mapPreset[0]?.proj?.bbox,
              srs: mapPreset[0]?.proj?.srs,
            } as MultiMapPreset;
          })}
          data-testid="coreMultiMapViewConnect"
        />
      );
    }
    case 'HarmonieTempAndPrecipPreset': {
      const { layers } = initialProps as InitialHarmTempAndPrecipProps;
      return (
        <HarmonieTempAndPrecipPreset
          layers={layers || null}
          data-testid="coreHarmonieTempAndPrecipPreset"
        />
      );
    }
    case 'ModelRunInterval': {
      return (
        <ModelRunInterval
          {...(initialProps as ModelRunIntervalProps)}
          data-testid="coreModelRunInterval"
        />
      );
    }

    case 'TimeSlider': {
      const { sliderPreset } = initialProps as InitialSliderProps;
      return (
        <TimeSliderConnect
          {...sliderPreset}
          sourceId={id}
          data-testid="coreTimeSliderConnect"
        />
      );
    }
    default:
      return null;
  }
};
