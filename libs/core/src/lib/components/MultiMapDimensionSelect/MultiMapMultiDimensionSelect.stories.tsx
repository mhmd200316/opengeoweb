/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { store } from '../../storybookUtils/store';
import { MapViewConnect } from '../MapView';
import {
  baseLayerGrey,
  harmoniePrecipitation,
  metNorwayWind1,
  metNorwayWind2,
  metNorwayWind3,
} from '../../utils/publicLayers';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../LayerManager';

import { CoreThemeStoreProvider } from '../Providers/Providers';
import { LegendConnect, LegendMapButtonConnect } from '../Legend';
import MultiDimensionSelectMapButtonsConnect from './MultiDimensionSelectMapButtonsConnect';
import MultiMapMultiDimensionSelectConnect from './MultiMapMultiDimensionSelectConnect';
import MapControls from '../MapControls/MapControls';

const initialBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: -7264356.781958314,
    bottom: 5486720.808524769,
    right: 12998111.264068486,
    top: 13399817.799776679,
  },
};

const MultiDimensionSelectStory = (): React.ReactElement => {
  const mapId1 = 'mapid_1';
  const mapId2 = 'mapid_2';

  useDefaultMapSettings({
    mapId: mapId1,
    layers: [
      { ...metNorwayWind1, id: 'thredds_meps_latest_wind' },
      { ...metNorwayWind2, id: 'thredds_aromearctic_extracted_t' },
    ],
    baseLayers: [{ ...baseLayerGrey, id: `baseGrey-${mapId1}` }],
    bbox: initialBbox.bbox,
    srs: initialBbox.srs,
  });
  useDefaultMapSettings({
    mapId: mapId2,
    layers: [
      { ...metNorwayWind3, id: 'thredds_nk800_temperature' },
      { ...harmoniePrecipitation, id: `radar-${mapId2}` },
    ],
    baseLayers: [{ ...baseLayerGrey, id: `baseGrey-${mapId2}` }],
    bbox: initialBbox.bbox,
    srs: initialBbox.srs,
  });

  return (
    <div style={{ display: 'flex' }}>
      <LegendConnect mapId={mapId1} />
      <LayerManagerConnect />
      <MultiMapMultiDimensionSelectConnect />
      <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
        <MapControls>
          <LegendMapButtonConnect mapId={mapId1} />
          <LayerManagerMapButtonConnect mapId={mapId1} />
          <MultiDimensionSelectMapButtonsConnect mapId={mapId1} />
        </MapControls>

        <MapViewConnect mapId={mapId1} />
      </div>
      <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
        <MapControls>
          <LegendMapButtonConnect mapId={mapId2} />
          <LayerManagerMapButtonConnect mapId={mapId2} />
          <MultiDimensionSelectMapButtonsConnect mapId={mapId2} />
        </MapControls>

        <MapViewConnect mapId={mapId2} />
      </div>
    </div>
  );
};

export const MultiDimensionSelectStoryWithMultiMaps =
  (): React.ReactElement => (
    <CoreThemeStoreProvider store={store}>
      <MultiDimensionSelectStory />
    </CoreThemeStoreProvider>
  );
