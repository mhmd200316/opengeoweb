/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';

import { SvgIconProps } from '@mui/material';
import { Equalizer, Sort } from '@opengeoweb/theme';

export interface DimensionConfig {
  name: string;
  label: string;
  defaultUnit: string;
  reversed: boolean;
  iconType: string;
}

export const dimensionConfig: DimensionConfig[] = [
  {
    name: 'elevation',
    label: 'Vertical level',
    defaultUnit: 'hPa',
    reversed: true,
    iconType: 'IconLevels',
  },
  {
    name: 'ensemble_member',
    label: 'Ensemble member',
    defaultUnit: 'member',
    reversed: false,
    iconType: 'IconEnsemble',
  },
  {
    name: 'probability',
    label: 'Probability',
    defaultUnit: '%',
    reversed: false,
    iconType: 'IconLevels',
  },
  {
    name: 'level',
    label: 'Level',
    defaultUnit: 'level',
    reversed: false,
    iconType: 'IconLevels',
  },
];

export const getDimensionIcon = (iconType: string): React.FC<SvgIconProps> => {
  if (iconType === 'IconEnsemble') {
    return Equalizer;
  }
  return Sort;
};
