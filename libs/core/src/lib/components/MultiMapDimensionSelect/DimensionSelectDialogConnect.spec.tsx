/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, getAllByText, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import DimensionSelectDialogConnect from './DimensionSelectDialogConnect';
import {
  flDimensionLayer,
  multiDimensionLayer,
  WmMultiDimensionLayer,
  WmMultiDimensionLayer3,
} from '../../utils/defaultTestSettings';
import { mockStateMapWithDimensions } from '../../utils/testUtils';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';
import { dimensionConfig } from './MultiDimensionSelectConfig';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { layerActions } from '../../store';
import { LayerActionOrigin } from '../../store/mapStore/types';

describe('src/components/MultiMapDimensionSelect/DimensionSelectDialogConnect', () => {
  it('should render a dialog for elevation when map has elevation dimension and a slider if layer present with that dimension', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    registerWMLayer(WmMultiDimensionLayer, 'multiDimensionLayerMock');
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: ['dimensionSelect-elevation', 'dimensionSelect-ensemble_member'],
        dialogs: {
          'dimensionSelect-elevation': {
            activeMapId: mapId,
            isOpen: true,
            type: 'dimensionSelect-elevation',
          },
          'dimensionSelect-ensemble_member': {
            activeMapId: mapId,
            isOpen: true,
            type: 'dimensionSelect-ensemble_member',
          },
        },
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    const { container, getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectDialogConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const dimCnf = dimensionConfig.find(
      (cnf) => cnf.name === props.dimensionName,
    );
    expect(getAllByText(container, dimCnf.label).length).toBe(1);
    expect(getByTestId('slider-dimensionSelect')).toBeTruthy();
  });

  it('should only render slider for elevation for layers with elevation dimension', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    registerWMLayer(WmMultiDimensionLayer3, 'multiDimensionLayerMock');
    const mockState = mockStateMapWithDimensions(flDimensionLayer, mapId);
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectDialogConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(queryByTestId('slider-dimensionSelect')).toBeFalsy();
  });

  it('should call handleDimensionValueChanged when clicking on the slider', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    registerWMLayer(WmMultiDimensionLayer, 'multiDimensionLayerMock');
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: ['dimensionSelect-elevation', 'dimensionSelect-ensemble_member'],
        dialogs: {
          'dimensionSelect-elevation': {
            activeMapId: mapId,
            isOpen: true,
            type: 'dimensionSelect-elevation',
          },
          'dimensionSelect-ensemble_member': {
            activeMapId: mapId,
            isOpen: true,
            type: 'dimensionSelect-ensemble_member',
          },
        },
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    const { container, getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectDialogConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const dimCnf = dimensionConfig.find(
      (cnf) => cnf.name === props.dimensionName,
    );
    expect(getAllByText(container, dimCnf.label).length).toBe(1);
    expect(getByTestId('slider-dimensionSelect')).toBeTruthy();

    const slider = getByTestId('verticalSlider');
    expect(slider).toBeTruthy();

    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);

    const expectedAction = [
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId: 'multiDimensionLayerMock',
        dimension: {
          name: 'elevation',
          currentValue: '1000',
        },
        mapId,
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should call handleSyncChanged when clicking on the sync button', () => {
    const mapId = 'mapid_1';
    const mockStore = configureStore();
    registerWMLayer(WmMultiDimensionLayer, 'multiDimensionLayerMock');
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: ['dimensionSelect-elevation', 'dimensionSelect-ensemble_member'],
        dialogs: {
          'dimensionSelect-elevation': {
            activeMapId: mapId,
            isOpen: true,
            type: 'dimensionSelect-elevation',
          },
          'dimensionSelect-ensemble_member': {
            activeMapId: mapId,
            isOpen: true,
            type: 'dimensionSelect-ensemble_member',
          },
        },
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId,
      dimensionName: 'elevation',
      isOpen: true,
    };

    const { container, getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectDialogConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const dimCnf = dimensionConfig.find(
      (cnf) => cnf.name === props.dimensionName,
    );
    expect(getAllByText(container, dimCnf.label).length).toBe(1);
    expect(getByTestId('slider-dimensionSelect')).toBeTruthy();
    expect(getByTestId('syncButton')).toBeTruthy();

    fireEvent.click(getByTestId('syncButton'));

    const expectedAction = [
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId: 'multiDimensionLayerMock',
        dimension: {
          name: 'elevation',
          currentValue: '9000',
          synced: true,
        },
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);
  });
});
