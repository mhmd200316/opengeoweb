/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Grid, IconButton, Tooltip } from '@mui/material';
import { useSelector } from 'react-redux';
import { Link, LinkOff } from '@opengeoweb/theme';
import { AppStore } from '../../types/types';

import { getWMLayerById } from '../../store/mapStore/utils/helpers';
import { layerSelectors } from '../../store';
import { DimensionConfig } from './MultiDimensionSelectConfig';
import DimensionSelectSlider from './DimensionSelectSlider';
import { marksByDimension } from '../../utils/dimensionUtils';

export interface DimensionSelectSliderConnectProps {
  layerId: string;
  dimConfig: DimensionConfig;
  dimensionName: string;
  handleDimensionValueChanged: (
    layerId: string,
    dimensionName: string,
    dimensionValue: string,
  ) => void;
  handleSyncChanged: (
    layerId: string,
    dimensionName: string,
    dimensionValue: string,
    synced: boolean,
  ) => void;
}

const DimensionSelectSliderConnect: React.FC<DimensionSelectSliderConnectProps> =
  ({
    layerId,
    dimConfig,
    dimensionName,
    handleDimensionValueChanged,
    handleSyncChanged,
  }: DimensionSelectSliderConnectProps) => {
    const layerDimension = useSelector((store: AppStore) =>
      layerSelectors.getLayerDimension(store, layerId, dimensionName),
    );
    const wmLayer = getWMLayerById(layerId);
    if (!layerDimension || !wmLayer) {
      return null;
    }
    const wmsDimension = wmLayer.getDimension(dimensionName);
    if (!wmsDimension) {
      return null;
    }
    const layerName = wmLayer.getLayerName();
    const marks = marksByDimension(wmsDimension);
    const isLayerDimensionSynced =
      layerDimension.synced !== undefined && layerDimension.synced === true;

    const tooltipTitle = isLayerDimensionSynced
      ? 'Click to disconnect layer'
      : 'Click to connect layer';
    const Icon: React.FC = () =>
      isLayerDimensionSynced ? (
        <Link data-testid="syncIcon" />
      ) : (
        <LinkOff data-testid="syncDisIcon" />
      );

    return (
      <Grid key={layerId} item xs="auto">
        <Tooltip title={tooltipTitle} placement="top">
          <IconButton
            data-testid="syncButton"
            size="small"
            onClick={(): void =>
              handleSyncChanged(
                layerId,
                dimensionName,
                layerDimension.currentValue,
                !layerDimension.synced,
              )
            }
          >
            <Icon />
          </IconButton>
        </Tooltip>
        <DimensionSelectSlider
          marks={marks}
          layerName={layerName}
          reverse={dimConfig ? dimConfig.reversed : false}
          managedValue={
            Number(layerDimension.currentValue) || layerDimension.currentValue
          }
          onChangeDimensionValue={(value): void => {
            handleDimensionValueChanged(layerId, dimensionName, value);
          }}
          isDisabled={false}
          validSelection={
            layerDimension.synced === false ||
            !(
              layerDimension.validSyncSelection !== undefined &&
              layerDimension.validSyncSelection === false
            )
          }
        />
      </Grid>
    );
  };

export default DimensionSelectSliderConnect;
