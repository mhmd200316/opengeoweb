/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { uiActions, mapSelectors } from '../../store';
import * as uiSelectors from '../../store/ui/selectors';
import { DialogType } from '../../store/ui/types';
import { AppStore } from '../../types/types';
import DimensionSelectDialogConnect from './DimensionSelectDialogConnect';

export const getDimensionType = (dimensionName: string): string => {
  return `dimensionSelect-${dimensionName}`;
};
interface MultiMapSingleDimensionSelectConnectProps {
  dimensionName: string;
  index?: number;
}

const MultiMapSingleDimensionSelectConnect: React.FC<MultiMapSingleDimensionSelectConnectProps> =
  ({ dimensionName, index }: MultiMapSingleDimensionSelectConnectProps) => {
    const dispatch = useDispatch();
    const uiDialogType = getDimensionType(dimensionName) as DialogType;

    const mapId = useSelector((store: AppStore) =>
      uiSelectors.getDialogMapId(store, uiDialogType),
    );

    const uiOrder = useSelector((store: AppStore) =>
      uiSelectors.getDialogOrder(store, uiDialogType),
    );
    const uiSource = useSelector((store: AppStore) =>
      uiSelectors.getDialogSource(store, uiDialogType),
    );
    const uiIsOrderedOnTop = useSelector((store: AppStore) =>
      uiSelectors.getDialogIsOrderedOnTop(store, uiDialogType),
    );

    const isOpen = useSelector((store: AppStore) =>
      uiSelectors.getisDialogOpen(store, uiDialogType),
    );

    const isMapPresent = useSelector((store: AppStore) =>
      mapSelectors.getIsMapPresent(store, mapId),
    );

    const onOrderDialog = React.useCallback(() => {
      if (!uiIsOrderedOnTop) {
        dispatch(
          uiActions.orderDialog({
            type: uiDialogType,
          }),
        );
      }
    }, [dispatch, uiDialogType, uiIsOrderedOnTop]);

    const onToggleDialog = React.useCallback(
      (setOpen): void => {
        dispatch(
          uiActions.setToggleOpenDialog({
            type: uiDialogType,
            setOpen,
          }),
        );
      },
      [dispatch, uiDialogType],
    );

    const registerDialog = React.useCallback(
      (activeMapId) =>
        dispatch(
          uiActions.registerDialog({
            type: uiDialogType,
            mapId: activeMapId,
          }),
        ),
      [dispatch, uiDialogType],
    );

    const unregisterDialog = React.useCallback(
      () =>
        dispatch(
          uiActions.unregisterDialog({
            type: uiDialogType,
          }),
        ),
      [dispatch, uiDialogType],
    );

    // Check to ensure the currently active map is still present on screen - if not, close the dialog
    React.useEffect(() => {
      if (mapId !== '' && !isMapPresent) {
        onToggleDialog(false);
      }
    }, [mapId, isMapPresent, onToggleDialog]);

    // Register this dialog in the store
    React.useEffect(() => {
      registerDialog(mapId);

      return (): void => {
        unregisterDialog();
      };
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
      <DimensionSelectDialogConnect
        mapId={mapId}
        dimensionName={dimensionName}
        onMouseDown={onOrderDialog}
        onToggleDialog={onToggleDialog}
        order={uiOrder}
        source={uiSource}
        isOpen={isOpen}
        index={index}
      />
    );
  };

export default MultiMapSingleDimensionSelectConnect;
