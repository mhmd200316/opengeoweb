/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import configureStore from 'redux-mock-store';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { WMLayer } from '@opengeoweb/webmap';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import DimensionSelectDialogConnect from './DimensionSelectDialogConnect';
import { mockStateMapWithDimensions } from '../../utils/testUtils';
import { LayerType } from '../../store/mapStore/types';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';

const multiDimensionLayer = {
  service: 'https://testservice',
  id: 'layer-1',
  name: 'air_temperature__at_pl',
  title: 'air_temperature__at_pl',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'elevation',
      units: 'hPa',
      currentValue: '850',
      values: '850,925',
      synced: false,
      validSyncSelection: true,
    },
    {
      name: 'ensemble_member',
      units: 'member',
      currentValue: '7',
      values:
        '0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29',
      synced: true,
      validSyncSelection: true,
    },
    {
      name: 'modellevel',
      units: '',
      currentValue: '',
      values: '_0_,_1_,_2_',
      synced: true,
      validSyncSelection: false,
    },
  ],
};

const wmMultiDimensionLayer = new WMLayer(multiDimensionLayer);

const mapId = 'mapid_1';
const mockStore = configureStore();
registerWMLayer(wmMultiDimensionLayer, multiDimensionLayer.id);
const mockState = mockStateMapWithDimensions(multiDimensionLayer, mapId);
const store = mockStore(mockState);
store.addModules = (): void => null; // mocking the dynamic module loader

const elevationProps = {
  mapId,
  dimensionName: 'elevation',
  isOpen: true,
};

const ensembleMemberProps = {
  mapId,
  dimensionName: 'ensemble_member',
  isOpen: true,
};

const customProps = {
  mapId,
  dimensionName: 'modellevel',
  isOpen: true,
};

export const DimensionSelectElevationLight = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <DimensionSelectDialogConnect {...elevationProps} />
  </CoreThemeStoreProvider>
);

export const DimensionSelectElevationDark = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <DimensionSelectDialogConnect {...elevationProps} />
  </CoreThemeStoreProvider>
);

export const DimensionSelectEnsembleMemberLight = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <DimensionSelectDialogConnect {...ensembleMemberProps} />
  </CoreThemeStoreProvider>
);

export const DimensionSelectEnsembleMemberDark = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <DimensionSelectDialogConnect {...ensembleMemberProps} />
  </CoreThemeStoreProvider>
);

export const DimensionSelectInvalidSyncLight = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <DimensionSelectDialogConnect {...customProps} />
  </CoreThemeStoreProvider>
);

export const DimensionSelectInvalidSyncDark = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <DimensionSelectDialogConnect {...customProps} />
  </CoreThemeStoreProvider>
);

DimensionSelectElevationLight.storyName =
  'Elevation Light Theme (takeSnapshot)';
DimensionSelectElevationDark.storyName = 'Elevation Dark Theme (takeSnapshot)';
DimensionSelectEnsembleMemberLight.storyName =
  'Ensemble Member Light Theme (takeSnapshot)';
DimensionSelectEnsembleMemberDark.storyName =
  'Ensemble Member Dark Theme (takeSnapshot)';
DimensionSelectInvalidSyncLight.storyName =
  'Invalid Sync Value Light Theme (takeSnapshot)';
DimensionSelectInvalidSyncDark.storyName =
  'Invalid Sync Value Dark Theme (takeSnapshot)';
