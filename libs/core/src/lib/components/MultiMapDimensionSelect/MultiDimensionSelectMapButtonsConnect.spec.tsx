/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import MultiDimensionSelectMapButtonsConnect from './MultiDimensionSelectMapButtonsConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { createMap } from '../../store/mapStore/map/utils';

const mapId = 'map-1';
const mockMap = createMap({ id: mapId });
const mapId2 = 'map-2';
const mockMap2 = createMap({ id: mapId2 });
const mockStoreMapWithDimensions = {
  webmap: {
    byId: {
      [mapId]: {
        ...mockMap,
        dimensions: [
          {
            name: 'time',
            currentValue: '2021-11-11T12:50:00Z',
          },
          {
            name: 'reference_time',
            currentValue: '2021-11-11T09:00:00Z',
          },
          {
            name: 'elevation',
            currentValue: '850',
          },
        ],
      },
      [mapId2]: {
        ...mockMap2,
        dimensions: [
          {
            name: 'time',
            currentValue: '2021-11-11T12:50:00Z',
          },
          {
            name: 'reference_time',
            currentValue: '2021-11-11T09:00:00Z',
          },
        ],
      },
    },
    allIds: [mapId, mapId2],
  },
};

describe('src/components/MultiMapDimensionSelect/MultiDimensionSelectMapButtonsConnect/', () => {
  it('should create buttons for every non-time dimensions a single map', () => {
    const mockStore = configureStore();
    const store = mockStore(mockStoreMapWithDimensions);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId,
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MultiDimensionSelectMapButtonsConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('dimensionMapBtn-elevation')).toBeTruthy();
  });
  it('should create buttons no buttons in case no non-time dimensions available for map', () => {
    const mockStore = configureStore();
    const store = mockStore(mockStoreMapWithDimensions);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: mapId2,
    };
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MultiDimensionSelectMapButtonsConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(queryByTestId('dimensionMapBtnTooltip')).toBeFalsy();
  });
});
