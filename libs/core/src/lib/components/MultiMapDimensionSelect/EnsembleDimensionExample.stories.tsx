/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { mapActions, layerActions } from '../../store';
import { MapViewConnect } from '../MapView';
import { store } from '../../storybookUtils/store';
import MultiMapMultiDimensionSelectConnect from './MultiMapMultiDimensionSelectConnect';
import TimeSliderConnect from '../TimeSlider/TimeSliderConnect';
import {
  metNorwayWind1,
  metNorwayWind2,
  baseLayerGrey,
  metNorwayWind3,
} from '../../utils/publicLayers';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../LayerManager';

import { CoreThemeStoreProvider } from '../Providers/Providers';
import { MapControls } from '../MapControls';
import MultiDimensionSelectMapButtonsConnect from './MultiDimensionSelectMapButtonsConnect';
import { LegendConnect, LegendMapButtonConnect } from '../Legend';

const connectRedux = connect(null, {
  addLayer: layerActions.addLayer,
  addBaseLayer: layerActions.addBaseLayer,
  setBbox: mapActions.setBbox,
  onLayerChangeDimension: layerActions.layerChangeDimension,
});

const EnsembleDimensionComponent = connectRedux(() => {
  const layers = [
    { ...metNorwayWind1, id: 'thredds_meps_latest_wind' },
    { ...metNorwayWind2, id: 'thredds_aromearctic_extracted_t' },
    { ...metNorwayWind3, id: 'thredds_nk800_temperature' },
  ];

  const initialBbox = {
    srs: 'EPSG:3857',
    bbox: {
      left: -7264356.781958314,
      bottom: 5486720.808524769,
      right: 12998111.264068486,
      top: 13399817.799776679,
    },
  };

  useDefaultMapSettings({
    mapId: 'mapid_1',
    layers,
    baseLayers: [{ ...baseLayerGrey, id: 'baseGrey-mapid_1' }],
    bbox: initialBbox.bbox,
    srs: initialBbox.srs,
  });

  return (
    <div style={{ height: '100vh' }}>
      <div style={{ height: '100vh' }}>
        <MultiMapMultiDimensionSelectConnect />
        <MapViewConnect mapId="mapid_1" />
        <LayerManagerConnect />
        <LegendConnect mapId="mapid_1" />

        <MapControls>
          <LegendMapButtonConnect mapId="mapid_1" />
          <LayerManagerMapButtonConnect mapId="mapid_1" />
          <MultiDimensionSelectMapButtonsConnect mapId="mapid_1" />
        </MapControls>

        <div
          style={{
            position: 'absolute',
            left: '0px',
            bottom: '0px',
            zIndex: 10,
            right: '0px',
          }}
        >
          <TimeSliderConnect sourceId="timeslider-1" mapId="mapid_1" />
        </div>
      </div>
    </div>
  );
});

export const MultiDimensionDemo: React.FC = () => (
  <CoreThemeStoreProvider store={store}>
    <EnsembleDimensionComponent />
  </CoreThemeStoreProvider>
);
