/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { DialogType, Source } from '../../store/ui/types';
import DimensionSelectButtonConnect from './DimensionSelectButtonConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { uiActions } from '../../store';

describe('src/components/MultiMapDimensionSelect/DimensionSelectButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked, action should set isOpen to true if currently closed', () => {
    const mockStore = configureStore();
    const mockState = {
      'dimensionSelect-elevation': {
        type: 'dimensionSelect-elevation' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
        source: 'app',
      },
    };
    const store = mockStore({ ui: { dialogs: mockState } });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
      buttonTopPosition: 0,
      dimension: 'elevation',
      source: 'module' as Source,
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('dimensionMapBtn-elevation')).toBeTruthy();

    // open the dimension dialog for a different map
    fireEvent.click(getByTestId('dimensionMapBtn-elevation'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'dimensionSelect-elevation',
        mapId: props.mapId,
        setOpen: true,
        source: props.source,
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action with passed in mapid when clicked, action should set isOpen to false if currently opened', () => {
    const mockStore = configureStore();
    const mockState = {
      'dimensionSelect-elevation': {
        type: 'dimensionSelect-elevation' as DialogType,
        activeMapId: 'map1',
        isOpen: true,
        source: 'app',
      },
    };
    const store = mockStore({ ui: { dialogs: mockState } });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'map1',
      buttonTopPosition: 0,
      dimension: 'elevation',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('dimensionMapBtn-elevation')).toBeTruthy();

    // close the dimension dialog
    fireEvent.click(getByTestId('dimensionMapBtn-elevation'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'dimensionSelect-elevation',
        mapId: props.mapId,
        setOpen: false,
        source: 'app',
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);
  });
});
