/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import MultiMapMultiDimensionSelectConnect from './MultiMapMultiDimensionSelectConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { createMap } from '../../store/mapStore/map/utils';
import { uiActions } from '../../store';

const mapId = 'map-1';
const mockMap = createMap({ id: mapId });
const mapId2 = 'map-2';
const mockMap2 = createMap({ id: mapId2 });
const mockStoreMapWithDimensions = {
  webmap: {
    byId: {
      [mapId]: {
        ...mockMap,
        dimensions: [
          {
            name: 'time',
            currentValue: '2021-11-11T12:50:00Z',
          },
          {
            name: 'reference_time',
            currentValue: '2021-11-11T09:00:00Z',
          },
          {
            name: 'elevation',
            currentValue: '850',
          },
        ],
      },
      [mapId2]: {
        ...mockMap2,
        dimensions: [
          {
            name: 'time',
            currentValue: '2021-11-11T12:50:00Z',
          },
          {
            name: 'reference_time',
            currentValue: '2021-11-11T09:00:00Z',
          },
          {
            name: 'member',
            currentValue: '2',
          },
        ],
      },
    },
    allIds: [mapId, mapId2],
  },
};

describe('src/components/MultiMapDimensionSelect/MultiMapMultiDimensionSelectConnect/', () => {
  it('should create draggable dialogs for each dimension present in all maps', () => {
    const mockStore = configureStore();
    const store = mockStore(mockStoreMapWithDimensions);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <MultiMapMultiDimensionSelectConnect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: 'dimensionSelect-elevation',
        mapId: '',
      }),
      uiActions.registerDialog({
        type: 'dimensionSelect-member',
        mapId: '',
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);
  });
});
