/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { darkTheme, lightTheme } from '@opengeoweb/theme';

import { mapActions, layerActions } from '../../store';
import { MapViewConnect } from '../MapView';
import { store } from '../../storybookUtils/store';
import MultiMapMultiDimensionSelectConnect from './MultiMapMultiDimensionSelectConnect';
import TimeSliderConnect from '../TimeSlider/TimeSliderConnect';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../LayerManager';

import {
  metNorwayLatestT,
  metNorwaySalinaty,
  baseLayerGrey,
  overLayer,
} from '../../utils/publicLayers';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { LegendConnect, LegendMapButtonConnect } from '../Legend';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { MapControls } from '../MapControls';
import MultiDimensionSelectMapButtonsConnect from './MultiDimensionSelectMapButtonsConnect';

const connectRedux = connect(null, {
  addLayer: layerActions.addLayer,
  addBaseLayer: layerActions.addBaseLayer,
  setBbox: mapActions.setBbox,
  onLayerChangeDimension: layerActions.layerChangeDimension,
});

const DimensionExampleComponent = connectRedux(() => {
  const layers = [
    { ...metNorwayLatestT, id: 'thredds_meps_latest_t' },
    { ...metNorwaySalinaty, id: 'thredds_barents_2_5km_1h_salinity' },
  ];

  const initialBbox = {
    srs: 'EPSG:3857',
    bbox: {
      left: -7264356.781958314,
      bottom: 5486720.808524769,
      right: 12998111.264068486,
      top: 13399817.799776679,
    },
  };

  useDefaultMapSettings({
    mapId: 'mapid_1',
    layers,
    baseLayers: [{ ...baseLayerGrey, id: 'baseGrey-mapid_1' }, overLayer],
    bbox: initialBbox.bbox,
    srs: initialBbox.srs,
  });

  return (
    <div style={{ height: '100vh', width: '100vw' }}>
      <MapControls>
        <LayerManagerMapButtonConnect mapId="mapid_1" />
        <LegendMapButtonConnect mapId="mapid_1" />
        <MultiDimensionSelectMapButtonsConnect mapId="mapid_1" />
      </MapControls>

      <MultiMapMultiDimensionSelectConnect />
      <LegendConnect mapId="mapid_1" />
      <MapViewConnect mapId="mapid_1" />
      <LayerManagerConnect />

      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 10,
          right: '0px',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId="mapid_1" />
      </div>
    </div>
  );
});

export const ElevationDimensionDemoWithLightTheme: React.FC = () => (
  <CoreThemeStoreProvider store={store} theme={lightTheme}>
    <DimensionExampleComponent />
  </CoreThemeStoreProvider>
);

export const ElevationDimensionDemoWithDarkTheme: React.FC = () => (
  <CoreThemeStoreProvider store={store} theme={darkTheme}>
    <DimensionExampleComponent />
  </CoreThemeStoreProvider>
);
