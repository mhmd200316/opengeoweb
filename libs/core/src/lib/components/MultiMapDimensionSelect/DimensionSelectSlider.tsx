/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box, Slider, Tooltip, Typography } from '@mui/material';
import withStyles from '@mui/styles/withStyles';
import makeStyles from '@mui/styles/makeStyles';
import { useState } from 'react';
import { Mark } from '../../utils/dimensionUtils';

const useStyles = makeStyles(() => ({
  verticalLevelDimensionSelectContainer: {
    height: '320px',
    margin: '10px',
    marginLeft: '-5px',
    minWidth: 120,
    fontSize: '90%',
  },
  layerName: {
    paddingLeft: '10px',
    maxWidth: 130,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
}));

const SmallFontSlider = withStyles({
  markLabel: {
    fontSize: '80%',
  },
})(Slider);

interface DimensionSelectSliderProps {
  marks: Mark[];
  layerName: string;
  reverse?: boolean;
  isDisabled?: boolean;
  managedValue?: number | string;
  validSelection?: boolean;
  alertColor?: string;
  onChangeDimensionValue: (dimensionValue: string) => void;
}

const DimensionSelectSlider: React.FC<DimensionSelectSliderProps> = ({
  marks,
  layerName,
  reverse = false,
  isDisabled = false,
  managedValue = null,
  validSelection = true,
  alertColor = 'red',
  onChangeDimensionValue,
}: DimensionSelectSliderProps) => {
  const classes = useStyles();
  const [currentIndex, setCurrentIndex] = useState(0);

  if (!marks || !marks.length) {
    return null;
  }
  const sortedMarks = [...marks].sort(
    (a, b) =>
      typeof a.value === 'number' &&
      typeof b.value === 'number' &&
      a.value - b.value,
  );
  const adaptedMarks = reverse ? sortedMarks.reverse() : sortedMarks;

  const values = adaptedMarks.map((m) => m.value);
  // using index as values in slider to get constant spacing between marks
  const indexLabelMarks = adaptedMarks.map((m, index) => {
    return { value: index, label: m.label };
  });

  const sliderValue =
    managedValue !== null
      ? values.findIndex((v) => v === managedValue)
      : currentIndex;

  return (
    <>
      <div
        className={classes.verticalLevelDimensionSelectContainer}
        data-testid="slider-dimensionSelect"
      >
        <Box component="span" m={1}>
          <SmallFontSlider
            data-testid="verticalSlider"
            orientation="vertical"
            aria-labelledby="vertical-slider"
            step={null}
            min={0}
            max={indexLabelMarks.length - 1}
            marks={indexLabelMarks}
            style={!validSelection ? { color: alertColor } : {}}
            disabled={isDisabled}
            value={sliderValue}
            onChange={(e, val: number): void => {
              onChangeDimensionValue(values[val].toString());
              if (!managedValue) setCurrentIndex(val);
            }}
          />
        </Box>
      </div>
      <Tooltip title={layerName}>
        <Typography variant="body2" className={classes.layerName}>
          {layerName}
        </Typography>
      </Tooltip>
    </>
  );
};

export default DimensionSelectSlider;
