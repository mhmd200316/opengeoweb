/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { uiActions } from '../../store';
import * as uiSelectors from '../../store/ui/selectors';
import { AppStore } from '../../types/types';
import { DialogType, Source } from '../../store/ui/types';
import DimensionSelectButton from './DimensionSelectButton';
import { getDimensionType } from './MultiMapSingleDimensionSelectConnect';

interface DimensionSelectMapButtonProps {
  mapId: string;
  dimension: string;
  source?: Source;
}

const DimensionSelectMapButtonConnect: React.FC<DimensionSelectMapButtonProps> =
  ({ mapId, dimension, source = 'app' }: DimensionSelectMapButtonProps) => {
    const dispatch = useDispatch();
    const uiDialogType = getDimensionType(dimension) as DialogType;

    const currentActiveMapId = useSelector((store: AppStore) =>
      uiSelectors.getDialogMapId(store, uiDialogType),
    );

    const isOpenInStore = useSelector((store: AppStore) =>
      uiSelectors.getisDialogOpen(store, uiDialogType),
    );

    const openMultiDimensionDialog = React.useCallback((): void => {
      const setOpen = currentActiveMapId !== mapId ? true : !isOpenInStore;
      dispatch(
        uiActions.setActiveMapIdForDialog({
          type: uiDialogType,
          mapId,
          setOpen,
          source,
        }),
      );
    }, [
      currentActiveMapId,
      dispatch,
      isOpenInStore,
      uiDialogType,
      mapId,
      source,
    ]);

    const isOpen = currentActiveMapId === mapId && isOpenInStore;

    return (
      <DimensionSelectButton
        dimension={dimension}
        onClickDimensionButton={openMultiDimensionDialog}
        isActive={isOpen}
      />
    );
  };

export default DimensionSelectMapButtonConnect;
