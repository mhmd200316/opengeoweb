/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { ToolContainerDraggable } from '@opengeoweb/shared';
import { Grid } from '@mui/material';

import { layerActions, mapSelectors } from '../../store';
import { AppStore } from '../../types/types';
import DimensionSelectSliderConnect from './DimensionSelectSliderConnect';
import { DimensionConfig, dimensionConfig } from './MultiDimensionSelectConfig';
import { Source } from '../../store/ui/types';
import { LayerActionOrigin } from '../../store/mapStore/types';

interface DimensionSelectDialogConnectProps {
  mapId: string;
  dimensionName: string;
  isOpen: boolean;
  order?: number;
  onMouseDown?: () => void;
  onToggleDialog?: (isDialogOpen: boolean) => void;
  index?: number;
  source?: Source;
}

const DimensionSelectDialogConnect: React.FC<DimensionSelectDialogConnectProps> =
  ({
    mapId,
    dimensionName,
    isOpen,
    order = 0,
    onMouseDown,
    onToggleDialog,
    index = 0,
    source = 'app',
  }: DimensionSelectDialogConnectProps) => {
    const dispatch = useDispatch();

    const mapDimension = useSelector((store: AppStore) =>
      mapSelectors.getMapDimension(store, mapId, dimensionName),
    );

    const layerIds = useSelector((store: AppStore) =>
      mapSelectors.getLayerIds(store, mapId),
    );

    const handleDimensionValueChanged = React.useCallback(
      (layerId: string, dimensionName: string, newValue: string): void => {
        dispatch(
          layerActions.layerChangeDimension({
            origin: LayerActionOrigin.layerManager,
            layerId,
            dimension: {
              name: dimensionName,
              currentValue: newValue,
            },
            mapId,
          }),
        );
      },
      [dispatch, mapId],
    );

    const handleSyncChanged = React.useCallback(
      (layerId, dimensionName, dimensionValue, synced): void => {
        dispatch(
          layerActions.layerChangeDimension({
            origin: LayerActionOrigin.layerManager,
            layerId,
            dimension: {
              name: dimensionName,
              currentValue: dimensionValue,
              synced,
            },
          }),
        );
      },
      [dispatch],
    );

    if (!mapDimension || !layerIds || layerIds.length < 1) {
      return null;
    }

    const dimConfig: DimensionConfig = dimensionConfig.find(
      (cnf) => cnf.name === dimensionName,
    );
    const title = dimConfig ? dimConfig.label : dimensionName;

    const topPos = index * 32 + 164; // index * btn height + offset top of window
    return (
      <ToolContainerDraggable
        startPosition={{ left: 50, top: topPos }}
        minWidth={180}
        title={title}
        headerSize="small"
        isOpen={isOpen}
        data-testid={`multidimensionContent-${title}`}
        onClose={(): void => onToggleDialog(false)}
        bounds="parent"
        onMouseDown={onMouseDown}
        order={order}
        source={source}
      >
        <div
          style={{
            padding: 4,
          }}
        >
          <Grid container direction="row">
            {layerIds.map((layerId) => (
              <DimensionSelectSliderConnect
                key={layerId}
                handleDimensionValueChanged={handleDimensionValueChanged}
                handleSyncChanged={handleSyncChanged}
                layerId={layerId}
                dimConfig={dimConfig}
                dimensionName={dimensionName}
              />
            ))}
          </Grid>
        </div>
      </ToolContainerDraggable>
    );
  };
export default DimensionSelectDialogConnect;
