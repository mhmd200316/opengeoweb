/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { MapView, MapViewLayer } from '.';
import { generateMapId } from '../../store/mapStore/utils/helpers';
import {
  baseLayer,
  obsAirTemperature,
  overLayer,
  radarLayerWithError,
} from '../../utils/publicLayers';
import { CoreThemeProvider } from '../Providers/Providers';

export const MapError = (): React.ReactElement => (
  <CoreThemeProvider>
    <div
      style={{
        top: '10px',
        left: '50px',
        position: 'absolute',
        padding: '20px',
        background: '#DDD',
        zIndex: 10000,
      }}
    >
      The console panel of your browser shows the error produced by this layer.
    </div>
    <div style={{ height: '100vh' }}>
      <MapView mapId={generateMapId()}>
        <MapViewLayer {...baseLayer} />
        <MapViewLayer
          {...radarLayerWithError}
          onLayerError={(_, error): void => {
            // eslint-disable-next-line no-console
            console.log('Layer with error', error);
          }}
        />
        <MapViewLayer {...overLayer} />
      </MapView>
    </div>
  </CoreThemeProvider>
);

MapError.storyName = 'Map with a layer which has an error';

export const MapWithDuplicateLayerIdsError = (): React.ReactElement => (
  <CoreThemeProvider>
    <div
      style={{
        top: '10px',
        left: '50px',
        position: 'absolute',
        padding: '20px',
        background: '#DDD',
        zIndex: 10000,
      }}
    >
      The console panel of your browser shows the error produced by this layer.
    </div>
    <div style={{ height: '100vh' }}>
      <MapView mapId={generateMapId()}>
        <MapViewLayer {...baseLayer} />
        <MapViewLayer
          {...obsAirTemperature}
          onLayerError={(_, error): void => {
            // eslint-disable-next-line no-console
            console.log('Layer with error', error);
          }}
        />
        <MapViewLayer
          {...obsAirTemperature}
          onLayerError={(_, error): void => {
            // eslint-disable-next-line no-console
            console.log('Layer with error', error);
          }}
        />
        <MapViewLayer {...overLayer} />
      </MapView>
    </div>
  </CoreThemeProvider>
);

MapWithDuplicateLayerIdsError.storyName =
  "Map with duplicate layers id's has an error";
