/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { Button, Paper, DialogContent, Grid } from '@mui/material';

import makeStyles from '@mui/styles/makeStyles';

import TimeSliderConnect from '../TimeSlider/TimeSliderConnect';
import { MapView, MapViewConnect, MapViewLayer } from '.';
import { store } from '../../storybookUtils/store';
import { LegendConnect, LegendMapButtonConnect } from '../Legend';
import { radarLayer, overLayer, baseLayerGrey } from '../../utils/publicLayers';

import { layerActions, mapActions } from '../../store';
import {
  generateLayerId,
  generateMapId,
} from '../../store/mapStore/utils/helpers';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../LayerManager';

import { CoreThemeStoreProvider } from '../Providers/Providers';
import { MapControls } from '../MapControls';
import {
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
} from '../MultiMapDimensionSelect';

const initialBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: -450651.2255879827,
    bottom: 6490531.093143953,
    right: 1428345.8183648037,
    top: 7438773.776232235,
  },
};

const useStyles = makeStyles({
  dialogChildren: {
    display: 'flex',
    padding: 20,
    minHeight: 200,
    minWidth: 400,
    height: '100%',
  },
  dialog: {
    position: 'absolute',
    top: '5%',
    left: '10%',
    maxWidth: '90%',
    maxHeight: '95%',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
    zIndex: 650,
  },
});

const connectRedux = connect(null, {
  setLayers: layerActions.setLayers,
  setBaseLayers: layerActions.setBaseLayers,
  setBbox: mapActions.setBbox,
});

interface MapWithGeoWebLayerSelectProps {
  mapId: string;
  setLayers: typeof layerActions.setLayers;
  setBaseLayers: typeof layerActions.setBaseLayers;
  setBbox: typeof mapActions.setBbox;
}

const GeowebFakeModuleDemo: React.FC<MapWithGeoWebLayerSelectProps> = ({
  mapId,
  setLayers,
  setBaseLayers,
  setBbox,
}: MapWithGeoWebLayerSelectProps) => {
  React.useEffect(() => {
    setLayers({ layers: [radarLayer], mapId });
    setBaseLayers({
      mapId,
      layers: [baseLayerGrey, overLayer],
    });
    setBbox({
      bbox: initialBbox.bbox,
      srs: initialBbox.srs,
      mapId,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div style={{ height: '100vh' }}>
      <MapControls>
        <LayerManagerMapButtonConnect mapId={mapId} />
        <LegendMapButtonConnect mapId={mapId} />
        <MultiDimensionSelectMapButtonsConnect mapId={mapId} />
      </MapControls>

      <LegendConnect mapId={mapId} />

      <MultiMapDimensionSelectConnect />
      <MapViewConnect mapId={mapId} displayTimeInMap />
      <LayerManagerConnect />

      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 1000,
          width: '100%',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
    </div>
  );
};

const ConnectedGeoweb = connectRedux(GeowebFakeModuleDemo);

interface ModuleProps {
  handleClose: () => void;
}
const FakeModuleDialog: React.FC<ModuleProps> = ({
  handleClose,
}: ModuleProps) => {
  const classes = useStyles();
  const mapId = React.useRef(`sigmet-${generateMapId()}`).current;

  return (
    <Paper className={classes.dialog} elevation={24}>
      <Button onClick={handleClose} color="secondary" size="medium">
        CLOSE
      </Button>
      <DialogContent className={classes.dialogChildren}>
        <Grid container>
          <MapView mapId={mapId} srs={initialBbox.srs} bbox={initialBbox.bbox}>
            <MapViewLayer {...baseLayerGrey} id={generateLayerId()} />
          </MapView>
        </Grid>
      </DialogContent>
    </Paper>
  );
};

export const MapModuleOverDefaultMap = (): React.ReactElement => {
  const [showSigmet, setShowSigmet] = React.useState(false);
  const mapId = React.useRef(`app-${generateMapId()}`).current;

  return (
    <CoreThemeStoreProvider store={store}>
      <ConnectedGeoweb mapId={mapId} />
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 51,
        }}
      >
        <Button
          variant="contained"
          color="primary"
          onClick={(): void => setShowSigmet(true)}
        >
          Open Module
        </Button>
      </div>
      {showSigmet && (
        <FakeModuleDialog handleClose={(): void => setShowSigmet(false)} />
      )}
    </CoreThemeStoreProvider>
  );
};
