/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  Button,
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  Grid,
  Box,
} from '@mui/material';
import MapViewConnect from './MapViewConnect';
import MapViewLayer from './MapViewLayer';
import { store } from '../../storybookUtils/store';

import {
  radarLayer,
  dwdWarningLayer,
  harmoniePressure,
  harmoniePrecipitation,
} from '../../utils/publicLayers';

import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { ActionCard } from '../../storybookUtils/HelperComponents';
import { layerActions } from '../../store';
import {
  useDrawPolyStoryStyles,
  useGeoJSON,
} from '../../storybookUtils/storyComponents/MapDrawGeoJSON';
import { simplePolygonGeoJSON } from '../../storybookUtils/geojsonExamples';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { AppStore, mapActions, mapSelectors } from '../..';

interface PresetsConnectProps {
  mapId: string;
}

const PresetsConnect: React.FC<PresetsConnectProps> = ({
  mapId,
}: PresetsConnectProps) => {
  const dispatch = useDispatch();
  const onSetLayers = ({ layers }): void => {
    dispatch(layerActions.setLayers({ layers, mapId }));
  };

  return (
    <ActionCard
      name="setLayers"
      exampleLayers={[
        {
          layers: [radarLayer],
          title: 'Radar',
        },
        {
          layers: [harmoniePrecipitation, harmoniePressure],
          title: 'Precip + Obs',
        },
        {
          layers: [radarLayer, dwdWarningLayer],
          title: 'Radar + DWD Warnings',
        },
      ]}
      description="sets new layers on a map while removing all current ones"
      onClickBtn={onSetLayers}
    />
  );
};

const Demo: React.FC = () => {
  const mapId = 'mapid_1';
  const classes = useDrawPolyStoryStyles();
  const {
    geojson,
    isInEditMode,
    currentFeatureNrToEdit,
    drawMode,
    changeDrawMode,
    editModes,
    setGeojson,
    setEditMode,
  } = useGeoJSON();

  useDefaultMapSettings({
    mapId,
    bbox: {
      left: -2226405.5515279276,
      bottom: 3505518.443190464,
      right: 5163689.822338379,
      top: 13024282.593224784,
    },
  });

  const selectedFeatureIndex = useSelector((store: AppStore) =>
    mapSelectors.getSelectedFeatureIndex(store, mapId),
  );

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(
      mapActions.addLayer({
        mapId,
        layer: { geojson: simplePolygonGeoJSON },
        layerId: 'layerId123',
        origin: 'MapViewConnect.FeatureLayers.stories.tsx',
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={classes.MapDrawGeoJSONContainer}>
      <div className={classes.MapDrawGeoJSONMapContainer}>
        <MapViewConnect mapId={mapId}>
          <MapViewLayer
            id="geojson-layer"
            geojson={geojson}
            isInEditMode={isInEditMode}
            drawMode={drawMode}
            updateGeojson={(updatedGeojson): void => {
              setGeojson(updatedGeojson);
            }}
            exitDrawModeCallback={(): void => {
              setEditMode(!isInEditMode);
            }}
            featureNrToEdit={currentFeatureNrToEdit}
          />
        </MapViewConnect>
      </div>
      <div className={classes.MapDrawGeoJSONControlsContainer}>
        <Box p={2}>
          <Grid spacing={2} container>
            <Grid item sm={12}>
              <FormControl variant="filled" style={{ minWidth: 120 }}>
                <InputLabel id="demo-feature-type">Feature type</InputLabel>
                <Select
                  labelId="demo-feature-type"
                  value={drawMode}
                  onChange={(
                    event: React.ChangeEvent<HTMLInputElement>,
                  ): void => {
                    changeDrawMode(event.target.value);
                  }}
                >
                  {editModes.map((mode) => (
                    <MenuItem key={mode.key} value={mode.key}>
                      {mode.value}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>

            <Grid item sm={12}>
              <Button
                variant="contained"
                color="primary"
                onClick={(): void => {
                  setEditMode(!isInEditMode);
                  if (!isInEditMode) {
                    // reset
                    setGeojson({
                      type: 'FeatureCollection',
                      features: [
                        {
                          geometry: { type: 'LineString', coordinates: [] },
                          properties: {
                            stroke: '#66F',
                            'stroke-opacity': '1',
                            'stroke-width': 5,
                          },
                          type: 'Feature',
                        },
                      ],
                    });
                  }
                }}
              >
                {isInEditMode ? 'Finish edit' : 'Start edit'}
              </Button>
            </Grid>
            <Grid item sm={12}>
              <PresetsConnect mapId={mapId} />
            </Grid>
            {selectedFeatureIndex !== undefined && (
              <Grid item sm={12}>
                {JSON.stringify(
                  simplePolygonGeoJSON.features[selectedFeatureIndex],
                )}
              </Grid>
            )}
          </Grid>
        </Box>
      </div>
    </div>
  );
};

export const FeatureLayers = (): React.ReactElement => {
  return (
    <CoreThemeStoreProvider store={store}>
      <Demo />
    </CoreThemeStoreProvider>
  );
};
