/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { MapViewConnect } from '.';

export * from './MapViewConnect.MapModuleOverDefaultMap.stories';
export * from './MapViewConnect.DoubleMap.stories';
export * from './MapViewConnect.Errors.stories';
export * from './MapViewConnect.InitialBBoxAndWithout.stories';
export * from './MapViewConnect.LayerActions.stories';
export * from './MapViewConnect.LayerChangeEnabledOpacity.stories';
export * from './MapViewConnect.SetBaseLayers.stories';
export * from './MapViewConnect.ToggleControls.stories';
export * from './MapViewConnect.FeatureLayers.stories';
export * from './MapViewConnect.MapPin.stories';
export * from './MapViewConnect.SelectorTest.stories';

export default {
  title: 'components/MapView/MapViewConnect',
  component: MapViewConnect,
};
