/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { MapView, MapViewLayer } from '.';
import { generateMapId } from '../../store/mapStore/utils/helpers';
import { baseLayer, radarLayer, overLayer } from '../../utils/publicLayers';

export const PassiveMap = (): React.ReactElement => (
  <div style={{ height: '100vh' }}>
    <MapView
      mapId={generateMapId()}
      controls={{}}
      showScaleBar={false}
      passiveMap
      // eslint-disable-next-line no-console
      onClick={(): void => console.log('Passive map clicked')}
    >
      <MapViewLayer {...baseLayer} />
      <MapViewLayer
        {...radarLayer}
        onLayerReady={(layer): void => {
          layer.zoomToLayer();
        }}
      />
      <MapViewLayer {...overLayer} />
    </MapView>
  </div>
);

PassiveMap.storyName = 'Passive MapView without controls';
