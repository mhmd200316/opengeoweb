/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Paper, Card, CardContent, Typography } from '@mui/material';

import { MapView, MapViewLayer } from '.';
import { generateMapId } from '../../store/mapStore/utils/helpers';
import { baseLayer, overLayer } from '../../utils/publicLayers';
import { CoreThemeProvider } from '../Providers/Providers';

const TextboxDiv = (props: {
  text: {
    title: string;
    body: string;
  };
}): React.ReactElement => {
  const { text } = props;
  return (
    <div
      style={{
        position: 'absolute',
        left: '50px',
        top: '10px',
        zIndex: 10000,
      }}
    >
      <Paper>
        <Card>
          <CardContent>
            <Typography variant="subtitle1">{text.title}</Typography>
            {text.body !== '' && (
              <Typography variant="body2">{text.body}</Typography>
            )}
          </CardContent>
        </Card>
      </Paper>
    </div>
  );
};

export const SetProjectionBBox = (): React.ReactElement => (
  <CoreThemeProvider>
    <div style={{ height: '100vh' }}>
      <div style={{ display: 'flex', height: '50%' }}>
        <div style={{ position: 'relative', width: '50%', height: '100%' }}>
          <TextboxDiv text={{ title: 'Default SRS and BBOX', body: '' }} />
          {/* 'Europe North Pole' */}
          <MapView mapId={generateMapId()}>
            <MapViewLayer {...baseLayer} />
            <MapViewLayer {...overLayer} />
          </MapView>
        </div>
        <div style={{ position: 'relative', width: '50%', height: '100%' }}>
          <TextboxDiv
            text={{ title: 'Europe stereographic', body: 'EPSG:32661' }}
          />
          <MapView
            mapId={generateMapId()}
            srs="EPSG:32661"
            bbox={{
              left: -2776118.977564746,
              bottom: -6499490.259201691,
              right: 9187990.785775745,
              top: 971675.53185069,
            }}
          >
            <MapViewLayer {...baseLayer} />
            <MapViewLayer {...overLayer} />
          </MapView>
        </div>
      </div>
      <div style={{ display: 'flex', height: '50%' }}>
        <div style={{ position: 'relative', width: '50%', height: '100%' }}>
          <TextboxDiv
            text={{ title: 'Southern Hemisphere', body: 'EPSG:3412' }}
          />
          {/* 'Southern Hemisphere' */}
          <MapView
            mapId={generateMapId()}
            srs="EPSG:3412"
            bbox={{
              left: -4589984.273212382,
              bottom: -2752857.546211313,
              right: 5425154.657417289,
              top: 2986705.2537886878,
            }}
          >
            <MapViewLayer {...baseLayer} />
            <MapViewLayer {...overLayer} />
          </MapView>
        </div>
        <div style={{ position: 'relative', width: '50%', height: '100%' }}>
          <TextboxDiv
            text={{ title: 'Northern Hemisphere', body: 'EPSG:3411' }}
          />
          {/* Northern Hemisphere */}
          <MapView
            mapId={generateMapId()}
            srs="EPSG:3411"
            bbox={{
              left: -5661541.927991125,
              bottom: -3634073.745615984,
              right: 5795287.923063262,
              top: 2679445.334384017,
            }}
          >
            <MapViewLayer {...baseLayer} />
            <MapViewLayer {...overLayer} />
          </MapView>
        </div>
      </div>
    </div>
  </CoreThemeProvider>
);

SetProjectionBBox.storyName = 'Set projection and BBOX';
