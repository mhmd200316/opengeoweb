/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Paper,
  FormControl,
  InputLabel,
  Select,
  Box,
  MenuItem,
} from '@mui/material';

import { MapView, MapViewLayer } from '.';
import { generateMapId } from '../../store/mapStore/utils/helpers';
import { baseLayer, overLayer } from '../../utils/publicLayers';
import { CoreThemeProvider } from '../Providers/Providers';

export const SetProjectionBBoxLocalState = (): React.ReactElement => {
  const [srsAndBBOX, setSrsAndBBOX] = React.useState({
    srs: 'EPSG:3575',
    bbox: {
      left: -13000000,
      bottom: -13000000,
      right: 13000000,
      top: 13000000,
    },
  });

  const handleChange = (srsValue: string): void => {
    switch (srsValue) {
      case 'EPSG:32661':
        setSrsAndBBOX({
          srs: 'EPSG:32661',
          bbox: {
            left: -2776118.977564746,
            bottom: -6499490.259201691,
            right: 9187990.785775745,
            top: 971675.53185069,
          },
        });
        break;
      case 'EPSG:3412':
        setSrsAndBBOX({
          srs: 'EPSG:3412',
          bbox: {
            left: -4589984.273212382,
            bottom: -2752857.546211313,
            right: 5425154.657417289,
            top: 2986705.2537886878,
          },
        });
        break;
      case 'EPSG:3411':
        setSrsAndBBOX({
          srs: 'EPSG:3411',
          bbox: {
            left: -5661541.927991125,
            bottom: -3634073.745615984,
            right: 5795287.923063262,
            top: 2679445.334384017,
          },
        });
        break;
      case 'EPSG:3857':
        setSrsAndBBOX({
          srs: 'EPSG:3857',
          bbox: {
            left: -19000000,
            bottom: -19000000,
            right: 19000000,
            top: 19000000,
          },
        });
        break;
      case 'EPSG:54030':
        setSrsAndBBOX({
          srs: 'EPSG:54030',
          bbox: {
            left: -17036744.451383516,
            bottom: -10711364.114367772,
            right: 16912038.081015453,
            top: 10488456.659686875,
          },
        });
        break;
      case 'EPSG:28992':
        setSrsAndBBOX({
          srs: 'EPSG:28992',
          bbox: {
            left: -350000,
            bottom: 125000,
            right: 700000,
            top: 900000,
          },
        });
        break;
      default:
        setSrsAndBBOX({
          srs: 'EPSG:3575',
          bbox: {
            left: -13000000,
            bottom: -13000000,
            right: 13000000,
            top: 13000000,
          },
        });
    }
  };

  return (
    <CoreThemeProvider>
      <div style={{ height: '100vh' }}>
        {/* 'Europe North Pole' */}
        <MapView
          mapId={generateMapId()}
          srs={srsAndBBOX.srs}
          bbox={srsAndBBOX.bbox}
          onMapZoomEnd={(bboxPayload): void => {
            // eslint-disable-next-line no-console
            console.log('onMapZoomEnd', JSON.stringify(bboxPayload));
          }}
        >
          <MapViewLayer {...baseLayer} />
          <MapViewLayer {...overLayer} />
        </MapView>
      </div>
      <div
        style={{
          position: 'absolute',
          top: '20px',
          left: '50px',
          zIndex: 1000,
        }}
      >
        <Box>
          <Paper>
            <FormControl variant="filled">
              <InputLabel id="demo-select-SRS-label">Select SRS</InputLabel>
              <Select
                labelId="demo-select-SRS-label"
                id="demo-select-SRS"
                value={srsAndBBOX.srs}
                onChange={(event): void => {
                  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                  // @ts-ignore
                  handleChange(event.target.value);
                }}
              >
                <MenuItem value="EPSG:3575">Europe North Pole</MenuItem>
                <MenuItem value="EPSG:3411">Northern Hemisphere</MenuItem>
                <MenuItem value="EPSG:3412">Southern Hemisphere</MenuItem>
                <MenuItem value="EPSG:32661">Europe stereographic</MenuItem>
                <MenuItem value="EPSG:3857">World Mercator</MenuItem>
                <MenuItem value="EPSG:54030">Robinson</MenuItem>
                <MenuItem value="EPSG:28992">The Netherlands (28992)</MenuItem>
              </Select>
            </FormControl>
          </Paper>
        </Box>
      </div>
    </CoreThemeProvider>
  );
};

SetProjectionBBoxLocalState.storyName =
  'Set projection and BBOX via local state';
