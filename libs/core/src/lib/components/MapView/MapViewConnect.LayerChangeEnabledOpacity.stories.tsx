/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Slider, Paper, Box, Grid, Button } from '@mui/material';
import { store } from '../../storybookUtils/store';

import MapViewConnect from './MapViewConnect';
import { layerActions } from '../../store';

import { radarLayer } from '../../utils/publicLayers';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { AppStore } from '../../types/types';
import { Layer } from '../../store/mapStore/types';
import { mapSelectors } from '../../..';
import { CoreThemeStoreProvider } from '../Providers/Providers';

export interface LayerChangeOpacityInputProps {
  mapId: string;
  layerChangeOpacity?: typeof layerActions.layerChangeOpacity;
  layers?: Layer[];
}

const connectRedux = connect(
  (state: AppStore, props: LayerChangeOpacityInputProps) => ({
    layers: mapSelectors.getMapLayers(state, props.mapId),
  }),
  {
    layerChangeOpacity: layerActions.layerChangeOpacity,
  },
);

/* Just a button inside a component to connect it to redux */

const LayerChangeOpacityInput: React.FC<LayerChangeOpacityInputProps> = ({
  layers,
  layerChangeOpacity,
}: LayerChangeOpacityInputProps) => {
  if (layers.length === 0) return <div>No layers</div>;
  const layerOpacity = layers[0].opacity;
  const layerId = layers[0].id;

  return (
    <div
      style={{
        width: '200px',
      }}
    >
      <Box>
        <Paper>
          <Box p={2}>
            <Grid spacing={2} container direction="column">
              <Grid item sm={12}>
                <Slider
                  min={0}
                  max={1}
                  step={0.1}
                  value={layerOpacity}
                  onChange={(_event: Event, value: number): void => {
                    layerChangeOpacity({
                      layerId,
                      opacity: value,
                    });
                  }}
                />
              </Grid>
              <Grid item sm={12}>
                <span>
                  Current opacity:
                  {layers[0].opacity}
                </span>
              </Grid>
            </Grid>
          </Box>
        </Paper>
      </Box>
    </div>
  );
};

const ConnectedLayerChangeOpacityInput = connectRedux(LayerChangeOpacityInput);

// Enable button
const connectReduxEnable = connect(
  (state, props: { mapId: string }) => ({
    layers: mapSelectors.getMapLayers(state, props.mapId),
  }),
  {
    layerChangeEnabled: layerActions.layerChangeEnabled,
  },
);

type Props = ConnectedProps<typeof connectReduxEnable> & { mapId: string };

const LayerEnableButton = connectReduxEnable(
  ({
    layers,

    layerChangeEnabled,
    mapId,
  }: Props) => {
    useDefaultMapSettings({ mapId, layers: [radarLayer] });

    if (layers.length === 0) return <div>No layers</div>;
    const isLayerEnabled = layers[0].enabled;
    const layerId = layers[0].id;
    return (
      <div>
        <Button
          variant="contained"
          color="primary"
          onClick={(): void => {
            layerChangeEnabled({
              layerId,
              mapId,
              enabled: !isLayerEnabled,
            });
          }}
        >
          {!isLayerEnabled ? 'Enable' : 'Disable'}
        </Button>
      </div>
    );
  },
);

export const LayerChangeEnableOpacityAction = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <div style={{ height: '100vh' }}>
      <MapViewConnect mapId="mapid_1" />
    </div>
    <div
      style={{
        position: 'absolute',
        left: '50px',
        top: '10px',
        zIndex: 10000,
      }}
    >
      <LayerEnableButton mapId="mapid_1" />
    </div>
    <div
      style={{
        position: 'absolute',
        left: '50px',
        top: '60px',
        zIndex: 10000,
      }}
    >
      <ConnectedLayerChangeOpacityInput mapId="mapid_1" />
    </div>
  </CoreThemeStoreProvider>
);
