/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import {
  Button,
  ButtonGroup,
  Paper,
  Box,
  Grid,
  Checkbox,
  FormControlLabel,
  List,
  ListItem,
  Slider,
} from '@mui/material';

import { MapViewConnect } from '.';

import { layerActions, layerSelectors, mapSelectors } from '../../store';
import tilesettings from '../../store/mapStore/utils/tilesettings';

import { baseLayer, radarLayer } from '../../utils/publicLayers';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';

import { store } from '../../storybookUtils/store';
import { LayerType } from '../../store/mapStore/types';
import { AppStore } from '../../..';
import { CoreThemeStoreProvider } from '../Providers/Providers';

interface LayerSliderProps {
  opacity: number;
  layerId: string;
  enabled: boolean;
  name: string;
  onChangeOpacity: typeof layerActions.layerChangeOpacity;
  onChangeEnabled: typeof layerActions.layerChangeEnabled;
}

const LayerSlider: React.FC<LayerSliderProps> = ({
  opacity,
  layerId,
  enabled,
  name,
  onChangeOpacity,
  onChangeEnabled,
}: LayerSliderProps) => (
  <ListItem dense>
    <Grid container direction="column">
      <Grid item sm={12}>
        <Box>
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                inputProps={{ 'aria-label': 'secondary checkbox' }}
                checked={enabled}
                onChange={(_, newEnabled): void => {
                  onChangeEnabled({ layerId, enabled: newEnabled });
                }}
              />
            }
            label={name}
          />
        </Box>
      </Grid>
      <Grid item sm={12}>
        <Slider
          min={0}
          max={1}
          step={0.1}
          value={opacity}
          onChange={(_event: Event, newOpacity: number): void => {
            onChangeOpacity({
              layerId,
              opacity: newOpacity,
            });
          }}
        />
      </Grid>
    </Grid>
  </ListItem>
);

const LayerSliderConnect = connect(
  (state, props: { layerId: string }) => ({
    opacity: layerSelectors.getLayerOpacity(state, props.layerId),
    enabled: layerSelectors.getLayerEnabled(state, props.layerId),
    name: layerSelectors.getLayerName(state, props.layerId),
  }),
  {
    onChangeOpacity: layerActions.layerChangeOpacity,
    onChangeEnabled: layerActions.layerChangeEnabled,
  },
)(LayerSlider);

const connectRedux = connect(
  (state: AppStore, props: ExampleComponentProps) => ({
    layerIds: mapSelectors.getLayerIds(state, props.mapId),
    baseLayerIds: mapSelectors.getMapBaseLayersIds(state, props.mapId),
  }),
  {
    setBaseLayers: layerActions.setBaseLayers,
  },
);

interface ExampleComponentProps {
  setBaseLayers?: typeof layerActions.setBaseLayers;
  layerIds?: string[];
  baseLayerIds?: string[];
  mapId: string;
}

const ExampleComponent = connectRedux(
  ({ layerIds, baseLayerIds, setBaseLayers, mapId }: ExampleComponentProps) => {
    useDefaultMapSettings({ mapId, layers: [radarLayer] });

    return (
      <>
        <div style={{ height: '100vh' }}>
          <MapViewConnect mapId={mapId} />
        </div>
        <div
          style={{
            position: 'absolute',
            left: '50px',
            top: '10px',
            zIndex: 10000,
          }}
        >
          <Paper>
            <Box p={1}>
              <ButtonGroup
                variant="text"
                orientation="vertical"
                color="primary"
                aria-label="vertical outlined primary button group"
              >
                <Button
                  onClick={(): void => {
                    setBaseLayers({
                      layers: [
                        { ...baseLayer, layerType: LayerType.baseLayer },
                      ],
                      mapId,
                    });
                  }}
                >
                  SetBaseLayer ArcGIS satellite
                </Button>
              </ButtonGroup>
              <p style={{ margin: '0px' }}>From tilesettings:</p>
              <ButtonGroup
                variant="text"
                orientation="vertical"
                color="primary"
                aria-label="vertical outlined primary button group"
              >
                {Object.keys(tilesettings).map((tilesettingName, tileIndex) => {
                  if (!tilesettings[tilesettingName]['EPSG:3857']) return null;
                  return (
                    <Button
                      key={tilesettingName}
                      onClick={(): void => {
                        setBaseLayers({
                          layers: [
                            {
                              name: tilesettingName,
                              type: 'twms',
                              id: `tilesetting-${tilesettingName}-${tileIndex}`,
                              layerType: LayerType.baseLayer,
                            },
                          ],
                          mapId,
                        });
                      }}
                    >
                      {tilesettingName}
                    </Button>
                  );
                })}
              </ButtonGroup>
            </Box>
          </Paper>
        </div>
        <div
          style={{
            width: '300px',
            position: 'absolute',
            right: '10px',
            top: '10px',
            zIndex: 10000,
          }}
        >
          <Box>
            <Paper>
              <Box p={2}>
                <Grid spacing={2} container direction="column">
                  <Grid item sm={12}>
                    <p>Layers:</p>
                    <List>
                      {layerIds.map((layerId) => (
                        <LayerSliderConnect key={layerId} layerId={layerId} />
                      ))}
                    </List>
                  </Grid>
                </Grid>
              </Box>
              <Box p={2}>
                <Grid spacing={2} container direction="column">
                  <Grid item sm={12}>
                    <p>BaseLayers:</p>
                    <List>
                      {baseLayerIds.map((layerId) => (
                        <LayerSliderConnect key={layerId} layerId={layerId} />
                      ))}
                    </List>
                  </Grid>
                </Grid>
              </Box>
            </Paper>
          </Box>
        </div>
      </>
    );
  },
);

export const SetBaseLayersAction: React.FC = () => (
  <CoreThemeStoreProvider store={store}>
    <ExampleComponent mapId="mapid_1" />
  </CoreThemeStoreProvider>
);
