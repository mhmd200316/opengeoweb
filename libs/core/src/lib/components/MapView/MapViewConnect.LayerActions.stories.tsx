/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

import { Chip } from '@mui/material';

import { AppStore } from '../../types/types';
import { layerActions, mapSelectors } from '../../store';
import MapViewConnect from './MapViewConnect';
import { store } from '../../storybookUtils/store';

import {
  msgCppLayer,
  radarLayer,
  dwdWarningLayer,
  harmoniePressure,
  harmoniePrecipitation,
} from '../../utils/publicLayers';

import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { generateLayerId } from '../../..';
import { ActionCard, useStyles } from '../../storybookUtils/HelperComponents';
import { CoreThemeStoreProvider } from '../Providers/Providers';

const connectRedux = connect(
  (state: AppStore, props: { mapId: string }) => ({
    layerIds: mapSelectors.getLayerIds(state, props.mapId),
  }),
  {
    setLayers: layerActions.setLayers,
    addLayer: layerActions.addLayer,
    deleteLayer: layerActions.layerDelete,
  },
);

type Props = ConnectedProps<typeof connectRedux> & { mapId: string };
const Presets = connectRedux(
  ({ mapId, setLayers, addLayer, deleteLayer, layerIds }: Props) => {
    useDefaultMapSettings({ mapId });
    const classes = useStyles();

    return (
      <>
        <ActionCard
          name="setLayers"
          exampleLayers={[
            {
              layers: [radarLayer],
              title: 'Radar',
            },
            {
              layers: [harmoniePrecipitation, harmoniePressure],
              title: 'Precip + Obs',
            },
            {
              layers: [radarLayer, dwdWarningLayer],
              title: 'Radar + DWD Warnings',
            },
          ]}
          description="sets new layers on a map while removing all current ones"
          onClickBtn={({ layers }): void => {
            setLayers({ layers, mapId });
          }}
        />

        <ActionCard
          name="addLayer"
          exampleLayers={[
            {
              layers: [radarLayer],
              title: 'Radar',
            },
            {
              layers: [msgCppLayer],
              title: 'MSGCPP',
            },
            {
              layers: [dwdWarningLayer],
              title: 'DWD Warnings',
            },
          ]}
          description="adds a new layer on a map while keeping the current ones"
          onClickBtn={({ layers }): void => {
            addLayer({
              layer: layers[0],
              layerId: generateLayerId(),
              mapId,
              origin: 'story',
            });
          }}
        />

        <ActionCard name="removeLayer" description="removes a layer on a map ">
          <div className={classes.chips}>
            {layerIds.map((layerId) => (
              <Chip
                key={layerId}
                label={layerId}
                onDelete={(): void => {
                  deleteLayer({ layerId, mapId, layerIndex: 0 });
                }}
                color="secondary"
              />
            ))}
          </div>
        </ActionCard>
      </>
    );
  },
);

export const LayerActions = (): React.ReactElement => {
  return (
    <CoreThemeStoreProvider store={store}>
      <div style={{ height: '100vh' }}>
        <MapViewConnect mapId="mapid_1" />
      </div>
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 99910000,
        }}
      >
        <Presets mapId="mapid_1" />
      </div>
    </CoreThemeStoreProvider>
  );
};
