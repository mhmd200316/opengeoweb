/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect, ConnectedProps, useSelector } from 'react-redux';
import { Slider, Paper, Box, Grid, Button } from '@mui/material';

import { store } from '../../storybookUtils/store';

import MapViewConnect, { MapViewConnectProps } from './MapViewConnect';
import { layerActions } from '../../store';

import { radarLayer } from '../../utils/publicLayers';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { AppStore } from '../../types/types';
import { mapSelectors } from '../../..';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { LayerChangeOpacityInputProps } from './MapViewConnect.LayerChangeEnabledOpacity.stories';
import { generateLayerId } from '../..';

const connectRedux = connect(
  (state: AppStore, props: LayerChangeOpacityInputProps) => ({
    layers: mapSelectors.getMapLayers(state, props.mapId),
  }),
  {
    layerChangeOpacity: layerActions.layerChangeOpacity,
  },
);

/* Just a button inside a component to connect it to redux */

const LayerChangeOpacityInput: React.FC<LayerChangeOpacityInputProps> = ({
  layers,
  layerChangeOpacity,
}: LayerChangeOpacityInputProps) => {
  if (layers.length === 0) return <div>No layers</div>;
  const layerOpacity = layers[0].opacity;
  const layerId = layers[0].id;

  return (
    <div
      style={{
        width: '200px',
      }}
    >
      <Box>
        <Paper>
          <Box p={2}>
            <Grid spacing={2} container direction="column">
              <Grid item sm={12}>
                <Slider
                  min={0}
                  max={1}
                  step={0.1}
                  value={layerOpacity}
                  onChange={(_event: Event, value: number): void => {
                    layerChangeOpacity({
                      layerId,
                      opacity: value,
                    });
                  }}
                />
              </Grid>
              <Grid item sm={12}>
                <span>
                  Current opacity:
                  {layers[0].opacity}
                </span>
              </Grid>
            </Grid>
          </Box>
        </Paper>
      </Box>
    </div>
  );
};

const ConnectedLayerChangeOpacityInput = connectRedux(LayerChangeOpacityInput);

// Enable button
const connectReduxEnable = connect(
  (state, props: { mapId: string }) => ({
    layers: mapSelectors.getMapLayers(state, props.mapId),
  }),
  {
    layerChangeEnabled: layerActions.layerChangeEnabled,
  },
);

type Props = ConnectedProps<typeof connectReduxEnable> & { mapId: string };

const LayerEnableButton = connectReduxEnable(
  ({ layers, layerChangeEnabled, mapId }: Props) => {
    useDefaultMapSettings({
      mapId,
      layers: [
        {
          ...radarLayer,
          id: generateLayerId(),
          style: 'precip-with-range/nearest',
        },
      ],
    });

    if (layers.length === 0) return <div>No layers</div>;
    const isLayerEnabled = layers[0].enabled;
    const layerId = layers[0].id;
    return (
      <div>
        <Button
          variant="contained"
          color="primary"
          onClick={(): void => {
            layerChangeEnabled({
              layerId,
              mapId,
              enabled: !isLayerEnabled,
            });
          }}
        >
          {!isLayerEnabled ? 'Enable' : 'Disable'}
        </Button>
      </div>
    );
  },
);

/* This is an object to keep the number of renders for each mapId */
const renderCounter: Record<string, number> = {};

/* 
  Here we keep a reference to the real console.log. 
  This is is used to detect in which phase the React.StrictMode check is, to do a correct number of render counts.
  React.StrictMode does a check phase and a render phase.
  Without realConsoleLog, you can not detect the render phase, which will make the number of renders counts to be twice as high.

  React.StrictMode overwrites console.log during the check phase, 
  that is why you can use realConsoleLog to check if we are in render mode or not.
*/
const { log: realConsoleLog } = console;

/**
 * Component which counts the number of renders based on the  mapSelectors.getMapLayers selector
 */
const SelectorTester: React.FC<MapViewConnectProps> = ({
  mapId,
}: MapViewConnectProps) => {
  const layers = useSelector((store: AppStore) =>
    mapSelectors.getMapLayers(store, mapId),
  );

  if (renderCounter[mapId] === undefined) {
    renderCounter[mapId] = 0;
    /* Only increase the counter when we are in render mode( React StrictMode otherwise causes this to increment twice */
    // eslint-disable-next-line no-console
  } else if (realConsoleLog === console.log) {
    renderCounter[mapId] += 1;
  }

  return (
    <div
      style={{
        width: '80%',
        height: '100%',
        zIndex: 20000,
        background: 'rgba(255, 255, 255, 0.7)',
        overflow: 'hidden',
        padding: '10px',
        margin: '10px 0',
      }}
    >
      Map <b>{mapId}</b> rendered <b>{renderCounter[mapId]}</b> times.
      <br />
      Layers info from selector:
      <p style={{ whiteSpace: 'pre', fontFamily: 'courier', fontSize: '10px' }}>
        {JSON.stringify(layers, null, 2)}
      </p>
    </div>
  );
};

export const SelectorTests = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <div
      style={{
        height: '5vh',
        width: '100%',
        display: 'inline-block',
        position: 'relative',
        background: 'white',
        padding: '8px',
      }}
    >
      Changing the opacity on the map to the left should not trigger renders on
      the map to the right. Same for layer enabled.
    </div>
    <div
      style={{
        height: '95vh',
        width: '50%',
        display: 'inline-block',
        position: 'relative',
      }}
    >
      <MapViewConnect mapId="mapid_1" />

      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 10000,
        }}
      >
        <LayerEnableButton mapId="mapid_1" />
        <ConnectedLayerChangeOpacityInput mapId="mapid_1" />
        <SelectorTester mapId="mapid_1" />
      </div>
    </div>
    <div
      style={{
        height: '95vh',
        width: '50%',
        display: 'inline-block',
        position: 'relative',
      }}
    >
      <MapViewConnect mapId="mapid_2" />

      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 10000,
        }}
      >
        <LayerEnableButton mapId="mapid_2" />
        <ConnectedLayerChangeOpacityInput mapId="mapid_2" />
        <SelectorTester mapId="mapid_2" />
      </div>
    </div>
  </CoreThemeStoreProvider>
);
