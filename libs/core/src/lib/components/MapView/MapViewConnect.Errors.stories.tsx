/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import {
  FormControlLabel,
  FormControl,
  FormHelperText,
  ListItem,
  Checkbox,
  List,
  Paper,
  Grid,
} from '@mui/material';
import MapViewConnect from './MapViewConnect';
import { store } from '../../storybookUtils/store';
import { layerActions, layerSelectors, mapSelectors } from '../../store';

import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';

import {
  radarLayerWithError,
  radarLayer,
  harmoniePressure,
} from '../../utils/publicLayers';

import { LayerStatus } from '../../store/mapStore/types';
import { AppStore } from '../../types/types';
import { CoreThemeStoreProvider } from '../Providers/Providers';

const connectLayerControls = connect(
  (state, props: { layerId: string }) => ({
    hasError:
      layerSelectors.getLayerStatus(state, props.layerId) === LayerStatus.error,
    name: layerSelectors.getLayerName(state, props.layerId),
    isEnabled: layerSelectors.getLayerEnabled(state, props.layerId),
  }),
  {
    layerChangeEnabled: layerActions.layerChangeEnabled,
  },
);

type Props = ConnectedProps<typeof connectLayerControls> & { layerId: string };

const LayerControl = connectLayerControls(
  ({ layerId, isEnabled, name, layerChangeEnabled, hasError }: Props) => (
    <ListItem dense>
      <Grid container direction="column">
        <Grid item sm={12}>
          <FormControl error={hasError}>
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  inputProps={{ 'aria-label': 'secondary checkbox' }}
                  checked={isEnabled}
                  disabled={hasError}
                  onChange={(): void => {
                    layerChangeEnabled({ layerId, enabled: !isEnabled });
                  }}
                />
              }
              label={name}
            />
            {hasError ? (
              <FormHelperText>Error loading layer</FormHelperText>
            ) : null}
          </FormControl>
        </Grid>
      </Grid>
    </ListItem>
  ),
);

const connectRedux = connect((state: AppStore, props: { mapId: string }) => ({
  layerIds: mapSelectors.getLayerIds(state, props.mapId),
}));

const MapWithError = connectRedux(
  ({ mapId, layerIds }: { mapId: string; layerIds: string[] }) => {
    useDefaultMapSettings({
      mapId,
      layers: [harmoniePressure, radarLayerWithError, radarLayer],
    });

    return (
      <Paper>
        <List>
          {layerIds.map((layerId) => (
            <LayerControl key={layerId} layerId={layerId} />
          ))}
        </List>
      </Paper>
    );
  },
);

export const MapError = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <div style={{ height: '100vh' }}>
      <MapViewConnect mapId="mapid_1" />
    </div>
    <div
      style={{
        position: 'absolute',
        left: '50px',
        top: '10px',
        zIndex: 99910000,
      }}
    >
      <MapWithError mapId="mapid_1" />
    </div>
  </CoreThemeStoreProvider>
);
