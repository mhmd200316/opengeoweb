/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';
import { Paper, Card, Typography, CardContent } from '@mui/material';
import MapViewConnect from './MapViewConnect';
import { layerActions, mapActions } from '../../store';
import { radarLayer, overLayer, baseLayerGrey } from '../../utils/publicLayers';
import { initialBbox } from '../../storybookUtils/defaultStorySettings';

import { store } from '../../storybookUtils/store';
import { CoreThemeStoreProvider } from '../Providers/Providers';

interface ExampleComponentProps {
  addLayer: typeof layerActions.addLayer;
  addBaseLayer: typeof layerActions.addBaseLayer;
  setBbox: typeof mapActions.setBbox;
}

const connectRedux = connect(null, {
  addLayer: layerActions.addLayer,
  addBaseLayer: layerActions.addBaseLayer,
  setBbox: mapActions.setBbox,
});

const ExampleComponent = connectRedux(
  ({ addLayer, setBbox, addBaseLayer }: ExampleComponentProps) => {
    React.useEffect(() => {
      addLayer({
        layer: radarLayer,
        layerId: 'layerid_1',
        mapId: 'mapid_1',
        origin: 'MapViewConnect.InitialBBoxAndWithout.stories.tsx',
      });
      addLayer({
        layer: radarLayer,
        layerId: 'layerid_2',
        mapId: 'mapid_2',
        origin: 'MapViewConnect.InitialBBoxAndWithout.stories.tsx',
      });
      setBbox({
        bbox: initialBbox.bbox,
        srs: initialBbox.srs,
        mapId: 'mapid_2',
      });
      addBaseLayer({
        mapId: 'mapid_1',
        layer: { ...overLayer, id: 'overLayer-011' },
        layerId: 'overLayer-011',
      });
      addBaseLayer({
        mapId: 'mapid_1',
        layer: { ...baseLayerGrey, id: 'base-011' },
        layerId: 'base-011',
      });
      addBaseLayer({
        mapId: 'mapid_2',
        layer: { ...overLayer, id: 'overLayer-012' },
        layerId: 'overLayer-012',
      });
      addBaseLayer({
        mapId: 'mapid_2',
        layer: { ...baseLayerGrey, id: 'base-012' },
        layerId: 'base-012',
      });

      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
      <div style={{ display: 'flex' }}>
        <div style={{ height: '100vh', width: '50%', position: 'relative' }}>
          <div
            style={{
              position: 'absolute',
              left: '50px',
              top: '10px',
              zIndex: 10000,
            }}
          >
            <Paper>
              <Card>
                <CardContent>
                  <Typography variant="h6">No initial BBOX and SRS</Typography>
                  <Typography variant="body1">
                    Fallen back to default settings
                  </Typography>
                </CardContent>
              </Card>
            </Paper>
          </div>
          <MapViewConnect mapId="mapid_1" />
        </div>
        <div style={{ width: '50%', position: 'relative' }}>
          <div
            style={{
              position: 'absolute',
              left: '50px',
              top: '10px',
              zIndex: 10000,
            }}
          >
            <Paper>
              <Card>
                <CardContent>
                  <Typography variant="h6">Initial SRS and BBox</Typography>
                  <Typography variant="body1">
                    Passed in SRS and BBox used
                  </Typography>
                </CardContent>
              </Card>
            </Paper>
          </div>
          <MapViewConnect mapId="mapid_2" />
        </div>
      </div>
    );
  },
);

export const MapWithAndWithoutInitialBBOX: React.FC = () => (
  <CoreThemeStoreProvider store={store}>
    <ExampleComponent />
  </CoreThemeStoreProvider>
);
