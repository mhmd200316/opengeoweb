/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { connect } from 'react-redux';

import MapViewConnect from './MapViewConnect';
import { layerActions } from '../../store';
import * as publicLayers from '../../utils/publicLayers';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';

import { store } from '../../storybookUtils/store';
import { ActionCard } from '../../storybookUtils/HelperComponents';
import { CoreThemeStoreProvider } from '../Providers/Providers';

interface LayerActionControlsProps extends React.FC {
  setLayers: typeof layerActions.setLayers;
  mapId: string;
}

const connectRedux = connect(null, {
  setLayers: layerActions.setLayers,
});

const LayerActionControls: React.FC<LayerActionControlsProps> = ({
  mapId,
  setLayers,
}: LayerActionControlsProps) => {
  useDefaultMapSettings({
    mapId,
    baseLayers: [
      { ...publicLayers.baseLayerGrey, id: `baseLayer-2-${mapId}` },
      {
        ...publicLayers.overLayer,
        id: `overLayer-1-${mapId}`,
      },
    ],
  });

  return (
    <ActionCard
      name="setLayers"
      exampleLayers={[
        {
          layers: [{ ...publicLayers.radarLayer, id: `radar-1-${mapId}` }],
          title: 'Radar',
        },
        {
          layers: [
            { ...publicLayers.harmoniePrecipitation, id: `har-prec-${mapId}` },
            { ...publicLayers.harmoniePressure, id: `har-pres-${mapId}` },
          ],
          title: 'Precip + Obs',
        },
        {
          layers: [
            { ...publicLayers.radarLayer, id: `radar-2-${mapId}` },
            {
              ...publicLayers.dwdWarningLayer,
              id: `msg-${mapId}`,
            },
          ],
          title: 'Radar + DWD Warnings',
        },
      ]}
      description="sets new layers on a map while removing all current ones"
      onClickBtn={({ layers }): void => {
        setLayers({ layers, mapId });
      }}
    />
  );
};

const LayerActionControlsConnect = connectRedux(LayerActionControls);
export const DoubleMap: React.FC = () => (
  <CoreThemeStoreProvider store={store}>
    <div style={{ display: 'flex' }}>
      <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
        <MapViewConnect mapId="mapid_1" />
        <div
          style={{
            position: 'absolute',
            left: '10px',
            bottom: '10px',
            zIndex: 99910000,
          }}
        >
          <LayerActionControlsConnect mapId="mapid_1" />
        </div>
      </div>
      <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
        <MapViewConnect mapId="mapid_2" />
        <div
          style={{
            position: 'absolute',
            left: '10px',
            bottom: '10px',
            zIndex: 99910000,
          }}
        >
          <LayerActionControlsConnect mapId="mapid_2" />
        </div>
      </div>
    </div>
  </CoreThemeStoreProvider>
);
