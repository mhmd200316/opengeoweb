/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import produce from 'immer';
import userEvent from '@testing-library/user-event';
import MapViewConnect from './MapViewConnect';
import {
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
  WmdefaultReduxLayerRadarKNMI,
} from '../../utils/defaultTestSettings';
import { mockStateMapWithMultipleLayers } from '../../utils/testUtils';
import {
  baseLayerGrey,
  baseLayerWorldMap,
  overLayer,
  overLayer2,
  simplePolygonGeoJSON,
} from '../../utils/testLayers';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';
import {
  SYNCGROUPS_TYPE_SETTIME,
  SYNCGROUPS_TYPE_SETBBOX,
} from '../../store/generic/synchronizationGroups/constants';

import MapViewLayer from './MapViewLayer';

import { CoreThemeStoreProvider } from '../Providers/Providers';
import {
  MapActionOrigin,
  WebMapStateModuleState,
} from '../../store/mapStore/types';
import { UIModuleState } from '../../store/ui/types';
import { mapActions, genericActions } from '../../store';

describe('src/components/MapView/MapViewConnect', () => {
  it('should show a layer for each maplayer, baselayer and overlayer', () => {
    const mapId = 'map-1';
    const testLayers = [
      defaultReduxLayerRadarKNMI,
      baseLayerGrey,
      overLayer,
      defaultReduxLayerRadarColor,
      baseLayerWorldMap,
      overLayer2,
    ];
    const mockState = mockStateMapWithMultipleLayers(testLayers, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { container } = render(
      <CoreThemeStoreProvider store={store}>
        <MapViewConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    const layerList = container.querySelectorAll(
      '[class^=makeStyles-MapViewLayer]',
    );

    expect(layerList.length).toEqual(testLayers.length);

    // baselayers should be shown first
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
    expect(layerList[1].textContent).toContain(baseLayerWorldMap.id);

    // maplayers should be shown next
    expect(layerList[2].textContent).toContain(defaultReduxLayerRadarKNMI.id);
    expect(layerList[3].textContent).toContain(defaultReduxLayerRadarColor.id);

    // overlayers should be shown last
    expect(layerList[4].textContent).toContain(overLayer.id);
    expect(layerList[5].textContent).toContain(overLayer2.id);
  });

  it('should zoom and pan the map', async () => {
    jest.useFakeTimers();

    const mapId = 'map-1';
    const layers = [defaultReduxLayerRadarKNMI, baseLayerGrey, overLayer];
    registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      defaultReduxLayerRadarKNMI.id,
    );
    const mockState: WebMapStateModuleState & UIModuleState = {
      ...mockStateMapWithMultipleLayers(layers, mapId),
      ui: {
        order: [],
        dialogs: {},
        activeWindowId: mapId,
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <MapViewConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('zoom-reset')).toBeTruthy();
    expect(getByTestId('zoom-in')).toBeTruthy();
    expect(getByTestId('zoom-out')).toBeTruthy();

    const expectedActionsA = [
      mapActions.registerMap({ mapId }),
      genericActions.syncGroupAddSource({
        id: mapId,
        type: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
      }),
    ];

    await waitFor(() => expect(store.getActions()).toEqual(expectedActionsA));

    store.clearActions();

    /* Now check the bbox action */
    userEvent.click(getByTestId('zoom-out'));

    const zoomOutButtonAction = genericActions.setBbox({
      bbox: {
        bottom: -25333333.333333332,
        left: -25333333.333333332,
        right: 25333333.333333332,
        top: 25333333.333333332,
      },
      sourceId: mapId,
      srs: 'EPSG:3857',
      mapId,
      origin: MapActionOrigin.map,
    });
    await waitFor(() =>
      expect(store.getActions()).toEqual([zoomOutButtonAction]),
    );

    store.clearActions();

    userEvent.keyboard('-');

    const zoomOutKeyboardAction = produce(zoomOutButtonAction, (draft) => {
      draft.payload.bbox = {
        bottom: -33777777.777777776,
        left: -33777777.777777776,
        right: 33777777.777777776,
        top: 33777777.777777776,
      };
    });

    await waitFor(() =>
      expect(store.getActions()).toEqual([zoomOutKeyboardAction]),
    );

    store.clearActions();

    userEvent.keyboard('{ArrowLeft}');

    // Avoid waiting for debounce
    jest.runOnlyPendingTimers();

    const arrowLeftKeyboardAction = produce(zoomOutKeyboardAction, (draft) => {
      draft.payload.bbox.left = -35128888.88888889;
      draft.payload.bbox.right = 32426666.666666664;
    });

    expect(store.getActions()).toEqual([arrowLeftKeyboardAction]);

    store.clearActions();

    // If control key is pressed then ignore keypress
    userEvent.keyboard('{Control>}{ArrowLeft}');

    jest.runOnlyPendingTimers();

    expect(store.getActions()).toEqual([]);

    jest.useRealTimers();
  });

  it('should be able to render layers as children', async () => {
    const mapId = 'map-1';
    const layers = [defaultReduxLayerRadarKNMI, baseLayerGrey, overLayer];

    registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      defaultReduxLayerRadarKNMI.id,
    );
    const mockState = mockStateMapWithMultipleLayers(layers, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { container } = render(
      <CoreThemeStoreProvider store={store}>
        <MapViewConnect mapId={mapId}>
          <MapViewLayer id="test-layer-A" geojson={simplePolygonGeoJSON} />
          <MapViewLayer id="test-layer-B" geojson={simplePolygonGeoJSON} />
        </MapViewConnect>
      </CoreThemeStoreProvider>,
    );

    const layerList = container.querySelectorAll(
      '[class^=makeStyles-MapViewLayer]',
    );

    expect(layerList.length).toEqual(layers.length + 2);

    // order: baselayers, maplayers, overlayers
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
    expect(layerList[1].textContent).toContain(defaultReduxLayerRadarKNMI.id);
    expect(layerList[2].textContent).toContain(overLayer.id);
    // child as last
    expect(layerList[3].textContent).toContain('test-layer-A');
    expect(layerList[4].textContent).toContain('test-layer-B');
  });
});
