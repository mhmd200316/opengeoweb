/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import MapView from './MapView';
import { generateMapId } from '../../store/mapStore/utils/helpers';
import { baseLayerGrey, overLayer } from '../../utils/testLayers';
import { defaultReduxLayerRadarColor } from '../../utils/defaultTestSettings';
import MapViewLayer from './MapViewLayer';
import { CoreThemeProvider } from '../Providers/Providers';

describe('src/components/MapView/MapView', () => {
  const mapViewProps = {
    mapId: generateMapId(),
    onMount: (): void => {},
    onUnMount: (): void => {},
  };

  it('should show  map', () => {
    const { container } = render(
      <CoreThemeProvider>
        <MapView {...mapViewProps} />
      </CoreThemeProvider>,
    );
    expect(container.querySelector('[class^=MapViewComponent]')).toBeTruthy();
  });

  it('should show the zoomControls', () => {
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <MapView {...mapViewProps} />
      </CoreThemeProvider>,
    );
    expect(queryByTestId('zoom-reset')).toBeTruthy();
    expect(queryByTestId('zoom-in')).toBeTruthy();
    expect(queryByTestId('zoom-out')).toBeTruthy();
  });

  it('should hide the zoomControls', () => {
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <MapView {...mapViewProps} controls={{}} />
      </CoreThemeProvider>,
    );
    expect(queryByTestId('zoom-reset')).toBeFalsy();
    expect(queryByTestId('zoom-in')).toBeFalsy();
    expect(queryByTestId('zoom-out')).toBeFalsy();
  });

  it('should render map with layers when layers exist', () => {
    const { container } = render(
      <CoreThemeProvider>
        <MapView {...mapViewProps}>
          <MapViewLayer {...baseLayerGrey} />
          <MapViewLayer {...overLayer} />
          <MapViewLayer {...defaultReduxLayerRadarColor} />
        </MapView>
      </CoreThemeProvider>,
    );

    const layerList = container.querySelectorAll(
      '[class^=makeStyles-MapViewLayer]',
    );

    expect(layerList.length).toEqual(3);
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
    expect(layerList[1].textContent).toContain(overLayer.id);
    expect(layerList[2].textContent).toContain(defaultReduxLayerRadarColor.id);
  });

  it('should show the time display by default', () => {
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <MapView
          {...mapViewProps}
          dimensions={[
            { name: 'time', currentValue: new Date().toISOString() },
          ]}
        />
      </CoreThemeProvider>,
    );
    expect(queryByTestId('map-time')).toBeTruthy();
  });

  it('should hide the time display', () => {
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <MapView
          {...mapViewProps}
          displayTimeInMap={false}
          dimensions={[
            { name: 'time', currentValue: new Date().toISOString() },
          ]}
        />
      </CoreThemeProvider>,
    );
    expect(queryByTestId('map-time')).toBeFalsy();
  });

  it('should support null objects as children', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const MapWithOptionalChildren: React.FC = (layer1: any, layer2: any) => (
      <CoreThemeProvider>
        <MapView mapId={generateMapId()}>
          {layer1 && <MapViewLayer {...layer1} />}
          {layer2 && <MapViewLayer {...layer2} />}
        </MapView>
      </CoreThemeProvider>
    );

    const { container } = render(MapWithOptionalChildren(null, baseLayerGrey));

    expect(container.querySelector('[class^=MapViewComponent]')).toBeTruthy();
    const layerList = container.querySelectorAll(
      '[class^=makeStyles-MapViewLayer]',
    );
    expect(layerList.length).toEqual(1);
    expect(layerList[0].textContent).toContain(baseLayerGrey.id);
  });
});
