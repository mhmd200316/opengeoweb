/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import MapViewLayer from './MapViewLayer';
import { AppStore } from '../../types/types';
import {
  layerActions,
  mapActions,
  mapSelectors,
  genericActions,
} from '../../store';

import MapView from './MapView';
import {
  SYNCGROUPS_TYPE_SETTIME,
  SYNCGROUPS_TYPE_SETBBOX,
} from '../../store/generic/synchronizationGroups/constants';

import { Layer } from '../../store/mapStore/layers/types';
import { SetTimePayload } from '../../store/generic/types';
import { handleMomentISOString } from '../../utils/dimensionUtils';
import { getWMJSMapById } from '../../store/mapStore/utils/helpers';

import { getActiveWindowId } from '../../store/ui/selectors';
import {
  MapActionOrigin,
  SetMapDimensionPayload,
} from '../../store/mapStore/types';

export interface MapViewConnectProps {
  mapId: string;
  displayTimeInMap?: boolean;
  controls?: {
    zoomControls?: boolean;
  };
  showScaleBar?: boolean;
  children?: React.ReactNode;
  showLayerInfo?: boolean;
}

export const ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION =
  'ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION';

export const ORIGIN_REACTMAPVIEWCONNECT_ONUPDATELAYERINFO =
  'ORIGIN_REACTMAPVIEWCONNECT_ONUPDATELAYERINFO';

/**
 * Connected component used to display the map and selected layers.
 * Includes options to disable the map controls and legend.
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {object} [controls.zoomControls] **optional** controls: object - toggle the map controls, zoomControls defaults to true
 * @param {boolean} [displayTimeInMap] **optional** displayTimeInMap: boolean, toggles the mapTime, defaults to true
 * @param {boolean} [showScaleBar] **optional** showScaleBar: boolean, toggles the scaleBar, defaults to true
 * @example
 * ```<MapViewConnect mapId={mapId} controls={{ zoomControls: false }} displayTimeInMap={false} showScaleBar={false}/>```
 */
const MapViewConnect: React.FC<MapViewConnectProps> = ({
  mapId,
  children,
  ...props
}: MapViewConnectProps) => {
  const mapDimensions = useSelector((store: AppStore) =>
    mapSelectors.getMapDimensions(store, mapId),
  );
  const layers = useSelector((store: AppStore) =>
    mapSelectors.getMapLayers(store, mapId),
  );
  const baseLayers = useSelector((store: AppStore) =>
    mapSelectors.getMapBaseLayers(store, mapId),
  );
  const overLayers = useSelector((store: AppStore) =>
    mapSelectors.getMapOverLayers(store, mapId),
  );
  const bbox = useSelector((store: AppStore) =>
    mapSelectors.getBbox(store, mapId),
  );
  const srs = useSelector((store: AppStore) =>
    mapSelectors.getSrs(store, mapId),
  );
  const activeLayerId = useSelector((store: AppStore) =>
    mapSelectors.getActiveLayerId(store, mapId),
  );
  const animationDelay = useSelector((store: AppStore) =>
    mapSelectors.getMapAnimationDelay(store, mapId),
  );
  const mapPinLocation = useSelector((store: AppStore) =>
    mapSelectors.getPinLocation(store, mapId),
  );
  const disableMapPin = useSelector((store: AppStore) =>
    mapSelectors.getDisableMapPin(store, mapId),
  );

  const displayMapPin = useSelector((store: AppStore) =>
    mapSelectors.getDisplayMapPin(store, mapId),
  );

  const dispatch = useDispatch();
  const map = getWMJSMapById(mapId);

  const mapChangeDimension = React.useCallback(
    (mapDimensionPayload: SetMapDimensionPayload): void => {
      if (mapDimensionPayload.dimension) {
        const dimension = map.getDimension(mapDimensionPayload.dimension.name);
        if (
          dimension &&
          dimension.currentValue === mapDimensionPayload.dimension.currentValue
        ) {
          return;
        }
      }
      dispatch(mapActions.mapChangeDimension(mapDimensionPayload));
    },
    [dispatch, map],
  );

  const setTime = React.useCallback(
    (setTimePayload: SetTimePayload): void => {
      /* Check if the map not already has this value set, otherwise this component will re-render */
      if (map.getDimension('time').currentValue === setTimePayload.value) {
        return;
      }
      dispatch(genericActions.setTime(setTimePayload));
    },
    [dispatch, map],
  );

  const updateLayerInformation = React.useCallback(
    (payload): void => {
      dispatch(layerActions.onUpdateLayerInformation(payload));
    },
    [dispatch],
  );

  const registerMap = React.useCallback(
    (payload): void => {
      dispatch(mapActions.registerMap(payload));
    },
    [dispatch],
  );
  const unregisterMap = React.useCallback(
    (payload): void => {
      dispatch(mapActions.unregisterMap(payload));
    },
    [dispatch],
  );
  const genericSetBbox = React.useCallback(
    (payload): void => {
      dispatch(genericActions.setBbox(payload));
    },
    [dispatch],
  );
  const syncGroupAddSource = React.useCallback(
    (payload): void => {
      dispatch(genericActions.syncGroupAddSource(payload));
    },
    [dispatch],
  );
  const syncGroupRemoveSource = React.useCallback(
    (payload): void => {
      dispatch(genericActions.syncGroupRemoveSource(payload));
    },
    [dispatch],
  );
  const layerError = React.useCallback(
    (payload): void => {
      dispatch(layerActions.layerError(payload));
    },
    [dispatch],
  );
  const mapPinChangeLocation = React.useCallback(
    (payload): void => {
      dispatch(mapActions.setMapPinLocation(payload));
    },
    [dispatch],
  );
  const setSelectedFeature = React.useCallback(
    (payload): void => {
      dispatch(mapActions.setSelectedFeature(payload));
    },
    [dispatch],
  );

  useKeyboardZoomAndPan(mapId);

  return (
    <div style={{ height: '100%' }}>
      <MapView
        {...props}
        mapId={mapId}
        onMount={(): void => {
          registerMap({ mapId });

          syncGroupAddSource({
            id: mapId,
            type: [SYNCGROUPS_TYPE_SETTIME, SYNCGROUPS_TYPE_SETBBOX],
          });
        }}
        onUnMount={(): void => {
          unregisterMap({ mapId });
          syncGroupRemoveSource({
            id: mapId,
          });
        }}
        srs={srs}
        bbox={bbox}
        mapPinLocation={displayMapPin && mapPinLocation}
        dimensions={mapDimensions}
        activeLayerId={activeLayerId}
        animationDelay={animationDelay}
        displayMapPin={displayMapPin}
        disableMapPin={disableMapPin}
        onMapChangeDimension={(
          mapDimensionPayload: SetMapDimensionPayload,
        ): void => {
          if (
            mapDimensionPayload &&
            mapDimensionPayload.dimension &&
            mapDimensionPayload.dimension.name &&
            mapDimensionPayload.dimension.name === 'time'
          ) {
            setTime({
              sourceId: mapId,
              origin: `${mapDimensionPayload.origin}==> ${ORIGIN_REACTMAPVIEWCONNECT_ONMAPCHANGEDIMENSION}`,
              value: handleMomentISOString(
                mapDimensionPayload.dimension.currentValue,
              ),
            } as SetTimePayload);
          } else {
            mapChangeDimension(mapDimensionPayload);
          }
        }}
        onUpdateLayerInformation={updateLayerInformation}
        onMapPinChangeLocation={mapPinChangeLocation}
        onMapZoomEnd={(a): void => {
          genericSetBbox({
            bbox: a.bbox,
            srs: a.srs,
            sourceId: mapId,
            origin: MapActionOrigin.map,
            mapId,
          });
        }}
      >
        {baseLayers.map((layer) => (
          <MapViewLayer
            id={`baselayer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id, error });
            }}
            {...layer}
          />
        ))}
        {layers.map((layer: Layer) => (
          <MapViewLayer
            id={`layer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id, error });
            }}
            onClickFeature={
              layer.geojson
                ? (event): void => {
                    setSelectedFeature({
                      mapId,
                      selectedFeatureIndex: event?.featureIndex,
                    });
                  }
                : undefined
            }
            {...layer}
          />
        ))}
        {overLayers.map((layer) => (
          <MapViewLayer
            id={`baselayer-${layer.id}`}
            key={layer.id}
            onLayerError={(_, error): void => {
              layerError({ layerId: layer.id, error });
            }}
            {...layer}
          />
        ))}
        {children}
      </MapView>
    </div>
  );
};

export default MapViewConnect;

const useKeyboardZoomAndPan = (mapId: string): void => {
  const activeWindowId = useSelector(getActiveWindowId);
  const mapIsActive = activeWindowId === mapId;

  React.useEffect(() => {
    const keysPressedDown: { [key: string]: boolean } = {};
    const handleKeyPress = (event: KeyboardEvent): void => {
      const target = event.target as Element;
      const map = getWMJSMapById(mapId);
      if (map && target.tagName !== 'INPUT' && mapIsActive) {
        const { key, type } = event;
        keysPressedDown[key] = type === 'keydown';

        if (keysPressedDown['+']) {
          map.zoomIn(1);
        } else if (keysPressedDown['-']) {
          map.zoomOut();
        }

        const arrowRight = keysPressedDown['ArrowRight'];
        const arrowLeft = keysPressedDown['ArrowLeft'];
        const arrowUp = keysPressedDown['ArrowUp'];
        const arrowDown = keysPressedDown['ArrowDown'];
        const control = keysPressedDown['Control'];

        if (!control && (arrowRight || arrowLeft || arrowUp || arrowDown)) {
          let x = 0;
          let y = 0;
          if (arrowRight) {
            x -= PAN_STEP_PERCENTAGE;
          }
          if (arrowLeft) {
            x += PAN_STEP_PERCENTAGE;
          }
          if (arrowUp) {
            y -= PAN_STEP_PERCENTAGE;
          }
          if (arrowDown) {
            y += PAN_STEP_PERCENTAGE;
          }
          map.mapPanPercentage(x, y);
        }
      }
    };

    document.addEventListener('keydown', handleKeyPress);
    document.addEventListener('keyup', handleKeyPress);
    return (): void => {
      document.removeEventListener('keydown', handleKeyPress);
      document.removeEventListener('keyup', handleKeyPress);
    };
  }, [mapIsActive, mapId]);
};

const PAN_STEP_PERCENTAGE = 0.02;
