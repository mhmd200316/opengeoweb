/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Paper, Card, CardContent, Typography } from '@mui/material';

import { MapView, MapViewLayer } from '.';
import { generateMapId } from '../../store/mapStore/utils/helpers';
import { baseLayer, overLayer } from '../../utils/publicLayers';
import { CoreThemeProvider } from '../Providers/Providers';
import { MapLocation } from '../../store/mapStore/map/types';

export const SetMapPin = (): React.ReactElement => {
  const [mapPinLocation, setMapPinLocation] = React.useState<MapLocation>({
    lat: 52,
    lon: 5,
  });
  return (
    <CoreThemeProvider>
      <div style={{ height: '100vh' }}>
        <div style={{ display: 'flex', height: '100%' }}>
          <div style={{ position: 'relative', width: '100%', height: '100%' }}>
            <div
              style={{
                position: 'absolute',
                left: '50px',
                top: '20px',
                zIndex: 10000,
              }}
            >
              <Paper>
                <Card>
                  <CardContent>
                    <Typography variant="subtitle1">
                      Position of map cursor:
                    </Typography>
                    <Typography variant="body2">
                      {`Lon: ${mapPinLocation.lon.toFixed(2)}`}
                      <br />
                      {`Lat: ${mapPinLocation.lat.toFixed(2)}`}
                      <br />
                    </Typography>
                  </CardContent>
                </Card>
              </Paper>
            </div>
            <MapView
              mapId={generateMapId()}
              mapPinLocation={mapPinLocation}
              onMapPinChangeLocation={(newPinLocation): void => {
                setMapPinLocation({
                  lat: newPinLocation.mapPinLocation.lat,
                  lon: newPinLocation.mapPinLocation.lon,
                });
              }}
              displayMapPin
            >
              <MapViewLayer {...baseLayer} />
              <MapViewLayer {...overLayer} />
            </MapView>
          </div>
        </div>
      </div>
    </CoreThemeProvider>
  );
};

SetMapPin.storyName = 'Set Map cursor and location';
