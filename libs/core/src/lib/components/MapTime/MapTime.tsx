/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';
import { Typography, Box } from '@mui/material';

import makeStyles from '@mui/styles/makeStyles';

import { Dimension } from '../../store/mapStore/types';

const useStyles = makeStyles(() => ({
  mapTimeContainer: {
    zIndex: 10,
    justifySelf: 'center',
    bottom: 12,
    position: 'absolute',
  },
  mapTimeText: {
    fontWeight: 500,
  },
}));

export const getTimeDimension = (dimensions: Dimension[]): Dimension =>
  dimensions.find(
    (dimension) =>
      dimension.name === 'time' && dimension.currentValue !== undefined,
  );

export const formatTime = (time: string, timeFormat: string): string =>
  moment.utc(time).format(timeFormat).toString();

interface MapeTimeProps {
  dimensions: Dimension[];
  timeFormat?: string;
}

export const defaultTimeFormat = 'ddd DD MMM YYYY HH:mm [UTC]';

const MapTime: React.FC<MapeTimeProps> = ({
  dimensions,
  timeFormat = defaultTimeFormat,
}: MapeTimeProps) => {
  const classes = useStyles();
  const timeDimension = getTimeDimension(dimensions);

  if (!timeDimension) {
    return null;
  }
  const mapTime = formatTime(timeDimension.currentValue, timeFormat);

  return (
    <div data-testid="map-time" className={classes.mapTimeContainer}>
      <Box>
        <Typography variant="caption" className={classes.mapTimeText}>
          {mapTime}
        </Typography>
      </Box>
    </div>
  );
};

export default MapTime;
