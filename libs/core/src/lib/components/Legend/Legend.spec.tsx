/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, findByRole } from '@testing-library/react';
import { LayerOptions, WMEmptyLayerTitle, WMLayer } from '@opengeoweb/webmap';
import { Legend } from '.';
import {
  multiDimensionLayer,
  WmdefaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarKNMI,
} from '../../utils/defaultTestSettings';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';
import { CoreThemeProvider } from '../Providers/Providers';

describe('src/components/LegendOverview/Legend', () => {
  it('should not show a legend when there is no layer', () => {
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <Legend layer={null} />
      </CoreThemeProvider>,
    );

    expect(queryByTestId('legend')).toBeFalsy();
  });

  it('should show the layer name, time and all dimensions for a layer', () => {
    const testLayer = {
      ...multiDimensionLayer,
      id: 'test-multi',
    } as LayerOptions;
    const wmLayer = new WMLayer(testLayer);
    registerWMLayer(wmLayer, testLayer.id);
    const { getByTestId } = render(
      <CoreThemeProvider>
        <Legend layer={testLayer} />
      </CoreThemeProvider>,
    );

    expect(getByTestId('legend')).toBeTruthy();
    expect(getByTestId('legend-title').textContent).toEqual(testLayer.name);

    testLayer.dimensions.forEach((dimension) => {
      if (dimension.name !== 'time') {
        expect(getByTestId('legend-info').textContent).toContain(
          `${dimension.name}:${dimension.currentValue} ${dimension.units}`,
        );
      } else {
        expect(getByTestId('legend-info').textContent).toContain(
          `${dimension.currentValue}`,
        );
      }
    });
  });

  it('should show the layer graphic when it is available', () => {
    registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      WmdefaultReduxLayerRadarKNMI.id,
    );
    WmdefaultReduxLayerRadarKNMI.legendGraphic = 'https://SOMEMOCKURLFORLEGEND';
    const { container } = render(
      <CoreThemeProvider>
        <Legend layer={defaultReduxLayerRadarKNMI} />
      </CoreThemeProvider>,
    );

    expect(container.querySelector('canvas')).toBeTruthy();
  });

  it('should not show a legend when the layer is disabled', () => {
    const testLayer = {
      ...defaultReduxLayerRadarKNMI,
      id: 'test-disabled',
      enabled: false,
    } as LayerOptions;
    const wmLayer = new WMLayer(testLayer);
    registerWMLayer(wmLayer, testLayer.id);
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <Legend layer={testLayer} />
      </CoreThemeProvider>,
    );

    expect(queryByTestId('legend')).toBeFalsy();
  });

  it('should show the layer title in the tooltip on hover', async () => {
    const testLayer = {
      ...defaultReduxLayerRadarKNMI,
      id: 'test-tooltip',
      enabled: true,
      title: 'TEST_LAYER',
    } as LayerOptions;
    const wmLayer = new WMLayer(testLayer);
    registerWMLayer(wmLayer, testLayer.id);
    wmLayer.legendGraphic = 'https://mockurl';
    const { container } = render(
      <CoreThemeProvider>
        <Legend layer={testLayer} />
      </CoreThemeProvider>,
    );

    expect(container.querySelector('canvas')).toBeTruthy();
    fireEvent.mouseOver(container.querySelector('canvas'));
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toEqual(testLayer.title);
  });

  it('should show "empty layer" in the tooltip on hover when the layer has no title', async () => {
    const testLayer = {
      ...defaultReduxLayerRadarKNMI,
      id: 'test-no-title',
      enabled: true,
      title: null,
    } as LayerOptions;
    const wmLayer = new WMLayer(testLayer);
    registerWMLayer(wmLayer, testLayer.id);
    const { container } = render(
      <CoreThemeProvider>
        <Legend layer={testLayer} />
      </CoreThemeProvider>,
    );

    expect(container.querySelector('canvas')).toBeTruthy();
    fireEvent.mouseOver(container.querySelector('canvas'));
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toEqual(WMEmptyLayerTitle);
  });

  it('should show "loading" in the tooltip on hover when the layer is not registered yet', async () => {
    const testLayer = {
      ...defaultReduxLayerRadarKNMI,
      id: 'test-not-registered',
      enabled: true,
    };
    const { container } = render(
      <CoreThemeProvider>
        <Legend layer={testLayer} />
      </CoreThemeProvider>,
    );

    expect(container.querySelector('canvas')).toBeTruthy();
    fireEvent.mouseOver(container.querySelector('canvas'));
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toEqual('loading');
  });
});
