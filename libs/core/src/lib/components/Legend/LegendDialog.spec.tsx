/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { LegendDialog } from '.';
import {
  defaultReduxLayerRadarColor,
  defaultReduxLayerRadarKNMI,
  WmdefaultReduxLayerRadarKNMI,
  WmMultiDimensionLayer,
  multiDimensionLayer,
} from '../../utils/defaultTestSettings';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';
import { DialogType } from '../../store/ui/types';
import { CoreThemeStoreProvider } from '../Providers/Providers';

describe('src/components/Legend/LegendDialog', () => {
  it('should follow the isOpen property to determine if open or closed, clicking on the cross should call onClose prop', () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      layers: [defaultReduxLayerRadarColor, defaultReduxLayerRadarKNMI],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LegendDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    // legend dialog should be opened
    expect(getByTestId('moveable-legend')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(getByTestId('closeBtn'));
    expect(props.onClose).toHaveBeenCalled();
  });

  it('should show the mapId in the title if if showMapId passed is true', async () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      layers: [defaultReduxLayerRadarColor, defaultReduxLayerRadarKNMI],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
      showMapId: true,
    };
    const { getByTestId, findByText } = render(
      <CoreThemeStoreProvider store={store}>
        <LegendDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    // legend dialog should be opened
    expect(getByTestId('moveable-legend')).toBeTruthy();

    expect(await findByText('Legend mapId_123')).toBeTruthy();
  });

  it('should trigger onMouseDown', async () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      layers: [defaultReduxLayerRadarColor, defaultReduxLayerRadarKNMI],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
      onMouseDown: jest.fn(),
    };
    const { getByTestId, findByText } = render(
      <CoreThemeStoreProvider store={store}>
        <LegendDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    // legend dialog should be opened
    expect(getByTestId('moveable-legend')).toBeTruthy();

    expect(await findByText('Legend')).toBeTruthy();

    fireEvent.mouseDown(getByTestId('moveable-legend'));
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should not show the mapId in the title by default', async () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      layers: [defaultReduxLayerRadarColor, defaultReduxLayerRadarKNMI],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };
    const { getByTestId, findByText, queryByText } = render(
      <CoreThemeStoreProvider store={store}>
        <LegendDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    // legend dialog should be opened
    expect(getByTestId('moveable-legend')).toBeTruthy();

    expect(await findByText('Legend')).toBeTruthy();
    expect(queryByText('Legend mapId_123')).toBeFalsy();
  });

  it('should show the legend of the layers in the legend dialog if there are layers loaded', () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      layers: [defaultReduxLayerRadarColor, defaultReduxLayerRadarKNMI],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LegendDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('LegendList')).toBeTruthy();
  });

  it('should show a legend for each layer', () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const multiDimensionLayerMock = {
      ...multiDimensionLayer,
      id: WmMultiDimensionLayer.id,
    };
    const layers = [defaultReduxLayerRadarKNMI, multiDimensionLayerMock];
    registerWMLayer(WmMultiDimensionLayer, WmMultiDimensionLayer.id);
    registerWMLayer(
      WmdefaultReduxLayerRadarKNMI,
      WmdefaultReduxLayerRadarKNMI.id,
    );
    const props = {
      layers,
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };

    const { getAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LegendDialog {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(getAllByTestId('legend').length).toEqual(layers.length);
  });

  it('should show No layers if no layers passed', () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      layers: [],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LegendDialog {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(getByTestId('NoLayers')).toBeTruthy();
  });
});
