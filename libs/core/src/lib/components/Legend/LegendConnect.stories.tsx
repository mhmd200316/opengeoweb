/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { store } from '../../storybookUtils/store';
import { LegendConnect } from '.';
import { MapViewConnect } from '../MapView';
import {
  baseLayerGrey,
  radarLayer,
  harmoniePrecipitation,
} from '../../utils/publicLayers';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import LegendMapButtonConnect from './LegendMapButtonConnect';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../LayerManager';

import { CoreThemeStoreProvider } from '../Providers/Providers';
import { MapControls } from '../MapControls';

const LegendConnectComponent = (): React.ReactElement => {
  const mapId1 = 'mapid_1';
  const mapId2 = 'mapid_2';

  useDefaultMapSettings({
    mapId: mapId1,
    layers: [{ ...radarLayer, id: `radar-${mapId1}` }],
    baseLayers: [{ ...baseLayerGrey, id: `baseGrey-${mapId1}` }],
  });
  useDefaultMapSettings({
    mapId: mapId2,
    layers: [{ ...harmoniePrecipitation, id: `radar-${mapId2}` }],
    baseLayers: [{ ...baseLayerGrey, id: `baseGrey-${mapId2}` }],
  });

  return (
    <div style={{ display: 'flex' }}>
      <LayerManagerConnect />
      <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
        <MapControls>
          <LegendMapButtonConnect mapId={mapId1} multiLegend={true} />
          <LayerManagerMapButtonConnect mapId={mapId1} />
        </MapControls>
        <LegendConnect showMapId mapId={mapId1} multiLegend={true} />
        <MapViewConnect mapId={mapId1} />
      </div>
      <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
        <MapControls>
          <LegendMapButtonConnect mapId={mapId2} multiLegend={true} />
          <LayerManagerMapButtonConnect mapId={mapId2} />
        </MapControls>
        <LegendConnect showMapId mapId={mapId2} multiLegend={true} />
        <MapViewConnect mapId={mapId2} />
      </div>
    </div>
  );
};

export const LegendConnectStoryWithMultiMaps = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <LegendConnectComponent />
  </CoreThemeStoreProvider>
);
