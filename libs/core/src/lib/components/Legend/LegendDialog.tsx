/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { ToolContainerDraggable } from '@opengeoweb/shared';
import Legend from './Legend';
import { Layer } from '../../store/mapStore/types';
import { Source } from '../../store/ui/types';

const useStyles = makeStyles(() => ({
  legendList: {
    display: 'flex',
    flexDirection: 'column',
    '&>div:last-child': {
      marginBottom: 0,
    },
  },
  legendContainer: {
    padding: 4,
  },
}));

interface LegendDialogProps {
  layers: Layer[];
  isOpen: boolean;
  onClose: () => void;
  onMouseDown?: () => void;
  mapId: string;
  showMapId?: boolean;
  order?: number;
  source?: Source;
}

const LegendDialog: React.FC<LegendDialogProps> = ({
  layers,
  isOpen,
  onClose,
  onMouseDown = (): void => {},
  mapId,
  showMapId = false,
  order = 0,
  source = 'app',
}: LegendDialogProps) => {
  const classes = useStyles();
  return (
    <ToolContainerDraggable
      startPosition={{ right: 20, top: 50 }}
      minWidth={100}
      onClose={onClose}
      title={showMapId ? `Legend ${mapId}` : 'Legend'}
      headerSize="small"
      initialMaxHeight={440}
      isOpen={isOpen}
      data-testid="moveable-legend"
      bounds="parent"
      onMouseDown={onMouseDown}
      order={order}
      source={source}
    >
      <div className={classes.legendContainer}>
        {layers && layers.length > 0 ? (
          <div data-testid="LegendList" className={classes.legendList}>
            {layers.map((layer) => (
              <Legend key={layer.id} layer={layer} />
            ))}
          </div>
        ) : (
          <Typography
            data-testid="NoLayers"
            variant="body1"
            style={{ fontWeight: 500, fontSize: 14 }}
          >
            No layers
          </Typography>
        )}
      </div>
    </ToolContainerDraggable>
  );
};

export default LegendDialog;
