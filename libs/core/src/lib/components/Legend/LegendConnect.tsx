/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import LegendDialog from './LegendDialog';
import { AppStore } from '../../types/types';
import * as uiSelectors from '../../store/ui/selectors';
import { uiActions, mapSelectors } from '../../store';
import { Source } from '../../store/ui/types';
import { MapActionOrigin } from '../../store/mapStore/types';

export const IS_LEGEND_OPEN_BY_DEFAULT = false;

interface LegendConnectProps {
  showMapId?: boolean;
  source?: Source;
  mapId?: string;
  multiLegend?: boolean;
}

const LegendConnectComponent: React.FC<LegendConnectProps> = ({
  showMapId = false,
  source = 'app',
  mapId: initialMapId = null,
  multiLegend = false,
}: LegendConnectProps) => {
  const dispatch = useDispatch();
  const legendType = multiLegend ? `legend-${initialMapId}` : `legend`;

  const mapIdStore = useSelector((store: AppStore) =>
    uiSelectors.getDialogMapId(store, legendType),
  );

  const [mapId, setMapId] = React.useState(initialMapId);

  React.useEffect(() => {
    setMapId(mapIdStore);
  }, [mapIdStore]);

  const mapLayers = useSelector((store: AppStore) =>
    mapSelectors.getMapLayers(store, mapId),
  );
  const isDialogOpen = useSelector((store: AppStore) =>
    uiSelectors.getisDialogOpen(store, legendType),
  );
  const uiOrder = useSelector((store: AppStore) =>
    uiSelectors.getDialogOrder(store, legendType),
  );
  const uiSource = useSelector((store: AppStore) =>
    uiSelectors.getDialogSource(store, legendType),
  );
  const uiIsOrderedOnTop = useSelector((store: AppStore) =>
    uiSelectors.getDialogIsOrderedOnTop(store, legendType),
  );

  const onClose = (): void => {
    dispatch(
      uiActions.setToggleOpenDialog({
        type: legendType,
        setOpen: false,
        origin: MapActionOrigin.map,
        mapId,
      }),
    );
  };

  const registerDialog = React.useCallback(
    (setOpen) =>
      dispatch(
        uiActions.registerDialog({
          type: legendType,
          mapId,
          setOpen,
          source,
        }),
      ),
    [dispatch, source, legendType, mapId],
  );
  const unregisterDialog = React.useCallback(
    () =>
      dispatch(
        uiActions.unregisterDialog({
          type: legendType,
        }),
      ),
    [dispatch, legendType],
  );

  const onOrderDialog = React.useCallback((): void => {
    if (!uiIsOrderedOnTop) {
      dispatch(
        uiActions.orderDialog({
          type: legendType,
        }),
      );
    }
  }, [dispatch, uiIsOrderedOnTop, legendType]);

  // Register this dialog in the store
  React.useEffect(() => {
    registerDialog(IS_LEGEND_OPEN_BY_DEFAULT);

    return (): void => {
      unregisterDialog();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <LegendDialog
      layers={mapLayers}
      isOpen={isDialogOpen}
      onClose={onClose}
      mapId={mapId}
      showMapId={showMapId}
      onMouseDown={onOrderDialog}
      order={uiOrder}
      source={uiSource}
    />
  );
};

/**
 * Legend component connected to the store displaying a legend for every layer shown on the active map Id
 * Please note that in order to use this and open/close the dialog, every map on the screen that should be able to show
 * a dialog should have the <LegendMapButtonConnect/> which is used to focus the correct map and open the dialog
 *
 * Expects the following props:
 * @param {boolean} showMapId (optional) showMapId: boolean - show the id of the map in the dialog title of which the legend is shown in te dialog
 * @param {Source} source (optional) source: Source - default is 'app', 'module' makes sure it will be shown on top of the module

 * ``` <LegendConnect legendDialogId={legendDialogId} />```
 */
const LegendConnect: React.FC<LegendConnectProps> = ({
  showMapId = false,
  ...props
}: LegendConnectProps) => (
  <LegendConnectComponent showMapId={showMapId} {...props} />
);

export default LegendConnect;
