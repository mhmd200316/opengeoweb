/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { DialogType, Source } from '../../store/ui/types';
import LegendMapButtonConnect from './LegendMapButtonConnect';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { uiActions } from '../../store';
import { MapActionOrigin } from '../../store/mapStore/types';

describe('src/components/Legend/LegendMapButtonConnect', () => {
  it('should dispatch action with passed in mapid when clicked, action should set isOpen to true if currently closed', () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
        source: 'app',
      },
    };
    const store = mockStore({ ui: { dialogs: mockState } });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LegendMapButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('open-Legend')).toBeTruthy();

    // open the legend dialog
    fireEvent.click(getByTestId('open-Legend'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'legend',
        mapId: props.mapId,
        setOpen: true,
        source: 'app',
        origin: MapActionOrigin.map,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
  it('should dispatch action with passed in mapid when clicked, action should update source and mapId when already open', () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: true,
        source: 'app',
      },
    };
    const store = mockStore({ ui: { dialogs: mockState } });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
      source: 'module' as Source,
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LegendMapButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('open-Legend')).toBeTruthy();

    // open the legend dialog for another map
    fireEvent.click(getByTestId('open-Legend'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'legend',
        mapId: props.mapId,
        setOpen: true,
        source: props.source,
        origin: MapActionOrigin.map,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
  it('should dispatch action with passed in mapid when clicked, action should close the dialog when currently open for this map', () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map3',
        isOpen: true,
        source: 'app',
      },
    };
    const store = mockStore({ ui: { dialogs: mockState } });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'map3',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LegendMapButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('open-Legend')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(getByTestId('open-Legend'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'legend',
        mapId: props.mapId,
        setOpen: false,
        source: 'app',
        origin: MapActionOrigin.map,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
