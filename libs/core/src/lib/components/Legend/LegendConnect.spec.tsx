/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { LegendConnect } from '.';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { uiActions } from '../../store';
import { IS_LEGEND_OPEN_BY_DEFAULT } from './LegendConnect';

describe('src/components/Legend/LegendConnect', () => {
  it('should register dialog and set it active', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          mapId123: {
            id: 'mapId123',
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: ['mapId123'],
      },
      ui: {
        dialogs: {
          legend: {
            isOpen: true,
            activeMapId: '',
          },
        },
      },
      layers: {
        byId: {},
        allIds: [],
      },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <LegendConnect mapId="mapId123" />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: 'legend',
        mapId: 'mapId123',
        setOpen: IS_LEGEND_OPEN_BY_DEFAULT,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should register multilegend and set it active', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          mapId123: {
            id: 'mapId123',
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: ['mapId123'],
      },
      ui: {
        dialogs: {
          legend: {
            isOpen: true,
            activeMapId: '',
          },
        },
      },
      layers: {
        byId: {},
        allIds: [],
      },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <LegendConnect mapId="mapId123" multiLegend={true} />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: 'legend-mapId123',
        mapId: 'mapId123',
        setOpen: IS_LEGEND_OPEN_BY_DEFAULT,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should register 2 x multilegends', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: {
        byId: {
          mapId123: {
            id: 'mapId123',
            baseLayers: [],
            mapLayers: [],
          },
          mapId456: {
            id: 'mapId456',
            baseLayers: [],
            mapLayers: [],
          },
        },
        allIds: ['mapId123', 'mapId456'],
      },
      ui: {
        dialogs: {
          legend: {
            isOpen: true,
            activeMapId: '',
          },
        },
      },
      layers: {
        byId: {},
        allIds: [],
      },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    render(
      <CoreThemeStoreProvider store={store}>
        <LegendConnect mapId="mapId123" multiLegend={true} />
        <LegendConnect mapId="mapId456" multiLegend={true} />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: 'legend-mapId123',
        mapId: 'mapId123',
        setOpen: IS_LEGEND_OPEN_BY_DEFAULT,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'legend-mapId456',
        mapId: 'mapId456',
        setOpen: IS_LEGEND_OPEN_BY_DEFAULT,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
