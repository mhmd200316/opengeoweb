/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render } from '@testing-library/react';
import AdagucMapDraw, { AdagucMapDrawProps, EDITMODE } from './AdagucMapDraw';
import {
  simpleBoxGeoJSON,
  simpleMultiPolygon,
  simplePolygonGeoJSON,
} from '../../storybookUtils/geojsonExamples';

describe('components/ReactMapView/AdagucMapDraw', () => {
  it('should render with default props', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      featureNrToEdit: 0,
      geojson: simplePolygonGeoJSON,
    };

    const { container } = render(<AdagucMapDraw {...props} />);

    expect(container.querySelectorAll('div')).toHaveLength(1);
  });

  it('handle edit mode', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      featureNrToEdit: 0,
      geojson: simplePolygonGeoJSON,
    };
    const prevProps = {
      ...props,
      drawMode: '',
    } as unknown as AdagucMapDrawProps;

    const testComponent = new AdagucMapDraw({
      ...(props as unknown as AdagucMapDrawProps),
    });
    const spy = jest.spyOn(testComponent, 'handleDrawMode');

    testComponent.componentDidUpdate(prevProps);

    expect(testComponent.myEditMode).toEqual(EDITMODE.EMPTY);
    expect(spy).toHaveBeenCalledWith(props.drawMode);
  });

  it('handle cancel edit', () => {
    const props = {
      isInEditMode: false,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      featureNrToEdit: 0,
      geojson: simplePolygonGeoJSON,
    };

    const prevProps = {
      ...props,
      isInEditMode: true,
    } as unknown as AdagucMapDrawProps;

    const testComponent = new AdagucMapDraw({
      ...props,
    } as unknown as AdagucMapDrawProps);
    testComponent.myEditMode = EDITMODE.DELETE_FEATURES;
    const spy = jest.spyOn(testComponent, 'cancelEdit');
    const drawspy = jest.spyOn(testComponent, 'handleDrawMode');

    testComponent.componentDidUpdate(prevProps);

    expect(testComponent.myEditMode).toEqual(EDITMODE.EMPTY);
    expect(spy).toHaveBeenCalledWith(true);
    expect(drawspy).not.toHaveBeenCalled();
  });

  it('handle change snappedPolygonIndex nr correctly', () => {
    const props: AdagucMapDrawProps = {
      isInEditMode: true,
      isInDeleteMode: false,
      drawMode: 'POLYGON',
      onHoverFeature: jest.fn(),
      updateGeojson: jest.fn(),
      exitDrawModeCallback: jest.fn(),
      featureNrToEdit: 0,
      geojson: simpleMultiPolygon,
    } as unknown as AdagucMapDrawProps;

    const instanceOfAdagucMapDraw = new AdagucMapDraw({
      ...props,
    } as unknown as AdagucMapDrawProps);

    instanceOfAdagucMapDraw.snappedPolygonIndex = 1;
    instanceOfAdagucMapDraw.handleGeoJSONUpdate(simpleBoxGeoJSON);
    instanceOfAdagucMapDraw.componentDidUpdate(props);
    expect(instanceOfAdagucMapDraw.snappedPolygonIndex).toEqual(0);
  });
});
