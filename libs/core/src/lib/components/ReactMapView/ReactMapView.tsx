/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import moment from 'moment';
// TODO: replace with lodash debounce https://gitlab.com/opengeoweb/opengeoweb/-/issues/504
import { debounce } from 'throttle-debounce';
import { WMJSMap, WMLayer, WMBBOX, debug, DebugType } from '@opengeoweb/webmap';

import tileRenderSettings from '../../store/mapStore/utils/tilesettings';

import {
  registerWMLayer,
  getWMLayerById,
  registerWMJSMap,
} from '../../store/mapStore/utils/helpers';
import { setServiceMetadata } from './ReactMapViewParseLayer';
import AdagucMapDrawContainer, { FeatureLayer } from './AdagucMapDrawContainer';

import { ReactMapViewProps } from './types';
import { Layer, LayerType } from '../../store/mapStore/layers/types';
import MapViewLayer from '../MapView/MapViewLayer';

/**
 * Returns filtered list of props with geoJson
 * @param children React.ReactNode, layers with geoJson
 */
export const getFeatureLayers = (children: React.ReactNode): FeatureLayer[] =>
  children && Array.isArray(children)
    ? children
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        .reduce((acc, val) => acc.concat(val), [])
        .filter((c) => c && c.props && c.props.geojson)
        .map((c) => c.props)
        .reverse()
    : [];
/**
 * Returns true if this is a maplayer and not a baselayer or overlayer
 * @param layer The Layer object, or the props from the ReactWMJSLayer
 */
export const isAMapLayer = (layer: Layer): boolean =>
  layer.layerType === LayerType.mapLayer;

/**
 * Returns true if this is a geojsonlayer (layer containing geojson field)
 * @param layer The Layer object, or the props from the ReactWMJSLayer
 */
export const isAGeoJSONLayer = (layer: Layer): boolean =>
  layer.geojson !== undefined;

const getDisplayText = (currentAdagucTime: string): string => {
  const timeFormat = 'ddd DD MMM YYYY HH:mm [UTC]';
  const adagucTime = moment.utc(currentAdagucTime);
  const adagucTimeFormatted = adagucTime.format(timeFormat).toString();
  return adagucTimeFormatted;
};

export const ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION =
  'ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION';

export const ORIGIN_REACTMAPVIEW_ONUPDATELAYERINFO =
  'ORIGIN_REACTMAPVIEW_ONUPDATELAYERINFO';

interface ReactMapViewState {
  adagucInitialised: boolean;
}

interface AdagucObjectProp {
  webMapJSCreated: boolean;
  initialized: boolean;
  baseLayers: WMLayer[];
  webMapJS: WMJSMap;
  oldbbox: WMBBOX;
}

class ReactMapView extends React.Component<
  ReactMapViewProps,
  ReactMapViewState
> {
  adaguc: AdagucObjectProp = {
    webMapJSCreated: false,
    initialized: false,
    baseLayers: [],
    webMapJS: undefined,
    oldbbox: undefined,
  };

  currentMapProps: { children?: React.ReactNode };

  mapTimer = undefined;

  featureLayerUpdateTimer = undefined;

  currentWidth = undefined;

  currentHeight = undefined;

  adagucContainerRef;

  adagucWebMapJSRef;

  refetchTimer = null;

  static defaultProps = {
    srs: 'EPSG:3857',
    bbox: {
      left: -2324980.5498391856,
      bottom: 5890854.775012179,
      right: 6393377.702660825,
      top: 11652109.058827976,
    },
    shouldAutoFetch: true,
    displayMapPin: false,
    disableMapPin: false,
    onMount: (): void => null,
    onUnMount: (): void => null,
    onMapChangeDimension: (): void => {
      /* nothing */
    },
    onUpdateLayerInformation: (): void => {
      /* nothing */
    },
    onMapZoomEnd: (): void => {
      /* nothing */
    },
  };

  constructor(props: ReactMapViewProps) {
    super(props);
    this.state = {
      adagucInitialised: false,
    };
    this.resize = this.resize.bind(this);
    this.handleWindowResize = this.handleWindowResize.bind(this);
    this.drawDebounced = debounce(600, this.drawDebounced);
    this.checkNewProps = this.checkNewProps.bind(this);
    this.checkAdaguc = this.checkAdaguc.bind(this);
    this.onAfterSetBBoxListener = this.onAfterSetBBoxListener.bind(this);
    this.onUpdateBBoxListener = this.onUpdateBBoxListener.bind(this);
    this.onDimChangeListener = this.onDimChangeListener.bind(this);
    this.currentMapProps = {};
    this.adagucContainerRef = React.createRef();
    this.adagucWebMapJSRef = React.createRef();
  }

  componentDidMount(): void {
    const { onMount, mapId, onRegisterAdaguc, shouldAutoFetch } = this.props;

    this.checkAdaguc();
    this.checkNewProps(null, this.props);
    window.addEventListener('resize', this.handleWindowResize);
    if (this.adaguc.initialized === false && this.adaguc.webMapJS) {
      this.adaguc.initialized = true;

      if (onRegisterAdaguc) {
        onRegisterAdaguc(this.adaguc.webMapJS);
      }
    }

    onMount(mapId, this.adaguc.webMapJS);

    if (shouldAutoFetch) {
      this.onStartRefetchTimer();
    }
  }

  componentDidUpdate = (prevProps: ReactMapViewProps): void => {
    this.checkNewProps(prevProps, this.props);
  };

  componentWillUnmount(): void {
    const { onUnMount, mapId } = this.props;

    window.removeEventListener('resize', this.handleWindowResize);
    onUnMount(mapId, this.adaguc.webMapJS);
    this.adaguc.webMapJS.getListener().suspendEvents();

    if (typeof this.adaguc.webMapJS.stopAnimating === 'function') {
      this.adaguc.webMapJS.stopAnimating();
      this.adaguc.webMapJS.destroy();
      this.adaguc.webMapJSCreated = false;
    }

    this.clearRefetchTimer();
  }

  onDimChangeListener(): void {
    const { mapId, onMapChangeDimension } = this.props;
    if (this.adaguc && this.adaguc.webMapJS) {
      const timeDimension = this.adaguc.webMapJS.getDimension('time');
      if (timeDimension) {
        onMapChangeDimension({
          mapId,
          origin: ORIGIN_REACTMAPVIEW_ONMAPCHANGEDIMENSION,
          dimension: {
            name: 'time',
            currentValue: timeDimension.currentValue,
          },
        });
      }
    }
  }
  onAfterSetBBoxListener(wmjsMap: WMJSMap): void {
    const { mapId, onMapZoomEnd } = this.props;
    /* Update the map after 100 ms */
    window.setTimeout(() => {
      const { bbox: MapViewBBOX } = this.props;
      const projectionInfo = wmjsMap.getProjection();
      /* Trigger onMapZoomEnd callback */
      const mapBBOX = projectionInfo.bbox;
      if (
        MapViewBBOX.left !== mapBBOX.left ||
        MapViewBBOX.right !== mapBBOX.right ||
        MapViewBBOX.bottom !== mapBBOX.bottom ||
        MapViewBBOX.top !== mapBBOX.top
      ) {
        onMapZoomEnd({
          mapId,
          bbox: {
            left: projectionInfo.bbox.left,
            bottom: projectionInfo.bbox.bottom,
            right: projectionInfo.bbox.right,
            top: projectionInfo.bbox.top,
          },
          srs: projectionInfo.srs,
        });
      }
    }, 100);
  }
  onUpdateBBoxListener(newBbox: WMBBOX): void {
    const oldbbox =
      this.adaguc.oldbbox || new WMBBOX(ReactMapView.defaultProps.bbox);
    if (
      oldbbox.left !== newBbox.left ||
      oldbbox.right !== newBbox.right ||
      oldbbox.top !== newBbox.top ||
      oldbbox.bottom !== newBbox.bottom
    ) {
      oldbbox.left = newBbox.left;
      oldbbox.right = newBbox.right;
      oldbbox.top = newBbox.top;
      oldbbox.bottom = newBbox.bottom;
      this.adaguc.oldbbox = new WMBBOX(oldbbox);
    }
  }
  onStartRefetchTimer = (): void => {
    /* 
      TODO, Maarten Plieger, 2021-09-02, https://gitlab.com/opengeoweb/opengeoweb/-/issues/1159: 
      This causes many issues, we should improve the update strategy.
      Certain presets have >20 layers. This causes 20 times parsing the GetCapabilities for a minor update of the store.    


    */
    this.clearRefetchTimer();
    const fetchSpeed = 60000; // 1 minute
    this.refetchTimer = setInterval(() => {
      const layers = this.adaguc.webMapJS.getLayers();
      layers.forEach((layer: WMLayer) => {
        if (layer.enabled !== false) {
          this.parseWMJSLayer(layer, true);
        }
      });
    }, fetchSpeed);
  };

  getWMJSLayerFromReactLayer(
    wmLayers: WMLayer[],
    reactWebMapJSLayer: React.ReactElement,
    index: number,
  ): { layer: WMLayer; layerArrayMutated: boolean } {
    let foundLayer = null;
    if (reactWebMapJSLayer.props.name && reactWebMapJSLayer.props.id) {
      if (index >= 0 && index < wmLayers.length) {
        for (
          let layerIndex = 0;
          layerIndex < wmLayers.length;
          layerIndex += 1
        ) {
          const secondIndex = wmLayers.length - 1 - index;
          const layer = wmLayers[layerIndex];
          if (layer === getWMLayerById(reactWebMapJSLayer.props.id)) {
            foundLayer = layer;
            if (
              isAMapLayer(reactWebMapJSLayer.props) &&
              layerIndex !== secondIndex
            ) {
              this.adaguc.webMapJS.swapLayers(
                wmLayers[layerIndex],
                wmLayers[secondIndex],
              );
              this.adaguc.webMapJS.draw();
              return { layer: foundLayer, layerArrayMutated: true };
            }
          }
        }
      }
    }
    return { layer: foundLayer, layerArrayMutated: false };
  }

  clearRefetchTimer = (): void => {
    if (this.refetchTimer) {
      clearInterval(this.refetchTimer);
    }
  };

  parseWMJSLayer = (
    wmLayer: WMLayer,
    forceReload: boolean,
    child?: React.ReactElement,
  ): void => {
    const callback = (): void => {
      const { onUpdateLayerInformation, mapId } = this.props;

      setServiceMetadata(
        wmLayer,
        mapId,
        this.adaguc.webMapJS,
        this.props,
        onUpdateLayerInformation,
      );

      if (child) {
        if (wmLayer.hasError) {
          if (child.props.onLayerError) {
            child.props.onLayerError(
              wmLayer,
              new Error(wmLayer.lastError),
              this.adaguc.webMapJS,
            );
          }
        } else if (child.props.onLayerReady) {
          child.props.onLayerReady(wmLayer, this.adaguc.webMapJS);
        }
      }
    };

    wmLayer.parseLayer(callback, forceReload, 'ReactMapView parseWMJSLayer');
  };

  checkNewProps(prevProps: ReactMapViewProps, props: ReactMapViewProps): void {
    if (!props) {
      return;
    }

    let needsRedraw = false;

    /* Check map props */
    if (!prevProps || prevProps.showLegend !== props.showLegend) {
      this.adaguc.webMapJS.displayLegendInMap(props.showLegend !== false);
    }
    if (!prevProps || prevProps.showScaleBar !== props.showScaleBar) {
      this.adaguc.webMapJS.displayScaleBarInMap(props.showScaleBar !== false);
      needsRedraw = true;
    }

    /* Check map dimensions */
    if (!prevProps || prevProps.dimensions !== props.dimensions) {
      if (props.dimensions) {
        for (let d = 0; d < props.dimensions.length; d += 1) {
          const propDimension = props.dimensions[d];
          const mapDim = this.adaguc.webMapJS.getDimension(propDimension.name);
          if (mapDim && mapDim.currentValue !== propDimension.currentValue) {
            this.adaguc.webMapJS.setDimension(
              propDimension.name,
              propDimension.currentValue,
              false,
              false,
            );
          }
          if (
            props.displayTimeInMap &&
            propDimension.name === 'time' &&
            propDimension.currentValue !== undefined
          ) {
            needsRedraw = true;
            const displayText = getDisplayText(propDimension.currentValue);
            this.adaguc.webMapJS.setTimeOffset(displayText);
          }
        }
      }
    }

    /* Check if srs and BBOX is updated */
    if (!prevProps || prevProps.bbox !== props.bbox) {
      if (props.bbox.left !== undefined) {
        const projectionInfo = this.adaguc.webMapJS.getProjection();
        const mapBBOX = projectionInfo.bbox;
        if (
          props.bbox.left !== mapBBOX.left ||
          props.bbox.right !== mapBBOX.right ||
          props.bbox.bottom !== mapBBOX.bottom ||
          props.bbox.top !== mapBBOX.top
        ) {
          this.adaguc.webMapJS.suspendEvent('onupdatebbox');
          this.adaguc.webMapJS.setProjection(props.srs, new WMBBOX(props.bbox));
          this.adaguc.webMapJS.resumeEvent('onupdatebbox');
          this.adaguc.webMapJS.draw();
        }
      }
    }

    /* Check display/hide map cursor */
    if (!prevProps || prevProps.displayMapPin !== props.displayMapPin) {
      if (props.displayMapPin === true) {
        this.adaguc.webMapJS.getMapPin().showMapPin();
      } else if (props.displayMapPin === false) {
        this.adaguc.webMapJS.getMapPin().hideMapPin();
      }
    }
    /* Set map cursor location */
    if (!prevProps || prevProps.mapPinLocation !== props.mapPinLocation) {
      if (props.mapPinLocation) {
        this.adaguc.webMapJS.getMapPin().positionMapPinByLatLon({
          x: props.mapPinLocation.lon,
          y: props.mapPinLocation.lat,
        });
      }
    }
    /* Set disable map pin */
    if (!prevProps || prevProps.disableMapPin !== props.disableMapPin) {
      if (props.disableMapPin === true) {
        this.adaguc.webMapJS.getMapPin().setMapPinDisabled();
      } else if (props.disableMapPin === false) {
        this.adaguc.webMapJS.getMapPin().setMapPinEnabled();
      }
    }

    /* Change the animation delay */
    if (!prevProps || prevProps.animationDelay !== props.animationDelay) {
      if (props.animationDelay) {
        if (typeof this.adaguc.webMapJS.setAnimationDelay === 'function') {
          this.adaguc.webMapJS.setAnimationDelay(props.animationDelay);
        }
      }
    }

    /* Check if layer metadata should be shown */
    if (!prevProps || prevProps.showLayerInfo !== props.showLayerInfo) {
      if (props.showLayerInfo) {
        this.adaguc.webMapJS.showLayerInfo = props.showLayerInfo;
      }
    }

    const { children } = props;
    if (children !== this.currentMapProps.children) {
      const myChildren = [];
      const takenIds = new Set();

      React.Children.forEach(children, (child: React.ReactElement) => {
        if (child) {
          const { props: childProps } = child;
          if (childProps && childProps.id) {
            if (!takenIds.has(childProps.id)) {
              myChildren.push(child);
              takenIds.add(childProps.id);
            } else {
              childProps.onLayerError(
                childProps,
                new Error(
                  `Duplicate layer id "${childProps.id}" encountered within this WebMap`,
                ),
                this.adaguc.webMapJS,
              );
              console.warn('ReactWMJSLayer has a duplicate id', child);
            }
          } else {
            debug(
              DebugType.Warning,
              'ReactElement child ignored: has no props or id',
              child,
            );
          }
        }
      });
      myChildren.reverse();

      const wmjsLayers = this.adaguc.webMapJS.getLayers();
      /* ReactWMJSLayer Layer Childs: Detect all ReactLayers connected to WMJSLayers, remove WMJSLayer if there is no ReactLayer */
      for (let l = 0; l < wmjsLayers.length; l += 1) {
        if (
          myChildren.filter(
            (c) =>
              c && c.props && c.props.id === wmjsLayers[l].ReactWMJSLayerId,
          ).length === 0
        ) {
          /* This will call the remove property of the WMJSLayer, which will adjust the layers array of WebMapJS */
          wmjsLayers[l].remove();
          /* Trigger update of all map dimensions */

          const { onUpdateLayerInformation, mapId } = props;
          onUpdateLayerInformation &&
            onUpdateLayerInformation({
              origin: ORIGIN_REACTMAPVIEW_ONUPDATELAYERINFO,
              serviceLayers: null,
              layerStyle: null,
              layerDimensions: null,
              mapDimensions: {
                origin,
                mapId,
                dimensions: this.adaguc.webMapJS.mapdimensions.map(
                  ({ name, units, currentValue, synced }) => {
                    return {
                      name,
                      units,
                      currentValue,
                      synced,
                    };
                  },
                ),
              },
            });
          this.checkNewProps(prevProps, props);
          return;
        }
      }

      /* ReactWMJSLayer BaseLayer Childs: For the baseLayers, detect all ReactLayers connected to WMJSLayers, remove WMJSLayer if there is no ReactLayer */
      const webmapJSBaselayers = this.adaguc.webMapJS.getBaseLayers();
      for (let l = 0; l < webmapJSBaselayers.length; l += 1) {
        let wmjsBaseLayers = this.adaguc.webMapJS.getBaseLayers();
        if (
          myChildren.filter((c) =>
            c && c.props
              ? c.props.id === wmjsBaseLayers[l].ReactWMJSLayerId
              : false,
          ).length === 0
        ) {
          /*  TODO (Maarten Plieger, 2020-03-19):The remove property for the baselayer is not working yet */
          wmjsBaseLayers.splice(l, 1);
          this.adaguc.webMapJS.setBaseLayers(wmjsBaseLayers);
          wmjsBaseLayers = this.adaguc.webMapJS.getBaseLayers();
          this.checkNewProps(prevProps, props);
          return;
        }
      }

      /* Loop through all layers and update WMJSLayer properties where needed */

      let adagucWMJSLayerIndex = 0;
      let adagucWMJSBaseLayerIndex = 0;

      for (let c = 0; c < myChildren.length; c += 1) {
        const child = myChildren[c];
        if (child && child.type) {
          /* Check layers */
          if (typeof child.type === typeof MapViewLayer) {
            /* Feature layer (with child.props.geojson), these are handled collectively by the setState commando above. */
            const isBaselayer =
              !isAMapLayer(child.props) && !isAGeoJSONLayer(child.props);

            const adagucWMJSLayers = isBaselayer
              ? this.adaguc.webMapJS.getBaseLayers()
              : this.adaguc.webMapJS.getLayers();
            const obj = this.getWMJSLayerFromReactLayer(
              adagucWMJSLayers,
              child,
              isBaselayer ? adagucWMJSBaseLayerIndex : adagucWMJSLayerIndex,
            );
            if (obj.layerArrayMutated) {
              this.checkNewProps(prevProps, props);
              return;
            }
            const wmLayer = obj.layer;
            if (isBaselayer) {
              adagucWMJSBaseLayerIndex += 1;
            } else {
              adagucWMJSLayerIndex += 1;
            }
            if (wmLayer === null) {
              const keepOnTop =
                child.props.layerType === LayerType.overLayer || false;
              const newWMLayer = new WMLayer({
                ...child.props,
                keepOnTop,
              });
              registerWMLayer(newWMLayer, child.props.id);
              newWMLayer.ReactWMJSLayerId = child.props.id;
              if (isBaselayer) {
                /* Add ADAGUC WebMapJS Baselayer */
                this.adaguc.baseLayers.push(newWMLayer);
                this.adaguc.webMapJS.setBaseLayers(
                  this.adaguc.baseLayers.reverse(),
                );
              } else {
                /* Add ADAGUC WebMapJS Layer */
                this.adaguc.webMapJS.addLayer(newWMLayer).then(() => {
                  this.adaguc.webMapJS.draw();
                });
              }
              if (!isBaselayer) {
                this.parseWMJSLayer(newWMLayer, false, child);
              }
              needsRedraw = true;
            } else {
              /* Set the name of the ADAGUC WMJSLayer */
              if (
                child.props.name !== undefined &&
                wmLayer.name !== child.props.name
              ) {
                wmLayer.setName(child.props.name).then(() => {
                  this.parseWMJSLayer(wmLayer, false, child);
                });
                needsRedraw = true;
              }

              /* Set the Opacity of the ADAGUC WMJSLayer */
              if (
                child.props.opacity !== undefined &&
                wmLayer.opacity !== parseFloat(child.props.opacity)
              ) {
                wmLayer.setOpacity(child.props.opacity);
                needsRedraw = false;
              }

              /* Set the Style of the ADAGUC WMJSLayer */
              if (
                child.props.style !== undefined &&
                wmLayer.currentStyle !== child.props.style
              ) {
                wmLayer.setStyle(child.props.style);
                needsRedraw = true;
              }

              /* Set the Enabled prop of the ADAGUC WMJSLayer */
              if (
                child.props.enabled !== undefined &&
                wmLayer.enabled !== child.props.enabled
              ) {
                wmLayer.display(child.props.enabled);
                needsRedraw = true;
              }

              /* Set the dimensions of the ADAGUC WMJSLayer */
              if (child.props.dimensions !== undefined) {
                for (let d = 0; d < child.props.dimensions.length; d += 1) {
                  const dim = child.props.dimensions[d];
                  const wmjsDim = wmLayer.getDimension(dim.name);
                  if (wmjsDim) {
                    if (wmjsDim.currentValue !== dim.currentValue) {
                      wmLayer.setDimension(dim.name, dim.currentValue, false);
                      needsRedraw = true;
                    }
                    if (wmjsDim.synced !== dim.synced) {
                      wmjsDim.synced = dim.synced;
                      needsRedraw = true;
                    }
                  } else {
                    debug(
                      DebugType.Warning,
                      `MapView: Dimension does not exist, skipping ${child.props.name} :: ${dim.name} = ${dim.currentValue}`,
                    );
                  }
                }
              }

              if (child.props.isProfileLayer) {
                const currentBbox = this.adaguc.webMapJS.getBBOX();
                const newBbox = new WMBBOX(props.bbox);
                if (currentBbox !== newBbox) {
                  this.adaguc.webMapJS.suspendEvent('onupdatebbox');
                  this.adaguc.webMapJS.setBBOX(new WMBBOX(props.bbox));
                  needsRedraw = true;
                  this.adaguc.webMapJS.resumeEvent('onupdatebbox');
                }
              }
            }
          }
        }
      }
      if (needsRedraw) {
        this.adaguc.webMapJS.draw();
      }
      /* Children have been processed */
      this.currentMapProps.children = children;
    }
  }

  drawDebounced(): void {
    this.adaguc.webMapJS.getListener().suspendEvents();
    this.adaguc.webMapJS.draw();
    this.adaguc.webMapJS.getListener().resumeEvents();
  }

  handleWindowResize(): void {
    this.resize();
  }

  checkAdaguc(): void {
    const { mapId, listeners, srs, bbox, onMapPinChangeLocation } = this.props;
    if (this.adaguc.webMapJSCreated) {
      return;
    }
    this.adaguc.webMapJSCreated = true;
    this.adaguc.webMapJS = new WMJSMap(this.adagucWebMapJSRef.current);
    this.adaguc.webMapJS.showLayerInfo =
      false; /* Enable to show actual layer properties in the map */
    registerWMJSMap(this.adaguc.webMapJS, mapId);
    this.adaguc.webMapJS.removeAllLayers();
    this.adaguc.webMapJS.setProjection(srs, new WMBBOX(bbox));
    this.adaguc.webMapJS.setWMTileRendererTileSettings(tileRenderSettings);

    if (listeners) {
      listeners.forEach((listener) => {
        this.adaguc.webMapJS.addListener(
          listener.name,
          (data) => {
            listener.callbackfunction(this.adaguc.webMapJS, data);
          },
          listener.keep,
        );
      });
    }

    this.adaguc.webMapJS.addListener(
      'ondimchange',
      () => {
        this.onDimChangeListener();
      },
      true,
    );

    this.adaguc.webMapJS.addListener('onmapready', () => {
      this.setState({ adagucInitialised: true });
    });

    this.adaguc.webMapJS.addListener(
      'aftersetbbox',
      (wmjsMap) => {
        this.onAfterSetBBoxListener(wmjsMap);
      },
      true,
    );

    this.adaguc.webMapJS.addListener(
      'onupdatebbox',
      (newBbox) => {
        this.onUpdateBBoxListener(newBbox);
      },
      true,
    );

    this.adaguc.webMapJS.addListener(
      'onsetmappin',
      (mapPinLatLonCoordinate): void => {
        // only change location when mapPin is visible and enabled
        if (
          onMapPinChangeLocation &&
          // eslint-disable-next-line react/destructuring-assignment
          this.props.displayMapPin &&
          // eslint-disable-next-line react/destructuring-assignment
          !this.props.disableMapPin
        ) {
          onMapPinChangeLocation({
            mapPinLocation: {
              lat: mapPinLatLonCoordinate.lat,
              lon: mapPinLatLonCoordinate.lon,
            },
            mapId,
          });
        }
      },
      true,
    );

    this.resize();
    this.adaguc.webMapJS.draw();
  }

  resize(): void {
    const element = this.adagucContainerRef.current;
    if (element) {
      const newWidth = element.clientWidth;
      const newHeight = element.clientHeight;
      if (this.currentWidth !== newWidth || this.currentHeight !== newHeight) {
        this.currentWidth = newWidth;
        this.currentHeight = newHeight;
        this.adaguc.webMapJS.setSize(newWidth, newHeight);
      }
    }
  }

  render(): React.ReactElement {
    const { passiveMap, children, onClick } = this.props;
    const { adagucInitialised } = this.state;
    const featureLayers = getFeatureLayers(children);

    return (
      <div
        className="MapView"
        style={{
          height: '100%',
          width: '100%',
          border: 'none',
          display: 'block',
          overflow: 'hidden',
        }}
      >
        <div
          ref={this.adagucContainerRef}
          style={{
            minWidth: 'inherit',
            minHeight: 'inherit',
            width: 'inherit',
            height: 'inherit',
            overflow: 'hidden',
            display: 'block',
            border: 'none',
          }}
        >
          <div
            className="MapViewComponent"
            style={{
              position: 'absolute',
              overflow: 'hidden',
              display: 'block',
              padding: '0',
              margin: '0',
              zIndex: 10,
            }}
          >
            <div ref={this.adagucWebMapJSRef} />
          </div>
          {/* MapViewLayers */}
          <div
            style={{
              position: 'absolute',
              overflow: 'hidden',
              display: 'block',
              padding: '0',
              margin: '0',
              zIndex: 100,
            }}
          >
            <div>{children}</div>
            {adagucInitialised && featureLayers && featureLayers.length ? (
              <AdagucMapDrawContainer
                featureLayers={featureLayers}
                webMapJS={this.adaguc.webMapJS}
              />
            ) : null}
          </div>
          {passiveMap && (
            // eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/control-has-associated-label, jsx-a11y/interactive-supports-focus
            <div
              style={{
                position: 'absolute',
                overflow: 'hidden',
                display: 'block',
                padding: '0',
                margin: '0',
                zIndex: 100,
                width: '100%',
                height: '100%',
              }}
              onClick={onClick}
              role="button"
            />
          )}
        </div>
      </div>
    );
  }
}

export default ReactMapView;
