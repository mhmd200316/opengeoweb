/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

// TODO: fix these https://gitlab.com/opengeoweb/opengeoweb/-/issues/626
/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
import * as React from 'react';
import cloneDeep from 'lodash.clonedeep';
import proj4, { InterfaceProjection } from 'proj4';
import { GeoJsonProperties, GeometryObject, Position } from 'geojson';
import { WMJSMap, getMapImageStore } from '@opengeoweb/webmap';
import {
  checkHoverFeatures,
  CheckHoverFeaturesResult,
} from './AdagucMapDrawTools';

const Proj4js = proj4;
// Cache for for storing and reusing Proj4 instances
const projectorCache = {};

// Ensure that you have a Proj4 object, pulling from the cache if necessary
const getProj4 = (projection): InterfaceProjection => {
  if (projection instanceof Proj4js.Proj) {
    return projection as InterfaceProjection;
  }
  if (projection in projectorCache) {
    return projectorCache[projection];
  }
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  projectorCache[projection] = new Proj4js.Proj(projection);
  return projectorCache[projection];
};

export const emptyGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [],
};

export const featurePoint: GeoJSON.Feature<GeoJSON.Point> = {
  type: 'Feature',
  properties: {},
  geometry: {
    type: 'Point',
    coordinates: [],
  },
};

export const featureMultiPoint: GeoJSON.Feature<GeoJSON.MultiPoint> = {
  type: 'Feature',
  properties: {},
  geometry: {
    type: 'MultiPoint',
    coordinates: [[]],
  },
};

export const featurePolygon: GeoJSON.Feature<GeoJSON.Polygon> = {
  type: 'Feature',
  properties: {},
  geometry: {
    type: 'Polygon',
    coordinates: [[]],
  },
};

export const featureBox: GeoJSON.Feature<GeoJSON.Polygon> = {
  type: 'Feature',
  properties: { _adaguctype: 'box' },
  geometry: {
    type: 'Polygon',
    coordinates: [[]],
  },
};

export const lineString: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        stroke: '#66F',
        'stroke-width': 5,
        'stroke-opacity': '1',
      },
      geometry: {
        type: 'LineString',
        coordinates: [[]],
      },
    },
  ],
};
export type GeoJsonFeatureType =
  | GeoJSON.Polygon
  | GeoJSON.MultiPoint
  | GeoJSON.Point
  | GeoJSON.LineString
  | GeoJSON.MultiPolygon;
export type GeoJsonFeature = GeoJSON.Feature<GeoJsonFeatureType>;

export type GeoFeatureStyle = {
  stroke?: string;
  fill?: string;
  'stroke-width'?: number;
  'stroke-opacity'?: number;
  'fill-opacity'?: number;
};

export type Coordinate = {
  x: number;
  y: number;
};

type DrawStyle = {
  x: number;
  y: number;
  nr: number;
};

type AdagucInputEvent = {
  leftButton: boolean;
  mouseDown: boolean;
  mouseX: number;
  mouseY: number;
  rightButton: boolean;
};

export interface FeatureEvent {
  coordinateIndexInFeature: number;
  featureIndex: number;
  mouseX: number;
  mouseY: number;
  isInEditMode: boolean;
  feature: GeoJsonFeature;
}

export interface AdagucMapDrawProps {
  webmapjs: WMJSMap;
  geojson: GeoJSON.FeatureCollection;
  drawMode: string;
  deletePolygonCallback?: string;
  exitDrawModeCallback: () => void;
  isInEditMode: boolean;
  isInDeleteMode: boolean;
  featureNrToEdit: number;
  onHoverFeature?: (event: FeatureEvent) => void;
  onClickFeature?: (event: FeatureEvent) => void;
  updateGeojson: (geoJson: GeometryObject, text: string) => void;
}

export enum EDITMODE {
  EMPTY = 'EMPTY',
  DELETE_FEATURES = 'DELETE_FEATURES',
  ADD_FEATURE = 'ADD_FEATURE',
}
enum DRAWMODE {
  POLYGON = 'POLYGON',
  BOX = 'BOX',
  MULTIPOINT = 'MULTIPOINT',
  POINT = 'POINT',
  LINESTRING = 'LINESTRING',
}
enum VERTEX {
  NONE = 'NONE',
  MIDDLE_POINT_OF_FEATURE = 'MIDDLE_POINT_OF_FEATURE',
}
enum EDGE {
  NONE = 'NONE',
}
enum SNAPPEDFEATURE {
  NONE = 'NONE',
}
enum DRAGMODE {
  NONE = 'NONE',
  VERTEX = 'VERTEX',
  FEATURE = 'FEATURE',
}

export default class AdagucMapDraw extends React.PureComponent<AdagucMapDrawProps> {
  myEditMode: EDITMODE;

  myDrawMode: DRAWMODE;

  textPositions: { x: number; y: number; text: string }[];

  mouseOverPolygonCoordinates: Coordinate[] | number;

  defaultPolyProps: GeoFeatureStyle;

  defaultLineStringProps: GeoFeatureStyle;

  defaultIconProps: {
    imageWidth: number;
    imageHeight: number;
  };

  somethingWasDragged: string;

  mouseIsOverVertexNr: SNAPPEDFEATURE | VERTEX | number;

  selectedEdge: EDGE;

  geojson: GeoJSON.FeatureCollection;

  listenersInitialized: boolean;

  mouseGeoCoord: Coordinate;

  snappedGeoCoords: Coordinate;

  doubleClickTimer: {
    isRunning?: boolean;
    timer?: number;
    mouseX?: number;
    mouseY?: number;
  };

  mouseStoppedTimer: number;

  snappedPolygonIndex: SNAPPEDFEATURE | number;

  mouseX: number;

  mouseY: number;

  mouseOverPolygonFeatureIndex: number;

  mouseOverPolygonIndex: number;

  drawMode: string;

  disabled: boolean;

  // eslint-disable-next-line react/static-property-placement
  static defaultProps = {
    isInEditMode: false,
    isInDeleteMode: false,
    webmapjs: undefined,
    featureNrToEdit: 0,
  };

  constructor(props: AdagucMapDrawProps) {
    super(props);
    this.myEditMode = EDITMODE.EMPTY;
    this.myDrawMode = DRAWMODE.POLYGON;
    this.adagucBeforeDraw = this.adagucBeforeDraw.bind(this);
    this.adagucMouseMove = this.adagucMouseMove.bind(this);
    this.adagucMouseDown = this.adagucMouseDown.bind(this);
    this.deleteFeature = this.deleteFeature.bind(this);
    this.adagucMouseUp = this.adagucMouseUp.bind(this);
    this.cancelEdit = this.cancelEdit.bind(this);
    this.distance = this.distance.bind(this);
    this.isBetween = this.isBetween.bind(this);
    this.checkDist = this.checkDist.bind(this);
    this.hoverEdge = this.hoverEdge.bind(this);
    this.drawPolygon = this.drawPolygon.bind(this);
    this.moveVertex = this.moveVertex.bind(this);
    this.featureHasChanged = this.featureHasChanged.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.hoverVertex = this.hoverVertex.bind(this);
    this.transposePolygon = this.transposePolygon.bind(this);
    this.transposeVertex = this.transposeVertex.bind(this);
    this.triggerMouseDownTimer = this.triggerMouseDownTimer.bind(this);
    this.adagucMouseDoubleClick = this.adagucMouseDoubleClick.bind(this);
    this.handleDrawMode = this.handleDrawMode.bind(this);
    this.componentCleanup = this.componentCleanup.bind(this);
    this.initializeFeature = this.initializeFeature.bind(this);
    this.validatePolys = this.validatePolys.bind(this);
    this.insertVertexInEdge = this.insertVertexInEdge.bind(this);
    this.createNewFeature = this.createNewFeature.bind(this);
    this.addPointToMultiPointFeature =
      this.addPointToMultiPointFeature.bind(this);
    this.addVerticesToPolygonFeature =
      this.addVerticesToPolygonFeature.bind(this);
    this.checkIfFeatureIsBox = this.checkIfFeatureIsBox.bind(this);

    this.handleDrawMode(props.drawMode);

    this.textPositions = [];
    this.mouseOverPolygonCoordinates = [];

    this.defaultPolyProps = {
      stroke: '#000',
      'stroke-width': 0.4,
      'stroke-opacity': 1,
      fill: '#33cc00',
      'fill-opacity': 1,
    };

    this.defaultLineStringProps = {
      stroke: '#000',
      'stroke-width': 1.5,
      'stroke-opacity': 1,
      fill: '#33cc00',
      'fill-opacity': 1,
    };

    this.defaultIconProps = {
      imageWidth: 30,
      imageHeight: 30,
    };

    this.somethingWasDragged = DRAGMODE.NONE;
    this.mouseIsOverVertexNr = VERTEX.NONE;
    this.selectedEdge = EDGE.NONE;

    if (props.geojson) {
      this.geojson = cloneDeep(props.geojson);
    } else {
      this.geojson = cloneDeep(emptyGeoJSON);
      this.featureHasChanged('new');
    }
    this.validatePolys(true);
  }

  componentDidMount(): void {
    document.addEventListener('keydown', this.handleKeyDown);
  }

  componentDidUpdate(prevProps: AdagucMapDrawProps): void {
    const { geojson, isInEditMode, isInDeleteMode, drawMode } = this.props;
    const prevGeojson = prevProps && prevProps.geojson;
    const prevIsInEditMode = prevProps && prevProps.isInEditMode;
    const prevIsInDeleteMode = prevProps && prevProps.isInDeleteMode;
    const prevDrawMode = prevProps && prevProps.drawMode;

    /* Handle geojson update */
    if (geojson !== prevGeojson) {
      this.handleGeoJSONUpdate(geojson);
    }

    /* Handle toggle edit */
    if (isInEditMode !== prevIsInEditMode) {
      if (isInEditMode === false && this.myEditMode !== EDITMODE.EMPTY) {
        this.cancelEdit(true); /* Throw away last vertice */
        if (this.myEditMode === EDITMODE.DELETE_FEATURES) {
          this.myEditMode = EDITMODE.EMPTY;
          return;
        }
      }
    }

    /* Handle toggle delete */
    if (isInDeleteMode !== prevIsInDeleteMode) {
      if (isInDeleteMode === true) {
        this.myEditMode = EDITMODE.DELETE_FEATURES;
      } else if (this.myEditMode === EDITMODE.DELETE_FEATURES) {
        this.myEditMode = EDITMODE.EMPTY;
      }
    }

    if (
      isInEditMode !== prevIsInEditMode ||
      isInDeleteMode !== prevIsInDeleteMode
    ) {
      if (isInEditMode === false && isInDeleteMode === false) {
        this.myEditMode = EDITMODE.EMPTY;
      }
    }

    /* Handle new drawmode */
    if (drawMode !== prevDrawMode) {
      /* Handle drawmode */
      this.handleDrawMode(drawMode);
    }
  }
  componentWillUnmount(): void {
    this.componentCleanup();
  }

  getPixelCoordFromGeoCoord(featureCoords: Position[]): Coordinate[] {
    const { webmapjs } = this.props;
    const { width, height } = webmapjs.getSize();
    const bbox = webmapjs.getBBOX();
    const proj = webmapjs.getProj4();

    const XYCoords = [];

    const from = getProj4(proj.lonlat);
    const to = getProj4(proj.crs);

    for (let j = 0; j < featureCoords.length; j += 1) {
      // eslint-disable-next-line no-continue
      if (featureCoords[j].length < 2) continue;
      let coordinates = { x: featureCoords[j][0], y: featureCoords[j][1] };
      coordinates = proj.proj4.transform(from, to, coordinates);
      const x =
        (width * (coordinates.x - bbox.left)) / (bbox.right - bbox.left);
      const y =
        (height * (coordinates.y - bbox.top)) / (bbox.bottom - bbox.top);
      XYCoords.push({ x, y });
    }
    return XYCoords;
  }
  handleGeoJSONUpdate = (newGeoJSON: GeoJSON.FeatureCollection): void => {
    const { featureNrToEdit } = this.props;
    this.geojson = cloneDeep(newGeoJSON);
    /* Ensure that this.snappedPolygonIndex is still within the coordinates array */
    if (
      this.geojson &&
      this.geojson.features &&
      featureNrToEdit < this.geojson.features.length
    ) {
      const feature = this.geojson.features[featureNrToEdit];
      if (feature.geometry) {
        const { coordinates } = feature.geometry as GeoJSON.Polygon;
        if (this.snappedPolygonIndex > coordinates.length - 1) {
          this.snappedPolygonIndex = coordinates.length - 1;
        }
      }
    }
  };
  componentCleanup(): void {
    // this will hold the cleanup code
    document.removeEventListener('keydown', this.handleKeyDown);
    const { webmapjs } = this.props;
    if (webmapjs !== undefined && this.listenersInitialized === true) {
      this.listenersInitialized = undefined;
      webmapjs.removeListener('beforecanvasdisplay', this.adagucBeforeDraw);
      webmapjs.removeListener('beforemousemove', this.adagucMouseMove);
      webmapjs.removeListener('beforemousedown', this.adagucMouseDown);
      webmapjs.removeListener('beforemouseup', this.adagucMouseUp);
      webmapjs.draw();
    }
  }

  transposePolygon(featureCoords: Position[]): void {
    const incX = this.mouseGeoCoord.x - this.snappedGeoCoords.x;
    const incY = this.mouseGeoCoord.y - this.snappedGeoCoords.y;
    for (let j = 0; j < featureCoords.length; j += 1) {
      /* eslint-disable-next-line no-param-reassign */
      featureCoords[j][0] += incX;
      /* eslint-disable-next-line no-param-reassign */
      featureCoords[j][1] += incY;
      this.snappedGeoCoords.x = this.mouseGeoCoord.x;
      this.snappedGeoCoords.y = this.mouseGeoCoord.y;
    }
    if (this.myEditMode !== EDITMODE.ADD_FEATURE) {
      this.somethingWasDragged = DRAGMODE.FEATURE;
    }
  }

  transposeVertex(featureCoords: Position[]): void {
    if (this.myEditMode !== EDITMODE.ADD_FEATURE) {
      this.somethingWasDragged = DRAGMODE.VERTEX;
    }

    if (this.myDrawMode === DRAWMODE.POINT) {
      /* eslint-disable-next-line no-param-reassign */
      featureCoords[0] = this.mouseGeoCoord.x as unknown as Position;
      /* eslint-disable-next-line no-param-reassign */
      featureCoords[1] = this.mouseGeoCoord.y as unknown as Position;
      return;
    }

    if (this.myDrawMode === DRAWMODE.LINESTRING) {
      // eslint-disable-next-line no-param-reassign
      featureCoords[this.mouseIsOverVertexNr][0] = this.mouseGeoCoord.x;
      // eslint-disable-next-line no-param-reassign
      featureCoords[this.mouseIsOverVertexNr][1] = this.mouseGeoCoord.y;
      return;
    }

    if (this.myDrawMode === DRAWMODE.MULTIPOINT) {
      if (this.mouseIsOverVertexNr !== SNAPPEDFEATURE.NONE) {
        // eslint-disable-next-line no-param-reassign
        featureCoords[this.mouseIsOverVertexNr][0] = this.mouseGeoCoord.x;
        // eslint-disable-next-line no-param-reassign
        featureCoords[this.mouseIsOverVertexNr][1] = this.mouseGeoCoord.y;
      }
      return;
    }

    if (
      this.myDrawMode === DRAWMODE.BOX &&
      (featureCoords.length === 4 || featureCoords.length === 5)
    ) {
      while (featureCoords.length < 4) {
        featureCoords.push([this.mouseGeoCoord.x, this.mouseGeoCoord.y]);
      }
      if (this.mouseIsOverVertexNr === 0) {
        featureCoords[0][0] = this.mouseGeoCoord.x;
        featureCoords[0][1] = this.mouseGeoCoord.y;

        featureCoords[1][0] = this.mouseGeoCoord.x;
        featureCoords[3][1] = this.mouseGeoCoord.y;
      }
      if (this.mouseIsOverVertexNr === 1) {
        featureCoords[1][0] = this.mouseGeoCoord.x;
        featureCoords[1][1] = this.mouseGeoCoord.y;

        featureCoords[0][0] = this.mouseGeoCoord.x;
        featureCoords[4][0] = this.mouseGeoCoord.x;
        featureCoords[2][1] = this.mouseGeoCoord.y;
      }
      if (this.mouseIsOverVertexNr === 2) {
        featureCoords[2][0] = this.mouseGeoCoord.x;
        featureCoords[2][1] = this.mouseGeoCoord.y;

        featureCoords[1][1] = this.mouseGeoCoord.y;
        featureCoords[3][0] = this.mouseGeoCoord.x;
      }
      if (this.mouseIsOverVertexNr === 3) {
        featureCoords[3][0] = this.mouseGeoCoord.x;
        featureCoords[3][1] = this.mouseGeoCoord.y;

        featureCoords[0][1] = this.mouseGeoCoord.y;
        featureCoords[4][1] = this.mouseGeoCoord.y;
        featureCoords[2][0] = this.mouseGeoCoord.x;
      }

      /* Ensure the box square shape */
      /* A: Left edge should be vertical */
      const [
        [featureCoord0X, featureCoord0Y],
        [featureCoord1X],
        [featureCoord2X, featureCoord2Y],
        [, featureCoord3Y],
      ] = featureCoords;
      featureCoords[0][0] = featureCoord1X;

      /* B: Top edge should be horizontal */
      featureCoords[0][1] = featureCoord3Y;

      /* C: Right edge should be vertical */
      featureCoords[3][0] = featureCoord2X;

      /* D: Bottom Edge should be horizontal */
      featureCoords[1][1] = featureCoord2Y;

      /* Close the box: Last point (point 5 of the box) should have the same coordinates as the starting point */
      featureCoords[4][0] = featureCoord0X;
      featureCoords[4][1] = featureCoord0Y;
    }

    if (this.myDrawMode === DRAWMODE.POLYGON) {
      featureCoords[this.mouseIsOverVertexNr][0] = this.mouseGeoCoord.x;
      featureCoords[this.mouseIsOverVertexNr][1] = this.mouseGeoCoord.y;
      /* Transpose begin and end vertice */
      if (this.mouseIsOverVertexNr === 0) {
        featureCoords[featureCoords.length - 1][0] = this.mouseGeoCoord.x;
        featureCoords[featureCoords.length - 1][1] = this.mouseGeoCoord.y;
      }
    }
  }

  moveVertex(feature: GeoJsonFeature, mouseDown: boolean): boolean {
    const featureType = feature.geometry.type;
    let featureCoords = feature.geometry.coordinates[
      this.snappedPolygonIndex
    ] as Position[];
    if (
      featureType === 'Point' ||
      featureType === 'MultiPoint' ||
      featureType === 'LineString'
    ) {
      featureCoords = feature.geometry.coordinates as Position[];
    }

    if (!featureCoords) {
      return;
    }
    const vertexSelected =
      mouseDown === true && this.mouseIsOverVertexNr !== VERTEX.NONE;
    if (vertexSelected || this.myEditMode === EDITMODE.ADD_FEATURE) {
      if (featureType === 'LineString') {
        this.transposeVertex(featureCoords);
        // eslint-disable-next-line consistent-return
        return false;
      }
      /* In case middle point is selected, transpose whole polygon */
      if (
        this.mouseIsOverVertexNr === VERTEX.MIDDLE_POINT_OF_FEATURE &&
        this.snappedGeoCoords
      ) {
        this.transposePolygon(featureCoords);
      } else if (this.mouseIsOverVertexNr !== VERTEX.NONE) {
        /* Transpose polygon vertex */
        this.transposeVertex(featureCoords);
      }
      // eslint-disable-next-line consistent-return
      return false;
    }
  }

  hoverVertex(feature: GeoJsonFeature, mouseX: number, mouseY: number): void {
    let foundVertex = VERTEX.NONE;
    this.mouseIsOverVertexNr = VERTEX.NONE;

    if (feature.geometry.type === 'Point') {
      this.snappedPolygonIndex = SNAPPEDFEATURE.NONE;
      const featureCoords = feature.geometry.coordinates;
      /* Get all vertexes */
      const XYCoords = this.convertGeoCoordsToScreenCoords([featureCoords]);
      if (
        XYCoords.length > 0 &&
        this.checkDist(XYCoords[0], 0, mouseX, mouseY)
      ) {
        this.mouseIsOverVertexNr = 0;
        return;
      }
    }

    if (feature.geometry.type === 'MultiPoint') {
      this.snappedPolygonIndex = SNAPPEDFEATURE.NONE;
      for (
        let polygonIndex = feature.geometry.coordinates.length - 1;
        polygonIndex >= 0;
        polygonIndex -= 1
      ) {
        const featureCoords = feature.geometry.coordinates[polygonIndex];
        if (featureCoords === undefined) {
          // eslint-disable-next-line no-continue
          continue;
        }
        /* Get all vertexes */
        const XYCoords = this.convertGeoCoordsToScreenCoords([featureCoords]);
        if (
          XYCoords.length > 0 &&
          this.checkDist(XYCoords[0], polygonIndex, mouseX, mouseY)
        ) {
          this.mouseIsOverVertexNr = polygonIndex;
          return;
        }
      }
    }

    if (feature.geometry.type === 'Polygon') {
      for (
        let polygonIndex = feature.geometry.coordinates.length - 1;
        polygonIndex >= 0;
        polygonIndex -= 1
      ) {
        const featureCoords = feature.geometry.coordinates[polygonIndex];
        if (featureCoords === undefined) {
          // eslint-disable-next-line no-continue
          continue;
        }
        /* Get all vertexes */
        const XYCoords = this.convertGeoCoordsToScreenCoords(featureCoords);
        const middle = { x: 0, y: 0 };
        /* Snap to the vertex closer than specified pixels */
        for (let j = 0; j < XYCoords.length - 1; j += 1) {
          const coord = XYCoords[j];
          middle.x += coord.x;
          middle.y += coord.y;

          if (this.checkDist(coord, polygonIndex, mouseX, mouseY)) {
            foundVertex = j as unknown as VERTEX;
            break;
          }
        }
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        middle.x = parseInt(middle.x / (XYCoords.length - 1), 10);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        middle.y = parseInt(middle.y / (XYCoords.length - 1), 10);
        /* Check if the mouse hovers the middle vertex */
        if (
          foundVertex === VERTEX.NONE &&
          this.checkDist(middle, polygonIndex, mouseX, mouseY)
        ) {
          foundVertex = VERTEX.MIDDLE_POINT_OF_FEATURE;
        }

        this.mouseIsOverVertexNr = foundVertex;
      }
    }

    if (feature.geometry.type === 'LineString') {
      const lineStringIndex = 0;
      const featureCoords = feature.geometry.coordinates;
      /* Get all vertexes */
      const XYCoords = this.convertGeoCoordsToScreenCoords(featureCoords);
      /* Snap to the vertex closer than specified pixels */
      for (let j = 0; j < XYCoords.length; j += 1) {
        const coord = XYCoords[j];
        // if (!coord || coord.length < 2) continue;
        if (this.checkDist(coord, lineStringIndex, mouseX, mouseY)) {
          foundVertex = j as unknown as VERTEX;
          break;
        }
      }
      this.mouseIsOverVertexNr = foundVertex;
    }
  }

  adagucMouseMove(event: AdagucInputEvent): boolean {
    const { isInEditMode, featureNrToEdit, onHoverFeature } = this.props;
    if (event && event.rightButton === true) return;
    /* adagucMouseMove is an event callback function which is triggered when the mouse moves over the map
      This event is only triggered if the map is in hover state.
      E.g. when the map is dragging/panning, this event is not triggerd
    */
    const { mouseX, mouseY, mouseDown } = event;

    this.mouseX = mouseX;
    this.mouseY = mouseY;

    if (onHoverFeature) {
      const handleMouseStoppedTimer = (): void => {
        const result: CheckHoverFeaturesResult = checkHoverFeatures(
          this.geojson,
          mouseX,
          mouseY,
          (coordinates) => {
            return this.convertGeoCoordsToScreenCoords(coordinates);
          },
        );
        if (result) {
          const featureEvent: FeatureEvent = {
            coordinateIndexInFeature: result.coordinateIndexInFeature,
            featureIndex: result.featureIndex,
            mouseX,
            mouseY,
            isInEditMode,
            feature: result.feature,
          };
          onHoverFeature(featureEvent);
        }
      };
      window.clearTimeout(this.mouseStoppedTimer);
      this.mouseStoppedTimer = window.setTimeout(handleMouseStoppedTimer, 20);
    }

    if (isInEditMode === false) {
      return;
    }

    const feature = this.geojson.features[featureNrToEdit] as GeoJsonFeature;
    if (!feature) return;

    const { webmapjs } = this.props;

    this.mouseGeoCoord = webmapjs.getLatLongFromPixelCoord({
      x: mouseX,
      y: mouseY,
    });

    /* The mouse is hovering a vertice, and the mousedown is into effect, move vertice accordingly */
    const ret = this.moveVertex(feature, mouseDown);
    if (ret === false) {
      webmapjs.draw('AdagucMapDraw::adagucMouseMove');
      // eslint-disable-next-line consistent-return
      return false;
    }

    /* Check if the mouse hovers any vertice of any polygon */
    this.hoverVertex(feature, mouseX, mouseY);
    if (this.mouseIsOverVertexNr !== VERTEX.NONE) {
      /* We found a vertex */
      this.selectedEdge =
        EDGE.NONE; /* We found a vertex, not an edge: reset it */
      webmapjs.setCursor('move');
      webmapjs.draw('AdagucMapDraw::hoverVertex');
      // eslint-disable-next-line consistent-return
      return false;
    }

    webmapjs.setCursor();

    /* Check if the mouse hovers an edge of a polygon */
    this.selectedEdge = EDGE.NONE;
    if (
      this.myEditMode !== EDITMODE.DELETE_FEATURES &&
      !this.checkIfFeatureIsBox(feature)
    ) {
      const retObj = this.hoverEdge(feature.geometry, mouseX, mouseY);

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.selectedEdge = retObj.selectedEdge;
      this.snappedPolygonIndex = retObj.snappedPolygonIndex;
    }

    if (this.selectedEdge !== EDGE.NONE) {
      webmapjs.draw('AdagucMapDraw::adagucMouseMove');
      // eslint-disable-next-line consistent-return
      return false;
    }

    if (this.myEditMode === EDITMODE.ADD_FEATURE) {
      // eslint-disable-next-line consistent-return
      return false;
    }

    /* We did not find anything under the mousecursor,
      return true means that the map will continue with its own mousehandling
    */
    if (
      this.mouseIsOverVertexNr === VERTEX.NONE &&
      this.selectedEdge === EDGE.NONE
    ) {
      webmapjs.draw('AdagucMapDraw::adagucMouseMove');
      // eslint-disable-next-line consistent-return
      return true; /* False means that this component will take over entire controll.
                     True means that it is still possible to pan and drag the map while editing */
    }
  }

  /* Returns true if double click is detected */
  triggerMouseDownTimer(event: AdagucInputEvent): void | boolean {
    if (!this.doubleClickTimer) this.doubleClickTimer = {};
    const { mouseX, mouseY } = event;

    const checkTimeOut = (): void => {
      this.doubleClickTimer.isRunning = false;
    };

    /* Reset the timer */
    if (this.doubleClickTimer.timer) {
      clearTimeout(this.doubleClickTimer.timer);
      this.doubleClickTimer.timer = null;
    }

    /* Check if double click on this location occured */
    if (this.doubleClickTimer.isRunning === true) {
      if (
        mouseX === this.doubleClickTimer.mouseX &&
        mouseY === this.doubleClickTimer.mouseY
      ) {
        this.doubleClickTimer.isRunning = false;
        return true;
      }
    }

    /* Create new timer */
    this.doubleClickTimer.isRunning = true;
    this.doubleClickTimer.mouseX = mouseX;
    this.doubleClickTimer.mouseY = mouseY;
    this.doubleClickTimer.timer = window.setTimeout(checkTimeOut, 300);

    /* No double click detected, return false */
    return false;
  }

  adagucMouseDoubleClick(): boolean {
    this.handleKeyDown({ keyCode: 27 } as KeyboardEvent);
    return true;
  }

  /* Insert a new vertex into an edge, e.g a line is clicked and a point is added */
  insertVertexInEdge(event: AdagucInputEvent): boolean {
    const { mouseX, mouseY } = event;
    const { webmapjs, featureNrToEdit } = this.props;
    if (this.myDrawMode === DRAWMODE.POLYGON) {
      if (
        this.selectedEdge !== EDGE.NONE &&
        this.myEditMode !== EDITMODE.DELETE_FEATURES
      ) {
        this.mouseGeoCoord = webmapjs.getLatLongFromPixelCoord({
          x: mouseX,
          y: mouseY,
        });
        const feature = this.geojson.features[
          featureNrToEdit
        ] as GeoJsonFeature;
        if (this.checkIfFeatureIsBox(feature)) {
          return false;
        }
        const featureCoords = (feature.geometry as GeoJSON.Polygon).coordinates[
          this.snappedPolygonIndex
        ];
        if (featureCoords === undefined) {
          return false;
        }
        featureCoords.splice(this.selectedEdge + 1, 0, [
          this.mouseGeoCoord.x,
          this.mouseGeoCoord.y,
        ]);
        this.featureHasChanged('insert vertex into line');
        this.adagucMouseMove(event);
        return false;
      }
    }
    if (this.myDrawMode === DRAWMODE.LINESTRING) {
      if (
        this.selectedEdge !== EDGE.NONE &&
        this.myEditMode !== EDITMODE.DELETE_FEATURES
      ) {
        this.mouseGeoCoord = webmapjs.getLatLongFromPixelCoord({
          x: mouseX,
          y: mouseY,
        });
        const feature = this.geojson.features[
          featureNrToEdit
        ] as GeoJsonFeature;
        if (this.checkIfFeatureIsBox(feature)) {
          return false;
        }
        const featureCoords = (feature.geometry as GeoJSON.LineString)
          .coordinates;
        if (featureCoords === undefined) {
          return false;
        }
        featureCoords.splice(this.selectedEdge + 1, 0, [
          this.mouseGeoCoord.x,
          this.mouseGeoCoord.y,
        ]);
        this.featureHasChanged('insert vertex into line');
        this.adagucMouseMove(event);
        return false;
      }
    }
    return true;
  }

  /* This is trigged when a new feature is created.  */
  createNewFeature(event: AdagucInputEvent): void {
    const { mouseX, mouseY } = event;
    const { webmapjs, featureNrToEdit } = this.props;
    if (this.myEditMode === EDITMODE.EMPTY) {
      this.myEditMode = EDITMODE.ADD_FEATURE;
      let feature = this.geojson.features[featureNrToEdit];

      // eslint-disable-next-line no-multi-assign
      feature = this.geojson.features[featureNrToEdit] = this.initializeFeature(
        feature as GeoJsonFeature,
        this.myDrawMode,
      ) as GeoJsonFeature;

      this.mouseGeoCoord = webmapjs.getLatLongFromPixelCoord({
        x: mouseX,
        y: mouseY,
      });

      if (
        this.myDrawMode === DRAWMODE.POINT ||
        this.myDrawMode === DRAWMODE.MULTIPOINT
      ) {
        /* Create points */
        const pointGeometry = feature.geometry as GeoJSON.Point;
        if (pointGeometry)
          if (pointGeometry.coordinates === undefined) {
            pointGeometry.coordinates = [];
          }
        const multiPointGeometry = feature.geometry as GeoJSON.MultiPoint;
        if (multiPointGeometry)
          if (multiPointGeometry.coordinates === undefined) {
            multiPointGeometry.coordinates = [[]];
          }
        if (this.myDrawMode === DRAWMODE.POINT) {
          const featureCoords = pointGeometry.coordinates;
          featureCoords[0] = this.mouseGeoCoord.x;
          featureCoords[1] = this.mouseGeoCoord.y;
          this.snappedPolygonIndex = pointGeometry.coordinates.length - 1;
          /* For type POINT it is not possible to add multiple points to the same feature */
          this.myEditMode = EDITMODE.EMPTY;
          this.featureHasChanged('new point created');
        }
        if (this.myDrawMode === DRAWMODE.MULTIPOINT) {
          this.myEditMode = EDITMODE.EMPTY;
          let featureCoords = multiPointGeometry.coordinates;
          if (featureCoords === undefined) {
            featureCoords = [];
          }
          if (featureCoords[0] === undefined || featureCoords[0].length === 0) {
            featureCoords[0] = []; /* Used to create the first polygon */
          } else {
            featureCoords.push(
              [],
            ); /* Used to create extra polygons in the same feature */
          }
          // featureCoords.push([this.mouseGeoCoord.x, this.mouseGeoCoord.y]);
          featureCoords[featureCoords.length - 1] = [
            this.mouseGeoCoord.x,
            this.mouseGeoCoord.y,
          ];
          this.snappedPolygonIndex = multiPointGeometry.coordinates.length - 1;
          this.featureHasChanged('new point in multipoint created');
        }
      }

      if (
        this.myDrawMode === DRAWMODE.POLYGON ||
        this.myDrawMode === DRAWMODE.BOX
      ) {
        /* Create poly's and boxes */
        this.myEditMode = EDITMODE.ADD_FEATURE;

        if (feature.geometry as GeoJSON.Polygon)
          if ((feature.geometry as GeoJSON.Polygon).coordinates === undefined) {
            (feature.geometry as GeoJSON.Polygon).coordinates = [[]];
          }
        if (
          (feature.geometry as GeoJSON.Polygon).coordinates[0] === undefined ||
          (feature.geometry as GeoJSON.Polygon).coordinates[0].length === 0
        ) {
          (feature.geometry as GeoJSON.Polygon).coordinates[0] =
            []; /* Used to create the first polygon */
        } else {
          (feature.geometry as GeoJSON.Polygon).coordinates.push(
            [],
          ); /* Used to create extra polygons in the same feature */
        }
        this.snappedPolygonIndex =
          (feature.geometry as GeoJSON.Polygon).coordinates.length - 1;
        const featureCoords = (feature.geometry as GeoJSON.Polygon).coordinates[
          this.snappedPolygonIndex
        ];
        featureCoords.push([this.mouseGeoCoord.x, this.mouseGeoCoord.y]);
        featureCoords.push([this.mouseGeoCoord.x, this.mouseGeoCoord.y]);

        /* This is triggered when a bounding box is created. Five points are added at once */
        if (this.myDrawMode === DRAWMODE.BOX) {
          featureCoords.push([this.mouseGeoCoord.x, this.mouseGeoCoord.y]);
          featureCoords.push([this.mouseGeoCoord.x, this.mouseGeoCoord.y]);
          featureCoords.push([this.mouseGeoCoord.x, this.mouseGeoCoord.y]);
        }

        if (this.myDrawMode === DRAWMODE.BOX) {
          this.mouseIsOverVertexNr = 3;
        } else {
          this.mouseIsOverVertexNr = featureCoords.length - 1;
        }

        this.featureHasChanged('new feature created');
      }

      if (this.myDrawMode === DRAWMODE.LINESTRING) {
        this.myEditMode = EDITMODE.EMPTY;
        /* Create linestring */
        this.myEditMode = EDITMODE.ADD_FEATURE;
        let featureCoords = (feature.geometry as GeoJSON.LineString)
          .coordinates;
        if (featureCoords === undefined || featureCoords.length === 0) {
          featureCoords = [[this.mouseGeoCoord.x, this.mouseGeoCoord.y]];
        }
        if (featureCoords[0].length === 0) {
          featureCoords[0] = [this.mouseGeoCoord.x, this.mouseGeoCoord.y];
        }
        featureCoords.push([this.mouseGeoCoord.x, this.mouseGeoCoord.y]);
        this.snappedPolygonIndex = 0;
        this.featureHasChanged('new point in LineString created');
        this.mouseIsOverVertexNr = featureCoords.length - 1;
      }
      webmapjs.draw('AdagucMapDraw::adagucMouseDown');

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      return false;
    }
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return true;
  }

  /* This is triggered when new points are added during the addmultipoint mode. One point is added per time */
  addPointToMultiPointFeature(event: AdagucInputEvent): boolean {
    const { mouseX, mouseY } = event;
    const { webmapjs, featureNrToEdit } = this.props;
    if (this.myDrawMode === DRAWMODE.MULTIPOINT) {
      if (
        this.myEditMode === EDITMODE.ADD_FEATURE &&
        this.snappedPolygonIndex !== SNAPPEDFEATURE.NONE
      ) {
        this.mouseGeoCoord = webmapjs.getLatLongFromPixelCoord({
          x: mouseX,
          y: mouseY,
        });
        const feature = this.geojson.features[featureNrToEdit];
        const featureCoords = (feature.geometry as GeoJSON.MultiPoint)
          .coordinates;
        featureCoords.push([this.mouseGeoCoord.x, this.mouseGeoCoord.y]);
        this.featureHasChanged('point added to multipoint');
        this.mouseIsOverVertexNr = VERTEX.NONE;
        this.adagucMouseMove(event);
        return false;
      }
    }
    return true;
  }

  addVerticesToPolygonFeature(event: AdagucInputEvent): boolean {
    const { mouseX, mouseY } = event;
    const { webmapjs, featureNrToEdit } = this.props;
    if (this.myDrawMode === DRAWMODE.POLYGON) {
      if (
        this.myEditMode === EDITMODE.ADD_FEATURE &&
        this.snappedPolygonIndex !== SNAPPEDFEATURE.NONE
      ) {
        this.mouseGeoCoord = webmapjs.getLatLongFromPixelCoord({
          x: mouseX,
          y: mouseY,
        });
        const feature = this.geojson.features[featureNrToEdit];
        const featureCoords = (feature.geometry as GeoJSON.Polygon).coordinates[
          this.snappedPolygonIndex
        ];
        featureCoords.push([this.mouseGeoCoord.x, this.mouseGeoCoord.y]);
        this.featureHasChanged('vertex added to polygon');
        this.mouseIsOverVertexNr = featureCoords.length - 1;
        this.adagucMouseMove(event);
        return false;
      }
    }
  }

  addPointToLineStringFeature(event: AdagucInputEvent): boolean {
    const { mouseX, mouseY } = event;
    const { webmapjs, featureNrToEdit } = this.props;
    if (this.myDrawMode === DRAWMODE.LINESTRING) {
      if (this.myEditMode === EDITMODE.ADD_FEATURE) {
        this.mouseGeoCoord = webmapjs.getLatLongFromPixelCoord({
          x: mouseX,
          y: mouseY,
        });
        const feature = this.geojson.features[featureNrToEdit];
        const featureCoords = (feature.geometry as GeoJSON.LineString)
          .coordinates;
        featureCoords.push([this.mouseGeoCoord.x, this.mouseGeoCoord.y]);
        this.featureHasChanged('vertex added to LineString');
        this.mouseIsOverVertexNr = featureCoords.length - 1;
        this.adagucMouseMove(event);
        return false;
      }
    }
    return true;
  }

  adagucMouseDown(event: AdagucInputEvent): void | boolean {
    const { isInEditMode } = this.props;

    if (event && event.rightButton === true) return;
    if (isInEditMode === false) {
      return;
    }

    if (this.triggerMouseDownTimer(event)) {
      // eslint-disable-next-line consistent-return
      return this.adagucMouseDoubleClick();
    }

    if (
      this.myEditMode === EDITMODE.ADD_FEATURE &&
      this.myDrawMode === DRAWMODE.BOX
    ) {
      // eslint-disable-next-line consistent-return
      return this.adagucMouseDoubleClick();
    }

    this.somethingWasDragged = DRAGMODE.NONE;

    if (
      this.mouseIsOverVertexNr !== VERTEX.NONE &&
      this.myEditMode === EDITMODE.EMPTY
    ) {
      // eslint-disable-next-line consistent-return
      return false;
    }

    /* Insert a new vertex into an edge, e.g a line is clicked and a point is added */
    // eslint-disable-next-line consistent-return
    if (this.insertVertexInEdge(event) === false) return false;

    /* This is trigged when a new feature is created.  */
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (this.createNewFeature(event) === false) {
      /* Start with mouse move from this mouse down location,  enables sizing the box while moving the mouse */
      this.adagucMouseMove(event);
      return false;
    }

    /* This is triggered when new points are added during the addmultipoint mode. One point is added per time */
    if (this.addPointToMultiPointFeature(event) === false) return false;

    /* This is triggered when new points are added during the addpolygon mode. One point is added per time */
    if (this.addVerticesToPolygonFeature(event) === false) return false;

    /* This is triggered when new points are added to a linestring */
    if (this.addPointToLineStringFeature(event) === false) return false;

    return false; /* False means that this component will take over entire controll.
                     True means that it is still possible to pan and drag the map while editing */
  }

  deletePolygon(index: number | SNAPPEDFEATURE): void {
    const { featureNrToEdit, deletePolygonCallback } = this.props;
    const feature = this.geojson.features[featureNrToEdit];
    (feature.geometry as GeoJSON.Polygon).coordinates.splice(
      index as number,
      1,
    );
    if (deletePolygonCallback) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      deletePolygonCallback();
    }
  }

  deleteFeature(): void {
    /* Deletes any features under the mousecursor */
    const { webmapjs, featureNrToEdit } = this.props;
    if (this.mouseIsOverVertexNr === VERTEX.NONE) {
      return;
    }
    const feature = this.geojson.features[featureNrToEdit] as GeoJsonFeature;
    const featureCoords = (feature.geometry as GeoJSON.Polygon).coordinates[
      this.snappedPolygonIndex
    ];
    if (featureCoords === undefined) {
      return;
    }
    if (this.mouseIsOverVertexNr !== VERTEX.MIDDLE_POINT_OF_FEATURE) {
      /* Remove edge of polygon */
      if (featureCoords.length <= 3) {
        /* Remove the polygon completely if it can not have an area */
        this.deletePolygon(this.snappedPolygonIndex);
      } else {
        /* Remove edge of polygon */
        // eslint-disable-next-line no-lonely-if
        if (!this.checkIfFeatureIsBox(feature)) {
          featureCoords.splice(this.mouseIsOverVertexNr, 1);
        }
      }
    } else {
      /* Remove the polygon completely */
      this.deletePolygon(this.snappedPolygonIndex);
    }
    this.featureHasChanged('deleteFeature');
    this.selectedEdge = EDGE.NONE;
    this.mouseIsOverVertexNr = VERTEX.NONE;
    webmapjs.draw('AdagucMapDraw::deletefeatures');
  }

  adagucMouseUp(event: AdagucInputEvent): void | boolean {
    const { isInEditMode, onClickFeature } = this.props;
    if (event && event.rightButton === true) return;
    if (onClickFeature) {
      const { mouseX, mouseY } = event;
      const ignoreCoordinateIndexInFeature = true;
      const result: CheckHoverFeaturesResult = checkHoverFeatures(
        this.geojson,
        mouseX,
        mouseY,
        (coordinates) => {
          return this.convertGeoCoordsToScreenCoords(coordinates);
        },
        ignoreCoordinateIndexInFeature,
      );
      if (result) {
        const featureEvent: FeatureEvent = {
          coordinateIndexInFeature: result.coordinateIndexInFeature,
          featureIndex: result.featureIndex,
          mouseX,
          mouseY,
          isInEditMode,
          feature: result.feature,
        };
        onClickFeature(featureEvent);
      } else {
        onClickFeature(null);
      }
    }
    if (isInEditMode === false) {
      return;
    }

    /* Keep the mouse down to drag a box */
    if (
      this.myDrawMode === DRAWMODE.BOX &&
      this.myEditMode === EDITMODE.ADD_FEATURE
    ) {
      this.cancelEdit(false);
      return false;
    }

    if (this.somethingWasDragged !== DRAGMODE.NONE) {
      this.featureHasChanged(
        `A ${this.somethingWasDragged.toString()} was dragged`,
      );
    }

    /* Delete a vertex or feature on mouseUp */
    if (this.myEditMode === EDITMODE.DELETE_FEATURES) {
      this.deleteFeature();
      return false;
    }

    if (
      this.mouseIsOverVertexNr !== VERTEX.NONE ||
      this.selectedEdge !== EDGE.NONE
    ) {
      return false;
    }

    if (this.myEditMode === EDITMODE.ADD_FEATURE) {
      return false;
    }
  }

  cancelEdit(cancelLastPoint: boolean): void {
    const { isInEditMode, featureNrToEdit } = this.props;
    if (isInEditMode === false) {
      return;
    }
    const { webmapjs } = this.props;

    /* When in addpolygon mode, finish the polygon */
    if (this.myEditMode === EDITMODE.ADD_FEATURE) {
      this.myEditMode = EDITMODE.EMPTY;
      if (
        this.drawMode === DRAWMODE.POLYGON ||
        this.drawMode === DRAWMODE.BOX
      ) {
        if (this.snappedPolygonIndex === SNAPPEDFEATURE.NONE) {
          return;
        }

        const feature = this.geojson.features[
          featureNrToEdit
        ] as GeoJsonFeature;
        const { coordinates } = feature.geometry as GeoJSON.Polygon;
        const polygon = coordinates[this.snappedPolygonIndex];

        if (!polygon) {
          coordinates[this.snappedPolygonIndex] = [];
          return;
        }

        if (polygon.length === 0) {
          return;
        }

        if (!this.checkIfFeatureIsBox(feature)) {
          if (cancelLastPoint === true) {
            polygon.pop();
          }
          if (polygon.length < 3) {
            coordinates.pop();
          }
        }
      }
      if (this.myDrawMode === DRAWMODE.LINESTRING) {
        const feature = this.geojson.features[featureNrToEdit];
        const featureCoords = (feature.geometry as GeoJSON.LineString)
          .coordinates;
        featureCoords.pop();
      }
      this.featureHasChanged('cancelEdit');
      webmapjs.draw('AdagucMapDraw::cancelEdit');
    } else {
      /* When in standard mode or deletefeatures mode, remove any vertex under the mousecursor */
      // eslint-disable-next-line no-lonely-if
      if (
        this.myEditMode === EDITMODE.EMPTY ||
        this.myEditMode === EDITMODE.DELETE_FEATURES
      ) {
        this.deleteFeature();
      }
    }
  }

  handleKeyDown(event: KeyboardEvent): void {
    const ESCAPE_KEY = 27;
    const { exitDrawModeCallback } = this.props;
    if (event.keyCode === ESCAPE_KEY) {
      if (
        exitDrawModeCallback &&
        (this.myEditMode === EDITMODE.EMPTY ||
          this.myEditMode === EDITMODE.DELETE_FEATURES)
      ) {
        exitDrawModeCallback();
      }
      this.cancelEdit(this.myDrawMode !== DRAWMODE.BOX);
      if (this.myDrawMode === DRAWMODE.POLYGON && exitDrawModeCallback) {
        exitDrawModeCallback();
      }
      if (this.myDrawMode === DRAWMODE.LINESTRING && exitDrawModeCallback) {
        exitDrawModeCallback();
      }
    }
  }

  // eslint-disable-next-line class-methods-use-this
  validateFeature(feature: GeoJsonFeature): void {
    if (!feature.properties) feature.properties = {};
    if (!feature.geometry) feature.geometry = {} as GeoJsonFeatureType;
    if (!feature.type) feature.type = 'Feature';
    if (!feature.geometry.coordinates) feature.geometry.coordinates = [];
  }

  validatePolys(fixPolys: boolean): void {
    if (
      !this.geojson ||
      !this.geojson.features ||
      !this.geojson.features.length
    )
      return;
    for (
      let featureIndex = 0;
      featureIndex < this.geojson.features.length;
      featureIndex += 1
    ) {
      if (!this.geojson.features) this.geojson.features = [];
      const feature = this.geojson.features[featureIndex] as GeoJsonFeature;
      this.validateFeature(feature);
      const featureType = feature.geometry.type;
      if (featureType === 'Polygon') {
        /* Loop through all polygons of the same feature */
        const polygonGeometry = feature.geometry as GeoJSON.Polygon;
        for (
          let polygonIndex = 0;
          polygonIndex < polygonGeometry.coordinates.length;
          polygonIndex += 1
        ) {
          let featureCoords = polygonGeometry.coordinates[polygonIndex];

          /* Remove duplicates */
          if (!this.checkIfFeatureIsBox(feature)) {
            for (
              let coordIndex = featureCoords.length - 2;
              coordIndex >= 0;
              coordIndex -= 1
            ) {
              const startCoord = featureCoords[coordIndex];
              const endCoord = featureCoords[coordIndex + 1];
              if (startCoord && endCoord) {
                if (
                  startCoord[0] === endCoord[0] &&
                  startCoord[1] === endCoord[1]
                ) {
                  featureCoords.splice(coordIndex, 1);
                }
              }
            }
            /* Add begin to end to make closed polygon */
            const startCoord = featureCoords[0];
            const endCoord = featureCoords[featureCoords.length - 1];
            if (startCoord && endCoord) {
              if (
                startCoord[0] !== endCoord[0] ||
                startCoord[1] !== endCoord[1]
              ) {
                featureCoords.push(cloneDeep(startCoord));
                if (
                  this.mouseIsOverVertexNr !== VERTEX.NONE &&
                  this.mouseIsOverVertexNr !== VERTEX.MIDDLE_POINT_OF_FEATURE
                ) {
                  /* This means that our selected vertex is one lower */
                  (this.mouseIsOverVertexNr as number) -= 1;
                }
              }
            }
          }

          /* Sort clockwise */
          if (fixPolys) {
            const checkClockwiseOrder = (featureCoordinates): number => {
              let sum = 0;
              for (let j = 0; j < featureCoordinates.length - 1; j += 1) {
                const currentPoint = featureCoordinates[j];
                const nextPoint =
                  featureCoordinates[(j + 1) % featureCoordinates.length];
                sum +=
                  (nextPoint[0] - currentPoint[0]) *
                  (nextPoint[1] + currentPoint[1]);
              }
              return sum;
            };
            const sum = checkClockwiseOrder(featureCoords);
            if (sum < 0) {
              featureCoords = featureCoords.reverse();
              /* The lastly selected vertex is now aways the second in the array */
              if (
                this.mouseIsOverVertexNr !== VERTEX.NONE &&
                this.mouseIsOverVertexNr !== VERTEX.MIDDLE_POINT_OF_FEATURE
              ) {
                this.mouseIsOverVertexNr = 1;
              }
            }
          }
        }
      }
    }
  }

  adagucBeforeDraw(ctx: CanvasRenderingContext2D): void {
    /* adagucBeforeDraw is an event callback function which is triggered
     just before adagucviewer will flip the back canvas buffer to the front.
     You are free to draw anything you like on the canvas.
    */

    const { featureNrToEdit, isInEditMode } = this.props;

    if (
      !this.geojson ||
      !this.geojson.features ||
      !this.geojson.features.length
    )
      return;
    this.textPositions = [];
    this.mouseOverPolygonCoordinates = [];
    this.mouseOverPolygonFeatureIndex = -1;
    this.mouseOverPolygonIndex = -1;
    /* Current selected feature from GeoJSON */
    for (
      let featureIndex = 0;
      featureIndex < this.geojson.features.length;
      featureIndex += 1
    ) {
      const feature = this.geojson.features[featureIndex] as GeoJsonFeature;
      const featureType = feature.geometry.type as string;
      const totalmiddle = { x: 0, y: 0, nr: 0 };

      if (featureType === 'Point') {
        const featureCoords = (feature.geometry as GeoJSON.Point).coordinates;

        const XYCoords = this.getPixelCoordFromGeoCoord([featureCoords]);
        if (XYCoords.length === 0) {
          // eslint-disable-next-line no-continue
          continue;
        }
        for (let j = 0; j < XYCoords.length; j += 1) {
          this.drawPoint(
            ctx,
            XYCoords[j],
            this.mouseIsOverVertexNr === j && featureNrToEdit === featureIndex,
            false,
            isInEditMode && featureNrToEdit === featureIndex,
            feature,
            featureIndex,
          );
        }
      }

      if (featureType === 'MultiPoint') {
        const featureCoords = (feature.geometry as GeoJSON.MultiPoint)
          .coordinates;
        const XYCoords = this.getPixelCoordFromGeoCoord(featureCoords);
        if (XYCoords.length === 0) {
          // eslint-disable-next-line no-continue
          continue;
        }
        for (let j = 0; j < XYCoords.length; j += 1) {
          this.drawPoint(
            ctx,
            XYCoords[j],
            this.mouseIsOverVertexNr === j && featureNrToEdit === featureIndex,
            false,
            isInEditMode && featureNrToEdit === featureIndex,
            feature,
            featureIndex,
          );
        }
      }

      if (featureType === 'MultiPolygon') {
        const multiPolygonGeometry = feature.geometry as GeoJSON.Polygon;
        for (
          let multiPolygonIndex = 0;
          multiPolygonIndex < multiPolygonGeometry.coordinates.length;
          multiPolygonIndex += 1
        ) {
          const multiPoly = multiPolygonGeometry.coordinates[multiPolygonIndex];
          for (
            let polygonIndex = 0;
            polygonIndex < multiPoly.length;
            polygonIndex += 1
          ) {
            const featureCoords = multiPoly[
              polygonIndex
            ] as unknown as Position[];
            const XYCoords = this.getPixelCoordFromGeoCoord(featureCoords);
            /* Only draw if there is stuff to show */
            if (XYCoords.length === 0) {
              // eslint-disable-next-line no-continue
              continue;
            }

            const middle = this.drawPolygon(
              ctx,
              XYCoords,
              featureIndex,
              polygonIndex,
            );
            if (middle) {
              totalmiddle.x += middle.x * middle.nr;
              totalmiddle.y += middle.y * middle.nr;
              totalmiddle.nr += middle.nr;
            }
            if (isInEditMode) {
              /* Draw all vertices on the edges of the polygons */
              for (let j = 0; j < XYCoords.length; j += 1) {
                this.drawVertice(
                  ctx,
                  XYCoords[j],
                  this.snappedPolygonIndex === polygonIndex &&
                    this.mouseIsOverVertexNr === j &&
                    featureNrToEdit === featureIndex,
                  false,
                  isInEditMode && featureNrToEdit === featureIndex,
                );
              }

              if (middle && isInEditMode === true && XYCoords.length >= 3) {
                /* Draw middle vertice for the poly if poly covers an area, e.g. when it contains more than three points */
                this.drawVertice(
                  ctx,
                  middle,
                  this.snappedPolygonIndex === polygonIndex &&
                    this.mouseIsOverVertexNr ===
                      VERTEX.MIDDLE_POINT_OF_FEATURE &&
                    featureNrToEdit === featureIndex,
                  true,
                  isInEditMode && featureNrToEdit === featureIndex,
                );
              }
            }
          }
        }
        if (totalmiddle.nr > 0) {
          const mx = totalmiddle.x / totalmiddle.nr;
          const my = totalmiddle.y / totalmiddle.nr;
          if (feature.properties && feature.properties.text) {
            this.textPositions.push({
              x: mx,
              y: my,
              text: feature.properties.text,
            });
          }
        }
      }

      if (featureType === 'Polygon') {
        const polygonGeometry = feature.geometry as GeoJSON.Polygon;
        /* Loop through all polygons of the same feature */
        for (
          let polygonIndex = 0;
          polygonIndex < polygonGeometry.coordinates.length;
          polygonIndex += 1
        ) {
          const featureCoords = polygonGeometry.coordinates[polygonIndex];

          const XYCoords = this.getPixelCoordFromGeoCoord(featureCoords);
          /* Only draw if there is stuff to show */
          if (XYCoords.length === 0) {
            // eslint-disable-next-line no-continue
            continue;
          }

          const middle = this.drawPolygon(
            ctx,
            XYCoords,
            featureIndex,
            polygonIndex,
          );

          if (middle && feature.properties && feature.properties.text) {
            this.textPositions.push({
              x: middle.x,
              y: middle.y,
              text: feature.properties.text,
            });
          }

          if (isInEditMode) {
            /* Draw all vertices on the edges of the polygons */
            for (let j = 0; j < XYCoords.length; j += 1) {
              this.drawVertice(
                ctx,
                XYCoords[j],
                this.snappedPolygonIndex === polygonIndex &&
                  this.mouseIsOverVertexNr === j &&
                  featureNrToEdit === featureIndex,
                false,
                isInEditMode && featureNrToEdit === featureIndex,
              );
            }

            if (middle && isInEditMode === true && XYCoords.length >= 3) {
              /* Draw middle vertice for the poly if poly covers an area, e.g. when it contains more than three points */
              this.drawVertice(
                ctx,
                middle,
                this.snappedPolygonIndex === polygonIndex &&
                  this.mouseIsOverVertexNr === VERTEX.MIDDLE_POINT_OF_FEATURE &&
                  featureNrToEdit === featureIndex,
                true,
                isInEditMode && featureNrToEdit === featureIndex,
              );
            }
          }
        }
      }

      if (featureType === 'LineString') {
        /* Loop through all line pints of the same feature */
        const featureCoords = (feature.geometry as GeoJSON.LineString)
          .coordinates;
        if (!featureCoords || !featureCoords.length) {
          // eslint-disable-next-line no-continue
          continue;
        }
        const XYCoords = this.getPixelCoordFromGeoCoord(featureCoords);
        /* Only draw if there is stuff to show */
        if (XYCoords.length === 0) {
          // eslint-disable-next-line no-continue
          continue;
        }

        const middle = this.drawLine(ctx, XYCoords, featureIndex, 0);

        if (middle && feature.properties && feature.properties.text) {
          this.textPositions.push({
            x: middle.x,
            y: middle.y,
            text: feature.properties.text,
          });
        }

        if (isInEditMode) {
          /* Draw all vertices on the edges of the polygons */
          for (let j = 0; j < XYCoords.length; j += 1) {
            this.drawVertice(
              ctx,
              XYCoords[j],
              this.mouseIsOverVertexNr === j &&
                featureNrToEdit === featureIndex,
              false,
              isInEditMode && featureNrToEdit === featureIndex,
            );
          }
        }
      }
    }

    /* Higlight polygon with mousehover */
    if (
      (isInEditMode === true &&
        this.mouseOverPolygonFeatureIndex === featureNrToEdit &&
        this.mouseIsOverVertexNr === VERTEX.NONE &&
        this.snappedPolygonIndex === SNAPPEDFEATURE.NONE &&
        this.selectedEdge === EDGE.NONE) ||
      this.mouseIsOverVertexNr === VERTEX.MIDDLE_POINT_OF_FEATURE
    ) {
      if (this.mouseOverPolygonCoordinates.length >= 2) {
        ctx.beginPath();

        ctx.moveTo(
          this.mouseOverPolygonCoordinates[0].x,
          this.mouseOverPolygonCoordinates[0].y,
        );
        for (let j = 1; j < this.mouseOverPolygonCoordinates.length; j += 1) {
          const coord = this.mouseOverPolygonCoordinates[j];
          ctx.lineTo(coord.x, coord.y);
        }
        ctx.closePath();
        ctx.strokeStyle = 'blue';
        ctx.lineWidth = 1;
        ctx.fillStyle = '#88F';
        ctx.globalAlpha = 0.3;
        ctx.fill();
        ctx.globalAlpha = 1;
        ctx.stroke();
      }
    }

    /* Draw labels */
    for (let j = 0; j < this.textPositions.length; j += 1) {
      const { x, y, text } = this.textPositions[j];
      ctx.fillStyle = 'black';
      ctx.font = '12px Arial';
      ctx.fillText(text, x, y);
    }
  }

  /* Function which calculates the distance between two points */
  // eslint-disable-next-line class-methods-use-this
  distance(a: Coordinate, b: Coordinate): number {
    return Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
  }

  /* Function which calculates if a point is between two other points */
  isBetween(a: Coordinate, c: Coordinate, b: Coordinate): boolean {
    const da = this.distance(a, c) + this.distance(c, b);
    const db = this.distance(a, b);
    return Math.abs(da - db) < 1;
  }

  /* Checks if mouse is in proximity of given coordinate */
  checkDist(
    coord: Coordinate,
    polygonIndex: SNAPPEDFEATURE | number,
    mouseX: number,
    mouseY: number,
  ): boolean {
    const VERTEX = this.distance(coord, { x: mouseX, y: mouseY });
    if (VERTEX < 10) {
      this.snappedGeoCoords = { ...this.mouseGeoCoord };
      this.snappedPolygonIndex = polygonIndex;
      return true;
    }
    return false;
  }

  hoverEdge(
    geometry: GeoJsonFeatureType,
    mouseX: number,
    mouseY: number,
  ): {
    selectedEdge: EDGE | number;
    snappedPolygonIndex: SNAPPEDFEATURE | number;
  } {
    const { coordinates, type } = geometry;
    if (type === 'Polygon') {
      for (
        let polygonIndex = coordinates.length - 1;
        polygonIndex >= 0;
        polygonIndex -= 1
      ) {
        const featureCoords = coordinates[polygonIndex];
        if (featureCoords === undefined) {
          // eslint-disable-next-line no-continue
          continue;
        }
        /* Get all vertexes */
        const XYCoords = this.convertGeoCoordsToScreenCoords(
          featureCoords as Position[],
        );
        for (let j = 0; j < XYCoords.length; j += 1) {
          const startV = XYCoords[j];
          const stopV = XYCoords[(j + 1) % XYCoords.length];
          if (this.isBetween(startV, { x: mouseX, y: mouseY }, stopV)) {
            return { selectedEdge: j, snappedPolygonIndex: polygonIndex };
          }
        }
      }
      return {
        selectedEdge: EDGE.NONE,
        snappedPolygonIndex: SNAPPEDFEATURE.NONE,
      };
    }
    if (type === 'LineString') {
      const featureCoords = coordinates;
      if (featureCoords === undefined) {
        return {
          selectedEdge: EDGE.NONE,
          snappedPolygonIndex: SNAPPEDFEATURE.NONE,
        };
      }
      /* Get all vertexes */
      const XYCoords = this.convertGeoCoordsToScreenCoords(
        featureCoords as Position[],
      );
      for (let j = 0; j < XYCoords.length - 1; j += 1) {
        const startV = XYCoords[j];
        const stopV = XYCoords[(j + 1) % XYCoords.length];
        if (this.isBetween(startV, { x: mouseX, y: mouseY }, stopV)) {
          return { selectedEdge: j, snappedPolygonIndex: 0 };
        }
      }
      return {
        selectedEdge: EDGE.NONE,
        snappedPolygonIndex: SNAPPEDFEATURE.NONE,
      };
    }
    return {
      selectedEdge: EDGE.NONE,
      snappedPolygonIndex: SNAPPEDFEATURE.NONE,
    };
  }

  // eslint-disable-next-line class-methods-use-this
  drawVertice(
    ctx: CanvasRenderingContext2D,
    _coord: Coordinate,
    selected: boolean,
    middle: boolean,
    isInEditMode: boolean,
  ): void {
    let w = 7;
    const coord = {
      x: parseInt(_coord.x as unknown as string, 10),
      y: parseInt(_coord.y as unknown as string, 10),
    };
    if (isInEditMode === false) {
      return;
    }
    if (selected === false) {
      if (middle === true) {
        /* Style for middle editable vertice */
        ctx.strokeStyle = '#000';
        ctx.fillStyle = '#D87502';
        ctx.lineWidth = 1.0;
      } else {
        /* Style for standard editable vertice */
        ctx.strokeStyle = '#000';
        ctx.fillStyle = '#0275D8';
        ctx.lineWidth = 1.0;
      }
    } else {
      /* Style for selected editable vertice */
      ctx.strokeStyle = '#000';
      ctx.fillStyle = '#FF0';
      ctx.lineWidth = 1.0;
      w = 11;
    }
    ctx.globalAlpha = 1.0;
    ctx.fillRect(coord.x - w / 2, coord.y - w / 2, w, w);
    ctx.strokeRect(coord.x - w / 2, coord.y - w / 2, w, w);
    if (isInEditMode) {
      ctx.strokeRect(coord.x - 0.5, coord.y - 0.5, 1, 1);
    }
  }

  drawPoint(
    ctx: CanvasRenderingContext2D,
    _coord: Coordinate,
    selected: boolean,
    middle: boolean,
    isInEditMode: boolean,
    feature: GeoJsonFeature,
    featureIndex: number,
  ): void {
    const { properties: featureProperties } = feature;
    if (featureProperties && featureProperties.imageURL) {
      this.drawIcon(ctx, _coord, featureProperties);
    }

    const drawStyledMarker = featureProperties
      ? featureProperties.stroke ||
        featureProperties['stroke-width'] ||
        featureProperties.fill
      : null;
    const drawMarkerByDefault =
      !featureProperties ||
      !featureProperties.imageURL ||
      !featureProperties.drawFunction;

    if (featureProperties.drawFunction) {
      featureProperties.drawFunction({
        context: ctx,
        featureIndex,
        coord: _coord,
        selected,
        middle,
        isInEditMode,
        feature,
        mouseX: this.mouseX,
        mouseY: this.mouseY,
      });
    } else if (drawStyledMarker || drawMarkerByDefault) {
      this.drawMarker(
        ctx,
        _coord,
        selected,
        middle,
        isInEditMode,
        featureProperties,
      );
    }
  }

  drawIcon(
    ctx: CanvasRenderingContext2D,
    _coord: Coordinate,
    featureProperties: GeoJsonProperties,
  ): DrawStyle {
    const image = getMapImageStore.getImage(featureProperties.imageURL);

    if (
      image.isLoaded() === false &&
      image.hasError() === false &&
      image.isLoading() === false
    ) {
      image.load();

      /* After the image is loaded the canvas will be redrawn and drawIcon will be triggered again
      via the beforecanvasdisplay listener. */
      return;
    }

    if (image.isLoaded()) {
      const imageWidth =
        (featureProperties && featureProperties.imageWidth) ||
        this.defaultIconProps.imageWidth;
      const imageHeight =
        (featureProperties && featureProperties.imageHeight) ||
        this.defaultIconProps.imageHeight;

      ctx.drawImage(
        image.getElement(),
        parseInt(_coord.x as unknown as string, 10) - imageWidth / 2,
        parseInt(_coord.y as unknown as string, 10) - imageHeight / 2,
        imageWidth,
        imageHeight,
      );
      if (
        featureProperties.imageBorderWidth &&
        featureProperties.imageBorderWidth > 0
      ) {
        ctx.lineWidth = featureProperties.imageBorderWidth;
        ctx.strokeRect(
          parseInt(_coord.x as unknown as string, 10) - imageWidth / 2,
          parseInt(_coord.y as unknown as string, 10) - imageHeight / 2,
          imageWidth,
          imageHeight,
        );
      }
    }
  }

  drawMarker(
    ctx: CanvasRenderingContext2D,
    _coord: Coordinate,
    selected: boolean,
    middle: boolean,
    isInEditMode: boolean,
    featureProperties: GeoFeatureStyle,
  ): void | DrawStyle {
    const strokeStyle =
      (featureProperties && featureProperties.stroke) ||
      this.defaultPolyProps.stroke;
    const lineWidth =
      (featureProperties && featureProperties['stroke-width']) ||
      this.defaultPolyProps['stroke-width'];
    const fillStyle =
      (featureProperties && featureProperties.fill) ||
      this.defaultPolyProps.fill;

    if (isInEditMode === false) {
      /* Standard style, no editing, just display location of vertices */
      ctx.strokeStyle = strokeStyle;
      ctx.fillStyle = fillStyle;
      ctx.lineWidth = lineWidth;
    } else if (selected === false) {
      /* Style for standard editable vertice */
      ctx.strokeStyle = '#000';
      ctx.fillStyle = '#0275D8';
      ctx.lineWidth = 1.0;
    } else {
      /* Style for selected editable vertice */
      ctx.strokeStyle = '#000';
      ctx.fillStyle = '#FF0';
      ctx.lineWidth = 1.0;
    }
    const coord = {
      x: parseInt(_coord.x as unknown as string, 10),
      y: parseInt(_coord.y as unknown as string, 10),
    };
    ctx.globalAlpha = 1.0;
    ctx.beginPath();
    const topRadius = 9;
    const topHeight = 2.5 * topRadius;
    ctx.arc(coord.x, coord.y - topHeight, topRadius, Math.PI, Math.PI * 2);
    ctx.bezierCurveTo(
      coord.x + topRadius,
      coord.y - topHeight,
      coord.x + topRadius / 1.6,
      coord.y - topRadius,
      coord.x,
      coord.y,
    );
    ctx.bezierCurveTo(
      coord.x,
      coord.y,
      coord.x - topRadius / 1.6,
      coord.y - topRadius,
      coord.x - topRadius,
      coord.y - topHeight,
    );
    ctx.stroke();
    ctx.fill();
    /* Fill center circle */
    ctx.fillStyle = '#FFF';
    ctx.beginPath();
    ctx.arc(coord.x, coord.y - topHeight, topRadius / 2, Math.PI * 2, 0);
    ctx.fill();

    /* Fill marker exact location */
    ctx.fillStyle = '#000';
    ctx.beginPath();
    ctx.arc(coord.x, coord.y, 2, Math.PI * 2, 0);
    ctx.fill();
  }

  drawLine(
    ctx: CanvasRenderingContext2D,
    XYCoords: Coordinate[],
    featureIndex: number,
    lineStringIndex: number,
  ): void | DrawStyle {
    const { isInEditMode, featureNrToEdit } = this.props;

    const feature = this.geojson.features[featureIndex];
    if (!feature || !feature.geometry) return;
    if (feature.geometry.type !== 'LineString') return;
    let lineProps = feature.properties;
    if (!lineProps) lineProps = this.defaultLineStringProps;

    /* Draw polygons and calculate center of poly */
    const middle = { x: 0, y: 0, nr: 0 };
    ctx.beginPath();
    ctx.strokeStyle = lineProps.stroke || this.defaultLineStringProps.stroke;
    ctx.lineWidth =
      lineProps['stroke-width'] || this.defaultLineStringProps['stroke-width'];
    ctx.fillStyle = lineProps.fill || this.defaultLineStringProps.fill;
    const startCoord = XYCoords[0];
    ctx.moveTo(startCoord.x, startCoord.y);
    middle.x += startCoord.x;
    middle.y += startCoord.y;

    for (let j = 1; j < XYCoords.length; j += 1) {
      const coord = XYCoords[j];
      ctx.lineTo(coord.x, coord.y);
      if (j < XYCoords.length - 1) {
        middle.x += coord.x;
        middle.y += coord.y;
      }
    }

    ctx.globalAlpha =
      lineProps['fill-opacity'] || this.defaultLineStringProps['fill-opacity'];
    if (lineProps['fill-opacity'] === 0) {
      ctx.globalAlpha = 0;
    }
    // ctx.fill();
    ctx.globalAlpha =
      lineProps['stroke-opacity'] ||
      this.defaultLineStringProps['stroke-opacity'];
    ctx.stroke();
    // let test = ctx.isPointInPath(this.mouseX, this.mouseY);
    // if (test) {
    this.mouseOverPolygonCoordinates = 0;
    this.mouseOverPolygonFeatureIndex = 0;
    this.mouseOverPolygonIndex = 0;
    // }

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    middle.x = parseInt(middle.x / (XYCoords.length - 1), 10);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    middle.y = parseInt(middle.y / (XYCoords.length - 1), 10);

    if (
      isInEditMode === true &&
      this.snappedPolygonIndex === lineStringIndex &&
      this.selectedEdge !== EDGE.NONE &&
      featureNrToEdit === featureIndex
    ) {
      /* Higlight selected edge of a LineString, previousely detected by mouseover event */
      ctx.beginPath();
      ctx.strokeStyle = '#FF0';
      ctx.lineWidth = 5;
      ctx.moveTo(XYCoords[this.selectedEdge].x, XYCoords[this.selectedEdge].y);
      ctx.lineTo(
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        XYCoords[(this.selectedEdge + 1) % XYCoords.length].x,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        XYCoords[(this.selectedEdge + 1) % XYCoords.length].y,
      );
      ctx.stroke();
    }

    middle.nr = XYCoords.length - 1;
    return middle;
  }

  drawPolygon(
    ctx: CanvasRenderingContext2D,
    XYCoords: Coordinate[],
    featureIndex: number,
    polygonIndex: number,
  ): void | DrawStyle {
    const { isInEditMode, featureNrToEdit } = this.props;

    const feature = this.geojson.features[featureIndex];
    if (!feature || !feature.geometry) return;
    if (
      feature.geometry.type !== 'Polygon' &&
      feature.geometry.type !== 'MultiPolygon'
    )
      return;
    let polyProps = feature.properties;
    if (!polyProps) polyProps = this.defaultPolyProps;

    /* Draw polygons and calculate center of poly */
    const middle = { x: 0, y: 0, nr: 0 };

    ctx.beginPath();
    ctx.strokeStyle = polyProps.stroke || this.defaultPolyProps.stroke;
    ctx.lineWidth =
      polyProps['stroke-width'] || this.defaultPolyProps['stroke-width'];
    ctx.fillStyle = polyProps.fill || this.defaultPolyProps.fill;
    const startCoord = XYCoords[0];
    ctx.moveTo(startCoord.x, startCoord.y);
    middle.x += startCoord.x;
    middle.y += startCoord.y;

    for (let j = 1; j < XYCoords.length; j += 1) {
      const coord = XYCoords[j];
      ctx.lineTo(coord.x, coord.y);
      if (j < XYCoords.length - 1) {
        middle.x += coord.x;
        middle.y += coord.y;
      }
    }
    ctx.closePath();

    ctx.globalAlpha =
      polyProps['fill-opacity'] || this.defaultPolyProps['fill-opacity'];
    if (polyProps['fill-opacity'] === 0) {
      ctx.globalAlpha = 0;
    }
    ctx.fill();
    ctx.globalAlpha =
      polyProps['stroke-opacity'] || this.defaultPolyProps['stroke-opacity'];
    ctx.stroke();

    const test = ctx.isPointInPath(this.mouseX, this.mouseY);
    if (test) {
      this.mouseOverPolygonCoordinates = XYCoords;
      this.mouseOverPolygonFeatureIndex = featureIndex;
      this.mouseOverPolygonIndex = polygonIndex;
    }
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    middle.x = parseInt(middle.x / (XYCoords.length - 1), 10);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    middle.y = parseInt(middle.y / (XYCoords.length - 1), 10);

    if (
      isInEditMode === true &&
      this.snappedPolygonIndex === polygonIndex &&
      this.selectedEdge !== EDGE.NONE &&
      featureNrToEdit === featureIndex
    ) {
      /* Higlight selected edge of a polygon, previousely detected by mouseover event */
      ctx.strokeStyle = '#FF0';
      ctx.lineWidth = 5;
      ctx.beginPath();
      ctx.moveTo(XYCoords[this.selectedEdge].x, XYCoords[this.selectedEdge].y);
      ctx.lineTo(
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        XYCoords[(this.selectedEdge + 1) % XYCoords.length].x,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        XYCoords[(this.selectedEdge + 1) % XYCoords.length].y,
      );
      ctx.stroke();
    }

    middle.nr = XYCoords.length - 1;

    return middle;
  }

  convertGeoCoordsToScreenCoords(featureCoords: Position[]): Coordinate[] {
    const { webmapjs } = this.props;
    const XYCoords = [];
    for (let j = 0; j < featureCoords.length; j += 1) {
      // eslint-disable-next-line no-continue
      if (featureCoords[j].length < 2) continue;
      const coord = webmapjs.getPixelCoordFromLatLong({
        x: featureCoords[j][0],
        y: featureCoords[j][1],
      });
      XYCoords.push(coord);
    }
    return XYCoords;
  }

  initializeFeature(feature: GeoJsonFeature, type: string): GeoJSON.Feature {
    if (!feature) {
      switch (type) {
        case DRAWMODE.POINT:
          return cloneDeep(featurePoint);
        case DRAWMODE.MULTIPOINT:
          return cloneDeep(featureMultiPoint);
        case DRAWMODE.POLYGON:
          return cloneDeep(featurePolygon);
        case DRAWMODE.BOX:
          return cloneDeep(featureBox);
        case DRAWMODE.LINESTRING:
          return cloneDeep(lineString);
        default:
      }
    } else {
      this.validateFeature(feature);
      // eslint-disable-next-line no-underscore-dangle
      if (feature.properties._adaguctype) delete feature.properties._adaguctype;
      // eslint-disable-next-line default-case
      switch (type) {
        case DRAWMODE.POINT:
          feature.geometry.type = 'Point';
          break;
        case DRAWMODE.MULTIPOINT:
          feature.geometry.type = 'MultiPoint';
          if (feature.geometry.coordinates.length === 0)
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            feature.geometry.coordinates.push([] as any);
          break;
        case DRAWMODE.BOX:
          feature.geometry.type = 'Polygon';
          // eslint-disable-next-line no-underscore-dangle
          feature.properties._adaguctype = 'box';
          break;
        case DRAWMODE.POLYGON:
          feature.geometry.type = 'Polygon';
          if (feature.geometry.coordinates.length === 0)
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            feature.geometry.coordinates.push([] as any);
          break;
        case DRAWMODE.LINESTRING:
          feature.geometry.type = 'LineString';
          if (feature.geometry.coordinates.length === 0)
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            feature.geometry.coordinates.push([] as any);
          break;
      }
    }
    return feature;
  }

  // eslint-disable-next-line class-methods-use-this
  checkIfFeatureIsBox(feature: GeoJsonFeature): boolean {
    if (!feature || !feature.properties || !feature.properties._adaguctype)
      return false;
    return feature.properties._adaguctype === 'box';
  }

  featureHasChanged(text: string): void {
    const { updateGeojson } = this.props;
    if (text !== 'new feature created') {
      this.validatePolys(text === 'cancelEdit');
    }
    if (updateGeojson && this.geojson) {
      updateGeojson(cloneDeep(this.geojson), text);
    }
  }

  /* Converts string input into right drawmode */
  handleDrawMode(_drawMode: string): void {
    if (_drawMode) {
      const drawMode = _drawMode.toUpperCase();
      if (drawMode === 'POINT') {
        this.myDrawMode = DRAWMODE.POINT;
      }
      if (drawMode === 'MULTIPOINT') {
        this.myDrawMode = DRAWMODE.MULTIPOINT;
      }
      if (drawMode === 'BOX') {
        this.myDrawMode = DRAWMODE.BOX;
      }
      if (drawMode === 'POLYGON') {
        this.myDrawMode = DRAWMODE.POLYGON;
      }
      if (drawMode === 'LINESTRING') {
        this.myDrawMode = DRAWMODE.LINESTRING;
      }
    }
  }

  render(): React.ReactElement {
    const { webmapjs, isInDeleteMode } = this.props;
    if (this.disabled === undefined) {
      this.disabled = isInDeleteMode;
    }

    if (webmapjs !== undefined && this.listenersInitialized !== true) {
      this.listenersInitialized = true;
      webmapjs.addListener('beforecanvasdisplay', this.adagucBeforeDraw, true);
      webmapjs.addListener('beforemousemove', this.adagucMouseMove, true);
      webmapjs.addListener('beforemousedown', this.adagucMouseDown, true);
      webmapjs.addListener('beforemouseup', this.adagucMouseUp, true);
      this.disabled = false;
    }

    if (webmapjs !== undefined) {
      webmapjs.draw();
    }

    return <div />;
  }
}
