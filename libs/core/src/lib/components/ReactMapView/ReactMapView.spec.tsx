/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, act, waitFor } from '@testing-library/react';
import {
  LayerType,
  LayerOptions,
  WMGetServiceFromStore,
} from '@opengeoweb/webmap';
import RadarGetCapabilities from './radarGetCapabilities.spec.json';

import ReactMapView, {
  isAMapLayer,
  getFeatureLayers,
  isAGeoJSONLayer,
} from './ReactMapView';
import { baseLayerGrey, overLayer } from '../../utils/testLayers';
import { defaultReduxLayerRadarKNMI } from '../../utils/defaultTestSettings';
import { getWMJSMapById, ReactMapViewLayer } from '../..';

export const mockGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [],
};

describe('src/components/ReactMapView/ReactMapView', () => {
  it('should trigger onClick when the passive map is clicked', async () => {
    const props = {
      mapId: 'map1',
      passiveMap: true,
      onClick: jest.fn(),
    };
    const { getByRole } = render(<ReactMapView {...props} />);

    fireEvent.click(getByRole('button'));

    expect(props.onClick).toHaveBeenCalledTimes(1);
  });

  it('should return false for a baselayer without geojson', () => {
    expect(isAMapLayer(baseLayerGrey)).toBeFalsy();
  });

  it('should return false for an overlayer without geojson', () => {
    expect(isAMapLayer(overLayer)).toBeFalsy();
  });

  it('should return true for a maplayer without geojson', () => {
    expect(isAMapLayer(defaultReduxLayerRadarKNMI)).toBeTruthy();
  });

  it('should return false for a baselayer with geojson', () => {
    const mockLayer = {
      ...baseLayerGrey,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeFalsy();
  });

  it('should detect geojson layer for a baselayer with geojson', () => {
    const mockLayer = {
      ...baseLayerGrey,
      geojson: mockGeoJSON,
    };
    expect(isAGeoJSONLayer(mockLayer)).toBeTruthy();
  });

  it('should return false for an overlayer with geojson', () => {
    const mockLayer = {
      ...overLayer,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeFalsy();
  });

  it('should detect geojson layer for an overlayer with geojson', () => {
    const mockLayer = {
      ...overLayer,
      geojson: mockGeoJSON,
    };
    expect(isAGeoJSONLayer(mockLayer)).toBeTruthy();
  });

  it('should return true for a maplayer with geojson', () => {
    const mockLayer = {
      ...defaultReduxLayerRadarKNMI,
      geojson: mockGeoJSON,
    };
    expect(isAMapLayer(mockLayer)).toBeTruthy();
  });

  it('should return a list of props if it has geojson', () => {
    const mockLayer = {
      ...defaultReduxLayerRadarKNMI,
      props: {
        geojson: 'string',
      },
    };
    const mockLayer2 = {
      ...baseLayerGrey,
      props: {
        geojson: 'another-string',
      },
    };
    expect(getFeatureLayers([mockLayer, mockLayer2])).toEqual([
      mockLayer2.props,
      mockLayer.props,
    ]);
  });

  it('should return an empty list if it has no geojson', () => {
    const mockLayer = {
      ...defaultReduxLayerRadarKNMI,
      props: {
        otherProp: 'string',
      },
    };
    const mockLayer2 = {
      ...baseLayerGrey,
      props: {
        anotherProp: 'another-string',
      },
    };
    expect(getFeatureLayers([mockLayer, mockLayer2])).toEqual([]);
  });

  it('should trigger onMapPinChangeLocation when the map is clicked at the same location', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      disableMapPin: false,
      onMapPinChangeLocation: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    /* Click at same location should trigger onMapPinChangeLocation */
    getWMJSMapById('map1').mouseDown(10, 10, null);
    getWMJSMapById('map1').mouseUp(10, 10, null);
    expect(props.onMapPinChangeLocation).toHaveBeenCalledTimes(1);

    /* Click at almost the same location should trigger onMapPinChangeLocation */
    getWMJSMapById('map1').mouseDown(10, 10, null);
    getWMJSMapById('map1').mouseUp(8, 8, null);
    expect(props.onMapPinChangeLocation).toHaveBeenCalledTimes(2);

    /* Click at different location should NOT trigger onMapPinChangeLocation */
    getWMJSMapById('map1').mouseDown(100, 100, null);
    getWMJSMapById('map1').mouseUp(3, 3, null);
    expect(props.onMapPinChangeLocation).toHaveBeenCalledTimes(2);
  });

  it('should not trigger onMapPinChangeLocation when the mapPin is disabled', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      disableMapPin: true,
      onMapPinChangeLocation: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    getWMJSMapById('map1').mouseDown(10, 10, null);
    getWMJSMapById('map1').mouseUp(10, 10, null);
    expect(props.onMapPinChangeLocation).not.toHaveBeenCalled();
  });

  it('should not trigger onMapPinChangeLocation when the mapPin is not visible', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: false,
      disableMapPin: false,
      onMapPinChangeLocation: jest.fn(),
    };
    render(<ReactMapView {...props} />);

    getWMJSMapById('map1').mouseDown(10, 10, null);
    getWMJSMapById('map1').mouseUp(10, 10, null);
    expect(props.onMapPinChangeLocation).not.toHaveBeenCalled();
  });

  it('should call onUpdateLayerInformation when a layer is removed', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      onUpdateLayerInformation: jest.fn(),
    };

    /* Faking a ReactMapView component where a layer was already added */
    const service = WMGetServiceFromStore('testservice');
    service.getcapabilitiesDoc = RadarGetCapabilities;

    const layerDef: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
    };

    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDef} />
      </ReactMapView>,
    );

    /* Check if WMJSMap instance was made */
    const map = getWMJSMapById('map1');

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if onUpdateLayerInformation was called */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(1),
    );

    /* Remove the layer */
    rerender(<ReactMapView {...props} />);

    /* Check if it has no layers anymore */
    expect(map.getLayers().length).toBe(0);

    /* Check if onUpdateLayerInformation was called again */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(2),
    );
  });

  it('should call onUpdateLayerInformation when the layername is changed', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
      onUpdateLayerInformation: jest.fn(),
    };

    /* Faking a ReactMapView component where a layer was already added */
    const service = WMGetServiceFromStore('testservice');
    service.getcapabilitiesDoc = RadarGetCapabilities;

    const layerDef: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
    };

    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDef} />
      </ReactMapView>,
    );

    /* Check if WMJSMap instance was made */
    const map = getWMJSMapById('map1');

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check its name */
    expect(map.getLayers()[0].name).toBe('RAD_NL25_PCP_CM');

    /* Check if onUpdateLayerInformation was called */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(1),
    );

    /* Change layerName */
    const newLayerDef: LayerOptions = {
      ...layerDef,
      name: 'Reflectivity',
    };
    await act(async () => {
      rerender(
        <ReactMapView {...props}>
          <ReactMapViewLayer {...newLayerDef} />
        </ReactMapView>,
      );
    });

    /* Check if layer is still there */
    expect(map.getLayers().length).toBe(1);

    /* Check its name */
    expect(map.getLayers()[0].name).toBe('Reflectivity');

    /* Check if onUpdateLayerInformation was called */
    await waitFor(() =>
      expect(props.onUpdateLayerInformation).toHaveBeenCalledTimes(2),
    );
  });

  it('should call onLayerError for duplicate layer id', async () => {
    const props = {
      mapId: 'map1',
      srs: 'EPSG:4326',
      bbox: { left: -180, right: 180, top: 90, bottom: -90 },
      mapPinLocation: { lat: 52, lon: 5 },
      displayMapPin: true,
    };

    /* Faking a ReactMapView component where a layer was already added */
    const service = WMGetServiceFromStore('testservice');
    service.getcapabilitiesDoc = RadarGetCapabilities;

    const layerDefA: LayerOptions = {
      id: 'testlayer',
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    const layerDefB: LayerOptions = {
      id: 'testlayer', // <== Same id as layerDefA
      name: 'RAD_NL25_PCP_CM',
      service: 'testservice',
      layerType: LayerType.mapLayer,
      ReactWMJSLayerId: 'test',
      onLayerError: jest.fn(),
    };

    const { rerender } = render(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
      </ReactMapView>,
    );

    /* Check if WMJSMap instance was made */
    const map = getWMJSMapById('map1');

    /* Check if it has one layer */
    expect(map.getLayers().length).toBe(1);

    expect(layerDefA.onLayerError).toHaveBeenCalledTimes(0);
    expect(layerDefB.onLayerError).toHaveBeenCalledTimes(0);

    jest.spyOn(console, 'warn').mockImplementation();
    /* Add an extra layer with the same id */
    rerender(
      <ReactMapView {...props}>
        <ReactMapViewLayer {...layerDefA} />
        <ReactMapViewLayer {...layerDefB} />
      </ReactMapView>,
    );
    expect(console.warn).toHaveBeenCalled();

    /* Check if it has still one layer */
    expect(map.getLayers().length).toBe(1);

    /* Check if onLayerError was called for layerDefB */
    expect(layerDefA.onLayerError).toHaveBeenCalledTimes(0);
    expect(layerDefB.onLayerError).toHaveBeenCalledTimes(1);
  });
});
