/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { GeometryObject } from 'geojson';
import { WMJSMap } from '@opengeoweb/webmap';
import AdagucMapDraw, { FeatureEvent } from './AdagucMapDraw';

export type FeatureLayer = {
  id: string;
  type: string;
  features: [];
  isInEditMode?: boolean;
  isInDeleteMode?: boolean;
  geojson?: GeoJSON.FeatureCollection;
  drawMode?: string;
  onHoverFeature?: (feature: FeatureEvent) => void;
  onClickFeature?: (feature: FeatureEvent) => void;
  updateGeojson?: (feature: GeometryObject) => void;
  exitDrawModeCallback?: () => void;
  featureNrToEdit?: string;
};

interface AdagucMapDrawContainerProps {
  featureLayers: FeatureLayer[];
  webMapJS: WMJSMap;
}

const AdagucMapDrawContainer: React.FC<AdagucMapDrawContainerProps> = ({
  featureLayers,
  webMapJS,
}: AdagucMapDrawContainerProps) => (
  <>
    {featureLayers.map((layer) => (
      <div
        data-testid={`featureLayer-${layer.id}`}
        key={layer.id}
        style={{ display: 'none' }}
      >
        <AdagucMapDraw
          geojson={layer.geojson}
          isInEditMode={layer.isInEditMode}
          isInDeleteMode={layer.isInDeleteMode}
          drawMode={layer.drawMode}
          webmapjs={webMapJS}
          onHoverFeature={(hoverInfo): void => {
            if (layer.onHoverFeature) {
              layer.onHoverFeature(hoverInfo);
            }
          }}
          onClickFeature={(hoverInfo): void => {
            if (layer.onClickFeature) {
              layer.onClickFeature(hoverInfo);
            }
          }}
          updateGeojson={(geojson): void => {
            if (layer.updateGeojson) {
              layer.updateGeojson(geojson);
            }
          }}
          exitDrawModeCallback={(): void => {
            if (layer.exitDrawModeCallback) layer.exitDrawModeCallback();
          }}
          featureNrToEdit={parseInt(layer.featureNrToEdit || '0', 10)}
        />
      </div>
    ))}
  </>
);
export default AdagucMapDrawContainer;
