/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { getCurrentDimensionValue } from './ReactMapViewParseLayer';

export const mockGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [],
};

describe('src/components/ReactMapView/ReactMapViewParseLayer', () => {
  it('should get current dimension value', () => {
    expect(getCurrentDimensionValue([], '')).toBeUndefined();
    expect(
      getCurrentDimensionValue([{ name: 'humid', currentValue: '800' }], ''),
    ).toBeUndefined();
    expect(
      getCurrentDimensionValue(
        [{ name: 'humid', currentValue: '800' }],
        'humid',
      ),
    ).toEqual('800');
    expect(
      getCurrentDimensionValue(
        [
          { name: 'humid', currentValue: '800' },
          { name: 'time', currentValue: '2022-08-10T09:00:00Z' },
        ],
        'humid',
      ),
    ).toEqual('800');
    expect(
      getCurrentDimensionValue(
        [
          { name: 'humid', currentValue: '800' },
          { name: 'time', currentValue: '2022-08-10T09:00:00Z' },
        ],
        'time',
      ),
    ).toEqual('2022-08-10T09:00:00Z');

    expect(
      getCurrentDimensionValue(
        [{ name: 'humid', currentValue: '800' }, { name: 'time' }],
        '2022-08-10T09:00:00Z',
      ),
    ).toBeUndefined();
  });
});
