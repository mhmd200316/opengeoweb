/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import { WMJSMap } from '@opengeoweb/webmap';
import {
  SetMapDimensionPayload,
  Dimension,
  SetBboxPayload,
  Bbox,
  MapPinLocationPayload,
  MapLocation,
  UpdateLayerInfoPayload,
} from '../../store/mapStore/types';

export interface ReactMapViewProps {
  listeners?: {
    name?: string;
    data: string;
    keep: boolean;
    callbackfunction: (webMap: WMJSMap, value: string) => void;
  }[];
  srs?: string;
  bbox?: Bbox;
  children?: React.ReactNode;
  mapId: string;
  activeLayerId?: string;
  showScaleBar?: boolean;
  showLegend?: boolean;
  passiveMap?: boolean;
  displayTimeInMap?: boolean;
  animationDelay?: number;
  dimensions?: Dimension[];
  onClick?: () => void;
  displayMapPin?: boolean;
  mapPinLocation?: MapLocation;
  shouldAutoFetch?: boolean;
  showLayerInfo?: boolean;
  disableMapPin?: boolean;
  /* Callback actions */
  onMount?: (mapId: string, WMJSMapInstance: WMJSMap) => void;
  onUnMount?: (mapId: string, WMJSMapInstance: WMJSMap) => void;
  onMapChangeDimension?: (payload: SetMapDimensionPayload) => void;
  onMapZoomEnd?: (payload: SetBboxPayload) => void;
  onRegisterAdaguc?: (WMJSMapInstance: WMJSMap) => void;
  onMapPinChangeLocation?: (payload: MapPinLocationPayload) => void;
  onUpdateLayerInformation?: (payload: UpdateLayerInfoPayload) => void;
}
