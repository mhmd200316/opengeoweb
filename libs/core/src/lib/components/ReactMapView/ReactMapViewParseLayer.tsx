/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  WMLayer,
  WMGetServiceFromStore,
  WMJSMap,
  WMJSDimension,
} from '@opengeoweb/webmap';
import {
  LayerActionOrigin,
  UpdateLayerInfoPayload,
} from '../../store/mapStore/types';

import { MapViewProps } from '../MapView/types';

export const getCurrentDimensionValue = (
  dimensions = [],
  name: string,
): string => {
  const currentDimension = dimensions.find((dim) => dim.name === name);
  return currentDimension?.currentValue;
};

export const setServiceMetadata = (
  wmLayer: WMLayer,
  mapId: string,
  webMapJSInstance: WMJSMap,
  mapProperties: MapViewProps,
  onUpdateLayerInformation?: (payload: UpdateLayerInfoPayload) => void,
): void => {
  const origin = LayerActionOrigin.ReactMapViewParseLayer;
  const service = WMGetServiceFromStore(wmLayer.service);

  /* Update list of layers for service */
  const done = (layers): void => {
    const updateObject: UpdateLayerInfoPayload = {
      origin,
      serviceLayers: {
        name: wmLayer.title,
        id: service.id,
        serviceUrl: wmLayer.service,
        layers,
        isUserAddedService: false,
      },
      layerStyle: {
        origin,
        layerId: wmLayer.ReactWMJSLayerId,
        style: wmLayer.currentStyle,
      },
      mapDimensions: {
        origin,
        mapId,
        dimensions: webMapJSInstance.mapdimensions.map(
          ({ name, units, currentValue, synced }) => {
            const reactDim =
              mapProperties.dimensions &&
              mapProperties.dimensions.find((dim) => dim.name === name);
            const newDimValue =
              (reactDim && reactDim.currentValue) || currentValue;
            const newDimSynced = (reactDim && reactDim.synced) || synced;
            return {
              name,
              units,
              currentValue: newDimValue,
              synced: newDimSynced,
            };
          },
        ),
      },
      layerDimensions: {
        origin,
        layerId: wmLayer.id,
        dimensions: wmLayer.dimensions.map((dim: WMJSDimension) => {
          const { name, units, synced } = dim;
          const currentDimensionValue = getCurrentDimensionValue(
            wmLayer.dimensions,
            name,
          );

          const newDimensionValue = currentDimensionValue || dim.getValue();

          return {
            name,
            units,
            currentValue: newDimensionValue,
            minValue: dim.getFirstValue(),
            maxValue: dim.getLastValue(),
            timeInterval: dim.getDimInterval(),
            values: dim.getValues(),
            synced,
          };
        }),
      },
    };
    onUpdateLayerInformation && onUpdateLayerInformation(updateObject);
  };

  service.getLayerObjectsFlat(
    done,
    () => {
      // do nothing
    },
    false,
    {
      headers: wmLayer.headers,
    },
  );
};
