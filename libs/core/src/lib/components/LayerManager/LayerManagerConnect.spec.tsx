/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import { LayerManagerConnect } from '.';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { uiActions } from '../../store';

describe('src/components/LayerManager/LayerManagerConnect', () => {
  it('should register the dialog when mounting', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: { byId: { mapId123: {} }, allIds: ['mapId123'] },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManagerConnect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: 'layerManager',
        setOpen: false,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should register the dialog when mounting when layerSelect is enabled', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: { byId: { mapId123: {} }, allIds: ['mapId123'] },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManagerConnect layerSelect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: 'keywordFilter',
        setOpen: false,
      }),
      uiActions.registerDialog({
        type: 'layerSelect',
        setOpen: false,
      }),
      uiActions.registerDialog({
        type: 'layerManager',
        setOpen: false,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
