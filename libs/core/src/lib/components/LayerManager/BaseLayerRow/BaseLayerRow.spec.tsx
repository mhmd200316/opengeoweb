/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { mockStateMapWithAnimationDelayWithoutLayers } from '../../../utils/testUtils';
import BaseLayerRow from './BaseLayerRow';
import { LayerManagerWidth } from '../LayerManagerUtils';
import { CoreThemeStoreProvider } from '../../Providers/Providers';

describe('src/components/LayerManager/BaseLayerRow/BaseLayerRow', () => {
  const props = {
    mapId: 'mapId_1',
    layerManagerWidth: LayerManagerWidth.lg,
  };

  it('should render correct component', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <BaseLayerRow {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('baseLayerRow')).toBeTruthy();
  });
});
