/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Grid } from '@mui/material';
import BaseLayersConnect from './BaseLayers/BaseLayersConnect';

import {
  LayerManagerColumnsLarge,
  LayerManagerColumnsSmall,
  LayerManagerWidth,
} from '../LayerManagerUtils';
import { Layer, LayerType } from '../../../store/mapStore/types';
import { Service } from '../../WMSLoader/services';
import AddLayersButton from '../AddLayersButton/AddLayersButton';
import WMSLayerTreeConnect from '../../WMSLoader/WMSLayerTree/WMSLayerTreeConnect';
import {
  availableDefaultBaseLayers,
  preloadedDefaultBaseServices,
} from '../../../utils/defaultConfigurations';

interface BaseLayerRowProps {
  mapId: string;
  preloadedAvailableBaseLayers?: Layer[];
  preloadedServices?: Service[];
  layerManagerWidth: LayerManagerWidth;
  tooltip?: string;
}

const BaseLayerRow: React.FC<BaseLayerRowProps> = ({
  mapId,
  preloadedAvailableBaseLayers = availableDefaultBaseLayers,
  preloadedServices = preloadedDefaultBaseServices,
  layerManagerWidth,
  tooltip = '',
}: BaseLayerRowProps) => {
  const columnSizes =
    layerManagerWidth === LayerManagerWidth.sm
      ? LayerManagerColumnsSmall
      : LayerManagerColumnsLarge;

  return (
    <Grid
      container
      item
      data-testid="baseLayerRow"
      sx={{
        width: '100%',
        height: '36px',
      }}
    >
      <Grid item sx={{ ...columnSizes.column1, marginTop: '-2px' }}>
        <AddLayersButton
          preloadedServices={preloadedServices}
          tooltip={tooltip}
          onRenderTree={(service): React.ReactChild => (
            <WMSLayerTreeConnect
              mapId={mapId}
              service={service}
              layerType={LayerType.baseLayer}
            />
          )}
        />
      </Grid>
      <Grid item sx={columnSizes.column2}>
        <BaseLayersConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        />
      </Grid>
    </Grid>
  );
};

export default BaseLayerRow;
