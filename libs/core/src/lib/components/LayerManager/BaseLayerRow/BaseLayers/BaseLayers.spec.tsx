/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import BaseLayers from './BaseLayers';
import {
  baseLayerGrey,
  baseLayerOpenStreetMapNL,
  baseLayerWorldMap,
} from '../../../../utils/testLayers';
import { CoreThemeProvider } from '../../../Providers/Providers';

describe('src/components/LayerManager/BaseLayerRow/BaseLayers/BaseLayers', () => {
  it('should show a message if there are no available baselayers', () => {
    const mockProps = {
      selectedBaseLayers: [],
      availableBaseLayers: [],
      onChangeBaseLayers: jest.fn(),
    };
    const { queryByText } = render(
      <CoreThemeProvider>
        <BaseLayers {...mockProps} />
      </CoreThemeProvider>,
    );

    expect(queryByText('No service available')).toBeTruthy();
  });

  it('should show no value if there is no baselayer selected yet', () => {
    const mockProps = {
      selectedBaseLayers: [],
      availableBaseLayers: [
        baseLayerGrey,
        baseLayerOpenStreetMapNL,
        baseLayerWorldMap,
      ],
      onChangeBaseLayers: jest.fn(),
    };
    const { getByTestId } = render(
      <CoreThemeProvider>
        <BaseLayers {...mockProps} />
      </CoreThemeProvider>,
    );

    expect(getByTestId('selectBaseLayer').textContent).toBe('​');
  });

  it('should show the first of the selected baselayers as selected', () => {
    const mockProps = {
      selectedBaseLayers: [baseLayerGrey, baseLayerWorldMap],
      availableBaseLayers: [
        baseLayerGrey,
        baseLayerOpenStreetMapNL,
        baseLayerWorldMap,
      ],
      onChangeBaseLayers: jest.fn(),
    };
    const { getByTestId } = render(
      <CoreThemeProvider>
        <BaseLayers {...mockProps} />
      </CoreThemeProvider>,
    );

    expect(getByTestId('selectBaseLayer').textContent).toEqual(
      mockProps.selectedBaseLayers[0].name,
    );
  });

  it('should trigger onChangeBaseLayers when choosing a new baselayer', async () => {
    const mockProps = {
      selectedBaseLayers: [baseLayerGrey, baseLayerWorldMap],
      availableBaseLayers: [
        baseLayerGrey,
        baseLayerOpenStreetMapNL,
        baseLayerWorldMap,
      ],
      onChangeBaseLayers: jest.fn(),
    };
    const { getByTestId, findByText } = render(
      <CoreThemeProvider>
        <BaseLayers {...mockProps} />
      </CoreThemeProvider>,
    );

    fireEvent.mouseDown(getByTestId('selectBaseLayer'));

    const newBaseLayer = mockProps.availableBaseLayers[2];
    const menuItem = await findByText(newBaseLayer.name);
    await waitFor(() => fireEvent.click(menuItem));

    expect(mockProps.onChangeBaseLayers).toHaveBeenCalledWith(newBaseLayer.id);
  });

  it('should call onChangeBaseLayers on wheel scroll', async () => {
    const mockProps = {
      selectedBaseLayers: [baseLayerGrey, baseLayerWorldMap],
      availableBaseLayers: [
        baseLayerGrey,
        baseLayerOpenStreetMapNL,
        baseLayerWorldMap,
      ],
      onChangeBaseLayers: jest.fn(),
    };
    const { getByTestId } = render(
      <CoreThemeProvider>
        <BaseLayers {...mockProps} />
      </CoreThemeProvider>,
    );
    const select = getByTestId('selectBaseLayer');

    fireEvent.wheel(select, { deltaY: 1 });
    expect(mockProps.onChangeBaseLayers).toHaveBeenCalledTimes(1);
    expect(mockProps.onChangeBaseLayers).toHaveBeenCalledWith(
      mockProps.availableBaseLayers[1].id,
    );

    fireEvent.wheel(select, { deltaY: -1 });
    expect(mockProps.onChangeBaseLayers).toHaveBeenCalledTimes(2);
    expect(mockProps.onChangeBaseLayers).toHaveBeenCalledWith(
      mockProps.availableBaseLayers[0].id,
    );
  });

  it('should show a menu item for each available baselayer', async () => {
    const mockProps = {
      selectedBaseLayers: [],
      availableBaseLayers: [
        baseLayerGrey,
        baseLayerOpenStreetMapNL,
        baseLayerWorldMap,
      ],
      onChangeBaseLayers: jest.fn(),
    };
    const { getByTestId, findAllByRole } = render(
      <CoreThemeProvider>
        <BaseLayers {...mockProps} />
      </CoreThemeProvider>,
    );

    fireEvent.mouseDown(getByTestId('selectBaseLayer'));
    const list = await findAllByRole('option');
    const listWithoutHeading = list.slice(1);

    expect(listWithoutHeading.length).toEqual(
      mockProps.availableBaseLayers.length,
    );
    listWithoutHeading.forEach((li) =>
      expect(
        mockProps.availableBaseLayers.find(
          (baselayer) => baselayer.name === li.textContent,
        ),
      ).toBeTruthy(),
    );
  });
});
