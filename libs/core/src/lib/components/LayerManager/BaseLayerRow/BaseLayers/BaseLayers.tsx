/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { MenuItem, FormControl, SelectChangeEvent } from '@mui/material';

import { TooltipSelect } from '@opengeoweb/shared';
import { Layer } from '../../../../store/mapStore/types';

interface BaseLayersProps {
  selectedBaseLayers: Layer[];
  availableBaseLayers: Layer[];
  onChangeBaseLayers: (name: string) => void;
}

const BaseLayers: React.FC<BaseLayersProps> = ({
  selectedBaseLayers,
  availableBaseLayers,
  onChangeBaseLayers,
}: BaseLayersProps) => {
  if (!availableBaseLayers || !availableBaseLayers.length) {
    return <div>No service available</div>;
  }

  const selectedBaseLayerId =
    typeof selectedBaseLayers[0] !== 'undefined'
      ? selectedBaseLayers[0].id
      : '';

  const list = availableBaseLayers.map((layer) => ({
    value: layer.id,
  }));

  const currentIndex = availableBaseLayers.findIndex(
    (layer) => selectedBaseLayerId === layer.id,
  );

  return (
    <FormControl style={{ width: '100%' }}>
      <TooltipSelect
        disableUnderline
        tooltip={`Base layer: ${availableBaseLayers[currentIndex]?.name}`}
        inputProps={{
          SelectDisplayProps: {
            'data-testid': 'selectBaseLayer',
          },
        }}
        style={{ maxWidth: '100%' }}
        isEnabled={true}
        value={selectedBaseLayerId}
        list={list}
        currentIndex={currentIndex}
        onChange={(event: SelectChangeEvent): void => {
          onChangeBaseLayers(event.target.value);
        }}
        onChangeMouseWheel={(e): void => onChangeBaseLayers(e.value)}
      >
        <MenuItem disabled>Base layer</MenuItem>
        {availableBaseLayers.map((baseLayer: Layer) => (
          <MenuItem key={baseLayer.id} value={baseLayer.id}>
            {baseLayer.name}
          </MenuItem>
        ))}
      </TooltipSelect>
    </FormControl>
  );
};

export default BaseLayers;
