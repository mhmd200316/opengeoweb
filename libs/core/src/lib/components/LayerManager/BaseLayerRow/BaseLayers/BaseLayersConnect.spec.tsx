/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { LayerType } from '@opengeoweb/webmap';
import BaseLayersConnect, {
  areAvailableBaseLayersSame,
  constructLayerId,
  constructListAvailableBaseLayers,
} from './BaseLayersConnect';
import {
  baseLayer,
  baseLayerGrey,
  baseLayerWorldMap,
} from '../../../../utils/testLayers';
import { mockStateMapWithLayer } from '../../../../utils/testUtils';
import * as helpers from '../../../../store/mapStore/utils/helpers';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { layerActions } from '../../../../store';
import { LayerActionOrigin } from '../../../../store/mapStore/types';

describe('src/components/LayerManager/BaseLayerRow/BaseLayers/BaseLayersConnect', () => {
  it('should should add available baseLayers on mount if not present in store', async () => {
    const mapId = 'mapid_1';
    const preloadedAvailableBaseLayers = [
      { ...baseLayerGrey, mapId },
      { ...baseLayerWorldMap, mapId },
    ];
    const mockState = mockStateMapWithLayer(
      preloadedAvailableBaseLayers[0],
      mapId,
    );

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { rerender } = render(
      <CoreThemeStoreProvider store={store}>
        <BaseLayersConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        />
      </CoreThemeStoreProvider>,
    );

    const expectedActions = [
      layerActions.setAvailableBaseLayers({
        layers: preloadedAvailableBaseLayers,
      }),
    ];

    const expectedLayerResult = expectedActions[0].payload.layers;
    expect(store.getActions()[0].payload.layers).toEqual(
      expectedLayerResult.map((layer) => ({
        ...layer,
        id: expect.any(String),
      })),
    );
    expect(store.getActions()).toHaveLength(1);

    // rerender with same preloaded baseLayers
    rerender(
      <CoreThemeStoreProvider store={store}>
        <BaseLayersConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        />
      </CoreThemeStoreProvider>,
    );
    expect(store.getActions()).toHaveLength(1);

    // rerender with new preloaded baseLayers
    const newPreloadedAvailableBaseLayers = preloadedAvailableBaseLayers.map(
      (layer) => ({ ...layer, name: `base-layer-new-${layer.name}` }),
    );
    rerender(
      <CoreThemeStoreProvider store={store}>
        <BaseLayersConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={newPreloadedAvailableBaseLayers}
        />
      </CoreThemeStoreProvider>,
    );
    expect(store.getActions()).toHaveLength(2);

    const expectedActions2 = [
      layerActions.setAvailableBaseLayers({
        layers: preloadedAvailableBaseLayers,
      }),
      layerActions.setAvailableBaseLayers({
        layers: newPreloadedAvailableBaseLayers,
      }),
    ];
    expect(store.getActions()[0].payload.layers).toEqual(
      expectedActions2[0].payload.layers.map((layer) => ({
        ...layer,
        id: expect.any(String),
      })),
    );
    expect(store.getActions()[1].payload.layers).toEqual(
      expectedActions2[1].payload.layers.map((layer) => ({
        ...layer,
        id: expect.any(String),
      })),
    );
  });

  it('should add a base layer', async () => {
    const mapId = 'mapid_1';
    const preloadedAvailableBaseLayers = [
      { ...baseLayerGrey, mapId },
      { ...baseLayerWorldMap, mapId },
    ];
    const mockState = mockStateMapWithLayer(
      preloadedAvailableBaseLayers[0],
      mapId,
    );
    mockState.layers.availableBaseLayers = {
      allIds: [baseLayerGrey.id, baseLayerWorldMap.id],
      byId: {
        [baseLayerGrey.id]: { ...baseLayerGrey, mapId },
        [baseLayerWorldMap.id]: {
          ...baseLayerWorldMap,
          mapId,
        },
      },
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, findByText } = render(
      <CoreThemeStoreProvider store={store}>
        <BaseLayersConnect
          mapId={mapId}
          preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        />
      </CoreThemeStoreProvider>,
    );

    const baseLayerSelect = getByTestId('selectBaseLayer');
    fireEvent.mouseDown(baseLayerSelect);

    const newBaseLayer = preloadedAvailableBaseLayers[1];
    const menuItem = await findByText(newBaseLayer.name);
    await waitFor(() => fireEvent.click(menuItem));

    const expectedActions = layerActions.setBaseLayers({
      layers: [newBaseLayer],
      mapId,
      origin: LayerActionOrigin.layerManager,
    });

    expect(store.getActions()).toContainEqual(expectedActions);
  });

  describe('constructLayerId', () => {
    it('should use the layerId of the selected base layer in case id or name match', async () => {
      expect(
        constructLayerId(
          {
            id: 'somerandomid',
            name: 'WorldMap_Light_Grey_Canvas',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
        ),
      ).toEqual('base-layer-1');
      expect(
        constructLayerId(
          {
            id: 'base-layer-1',
            name: 'WorldMap_Light_Grey_Canvas',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
        ),
      ).toEqual('base-layer-1');
      jest
        .spyOn(helpers, 'generateLayerId')
        .mockImplementationOnce((): string => {
          return 'layer12222';
        });
      expect(
        constructLayerId(
          {
            id: 'someRandomId',
            name: 'someRandomName',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
        ),
      ).toEqual('layer12222');
    });
  });

  describe('constructListAvailableBaseLayers', () => {
    it('should construct a list of available baselayers with mapId and unique id from the passed in selected layers and available layers', async () => {
      jest.spyOn(helpers, 'generateLayerId').mockImplementation((): string => {
        return 'layer12222';
      });
      const expectedMapIdAndLayerId = { mapId: 'mapid-1', id: 'layer12222' };
      const selectedLayer = {
        id: 'somerandomid',
        name: 'WorldMap_Light_Grey_Canvas',
        type: 'twms',
        layerType: LayerType.baseLayer,
      };
      expect(
        constructListAvailableBaseLayers(
          [selectedLayer],
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
          'mapid-1',
        ),
      ).toEqual([
        // Because selected layer has same name as this passed in initialAvailableBaseLayer, the layer id of the selected layer should be used
        { ...baseLayerGrey, mapId: 'mapid-1', id: 'somerandomid' },
        { ...baseLayer, ...expectedMapIdAndLayerId },
        { ...baseLayerWorldMap, ...expectedMapIdAndLayerId },
      ]);
    });
    it('should add currently selected base layer if not in the list of availableBaseLayers', async () => {
      jest.spyOn(helpers, 'generateLayerId').mockImplementation((): string => {
        return 'layer12222';
      });
      const expectedMapIdAndLayerId = { mapId: 'mapid-1', id: 'layer12222' };
      const selectedLayer = {
        id: 'somerandomid',
        name: 'some_different_baselayer',
        type: 'twms',
        layerType: LayerType.baseLayer,
      };
      expect(
        constructListAvailableBaseLayers(
          [selectedLayer],
          [baseLayerGrey, baseLayer, baseLayerWorldMap],
          'mapid-1',
        ),
      ).toEqual([
        // Because selected layer has different id and name than any layer passed in initialAvailableBaseLayer, the layer selectedBaseLayer is added to the list at the bottom
        { ...baseLayerGrey, ...expectedMapIdAndLayerId },
        { ...baseLayer, ...expectedMapIdAndLayerId },
        { ...baseLayerWorldMap, ...expectedMapIdAndLayerId },
        { ...selectedLayer, mapId: 'mapid-1' },
      ]);
    });
  });

  describe('areAvailableBaseLayersSame', () => {
    it('should return true if available baseLayers are the same', () => {
      expect(areAvailableBaseLayersSame([], [])).toBeTruthy();
      expect(
        areAvailableBaseLayersSame(
          [
            {
              id: 'testId',
              name: 'testName',
              type: 'twms',
              layerType: LayerType.baseLayer,
            },
          ],
          [
            {
              id: 'testId',
              name: 'testName',
              type: 'twms',
              layerType: LayerType.baseLayer,
            },
          ],
        ),
      ).toBeTruthy();

      expect(
        areAvailableBaseLayersSame(
          [
            {
              id: 'testId',
              name: 'testName',
              type: 'twms',
              layerType: LayerType.baseLayer,
            },
          ],
          [
            {
              id: 'testId2',
              name: 'testName',
              type: 'twms',
              layerType: LayerType.baseLayer,
            },
          ],
        ),
      ).toBeTruthy();
    });
  });

  it('should return false if available baseLayers are not same', () => {
    expect(
      areAvailableBaseLayersSame(
        [],
        [
          {
            id: 'testId',
            name: 'testName',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
      ),
    ).toBeFalsy();
    expect(
      areAvailableBaseLayersSame(
        [
          {
            id: 'testIdd',
            name: 'testName',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
        [],
      ),
    ).toBeFalsy();

    expect(
      areAvailableBaseLayersSame(
        [
          {
            id: 'testId',
            name: 'testName2',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
        [
          {
            id: 'testId',
            name: 'testName',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
      ),
    ).toBeFalsy();

    expect(
      areAvailableBaseLayersSame(
        [
          {
            id: 'testId',
            name: 'testName',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
        [
          {
            id: 'testId2',
            name: 'testName2',
            type: 'twms',
            layerType: LayerType.baseLayer,
          },
        ],
      ),
    ).toBeFalsy();
  });
});
