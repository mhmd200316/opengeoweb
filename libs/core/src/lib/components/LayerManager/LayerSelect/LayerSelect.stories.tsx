/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import configureStore from 'redux-mock-store';

import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import LayerSelect from './LayerSelect';

const mockState = {
  services: {
    byId: {
      mockService: {
        id: 'testId',
        name: 'testName',
        serviceUrl: 'mockService',
        layers: [
          {
            name: 'testLayerName',
            text: 'testLayerText',
            leaf: true,
            path: [],
            keywords: [],
            abstract: 'testLayerAbstract',
          },
          {
            name: 'testLayerNameTwo',
            text: 'testLayerTextTwo',
            leaf: true,
            path: [],
            keywords: [],
          },
        ],
      },
      mockService2: {
        id: 'testId2',
        name: 'testName2',
        serviceUrl: 'mockService2',
        layers: [
          {
            name: 'testLayerNameForServiceTwo',
            text: 'testLayerTextForServiceTwo',
            leaf: true,
            path: [],
            keywords: [],
            abstract: 'testLayerAbstractForServiceTwo',
          },
        ],
      },
      mockService3: {
        id: 'testId3',
        name: 'testName3',
        serviceUrl: 'mockService3',
        layers: [
          {
            name: 'testLayerNameForServiceThree',
            text: 'testLayerTextForServiceThree',
            leaf: true,
            path: [],
            keywords: [],
            abstract: 'testLayerAbstractForServiceThree',
          },
        ],
      },
    },
    allIds: ['mockService', 'mockService2', 'mockService3'],
  },
  layerSelect: {
    filters: {
      activeServices: {
        byId: {
          mockService: {
            serviceName: 'FMI',
            serviceUrl: 'mockserviceUrl1',
            enabled: true,
            keywordsPerService: [],
          },
          mockService2: {
            serviceName: 'MET Norway',
            serviceUrl: 'mockserviceUrl2',
            enabled: true,
            keywordsPerService: [],
          },
          mockService3: {
            serviceName: 'KNMI Radar',
            serviceUrl: 'mockserviceUrl3',
            enabled: true,
            keywordsPerService: [],
          },
        },
        allIds: ['mockService', 'mockService2', 'mockService3'],
      },
      searchFilter: '',
      keywords: {
        byId: {},
        allIds: [],
      },
    },
  },
};

const mockStore = configureStore();
const store = mockStore(mockState);
store.addModules = (): void => null; // mocking the dynamic module loader

const LayerSelectDemo: React.FC = () => {
  return (
    <div style={{ height: '100vh', backgroundColor: 'grey' }}>
      <LayerSelect mapId="mapId_1" bounds="parent" isOpen={true} />
    </div>
  );
};

export const LayerSelectDemoLightTheme = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <LayerSelectDemo />
  </CoreThemeStoreProvider>
);

export const LayerSelectDemoDarkTheme = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <LayerSelectDemo />
  </CoreThemeStoreProvider>
);

LayerSelectDemoLightTheme.storyName = 'Layer Select Light Theme (takeSnapshot)';
LayerSelectDemoDarkTheme.storyName = 'Layer Select Dark Theme (takeSnapshot)';
