/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import KeywordFilterResultsListItemConnect from './KeywordFilterResultsListItemConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { layerSelectActions } from '../../../../store';

describe('src/components/LayerSelect/KeywordFilterResultsListItemConnect', () => {
  const props = {
    keyword: 'keyword1',
  };
  const mockStore = configureStore();
  const store = mockStore({
    layerSelect: {
      filters: {
        keywords: {
          byId: {
            keyword1: {
              id: 'keyword1',
              amount: 1,
              amountVisible: 1,
              checked: true,
            },
            keyword2: {
              id: 'keyword2',
              amount: 1,
              amountVisible: 1,
              checked: true,
            },
          },
          allIds: ['keyword1', 'keyword2'],
        },
      },
    },
    ui: {
      dialogs: {
        keywordFilter: {
          isOpen: true,
        },
      },
    },
  });
  store.addModules = jest.fn(); // mocking the dynamic module loader
  it('should render the component', () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterResultsListItemConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const component = queryByTestId('filterResultListItem');
    expect(component).toBeTruthy();
  });

  it('clicking checkbox should dispatch an acton to toggle one keyword', () => {
    const { queryByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterResultsListItemConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    store.clearActions();
    const checkbox = queryByRole('checkbox') as HTMLInputElement;
    fireEvent.click(checkbox);

    const expectedAction = [
      layerSelectActions.toggleKeywords({
        keywords: ['keyword1'],
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch an acton that checks only one checkbox', async () => {
    const { queryAllByTestId, queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterResultsListItemConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    store.clearActions();

    const filterResultsListItems = queryAllByTestId('filterResultListItem');
    fireEvent.mouseOver(filterResultsListItems[0]);
    fireEvent.focus(filterResultsListItems[0]);

    await waitFor(
      () => {
        const onlyButton = queryByTestId('onlyButton');
        fireEvent.click(onlyButton);
      },
      { timeout: 2000 },
    );

    const expectedAction = [
      layerSelectActions.enableOnlyOneKeyword({
        keyword: 'keyword1',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
