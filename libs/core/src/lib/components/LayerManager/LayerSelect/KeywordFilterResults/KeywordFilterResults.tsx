/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

/* eslint-disable react/no-array-index-key */
import * as React from 'react';
import { Grid, List, Theme } from '@mui/material';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import { ToolContainerDraggable } from '@opengeoweb/shared';
import FilterResultsListItemConnect from './KeywordFilterResultsListItemConnect';
import { Source } from '../../../../store/ui/types';
import KeywordFilterSelectAllSwitchConnect from './KeywordFilterSelectAllSwitchConnect';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    selectAll: {
      padding: '15px',
      paddingLeft: '20px',
    },
    switch: {
      padding: '15px',
    },
    selectAllContainer: {
      backgroundColor: theme.palette.background.paper,
      position: 'sticky',
      top: 0,
      zIndex: 100,
    },
  }),
);

interface KeywordFilterResultsProps {
  allKeywordIds: string[];
  mapId: string;
  bounds?: string;
  showTitle?: boolean;
  onClose?: () => void;
  onMouseDown?: () => void;
  isOpen: boolean;
  order?: number;
  source?: Source;
}

const KeywordFilterResults: React.FC<KeywordFilterResultsProps> = ({
  allKeywordIds,
  mapId,
  bounds,
  onClose = (): void => {},
  showTitle,
  isOpen,
  onMouseDown = (): void => {},
  order = 0,
  source = 'module',
}: KeywordFilterResultsProps) => {
  const classes = useStyles();

  return (
    <ToolContainerDraggable
      title={showTitle ? `Filter Results ${mapId}` : 'Filter Results'}
      data-testid="keywordFilterResults"
      startSize={{ width: 349, height: 600 }}
      minWidth={312}
      minHeight={192}
      isOpen={isOpen}
      onClose={onClose}
      headerSize="small"
      bounds={bounds}
      onMouseDown={onMouseDown}
      order={order}
      source={source}
    >
      <Grid
        container
        item
        xs={12}
        justifyContent="space-between"
        alignItems="center"
        className={classes.selectAllContainer}
      >
        <Grid
          container
          item
          xs={6}
          justifyContent="flex-start"
          alignItems="center"
          className={classes.selectAll}
        >
          Select all
        </Grid>
        <Grid
          container
          item
          xs={6}
          justifyContent="flex-end"
          alignContent="center"
          className={classes.switch}
        >
          <KeywordFilterSelectAllSwitchConnect />
        </Grid>
      </Grid>
      <List dense={true} style={{ padding: '0px' }}>
        {allKeywordIds.map((keywordId) => {
          return (
            <FilterResultsListItemConnect key={keywordId} keyword={keywordId} />
          );
        })}
      </List>
    </ToolContainerDraggable>
  );
};

export default KeywordFilterResults;
