/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import KeywordFilterResultsConnect from './KeywordFilterResultsConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';

describe('src/components/LayerSelect/KeywordFilterResultsConnect', () => {
  it('should render component', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      layerSelect: {
        filters: {
          keywords: {
            byId: {
              keyword_A: {
                id: 'keyword_A',
                amount: 1,
                amountVisible: 1,
                checked: true,
              },
            },
            allIds: ['keyword_A'],
          },
        },
      },
      ui: {
        dialogs: {
          keywordFilter: {
            isOpen: true,
          },
        },
      },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterResultsConnect />
      </CoreThemeStoreProvider>,
    );

    const component = queryByTestId('keywordFilterResults');
    expect(component).toBeTruthy();
  });
});
