/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import configureStore from 'redux-mock-store';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import KeywordFilterResults from './KeywordFilterResults';

export default {
  title: 'components/LayerSelect/KeywordFilterResults',
};

const props = {
  mapId: 'mapId-1',
  isOpen: true,
  allKeywordIds: ['keyword-1', 'keyword-2', 'keyword-3'],
};

const mockState = {
  layerSelect: {
    filters: {
      activeServices: {
        byId: {},
        allIds: [],
      },
      searchFilter: '',
      keywords: {
        byId: {
          'keyword-1': {
            id: 'keyword-1',
            amount: 1,
            amountVisible: 1,
            checked: false,
          },
          'keyword-2': {
            id: 'keyword-2',
            amount: 1,
            amountVisible: 1,
            checked: true,
          },
          'keyword-3': {
            id: 'keyword-3',
            amount: 1,
            amountVisible: 1,
            checked: true,
          },
        },
        allIds: ['keyword-1', 'keyword-2', 'keyword-3'],
      },
    },
  },
};

const mockStore = configureStore();
const store = mockStore(mockState);
store.addModules = (): void => null; // mocking the dynamic module loader

export const KeywordFilterResultsLight = (): React.ReactElement => {
  return (
    <CoreThemeStoreProvider store={store} theme={lightTheme}>
      <KeywordFilterResults {...props} />
    </CoreThemeStoreProvider>
  );
};

export const KeywordFilterResultsDark = (): React.ReactElement => {
  return (
    <CoreThemeStoreProvider store={store} theme={darkTheme}>
      <KeywordFilterResults {...props} />
    </CoreThemeStoreProvider>
  );
};

KeywordFilterResultsDark.storyName =
  'Keyword Filter Results Dark Theme (takeSnapshot)';
KeywordFilterResultsLight.storyName =
  'Keyword Filter Results Light Theme (takeSnapshot)';
