/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';

import FilterResultsListItem from './KeywordFilterResultsListItem';
import { CoreThemeProvider } from '../../../Providers/Providers';

describe('src/components/LayerSelect/KeywordFilterResultsListItem', () => {
  const props = {
    keyword: {
      id: 'keyword1',
      amount: 1,
      amountVisible: 1,
      checked: true,
    },
    toggleKeywords: jest.fn(),
    enableOnlyOneKeyword: jest.fn(),
  };
  it('should render the component', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <FilterResultsListItem {...props} />
      </CoreThemeProvider>,
    );

    const component = getByTestId('filterResultListItem');
    expect(component).toBeTruthy();
  });

  it('should call toggleKeywords with correct value', async () => {
    const { getByRole } = render(
      <CoreThemeProvider>
        <FilterResultsListItem {...props} />
      </CoreThemeProvider>,
    );

    const checkbox = getByRole('checkbox') as HTMLInputElement;
    fireEvent.click(checkbox);
    expect(props.toggleKeywords).toBeCalledWith(['keyword1']);
  });

  it('should call enableOnlyOneKeyword with correct value', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <FilterResultsListItem {...props} />
      </CoreThemeProvider>,
    );
    const filterResultsListItem = getByTestId('filterResultListItem');
    fireEvent.mouseOver(filterResultsListItem);
    fireEvent.focus(filterResultsListItem);
    const onlyButton = getByTestId('onlyButton');
    fireEvent.click(onlyButton);
    expect(props.enableOnlyOneKeyword).toBeCalledWith('keyword1');
  });
});
