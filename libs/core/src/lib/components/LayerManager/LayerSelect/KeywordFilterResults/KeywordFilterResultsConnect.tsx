/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  uiActions,
  mapSelectors,
  layerSelectSelectors,
  uiSelectors,
} from '../../../../store';
import { AppStore } from '../../../../types/types';
import KeywordFilterResults from './KeywordFilterResults';

interface KeywordFilterResultsConnectProps {
  bounds?: string;
  showTitle?: boolean;
}

const KeywordFilterResultsConnect: React.FC<KeywordFilterResultsConnectProps> =
  ({ bounds, showTitle = false }: KeywordFilterResultsConnectProps) => {
    const dispatch = useDispatch();
    const mapId = useSelector((store) =>
      uiSelectors.getDialogMapId(store, 'keywordFilter'),
    );

    const isOpenInStore = useSelector((store) =>
      uiSelectors.getisDialogOpen(store, 'keywordFilter'),
    );

    const onClose = React.useCallback(
      () =>
        dispatch(
          uiActions.setToggleOpenDialog({
            type: 'keywordFilter',
            setOpen: false,
          }),
        ),
      [dispatch],
    );

    const isMapPresent = useSelector((store: AppStore) =>
      mapSelectors.getIsMapPresent(store, mapId),
    );

    const uiOrder = useSelector((store) =>
      uiSelectors.getDialogOrder(store, 'keywordFilter'),
    );

    const uiSource = useSelector((store: AppStore) =>
      uiSelectors.getDialogSource(store, 'keywordFilter'),
    );

    const uiIsOrderedOnTop = useSelector((store) =>
      uiSelectors.getDialogIsOrderedOnTop(store, 'keywordFilter'),
    );

    // Check to ensure the currently active map is still present on screen - if not, close the dialog
    React.useEffect(() => {
      if (mapId !== '' && !isMapPresent) {
        onClose();
      }
    }, [mapId, isMapPresent, onClose]);

    const registerDialog = React.useCallback(
      () =>
        dispatch(
          uiActions.registerDialog({
            type: 'keywordFilter',
            setOpen: false,
          }),
        ),
      [dispatch],
    );
    const unregisterDialog = React.useCallback(
      () =>
        dispatch(
          uiActions.unregisterDialog({
            type: 'keywordFilter',
          }),
        ),
      [dispatch],
    );

    const onOrderDialog = React.useCallback(() => {
      if (!uiIsOrderedOnTop) {
        dispatch(
          uiActions.orderDialog({
            type: 'keywordFilter',
          }),
        );
      }
    }, [dispatch, uiIsOrderedOnTop]);

    // Register this dialog in the store
    React.useEffect(() => {
      registerDialog();
      return (): void => {
        unregisterDialog();
      };
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const allKeywordIds = useSelector((store) =>
      layerSelectSelectors.getAllKeywordIds(store),
    );

    return (
      <KeywordFilterResults
        allKeywordIds={allKeywordIds}
        mapId={mapId}
        bounds={bounds}
        isOpen={isOpenInStore}
        onClose={onClose}
        showTitle={showTitle}
        onMouseDown={onOrderDialog}
        order={uiOrder}
        source={uiSource}
      />
    );
  };

export default KeywordFilterResultsConnect;
