/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Checkbox,
  ListItemIcon,
  ListItem,
  ListItemSecondaryAction,
  IconButton,
  Typography,
} from '@mui/material';

import { KeywordObject } from '../../../../store/layerSelect/types';

interface FilterListItemProps {
  keyword: KeywordObject;
  toggleKeywords: (keywords: string[]) => void;
  enableOnlyOneKeyword: (keyword: string) => void;
}

const FilterListItem: React.FC<FilterListItemProps> = ({
  keyword,
  toggleKeywords,
  enableOnlyOneKeyword,
}) => {
  const [hovering, setHovering] = React.useState(false);
  const handleMouseOver = React.useCallback(() => {
    setHovering(true);
  }, []);

  const handleMouseLeave = React.useCallback(() => {
    setHovering(false);
  }, []);

  const selectOnlyOne = (keyword): void => {
    enableOnlyOneKeyword(keyword);
  };

  const toggleCheckbox = (keyword): void => {
    toggleKeywords([keyword]);
  };

  if (keyword.amountVisible !== 0) {
    return (
      <div
        onMouseOver={handleMouseOver}
        onMouseLeave={handleMouseLeave}
        onFocus={handleMouseOver}
        onBlur={handleMouseLeave}
        data-testid="filterResultListItem"
      >
        <ListItem
          sx={{
            width: '100%',
            minWidth: '300px',
            padding: '0px',
            paddingLeft: '10px',
            paddingRight: '50px',
            fontSize: '12px',
          }}
        >
          <ListItemIcon sx={{ minWidth: '42px' }}>
            <Checkbox
              color="secondary"
              checked={keyword.checked}
              onChange={(): void => toggleCheckbox(keyword.id)}
              sx={{
                paddingTop: '6px',
                paddingBottom: '7px',
                '&&:hover': {
                  backgroundColor: 'transparent',
                },
              }}
            />
          </ListItemIcon>
          <Typography
            variant="body2"
            sx={{
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap',
              overflow: 'hidden',
            }}
          >
            {keyword.id}
          </Typography>
          {hovering ? (
            <ListItemSecondaryAction>
              <IconButton
                style={{ backgroundColor: 'transparent' }}
                edge="end"
                size="small"
                onClick={(): void => selectOnlyOne(keyword.id)}
                data-testid="onlyButton"
              >
                <Typography
                  sx={{
                    color: 'geowebColors.buttons.primary.default.fill',
                    fontSize: '14px',
                    fontWeight: 500,
                  }}
                >
                  ONLY
                </Typography>
              </IconButton>
            </ListItemSecondaryAction>
          ) : null}
        </ListItem>
      </div>
    );
  }
  return null;
};

export default FilterListItem;
