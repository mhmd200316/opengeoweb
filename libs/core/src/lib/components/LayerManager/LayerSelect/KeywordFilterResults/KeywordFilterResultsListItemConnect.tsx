/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { layerSelectActions } from '../../../../store';
import * as layerSelectSelectors from '../../../../store/layerSelect/selectors';
import KeywordFilterResultsListItem from './KeywordFilterResultsListItem';

interface KeywordFilterResultsListItemConnectProps {
  keyword: string;
}

const KeywordFilterResultsListItemConnect: React.FC<KeywordFilterResultsListItemConnectProps> =
  ({ keyword }: KeywordFilterResultsListItemConnectProps) => {
    const dispatch = useDispatch();

    const toggleKeywords = React.useCallback(
      (keywords: string[]) => {
        dispatch(layerSelectActions.toggleKeywords({ keywords }));
      },
      [dispatch],
    );

    const enableOnlyOneKeyword = React.useCallback(
      (keyword: string) => {
        dispatch(layerSelectActions.enableOnlyOneKeyword({ keyword }));
      },
      [dispatch],
    );

    const keywordObject = useSelector((store) =>
      layerSelectSelectors.getKeywordObjectById(store, keyword),
    );

    return (
      <KeywordFilterResultsListItem
        keyword={keywordObject}
        toggleKeywords={toggleKeywords}
        enableOnlyOneKeyword={enableOnlyOneKeyword}
      />
    );
  };

export default KeywordFilterResultsListItemConnect;
