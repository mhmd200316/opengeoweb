/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import KeywordFilterResults from './KeywordFilterResults';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';

describe('src/components/LayerSelect/KeywordFilterResults', () => {
  const props = {
    allKeywordIds: ['keyword1', 'keyword2'],
    mapId: 'mapId-1',
    isOpen: true,
  };
  const mockStore = configureStore();
  const store = mockStore({
    layerSelect: {
      filters: {
        keywords: {
          byId: {
            keyword1: {
              id: 'keyword1',
              amount: 1,
              amountVisible: 1,
              checked: true,
            },
            keyword2: {
              id: 'keyword2',
              amount: 1,
              amountVisible: 1,
              checked: true,
            },
          },
          allIds: ['keyword1', 'keyword2'],
        },
      },
    },
    ui: {
      dialogs: {
        keywordFilter: {
          isOpen: true,
        },
      },
    },
  });
  store.addModules = jest.fn(); // mocking the dynamic module loader
  it('should render the component', () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterResults {...props} />
      </CoreThemeStoreProvider>,
    );

    const component = queryByTestId('keywordFilterResults');
    expect(component).toBeTruthy();
  });

  it('should toggle all checkboxes', () => {
    const { queryByTestId, queryAllByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterResults {...props} />
      </CoreThemeStoreProvider>,
    );

    const customSwitch = queryByTestId('customSwitch');

    const checkboxes = queryAllByRole('checkbox') as HTMLInputElement[];
    checkboxes.forEach((checkbox) => {
      expect(checkbox.checked).toBe(true);
    });

    fireEvent.click(customSwitch);
    setTimeout(() => {
      const checkboxes = queryAllByRole('checkbox') as HTMLInputElement[];
      checkboxes.forEach((checkbox) => {
        expect(checkbox.checked).toBe(false);
      });
    }, 2000);

    fireEvent.click(customSwitch);
    setTimeout(() => {
      const checkboxes = queryAllByRole('checkbox') as HTMLInputElement[];
      checkboxes.forEach((checkbox) => {
        expect(checkbox.checked).toBe(true);
      });
    }, 2000);
  });

  it('should toggle only one checkbox to true', async () => {
    const { queryAllByRole, queryByTestId, queryAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterResults {...props} />
      </CoreThemeStoreProvider>,
    );

    const filterResultsListItems = queryAllByTestId('filterResultListItem');
    fireEvent.mouseOver(filterResultsListItems[0]);
    fireEvent.focus(filterResultsListItems[0]);

    await waitFor(() => {
      const onlyButton = queryByTestId('onlyButton');
      fireEvent.click(onlyButton);
    });

    setTimeout(() => {
      const checkboxes = queryAllByRole('checkbox') as HTMLInputElement[];
      checkboxes.forEach((checkbox, index) => {
        if (index === 0) {
          expect(checkbox.checked).toBe(true);
        } else {
          expect(checkbox.checked).toBe(false);
        }
      });
    }, 2000);
  });
});
