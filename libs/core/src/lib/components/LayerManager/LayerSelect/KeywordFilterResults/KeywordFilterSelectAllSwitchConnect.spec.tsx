/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import KeywordFilterSelectAllSwitchConnect from './KeywordFilterSelectAllSwitchConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { layerSelectActions } from '../../../../store';

describe('src/components/LayerSelect/KeywordFilterSelectAllSwitchConnect', () => {
  const mockStore = configureStore();
  const store = mockStore({
    layerSelect: {
      filters: {
        keywords: {
          byId: {
            keyword1: {
              id: 'keyword1',
              amount: 1,
              amountVisible: 1,
              checked: true,
            },
            keyword2: {
              id: 'keyword2',
              amount: 1,
              amountVisible: 1,
              checked: false,
            },
          },
          allIds: ['keyword1', 'keyword2'],
        },
      },
    },
    ui: {
      dialogs: {
        keywordFilter: {
          isOpen: true,
        },
      },
    },
  });
  const storeAllTrue = mockStore({
    layerSelect: {
      filters: {
        keywords: {
          byId: {
            keyword1: {
              id: 'keyword1',
              amount: 1,
              amountVisible: 1,
              checked: true,
            },
            keyword2: {
              id: 'keyword2',
              amount: 1,
              amountVisible: 1,
              checked: true,
            },
          },
          allIds: ['keyword1', 'keyword2'],
        },
      },
    },
    ui: {
      dialogs: {
        keywordFilter: {
          isOpen: true,
        },
      },
    },
  });
  store.addModules = jest.fn(); // mocking the dynamic module loader
  storeAllTrue.addModules = jest.fn(); // mocking the dynamic module loader
  it('should render the component', () => {
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterSelectAllSwitchConnect />
      </CoreThemeStoreProvider>,
    );

    const component = getByTestId('customSwitch');
    expect(component).toBeTruthy();
  });

  it('should call action with all false keywords to toggle all true', () => {
    const { queryAllByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterSelectAllSwitchConnect />
      </CoreThemeStoreProvider>,
    );

    const customSwitch = queryAllByRole('checkbox')[0] as HTMLInputElement;
    fireEvent.click(customSwitch);
    const expectedActionToTrue = [
      layerSelectActions.toggleKeywords({
        keywords: ['keyword2'],
      }),
    ];
    expect(store.getActions()).toEqual(expectedActionToTrue);
  });

  it('should call action with all keywords to toggle all false when all are true', () => {
    const { queryAllByRole } = render(
      <CoreThemeStoreProvider store={storeAllTrue}>
        <KeywordFilterSelectAllSwitchConnect />
      </CoreThemeStoreProvider>,
    );

    const customSwitch = queryAllByRole('checkbox')[0] as HTMLInputElement;
    fireEvent.click(customSwitch);
    const expectedActionToFalse = [
      layerSelectActions.toggleKeywords({
        keywords: ['keyword1', 'keyword2'],
      }),
    ];
    expect(storeAllTrue.getActions()).toEqual(expectedActionToFalse);
  });
});
