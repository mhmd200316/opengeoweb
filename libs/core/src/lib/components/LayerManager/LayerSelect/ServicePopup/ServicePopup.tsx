/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Button,
  Box,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  SxProps,
  TextField,
  Theme,
  Typography,
} from '@mui/material';

import { Close } from '@opengeoweb/theme';
import { WMGetServiceFromStore } from '@opengeoweb/webmap';
import { getLayersFromService } from '../../../../utils/getCapabilities';

import {
  SetLayersForServicePayload,
  ServiceLayer,
} from '../../../../store/mapStore/types';
import { ActiveServices } from '../../../../store/layerSelect/types';

const style = {
  dialogTitle: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '16px 16px 20px 16px',
    ' span': {
      fontSize: '2.125rem',
    },
  },
  container: (theme: Theme): SxProps<Theme> => ({
    '& .MuiDialog-paper': {
      backgroundColor: theme.palette.geowebColors.cards.cardContainer,
    },
  }),
  closeBtn: (theme: Theme): SxProps<Theme> => ({
    color: theme.palette.geowebColors.captions.captionDisabled.color,
    marginTop: 'auto',
    marginBottom: 'auto',
  }),
  textField: {
    width: '100%',
  },
  dialogContent: {
    padding: '8px 16px',
  },
  footerContent: {
    padding: '16px',
  },
};

interface ServicePopupProps {
  servicePopupVariant: 'edit' | 'add' | 'save';
  hideBackdrop?: boolean;
  isOpen?: boolean;
  closePopup?: () => void;
  serviceId?: string;
  url?: string;
  services?: ActiveServices;
  serviceSetLayers: (payload: SetLayersForServicePayload) => void;
}

export const ADD_HEADING = 'Add a new service';
export const EDIT_HEADING = 'Edit service';
export const SAVE_HEADING = 'Save service';

const getTitleForVariant = (
  variant: ServicePopupProps['servicePopupVariant'],
): string => {
  switch (variant) {
    case 'add':
      return ADD_HEADING;
    case 'edit':
      return EDIT_HEADING;
    default:
      return SAVE_HEADING;
  }
};

const ServicePopup: React.FC<ServicePopupProps> = ({
  servicePopupVariant,
  hideBackdrop = false,
  isOpen,
  closePopup,
  serviceId,
  url,
  services,
  serviceSetLayers,
}: ServicePopupProps) => {
  const [serviceName, setServiceName] = React.useState('');
  const [serviceUrl, setServiceUrl] = React.useState('');
  const [abstract, setAbstract] = React.useState('');
  const [disableButton, setDisableButton] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const [serviceNameHasError, setServiceNameHasError] = React.useState(false);
  const [serviceNameHelperText, setServiceNameHelperText] = React.useState('');
  const [serviceUrlHasError, setServiceUrlHasError] = React.useState(false);
  const [serviceUrlHelperText, setServiceUrlHelperText] = React.useState('');

  React.useEffect(() => {
    if (servicePopupVariant === 'edit') {
      setServiceName(services[serviceId]?.serviceName || '');
      setServiceUrl(url || '');
    }
    if (servicePopupVariant === 'save') {
      setServiceUrl(url || '');
    }
  }, [serviceId, services, url, servicePopupVariant, isOpen]);

  const validateServiceUrl = (url: string): boolean => {
    if (url === '' || url === null || typeof url === 'undefined') return false;
    const matcher = /^(?:\w+:)?\/\/([^\s.]+\.\S{2}|[0-9a-zA-Z]+[:?\d]*)\S*$/;
    if (!matcher.test(url)) return false;
    return true;
  };

  const addNewService = async (): Promise<void> => {
    setServiceUrlHasError(false);
    if (validateServiceUrl(serviceUrl)) {
      setIsLoading(true);
      try {
        const layers = await getLayersFromService(serviceUrl);
        if (layers.children) {
          const serviceLayers = [] as ServiceLayer[];
          for (const object of layers.children) {
            serviceLayers.push({
              name: object.name,
              text: object.title,
              leaf: object.leaf,
              path: object.path as string[],
              keywords: object.keywords ? object.keywords : [],
              abstract: object.abstract ? object.abstract : '',
              styles: object.styles ? object.styles : [],
            });
          }
          const service = WMGetServiceFromStore(serviceUrl);
          serviceSetLayers({
            id: service.id,
            name: serviceName,
            serviceUrl,
            layers: serviceLayers,
            isUserAddedService: true,
          });
          setIsLoading(false);
          closePopupAndResetState();
        }
      } catch (error) {
        setIsLoading(false);
        setServiceUrlHasError(true);
        setServiceUrlHelperText('Error: Download failed. Check the URL.');
      }
    }
  };

  const ServiceNameInRedux = (): boolean => {
    if (servicePopupVariant === 'add' || servicePopupVariant === 'save') {
      const foundName = Object.keys(services).find(
        (serviceId) =>
          services[serviceId].serviceName.toLowerCase() ===
          serviceName?.toLowerCase(),
      );
      return foundName !== undefined;
    }
    return false;
  };

  const UrlInRedux = (): boolean => {
    if (servicePopupVariant === 'add' || servicePopupVariant === 'save') {
      const foundUrl = Object.keys(services).find(
        (serviceId) => services[serviceId].serviceUrl === serviceUrl,
      );
      return foundUrl !== undefined;
    }
    return false;
  };

  // Check serviceName
  React.useEffect(() => {
    if (serviceName === undefined) return;
    setServiceNameHelperText('');
    if (ServiceNameInRedux()) {
      setServiceNameHasError(true);
      setServiceNameHelperText(
        'Error: This name already exists. Choose another name.',
      );
    } else {
      setServiceNameHasError(false);
      setServiceNameHelperText('');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [serviceName]);

  // Check Url
  React.useEffect(() => {
    setServiceUrlHelperText('');
    if (
      serviceUrl &&
      serviceUrl.length > 0 &&
      !validateServiceUrl(serviceUrl)
    ) {
      setServiceUrlHasError(true);
      setServiceUrlHelperText('Error: Not a valid URL.');
    } else if (UrlInRedux()) {
      setServiceUrlHasError(true);
      setServiceUrlHelperText('Error: URL already exists.');
    } else {
      setServiceUrlHasError(false);
      setServiceNameHelperText('');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [serviceUrl]);

  // Check if all fields are valid

  React.useEffect(() => {
    if (
      !serviceNameHasError &&
      !serviceUrlHasError &&
      validateServiceUrl(serviceUrl) &&
      serviceName.length > 0
    ) {
      setDisableButton(false);
    } else setDisableButton(true);
  }, [serviceNameHasError, serviceUrlHasError, serviceName, serviceUrl]);

  const closePopupAndResetState = (): void => {
    setServiceName('');
    setServiceUrl('');
    setAbstract('');
    closePopup();
  };

  return (
    <Dialog
      open={isOpen}
      hideBackdrop={hideBackdrop}
      fullWidth
      maxWidth="xs"
      data-testid="servicePopup"
      sx={style.container as SxProps<Theme>}
    >
      <DialogTitle sx={style.dialogTitle}>
        <Typography variant="h5" component="span">
          {getTitleForVariant(servicePopupVariant)}
        </Typography>
        <IconButton
          size="small"
          onClick={closePopupAndResetState}
          data-testid="closePopupButtonCross"
          sx={style.closeBtn as SxProps<Theme>}
        >
          <Close />
        </IconButton>
      </DialogTitle>
      <DialogContent sx={style.dialogContent}>
        <TextField
          sx={style.textField}
          variant="filled"
          label="Service name"
          onChange={(event): void => setServiceName(event.target.value)}
          value={serviceName}
          inputProps={{ 'data-testid': 'serviceNameTextField' }}
          error={serviceNameHasError}
          helperText={serviceNameHelperText}
        />
      </DialogContent>
      <DialogContent sx={style.dialogContent}>
        <TextField
          sx={style.textField}
          variant="filled"
          label="Service URL"
          onChange={(event): void => setServiceUrl(event.target.value)}
          value={serviceUrl}
          id="service-url"
          inputProps={{ 'data-testid': 'serviceUrlTextField' }}
          error={serviceUrlHasError}
          helperText={serviceUrlHelperText}
        />
      </DialogContent>
      <DialogContent sx={style.dialogContent}>
        <TextField
          sx={style.textField}
          variant="filled"
          multiline
          rows={4}
          label="Abstracts"
          helperText="Short description of what this service does"
          onChange={(event): void => setAbstract(event.target.value)}
          value={abstract}
          data-testid="abstractTextField"
          disabled={true}
        />
      </DialogContent>
      <DialogActions sx={style.footerContent}>
        <Button
          onClick={closePopupAndResetState}
          data-testid="closePopupButtonCancel"
          variant="tertiary"
          sx={{ width: '117px' }}
        >
          cancel
        </Button>
        {!isLoading ? (
          <Button
            variant="primary"
            disabled={disableButton}
            onClick={addNewService}
            data-testid="saveServiceButton"
            sx={{ width: '117px', marginLeft: '16px!important' }}
          >
            save
          </Button>
        ) : (
          <Box
            data-testid="loadingIndicator"
            sx={{
              width: '117px',
              height: '40px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              marginLeft: '16px!important',
            }}
          >
            <CircularProgress size={35} />
          </Box>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default ServicePopup;
