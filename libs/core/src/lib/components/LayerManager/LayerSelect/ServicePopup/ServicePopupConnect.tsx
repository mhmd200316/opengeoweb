/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppStore } from '../../../../types/types';
import { serviceActions, layerSelectSelectors } from '../../../../store';
import ServicePopup from './ServicePopup';
import { SetLayersForServicePayload } from '../../../../store/mapStore/types';

interface ServicePopupConnectProps {
  servicePopupVariant: 'edit' | 'add' | 'save';
  hideBackdrop?: boolean;
  isOpen?: boolean;
  closePopup?: () => void;
  serviceId?: string;
  url?: string;
}

const ServicePopupConnect: React.FC<ServicePopupConnectProps> = ({
  servicePopupVariant,
  hideBackdrop,
  isOpen,
  closePopup,
  serviceId,
  url,
}: ServicePopupConnectProps) => {
  const dispatch = useDispatch();

  const services = useSelector((store: AppStore) =>
    layerSelectSelectors.getActiveServices(store),
  );

  const serviceSetLayers = React.useCallback(
    (payload: SetLayersForServicePayload): void => {
      dispatch(serviceActions.serviceSetLayers(payload));
    },
    [dispatch],
  );

  return (
    <ServicePopup
      servicePopupVariant={servicePopupVariant}
      hideBackdrop={hideBackdrop}
      isOpen={isOpen}
      closePopup={closePopup}
      serviceId={serviceId}
      services={services}
      serviceSetLayers={serviceSetLayers}
      url={url}
    />
  );
};

export default ServicePopupConnect;
