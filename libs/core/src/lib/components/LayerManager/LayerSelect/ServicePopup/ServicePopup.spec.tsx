/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import ServicePopup, {
  ADD_HEADING,
  EDIT_HEADING,
  SAVE_HEADING,
} from './ServicePopup';
import { CoreThemeProvider } from '../../../Providers/Providers';

describe('src/lib/components/LayerManager/ServicePopup/ServicePopup', () => {
  const mockServices = {
    serviceid_1: {
      serviceName: 'Radar Norway',
      serviceUrl: 'https://public-wms.met.no/verportal/verportal.map?',
      enabled: true,
      keywordsPerService: [],
      isUserAddedService: true,
    },
  };

  it('should render correctly', () => {
    const { getByRole } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="add"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          serviceId="serviceid_1"
          services={mockServices}
        />
      </CoreThemeProvider>,
    );
    expect(getByRole('dialog').textContent).toBeTruthy();
  });

  it('should render the add title when using the add variant', () => {
    const { getByRole } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="add"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          serviceId="serviceid_1"
          services={mockServices}
        />
      </CoreThemeProvider>,
    );
    expect(getByRole('heading').textContent).toBe(ADD_HEADING);
  });

  it('should render the edit title when using the edit variant', () => {
    const { getByRole } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="edit"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          serviceId="serviceid_1"
          services={mockServices}
        />
      </CoreThemeProvider>,
    );
    expect(getByRole('heading').textContent).toBe(EDIT_HEADING);
  });

  it('should render the save title when using the save variant', () => {
    const { getByRole } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={jest.fn()}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          serviceId="serviceid_1"
          services={mockServices}
        />
      </CoreThemeProvider>,
    );
    expect(getByRole('heading').textContent).toBe(SAVE_HEADING);
  });

  it('should call the function that closes the popup on click', () => {
    const closePopup = jest.fn();
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={closePopup}
          isOpen={true}
          serviceSetLayers={jest.fn()}
          serviceId="serviceid_1"
          services={mockServices}
        />
      </CoreThemeProvider>,
    );

    fireEvent.click(queryByTestId('closePopupButtonCross'));
    expect(closePopup).toHaveBeenCalledTimes(1);

    fireEvent.click(queryByTestId('closePopupButtonCancel'));
    expect(closePopup).toBeCalledTimes(2);
  });

  it('should not show dialog when isOpen = false', () => {
    const { queryByRole } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="save"
          closePopup={jest.fn()}
          isOpen={false}
          serviceSetLayers={jest.fn()}
          serviceId="serviceid_1"
          services={mockServices}
        />
      </CoreThemeProvider>,
    );

    expect(queryByRole('dialog')).toBeFalsy();
  });

  it('should prefill the url field when passed an url as a param', async () => {
    const testUrl = 'https://google.com';
    const { findByRole } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="save"
          services={mockServices}
          isOpen
          url={testUrl}
          serviceSetLayers={jest.fn()}
        />
      </CoreThemeProvider>,
    );
    const urlField = (await findByRole('textbox', {
      name: 'Service URL',
    })) as HTMLInputElement;
    expect(urlField.value).toBe(testUrl);
  });

  it('should disable save button if url aready exists in store', async () => {
    const testUrl = 'https://public-wms.met.no/verportal/verportal.map?';

    const { getByTestId, findByTestId } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          url={testUrl}
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceId="serviceid_1"
        />
      </CoreThemeProvider>,
    );

    const serviceUrlTextField = getByTestId(
      'serviceUrlTextField',
    ) as HTMLInputElement;
    fireEvent.change(serviceUrlTextField, { target: { value: testUrl } });

    const saveButton = (await findByTestId(
      'saveServiceButton',
    )) as HTMLButtonElement;
    expect(saveButton.disabled).toBe(true);
  });

  it('should disable save button if service name aready exists in store', async () => {
    const { getByTestId, findByTestId } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceId="serviceid_1"
        />
      </CoreThemeProvider>,
    );

    const serviceNameTextField = getByTestId(
      'serviceNameTextField',
    ) as HTMLInputElement;
    fireEvent.change(serviceNameTextField, {
      target: { value: 'Radar Norway' },
    });

    const saveButton = (await findByTestId(
      'saveServiceButton',
    )) as HTMLButtonElement;
    expect(saveButton.disabled).toBe(true);
  });

  it('should display loading indicator when adding new service is in process', async () => {
    const { getByTestId, findByTestId } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceId="serviceid_1"
        />
      </CoreThemeProvider>,
    );

    const serviceNameTextField = getByTestId(
      'serviceNameTextField',
    ) as HTMLInputElement;
    fireEvent.change(serviceNameTextField, {
      target: { value: 'Precipitation Radar NL' },
    });

    const serviceUrlField = getByTestId(
      'serviceUrlTextField',
    ) as HTMLInputElement;
    fireEvent.change(serviceUrlField, {
      target: { value: 'https://geoservices.knmi.nl/wms?dataset=RADAR&' },
    });

    const saveButton = getByTestId('saveServiceButton') as HTMLButtonElement;
    fireEvent.click(saveButton);

    const loadingIndicator = await findByTestId('loadingIndicator');
    expect(loadingIndicator).toBeTruthy();
  });

  it('should show error message, if service URL is already in store', async () => {
    const { getByTestId, findByText } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceId="serviceid_1"
        />
      </CoreThemeProvider>,
    );

    const serviceUrlField = getByTestId(
      'serviceUrlTextField',
    ) as HTMLInputElement;
    fireEvent.change(serviceUrlField, {
      target: { value: 'https://public-wms.met.no/verportal/verportal.map?' },
    });

    const errorText = await findByText('Error: URL already exists.');
    expect(errorText).toBeTruthy();
  });

  it('should show error message, if service name is already in store', async () => {
    const { getByTestId, findByText } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceId="serviceid_1"
        />
      </CoreThemeProvider>,
    );

    const serviceUrlField = getByTestId(
      'serviceNameTextField',
    ) as HTMLInputElement;
    fireEvent.change(serviceUrlField, {
      target: { value: 'Radar Norway' },
    });

    const errorText = await findByText(
      'Error: This name already exists. Choose another name.',
    );
    expect(errorText).toBeTruthy();
  });

  it('should show error message, is service url is invalid', async () => {
    const { getByTestId, findByText } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceId="serviceid_1"
        />
      </CoreThemeProvider>,
    );

    const serviceUrlField = getByTestId(
      'serviceUrlTextField',
    ) as HTMLInputElement;
    fireEvent.change(serviceUrlField, {
      target: { value: 'asdasdasds' },
    });

    const errorText = await findByText('Error: Not a valid URL.');
    expect(errorText).toBeTruthy();
  });

  it('should show error message, if download fails to start', async () => {
    const { getByTestId, findByText } = render(
      <CoreThemeProvider>
        <ServicePopup
          servicePopupVariant="save"
          isOpen
          serviceSetLayers={jest.fn()}
          services={mockServices}
          serviceId="serviceid_1"
        />
      </CoreThemeProvider>,
    );

    const serviceNameTextField = getByTestId(
      'serviceNameTextField',
    ) as HTMLInputElement;
    fireEvent.change(serviceNameTextField, {
      target: { value: 'New service' },
    });

    const serviceUrlField = getByTestId(
      'serviceUrlTextField',
    ) as HTMLInputElement;
    fireEvent.change(serviceUrlField, {
      target: { value: 'https://asdasd' },
    });

    const saveButton = getByTestId('saveServiceButton') as HTMLButtonElement;
    fireEvent.click(saveButton);

    const errorText = await findByText(
      'Error: Download failed. Check the URL.',
    );
    expect(errorText).toBeTruthy();
  });
});
