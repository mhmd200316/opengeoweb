/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';

import { CoreThemeProvider } from '../../../Providers/Providers';
import ServicePopup from './ServicePopup';

export default {
  title: 'components/LayerSelect/ServicePopup',
};

const mockServices = {
  serviceid_1: {
    serviceName: 'Radar Norway',
    serviceUrl: 'https://public-wms.met.no/verportal/verportal.map?',
    enabled: true,
    keywordsPerService: [],
    isUserAddedService: true,
  },
};

export const ServicePopupLight = (): React.ReactElement => (
  <CoreThemeProvider theme={lightTheme}>
    <ServicePopup
      servicePopupVariant="add"
      hideBackdrop
      isOpen={true}
      serviceSetLayers={(): void => null}
      services={mockServices}
    />
  </CoreThemeProvider>
);

export const ServicePopupDark = (): React.ReactElement => (
  <CoreThemeProvider theme={darkTheme}>
    <ServicePopup
      servicePopupVariant="add"
      hideBackdrop
      isOpen={true}
      serviceSetLayers={(): void => null}
      services={mockServices}
    />
  </CoreThemeProvider>
);

ServicePopupLight.storyName = 'Service Popup Light Theme (takeSnapshot)';

ServicePopupDark.storyName = 'Service Popup Dark Theme (takeSnapshot)';
