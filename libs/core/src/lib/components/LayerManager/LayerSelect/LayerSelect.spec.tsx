/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import LayerSelect from './LayerSelect';

import { DialogType } from '../../../store/ui/types';
import { CoreThemeStoreProvider } from '../../Providers/Providers';

describe('src/components/LayerSelect', () => {
  const props = {
    mapId: 'mapId_1',
    isOpen: true,
    showTitle: false,
    onMouseDown: jest.fn(),
  };

  it('should render layer select window', async () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'map2',
        isOpen: false,
      },
      layerSelect: {
        type: 'layerSelect' as DialogType,
        activeMapId: 'map3',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelect {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('layerSelectWindow')).toBeTruthy();

    fireEvent.mouseDown(queryByTestId('layerSelectWindow'));
    expect(props.onMouseDown).toHaveBeenCalled();
  });
});
