/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import {
  ServiceLayer,
  Services,
} from '../../../../store/mapStore/service/types';

export const filterLayersFromService = (
  serviceId: string,
  services: Services,
  keywordIds: string[],
  allKeywordsActive: boolean,
  searchString: string,
): ServiceLayer[] => {
  const mapStoreService = services[serviceId];
  if (!mapStoreService) return [];
  if (searchString === '' && allKeywordsActive) {
    return mapStoreService.layers.filter((layer) => layer.leaf);
  }
  const searchStringArray = searchString.split(' ');
  return mapStoreService.layers
    .filter((layer) => {
      return searchStringArray.every((search) => {
        const layerPathAndKeywords = layer.path.concat(layer.keywords);
        return (
          ((!!layerPathAndKeywords &&
            !layerPathAndKeywords.every(
              (keyword) => !keywordIds.includes(keyword),
            )) ||
            allKeywordsActive) &&
          ((!!layer.name &&
            layer.name.toLowerCase().includes(search.toLowerCase())) ||
            (!!layer.text &&
              layer.text.toLowerCase().includes(search.toLowerCase())) ||
            (!!layer.abstract &&
              layer.abstract.toLowerCase().includes(search.toLowerCase())) ||
            (!!layerPathAndKeywords &&
              !layerPathAndKeywords.every(
                (keyword) =>
                  !keyword.toLowerCase().includes(search.toLowerCase()),
              )) ||
            mapStoreService.name.toLowerCase().includes(search.toLowerCase()))
        );
      });
    })
    .filter((layer) => layer.leaf);
};
