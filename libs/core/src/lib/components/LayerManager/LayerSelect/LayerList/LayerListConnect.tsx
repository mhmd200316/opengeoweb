/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { LayerType } from '@opengeoweb/webmap';
import {
  Layer,
  LayerActionOrigin,
} from '../../../../store/mapStore/layers/types';
import { AppStore } from '../../../../types/types';
import { Services } from '../../../../store/mapStore/service/types';
import * as mapServiceSelectors from '../../../../store/mapStore/service/selectors';
import { layerActions, mapSelectors } from '../../../../store';
import * as layerSelectSelectors from '../../../../store/layerSelect/selectors';
import LayerList from './LayerList';
import { generateLayerId } from '../../../../store/mapStore/utils/helpers';

interface LayerListConnectProps {
  mapId: string;
  layerSelectHeight: number;
}
const LayerListConnect: React.FC<LayerListConnectProps> = ({
  mapId,
  layerSelectHeight,
}: LayerListConnectProps) => {
  const services: Services = useSelector((store: AppStore) =>
    mapServiceSelectors.getServices(store),
  );

  const dispatch = useDispatch();

  const enabledServiceIds: string[] = useSelector((store: AppStore) =>
    layerSelectSelectors.getEnabledServiceIds(store),
  );

  const mapLayers = useSelector((store: AppStore): Layer[] =>
    mapSelectors.getMapLayers(store, mapId),
  );

  const addLayer = ({ serviceUrl, layerName }): void => {
    const layer = {
      service: serviceUrl,
      name: layerName,
      id: generateLayerId(),
      layerType: LayerType.mapLayer,
    } as Layer;

    addMapLayer({
      mapId,
      layerId: layer.id,
      layer,
      origin: LayerActionOrigin.layerManager,
    });
  };

  const addMapLayer = React.useCallback(
    ({ layerId, layer, origin }) =>
      dispatch(
        layerActions.addLayer({
          mapId,
          layerId,
          layer,
          origin,
        }),
      ),
    [dispatch, mapId],
  );

  const deleteLayer = React.useCallback(
    ({ layerId, layerIndex }) =>
      dispatch(
        layerActions.layerDelete({
          mapId,
          layerId,
          layerIndex,
          origin: LayerActionOrigin.layerManager,
        }),
      ),
    [dispatch, mapId],
  );

  const checkedKeywordIds: string[] = useSelector((store: AppStore) =>
    layerSelectSelectors.getCheckedKeywordIds(store),
  );

  const allKeywordsActive: boolean = useSelector((store: AppStore) =>
    layerSelectSelectors.isAllKeywordsChecked(store),
  );

  const searchString: string = useSelector((store: AppStore) =>
    layerSelectSelectors.getSearchFilter(store),
  );

  return (
    <LayerList
      services={services}
      serviceIds={enabledServiceIds}
      layerSelectHeight={layerSelectHeight}
      addLayer={addLayer}
      deleteLayer={deleteLayer}
      keywordIds={checkedKeywordIds}
      allKeywordsActive={allKeywordsActive}
      searchString={searchString}
      mapLayers={mapLayers}
    />
  );
};

export default LayerListConnect;
