/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import { ReduxService } from '../../../../store/mapStore/types';
import { filterLayersFromService } from './LayerListUtils';

describe('src/components/LayerManager/LayerList/LayerListUtils', () => {
  describe('filterLayersFromService', () => {
    const serviceId = 'service1';
    const layer1 = {
      name: 'layer1name',
      text: 'layer1text',
      leaf: true,
      path: ['group1'],
      abstract: 'layer1abstract',
      keywords: ['keyword1', 'keyword2'],
    };
    const layer2 = {
      name: 'layer2name',
      text: 'layer2text',
      leaf: true,
      path: [],
      abstract: 'layer2abstract',
      keywords: ['keyword2', 'keyword3'],
    };
    const layer3 = {
      name: 'layer3name',
      text: 'layer3text',
      leaf: true,
      path: [],
      abstract: 'layer3abstract',
      keywords: [],
    };
    const service1: ReduxService = {
      name: 'service1',
      serviceUrl: 'service1',
      layers: [layer1, layer2, layer3],
    };
    const services = {
      service1,
    };
    const keywordIds = ['keyword1', 'keyword2', 'keyword3'];
    const searchString = '';
    describe('searchString', () => {
      it('should return all layers when all keywords are checked and no searchString applied', () => {
        expect(
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            true,
            searchString,
          ),
        ).toEqual(services[serviceId].layers);
      });

      it('should return correct layers when searching', () => {
        const search = 'layer1';
        expect(
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            true,
            search,
          ),
        ).toEqual([layer1]);
      });

      it('should return correct layers when searching for layer text', () => {
        const search = 'layer1text';
        expect(
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            true,
            search,
          ),
        ).toEqual([layer1]);
      });

      it('should return correct layers when searching for layer name', () => {
        const search = 'layer1name';
        expect(
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            true,
            search,
          ),
        ).toEqual([layer1]);
      });

      it('should return correct layers when searching for layer abstract', () => {
        const search = 'layer1abstract';
        expect(
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            true,
            search,
          ),
        ).toEqual([layer1]);
      });

      it('should return correct layers when searching for keywords', () => {
        const search = 'keyword1';
        const search2 = 'keyword2';
        expect(
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            true,
            search,
          ),
        ).toEqual([layer1]);
        expect(
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            true,
            search2,
          ),
        ).toEqual([layer1, layer2]);
      });

      it('should return correct layers when searching with service name', () => {
        const search = 'service1';
        expect(
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            true,
            search,
          ),
        ).toEqual(services['service1'].layers);
      });

      it('should return correct layers when searching with group name', () => {
        const search = 'group';
        expect(
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            true,
            search,
          ),
        ).toEqual([layer1]);
      });

      it('should return correct layers when searching with multiple terms', () => {
        const search = 'layer1text layer1name keyword1 service1';
        expect(
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            true,
            search,
          ),
        ).toEqual([layer1]);
      });
    });
    describe('keywords', () => {
      it('should return layers with enabled keywords', () => {
        const modifiedKeywords = ['keyword1'];
        expect(
          filterLayersFromService(
            serviceId,
            services,
            modifiedKeywords,
            false,
            searchString,
          ),
        ).toEqual([layer1]);
      });

      it('should return layers with enabled keywords when search is not empty', () => {
        const search = 'layer service';
        const modifiedKeywords = ['keyword1'];
        expect(
          filterLayersFromService(
            serviceId,
            services,
            modifiedKeywords,
            false,
            search,
          ),
        ).toEqual([layer1]);
      });
    });
  });
});
