/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { CoreThemeProvider } from '../../../Providers/Providers';
import LayerAddRemoveButton from './LayerAddRemoveButton';

describe('src/components/LayerList/LayerAddRemoveButton', () => {
  const testLayers = {
    layers: [
      {
        name: 'RAD_NL25_PCP_CM',
        service: 'https://testservice',
      },
      {
        name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
        service: 'https://testservice',
      },
      {
        name: 'MULTI_DIMENSION_LAYER',
        service: 'https://testservice',
      },
    ],
  };

  const props = {
    layer: {
      name: 'RAD_NL25_PCP_CM',
      service: 'https://testservice',
    },
    layerIndex: 0,
    serviceUrl: 'https://testservice',
    addLayer: jest.fn(),
    deleteLayer: jest.fn(),
    mapLayers: testLayers.layers,
  };

  const blahProps = {
    layer: {
      name: 'BLAH BLAH',
      service: 'https://testservice',
    },
    layerIndex: 0,
    serviceUrl: 'https://testservice',
    addLayer: jest.fn(),
    deleteLayer: jest.fn(),
    mapLayers: testLayers.layers,
  };

  it('should render the button component', async () => {
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <LayerAddRemoveButton {...props} />
      </CoreThemeProvider>,
    );

    await waitFor(() => {
      expect(
        queryByTestId('layerAddRemoveButton-RAD_NL25_PCP_CM'),
      ).toBeTruthy();
    });
  });

  it('should render an "Add" button when layer given in props *cannot* be found in layers', async () => {
    expect(blahProps.mapLayers[0].name === 'RAD_NL25_PCP_CM').toBeTruthy();
    expect(blahProps.layer.name === 'BLAH BLAH').toBeTruthy();
    expect(blahProps.mapLayers[0].name === blahProps.layer.name).toBeFalsy();

    const { getByText } = render(
      <CoreThemeProvider>
        <LayerAddRemoveButton {...blahProps} />
      </CoreThemeProvider>,
    );

    const button = getByText('Add');
    await waitFor(() => {
      expect(button).toBeTruthy();
      expect(getByText('Add')).toBeTruthy();
    });
  });

  it('should render an "Remove" button when layer given in props *can* be found in layers', async () => {
    expect(props.mapLayers[0].name === 'RAD_NL25_PCP_CM').toBeTruthy();
    expect(props.layer.name === 'RAD_NL25_PCP_CM').toBeTruthy();
    expect(props.mapLayers[0].name === props.layer.name).toBeTruthy();

    const { getByText } = render(
      <CoreThemeProvider>
        <LayerAddRemoveButton {...props} />
      </CoreThemeProvider>,
    );

    const button = getByText('Remove');
    await waitFor(() => {
      expect(button).toBeTruthy();
      expect(getByText('Remove')).toBeTruthy();
    });
  });

  it('should call addLayer when "Add" button is pressed', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addModules = jest.fn(); // mocking the dynamic module loade

    const { getByText, getByTestId } = render(
      <Provider store={store}>
        <CoreThemeProvider>
          <LayerAddRemoveButton {...blahProps} />
        </CoreThemeProvider>
        ,
      </Provider>,
    );

    await waitFor(() => {
      expect(getByText('Add')).toBeTruthy();
    });

    fireEvent.click(getByTestId('layerAddRemoveButton-BLAH BLAH'));

    await waitFor(() => {
      expect(blahProps.addLayer).toHaveBeenCalled();
    });
  });

  it('should call deleteLayer when "Remove" button is pressed', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addModules = jest.fn(); // mocking the dynamic module loade

    const { getByText, getByTestId } = render(
      <Provider store={store}>
        <CoreThemeProvider>
          <LayerAddRemoveButton {...props} />
        </CoreThemeProvider>
        ,
      </Provider>,
    );

    await waitFor(() => {
      expect(getByText('Remove')).toBeTruthy();
    });

    fireEvent.click(getByTestId('layerAddRemoveButton-RAD_NL25_PCP_CM'));

    await waitFor(() => {
      expect(props.deleteLayer).toHaveBeenCalled();
    });
  });
});
