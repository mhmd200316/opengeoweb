/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box } from '@mui/material';
import { FixedSizeList as List } from 'react-window';
import { ReduxLayer } from '../../../../store/mapStore/layers/types';
import { Services } from '../../../../store/mapStore/service/types';
import { filterLayersFromService } from './LayerListUtils';
import LayerListRow from './LayerListRow';

interface LayerListProps {
  services: Services;
  serviceIds: string[];
  layerSelectHeight: number;
  addLayer: ({ serviceUrl, layerName }) => void;
  deleteLayer: ({ layerId, layerIndex }) => void;
  searchString: string;
  mapLayers: ReduxLayer[];
  keywordIds: string[];
  allKeywordsActive: boolean;
}

const LayerList: React.FC<LayerListProps> = ({
  services,
  serviceIds,
  layerSelectHeight,
  addLayer,
  deleteLayer,
  searchString,
  mapLayers,
  keywordIds,
  allKeywordsActive,
}: LayerListProps) => {
  const layerSelectFilterHeight = 199;
  const [numberOfLayers, setNumberOfLayers] = React.useState(0);
  const [layersFilteredFromService, setLayersFilteredFromService] =
    React.useState([]);

  React.useEffect(() => {
    setNumberOfLayers(
      serviceIds.reduce(
        (totalNumberOfLayers, serviceId) =>
          totalNumberOfLayers +
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            allKeywordsActive,
            searchString,
          ).length,
        0,
      ),
    );

    setLayersFilteredFromService(
      serviceIds.reduce((layerList, serviceId) => {
        return layerList.concat(
          filterLayersFromService(
            serviceId,
            services,
            keywordIds,
            allKeywordsActive,
            searchString,
          ).map((layer) => ({ ...layer, serviceId })),
        );
      }, []),
    );
  }, [serviceIds, keywordIds, services, searchString, allKeywordsActive]);

  return (
    <Box data-testid="layerList">
      <Box sx={{ marginBottom: '4px', fontSize: '12px' }}>
        {numberOfLayers} results
      </Box>
      <List
        height={layerSelectHeight - layerSelectFilterHeight}
        itemCount={numberOfLayers}
        itemSize={68}
      >
        {({ index, style }): JSX.Element => (
          <div style={style} key={layersFilteredFromService[index].name}>
            <LayerListRow
              layer={layersFilteredFromService[index]}
              layerIndex={index}
              service={services[layersFilteredFromService[index].serviceId]}
              serviceUrl={
                services[layersFilteredFromService[index].serviceId].serviceUrl
              }
              addLayer={addLayer}
              deleteLayer={deleteLayer}
              mapLayers={mapLayers}
            />
          </div>
        )}
      </List>
    </Box>
  );
};

export default LayerList;
