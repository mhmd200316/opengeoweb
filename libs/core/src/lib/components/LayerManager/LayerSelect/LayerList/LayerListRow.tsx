/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Box, Grid } from '@mui/material';
import {
  ReduxLayer,
  ServiceLayer,
  ReduxService,
} from '../../../../store/mapStore/types';
import LayerAddRemoveButton from './LayerAddRemoveButton';

interface LayerListRowProps {
  layer: ServiceLayer;
  layerIndex: number;
  service: ReduxService;
  serviceUrl: string;
  addLayer: ({ serviceUrl, layerName }) => void;
  deleteLayer: ({ layerId, layerIndex }) => void;
  mapLayers: ReduxLayer[];
}

const LayerListRow: React.FC<LayerListRowProps> = ({
  layer,
  layerIndex,
  service,
  serviceUrl,
  addLayer,
  deleteLayer,
  mapLayers,
}) => {
  return (
    <Grid
      data-testid="layerListLayerRow"
      container
      item
      xs={12}
      justifyContent="center"
      alignItems="center"
      sx={{
        backgroundColor: 'geowebColors.cards.cardContainer',
        paddingLeft: '12px',
        marginBottom: '4px',
        height: '64px',
        border: 'solid 1px',
        borderColor: 'geowebColors.cards.cardContainerBorder',
      }}
    >
      <Grid container item xs={10}>
        <Grid container item xs={12}>
          <Box
            sx={{
              fontSize: '12px',
              height: '24px',
              lineHeight: '24px',
              fontWeight: 500,
              WebkitLineClamp: 1,
              display: '-webkit-box',
              WebkitBoxOrient: 'vertical',
              textOverflow: 'ellipsis',
              overflow: 'hidden',
            }}
          >
            {layer.text}
          </Box>
        </Grid>
        <Grid container item xs={4}>
          <Box
            sx={{
              fontSize: '12px',
              height: '24px',
              lineHeight: '24px',
              WebkitLineClamp: 1,
              display: '-webkit-box',
              WebkitBoxOrient: 'vertical',
              textOverflow: 'ellipsis',
              overflow: 'hidden',
            }}
          >
            {service.name}
          </Box>
        </Grid>
        {layer.abstract ? (
          <Grid
            data-testid="layerAbstract"
            container
            item
            xs={8}
            sx={{
              fontSize: '10px',
              height: '30px',
              paddingLeft: '20px',
              WebkitLineClamp: 2,
              display: '-webkit-box',
              WebkitBoxOrient: 'vertical',
              textOverflow: 'ellipsis',
              overflow: 'hidden',
            }}
          >
            {layer.abstract}
          </Grid>
        ) : null}
      </Grid>
      <Grid container item xs={2} justifyContent="flex-end" alignItems="center">
        <Box
          sx={{
            width: '80px',
            height: '48px',
            marginRight: '8px',
            paddingTop: '4px',
          }}
        >
          <LayerAddRemoveButton
            layer={layer}
            layerIndex={layerIndex}
            serviceUrl={serviceUrl}
            addLayer={addLayer}
            deleteLayer={deleteLayer}
            mapLayers={mapLayers}
          />
        </Box>
      </Grid>
    </Grid>
  );
};

export default LayerListRow;
