/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';

import LayerList from './LayerList';
import { CoreThemeProvider } from '../../../Providers/Providers';
import {
  defaultReduxLayerRadarColor,
  defaultReduxLayerRadarKNMI,
  defaultReduxServices,
} from '../../../../utils/defaultTestSettings';

describe('src/components/LayerList', () => {
  const props = {
    layerSelectHeight: 500,
    serviceIds: ['serviceid_1'],
    services: defaultReduxServices,
    addLayer: jest.fn(),
    deleteLayer: jest.fn(),
    searchString: '',
    mapLayers: defaultReduxServices['serviceid_1'].layers,
    keywordIds: ['keyword', 'testword'],
    allKeywordsActive: true,
  };

  it('should render layer list', async () => {
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <LayerList {...props} />
      </CoreThemeProvider>,
    );

    expect(queryByTestId('layerList')).toBeTruthy();
  });

  it('should show correct amount of results', async () => {
    const { getByText, getAllByTestId } = render(
      <CoreThemeProvider>
        <LayerList {...props} />
      </CoreThemeProvider>,
    );

    expect(getByText('3 results')).toBeTruthy();
    expect(getAllByTestId('layerListLayerRow').length).toEqual(3);
  });

  it('layer list layer rows should show correct content', async () => {
    const { getByText, getAllByText } = render(
      <CoreThemeProvider>
        <LayerList {...props} />
      </CoreThemeProvider>,
    );

    expect(getAllByText(defaultReduxServices['serviceid_1'].name)).toBeTruthy();
    expect(getByText('RADAR NL COLOR')).toBeTruthy();
    expect(getByText('RADAR NL COLOR abstract')).toBeTruthy();
  });

  it('abstract text should be visible only when abstract is provided', async () => {
    const { getAllByTestId } = render(
      <CoreThemeProvider>
        <LayerList {...props} />
      </CoreThemeProvider>,
    );

    expect(getAllByTestId('layerListLayerRow').length).toEqual(3);
    expect(getAllByTestId('layerAbstract').length).toEqual(2);
  });

  it('should filter layers with simple searchString', async () => {
    props.searchString = 'radar';
    const { getByText, getAllByText } = render(
      <CoreThemeProvider>
        <LayerList {...props} />
      </CoreThemeProvider>,
    );
    expect(getByText('2 results')).toBeTruthy();
    expect(getAllByText(defaultReduxServices['serviceid_1'].name)).toBeTruthy();
    expect(getAllByText('RADAR NL KNMI')).toBeTruthy();
  });

  it('should render three layers each of them with "Add" buttons shown when no layers are provided', async () => {
    const props = {
      layerSelectHeight: 500,
      serviceIds: ['serviceid_1'],
      services: defaultReduxServices,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      searchString: '',
      mapLayers: [],
      keywordIds: ['keyword', 'testword'],
      allKeywordsActive: true,
    };

    const { getByText, getAllByRole, findAllByText } = render(
      <CoreThemeProvider>
        <LayerList {...props} />,
      </CoreThemeProvider>,
    );

    expect(getByText('3 results')).toBeTruthy();
    expect(getAllByRole('button').length).toEqual(3);
    expect((await findAllByText('Add')).length).toEqual(3);
  });

  it('should render three layers: one layer with "Add" button and two layers with "Remove" button shown', async () => {
    const props = {
      layerSelectHeight: 500,
      serviceIds: ['serviceid_1'],
      services: defaultReduxServices,
      addLayer: jest.fn(),
      deleteLayer: jest.fn(),
      searchString: '',
      mapLayers: [defaultReduxLayerRadarColor, defaultReduxLayerRadarKNMI],
      keywordIds: ['keyword', 'testword'],
      allKeywordsActive: true,
    };

    const { getByText, getAllByRole, findAllByText } = render(
      <CoreThemeProvider>
        <LayerList {...props} />,
      </CoreThemeProvider>,
    );

    expect(getByText('3 results')).toBeTruthy();
    // expect(getAllByTestId('layerAddRemove').length).toEqual(3);
    expect(getAllByRole('button').length).toEqual(3);
    expect((await findAllByText('Add')).length).toEqual(1);
    expect((await findAllByText('Remove')).length).toEqual(2);
  });

  it('should filter layers with searchString about keywords', async () => {
    props.searchString = 'keyword';
    const { getByText } = render(
      <CoreThemeProvider>
        <LayerList {...props} />
      </CoreThemeProvider>,
    );
    expect(getByText('1 results')).toBeTruthy();
    expect(getByText(defaultReduxServices['serviceid_1'].name)).toBeTruthy();
  });

  it('should filter layers with searchString that includes spaces', async () => {
    props.searchString = 'radar nl25';
    const { getByText } = render(
      <CoreThemeProvider>
        <LayerList {...props} />
      </CoreThemeProvider>,
    );
    expect(getByText('1 results')).toBeTruthy();
    expect(getByText(defaultReduxServices['serviceid_1'].name)).toBeTruthy();
  });

  it('should filter layers with searchString that includes service name', async () => {
    props.searchString = 'testservice';
    const { getByText, getAllByText } = render(
      <CoreThemeProvider>
        <LayerList {...props} />
      </CoreThemeProvider>,
    );
    expect(getByText('3 results')).toBeTruthy();
    expect(getAllByText(defaultReduxServices['serviceid_1'].name)).toBeTruthy();
    expect(getByText('RADAR NL KNMI')).toBeTruthy();
    expect(getByText('Multi Dimension Layer')).toBeTruthy();
  });

  it('should filter layers with chosen keywords when not all keywords are checked', async () => {
    props.keywordIds = ['keyword'];
    props.allKeywordsActive = false;
    const { getByText } = render(
      <CoreThemeProvider>
        <LayerList {...props} />
      </CoreThemeProvider>,
    );
    expect(getByText('1 results')).toBeTruthy();
    expect(getByText(defaultReduxServices['serviceid_1'].name)).toBeTruthy();
  });
});
