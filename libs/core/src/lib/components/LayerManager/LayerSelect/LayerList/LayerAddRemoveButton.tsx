/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { CustomToggleButton } from '@opengeoweb/shared';
import { ReduxLayer } from '../../../../store/mapStore/layers/types';

interface LayerAddRemoveButtonProps {
  layer: ReduxLayer;
  layerIndex: number;
  serviceUrl: string;
  addLayer: ({ serviceUrl, layerName }) => void;
  deleteLayer: ({ layerId, layerIndex }) => void;
  mapLayers: ReduxLayer[];
}

const LayerAddRemoveButton: React.FC<LayerAddRemoveButtonProps> = ({
  layer,
  layerIndex,
  serviceUrl,
  addLayer,
  deleteLayer,
  mapLayers,
}: LayerAddRemoveButtonProps) => {
  const foundLayer: ReduxLayer = mapLayers.find(
    (lr) => lr.name === layer.name && lr.service === serviceUrl,
  );

  return (
    <CustomToggleButton
      data-testid={`layerAddRemoveButton-${layer.name}`}
      onClick={(): void => {
        !foundLayer
          ? addLayer({ serviceUrl, layerName: layer.name })
          : deleteLayer({ layerId: foundLayer.id, layerIndex });
      }}
      selected={!!foundLayer}
      variant="tool"
      fullWidth={true}
    >
      {!foundLayer ? 'Add' : 'Remove'}
    </CustomToggleButton>
  );
};

export default LayerAddRemoveButton;
