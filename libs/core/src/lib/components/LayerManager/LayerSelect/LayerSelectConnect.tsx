/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useIsMounted } from '@opengeoweb/shared';

import { WMGetServiceFromStore } from '@opengeoweb/webmap';
import {
  uiActions,
  serviceActions,
  mapSelectors,
  uiSelectors,
} from '../../../store';
import { AppStore } from '../../../types/types';
import LayerSelect from './LayerSelect';
import { preloadedDefaultMapServices } from '../../../utils/defaultConfigurations';
import { getLayersFlattenedFromService } from '../../../utils/getCapabilities';
import KeywordFilterResultsConnect from './KeywordFilterResults/KeywordFilterResultsConnect';
import { getUserAddedServices } from '../../../utils/localStorage';
import { mergePresetsAndUserAddedServices } from './LayerSelectUtils';
import { Service } from '../../WMSLoader/services';
import { SetLayersForServicePayload } from '../../../store/mapStore/types';
import { NoIdService } from '../../../utils/types';

interface LayerSelectConnectProps {
  bounds?: string;
  showTitle?: boolean;
  preloadedServices?: Service[];
}

/**
 * LayerSelectConnect is a component that is opened from LayerManager
 * It's used to add Services and Layers to the application
 * It doesn't expect any props, its subcomponents only read and write to redux store
 *
 * ``` <LayerSelectConnect />```
 */
const LayerSelectConnect: React.FC<LayerSelectConnectProps> = ({
  bounds,
  showTitle = false,
  preloadedServices = preloadedDefaultMapServices,
}: LayerSelectConnectProps) => {
  const dispatch = useDispatch();
  const { isMounted } = useIsMounted();

  const mapId = useSelector((store) =>
    uiSelectors.getDialogMapId(store, 'layerSelect'),
  );

  const isOpenInStore = useSelector((store) =>
    uiSelectors.getisDialogOpen(store, 'layerSelect'),
  );

  const onClose = React.useCallback(
    () =>
      dispatch(
        uiActions.setToggleOpenDialog({
          type: 'layerSelect',
          setOpen: false,
        }),
      ),
    [dispatch],
  );

  const isMapPresent = useSelector((store: AppStore) =>
    mapSelectors.getIsMapPresent(store, mapId),
  );

  const uiOrder = useSelector((store) =>
    uiSelectors.getDialogOrder(store, 'layerSelect'),
  );

  const uiSource = useSelector((store: AppStore) =>
    uiSelectors.getDialogSource(store, 'layerSelect'),
  );

  const uiIsOrderedOnTop = useSelector((store) =>
    uiSelectors.getDialogIsOrderedOnTop(store, 'layerSelect'),
  );

  // Check to ensure the currently active map is still present on screen - if not, close the dialog
  React.useEffect(() => {
    if (mapId !== '' && !isMapPresent) {
      onClose();
    }
  }, [mapId, isMapPresent, onClose]);

  const registerDialog = React.useCallback(
    () =>
      dispatch(
        uiActions.registerDialog({
          type: 'layerSelect',
          setOpen: false,
        }),
      ),
    [dispatch],
  );
  const unregisterDialog = React.useCallback(
    () =>
      dispatch(
        uiActions.unregisterDialog({
          type: 'layerSelect',
        }),
      ),
    [dispatch],
  );

  const onOrderDialog = React.useCallback(() => {
    if (!uiIsOrderedOnTop) {
      dispatch(
        uiActions.orderDialog({
          type: 'layerSelect',
        }),
      );
    }
  }, [dispatch, uiIsOrderedOnTop]);

  // Register this dialog in the store
  React.useEffect(() => {
    registerDialog();
    return (): void => {
      unregisterDialog();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const serviceSetLayers = React.useCallback(
    (payload: SetLayersForServicePayload): void => {
      dispatch(serviceActions.serviceSetLayers(payload));
    },
    [dispatch],
  );

  const checkIfUserAddedService = (service: NoIdService): boolean => {
    return !!service.isUserAddedService;
  };

  React.useEffect(() => {
    if (isOpenInStore && preloadedServices && preloadedServices.length > 0) {
      mergePresetsAndUserAddedServices(
        preloadedServices,
        getUserAddedServices(),
      ).forEach((service) => {
        getLayersFlattenedFromService(service.url)
          .then((layers) => {
            if (isMounted.current) {
              const wmService = WMGetServiceFromStore(service.url);
              serviceSetLayers({
                id: wmService.id,
                name: service.name,
                serviceUrl: service.url,
                layers,
                isUserAddedService: checkIfUserAddedService(service),
              });
            }
          })
          .catch(() => null);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isOpenInStore, preloadedServices, serviceSetLayers]);

  return (
    <>
      <LayerSelect
        mapId={mapId}
        bounds={bounds}
        isOpen={isOpenInStore}
        onClose={onClose}
        showTitle={showTitle}
        onMouseDown={onOrderDialog}
        order={uiOrder}
        source={uiSource}
      />
      <KeywordFilterResultsConnect />
    </>
  );
};

export default LayerSelectConnect;
