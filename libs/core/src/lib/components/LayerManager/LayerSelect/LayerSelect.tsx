/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { ToolContainerDraggable } from '@opengeoweb/shared';
import * as React from 'react';
import { Box } from '@mui/material';
import { Source } from '../../../store/ui/types';
import KeywordFilterButtonConnect from './KeywordFilterButton/KeywordFilterButtonConnect';
import SearchFieldConnect from './SearchField/SearchFieldConnect';
import LayerListConnect from './LayerList/LayerListConnect';
import ServiceListConnect from './ServiceList/ServiceListConnect';
import ServiceOptionsButton from './ServiceOptionsButton/ServiceOptionsButton';

interface LayerSelectProps {
  mapId: string;
  bounds?: string;
  showTitle?: boolean;
  onClose?: () => void;
  onMouseDown?: () => void;
  isOpen: boolean;
  order?: number;
  source?: Source;
}

const LayerSelect: React.FC<LayerSelectProps> = ({
  mapId,
  bounds,
  onClose = (): void => {},
  showTitle,
  isOpen,
  onMouseDown = (): void => {},
  order = 0,
  source = 'module',
}: LayerSelectProps) => {
  const [height, setHeight] = React.useState(500);
  const [width, setWidth] = React.useState(750);
  const onChangeSize = ({ height, width }): void => {
    setHeight(height);
    setWidth(width);
  };
  return (
    <ToolContainerDraggable
      title={showTitle ? `Layer Select ${mapId}` : 'Layer Select'}
      startSize={{ width, height }}
      minWidth={390}
      minHeight={126}
      startPosition={{ top: 150, left: 100 }}
      isOpen={isOpen}
      onChangeSize={onChangeSize}
      onClose={onClose}
      headerSize="small"
      bounds={bounds}
      data-testid="layerSelectWindow"
      onMouseDown={onMouseDown}
      order={order}
      source={source}
    >
      <Box sx={{ padding: '6px' }}>
        <SearchFieldConnect />
      </Box>
      <Box
        sx={{
          float: 'left',
          position: 'absolute',
          margin: '12px 0 0 8px',
          zIndex: 2,
        }}
      >
        <KeywordFilterButtonConnect mapId={mapId} />
      </Box>
      <Box
        sx={{
          position: 'absolute',
          margin: '12px 0 0 calc(100% - 36px)',
          zIndex: 2,
        }}
      >
        <ServiceOptionsButton />
      </Box>
      <Box
        sx={{
          height: '48px',
        }}
      >
        <ServiceListConnect layerSelectWidth={width} />
      </Box>
      <Box
        sx={{
          padding: '0 8px',
        }}
      >
        <LayerListConnect mapId={mapId} layerSelectHeight={height} />
      </Box>
    </ToolContainerDraggable>
  );
};

export default LayerSelect;
