/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';

import configureStore from 'redux-mock-store';
import { lightTheme } from '@opengeoweb/theme';
import { store } from '../../../../storybookUtils/store';
import KeywordFilterButtonConnect from './KeywordFilterButtonConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { DialogType } from '../../../../store/ui/types';
import { uiActions } from '../../../../store';

const props = {
  mapId: 'mapid-1',
};
describe('src/components/LayerSelect/KeywordFilterButton', () => {
  it('should render the component', () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider theme={lightTheme} store={store}>
        <KeywordFilterButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const button = queryByTestId('keywordFilterButton');
    expect(button).toBeTruthy();
  });

  it('should dispatch action with passed in mapid when clicked', () => {
    const mockStore = configureStore();
    const mockState = {
      keywordFilter: {
        type: 'keywordFilter' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('keywordFilterButton')).toBeTruthy();

    // open the keyword filter dialog
    fireEvent.click(getByTestId('keywordFilterButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'keywordFilter',
        mapId: props.mapId,
        setOpen: true,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch action to close dialog when dialog is already opened for corresponding map', () => {
    const mockStore = configureStore();
    const mockState = {
      dialogs: {
        keywordFilter: {
          type: 'keywordFilter' as DialogType,
          activeMapId: 'mapId_123',
          isOpen: true,
          source: 'app',
        },
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      mapId: 'mapId_123',
    };
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <KeywordFilterButtonConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('keywordFilterButton')).toBeTruthy();

    // close the keyword filter dialog
    fireEvent.click(getByTestId('keywordFilterButton'));
    const expectedAction = [
      uiActions.setActiveMapIdForDialog({
        type: 'keywordFilter',
        mapId: props.mapId,
        setOpen: false,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
