/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { CustomToggleButton } from '@opengeoweb/shared';
import { ActiveServiceObject } from '../../../../store/layerSelect/types';

const style = {
  borderRadius: '20px!important',
  marginRight: '8px',
  whiteSpace: 'nowrap',
  height: '32px!important',
  padding: '5px 12px!important',
  textTransform: 'none!important',
  fontWeight: 'normal',
};

interface ServiceChipProps {
  all?: boolean;
  service?: ActiveServiceObject;
  isSelected?: boolean;
  isDisabled?: boolean;
  toggleChip?: (service: string) => void;
}

const ServiceChip: React.FC<ServiceChipProps> = ({
  all = false,
  service,
  isSelected,
  isDisabled,
  toggleChip,
}: ServiceChipProps) => {
  return (
    <CustomToggleButton
      variant="boxed"
      selected={isSelected}
      tabIndex={0}
      data-testid={all ? 'serviceChipAll' : 'serviceChip'}
      onClick={(): void =>
        toggleChip(all ? 'all' : service && service.serviceUrl)
      }
      onKeyPress={(): void =>
        toggleChip(all ? 'all' : service && service.serviceUrl)
      }
      disabled={isDisabled}
      sx={style}
    >
      {service ? service.serviceName : 'All'}
    </CustomToggleButton>
  );
};

export default ServiceChip;
