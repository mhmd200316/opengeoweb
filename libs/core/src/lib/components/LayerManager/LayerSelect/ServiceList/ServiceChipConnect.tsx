/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppStore } from '../../../../types/types';
import * as layerSelectFilterSelectors from '../../../../store/layerSelect/selectors';
import { layerSelectActions } from '../../../../store';
import ServiceChip from './ServiceChip';
import {
  ActiveServiceObject,
  ActiveServices,
} from '../../../../store/layerSelect/types';

interface ServiceChipConnectProps {
  all?: boolean;
  serviceId?: string;
  service?: ActiveServiceObject;
  isSelected?: boolean;
  isDisabled?: boolean;
}

const ServiceChipConnect: React.FC<ServiceChipConnectProps> = ({
  all = false,
  serviceId,
  service,
  isSelected,
  isDisabled,
}: ServiceChipConnectProps) => {
  const dispatch = useDispatch();
  const activeServices: ActiveServices = useSelector((store: AppStore) =>
    layerSelectFilterSelectors.getActiveServices(store),
  );

  const toggleChip = (service: string): void => {
    if (service === 'all') {
      if (isSelected) {
        for (const [key, value] of Object.entries(activeServices)) {
          const serviceID = key;
          const service = value;
          if (service.enabled) {
            dispatch(
              layerSelectActions.disableActiveService({
                serviceId: serviceID,
                keywords: service.keywordsPerService,
              }),
            );
          }
        }
      } else {
        for (const [key, value] of Object.entries(activeServices)) {
          const serviceID = key;
          const service = value;
          if (!service.enabled) {
            dispatch(
              layerSelectActions.enableActiveService({
                serviceId: serviceID,
                keywords: service.keywordsPerService,
              }),
            );
          }
        }
      }
    } else if (isSelected) {
      dispatch(
        layerSelectActions.disableActiveService({
          serviceId,
          keywords: activeServices[serviceId].keywordsPerService,
        }),
      );
    } else {
      dispatch(
        layerSelectActions.enableActiveService({
          serviceId,
          keywords: activeServices[serviceId].keywordsPerService,
        }),
      );
    }
  };
  return (
    <ServiceChip
      service={service}
      all={all}
      toggleChip={toggleChip}
      isSelected={isSelected}
      isDisabled={isDisabled}
    />
  );
};

export default ServiceChipConnect;
