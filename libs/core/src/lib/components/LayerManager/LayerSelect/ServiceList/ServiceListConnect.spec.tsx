/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import ServiceListConnect from './ServiceListConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { ActiveServiceObject } from '../../../../store/layerSelect/types';

describe('src/components/LayerSelect/ServiceList', () => {
  const activeServices: Record<string, ActiveServiceObject> = {
    serviceid_1: {
      serviceName: 'service1',
      serviceUrl: 'https://service1',
      enabled: true,
      keywordsPerService: [] as string[],
    },
    serviceid_2: {
      serviceName: 'service2',
      serviceUrl: 'https://service2',
      enabled: true,
      keywordsPerService: [] as string[],
    },
  };

  const props = { layerSelectWidth: 500 };
  const mockStore = configureStore();
  const store = mockStore({
    layerSelect: {
      filters: {
        activeServices: {
          byId: activeServices,
          allIds: ['serviceid_1', 'serviceid_2'],
        },
      },
    },
  });
  store.addModules = jest.fn(); // mocking the dynamic module loader
  it('should render component', () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceListConnect {...props} />,
      </CoreThemeStoreProvider>,
    );

    const list = queryByTestId('serviceList');
    expect(list).toBeTruthy();
  });

  it('should render correct chips', () => {
    const { getAllByTestId, getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceListConnect {...props} />,
      </CoreThemeStoreProvider>,
    );

    const allChip = getByTestId('serviceChipAll');
    expect(allChip.innerHTML).toContain('All');

    const chips = getAllByTestId('serviceChip');
    expect(chips[0].innerHTML).toContain(
      activeServices['serviceid_1'].serviceName,
    );
    expect(chips[1].innerHTML).toContain(
      activeServices['serviceid_2'].serviceName,
    );
    expect(chips.length).toEqual(2);
  });
});
