/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useSelector } from 'react-redux';
import { AppStore } from '../../../../types/types';
import * as layerSelectFilterSelectors from '../../../../store/layerSelect/selectors';
import ServiceList from './ServiceList';

interface ServiceListConnectProps {
  layerSelectWidth?: number;
}

const ServiceListConnect: React.FC<ServiceListConnectProps> = ({
  layerSelectWidth,
}: ServiceListConnectProps) => {
  const activeServices = useSelector((store: AppStore) =>
    layerSelectFilterSelectors.getActiveServices(store),
  );

  return (
    <ServiceList
      layerSelectWidth={layerSelectWidth}
      activeServices={activeServices}
    />
  );
};

export default ServiceListConnect;
