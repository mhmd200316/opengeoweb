/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import ServiceChipConnect from './ServiceChipConnect';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import { ActiveServiceObject } from '../../../../store/layerSelect/types';
import { layerSelectActions } from '../../../../store';

describe('src/components/LayerSelect/ServiceChipConnect', () => {
  const props = {
    serviceId: 'serviceid_1',
    service: {
      serviceid_1: {
        serviceName: 'FMI',
        serviceUrl: 'https://testservice1',
        enabled: false,
        keywordsPerService: [] as string[],
      },
    } as ActiveServiceObject,
    isSelected: true,
    isDisabled: false,
  };
  const propsAll = {
    all: true,
    isSelected: true,
  };
  const mockStore = configureStore();

  const store = mockStore({
    layerSelect: {
      filters: {
        activeServices: {
          byId: {
            serviceid_1: {
              serviceName: 'FMI',
              serviceUrl: 'https://testservice1',
              enabled: true,
              keywordsPerService: [] as string[],
            },
            serviceid_2: {
              serviceName: 'METNO',
              serviceUrl: 'https://testservice2',
              enabled: true,
              keywordsPerService: [] as string[],
            },
          },
          allIds: ['servicid_1', 'serviceid_2'],
        },
      },
    },
  });
  store.addModules = jest.fn(); // mocking the dynamic module loader

  it('should render component', async () => {
    const { findByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceChipConnect {...props} />,
      </CoreThemeStoreProvider>,
    );

    const button = await findByTestId('serviceChip');
    expect(button).toBeTruthy();
  });

  it('should toggle enabled true in store when clicked', async () => {
    props.isSelected = false;
    propsAll.all = false;
    const { findByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceChipConnect {...props} />,
      </CoreThemeStoreProvider>,
    );
    const expectedAction = [
      layerSelectActions.enableActiveService({
        serviceId: 'serviceid_1',
        keywords: [],
      }),
    ];

    const button = await findByTestId('serviceChip');
    fireEvent.click(button);

    expect(store.getActions()).toEqual(expectedAction);
    store.clearActions();
  });

  it('should toggle enabled false in store when clicked', async () => {
    props.isSelected = true;
    const { findByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceChipConnect {...props} />,
      </CoreThemeStoreProvider>,
    );
    const expectedAction = [
      layerSelectActions.disableActiveService({
        serviceId: 'serviceid_1',
        keywords: [],
      }),
    ];

    const button = await findByTestId('serviceChip');
    fireEvent.click(button);
    expect(store.getActions()).toEqual(expectedAction);
    store.clearActions();
  });

  it('should toggle all false in store when all chip clicked', async () => {
    const mockStore = configureStore();

    const store = mockStore({
      layerSelect: {
        filters: {
          activeServices: {
            byId: {
              serviceid_1: {
                serviceName: 'FMI',
                serviceUrl: 'https://testservice1',
                enabled: true,
                keywordsPerService: [] as string[],
              },
              serviceid_2: {
                serviceName: 'METNO',
                serviceUrl: 'https://testservice2',
                enabled: true,
                keywordsPerService: [] as string[],
              },
            },
            allIds: ['servicid_1', 'serviceid_2'],
          },
        },
      },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    propsAll.all = true;
    propsAll.isSelected = true;

    const { findByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceChipConnect {...propsAll} />,
      </CoreThemeStoreProvider>,
    );
    const expectedActions = [
      layerSelectActions.disableActiveService({
        serviceId: 'serviceid_1',
        keywords: [],
      }),
      layerSelectActions.disableActiveService({
        serviceId: 'serviceid_2',
        keywords: [],
      }),
    ];
    const button = await findByRole('button', { name: 'All' });
    fireEvent.click(button);
    expect(store.getActions()).toEqual(expectedActions);
    store.clearActions();
  });

  it('should toggle all true in store when all chip clicked', async () => {
    const mockStore = configureStore();

    const store = mockStore({
      layerSelect: {
        filters: {
          activeServices: {
            byId: {
              serviceid_1: {
                serviceName: 'FMI',
                serviceUrl: 'https://testservice1',
                enabled: false,
                keywordsPerService: [] as string[],
              },
              serviceid_2: {
                serviceName: 'METNO',
                serviceUrl: 'https://testservice2',
                enabled: false,
                keywordsPerService: [] as string[],
              },
            },
            allIds: ['servicid_1', 'serviceid_2'],
          },
        },
      },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    propsAll.all = true;
    propsAll.isSelected = false;

    const { findByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceChipConnect {...propsAll} />,
      </CoreThemeStoreProvider>,
    );
    const expectedActions = [
      layerSelectActions.enableActiveService({
        serviceId: 'serviceid_1',
        keywords: [],
      }),
      layerSelectActions.enableActiveService({
        serviceId: 'serviceid_2',
        keywords: [],
      }),
    ];

    const button = await findByRole('button', { name: 'All' });
    fireEvent.click(button);
    expect(store.getActions()).toEqual(expectedActions);
    store.clearActions();
  });
});
