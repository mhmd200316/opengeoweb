/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Theme } from '@mui/material';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import { ChevronLeft, ChevronRight } from '@opengeoweb/theme';
import ServiceChipConnect from './ServiceChipConnect';
import { ActiveServiceObject } from '../../../../store/layerSelect/types';

interface ServiceListStyleType {
  margin?: number;
  maxWidthValue?: number;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    serviceChips: {
      width: '100%',
      display: 'inline-flex',
      alignItems: 'center',
      overflowX: 'visible',
      float: 'left',
      paddingLeft: '40px',
      marginRight: ({ margin }: ServiceListStyleType) => margin,
      marginLeft: ({ margin }: ServiceListStyleType) => -margin,
      maxWidth: ({ maxWidthValue }: ServiceListStyleType) => maxWidthValue,
      fontSize: '0.875rem',
    },
    scrollArrowLeft: {
      width: '100px',
      marginBottom: '-12px',
      height: '34px',
      backgroundImage: `linear-gradient(to left, rgba(0, 0, 0, 0), ${theme.palette.geowebColors.background.surfaceApp} 51%)`,
      cursor: 'pointer',
      marginRight: '-120px',
      paddingTop: '6px',
      paddingBottom: '6px',
      float: 'left',
      zIndex: 1,
      position: 'relative',
      color: theme.palette.geowebColors.buttons.tertiary.default.color,
      '&:hover': {
        color:
          theme.palette.geowebColors.buttons.tertiary.activeMouseOver.color,
      },
    },
    serviceList: {
      height: '32px',
      marginTop: '8px',
      width: '100%',
      overflow: 'hidden',
    },
    scrollArrowRight: {
      width: '100px',
      marginBottom: '-12px',
      height: '34px',
      paddingTop: '6px',
      paddingBottom: '6px',
      backgroundImage: `linear-gradient(to right, rgba(0, 0, 0, 0), ${theme.palette.geowebColors.background.surfaceApp} 51%)`,
      right: '0px',
      position: 'fixed',
      cursor: 'pointer',
      color: theme.palette.geowebColors.buttons.tertiary.default.color,
      '&:hover': {
        color:
          theme.palette.geowebColors.buttons.tertiary.activeMouseOver.color,
      },
    },
  }),
);

interface ServiceListProps {
  layerSelectWidth?: number;
  activeServices: Record<string, ActiveServiceObject>;
}

const ServiceList: React.FC<ServiceListProps> = ({
  layerSelectWidth,
  activeServices,
}: ServiceListProps) => {
  const [margin, setMargin] = React.useState(0);
  const [width, setWidth] = React.useState(0);
  const [intervalId, setIntervalId] = React.useState(null);
  const ref = React.useRef(null);
  const scrollSpeed = 5;
  const maxWidthValue =
    (ref &&
      ref.current &&
      ref.current.scrollWidth &&
      ref.current.scrollWidth >= layerSelectWidth - margin &&
      ref.current.scrollWidth) ||
    layerSelectWidth - 50;
  const classes = useStyles({ margin, maxWidthValue });

  const onMouseWheelScroll = (e): void => {
    const delta = e.deltaY;
    const scrollSpeed = 0.1;
    if (delta > 0)
      setMargin(
        Math.min(
          Math.max(margin + scrollSpeed * delta, 0),
          width - layerSelectWidth,
        ),
      );
    else
      setMargin(
        Math.min(
          Math.max(margin + scrollSpeed * delta, 0),
          width - layerSelectWidth,
        ),
      );
  };

  const onLeftChevronPressed = (): void => {
    setMargin((prevMargin) => Math.max(prevMargin - scrollSpeed, 0));
    setIntervalId(
      setInterval(() => {
        setMargin((prevMargin) => Math.max(prevMargin - scrollSpeed, 0));
      }, 20),
    );
  };

  const onRightChevronPressed = React.useCallback((): void => {
    setMargin((prevMargin) =>
      Math.min(prevMargin + scrollSpeed, width - layerSelectWidth),
    );
    setIntervalId(
      setInterval(() => {
        setMargin((prevMargin) =>
          Math.min(prevMargin + scrollSpeed, width - layerSelectWidth),
        );
      }, 20),
    );
  }, [width, layerSelectWidth]);

  const onChevronMouseUp = (): void => {
    clearInterval(intervalId);
    setIntervalId(null);
  };

  React.useEffect(() => {
    setWidth(ref.current.scrollWidth + 50);
    setMargin(Math.max(0, Math.min(width - layerSelectWidth, margin)));

    if (
      (margin === 0 || layerSelectWidth + margin === width) &&
      intervalId != null
    ) {
      clearInterval(intervalId);
      setIntervalId(null);
    }
  }, [activeServices, layerSelectWidth, width, margin, intervalId]);

  const isAllSelected = Object.values(activeServices).every(
    (activeService) => activeService.enabled,
  );

  return (
    <div
      className={classes.serviceList}
      data-testid="serviceList"
      onWheel={onMouseWheelScroll}
    >
      {margin !== 0 && (
        <ChevronLeft
          className={classes.scrollArrowLeft}
          onMouseDown={onLeftChevronPressed}
          onMouseUp={onChevronMouseUp}
          data-testid="leftScrollArrow"
        />
      )}
      <div className={classes.serviceChips} ref={ref}>
        {activeServices &&
          Object.keys(activeServices) &&
          Object.keys(activeServices).length > 0 && (
            <ServiceChipConnect
              key="All"
              all={true}
              isSelected={isAllSelected}
            />
          )}
        {Object.keys(activeServices).map((serviceId) => {
          const service: ActiveServiceObject = activeServices[serviceId];
          return (
            <ServiceChipConnect
              key={serviceId}
              serviceId={serviceId}
              service={service}
              isSelected={activeServices[serviceId]?.enabled}
              isDisabled={activeServices[serviceId] === undefined}
            />
          );
        })}
      </div>
      {layerSelectWidth + margin < width && (
        <ChevronRight
          className={classes.scrollArrowRight}
          onMouseDown={onRightChevronPressed}
          onMouseUp={onChevronMouseUp}
          data-testid="rightScrollArrow"
        />
      )}
    </div>
  );
};

export default ServiceList;
