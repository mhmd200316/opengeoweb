/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import LayerSelectConnect from './LayerSelectConnect';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import { DialogType } from '../../../store/ui/types';
import { preloadedDefaultMapServices } from '../../../utils/defaultConfigurations';
import WMS111GetCapabilitiesGeoServicesRADAR from '../../../utils/__mocks__/WMS111GetCapabilitiesGeoServicesRADAR';
import { styleListForRADNL25PCPCMLayer } from '../../../utils/defaultTestSettings';
import { serviceActions, uiActions } from '../../../store';

describe('src/components/LayerSelect/LayerSelectConnect', () => {
  const storedFetch = global['fetch'];

  afterEach(() => {
    global['fetch'] = storedFetch;
  });

  it('should register the dialog when mounting', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      webmap: { byId: { mapId123: {} }, allIds: ['mapId123'] },
    });
    store.addModules = jest.fn(); // mocking the dynamic module loader
    render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectConnect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: 'keywordFilter',
        setOpen: false,
      }),
      uiActions.registerDialog({
        type: 'layerSelect',
        setOpen: false,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should not make GetCapabilities requests if not opened', async () => {
    const mockStore = configureStore();
    const mockState = {
      webmap: { byId: { mapId123: {} }, allIds: ['mapIdA'] },
      ui: {
        dialogs: {
          layerSelect: {
            type: 'layerSelect' as DialogType,
            activeMapId: 'mapIdA',
            isOpen: false,
            source: 'module',
          },
        },
      },
    };
    const store = mockStore({ mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    // mock the WMXMLParser fetch
    const headers = new Headers();
    window.fetch = jest.fn().mockResolvedValueOnce({
      text: () =>
        Promise.resolve(
          `<?xml version="1.0" encoding="UTF-8"?>
              <WMS_Capabilities>
                <Capability>
                    <Layer>
                    </Layer>
                </Capability>
              </WMS_Capabilities>`,
        ),
      headers,
    });
    const spy = jest.spyOn(window, 'fetch');

    const { baseElement } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectConnect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      uiActions.registerDialog({
        type: 'keywordFilter',
        setOpen: false,
      }),
      uiActions.registerDialog({
        type: 'layerSelect',
        setOpen: false,
      }),
    ];

    await waitFor(() => {
      expect(baseElement).toBeTruthy();
      expect(store.getActions()).toEqual(expectedAction);
      expect(spy).toHaveBeenCalledTimes(0);
    });
  });

  it('should request GetCapabilities for default services when opened', async () => {
    const mockStore = configureStore();
    const mockState = {
      dialogs: {
        layerSelect: {
          type: 'layerSelect' as DialogType,
          activeMapId: 'mapIdB',
          isOpen: true,
          source: 'module',
        },
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    // mock the WMXMLParser fetch
    const headers = new Headers();
    window.fetch = jest.fn().mockResolvedValueOnce({
      text: () =>
        Promise.resolve(
          `<?xml version="1.0" encoding="UTF-8"?>
              <WMS_Capabilities>
                <Capability>
                    <Layer>
                    </Layer>
                </Capability>
              </WMS_Capabilities>`,
        ),
      headers,
    });
    const spy = jest.spyOn(window, 'fetch');

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectConnect />
      </CoreThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(spy).toHaveBeenCalledTimes(preloadedDefaultMapServices.length);
      expect(spy).toHaveBeenCalledWith(
        `${preloadedDefaultMapServices[0].url}service=WMS&request=GetCapabilities`,
        {
          headers,
          method: 'GET',
          mode: 'cors',
        },
      );
    });
  });

  it('should request GetCapabilities for given services when opened', async () => {
    const mockStore = configureStore();
    const mockState = {
      dialogs: {
        layerSelect: {
          type: 'layerSelect' as DialogType,
          activeMapId: 'mapIdC',
          isOpen: true,
          source: 'module',
        },
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const preloadedServices = [
      {
        name: 'Test123',
        url: 'https://testurl123/wms?',
        id: 'TestId123',
      },
      {
        name: 'Test456',
        url: 'https://testurl456/wms?',
        id: 'TestId456',
      },
    ];

    // mock the WMXMLParser fetch
    const headers = new Headers();
    window.fetch = jest.fn().mockResolvedValue({
      text: () =>
        Promise.resolve(
          `<?xml version="1.0" encoding="UTF-8"?>
              <WMS_Capabilities>
                <Capability>
                    <Layer>
                    </Layer>
                </Capability>
              </WMS_Capabilities>`,
        ),
      headers,
    });
    const spy = jest.spyOn(window, 'fetch');

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectConnect preloadedServices={preloadedServices} />
      </CoreThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(spy).toHaveBeenCalledTimes(2);
      expect(spy).toHaveBeenCalledWith(
        'https://testurl123/wms?service=WMS&request=GetCapabilities',
        { headers, method: 'GET', mode: 'cors' },
      );
      expect(spy).toHaveBeenCalledWith(
        'https://testurl456/wms?service=WMS&request=GetCapabilities',
        { headers, method: 'GET', mode: 'cors' },
      );
    });
  });

  it('should catch failed request', async () => {
    const mockStore = configureStore();
    const mockState = {
      dialogs: {
        layerSelect: {
          type: 'layerSelect' as DialogType,
          activeMapId: 'mapIdD',
          isOpen: true,
          source: 'module',
        },
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const preloadedServices = [
      {
        name: 'TestAbc',
        url: 'https://testurlabc/wms?',
        id: 'TestIdAbc',
      },
    ];

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectConnect preloadedServices={preloadedServices} />
      </CoreThemeStoreProvider>,
    );
    // mock the WMXMLParser fetch with a failed request
    const headers = new Headers();
    window.fetch = jest.fn().mockRejectedValueOnce({
      text: () => Promise.reject(new Error('failed')),
      headers,
    });

    const spy = jest.spyOn(window, 'fetch');

    await waitFor(() => {
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(
        'Check WMJSGlobals WMJSServiceStoreXML2JSONRequestrequest=https%3A%2F%2Ftesturlabc%2Fwms%3Fservice%3DWMS%26request%3DGetCapabilities',
        {},
      );
    });
  });

  it('should call serviceSetLayers with the correct layers when open and the preloaded service has layers', async () => {
    const mockStore = configureStore();
    const mockState = {
      dialogs: {
        layerSelect: {
          type: 'layerSelect' as DialogType,
          activeMapId: 'mapIdD',
          isOpen: true,
          source: 'module',
        },
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const preloadedServices = [
      {
        name: 'Testservice1',
        url: 'https://testservice1/wms?',
        id: 'testservice1',
      },
    ];

    // mock the WMXMLParser fetch
    const headers = new Headers();
    window.fetch = jest.fn().mockResolvedValueOnce({
      text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
      headers,
    });
    const spy = jest.spyOn(window, 'fetch');

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectConnect preloadedServices={preloadedServices} />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = serviceActions.serviceSetLayers({
      id: expect.any(String),
      name: 'Testservice1',
      serviceUrl: 'https://testservice1/wms?',
      layers: [
        {
          abstract: 'Radar NL',
          keywords: ['Radar'],
          leaf: true,
          name: 'RAD_NL25_PCP_CM',
          path: [],
          text: 'Precipitation Radar NL',
          styles: styleListForRADNL25PCPCMLayer,
        },
      ],
      isUserAddedService: false,
    });
    await waitFor(() => {
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(
        'https://testservice1/wms?service=WMS&request=GetCapabilities',
        { headers, method: 'GET', mode: 'cors' },
      );
      expect(getByTestId('serviceList')).toBeTruthy();
      const storeAction = store
        .getActions()
        .filter(
          (action) => action.type === serviceActions.serviceSetLayers.type,
        )[0];

      expect(storeAction.payload.id).toBeDefined();

      expect(storeAction).toEqual(expectedAction);
    });
  });

  it('should read user-added services from localstorage when opened', () => {
    const mockStore = configureStore();
    const mockState = {
      dialogs: {
        layerSelect: {
          type: 'layerSelect' as DialogType,
          activeMapId: 'mapIdD',
          isOpen: true,
          source: 'module',
        },
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    // mock the WMXMLParser fetch
    const headers = new Headers();
    window.fetch = jest.fn().mockResolvedValueOnce({
      text: () => Promise.resolve(WMS111GetCapabilitiesGeoServicesRADAR),
      headers,
    });
    jest.spyOn(Object.getPrototypeOf(window.localStorage), 'getItem');

    const preloadedServices = [
      {
        name: 'Testservice1',
        url: 'https://testservice1/wms?',
        id: 'testservice1',
      },
    ];

    expect(window.localStorage.getItem).not.toHaveBeenCalled();
    render(
      <CoreThemeStoreProvider store={store}>
        <LayerSelectConnect preloadedServices={preloadedServices} />
      </CoreThemeStoreProvider>,
    );
    expect(window.localStorage.getItem).toHaveBeenCalled();
  });
});
