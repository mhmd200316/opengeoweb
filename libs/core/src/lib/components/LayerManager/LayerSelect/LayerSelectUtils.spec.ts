/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { NoIdService, UserAddedServices } from '../../../utils/types';
import { Service } from '../../WMSLoader/services';
import { mergePresetsAndUserAddedServices } from './LayerSelectUtils';

describe('LayerManager/LayerSelect/LayerSelectUtils', () => {
  const preloadedServices: Service[] = [
    {
      name: 'Testservice1',
      url: 'https://testservice1/wms?',
      id: 'testservice1',
    },
    {
      name: 'Testservice2',
      url: 'https://testservice2/wms?',
      id: 'testservice2',
    },
  ];
  const userAddedServices: UserAddedServices = {
    'https://testservice1/wms?': {
      name: 'Customised name for service 1',
      url: 'https://testservice1/wms?',
    },
    'https://testservice3/wms?': {
      name: 'Customised name for service 3',
      url: 'https://testservice3/wms?',
    },
  };
  it('should merge duplicate ids by preferring the user-added entry', () => {
    expect(
      mergePresetsAndUserAddedServices(preloadedServices, userAddedServices),
    ).toEqual([
      {
        name: 'Customised name for service 1',
        url: 'https://testservice1/wms?',
      },
      {
        id: 'testservice2',
        name: 'Testservice2',
        url: 'https://testservice2/wms?',
      },
      {
        name: 'Customised name for service 3',
        url: 'https://testservice3/wms?',
      },
    ]);
  });
  it('should return only preloaded services when only preloaded services are given', () => {
    expect(mergePresetsAndUserAddedServices(preloadedServices, {})).toEqual(
      preloadedServices as NoIdService[],
    );
  });
  it('should return only user-added services when only user-added services are given', () => {
    expect(mergePresetsAndUserAddedServices([], userAddedServices)).toEqual([
      {
        name: 'Customised name for service 1',
        url: 'https://testservice1/wms?',
      },
      {
        name: 'Customised name for service 3',
        url: 'https://testservice3/wms?',
      },
    ]);
  });
  it('should return empty list on empty inputs', () => {
    expect(mergePresetsAndUserAddedServices([], {})).toEqual([]);
  });
});
