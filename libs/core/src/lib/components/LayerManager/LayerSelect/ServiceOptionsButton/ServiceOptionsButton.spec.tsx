/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { lightTheme } from '@opengeoweb/theme';
import ServiceOptionsButton from './ServiceOptionsButton';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';

describe('src/components/LayerSelect/ServiceOptionsButton', () => {
  const mockStore = configureStore();
  const store = mockStore({
    layerSelect: {
      filters: {
        activeServices: {
          byId: {
            serviceid_1: {
              serviceName: 'Radar Norway',
              serviceUrl: 'https://public-wms.met.no/verportal/verportal.map?',
              enabled: true,
              keywordsPerService: [],
            },
            serviceid_2: {
              name: 'Precipitation Radar NL',
              serviceUrl: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
              enabled: true,
              keywordsPerService: [],
            },
          },
          allIds: ['serviceid_1', 'serviceid_2'],
        },
      },
    },
  });
  store.addModules = jest.fn(); // mocking the dynamic module loader

  it('should render the component', () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider theme={lightTheme} store={store}>
        <ServiceOptionsButton />,
      </CoreThemeStoreProvider>,
    );

    const button = queryByTestId('serviceOptionsButton');
    expect(button).toBeTruthy();
  });

  it('should open the service dialog', () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider theme={lightTheme} store={store}>
        <ServiceOptionsButton />,
      </CoreThemeStoreProvider>,
    );

    fireEvent.click(queryByTestId('serviceOptionsButton'));
    expect(queryByTestId('ServiceDialog')).toBeTruthy();
  });
});
