/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import ServiceOptionsButton from './ServiceOptionsButton';
import { store } from '../../../../storybookUtils/store';

export default {
  title: 'components/LayerSelect/ServiceOptionsButton',
};

export const ServiceOptionsButtonLight = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <div style={{ position: 'absolute', left: '400px', top: '20px' }}>
      <ServiceOptionsButton />
    </div>
  </CoreThemeStoreProvider>
);

export const ServiceOptionsButtonDark = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <div style={{ position: 'absolute', left: '400px', top: '20px' }}>
      <ServiceOptionsButton />
    </div>
  </CoreThemeStoreProvider>
);
