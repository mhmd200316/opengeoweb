/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { IconButton, Tooltip } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import { Add } from '@opengeoweb/theme';
import * as uiSelectors from '../../../store/ui/selectors';
import { uiActions } from '../../../store';
import { AppStore } from '../../../types/types';
import { Source } from '../../../store/ui/types';

const style = {
  width: '24px',
  height: '24px',
  margin: '6px 0px 0px 10px',
  borderRadius: '15%',
};

interface LayerSelectButtonConnectProps {
  mapId: string;
  source?: Source;
}

const LayerSelectButtonConnect: React.FC<LayerSelectButtonConnectProps> = ({
  mapId,
  source = 'app',
}: LayerSelectButtonConnectProps) => {
  const dispatch = useDispatch();
  const currentActiveMapId = useSelector((store: AppStore) =>
    uiSelectors.getDialogMapId(store, 'layerSelect'),
  );

  const isOpenInStore = useSelector((store: AppStore) =>
    uiSelectors.getisDialogOpen(store, 'layerSelect'),
  );

  const openLayerSelectDialog = React.useCallback((): void => {
    dispatch(
      uiActions.setActiveMapIdForDialog({
        type: 'layerSelect',
        mapId,
        setOpen: currentActiveMapId !== mapId ? true : !isOpenInStore,
        source,
      }),
    );
  }, [currentActiveMapId, dispatch, isOpenInStore, mapId, source]);

  return (
    <Tooltip title="Layer Select">
      <IconButton
        onClick={openLayerSelectDialog}
        sx={style}
        id="layerSelectButton"
        data-testid="layerSelectButton"
        disableRipple
        size="large"
      >
        <Add />
      </IconButton>
    </Tooltip>
  );
};

export default LayerSelectButtonConnect;
