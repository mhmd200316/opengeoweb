/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import configureStore from 'redux-mock-store';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import ServiceOptionsDialogConnect from './ServiceOptionsDialogConnect';
import { serviceActions } from '../../../../store';

describe('src/lib/components/LayerManager/ServiceOptionsDialogConnect', () => {
  const mockStore = configureStore();
  const store = mockStore({
    layerSelect: {
      filters: {
        activeServices: {
          byId: {
            serviceid_1: {
              serviceName: 'Radar Norway',
              serviceUrl: 'https://public-wms.met.no/verportal/verportal.map?',
              enabled: true,
              keywordsPerService: [],
              isUserAddedService: false,
            },
            serviceid_2: {
              serviceName: 'Precipitation Radar NL',
              serviceUrl: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
              enabled: true,
              keywordsPerService: [],
              isUserAddedService: true,
            },
          },
          allIds: ['serviceid_1', 'serviceid_2'],
        },
      },
    },
  });
  store.addModules = jest.fn(); // mocking the dynamic module loader

  it('should render the component', () => {
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('ServiceDialog')).toBeTruthy();
  });

  it('should open the add service popup on click', async () => {
    const { queryByRole, queryAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </CoreThemeStoreProvider>,
    );

    expect(queryByRole('dialog')).toBeFalsy();
    fireEvent.click(queryAllByTestId('openAddServiceButton')[0]);
    await waitFor(() => expect(queryByRole('dialog')).toBeTruthy());
  });

  it('should close the dialog when cross -button is clicked', async () => {
    const { queryByRole, queryAllByTestId, queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </CoreThemeStoreProvider>,
    );

    expect(queryByRole('dialog')).toBeFalsy();
    fireEvent.click(queryAllByTestId('openAddServiceButton')[0]);
    await waitFor(() => expect(queryByRole('dialog')).toBeTruthy());

    fireEvent.click(queryByTestId('closePopupButtonCross'));
    expect(queryByRole('dialog')).toBeFalsy();
  });

  it('should close the dialog when cancel -button is clicked', async () => {
    const { queryByRole, queryAllByTestId, queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </CoreThemeStoreProvider>,
    );

    expect(queryByRole('dialog')).toBeFalsy();
    fireEvent.click(queryAllByTestId('openAddServiceButton')[0]);
    await waitFor(() => expect(queryByRole('dialog')).toBeTruthy());

    fireEvent.click(queryByTestId('closePopupButtonCancel'));
    expect(queryByRole('dialog')).toBeFalsy();
  });

  it('should dispatch mapStore removeService action on click', async () => {
    const { queryAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </CoreThemeStoreProvider>,
    );

    const expectedAction = [
      serviceActions.mapStoreRemoveService({
        id: 'serviceid_2',
        serviceUrl: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
      }),
    ];
    const removeServiceButton = queryAllByTestId('removeServiceButton');

    fireEvent.click(removeServiceButton[1]);
    expect(store.getActions()).toEqual(expectedAction);
  });
});
