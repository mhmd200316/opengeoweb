/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { layerSelectSelectors, serviceActions } from '../../../../store';
import ServiceOptionsDialog from './ServiceOptionsDialog';
import { AppStore } from '../../../../types/types';

const ServiceOptionsDialogConnect: React.FC = () => {
  const dispatch = useDispatch();

  const mapStoreRemoveService = React.useCallback(
    (serviceId: string, serviceUrl: string): void => {
      dispatch(
        serviceActions.mapStoreRemoveService({
          id: serviceId,
          serviceUrl,
        }),
      );
    },
    [dispatch],
  );
  const services = useSelector((store: AppStore) =>
    layerSelectSelectors.getActiveServices(store),
  );

  return (
    <ServiceOptionsDialog
      services={services}
      mapStoreRemoveService={mapStoreRemoveService}
    />
  );
};

export default ServiceOptionsDialogConnect;
