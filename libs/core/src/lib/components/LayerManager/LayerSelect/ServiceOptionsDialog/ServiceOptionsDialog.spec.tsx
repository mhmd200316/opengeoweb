/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import ServiceOptionsDialog from './ServiceOptionsDialog';

describe('src/lib/components/LayerManager/ServiceOptionsDialog', () => {
  const mockStore = configureStore();
  const store = mockStore({});
  const props = {
    services: {
      serviceid_1: {
        serviceName: 'MET Norway',
        serviceUrl: 'https:service.wms1',
        enabled: true,
        keywordsPerService: [] as string[],
        isUserAddedService: true,
      },
      serviceid_2: {
        serviceName: 'KNMI Radar',
        serviceUrl: 'https:service.wms2',
        enabled: true,
        keywordsPerService: [] as string[],
        isUserAddedService: false,
      },
      serviceid_3: {
        serviceName: 'DWD',
        serviceUrl: 'https://maps.dwd.de/geoserver/ows?',
        enabled: true,
        keywordsPerService: [] as string[],
        isUserAddedService: false,
      },
    },

    mapStoreRemoveService: jest.fn(),
    layers: [],
  };
  store.addModules = jest.fn(); // mocking the dynamic module loader

  it('should render the component', () => {
    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(getByTestId('ServiceDialog')).toBeTruthy();
  });

  it('should call mapStoreRemoveService with correct value', async () => {
    const { getAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    const removeButton = getAllByTestId('removeServiceButton');
    fireEvent.click(removeButton[0]);
    expect(props.mapStoreRemoveService).toBeCalledWith(
      'serviceid_1',
      props.services['serviceid_1'].serviceUrl,
    );
  });

  it('should show button disabled if service is from presets', async () => {
    const { getAllByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ServiceOptionsDialog {...props} />
      </CoreThemeStoreProvider>,
    );

    const removeButton = getAllByTestId('removeServiceButton');
    const editButton = getAllByTestId('openEditServiceButton');
    expect((removeButton[0] as HTMLButtonElement).disabled).toBeFalsy();
    expect((removeButton[1] as HTMLButtonElement).disabled).toBeTruthy();
    expect((editButton[0] as HTMLButtonElement).disabled).toBeFalsy();
    expect((editButton[1] as HTMLButtonElement).disabled).toBeTruthy();
  });
});
