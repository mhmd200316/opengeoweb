/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Box,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Button,
  Typography,
  Tooltip,
} from '@mui/material';

import { Add, Delete, Edit } from '@opengeoweb/theme';
import ServicePopupConnect from '../ServicePopup/ServicePopupConnect';
import { ActiveServiceObject } from '../../../../store/layerSelect/types';

interface ServiceOptionsDialogProps {
  services: Record<string, ActiveServiceObject>;
  mapStoreRemoveService?: (serviceId: string, serviceUrl: string) => void;
}

const styles = {
  servicesContainer: {
    width: '300px',
    maxHeight: '380px',
    top: '140px',
    backgroundColor: 'geowebColors.background.surface',
    boxShadow: 6,
    overflow: 'auto',
  },
  header: {
    position: 'sticky',
    top: 0,
    fontSize: 'default',
    padding: '12px',
    backgroundColor: 'geowebColors.background.surface',
    zIndex: 100,
  },
  footer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'sticky',
    bottom: 0,
    backgroundColor: 'geowebColors.background.surface',
    zIndex: 100,
  },
  button: {
    margin: '16px',
    width: '80%',
    fontSize: '12px',
    textTransform: 'none',
  },
};

const ServiceOptionsDialog: React.FC<ServiceOptionsDialogProps> = ({
  services,
  mapStoreRemoveService,
}: ServiceOptionsDialogProps) => {
  const [addServiceIsOpen, setAddServiceIsOpen] = React.useState(false);
  const [editServiceIsOpen, setEditServiceIsOpen] = React.useState({
    isOpen: false,
    serviceId: '',
    serviceUrl: '',
  });

  const openAddService = (): void => {
    setAddServiceIsOpen(true);
  };

  const closePopup = (): void => {
    setAddServiceIsOpen(false);
    setEditServiceIsOpen({ isOpen: false, serviceId: '', serviceUrl: '' });
  };

  return (
    <>
      <Box sx={styles.servicesContainer} data-testid="ServiceDialog">
        <Box sx={styles.header}>Services</Box>
        <List disablePadding>
          {Object.keys(services).map((serviceId) => (
            <ListItem key={services[serviceId].serviceUrl}>
              <ListItemText style={{ fontSize: '16px' }}>
                <Typography
                  noWrap
                  style={{ textOverflow: 'ellipsis', paddingRight: '60px' }}
                >
                  {services[serviceId].serviceName}
                </Typography>
              </ListItemText>
              <ListItemSecondaryAction style={{ marginRight: '-10px' }}>
                <Tooltip
                  title="Edit Service"
                  disableHoverListener={!services[serviceId].isUserAddedService}
                >
                  <span>
                    <IconButton
                      disabled={!services[serviceId].isUserAddedService}
                      data-testid="openEditServiceButton"
                      size="large"
                      onClick={(): void => {
                        setEditServiceIsOpen({
                          isOpen: true,
                          serviceId,
                          serviceUrl: services[serviceId].serviceUrl,
                        });
                      }}
                    >
                      <Edit />
                    </IconButton>
                  </span>
                </Tooltip>
                <Tooltip
                  title="Delete Service"
                  disableHoverListener={!services[serviceId].isUserAddedService}
                >
                  <span>
                    <IconButton
                      size="large"
                      disabled={!services[serviceId].isUserAddedService}
                      onClick={(): void =>
                        mapStoreRemoveService(
                          serviceId,
                          services[serviceId].serviceUrl,
                        )
                      }
                      data-testid="removeServiceButton"
                    >
                      <Delete />
                    </IconButton>
                  </span>
                </Tooltip>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
        <Box sx={styles.footer}>
          <Button
            onClick={openAddService}
            data-testid="openAddServiceButton"
            sx={styles.button}
            variant="tertiary"
            startIcon={<Add />}
          >
            Add a new service
          </Button>
        </Box>
      </Box>
      {addServiceIsOpen && (
        <ServicePopupConnect
          servicePopupVariant="add"
          isOpen={addServiceIsOpen}
          closePopup={closePopup}
        />
      )}
      {editServiceIsOpen && (
        <ServicePopupConnect
          servicePopupVariant="edit"
          isOpen={editServiceIsOpen.isOpen}
          closePopup={closePopup}
          serviceId={editServiceIsOpen.serviceId}
          url={editServiceIsOpen.serviceUrl}
        />
      )}
    </>
  );
};

export default ServiceOptionsDialog;
