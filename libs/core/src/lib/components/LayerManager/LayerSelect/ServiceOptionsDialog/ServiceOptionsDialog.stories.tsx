/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import configureStore from 'redux-mock-store';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import ServiceOptionsDialog from './ServiceOptionsDialog';

export default {
  title: 'components/LayerSelect/ServiceOptionsDialog',
};

const mockStore = configureStore();
const store = mockStore({});
store.addModules = (): void => {};

const services = {
  serviceid_1: {
    serviceName: 'MET Norway',
    serviceUrl: 'https://service1',
    enabled: true,
    keywordsPerService: [] as string[],
  },
  serviceid_2: {
    serviceName: 'MET Norway',
    serviceUrl: 'https://service2',
    enabled: true,
    keywordsPerService: [] as string[],
  },
  serviceid_3: {
    serviceName: 'NOAA Tropical Cyclones',
    serviceUrl: 'https://service3',
    enabled: true,
    keywordsPerService: [] as string[],
  },
};

export const ServiceOptionsDialogLight = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <ServiceOptionsDialog services={services} />
  </CoreThemeStoreProvider>
);

export const ServiceOptionsDialogDark = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <ServiceOptionsDialog services={services} />
  </CoreThemeStoreProvider>
);

ServiceOptionsDialogLight.storyName =
  'Service Options Dialog Light Theme (takeSnapshot)';

ServiceOptionsDialogDark.storyName =
  'Service Options Dialog Dark Theme (takeSnapshot)';
