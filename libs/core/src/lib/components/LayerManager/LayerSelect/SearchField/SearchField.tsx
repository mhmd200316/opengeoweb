/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { TextField, InputAdornment } from '@mui/material';
import * as React from 'react';
import { debounce } from 'lodash';
import SearchFieldButtonContainer, {
  isURL,
} from './SearchFieldButtonContainer';

interface SearchFieldProps {
  searchFilter: string;
  setSearchFilter: (filterText: string) => void;
}

const SearchField: React.FC<SearchFieldProps> = ({
  searchFilter,
  setSearchFilter,
}: SearchFieldProps) => {
  const [localSearchString, setLocalSearchString] = React.useState('');

  // mirror store unless typing a URL
  React.useEffect(() => {
    if (isURL(localSearchString)) return;
    setLocalSearchString(searchFilter);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchFilter]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const setSearchFilterDebounced = React.useCallback(
    debounce(setSearchFilter, 300),
    [],
  );

  const handleChange = (text: string): void => {
    setLocalSearchString(text);
    if (!isURL(text)) setSearchFilterDebounced(text);
    else if (searchFilter) setSearchFilterDebounced('');
  };

  return (
    <TextField
      variant="filled"
      label="Search through services or enter a service URL"
      value={localSearchString}
      onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
        handleChange(e.target.value);
      }}
      autoFocus
      fullWidth
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <SearchFieldButtonContainer
              localSearchString={localSearchString}
              onClickClear={(): void => {
                setLocalSearchString('');
                setSearchFilterDebounced('');
              }}
            />
          </InputAdornment>
        ),
      }}
    />
  );
};

export default SearchField;
