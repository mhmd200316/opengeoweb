/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import configureStore from 'redux-mock-store';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import SearchFieldButtonContainer from './SearchFieldButtonContainer';

describe('components/LayerManager/LayerSelect/SearchField/SearchFieldButtonContainer', () => {
  const mockStore = configureStore();
  const store = mockStore({});
  store.addModules = jest.fn(); // mocking the dynamic module loader

  it('should show clear button when typing non-url', () => {
    const clearFn = jest.fn();
    const { getByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <SearchFieldButtonContainer
          localSearchString="test"
          onClickClear={clearFn}
        />
      </CoreThemeStoreProvider>,
    );
    const button = getByRole('button');
    expect(button.getAttribute('aria-label')).toBe('Clear');
  });
  it('should show clear and save buttons when typing url', () => {
    const clearFn = jest.fn();
    const { getAllByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <SearchFieldButtonContainer
          localSearchString="https://openwms.fmi.fi/geoserver/"
          onClickClear={clearFn}
        />
      </CoreThemeStoreProvider>,
    );
    const buttons = getAllByRole('button');
    expect(buttons[0].getAttribute('aria-label')).toBe('Clear');
    expect(buttons[1].textContent).toBe('Save');
  });
  it('should open a popup when pressing save button', () => {
    const clearFn = jest.fn();
    const { getAllByRole, queryByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <SearchFieldButtonContainer
          localSearchString="https://openwms.fmi.fi/geoserver/"
          onClickClear={clearFn}
        />
      </CoreThemeStoreProvider>,
    );
    expect(queryByRole('dialog')).toBeNull();
    const button = getAllByRole('button')[1];
    fireEvent.click(button);
    const dialog = queryByRole('dialog');
    expect(dialog).toBeTruthy();
    expect(dialog.querySelector('.MuiTypography-h5').textContent).toBe(
      'Save service',
    );
  });

  it('Should clear the text from searchbar when popup is closed', async () => {
    const clearFn = jest.fn();
    const { getAllByRole, findByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <SearchFieldButtonContainer
          localSearchString="https://openwms.fmi.fi/geoserver/"
          onClickClear={clearFn}
        />
      </CoreThemeStoreProvider>,
    );
    const button = getAllByRole('button')[1];
    fireEvent.click(button);

    const closeButton = await findByTestId('closePopupButtonCancel');
    fireEvent.click(closeButton);

    expect(clearFn).toHaveBeenCalled();
  });
});
