/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { Button, IconButton, Tooltip } from '@mui/material';
import React from 'react';

import { Clear, Search } from '@opengeoweb/theme';
import ServicePopupConnect from '../ServicePopup/ServicePopupConnect';

export const isURL = (string: string): boolean => /^https?:\/\//.test(string);

interface FieldButtonProps {
  localSearchString: string;
  onClickClear: () => void;
}

const SearchFieldButtonContainer: React.FC<FieldButtonProps> = ({
  localSearchString,
  onClickClear,
}: FieldButtonProps) => {
  const [isPopupOpen, setPopupOpen] = React.useState(false);

  React.useEffect(() => {
    if (isPopupOpen === false) {
      onClickClear();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isPopupOpen]);

  const ClearButton = (
    <Tooltip title="Clear" placement="top">
      <IconButton
        edge="end"
        onClick={(): void => {
          onClickClear();
        }}
        size="large"
      >
        <Clear />
      </IconButton>
    </Tooltip>
  );

  if (isURL(localSearchString))
    return (
      <>
        <div style={{ marginRight: '15px' }}>{ClearButton}</div>

        <Button variant="tertiary" onClick={(): void => setPopupOpen(true)}>
          Save
        </Button>
        <ServicePopupConnect
          servicePopupVariant="save"
          isOpen={isPopupOpen}
          closePopup={(): void => setPopupOpen(false)}
          url={localSearchString}
        />
      </>
    );
  if (localSearchString) return ClearButton;
  return (
    <Tooltip title="Search" placement="top">
      <IconButton edge="end" size="large">
        <Search />
      </IconButton>
    </Tooltip>
  );
};

export default SearchFieldButtonContainer;
