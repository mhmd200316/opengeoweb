/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';

import { CoreThemeProvider } from '../../../Providers/Providers';
import SearchField from './SearchField';

describe('components/LayerManager/LayerSelect/SearchField/SearchFieldConnect', () => {
  const setSearchFilter = jest.fn();
  it('should render correctly', () => {
    const { getByRole } = render(
      <CoreThemeProvider>
        <SearchField searchFilter="" setSearchFilter={setSearchFilter} />
      </CoreThemeProvider>,
    );
    expect(getByRole('textbox')).toBeTruthy();
  });
  it('should be focused by default', () => {
    const { getByRole } = render(
      <CoreThemeProvider>
        <SearchField searchFilter="" setSearchFilter={setSearchFilter} />
      </CoreThemeProvider>,
    );
    expect(
      getByRole('textbox').parentElement.className.includes('Mui-focused'),
    ).toBeTruthy();
  });
  it('should allow text entry', () => {
    const { getByRole } = render(
      <CoreThemeProvider>
        <SearchField searchFilter="" setSearchFilter={setSearchFilter} />
      </CoreThemeProvider>,
    );
    const textField = getByRole('textbox');
    const testString = 'test';
    fireEvent.change(textField, { target: { value: testString } });
    expect((textField as HTMLInputElement).value).toBe(testString);
  });
  it('should clear the input when pressing the cancel button', () => {
    const { getByRole } = render(
      <CoreThemeProvider>
        <SearchField searchFilter="" setSearchFilter={setSearchFilter} />
      </CoreThemeProvider>,
    );
    const textField = getByRole('textbox');
    fireEvent.change(textField, { target: { value: 'abc' } });
    const button = getByRole('button');
    fireEvent.click(button);
    expect((textField as HTMLInputElement).value).toBeFalsy();
  });

  it('should initialize to passed in filter', () => {
    const initialText = 'test';
    const { getByRole } = render(
      <CoreThemeProvider>
        <SearchField
          searchFilter={initialText}
          setSearchFilter={setSearchFilter}
        />
      </CoreThemeProvider>,
    );
    const textField = getByRole('textbox');
    expect((textField as HTMLInputElement).value).toBe(initialText);
  });

  it('should debounce dispatch calls', () => {
    jest.useFakeTimers();
    expect(setSearchFilter).not.toHaveBeenCalled();
    const { getByRole } = render(
      <CoreThemeProvider>
        <SearchField searchFilter="" setSearchFilter={setSearchFilter} />
      </CoreThemeProvider>,
    );
    const textField = getByRole('textbox');
    fireEvent.change(textField, { target: { value: 'a' } });
    fireEvent.change(textField, { target: { value: 'ab' } });
    fireEvent.change(textField, { target: { value: 'abc' } });
    jest.runOnlyPendingTimers();
    expect(setSearchFilter).toHaveBeenCalledTimes(1);

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
