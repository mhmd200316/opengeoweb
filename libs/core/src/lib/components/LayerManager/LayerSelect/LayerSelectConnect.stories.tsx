/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';

import { store } from '../../../storybookUtils/store';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import { useDefaultMapSettings } from '../../../storybookUtils/defaultStorySettings';
import TimeSliderConnect from '../../TimeSlider/TimeSliderConnect';
import { MapViewConnect } from '../../MapView';
import {
  radarLayer,
  overLayer,
  baseLayerGrey,
  harmonieWindPl,
  harmonieAirTemperature,
} from '../../../utils/publicLayers';
import { MapControls } from '../../MapControls';
import LayerManagerConnect from '../LayerManagerConnect';
import LayerManagerMapButtonConnect from '../LayerManagerMapButtonConnect';

interface LayerSelectProps {
  mapId?: string;
}

const MapWithLayerSelect: React.FC<LayerSelectProps> = ({
  mapId,
}: LayerSelectProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [
      { ...radarLayer, id: `radar-${mapId}` },
      { ...harmonieWindPl, id: `harmonieWindPl-${mapId}` },
      { ...harmonieAirTemperature, id: `temp-${mapId}` },
    ],
    baseLayers: [{ ...baseLayerGrey, id: `baseGrey-${mapId}` }, overLayer],
  });
  return (
    <div style={{ height: '100vh' }}>
      <LayerManagerConnect layerSelect />

      <MapControls>
        <LayerManagerMapButtonConnect mapId={mapId} />
      </MapControls>

      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 50,
          width: '100%',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
      <MapViewConnect mapId={mapId} />
    </div>
  );
};

export const MapWithLayerSelectDemoLightTheme: React.FC = () => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <MapWithLayerSelect mapId="mapid_1" />
  </CoreThemeStoreProvider>
);

export const MapWithLayerSelectDemoDarkTheme: React.FC = () => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <MapWithLayerSelect mapId="mapid_1" />
  </CoreThemeStoreProvider>
);
