/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { act, fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import {
  mockStateMapWithAnimationDelayWithoutLayers,
  mockStateMapWithMultipleLayers,
} from '../../../utils/testUtils';
import LayerContainerRow from './LayerContainerRow';
import { LayerManagerWidth } from '../LayerManagerUtils';
import { CoreThemeProvider } from '../../Providers/Providers';
import {
  defaultReduxLayerRadarKNMI,
  multiDimensionLayer,
} from '../../../utils/defaultTestSettings';

describe('src/components/LayerManager/LayerContainerRow', () => {
  const props = {
    mapId: 'mapId_1',
    layerManagerWidth: LayerManagerWidth.lg,
  };

  it('should render correct component', () => {
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = mockStore(mockState);
    const { queryByTestId } = render(
      <Provider store={store}>
        <LayerContainerRow {...props} />
      </Provider>,
    );
    expect(queryByTestId('layerContainerRow')).toBeTruthy();
  });

  it('should have a layer row for each layer in the default order', () => {
    const layers = [defaultReduxLayerRadarKNMI, multiDimensionLayer];
    const mockState = mockStateMapWithMultipleLayers(layers, props.mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);

    const { container } = render(
      <CoreThemeProvider>
        <Provider store={store}>
          <LayerContainerRow {...props} />
        </Provider>
      </CoreThemeProvider>,
    );
    const layerRows = container.querySelectorAll('*[data-testid^="layerRow"]');

    expect(layerRows.length).toEqual(layers.length);
    layerRows.forEach((row, index): void => {
      expect(row.getAttribute('data-testid')).toEqual(
        `layerRow-${layers[index].id}`,
      );
      expect(
        row
          .querySelector(`[data-testid="dragHandle-${index}"]`)
          .getAttribute('disabled'),
      ).toBeNull();
    });
  });

  it('should disable drag if only one layer', () => {
    const layers = [defaultReduxLayerRadarKNMI];
    const mockState = mockStateMapWithMultipleLayers(layers, props.mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);

    const { container } = render(
      <CoreThemeProvider>
        <Provider store={store}>
          <LayerContainerRow {...props} />
        </Provider>
      </CoreThemeProvider>,
    );
    const layerRows = container.querySelectorAll('*[data-testid^="layerRow"]');

    expect(layerRows.length).toEqual(layers.length);
    const dragHandle = layerRows[0].querySelector(
      '[data-testid="dragHandle-0"]',
    );
    expect(dragHandle.getAttribute('title')).toBeNull();
    expect(dragHandle.getAttribute('disabled')).toEqual('');
  });

  it('should call prevent default to prevent scrolling when ctrl or command is pressed', () => {
    const layers = [defaultReduxLayerRadarKNMI];
    const mockState = mockStateMapWithMultipleLayers(layers, props.mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);

    const { queryByTestId } = render(
      <CoreThemeProvider>
        <Provider store={store}>
          <LayerContainerRow {...props} />
        </Provider>
      </CoreThemeProvider>,
    );

    const layerContainerRow = queryByTestId('layerContainerRow');

    act(() => {
      // This simulates that keys are pressed (and kept pressed down)
      userEvent.keyboard('{ctrl>}');
      userEvent.keyboard('{meta>}');
      const isPrevented = fireEvent.wheel(layerContainerRow);

      expect(isPrevented).toBe(true);
    });
  });
});
