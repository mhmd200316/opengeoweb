/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { DragHandle as DragHandleIcon } from '@opengeoweb/theme';
import { ManagerButton } from '@opengeoweb/shared';

interface DragHandleProps {
  isDisabled?: boolean;
  index?: number;
  hideTooltip?: boolean;
}

export const TOOLTIP_TITLE = 'Drag';

const DragHandle: React.FC<DragHandleProps> = ({
  isDisabled = false,
  index = undefined,
  hideTooltip = false,
}: DragHandleProps) => {
  const tooltipTitle = hideTooltip ? '' : TOOLTIP_TITLE;

  return (
    <ManagerButton
      tooltipTitle={tooltipTitle}
      icon={<DragHandleIcon />}
      testId={`dragHandle${index !== undefined ? `-${index}` : ''}`}
      className="handle"
      tabIndex={-1}
      isEnabled={!isDisabled}
      disableFocusRipple
      disableTouchRipple
      disabled={isDisabled}
      isAccessible
      sx={{
        cursor: 'grab',
        '&:hover': {
          backgroundColor: 'inherit!important',
        },
        marginLeft: '3px',
        marginRight: '3px',
      }}
    />
  );
};

export default DragHandle;
