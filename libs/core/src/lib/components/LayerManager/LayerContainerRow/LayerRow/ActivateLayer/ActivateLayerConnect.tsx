/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { layerSelectors, mapActions, mapSelectors } from '../../../../../store';
import { LayerActionOrigin } from '../../../../../store/mapStore/layers/types';
import { AppStore } from '../../../../../types/types';
import ActivateLayer from './ActivateLayer';

interface ActivateLayerProps {
  layerId: string;
  mapId: string;
}

const ActivateLayerConnect: React.FC<ActivateLayerProps> = ({
  layerId,
  mapId,
}: ActivateLayerProps) => {
  const dispatch = useDispatch();
  const activeLayerId = useSelector((store: AppStore) =>
    mapSelectors.getActiveLayerId(store, mapId),
  );
  const isLayerEnabled = useSelector((store: AppStore) =>
    layerSelectors.getLayerEnabled(store, layerId),
  );

  const setActiveLayerId = React.useCallback(
    ({ layerId, mapId }) =>
      dispatch(
        mapActions.setActiveLayerId({
          layerId,
          mapId,
          origin: LayerActionOrigin.layerManager,
        }),
      ),
    [dispatch],
  );

  const isAutoUpdating = useSelector((store: AppStore) =>
    mapSelectors.isAutoUpdating(store, mapId),
  );
  const setAutoUpdateOn = React.useCallback(
    (mapId) =>
      dispatch(
        mapActions.toggleAutoUpdate({
          mapId,
          shouldAutoUpdate: true,
        }),
      ),
    [dispatch],
  );

  const isActive = activeLayerId === layerId;

  return (
    <ActivateLayer
      tooltipTitle="Leading layer"
      onClickActivate={(): void => {
        setActiveLayerId({ mapId, layerId });
        if (isAutoUpdating) {
          // Layer is now set active *and* autoupdate is currently on => autoupdate is desired to be set on.
          setAutoUpdateOn(mapId);
        }
      }}
      isActive={isActive}
      isEnabled={isLayerEnabled}
    />
  );
};

/**
 * Activates a maplayer from the store
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {string} layerId layerId: string - Id of the layer that is activated
 * @example
 * ``` <ActivateLayerConnect mapId={mapId} layerId={layerId} />```
 */
export default ActivateLayerConnect;
