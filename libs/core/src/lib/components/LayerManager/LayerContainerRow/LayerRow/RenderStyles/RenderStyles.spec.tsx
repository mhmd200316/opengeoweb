/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, findByRole, waitFor } from '@testing-library/react';
import RenderStyles from './RenderStyles';
import { defaultReduxLayerRadarKNMI } from '../../../../../utils/defaultTestSettings';
import { CoreThemeProvider } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/RenderStyles/RenderStyles', () => {
  const defaultProps = {
    onChangeLayerStyle: jest.fn(),
    layerStyles: defaultReduxLayerRadarKNMI.styles,
    currentLayerStyle: defaultReduxLayerRadarKNMI.style,
  };

  it('should show default style when layer not yet loaded', () => {
    const mockProps = {
      ...defaultProps,
      layerStyles: [],
      currentLayerStyle: '',
    };
    const { getByTestId } = render(
      <CoreThemeProvider>
        <RenderStyles {...mockProps} />
      </CoreThemeProvider>,
    );
    const styleSelect = getByTestId('selectStyle');

    expect(styleSelect).toBeTruthy();
    expect(styleSelect.textContent).toEqual('default');
  });

  it('should show the select style component with the first style selected', () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <RenderStyles {...defaultProps} />
      </CoreThemeProvider>,
    );
    const styleSelect = getByTestId('selectStyle');

    expect(styleSelect).toBeTruthy();
    expect(styleSelect.textContent).toEqual(defaultProps.layerStyles[0].name);
  });

  it('should trigger onChangeLayerStyle when a new style is selected', async () => {
    const { getByTestId, findByText } = render(
      <CoreThemeProvider>
        <RenderStyles {...defaultProps} />
      </CoreThemeProvider>,
    );
    const newStyleName = defaultProps.layerStyles[1].name;
    const styleSelect = getByTestId('selectStyle');

    fireEvent.mouseDown(styleSelect);

    const menuItem = await findByText(newStyleName);
    await waitFor(() => fireEvent.click(menuItem));

    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledWith(newStyleName);
    // showing the newStyleName in the select only works when this component is connected to the store, so no checking on that here
  });

  it('should call onChangeLayerStyle on wheel scroll only if the Ctrl key is pressed', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <RenderStyles {...defaultProps} />
      </CoreThemeProvider>,
    );
    const select = getByTestId('selectStyle');

    fireEvent.wheel(select, { ctrlKey: true, deltaY: 1 });
    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledTimes(1);
    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledWith(
      defaultProps.layerStyles[1].name,
    );

    fireEvent.wheel(select, { ctrlKey: true, deltaY: -1 });
    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledTimes(2);
    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledWith('default');

    fireEvent.wheel(select, { deltaY: -1 });
    expect(defaultProps.onChangeLayerStyle).toHaveBeenCalledTimes(2);
  });

  it('should show the style name in the tooltip', async () => {
    const { container, getByTestId } = render(
      <CoreThemeProvider>
        <RenderStyles {...defaultProps} />
      </CoreThemeProvider>,
    );
    const styleSelect = getByTestId('selectStyle');
    fireEvent.mouseOver(styleSelect);

    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain(defaultProps.currentLayerStyle);
  });
});
