/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { LayerActionOrigin } from '../../../../../store/mapStore/types';
import { defaultReduxLayerRadarColor } from '../../../../../utils/defaultTestSettings';

import { mockStateMapWithLayer } from '../../../../../utils/testUtils';
import { CoreThemeStoreProvider } from '../../../../Providers/Providers';
import ActivateLayerConnect from './ActivateLayerConnect';
import { mapActions } from '../../../../../store';

const { setActiveLayerId, toggleAutoUpdate } = mapActions;

describe('src/components/LayerManager/LayerContainerRow/LayerRow/ActivateLayer/ActivateLayerConnect', () => {
  it('should activate a layer', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = mockStateMapWithLayer(layer, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ActivateLayerConnect mapId={mapId} layerId={layer.id} />
      </CoreThemeStoreProvider>,
    );

    const button = getByTestId('activateLayerButton');

    fireEvent.click(button);

    const expectedAction = [
      setActiveLayerId({
        mapId,
        layerId: layer.id,
        origin: LayerActionOrigin.layerManager,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});

describe('src/components/LayerManager/LayerContainerRow/LayerRow/ActivateLayer/ActivateLayerConnect', () => {
  it('should autoupdate whenever autoupdating mode is on and layer is activated', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = mockStateMapWithLayer(layer, mapId);
    mockState.webmap.byId[mapId].isAutoUpdating = true; // Set auto-update on
    const mockStore = configureStore();
    const store = mockStore(mockState);

    store.addModules = jest.fn();

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ActivateLayerConnect mapId={mapId} layerId={layer.id} />
      </CoreThemeStoreProvider>,
    );

    const button = getByTestId('activateLayerButton');

    fireEvent.click(button);

    const expectedActions = [
      setActiveLayerId({
        mapId,
        layerId: layer.id,
        origin: LayerActionOrigin.layerManager,
      }),
      toggleAutoUpdate({ mapId, shouldAutoUpdate: true }),
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });
});

describe('src/components/LayerManager/LayerContainerRow/LayerRow/ActivateLayer/ActivateLayerConnect', () => {
  it('should NOT auto-update when auto-updating is off - even though layer had been set active', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <ActivateLayerConnect mapId={mapId} layerId={layer.id} />
      </CoreThemeStoreProvider>,
    );

    const button = getByTestId('activateLayerButton');

    fireEvent.click(button);

    const expectedAction = setActiveLayerId({
      mapId,
      layerId: layer.id,
      origin: LayerActionOrigin.layerManager,
    });

    const unacceptedAction = {
      payload: { mapId, layerId: layer.id },
      type: toggleAutoUpdate.type,
    };

    expect(mockState.webmap.byId[mapId].isAutoUpdating).toBeFalsy(); // Initially, auto-update should be off
    expect(store.getActions()).toContainEqual(expectedAction);
    expect(store.getActions()).not.toContainEqual(unacceptedAction);
  });
});
