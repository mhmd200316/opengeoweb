/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Copy } from '@opengeoweb/theme';
import { ToggleMenu } from '@opengeoweb/shared';

interface LayerManagerButtonProps {
  mapId: string;
  layerId?: string;
  onLayerDuplicate?: (payload: { mapId: string; layerId: string }) => void;
}

const LayerManagerMenuButton: React.FC<LayerManagerButtonProps> = ({
  mapId,
  layerId,
  onLayerDuplicate,
}: LayerManagerButtonProps) => {
  const onClickDuplicate = (): void => {
    onLayerDuplicate({ mapId, layerId });
  };

  return (
    <ToggleMenu
      buttonTestId="openMenuButton"
      buttonSx={{
        margin: '4px 2px',
        '&.MuiButtonBase-root': {
          backgroundColor: 'transparent',
          color: 'geowebColors.greys.accessible',
        },
      }}
      menuPosition="bottom"
      menuItems={[
        { text: 'Duplicate layer', action: onClickDuplicate, icon: <Copy /> },
      ]}
      tooltipTitle="Options"
    />
  );
};

export default LayerManagerMenuButton;
