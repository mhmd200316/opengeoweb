/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { layerActions, layerSelectors } from '../../../../../store';
import { AppStore } from '../../../../../types/types';

import OpacitySelect from './OpacitySelect';
import { LayerActionOrigin } from '../../../../../store/mapStore/layers/types';

interface OpacitySelectConnectProps {
  layerId: string;
  mapId: string;
}

const OpacitySelectConnect: React.FC<OpacitySelectConnectProps> = ({
  layerId,
  mapId,
}: OpacitySelectConnectProps) => {
  const dispatch = useDispatch();
  const opacity = useSelector((store: AppStore) =>
    layerSelectors.getLayerOpacity(store, layerId),
  );
  const isLayerEnabled = useSelector((store: AppStore) =>
    layerSelectors.getLayerEnabled(store, layerId),
  );

  return (
    <OpacitySelect
      currentOpacity={opacity}
      onLayerChangeOpacity={(newOpacity: number): void => {
        dispatch(
          layerActions.layerChangeOpacity({
            layerId,
            opacity: newOpacity,
            mapId,
            origin: LayerActionOrigin.layerManager,
          }),
        );
      }}
      isEnabled={isLayerEnabled}
    />
  );
};

/**
 * Contains a select allowing you to change the opacity of a layer on the map
 *
 * Expects the following props:
 * @param {string} layerId layerId: string - Id of the layer
 * @example
 * ``` <OpacitySelectConnect layerId="layerid_1"/> ```
 */
export default OpacitySelectConnect;
