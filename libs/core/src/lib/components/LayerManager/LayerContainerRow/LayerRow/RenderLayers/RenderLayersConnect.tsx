/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import RenderLayers from './RenderLayers';
import { layerActions, layerSelectors } from '../../../../../store';
import * as serviceSelectors from '../../../../../store/mapStore/service/selectors';
import { LayerActionOrigin } from '../../../../../store/mapStore/layers/types';
import { AppStore } from '../../../../../types/types';

interface RenderLayersProps {
  layerId: string;
  mapId: string;
}

const RenderLayersConnect: React.FC<RenderLayersProps> = ({
  layerId,
  mapId,
}: RenderLayersProps) => {
  const dispatch = useDispatch();
  const layerName = useSelector((store: AppStore) =>
    layerSelectors.getLayerName(store, layerId),
  );
  const layerService = useSelector((store: AppStore) =>
    layerSelectors.getLayerService(store, layerId),
  );
  const isLayerEnabled = useSelector((store: AppStore) =>
    layerSelectors.getLayerEnabled(store, layerId),
  );

  const layers = useSelector((store: AppStore) => {
    return serviceSelectors.getLayersFromService(store, layerService);
  });

  const layerChangeName = React.useCallback(
    ({ layerId, name }) =>
      dispatch(
        layerActions.layerChangeName({
          layerId,
          name,
          mapId,
          origin: LayerActionOrigin.layerManager,
        }),
      ),
    [dispatch, mapId],
  );
  return (
    <RenderLayers
      layers={layers}
      layerName={layerName}
      onChangeLayerName={(name): void => {
        layerChangeName({ layerId, name });
      }}
      isEnabled={isLayerEnabled}
    />
  );
};

export default RenderLayersConnect;
