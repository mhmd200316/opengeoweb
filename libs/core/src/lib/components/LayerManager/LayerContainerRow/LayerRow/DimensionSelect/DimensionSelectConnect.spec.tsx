/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import {
  multiDimensionLayer2,
  WmMultiDimensionLayer2,
} from '../../../../../utils/defaultTestSettings';

import DimensionSelectConnect from './DimensionSelectConnect';
import { mockStateMapWithDimensions } from '../../../../../utils/testUtils';
import { registerWMLayer } from '../../../../../store/mapStore/utils/helpers';
import { CoreThemeStoreProvider } from '../../../../Providers/Providers';
import { layerActions } from '../../../../../store';
import { LayerActionOrigin } from '../../../../../store/mapStore/layers/types';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/DimensionSelect/DimensionSelectConnect', () => {
  it('should set dimension value', async () => {
    const mapId = 'mapid_1';
    const layer = multiDimensionLayer2;
    const mockState = mockStateMapWithDimensions(layer, mapId);
    registerWMLayer(WmMultiDimensionLayer2, 'multiDimensionLayerMock2');

    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      layerId: layer.id,
      mapId: 'test-1',
    };

    const { findByText, findByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const select = await findByTestId('selectDimensionValue');

    fireEvent.mouseDown(select);
    const newName = {
      value: 'member3',
      title: 'member3',
    };

    const menuItem = await findByText(newName.title);

    await waitFor(() => fireEvent.click(menuItem));

    const expectedAction = [
      layerActions.layerChangeDimension({
        origin: undefined,
        layerId: props.layerId,
        mapId: props.mapId,
        dimension: {
          name: 'member',
          currentValue: layer.dimensions[0].currentValue,
        },
      }),
      layerActions.layerChangeDimension({
        origin: LayerActionOrigin.layerManager,
        layerId: props.layerId,
        mapId: props.mapId,
        dimension: {
          name: 'member',
          currentValue: newName.value,
        },
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
