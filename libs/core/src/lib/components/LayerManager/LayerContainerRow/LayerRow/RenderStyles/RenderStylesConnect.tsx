/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import RenderStyles from './RenderStyles';
import { layerActions, layerSelectors } from '../../../../../store';
import * as layersSelectors from '../../../../../store/mapStore/layers/selectors';
import * as serviceSelectors from '../../../../../store/mapStore/service/selectors';
import { AppStore } from '../../../../../types/types';
import { LayerActionOrigin } from '../../../../../store/mapStore/layers/types';

interface RenderStylesProps {
  layerId: string;
  mapId: string;
}

const RenderStylesConnect: React.FC<RenderStylesProps> = ({
  layerId,
  mapId,
}: RenderStylesProps) => {
  const dispatch = useDispatch();
  const layerService = useSelector((store: AppStore) =>
    layersSelectors.getLayerService(store, layerId),
  );
  const layerName = useSelector((store: AppStore) =>
    layersSelectors.getLayerName(store, layerId),
  );

  const layerStyles = useSelector((store: AppStore) => {
    return serviceSelectors.getLayerStyles(store, layerService, layerName);
  });

  const currentLayerStyle = useSelector((store: AppStore) =>
    layersSelectors.getLayerStyle(store, layerId),
  );
  const isLayerEnabled = useSelector((store: AppStore) =>
    layerSelectors.getLayerEnabled(store, layerId),
  );

  const layerChangeStyle = React.useCallback(
    (style) =>
      dispatch(
        layerActions.layerChangeStyle({
          layerId,
          style,
          mapId,
          origin: LayerActionOrigin.layerManager,
        }),
      ),
    [layerId, dispatch, mapId],
  );

  return (
    <RenderStyles
      layerStyles={layerStyles}
      currentLayerStyle={currentLayerStyle}
      onChangeLayerStyle={layerChangeStyle}
      isEnabled={isLayerEnabled}
    />
  );
};

export default RenderStylesConnect;
