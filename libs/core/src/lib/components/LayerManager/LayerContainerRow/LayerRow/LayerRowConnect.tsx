/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { useSelector } from 'react-redux';

import { AppStore } from '../../../../types/types';
import { layerSelectors, serviceSelectors } from '../../../../store';

import LayerRow from './LayerRow';
import { ServiceLayer } from '../../../../store/mapStore/types';
import DeleteLayerConnect from './DeleteLayer/DeleteLayerConnect';
import EnableLayerConnect from './EnableLayer/EnableLayerConnect';
import OpacityLayerConnect from './OpacitySelect/OpacitySelectConnect';
import DimensionSelectConnect from './DimensionSelect/DimensionSelectConnect';
import RenderStylesConnect from './RenderStyles/RenderStylesConnect';
import RenderLayersConnect from './RenderLayers/RenderLayersConnect';
import { LayerManagerWidth } from '../../LayerManagerUtils';
import LayerManagerMenuButtonConnect from './Menubutton/MenuButtonConnect';
import ActivateLayerConnect from './ActivateLayer/ActivateLayerConnect';

interface LayerRowConnectProps {
  layerId: string;
  mapId: string;
  layerManagerWidth: LayerManagerWidth;
  dragHandle: React.ReactElement;
  layerIndex: number;
}

const LayerRowConnect: React.FC<LayerRowConnectProps> = ({
  layerId,
  mapId,
  layerManagerWidth,
  dragHandle,
  layerIndex,
}: LayerRowConnectProps) => {
  const isEnabled = useSelector((store: AppStore) =>
    layerSelectors.getLayerEnabled(store, layerId),
  );
  const layerName = useSelector((store: AppStore) =>
    layerSelectors.getLayerName(store, layerId),
  );
  const layerService = useSelector((store: AppStore) =>
    layerSelectors.getLayerService(store, layerId),
  );
  const layers = useSelector((store: AppStore) => {
    return serviceSelectors.getLayersFromService(store, layerService);
  });

  const isLayerMissing =
    layers &&
    layers.length > 0 &&
    !layers.some((l: ServiceLayer) => l.name === layerName);

  return (
    <LayerRow
      isEnabled={isEnabled}
      mapId={mapId}
      layerId={layerId}
      isLayerMissing={isLayerMissing}
      layerEnableLayout={<EnableLayerConnect layerId={layerId} mapId={mapId} />}
      layerOpacityLayout={
        <OpacityLayerConnect layerId={layerId} mapId={mapId} />
      }
      layerServicesLayout={
        <RenderLayersConnect layerId={layerId} mapId={mapId} />
      }
      layerDimensionLayout={
        <DimensionSelectConnect layerId={layerId} mapId={mapId} />
      }
      layerStylesLayout={
        <RenderStylesConnect layerId={layerId} mapId={mapId} />
      }
      layerDeleteLayout={
        <DeleteLayerConnect
          mapId={mapId}
          layerId={layerId}
          layerIndex={layerIndex}
        />
      }
      layerMenuLayout={
        <LayerManagerMenuButtonConnect mapId={mapId} layerId={layerId} />
      }
      layerActiveLayout={
        <ActivateLayerConnect mapId={mapId} layerId={layerId} />
      }
      layerManagerWidth={layerManagerWidth}
      dragHandle={dragHandle}
    />
  );
};

export default LayerRowConnect;
