/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import DimensionSelect from './DimensionSelect';
import { layerActions, layerSelectors } from '../../../../../store';
import { AppStore } from '../../../../../types/types';
import { Dimension } from '../../../../../store/mapStore/types';

interface DimensionSelectProps {
  layerId: string;
  mapId: string;
}

const DimensionSelectConnect: React.FC<DimensionSelectProps> = ({
  layerId,
  mapId,
}: DimensionSelectProps) => {
  const dispatch = useDispatch();

  const layerDimensions = useSelector((store: AppStore) =>
    layerSelectors.getLayerNonTimeDimensions(store, layerId),
  );

  const isLayerEnabled = useSelector((store: AppStore) =>
    layerSelectors.getLayerEnabled(store, layerId),
  );

  const layerChangeDimension = React.useCallback(
    ({ dimension, origin }) =>
      dispatch(
        layerActions.layerChangeDimension({
          layerId,
          dimension,
          mapId,
          origin,
        }),
      ),
    [layerId, dispatch, mapId],
  );

  return (
    <DimensionSelect
      layerId={layerId}
      layerDimensions={layerDimensions}
      onLayerChangeDimension={(
        dimensionName: string,
        dimensionValue: string,
        origin,
      ): void => {
        const dimension: Dimension = {
          name: dimensionName,
          currentValue: dimensionValue,
        };
        layerChangeDimension({
          dimension,
          origin,
        });
      }}
      isEnabled={isLayerEnabled}
    />
  );
};

export default DimensionSelectConnect;
