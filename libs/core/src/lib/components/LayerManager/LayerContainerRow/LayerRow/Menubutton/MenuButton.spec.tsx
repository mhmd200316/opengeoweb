/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import LayerManagerMenuButton from './MenuButton';
import { CoreThemeProvider } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/MenuButton', () => {
  it('should render correct component', () => {
    const props = {
      mapId: 'mapId_1',
    };
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <LayerManagerMenuButton {...props} />
      </CoreThemeProvider>,
    );
    expect(queryByTestId('openMenuButton')).toBeTruthy();
  });

  it('should activate onClickDuplicate when clicked', () => {
    const props = {
      mapId: 'mapId_1',
      layerId: 'layer_1',
      onLayerDuplicate: jest.fn(),
    };

    const { queryByTestId, getByText } = render(
      <CoreThemeProvider>
        <LayerManagerMenuButton {...props} />
      </CoreThemeProvider>,
    );

    const button = queryByTestId('openMenuButton');
    fireEvent.click(button);
    expect(props.onLayerDuplicate).toHaveBeenCalledTimes(0);
    fireEvent.click(getByText('Duplicate layer'));
    expect(props.onLayerDuplicate).toHaveBeenCalledTimes(1);
    expect(props.onLayerDuplicate).toHaveBeenCalledWith({
      mapId: props.mapId,
      layerId: props.layerId,
    });
  });
});
