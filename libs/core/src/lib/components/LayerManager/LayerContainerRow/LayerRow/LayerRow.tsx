/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Grid } from '@mui/material';
import React from 'react';
import {
  ManagerDeleteButton,
  AlertBanner,
  EnableButton,
} from '@opengeoweb/shared';
import DimensionSelect from './DimensionSelect/DimensionSelect';
import RenderLayers from './RenderLayers/RenderLayers';
import OpacitySelect from './OpacitySelect/OpacitySelect';
import LayerManagerMenuButton from './Menubutton/MenuButton';
import RenderStyles from './RenderStyles/RenderStyles';
import { Dimension, Layer, Services } from '../../../../store/mapStore/types';
import { filterNonTimeDimensions } from '../../../../store/mapStore/utils/helpers';
import {
  LayerManagerColumnsLarge,
  LayerManagerColumnsSmall,
  LayerManagerWidth,
} from '../../LayerManagerUtils';
import ActivateLayer from './ActivateLayer/ActivateLayer';

interface LayerRowProps {
  layerId?: string;
  layer?: Layer;
  mapId: string;
  onLayerRowClick?: (layerId?: string) => void;
  onLayerEnable?: (payload: { layerId: string; enabled: boolean }) => void;
  onLayerChangeName?: (payload: { layerId: string; name: string }) => void;
  onLayerChangeStyle?: (payload: { layerId: string; style: string }) => void;
  onLayerChangeOpacity?: (payload: {
    layerId: string;
    opacity: number;
  }) => void;
  onLayerChangeDimension?: (payload: {
    origin: string;
    layerId: string;
    dimension: Dimension;
  }) => void;
  onLayerDelete?: (payload: { mapId: string; layerId: string }) => void;
  onLayerDuplicate?: (payload: { mapId: string; layerId: string }) => void;
  layerEnableLayout?: React.ReactChild;
  layerServicesLayout?: React.ReactChild;
  layerStylesLayout?: React.ReactChild;
  layerOpacityLayout?: React.ReactChild;
  layerDimensionLayout?: React.ReactChild;
  layerDeleteLayout?: React.ReactChild;
  layerMenuLayout?: React.ReactChild;
  layerActiveLayout?: React.ReactChild;
  services?: Services;
  layerManagerWidth: LayerManagerWidth;
  isEnabled?: boolean;
  dragHandle?: React.ReactElement;
  isLayerMissing?: boolean;
}

const LayerRow: React.FC<LayerRowProps> = ({
  layerId,
  layer,
  services,
  mapId,
  onLayerRowClick = (): void => null,
  isEnabled = true,
  isLayerMissing = false,
  onLayerEnable,
  onLayerChangeName,
  onLayerChangeStyle,
  onLayerChangeOpacity,
  onLayerChangeDimension,
  onLayerDelete,
  onLayerDuplicate,
  // layout
  layerEnableLayout,
  layerServicesLayout,
  layerStylesLayout,
  layerOpacityLayout,
  layerDimensionLayout,
  layerDeleteLayout,
  layerMenuLayout,
  layerActiveLayout,
  layerManagerWidth,
  dragHandle,
}: LayerRowProps) => {
  const columnSizes =
    layerManagerWidth === LayerManagerWidth.sm
      ? LayerManagerColumnsSmall
      : LayerManagerColumnsLarge;
  const onClickRow = (): void => onLayerRowClick(layerId);

  return (
    <Grid
      item
      container
      data-testid={`layerRow-${layerId}`}
      sx={{
        backgroundColor: isEnabled
          ? 'geowebColors.layerManager.tableRowDefaultCardContainer.fill'
          : 'geowebColors.layerManager.tableRowDisabledCardContainer.fill',
        border: 1,
        borderColor: isEnabled
          ? 'geowebColors.layerManager.tableRowDefaultCardContainer.borderColor'
          : 'geowebColors.layerManager.tableRowDisabledCardContainer.borderColor',
        borderRadius: 1,
        marginBottom: 0.5,
        height: '34px',
        '&.sortable-drag': {
          boxShadow: 4,
        },
        '&.sortable-ghost': {
          opacity: 0.5,
        },
      }}
    >
      <Grid item sx={columnSizes.column1}>
        {dragHandle}
        {layerEnableLayout || (
          <EnableButton
            isEnabled={layer.enabled}
            title={layer.name}
            onChangeEnableLayer={(): void => {
              onLayerEnable({ layerId, enabled: !layer.enabled });
            }}
          />
        )}
        {layerActiveLayout || <ActivateLayer onClickActivate={onClickRow} />}
      </Grid>
      <Grid item sx={columnSizes.column2}>
        {layerServicesLayout || (
          <RenderLayers
            layerName={layer.name}
            layers={
              (services &&
                layer.service &&
                services[layer.service] &&
                services[layer.service].layers) ||
              []
            }
            onChangeLayerName={(name): void => {
              onLayerChangeName({ layerId: layer.id, name });
            }}
          />
        )}
      </Grid>
      {isLayerMissing ? (
        <Grid item sx={columnSizes.columns35}>
          <AlertBanner
            title="This layer could not be loaded: does not exist on the server."
            isCompact
          />
        </Grid>
      ) : (
        <>
          <Grid item sx={columnSizes.column3}>
            {layerStylesLayout || (
              <RenderStyles
                layerStyles={layer.styles}
                currentLayerStyle={layer.style}
                onChangeLayerStyle={(style: string): void => {
                  onLayerChangeStyle({ layerId, style });
                }}
              />
            )}
          </Grid>
          <Grid item sx={columnSizes.column4}>
            {layerOpacityLayout || (
              <OpacitySelect
                currentOpacity={layer.opacity}
                onLayerChangeOpacity={(_opacity: number): void => {
                  onLayerChangeOpacity({
                    layerId: layer.id,
                    opacity: _opacity,
                  });
                }}
              />
            )}
          </Grid>
          <Grid item sx={columnSizes.column5}>
            {layerDimensionLayout || (
              <DimensionSelect
                layerId={layerId}
                layerDimensions={filterNonTimeDimensions(layer.dimensions)}
                onLayerChangeDimension={(
                  dimensionName: string,
                  dimensionValue: string,
                ): void => {
                  const dimension: Dimension = {
                    name: dimensionName,
                    currentValue: dimensionValue,
                  };
                  onLayerChangeDimension({
                    origin: 'layerrow',
                    layerId,
                    dimension,
                  });
                }}
              />
            )}
          </Grid>
        </>
      )}

      <Grid item sx={columnSizes.column6}>
        {layerDeleteLayout || (
          <ManagerDeleteButton
            tooltipTitle="Delete"
            onClickDelete={(): void => {
              onLayerDelete({ mapId, layerId });
            }}
          />
        )}

        {layerMenuLayout || (
          <LayerManagerMenuButton
            mapId={mapId}
            layerId={layerId}
            onLayerDuplicate={onLayerDuplicate}
          />
        )}
      </Grid>
    </Grid>
  );
};

export default LayerRow;
