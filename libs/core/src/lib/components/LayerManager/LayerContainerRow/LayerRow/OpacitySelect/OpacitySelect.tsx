/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Tooltip, Mark, Button, Box } from '@mui/material';
import {
  CustomSlider,
  sliderHeaderStyle,
  tooltipContainerStyles,
} from '@opengeoweb/shared';

interface OpacitySelectProps {
  currentOpacity: number;
  onLayerChangeOpacity: (opacity: number) => void;
  isEnabled?: boolean;
}

const marks: Mark[] = [
  {
    value: 0,
    label: '0 %',
  },
  {
    value: 0.1,
  },
  {
    value: 0.2,
  },
  {
    value: 0.3,
  },
  {
    value: 0.4,
  },
  {
    value: 0.5,
    label: '50 %',
  },
  {
    value: 0.6,
  },
  {
    value: 0.7,
  },
  {
    value: 0.8,
  },
  {
    value: 0.9,
  },
  {
    value: 1,
    label: '100 %',
  },
];

const OpacitySelect: React.FC<OpacitySelectProps> = ({
  currentOpacity,
  onLayerChangeOpacity,
  isEnabled = true,
}: OpacitySelectProps) => {
  const [open, setOpen] = React.useState(false);
  const [tooltipOpen, setTooltipOpen] = React.useState(false);

  const onWheel = React.useCallback(
    (event: React.WheelEvent): void => {
      if (event.ctrlKey || event.metaKey) {
        const multiplier = event.altKey ? 10 : 1;
        const direction =
          event.deltaY > 0 ? 0.01 * multiplier : -0.01 * multiplier;
        const newValue = Math.min(Math.max(currentOpacity - direction, 0), 1);

        // Kind of "one frame debouncer", meant to dampen too fast slider movement
        window.requestAnimationFrame(() => {
          onLayerChangeOpacity(newValue);
        });
      }
    },
    [currentOpacity, onLayerChangeOpacity],
  );

  const onClickButton = React.useCallback((): void => {
    setOpen(!open);
  }, [open]);
  const onKeyDown = React.useCallback(
    (event): void => {
      if (event.key === 'Tab') {
        setOpen(!open);
      }
    },
    [open],
  );
  const onMouseEnter = React.useCallback(() => {
    if (!open) setTooltipOpen(true);
  }, [open]);
  const onMouseLeave = React.useCallback(() => {
    setTooltipOpen(false);
  }, []);
  const onFocus = React.useCallback(() => {
    setTooltipOpen(true);
  }, []);
  const onBlur = React.useCallback(() => {
    setOpen(false);
    setTooltipOpen(false);
  }, []);

  const currentOpacityText = `${Math.round(currentOpacity * 100)}  %`;
  return (
    <Box
      onWheel={onWheel}
      data-testid="scrollOpacity"
      sx={{
        height: 'calc(100% - 5px)',
        width: '100%',
        position: 'relative',
        top: '2px',
      }}
    >
      <Tooltip
        title={`Opacity: ${currentOpacityText}`}
        sx={tooltipContainerStyles(isEnabled)}
        placement="top"
        open={tooltipOpen}
      >
        <Button
          tabIndex={0}
          onClick={onClickButton}
          data-testid="selectOpacity"
          disableRipple
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
          onFocus={onFocus}
          onBlur={onBlur}
          style={{
            height: '100%',
            width: '100%',
            borderRadius: '0',
            minWidth: '49px',
          }}
        >
          {currentOpacityText}
        </Button>
      </Tooltip>
      {open ? (
        <Box
          sx={{
            padding: '0 0 60px 0',
            marginLeft: '60px',
            backgroundColor: 'geowebColors.background.surface',
            width: '82px',
            height: '248px',
            position: 'fixed',
            top: '-5px',
            boxSizing: 'initial',
            zIndex: 2,
            boxShadow: 8,
          }}
        >
          <Box sx={sliderHeaderStyle}>Opacity</Box>
          <CustomSlider
            data-testid="opacitySlider"
            orientation="vertical"
            value={currentOpacity}
            step={0.01}
            max={1}
            min={0}
            marks={marks}
            onChange={(event, newValue: number): void => {
              event.stopPropagation();
              onLayerChangeOpacity(newValue);
            }}
            onKeyDown={onKeyDown}
            onFocus={(): void => {
              setOpen(true);
            }}
            onBlur={(): void => {
              setOpen(false);
            }}
          />
        </Box>
      ) : null}
    </Box>
  );
};

export default OpacitySelect;
