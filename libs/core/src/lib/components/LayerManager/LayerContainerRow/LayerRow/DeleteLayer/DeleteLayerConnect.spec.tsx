/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { defaultReduxLayerRadarColor } from '../../../../../utils/defaultTestSettings';

import DeleteLayerConnect from './DeleteLayerConnect';
import { mockStateMapWithLayer } from '../../../../../utils/testUtils';
import { CoreThemeStoreProvider } from '../../../../Providers/Providers';
import { layerActions } from '../../../../../store';
import { LayerActionOrigin } from '../../../../../store/mapStore/layers/types';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/MenuButton/DeleteLayer/DeleteLayerConnect', () => {
  it('should delete a layer', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = mockStateMapWithLayer(layer, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn();

    const { getByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <DeleteLayerConnect mapId={mapId} layerId={layer.id} layerIndex={0} />
      </CoreThemeStoreProvider>,
    );

    const button = getByTestId('deleteButton');

    fireEvent.click(button);

    const expectedAction = [
      layerActions.layerDelete({
        mapId,
        layerId: layer.id,
        layerIndex: 0,
        origin: LayerActionOrigin.layerManager,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
