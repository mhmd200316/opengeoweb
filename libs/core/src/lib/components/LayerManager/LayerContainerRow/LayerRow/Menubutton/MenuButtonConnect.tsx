/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { layerActions, layerSelectors } from '../../../../../store';

import LayerManagerMenuButton from './MenuButton';
import { AppStore } from '../../../../../types/types';
import { generateLayerId } from '../../../../../store/mapStore/utils/helpers';
import { LayerActionOrigin } from '../../../../../store/mapStore/types';

interface LayerManagerButtonProps {
  mapId: string;
  layerId?: string;
}

const LayerManagerMenuButtonConnect: React.FC<LayerManagerButtonProps> = ({
  layerId,
  mapId,
}: LayerManagerButtonProps) => {
  const dispatch = useDispatch();

  const layer = useSelector((store: AppStore) =>
    layerSelectors.getLayerById(store, layerId),
  );
  const addLayer = React.useCallback(
    ({ mapId, layerId, layer }) =>
      dispatch(
        layerActions.addLayer({
          mapId,
          layerId,
          layer,
          origin: LayerActionOrigin.layerManager,
        }),
      ),
    [dispatch],
  );

  return (
    <LayerManagerMenuButton
      layerId={layerId}
      mapId={mapId}
      onLayerDuplicate={(): void => {
        addLayer({
          layerId: generateLayerId(),
          mapId,
          layer,
        });
      }}
    />
  );
};

export default LayerManagerMenuButtonConnect;
