/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { MenuItem, Typography, SelectChangeEvent, Box } from '@mui/material';

import { AlertIcon, TooltipSelect } from '@opengeoweb/shared';
import { ServiceLayer } from '../../../../../store/mapStore/types';

interface RenderLayersProps {
  onChangeLayerName: (name: string) => void;
  layerName: string;
  layers: ServiceLayer[];
  isEnabled?: boolean;
}

const getRenderLayersValues = (
  layerName: string,
  layers: ServiceLayer[],
): {
  currentValue: string;
  currentIndex: number;
  extendedLayers: ServiceLayer[];
} => {
  const serviceLayer = layers.find((l) => l.name === layerName);
  if (!serviceLayer && layerName) {
    return {
      currentValue: layerName,
      currentIndex: layers.length,
      extendedLayers: [
        ...layers,
        {
          name: layerName,
          text: layerName,
          leaf: true,
          path: [],
        },
      ],
    };
  }
  return {
    currentValue: serviceLayer?.name || '',
    currentIndex: layers.findIndex((l) => l.name === serviceLayer?.name),
    extendedLayers: layers,
  };
};

const RenderLayers: React.FC<RenderLayersProps> = ({
  onChangeLayerName,
  layerName,
  layers = [],
  isEnabled = true,
}: RenderLayersProps) => {
  if (!layers || !layers.length) {
    return (
      <Box
        sx={{
          paddingLeft: 1,
          height: '32px',
          display: 'inline-flex',
          alignItems: 'center',
        }}
      >
        <Typography
          variant="body1"
          sx={{
            fontSize: 'geowebColors.layerManager.tableRowDefaultText.fontSize',
            color: 'geowebColors.layerManager.tableRowDefaultText.rgba',
            fontWeight: 500,
          }}
        >
          No service available
        </Typography>
      </Box>
    );
  }

  const selectLayer = (event: SelectChangeEvent): void => {
    event.stopPropagation();
    onChangeLayerName(event.target.value);
  };

  const { currentIndex, currentValue, extendedLayers } = getRenderLayersValues(
    layerName,
    layers,
  );

  const list = extendedLayers.map((layer) => ({
    value: layer.name,
  }));

  return (
    <TooltipSelect
      disableUnderline
      tooltip={`Layer: ${extendedLayers[currentIndex]?.text}`}
      inputProps={{
        SelectDisplayProps: {
          'data-testid': 'selectLayer',
        },
      }}
      isEnabled={isEnabled}
      value={currentValue}
      list={list}
      currentIndex={currentIndex}
      onChange={selectLayer}
      onChangeMouseWheel={(e): void => onChangeLayerName(e.value)}
      requiresCtrlToChange={true}
      renderValue={(value): string =>
        extendedLayers.find((layer): boolean => layer.name === value).text
      } // Omit possible warning sign in the selected item due to layout effects
    >
      <MenuItem disabled>Layer</MenuItem>
      {extendedLayers.map(
        (layerFromServiceLayers: ServiceLayer, currentIndex) => (
          <MenuItem
            key={layerFromServiceLayers.name}
            value={layerFromServiceLayers.name}
            disabled={!layerFromServiceLayers.leaf} // show categories as disabled
            sx={{ '& .MuiSvgIcon-root': { marginLeft: '28px' } }}
          >
            {layerFromServiceLayers.text}
            {currentIndex === layers.length && <AlertIcon severity="error" />}
          </MenuItem>
        ),
      )}
    </TooltipSelect>
  );
};

export default RenderLayers;
