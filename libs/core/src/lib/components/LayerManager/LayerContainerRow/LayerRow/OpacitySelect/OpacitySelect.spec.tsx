/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, findByRole, waitFor } from '@testing-library/react';
import OpacitySelect from './OpacitySelect';
import { CoreThemeProvider } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/OpacitySelect/OpacitySelect', () => {
  const props = {
    currentOpacity: 0.8,
    onLayerChangeOpacity: jest.fn(),
  };

  it('should display passed opacity value', async () => {
    const { findByText } = render(
      <CoreThemeProvider>
        <OpacitySelect {...props} />
      </CoreThemeProvider>,
    );

    expect(await findByText('80 %')).toBeTruthy();
  });

  it('should call onChangeOpacity on mouseClick', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <OpacitySelect {...props} />
      </CoreThemeProvider>,
    );
    const select = getByTestId('selectOpacity');
    fireEvent.click(select);
    const slider = getByTestId('opacitySlider');
    expect(slider).toBeTruthy();
    // fireEvent.click does not call a function, have to use here mouseDown - mouseUp
    fireEvent.mouseDown(slider, { clientY: 1 });
    fireEvent.mouseUp(slider);
    expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(1);
    expect(props.onLayerChangeOpacity).toHaveBeenCalledWith(0);

    fireEvent.mouseDown(slider, { clientY: -1 });
    fireEvent.mouseUp(slider);
    expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(2);
    expect(props.onLayerChangeOpacity).toHaveBeenCalledWith(1);
  });

  it('should call onChangeOpacity on wheel scroll when Ctrl or meta key is pressed', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <OpacitySelect {...props} />
      </CoreThemeProvider>,
    );
    const select = getByTestId('selectOpacity');

    fireEvent.wheel(select, { ctrlKey: true, deltaY: 1 });
    await waitFor(() => {
      expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(1);
      expect(props.onLayerChangeOpacity).toHaveBeenCalledWith(0.79);
    });

    fireEvent.wheel(select, { metaKey: true, deltaY: -1 });
    await waitFor(() => {
      expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(2);
      expect(props.onLayerChangeOpacity).toHaveBeenCalledWith(0.81);
    });
  });

  it('should call onChangeOpacity with 10% steps on wheel scroll while alt key is pressed', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <OpacitySelect {...props} />
      </CoreThemeProvider>,
    );
    const select = getByTestId('selectOpacity');

    fireEvent.wheel(select, { altKey: true, ctrlKey: true, deltaY: 1 });
    await waitFor(() => {
      expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(1);
      const calls1 = props.onLayerChangeOpacity.mock.calls;
      expect(calls1[0][0]).toBeCloseTo(0.7);
    });

    fireEvent.wheel(select, { altKey: true, ctrlKey: true, deltaY: -1 });
    await waitFor(() => {
      expect(props.onLayerChangeOpacity).toHaveBeenCalledTimes(2);
      const calls2 = props.onLayerChangeOpacity.mock.calls;
      expect(calls2[1][0]).toBeCloseTo(0.9);
    });
  });

  it('should have the correct tooltip', async () => {
    const { container, getByTestId } = render(
      <CoreThemeProvider>
        <OpacitySelect {...props} />
      </CoreThemeProvider>,
    );
    fireEvent.mouseOver(getByTestId('selectOpacity'));
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toMatch(
      `${Math.round(props.currentOpacity * 100)}  %`,
    );
  });
});
