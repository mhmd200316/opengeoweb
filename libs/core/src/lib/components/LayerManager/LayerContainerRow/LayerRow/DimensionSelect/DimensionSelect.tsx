/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { MenuItem, Grid } from '@mui/material';
import { TooltipSelect } from '@opengeoweb/shared';
import {
  Dimension,
  LayerActionOrigin,
} from '../../../../../store/mapStore/types';
import { getWMJSDimensionForLayerAndDimension } from '../../../../../store/mapStore/utils/helpers';

interface DimensionSelectProps {
  layerId: string;
  layerDimensions?: Dimension[];
  onLayerChangeDimension: (
    dimensionName: string,
    dimensionValue: string,
    origin?: LayerActionOrigin.layerManager,
  ) => void;
  isEnabled?: boolean;
}

const DimensionSelect: React.FC<DimensionSelectProps> = ({
  layerId,
  onLayerChangeDimension,
  layerDimensions = [],
  isEnabled,
}: DimensionSelectProps) => {
  const [activeDimName, setActiveDimName] = React.useState(
    layerDimensions.length ? layerDimensions[0].name : '',
  );

  React.useEffect(() => {
    const activeDim = layerDimensions.find((d) => d.name === activeDimName);
    if (!activeDim) {
      setActiveDimName(layerDimensions.length ? layerDimensions[0].name : '');
    }
  }, [layerDimensions, activeDimName]);

  React.useEffect(() => {
    const activeDim = layerDimensions.find((d) => d.name === activeDimName);
    if (activeDim) {
      onLayerChangeDimension(activeDim.name, activeDim.currentValue);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!layerDimensions.length || !layerId) return null;

  const getValuesToDisplay = (): string[] => {
    const activeWMJSDim =
      getWMJSDimensionForLayerAndDimension(layerId, activeDimName) ||
      getWMJSDimensionForLayerAndDimension(layerId, layerDimensions[0].name);
    if (!activeWMJSDim) return null;

    const availableDimValues = [];
    for (let j = 0; j < activeWMJSDim.size(); j += 1) {
      availableDimValues.push(activeWMJSDim.getValueForIndex(j));
    }
    availableDimValues.reverse();

    // Only display last 100 values, to avoid performance issues
    // TODO: (2020-06-29 Tineke van Rijn) replace this with lazy loading
    return availableDimValues.slice(0, 100);
  };

  const valuesToDisplay = getValuesToDisplay();

  if (valuesToDisplay === null) return null;

  const activeDim = layerDimensions.find(
    (dim): boolean => dim.name === activeDimName,
  );
  if (!activeDim) return null;

  const selectDimensionValue = (
    event: React.ChangeEvent<HTMLInputElement>,
  ): void => {
    event.stopPropagation();
    if (activeDim) {
      onLayerChangeDimension(
        activeDim.name,
        event.target.value,
        LayerActionOrigin.layerManager,
      );
    }
  };

  const onChangeActiveDimensionName = (
    event: React.ChangeEvent<HTMLInputElement>,
  ): void => {
    event.stopPropagation();
    setActiveDimName(event.target.value);
  };

  const dimNameList = layerDimensions.map((dimension) => ({
    value: dimension.name,
  }));
  const dimNameCurrentIndex = layerDimensions.findIndex(
    (value) => value.name === activeDim.name,
  );

  const dimValueList = valuesToDisplay.map((value) => ({
    value,
  }));
  const dimValueCurrentIndex = valuesToDisplay.findIndex(
    (value) => value === activeDim.currentValue,
  );
  if (dimValueCurrentIndex === -1) return null;

  return (
    <Grid
      container
      direction="row"
      justifyContent="flex-start"
      alignItems="center"
    >
      <Grid item xs={12}>
        <TooltipSelect
          disableUnderline
          tooltip={`Dimensions: ${activeDimName}`}
          inputProps={{
            SelectDisplayProps: {
              'data-testid': 'selectDimension',
            },
          }}
          isEnabled={isEnabled}
          value={activeDimName}
          list={dimNameList}
          currentIndex={dimNameCurrentIndex}
          onChange={onChangeActiveDimensionName}
          onChangeMouseWheel={(e): void => setActiveDimName(e.value)}
          style={{
            maxWidth: '50%',
          }}
        >
          <MenuItem disabled>Dimension</MenuItem>
          {layerDimensions.map((dimension) => (
            <MenuItem key={dimension.name} value={dimension.name}>
              {dimension.name}
            </MenuItem>
          ))}
        </TooltipSelect>

        <TooltipSelect
          disableUnderline
          tooltip={`Dimensions: ${activeDimName} ${activeDim.currentValue} ${activeDim.units}`}
          inputProps={{
            SelectDisplayProps: {
              'data-testid': 'selectDimensionValue',
            },
          }}
          isEnabled={isEnabled}
          value={activeDim && activeDim.currentValue}
          list={dimValueList}
          currentIndex={dimValueCurrentIndex}
          onChange={selectDimensionValue}
          onChangeMouseWheel={(e): void =>
            onLayerChangeDimension(
              activeDim.name,
              e.value,
              LayerActionOrigin.layerManager,
            )
          }
          style={{
            maxWidth: '50%',
          }}
        >
          <MenuItem disabled>
            Dimension value ({valuesToDisplay.length} options)
          </MenuItem>
          {valuesToDisplay.map((dimValue) => (
            <MenuItem key={dimValue} value={dimValue}>
              {`${dimValue}`}
              <>
                &nbsp;
                <i>{`${activeDim.units}`}</i>
              </>
            </MenuItem>
          ))}
        </TooltipSelect>
      </Grid>
    </Grid>
  );
};

export default DimensionSelect;
