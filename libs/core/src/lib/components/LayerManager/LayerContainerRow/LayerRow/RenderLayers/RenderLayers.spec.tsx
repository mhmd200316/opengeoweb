/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  fireEvent,
  waitFor,
  findAllByRole,
} from '@testing-library/react';
import { ServiceLayer } from '../../../../../store/mapStore/types';

import RenderLayers from './RenderLayers';
import {
  defaultReduxLayerRadarColor,
  defaultReduxServices,
} from '../../../../../utils/defaultTestSettings';
import { CoreThemeProvider } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/RenderLayers/RenderLayers', () => {
  const defaultProps = {
    layers: defaultReduxServices['serviceid_1'].layers,
    layerName: defaultReduxLayerRadarColor.name,
    onChangeLayerName: jest.fn(),
  };

  it('should show a message if no layers are provided', async () => {
    const mockProps = {
      ...defaultProps,
      layers: [],
    };

    const { queryByTestId, queryByText } = render(
      <CoreThemeProvider>
        <RenderLayers {...mockProps} />
      </CoreThemeProvider>,
    );

    expect(queryByText('No service available')).toBeTruthy();
    expect(queryByTestId('selectLayer')).toBeFalsy();
  });

  it('should show the select layer component with the first layer selected if layers are provided', () => {
    const { queryByText, getByTestId } = render(
      <CoreThemeProvider>
        <RenderLayers {...defaultProps} />
      </CoreThemeProvider>,
    );
    const selectLayer = getByTestId('selectLayer');

    expect(queryByText('No service available')).toBeFalsy();
    expect(selectLayer).toBeTruthy();
    expect(selectLayer.textContent).toEqual(defaultProps.layers[0].text);
  });

  it('should trigger onChangeLayerName if a new layer is selected', async () => {
    const { getByTestId, findByText } = render(
      <CoreThemeProvider>
        <RenderLayers {...defaultProps} />
      </CoreThemeProvider>,
    );
    const newLayer = defaultProps.layers[1];
    const selectLayer = getByTestId('selectLayer');

    fireEvent.mouseDown(selectLayer);

    const menuItem = await findByText(newLayer.text);
    await waitFor(() => fireEvent.click(menuItem));

    expect(defaultProps.onChangeLayerName).toHaveBeenCalledWith(newLayer.name);
  });

  it('should call onChangeLayerName on wheel scroll only if the Ctrl key is pressed', async () => {
    const { getByTestId } = render(
      <CoreThemeProvider>
        <RenderLayers {...defaultProps} />
      </CoreThemeProvider>,
    );
    const select = getByTestId('selectLayer');

    fireEvent.wheel(select, { deltaY: 1 });
    expect(defaultProps.onChangeLayerName).toHaveBeenCalledTimes(0);

    fireEvent.wheel(select, { ctrlKey: true, deltaY: 1 });
    expect(defaultProps.onChangeLayerName).toHaveBeenCalledTimes(1);
    expect(defaultProps.onChangeLayerName).toHaveBeenCalledWith(
      defaultProps.layers[1].name,
    );

    fireEvent.wheel(select, { ctrlKey: true, deltaY: -1 });
    expect(defaultProps.onChangeLayerName).toHaveBeenCalledTimes(2);
    expect(defaultProps.onChangeLayerName).toHaveBeenCalledWith(
      defaultProps.layers[0].name,
    );
  });

  it('should show only categories as disabled', async () => {
    const mockProps = {
      ...defaultProps,
      layers: [
        {
          name: 'Category',
          text: 'Category',
          leaf: false,
          path: [],
        },
        {
          name: 'Layername',
          text: 'Layername',
          leaf: true,
          path: ['Category'],
        },
      ],
    };
    const { getByTestId, findByText } = render(
      <CoreThemeProvider>
        <RenderLayers {...mockProps} />
      </CoreThemeProvider>,
    );

    const selectLayer = getByTestId('selectLayer');
    fireEvent.mouseDown(selectLayer);

    const category = await findByText(mockProps.layers[0].text);
    expect(category.className).toContain('disabled');

    const layer = await findByText(mockProps.layers[1].text);
    expect(layer.className).not.toContain('disabled');
  });

  it('should show passed in layerName as selected layer even if missing from original layer list', () => {
    const extraLayerName = 'Name of missing layer';
    const propsWithExtraLayer = { ...defaultProps, layerName: extraLayerName };
    const { getByTestId } = render(
      <CoreThemeProvider>
        <RenderLayers {...propsWithExtraLayer} />
      </CoreThemeProvider>,
    );
    const selectLayer = getByTestId('selectLayer');
    expect(selectLayer).toBeTruthy();
    expect(selectLayer.textContent).toBe(extraLayerName);
  });

  it('should show passed in layerName among layer options even if missing from original layer list', async () => {
    const extraLayerName = 'Name of missing layer';
    const propsWithExtraLayer = { ...defaultProps, layerName: extraLayerName };
    const { getByTestId, container } = render(
      <CoreThemeProvider>
        <RenderLayers {...propsWithExtraLayer} />
      </CoreThemeProvider>,
    );
    const selectLayer = getByTestId('selectLayer');
    expect(selectLayer).toBeTruthy();
    fireEvent.mouseDown(selectLayer);

    const options = await findAllByRole(container.parentElement, 'option');
    const optionTexts = options.map((option) => option.textContent);
    const expectedLayerTexts = defaultProps.layers.map(
      (l: ServiceLayer) => l.text,
    );

    expect(optionTexts).toEqual(expect.arrayContaining(expectedLayerTexts));
    expect(optionTexts).toContain(extraLayerName);
  });

  it('should show an error icon if the layer is missing', async () => {
    const extraLayerName = 'Name of missing layer';
    const propsWithExtraLayer = { ...defaultProps, layerName: extraLayerName };
    const { queryByTestId, getByTestId } = render(
      <CoreThemeProvider>
        <RenderLayers {...propsWithExtraLayer} />
      </CoreThemeProvider>,
    );
    const selectLayer = getByTestId('selectLayer');
    expect(selectLayer).toBeTruthy();
    fireEvent.mouseDown(selectLayer);

    expect(queryByTestId('alert-icon')).toBeTruthy();
  });

  it('should not show an error icon if the layer is not missing', async () => {
    const propsWithExtraLayer = { ...defaultProps };
    const { queryByTestId, getByTestId } = render(
      <CoreThemeProvider>
        <RenderLayers {...propsWithExtraLayer} />
      </CoreThemeProvider>,
    );

    const selectLayer = getByTestId('selectLayer');
    expect(selectLayer).toBeTruthy();
    fireEvent.mouseDown(selectLayer);

    expect(queryByTestId('alert-icon')).toBeNull();
  });
});
