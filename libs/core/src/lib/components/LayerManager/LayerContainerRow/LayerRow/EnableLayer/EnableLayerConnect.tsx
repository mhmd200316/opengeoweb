/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { EnableButton } from '@opengeoweb/shared';
import { useDispatch, useSelector } from 'react-redux';

import { layerActions, layerSelectors } from '../../../../../store';
import { LayerActionOrigin } from '../../../../../store/mapStore/layers/types';

import { AppStore } from '../../../../../types/types';

interface EnableLayerProps {
  layerId: string;
  mapId: string;
}

const EnableLayerConnect: React.FC<EnableLayerProps> = ({
  layerId,
  mapId,
}: EnableLayerProps) => {
  const dispatch = useDispatch();
  const isEnabled = useSelector((store: AppStore) =>
    layerSelectors.getLayerEnabled(store, layerId),
  );

  const layerChangeEnabled = React.useCallback(
    ({ layerId, enabled }) =>
      dispatch(
        layerActions.layerChangeEnabled({
          layerId,
          enabled,
          mapId,
          origin: LayerActionOrigin.layerManager,
        }),
      ),
    [dispatch, mapId],
  );

  return (
    <EnableButton
      isEnabled={isEnabled}
      title="Toggle visibility"
      onChangeEnableLayer={(enabled: boolean): void => {
        layerChangeEnabled({
          layerId,
          enabled,
        });
      }}
    />
  );
};

/**
 * Allows you to show/hide a layer on the map
 *
 * Expects the following props:
 * @param {string} layerId layerId: string - Id of the layer that is toggled
 * @example
 * ``` <EnableLayerConnect layerId={layerId} />```
 */
export default EnableLayerConnect;
