/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { ManagerDeleteButton } from '@opengeoweb/shared';
import { useDispatch, useSelector } from 'react-redux';

import { layerActions, layerSelectors } from '../../../../../store';
import { LayerActionOrigin } from '../../../../../store/mapStore/layers/types';
import { AppStore } from '../../../../../types/types';

interface DeleteLayerProps {
  layerId: string;
  mapId: string;
  layerIndex: number;
}

const DeleteLayerConnect: React.FC<DeleteLayerProps> = ({
  layerId,
  mapId,
  layerIndex,
}: DeleteLayerProps) => {
  const dispatch = useDispatch();
  const layerDelete = React.useCallback(
    ({ mapId, layerId, layerIndex }) =>
      dispatch(
        layerActions.layerDelete({
          mapId,
          layerId,
          layerIndex,
          origin: LayerActionOrigin.layerManager,
        }),
      ),
    [dispatch],
  );
  const isLayerEnabled = useSelector((store: AppStore) =>
    layerSelectors.getLayerEnabled(store, layerId),
  );
  return (
    <ManagerDeleteButton
      tooltipTitle="Delete"
      onClickDelete={(): void => {
        layerDelete({ mapId, layerId, layerIndex });
      }}
      isEnabled={isLayerEnabled}
    />
  );
};

/**
 * Deletes a maplayer from the store
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {string} layerId layerId: string - Id of the layer that is deleted
 * @example
 * ``` <DeleteLayerConnect mapId={mapId} layerId={layerId} />```
 */
export default DeleteLayerConnect;
