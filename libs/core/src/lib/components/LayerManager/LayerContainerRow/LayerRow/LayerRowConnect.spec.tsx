/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { defaultReduxLayerRadarKNMI } from '../../../../utils/defaultTestSettings';

import { mockStateMapWithLayer } from '../../../../utils/testUtils';
import { CoreThemeStoreProvider } from '../../../Providers/Providers';
import LayerRowConnect from './LayerRowConnect';
import DragHandle from './DragHandle/DragHandle';
import { LayerType } from '../../../../store/mapStore/types';

const missingLayer = {
  service: 'https://testservice',
  name: 'missing layer name',
  title: 'missing layer title',
  format: 'image/png',
  style: 'knmiradar/nearest',
  enabled: true,
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T13:30:00Z',
    },
  ],
  styles: [
    {
      title: 'knmiradar/nearest',
      name: 'knmiradar/nearest',
      legendURL:
        'https://geoservices.knmi.nl/wms?dataset=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RADNL_OPER_R___25PCPRR_L3_KNMI&format=image/png&STYLE=knmiradar/nearest',
      abstract: 'No abstract available',
    },
  ],
  id: 'layerid_2',
};

describe('core/LayerManger/LayerContainerRow/LayerRow/LayerRowConnect', () => {
  it('should create LayerRow with no alert if layer is not missing from service', () => {
    const mapId = 'mapid_1';
    // the given layer is included in default services
    const layer = defaultReduxLayerRadarKNMI;
    const mockState = mockStateMapWithLayer(layer, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { queryByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerRowConnect
          layerId={layer.id}
          dragHandle={<DragHandle index={0} hideTooltip />}
          mapId={mapId}
          layerManagerWidth={1000}
          layerIndex={0}
        />
      </CoreThemeStoreProvider>,
    );

    expect(queryByRole('alert')).toBeNull();
  });

  it('should create LayerRow with alert if layer is missing from service', () => {
    const mapId = 'mapid_1';
    // the missing layer is not included in default services
    const mockState = mockStateMapWithLayer(missingLayer, mapId);
    const mockStore = configureStore();
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByRole } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerRowConnect
          layerId={missingLayer.id}
          dragHandle={<DragHandle index={0} hideTooltip />}
          mapId={mapId}
          layerManagerWidth={1000}
          layerIndex={0}
        />
      </CoreThemeStoreProvider>,
    );

    expect(getByRole('alert')).toBeTruthy();
  });
});
