/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { SxProps, Theme } from '@mui/material';
import { Flag } from '@opengeoweb/theme';
import { ManagerButton } from '@opengeoweb/shared';

const makeIconStyle = (
  isActive: boolean,
  isEnabled: boolean,
): SxProps<Theme> => ({
  color: (): string => {
    if (isActive) {
      return 'common.white';
    }
    if (isEnabled) {
      return 'geowebColors.buttons.flat.default.color';
    }
    return 'geowebColors.buttons.flat.disabled.color';
  },
});

interface ActivateLayerProps {
  tooltipTitle?: string;
  onClickActivate: () => void;
  isActive?: boolean;
  isEnabled?: boolean;
}

const ActivateLayer: React.FC<ActivateLayerProps> = ({
  tooltipTitle = 'Leading layer',
  onClickActivate,
  isActive = false,
  isEnabled = true,
}: ActivateLayerProps) => {
  const iconStyle = makeIconStyle(isActive, isEnabled);

  return (
    <ManagerButton
      tooltipTitle={tooltipTitle}
      onClick={onClickActivate}
      icon={<Flag sx={iconStyle} />}
      testId="activateLayerButton"
      isActive={isActive}
    />
  );
};

export default ActivateLayer;
