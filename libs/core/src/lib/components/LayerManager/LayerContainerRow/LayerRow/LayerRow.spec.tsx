/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import LayerRow from './LayerRow';
import {
  layerWithoutTimeDimension,
  defaultReduxServices,
} from '../../../../utils/defaultTestSettings';
import { LayerManagerWidth } from '../../LayerManagerUtils';
import { CoreThemeProvider } from '../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/LayerRow', () => {
  const props = {
    mapId: 'mapId_1',
    layer: layerWithoutTimeDimension,
    layerId: layerWithoutTimeDimension.id,
    layerManagerWidth: LayerManagerWidth.lg,
    onLayerChangeDimension: jest.fn(),
  };

  it('should render correct component and subcomponents', () => {
    const { queryByTestId } = render(
      <CoreThemeProvider>
        <LayerRow {...props} />
      </CoreThemeProvider>,
    );
    expect(
      queryByTestId(`layerRow-${layerWithoutTimeDimension.id}`),
    ).toBeTruthy();
    expect(queryByTestId('deleteButton')).toBeTruthy();
  });

  it('should be able to click on a row', () => {
    const defaultProps = {
      services: defaultReduxServices,
      layer: layerWithoutTimeDimension,
      mapId: 'map_1',
      layerId: 'test-1',
      onLayerRowClick: jest.fn(),
      onLayerChangeDimension: jest.fn(),
      layerManagerWidth: LayerManagerWidth.lg,
    };

    const { queryByTestId } = render(
      <CoreThemeProvider>
        <LayerRow {...defaultProps} />
      </CoreThemeProvider>,
    );
    const row = queryByTestId('activateLayerButton');

    fireEvent.click(row);

    expect(defaultProps.onLayerRowClick).toHaveBeenCalledWith(
      defaultProps.layerId,
    );
  });

  it('should be able to pass a draghandle', async () => {
    const TestComponent: React.ReactElement = <div>test draghandle</div>;
    const testProps = {
      ...props,
      dragHandle: TestComponent,
    };
    const { findByText } = render(
      <CoreThemeProvider>
        <LayerRow {...testProps} />
      </CoreThemeProvider>,
    );
    expect(await findByText('test draghandle')).toBeTruthy();
  });

  it('should contain an alert banner if selected layer is missing from service', () => {
    const propsWithMissingLayer = { ...props, isLayerMissing: true };
    const { getByRole } = render(
      <CoreThemeProvider>
        <LayerRow {...propsWithMissingLayer} />
      </CoreThemeProvider>,
    );
    expect(getByRole('alert')).toBeTruthy();
  });

  it('should contain no alert banner if selected layer is not missing from service', () => {
    const propsWithNoMissingLayer = { ...props, isLayerMissing: false };
    const { queryByRole } = render(
      <CoreThemeProvider>
        <LayerRow {...propsWithNoMissingLayer} />
      </CoreThemeProvider>,
    );
    expect(queryByRole('alert')).toBeNull();
  });
});
