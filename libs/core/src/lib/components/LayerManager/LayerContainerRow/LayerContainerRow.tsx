/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Grid } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { ItemInterface, ReactSortable, Sortable } from 'react-sortablejs';
import { AppStore } from '../../../types/types';
import { LayerManagerWidth } from '../LayerManagerUtils';
import LayerRowConnect from './LayerRow/LayerRowConnect';
import DragHandle from './LayerRow/DragHandle/DragHandle';
import { mapActions, mapSelectors } from '../../../store';
import { LayerActionOrigin } from '../../../store/mapStore/types';

interface LayerContainerRowProps {
  mapId: string;
  layerManagerWidth: LayerManagerWidth;
}

const LayerContainerRow: React.FC<LayerContainerRowProps> = ({
  mapId,
  layerManagerWidth,
}: LayerContainerRowProps) => {
  const dispatch = useDispatch();
  const [keyPressed, setKeyPressed] = React.useState(false);

  const layerIds = useSelector((store: AppStore) =>
    mapSelectors.getLayerIds(store, mapId),
  );

  const layerMoveLayer = React.useCallback(
    ({ mapId, oldIndex, newIndex }) =>
      dispatch(
        mapActions.layerMoveLayer({
          mapId,
          oldIndex,
          newIndex,
          origin: LayerActionOrigin.layerManager,
        }),
      ),
    [dispatch],
  );

  const [activeDragIndex, setActiveDragIndex] = React.useState(null);

  const onSetList = (): void => {}; // since we use activeDragIndex, we don't need inner state handling of ReactSortable

  const onStart = React.useCallback(({ oldIndex }) => {
    setActiveDragIndex(oldIndex);
    Sortable.ghost.style.opacity = '1';
  }, []);

  const onEnd = React.useCallback(() => {
    if (activeDragIndex !== null) {
      setActiveDragIndex(null);
    }
  }, [activeDragIndex]);

  const onSortEnd = ({ oldIndex, newIndex }): void => {
    setActiveDragIndex(null);
    layerMoveLayer({
      mapId,
      oldIndex,
      newIndex,
    });
  };

  const isSorting = activeDragIndex !== null;
  const isDragDisabled = layerIds.length === 1;

  const keyPressedTrue = (event): void => {
    if (event.ctrlKey || event.metaKey) {
      setKeyPressed(true);
    }
  };

  const keyPressedFalse = (): void => {
    setKeyPressed(false);
  };

  React.useEffect(() => {
    window.addEventListener('keydown', keyPressedTrue);
    window.addEventListener('keyup', keyPressedFalse);
    return (): void => {
      window.removeEventListener('keydown', keyPressedTrue);
      window.removeEventListener('keyup', keyPressedFalse);
    };
  }, []);

  const preventDefault = React.useCallback((event: WheelEvent) => {
    event.preventDefault();
    event.stopPropagation();
  }, []);

  React.useEffect(() => {
    if (keyPressed === true) {
      window.addEventListener('wheel', preventDefault, { passive: false });
    } else {
      window.removeEventListener('wheel', preventDefault);
    }
    return (): void => window.removeEventListener('wheel', preventDefault);
  }, [keyPressed, preventDefault]);

  return (
    <Grid
      container
      item
      data-testid="layerContainerRow"
      sx={{
        width: '100%',
        maxHeight: 'calc(100% - 10px)',
        overflow: 'auto',
      }}
    >
      <ReactSortable
        tag="div"
        list={layerIds.map(
          (layerId): ItemInterface => ({
            id: layerId,
          }),
        )}
        setList={onSetList}
        animation={200}
        onSort={onSortEnd}
        handle=".handle"
        direction="vertical"
        // hover props
        forceFallback={true}
        onStart={onStart}
        onEnd={onEnd}
        style={{
          width: '100%',
          cursor: isSorting ? 'grabbing' : 'inherit',
        }}
      >
        {layerIds.map((layerId, index) => (
          <LayerRowConnect
            mapId={mapId}
            layerId={layerId}
            layerIndex={index}
            layerManagerWidth={layerManagerWidth}
            key={`layerRowConnect-${layerId}`}
            dragHandle={
              <DragHandle
                isDisabled={isDragDisabled}
                index={index}
                hideTooltip={isSorting || isDragDisabled}
              />
            }
          />
        ))}
      </ReactSortable>
    </Grid>
  );
};

/**
 * A Connected container component for rendering a sortable set of rows
 *
 * Expects the following props:
 * @param {string} mapId mapId: string - Id of the map
 * @param {LayerManagerWidth} layerManagerWidth layerManagerWidth: LayerManagerWidth - Width of the Layer Manager
 * @example
 * ``` <LayerContainerRow mapId={mapId} layerManagerWidth={LayerManagerWidth.lg} />```
 */
export default LayerContainerRow;
