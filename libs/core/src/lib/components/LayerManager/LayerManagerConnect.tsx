/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { uiActions, mapSelectors, uiSelectors } from '../../store';
import { Layer } from '../../store/mapStore/types';
import { AppStore } from '../../types/types';
import LayerManager from './LayerManager';
import LayerSelectConnect from './LayerSelect/LayerSelectConnect';
import { Service } from '../WMSLoader/services';

interface LayerManagerConnectProps {
  preloadedAvailableBaseLayers?: Layer[];
  preloadedMapServices?: Service[];
  preloadedBaseServices?: Service[];
  bounds?: string;
  showTitle?: boolean;
  layerSelect?: boolean;
  leftComponent?: React.ReactNode;
}

/**
 * Layer Managerconnected to the store displaying the layers forthe active map Id
 * Please note that in order to use this and open/close the dialog, every map on the screen that should be able to show
 * a dialog should have the <LayerManagerMapButtonConnect/> which is used to focus the correct map and open the dialog
 *
 * Expects the following props:
 * @param {Service[]} preloadedMapServices preloadedMapServices: array of Service objects - contains an array of Service objects that are preloaded into in the WMS Loader for maplayers
 * @param {Service[]} preloadedBaseServices preloadedBaseServices: array of Service objects - contains an array of Service objects that are preloaded into in the WMS Loader fpr baselayers
 * @param {Layer[]} preloadedAvailableBaseLayers preloadedAvailableBaseLayers: array of Layer objects - contains an array of Layer objects (consisting solely of type baseLayer) that are available to be shown as basemaps
 * ``` <LayerManagerConnect />```
 */
const LayerManagerConnect: React.FC<LayerManagerConnectProps> = ({
  preloadedAvailableBaseLayers,
  preloadedMapServices,
  preloadedBaseServices,
  bounds,
  showTitle = false,
  layerSelect = false,
  leftComponent = null,
}: LayerManagerConnectProps) => {
  const dispatch = useDispatch();

  const isOpenInStore = useSelector((store) =>
    uiSelectors.getisDialogOpen(store, 'layerManager'),
  );

  const mapId = useSelector((store) =>
    uiSelectors.getDialogMapId(store, 'layerManager'),
  );

  const onClose = React.useCallback(
    () =>
      dispatch(
        uiActions.setToggleOpenDialog({
          type: 'layerManager',
          setOpen: false,
        }),
      ),
    [dispatch],
  );

  const isMapPresent = useSelector((store: AppStore) =>
    mapSelectors.getIsMapPresent(store, mapId),
  );

  const uiOrder = useSelector((store) =>
    uiSelectors.getDialogOrder(store, 'layerManager'),
  );

  const uiSource = useSelector((store: AppStore) =>
    uiSelectors.getDialogSource(store, 'layerManager'),
  );

  const uiIsOrderedOnTop = useSelector((store) =>
    uiSelectors.getDialogIsOrderedOnTop(store, 'layerManager'),
  );

  const isMapPresetLoading = useSelector((store: AppStore) =>
    mapSelectors.getIsMapPresetLoading(store, mapId),
  );

  const mapPresetError = useSelector((store: AppStore) =>
    mapSelectors.getMapPresetError(store, mapId),
  );

  // Check to ensure the currently active map is still present on screen - if not, close the dialog
  React.useEffect(() => {
    if (mapId !== '' && !isMapPresent) {
      onClose();
    }
  }, [mapId, isMapPresent, onClose]);

  const registerDialog = React.useCallback(
    () =>
      dispatch(
        uiActions.registerDialog({
          type: 'layerManager',
          setOpen: false,
        }),
      ),
    [dispatch],
  );
  const unregisterDialog = React.useCallback(
    () =>
      dispatch(
        uiActions.unregisterDialog({
          type: 'layerManager',
        }),
      ),
    [dispatch],
  );

  const onOrderDialog = React.useCallback(() => {
    if (!uiIsOrderedOnTop) {
      dispatch(
        uiActions.orderDialog({
          type: 'layerManager',
        }),
      );
    }
  }, [dispatch, uiIsOrderedOnTop]);

  // Register this dialog in the store
  React.useEffect(() => {
    registerDialog();
    return (): void => {
      unregisterDialog();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <LayerManager
        mapId={mapId}
        preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
        preloadedMapServices={preloadedMapServices}
        preloadedBaseServices={preloadedBaseServices}
        bounds={bounds}
        isOpen={isOpenInStore}
        onClose={onClose}
        showTitle={showTitle}
        onMouseDown={onOrderDialog}
        order={uiOrder}
        source={uiSource}
        layerSelect={layerSelect}
        leftComponent={leftComponent}
        isLoading={isMapPresetLoading}
        error={mapPresetError}
      />
      {layerSelect && (
        <LayerSelectConnect preloadedServices={preloadedMapServices} />
      )}
    </>
  );
};

export default LayerManagerConnect;
