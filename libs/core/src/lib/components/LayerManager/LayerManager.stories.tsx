/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import configureStore from 'redux-mock-store';
import { WMLayer } from '@opengeoweb/webmap';
import { darkTheme, lightTheme } from '@opengeoweb/theme';

import LayerManager from './LayerManager';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { LayerType } from '../../store/mapStore/types';
import { registerWMLayer } from '../../store/mapStore/utils/helpers';
import { mockStateMapWithMultipleLayers } from '../../utils/testUtils';

interface LayerManagerDemoProps {
  mapId: string;
  isLoading?: boolean;
  error?: string;
}

const LayerManagerDemo: React.FC<LayerManagerDemoProps> = ({
  mapId,
  isLoading = false,
  error,
}: LayerManagerDemoProps) => {
  const [isOpen, setIsOpen] = React.useState(true);
  const onClose = (): void => {
    setIsOpen(false);
  };

  return (
    <LayerManager
      mapId={mapId}
      isOpen={isOpen}
      onClose={onClose}
      showAddLayersTooltip={false}
      isLoading={isLoading}
      error={error}
    />
  );
};

const mapId = 'mapid_1';
const mockBaseLayer = {
  mapId,
  name: 'WorldMap_Light_Grey_Canvas',
  type: 'twms',
  dimensions: [],
  id: 'baseGrey_mapid_1',
  opacity: 1,
  enabled: true,
  layerType: LayerType.baseLayer,
};

const mockRadarLayer = {
  mapId,
  service: 'serviceId',
  name: 'RAD_NL25_PCP_CM',
  format: 'image/png',
  style: 'radar/nearest',
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2021-12-13T16:00:00Z',
      maxValue: '2021-12-13T16:00:00Z',
      minValue: '2021-03-31T09:25:00Z',
      values: '2021-03-31T09:25:00Z/2021-12-13T16:00:00Z/PT5M',
    },
  ],
  styles: [
    {
      title: 'radar/nearest',
      name: 'radar/nearest',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
    {
      title: 'precip-rainbow/nearest',
      name: 'precip-rainbow/nearest',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
  ],
  id: 'layerid_41',
  opacity: 1,
  enabled: true,
  layerType: LayerType.mapLayer,
};

const mockElevationLayer = {
  mapId,
  service: 'serviceId',
  name: 'ELEVATION_LAYER',
  format: 'image/png',
  style: 'temp',
  dimensions: [
    {
      name: 'elevation',
      units: 'hPa',
      currentValue: '850',
      maxValue: '1000',
      minValue: '200',
      timeInterval: null,
      values: '200,300,400,500,600,700,800,850,900,925,950,1000',
      synced: false,
    },
  ],
  styles: [
    {
      title: 'Temperature (wow) shaded + contours',
      name: 'temp',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
    {
      title: 'Automatic minimum and maximum',
      name: 'min-max',
      legendURL: 'url',
      abstract: 'No abstract available',
    },
  ],
  id: 'layerid_42',
  opacity: 0.8,
  enabled: false,
  layerType: LayerType.mapLayer,
};

const mockPressureLayer = {
  mapId,
  service: 'serviceId',
  name: 'air_pressure_at_sea_level',
  dimensions: [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2022-03-29T09:00:00Z',
      minValue: '2022-03-29T09:00:00Z',
      maxValue: '2022-03-31T09:00:00Z',
      synced: false,
    },
    {
      name: 'reference_time',
      units: 'ISO8601',
      currentValue: '2022-03-29T09:00:00Z',
      minValue: '2022-03-27T00:00:00Z',
      maxValue: '2022-03-29T09:00:00Z',
      timeInterval: null,
      synced: false,
    },
  ],
  id: 'layerid_20',
  opacity: 1,
  enabled: true,
  layerType: LayerType.mapLayer,
  style: 'pressure_cwk/contour',
};

const wmMockElevationLayer = new WMLayer(mockElevationLayer);
registerWMLayer(wmMockElevationLayer, mockElevationLayer.id);

const mockState1 = mockStateMapWithMultipleLayers(
  [mockRadarLayer, mockElevationLayer, mockPressureLayer, mockBaseLayer],
  mapId,
);

const mockState = {
  ...mockState1,
  services: {
    byId: {
      serviceId: {
        serviceUrl: mockRadarLayer.service,
        layers: [
          {
            name: 'RAD_NL25_PCP_CM',
            text: 'Precipitation Radar NL',
            leaf: true,
            path: [],
            keywords: [],
            styles: [
              {
                title: 'radar/nearest',
                name: 'radar/nearest',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
              {
                title: 'precip-rainbow/nearest',
                name: 'precip-rainbow/nearest',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
            ],
          },
          {
            name: 'ELEVATION_LAYER',
            text: 'Temperature (PL)',
            leaf: true,
            path: [],
            keywords: [],
            styles: [
              {
                title: 'Temperature (wow) shaded + contours',
                name: 'temp',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
              {
                title: 'Automatic minimum and maximum',
                name: 'min-max',
                legendURL: 'url',
                abstract: 'No abstract available',
              },
            ],
          },
        ],
      },
    },
    allIds: ['serviceId'],
  },
};

const mockStore = configureStore();
const store = mockStore(mockState);
store.addModules = (): void => null; // mocking the dynamic module loader

export const LayerManagerLightTheme = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <LayerManagerDemo mapId={mapId} />
  </CoreThemeStoreProvider>
);

export const LayerManagerDarkTheme = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <LayerManagerDemo mapId={mapId} />
  </CoreThemeStoreProvider>
);

LayerManagerLightTheme.storyName = 'LayerManager Light Theme (takeSnapshot)';
LayerManagerDarkTheme.storyName = 'LayerManager Dark Theme (takeSnapshot)';

export const LayerManagerLoadingState = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <LayerManagerDemo mapId={mapId} isLoading={true} />
  </CoreThemeStoreProvider>
);

export const LayerManagerWithErrorLight = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={lightTheme} store={store}>
    <LayerManagerDemo
      mapId={mapId}
      error="Preset could not be loaded: Select a different one or try again."
    />
  </CoreThemeStoreProvider>
);

export const LayerManagerWithErrorDark = (): React.ReactElement => (
  <CoreThemeStoreProvider theme={darkTheme} store={store}>
    <LayerManagerDemo
      mapId={mapId}
      error="Preset could not be loaded: Select a different one or try again."
    />
  </CoreThemeStoreProvider>
);

LayerManagerWithErrorLight.storyName =
  'LayerManager With Error Light (takeSnapshot)';
LayerManagerWithErrorDark.storyName =
  'LayerManager With Error Dark (takeSnapshot)';
