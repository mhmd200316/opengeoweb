/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  findByRole,
  fireEvent,
  queryByRole,
  render,
  waitFor,
} from '@testing-library/react';
import configureStore from 'redux-mock-store';
import AddLayersButton from './AddLayersButton';
import { defaultReduxLayerRadarColor } from '../../../utils/defaultTestSettings';
import { mockStateMapWithLayer } from '../../../utils/testUtils';
import {
  CoreThemeProvider,
  CoreThemeStoreProvider,
} from '../../Providers/Providers';

describe('src/components/LayerManager/AddLayersButton/AddLayersButton', () => {
  const props = {
    mapId: 'mapId_1',
    tooltip: 'Add layers',
  };
  const mapId = 'mapid_1';
  const layer = defaultReduxLayerRadarColor;
  const mockState = mockStateMapWithLayer(layer, mapId);
  const mockStore = configureStore();
  const store = mockStore(mockState);
  store.addModules = jest.fn(); // mocking the dynamic module loader

  it('should render correct component', async () => {
    const { queryByTestId, container } = render(
      <CoreThemeProvider>
        <AddLayersButton />
      </CoreThemeProvider>,
    );
    expect(queryByTestId('addLayersButton')).toBeTruthy();

    const tooltip = queryByRole(container.parentElement, 'tooltip');
    expect(tooltip).toBeFalsy();
  });

  it('should show a title text "Add layers" in tooltip when hovering', async () => {
    const { getByTestId, container } = render(
      <CoreThemeStoreProvider store={store}>
        <AddLayersButton {...props} />
      </CoreThemeStoreProvider>,
    );
    const addLayersButton = getByTestId('addLayersButton');
    expect(addLayersButton).toBeTruthy();
    fireEvent.mouseOver(addLayersButton);
    // Wait until tooltip appears
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain('Add layers');

    fireEvent.mouseOut(addLayersButton);
    await waitFor(() => {
      expect(queryByRole(container.parentElement, 'tooltip')).toBeFalsy();
    });
  });

  it('should show a title text "Add layers" in tooltip on focus', async () => {
    const { getByTestId, container } = render(
      <CoreThemeStoreProvider store={store}>
        <AddLayersButton {...props} />
      </CoreThemeStoreProvider>,
    );
    const addLayersButton = getByTestId('addLayersButton');
    expect(addLayersButton).toBeTruthy();
    fireEvent.focus(addLayersButton);
    // Wait until tooltip appears
    const tooltip = await findByRole(container.parentElement, 'tooltip');
    expect(tooltip.textContent).toContain('Add layers');

    fireEvent.blur(addLayersButton);
    await waitFor(() => {
      expect(queryByRole(container.parentElement, 'tooltip')).toBeFalsy();
    });
  });

  it('should open addLayersPopup on click', async () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <AddLayersButton {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('addLayersPopup')).toBeFalsy();
    fireEvent.click(queryByTestId('addLayersButton'));
    await waitFor(() => expect(queryByTestId('addLayersPopup')).toBeTruthy());
  });

  it('should have autofocus', async () => {
    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <AddLayersButton {...props} shouldFocus />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('addLayersPopup')).toBeFalsy();

    await waitFor(() =>
      expect(document.activeElement).toEqual(queryByTestId('addLayersButton')),
    );

    expect(
      queryByTestId('addLayersButton').classList.contains('Mui-focusVisible'),
    ).toBeTruthy();
  });
});
