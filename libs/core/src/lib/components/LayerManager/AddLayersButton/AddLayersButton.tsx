/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { Tooltip, IconButton } from '@mui/material';

import { Add } from '@opengeoweb/theme';
import { Service } from '../../WMSLoader/services';
import { LayerType } from '../../../store/mapStore/types';
import AddLayersPopup from '../AddLayersPopup/AddLayersPopup';
import { preloadedDefaultMapServices } from '../../../utils/defaultConfigurations';

const styles = {
  buttonDiv: {
    position: 'relative',
  },
  loadingButton: {
    position: 'absolute',
    top: 7,
    left: 7,
    zIndex: 1,
  },
  button: {
    width: '24px',
    height: '24px',
    margin: '6px 0px 0px 10px',
    borderRadius: '15%',
  },
};

interface AddLayerButtonProps {
  onRenderTree?: (service: Service) => React.ReactChild;
  preloadedServices?: Service[];
  tooltip?: string;
  layerType?: LayerType;
  shouldFocus?: boolean;
}

const AddLayersButton: React.FC<AddLayerButtonProps> = ({
  onRenderTree,
  tooltip = '',
  preloadedServices = preloadedDefaultMapServices,
  layerType = LayerType.mapLayer,
  shouldFocus = false,
}: AddLayerButtonProps) => {
  const ref = React.useRef(null);
  const [open, setOpen] = React.useState<boolean>(false);
  const [isTooltipDisabled, setTooltipDisabled] =
    React.useState<boolean>(shouldFocus);

  const handleClose = (): void => {
    setOpen(!open);
  };

  const onBlurTooltip = (event: React.FocusEvent<HTMLElement>): void => {
    // enable tooltip again after user blurs
    if (event.relatedTarget) {
      setTooltipDisabled(false);
    }
  };

  React.useEffect(() => {
    if (shouldFocus && ref.current) {
      ref.current.focus();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Tooltip
        title={tooltip}
        disableFocusListener={isTooltipDisabled}
        onBlur={onBlurTooltip}
      >
        <IconButton
          onClick={handleClose}
          sx={styles.button}
          disableRipple
          data-testid="addLayersButton"
          ref={ref}
          size="large"
        >
          <Add />
        </IconButton>
      </Tooltip>
      <AddLayersPopup
        open={open}
        handleClose={handleClose}
        onRenderTree={onRenderTree}
        preloadedServices={preloadedServices}
        layerType={layerType}
      />
    </>
  );
};

export default AddLayersButton;
