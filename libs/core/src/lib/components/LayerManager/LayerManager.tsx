/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box, Grid, LinearProgress } from '@mui/material';
import { AlertBanner, ToolContainerDraggable } from '@opengeoweb/shared';
import DescriptionRow from './DescriptionRow/DescriptionRow';
import LayerContainerRow from './LayerContainerRow/LayerContainerRow';
import BaseLayerRow from './BaseLayerRow/BaseLayerRow';
import { LayerManagerWidth, widthToWidthClass } from './LayerManagerUtils';
import { Layer } from '../../store/mapStore/types';
import { Source } from '../../store/ui/types';
import { Service } from '../WMSLoader/services';

interface LayerManagerProps {
  mapId: string;
  preloadedAvailableBaseLayers?: Layer[];
  preloadedMapServices?: Service[];
  preloadedBaseServices?: Service[];
  bounds?: string;
  showTitle?: boolean;
  onClose: () => void;
  onMouseDown?: () => void;
  isOpen: boolean;
  order?: number;
  source?: Source;
  layerSelect?: boolean;
  showAddLayersTooltip?: boolean;
  leftComponent?: React.ReactNode;
  isLoading?: boolean;
  error?: string;
}
const styles = {
  layerRowContainer: {
    position: 'relative',
    width: '100%',
    height: 'calc(100% - 59px)',
    display: 'inline-block',
  },
  layerManagerContainer: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    boxSizing: 'border-box',
  },
};

const LayerManager: React.FC<LayerManagerProps> = ({
  mapId,
  preloadedAvailableBaseLayers,
  preloadedMapServices,
  preloadedBaseServices,
  bounds,
  onClose,
  showTitle,
  isOpen,
  onMouseDown = (): void => {},
  order = 0,
  source = 'app',
  layerSelect = false,
  showAddLayersTooltip = true,
  leftComponent = null,
  isLoading = false,
  error,
}: LayerManagerProps) => {
  const [layerManagerWidth, setLayerManagerWidth] =
    React.useState<LayerManagerWidth>(LayerManagerWidth.lg);

  const onChangeSize = ({ width }): void => {
    setLayerManagerWidth(widthToWidthClass(width));
  };
  return (
    <ToolContainerDraggable
      title={showTitle ? `Layer Manager ${mapId}` : 'Layer Manager'}
      startSize={{ width: 720, height: 300 }}
      minWidth={300}
      minHeight={126}
      startPosition={{ top: 96, left: 50 }}
      isOpen={isOpen}
      onChangeSize={onChangeSize}
      onClose={onClose}
      headerSize="medium"
      bounds={bounds}
      data-testid="layerManagerWindow"
      onMouseDown={onMouseDown}
      order={order}
      source={source}
      leftComponent={leftComponent}
    >
      <Box sx={styles.layerManagerContainer}>
        {isLoading && (
          <LinearProgress
            data-testid="loading-bar"
            color="secondary"
            sx={{ position: 'absolute', width: '100%' }}
          />
        )}
        <Box sx={{ padding: '0 6px' }}>
          {error && <AlertBanner title={error} shouldClose />}
          <Grid
            container
            data-testid="layerManagerRowContainer"
            sx={styles.layerRowContainer}
          >
            <DescriptionRow
              mapId={mapId}
              tooltip={showAddLayersTooltip ? 'Add layers' : ''}
              layerManagerWidth={layerManagerWidth}
              preloadedServices={preloadedMapServices}
              layerSelect={layerSelect}
            />
            <LayerContainerRow
              mapId={mapId}
              layerManagerWidth={layerManagerWidth}
            />
            <BaseLayerRow
              mapId={mapId}
              tooltip="Add base layers"
              layerManagerWidth={layerManagerWidth}
              preloadedAvailableBaseLayers={preloadedAvailableBaseLayers}
              preloadedServices={preloadedBaseServices}
            />
          </Grid>
        </Box>
      </Box>
    </ToolContainerDraggable>
  );
};

export default LayerManager;
