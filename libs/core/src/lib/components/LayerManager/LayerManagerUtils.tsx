/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

const iconButtonSize = 30;
const totalButtons = 5;

export const LayerManagerColumnsLarge = {
  column1: {
    width: iconButtonSize * 3,
    display: 'flex',
  },
  column2: {
    width: `calc((100% - ${iconButtonSize * totalButtons}px) * 0.25)`,
  },
  column3: {
    width: `calc((100% - ${iconButtonSize * totalButtons}px) * 0.25)`,
  },
  column4: {
    width: `calc((100% - ${iconButtonSize * totalButtons}px) * 0.10)`,
  },
  column5: {
    width: `calc((100% - ${iconButtonSize * totalButtons}px) * 0.40)`,
  },
  column6: {
    width: iconButtonSize * 2,
    display: 'inline-flex',
  },
  columns35: {
    width: `calc((100% - ${iconButtonSize * totalButtons}px) * 0.75)`,
  },
};

export const LayerManagerColumnsSmall = {
  column1: {
    width: iconButtonSize * 3,
  },
  column2: {
    width: `calc(100% - ${iconButtonSize * totalButtons}px)`,
  },
  column3: {
    display: 'none',
  },
  column4: {
    display: 'none',
  },
  column5: {
    display: 'none',
  },
  column6: {
    width: iconButtonSize * 2,
    display: 'inline-flex',
  },
  columns35: {
    display: 'none',
  },
};

export enum LayerManagerWidth {
  sm,
  lg,
}

export const widthToWidthClass = (w: number): LayerManagerWidth => {
  if (w <= 500) return LayerManagerWidth.sm;
  return LayerManagerWidth.lg;
};
