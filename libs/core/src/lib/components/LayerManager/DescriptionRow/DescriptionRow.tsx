/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Box, Grid } from '@mui/material';
import * as React from 'react';
import WMSLayerTreeConnect from '../../WMSLoader/WMSLayerTree/WMSLayerTreeConnect';
import { Service } from '../../WMSLoader/services';
import { LayerType } from '../../../store/mapStore/types';
import AddLayersButton from '../AddLayersButton/AddLayersButton';
import {
  LayerManagerColumnsLarge,
  LayerManagerColumnsSmall,
  LayerManagerWidth,
} from '../LayerManagerUtils';
import { preloadedDefaultMapServices } from '../../../utils/defaultConfigurations';
import LayerSelectButtonConnect from '../LayerSelect/LayerSelectButtonConnect';

interface DescriptionRowProps {
  mapId: string;
  preloadedServices?: Service[];
  layerType?: LayerType;
  tooltip?: string;
  layerManagerWidth: LayerManagerWidth;
  layerSelect?: boolean;
}

const styles = {
  row: {
    height: '32px',
  },
  text: {
    paddingLeft: '10px',
    fontSize: '12px',
    marginTop: '10px',
    fontFamily: 'Roboto',
    opacity: 0.67,
  },
};

const DescriptionRow: React.FC<DescriptionRowProps> = ({
  mapId,
  layerType = LayerType.mapLayer,
  preloadedServices = preloadedDefaultMapServices,
  tooltip = '',
  layerManagerWidth,
  layerSelect = false,
}: DescriptionRowProps) => {
  const columnSizes =
    layerManagerWidth === LayerManagerWidth.sm
      ? LayerManagerColumnsSmall
      : LayerManagerColumnsLarge;

  return (
    <Grid container item data-testid="descriptionRow" sx={styles.row}>
      <Grid item sx={columnSizes.column1}>
        <AddLayersButton
          preloadedServices={preloadedServices}
          tooltip={tooltip}
          onRenderTree={(service): React.ReactChild => (
            <WMSLayerTreeConnect
              mapId={mapId}
              service={service}
              layerType={layerType}
            />
          )}
          shouldFocus
        />
        {layerSelect && <LayerSelectButtonConnect mapId={mapId} />}
      </Grid>
      <Grid sx={columnSizes.column2}>
        <Box sx={styles.text}>Layer</Box>
      </Grid>
      <Grid sx={columnSizes.column3}>
        <Box sx={styles.text}>Style</Box>
      </Grid>
      <Grid sx={columnSizes.column4}>
        <Box sx={styles.text}>Opacity</Box>
      </Grid>
      <Grid sx={columnSizes.column5}>
        <Box sx={styles.text}>Dimensions</Box>
      </Grid>
      <Grid sx={columnSizes.column6} />
    </Grid>
  );
};

export default DescriptionRow;
