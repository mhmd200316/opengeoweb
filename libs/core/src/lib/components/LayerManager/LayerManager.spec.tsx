/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import LayerManager from './LayerManager';

import { DialogType } from '../../store/ui/types';
import { CoreThemeStoreProvider } from '../Providers/Providers';

describe('src/components/LayerManager', () => {
  const props = {
    mapId: 'mapId_1',
    isOpen: true,
    onClose: (): void => {},
    showTitle: false,
    onMouseDown: jest.fn(),
  };

  it('should render required components', async () => {
    const mockStore = configureStore();
    const mockState = {
      legend: {
        type: 'legend' as DialogType,
        activeMapId: 'map1',
        isOpen: false,
      },
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { queryByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(queryByTestId('layerManagerRowContainer')).toBeTruthy();
    expect(queryByTestId('layerContainerRow')).toBeTruthy();
    expect(queryByTestId('baseLayerRow')).toBeTruthy();
    expect(queryByTestId('descriptionRow')).toBeTruthy();
    expect(queryByTestId('mappresets-menubutton')).toBeFalsy();

    fireEvent.mouseDown(queryByTestId('layerManagerWindow'));
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should render component in leftComponent slot', async () => {
    const props = {
      mapId: 'mapId_1',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      leftComponent: <div>test component</div>,
    };

    const mockStore = configureStore();
    const mockState = {
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'map2',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { findByText } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(await findByText('test component')).toBeTruthy();
  });

  it('should show the loading indicator when loading', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      isLoading: true,
    };

    const mockStore = configureStore();
    const mockState = {
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'mapId_3',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { findByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(await findByTestId('loading-bar')).toBeTruthy();
  });

  it('should show the alert banner when error given', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      error: 'Test error message.',
    };

    const mockStore = configureStore();
    const mockState = {
      layerManager: {
        type: 'layerManager' as DialogType,
        activeMapId: 'mapId_3',
        isOpen: true,
      },
    };
    const store = mockStore({ ui: mockState });
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { findByText, findByTestId } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(await findByText(props.error)).toBeTruthy();
    expect(await findByTestId('alert-banner')).toBeTruthy();
  });
});
