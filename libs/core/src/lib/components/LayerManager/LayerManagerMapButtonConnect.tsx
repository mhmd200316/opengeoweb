/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Layers } from '@opengeoweb/theme';
import * as uiSelectors from '../../store/ui/selectors';
import { uiActions } from '../../store';
import { AppStore } from '../../types/types';
import { Source } from '../../store/ui/types';
import { MapControlButton } from '../MapControls';

interface LayerManagerMapButtonConnectProps {
  mapId: string;
  source?: Source;
}

const LayerManagerMapButtonConnect: React.FC<LayerManagerMapButtonConnectProps> =
  ({ mapId, source = 'app' }: LayerManagerMapButtonConnectProps) => {
    const dispatch = useDispatch();

    const currentActiveMapId = useSelector((store: AppStore) =>
      uiSelectors.getDialogMapId(store, 'layerManager'),
    );

    const isOpenInStore = useSelector((store: AppStore) =>
      uiSelectors.getisDialogOpen(store, 'layerManager'),
    );

    const openLayerManagerDialog = React.useCallback((): void => {
      dispatch(
        uiActions.setActiveMapIdForDialog({
          type: 'layerManager',
          mapId,
          setOpen: currentActiveMapId !== mapId ? true : !isOpenInStore,
          source,
        }),
      );
    }, [currentActiveMapId, dispatch, isOpenInStore, mapId, source]);

    const isOpen = currentActiveMapId === mapId && isOpenInStore;

    return (
      <MapControlButton
        title="Layer Manager"
        data-testid="layerManagerButton"
        onClick={openLayerManagerDialog}
        isActive={isOpen}
      >
        <Layers />
      </MapControlButton>
    );
  };

export default LayerManagerMapButtonConnect;
