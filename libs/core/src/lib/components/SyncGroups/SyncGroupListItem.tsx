/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Typography,
  Checkbox,
  ListItem,
  ListItemIcon,
  Theme,
} from '@mui/material';

import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';

interface SyncGroupListItemProps {
  viewStateData;
  groupId;
  selected;
  sourcesById;
  handleToggle: (number) => void;
}

export const SyncGroupListItem: React.FC<SyncGroupListItemProps> = ({
  viewStateData,
  groupId,
  selected,
  sourcesById,
  handleToggle,
}: SyncGroupListItemProps) => {
  const classes = useStyles();
  return (
    <div className={classes.listItem}>
      {sourcesById.map((source) => {
        const isChecked = selected.includes(source.id);
        // Check if checkbox is checked in another group
        const isNotCheckedElseWhere = viewStateData.groups.every((group) => {
          return !(group.id !== groupId && group.selected.includes(source.id));
        });
        return (
          <ListItem
            key={source.id}
            dense
            button
            onClick={(): void => handleToggle(source.id)}
            className={classes.listItem}
          >
            <ListItemIcon>
              <Checkbox
                edge="start"
                checked={isChecked}
                tabIndex={-1}
                disableRipple
                inputProps={{ 'aria-labelledby': source.name }}
                className={
                  isNotCheckedElseWhere
                    ? classes.checkboxIsActive
                    : classes.isDisabled
                }
                color="default"
              />
            </ListItemIcon>
            <Typography
              className={
                isNotCheckedElseWhere ? classes.isActive : classes.isDisabled
              }
            >
              {source.name}
            </Typography>
          </ListItem>
        );
      })}
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listItem: {
      width: '80%',
      borderRadius: '5px',
    },
    isActive: {
      color: theme.palette.geowebColors.typographyAndIcons.icon,
    },
    checkboxIsActive: {
      color: theme.palette.geowebColors.typographyAndIcons.iconLinkActive,
    },
    isDisabled: {
      color: theme.palette.geowebColors.typographyAndIcons.iconLinkDisabled,
    },
  }),
);
