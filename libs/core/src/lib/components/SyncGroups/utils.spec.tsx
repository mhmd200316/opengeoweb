/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { LayerType } from '@opengeoweb/webmap';
import { generateUniqueLayerIds } from './utils';

const dummyLayer1 = {
  service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS&service=WMS&',
  name: '10M/ta',
  layerType: LayerType.mapLayer,
  id: 'dummyId1',
};

const dummyLayer2 = {
  service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS&service=WMS&',
  name: '10M/ta',
  layerType: LayerType.mapLayer,
};

const dummyLayer3 = {
  service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS&service=WMS&',
  name: '10M/ta',
  layerType: LayerType.mapLayer,
  id: 'dummyId',
};

const dummyLayer4 = {
  service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS&service=WMS&',
  name: '10M/ta',
  layerType: LayerType.mapLayer,
  id: 'dummyId',
};

const layerIdRegEx = /^layerid_[0-9]+$/;

describe('src/components/SyncGroups/utils', () => {
  describe('generateLayerIds', () => {
    it('should generate new layerIds and activeLayerId', () => {
      const result = generateUniqueLayerIds(
        [dummyLayer1, dummyLayer2],
        dummyLayer1.id,
      );
      expect(result).toEqual({
        layersNewIds: [
          { ...dummyLayer1, id: expect.stringMatching(layerIdRegEx) },
          { ...dummyLayer2, id: expect.stringMatching(layerIdRegEx) },
        ],
        activeLayerNewId: result.layersNewIds[0].id,
      });
    });
    it('should not set ids when none given', () => {
      expect(generateUniqueLayerIds([], undefined)).toEqual({
        layersNewIds: [],
        activeLayerNewId: undefined,
      });
      expect(
        generateUniqueLayerIds([dummyLayer1, dummyLayer2], undefined),
      ).toEqual({
        layersNewIds: [
          { ...dummyLayer1, id: expect.stringMatching(layerIdRegEx) },
          { ...dummyLayer2, id: expect.stringMatching(layerIdRegEx) },
        ],
        activeLayerNewId: undefined,
      });
    });
    it('should generate unique ids for layers with the same id', () => {
      const result = generateUniqueLayerIds(
        [dummyLayer3, dummyLayer4],
        undefined,
      );
      expect(result.layersNewIds[0].id).not.toEqual(result.layersNewIds[1].id);
    });
    it('should generate unique ids when same active layer is used in multiple presets', () => {
      const result1 = generateUniqueLayerIds(
        [dummyLayer1, dummyLayer2],
        dummyLayer1.id,
      );
      const result2 = generateUniqueLayerIds(
        [dummyLayer1, dummyLayer2],
        dummyLayer1.id,
      );
      expect(result1.layersNewIds[0].id).not.toEqual(
        result2.layersNewIds[0].id,
      );
      expect(result1.activeLayerNewId).not.toEqual(result2.activeLayerNewId);
    });
    it('should generate unique ids when same active layer id is used multiple times in a single preset and should set first layer active', () => {
      const result = generateUniqueLayerIds(
        [dummyLayer3, dummyLayer4], // these are different layers with the same id
        dummyLayer3.id,
      );
      expect(dummyLayer3.id).toEqual(dummyLayer4.id);
      expect(result.layersNewIds[0].id).not.toEqual(result.layersNewIds[1].id);
      expect(result.activeLayerNewId).toEqual(result.layersNewIds[0].id);
    });
    it('should generate unique ids when same layer is used multiple times in a single preset and set as active', () => {
      const result = generateUniqueLayerIds(
        [dummyLayer3, dummyLayer3], // same exact layer used twice and set active
        dummyLayer3.id,
      );
      expect(result.layersNewIds[0].id).not.toEqual(result.layersNewIds[1].id);
      expect(result.activeLayerNewId).toEqual(result.layersNewIds[0].id);
    });
  });
});
