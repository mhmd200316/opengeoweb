/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import { connect } from 'react-redux';
import { Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import produce from 'immer';

import { store } from '../../storybookUtils/store';
import { radarLayer } from '../../utils/publicLayers';
import { generateMapId, TimeSliderConnect } from '../../..';
import { DraggableThings, DraggableThingProps } from './DraggableThings';
import { ConfigurableMapConnect } from '../ConfigurableMap';
import { SyncGroupViewerConnect } from './SyncGroupViewerConnect';
import {
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETTIME,
} from '../../store/generic/synchronizationGroups/constants';
import { actions as synchronizationGroupActions } from '../../store/generic/synchronizationGroups/reducer';
import { uiActions } from '../../store';
import { CoreThemeStoreProvider } from '../Providers/Providers';

const useStyles = makeStyles(() => ({
  container: {
    display: 'grid',
    gridTemplateAreas: '"nav content content"\n"footer footer footer"',
    gridTemplateColumns: '350px 1fr',
    gridTemplateRows: '1fr 100px',
    gridGap: '1px',
    height: '100vh',
    background: 'black',
  },
  nav: {
    gridArea: 'nav',
    margin: '0 0 0 1px',
    background: 'white',
    overflowY: 'scroll',
  },
  content: {
    gridArea: 'content',
    margin: '0 1px 0px 0px',
    display: 'grid',
    gridTemplateAreas: '"mapA mapB mapC"',
    gridTemplateColumns: '1fr 1fr 1fr',
    gridTemplateRows: 'auto',
    gridGap: '1px',
    height: '100%',
  },
  footer: {
    margin: '0 1px 1px 1px',
    gridArea: 'footer',
    background: '#FEFEFF',
  },
  mapA: {
    gridArea: 'mapA',
    background: 'lightgray',
    height: '100%',
  },
  mapB: {
    gridArea: 'mapB',
    background: 'gray',
  },
  mapC: {
    gridArea: 'mapC',
    background: 'darkgray',
  },
}));

interface InitSyncGroupsProps {
  syncGroupAddGroup: typeof synchronizationGroupActions.syncGroupAddGroup;
  syncGroupAddTarget: typeof synchronizationGroupActions.syncGroupAddTarget;
}
const InitSyncGroups = connect(null, {
  syncGroupAddGroup: synchronizationGroupActions.syncGroupAddGroup,
  syncGroupAddTarget: synchronizationGroupActions.syncGroupAddTarget,
})(({ syncGroupAddGroup, syncGroupAddTarget }: InitSyncGroupsProps) => {
  React.useEffect(() => {
    syncGroupAddGroup({
      groupId: 'Time_A',
      title: 'Group 1 for time',
      type: SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_A',
      title: 'Group 2 for area',
      type: SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddGroup({
      groupId: 'Time_B',
      title: 'Group 3 for time',
      type: SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_B',
      title: 'Group 4 for area',
      type: SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_C' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_C' });
  }, [syncGroupAddGroup, syncGroupAddTarget]);
  return null;
});

let sliderCounterId = 0;

export const SyncMaps = (): React.ReactElement => {
  const [openDialogs, setOpenDialogs] = React.useState<DraggableThingProps[]>(
    [],
  );

  const addFloatingMap = (): void => {
    setOpenDialogs(
      produce(openDialogs, (draft) => {
        draft.push({
          id: generateMapId(),
          type: 'map',
          width: 300,
          height: 300,
        });
      }),
    );
  };
  const addFloatingTimeSlider = (): void => {
    setOpenDialogs(
      produce(openDialogs, (draft) => {
        draft.push({
          id: `slider${(sliderCounterId += 1)}`,
          type: 'slider',
          width: 300,
          height: 100,
        });
      }),
    );
  };
  const handleCloseDialog = (mapId): void => {
    setOpenDialogs(
      produce(openDialogs, (draft) => {
        draft.splice(
          draft.findIndex((a) => a.id === mapId),
          1,
        );
      }),
    );
  };

  const openSyncgroupsDialog = React.useCallback(() => {
    store.dispatch(
      uiActions.setToggleOpenDialog({ type: 'syncGroups', setOpen: true }),
    );
  }, []);

  const classes = useStyles();

  return (
    <CoreThemeStoreProvider store={store} theme={darkTheme}>
      <InitSyncGroups />
      <DraggableThings
        openDialogs={openDialogs}
        handleClose={handleCloseDialog}
      />
      <div className={classes.container}>
        <div className={classes.nav}>
          <Button
            variant="contained"
            color="primary"
            onClick={(): void => {
              addFloatingMap();
            }}
          >
            Add map
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={(): void => {
              addFloatingTimeSlider();
            }}
          >
            add slider
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={(): void => {
              openSyncgroupsDialog();
            }}
          >
            open Sync
          </Button>
          <SyncGroupViewerConnect />
        </div>
        <div className={classes.content}>
          <div className={classes.mapA}>
            <ConfigurableMapConnect
              id="map_A"
              layers={[radarLayer]}
              showTimeSlider={false}
              shouldShowZoomControls={false}
            />
          </div>
          <div className={classes.mapB}>
            <ConfigurableMapConnect
              id="map_B"
              layers={[radarLayer]}
              showTimeSlider={false}
              shouldShowZoomControls={false}
            />
          </div>
          <div className={classes.mapC}>
            <ConfigurableMapConnect
              id="map_C"
              layers={[radarLayer]}
              showTimeSlider={false}
              shouldShowZoomControls={false}
            />
          </div>
        </div>
        <div className={classes.footer}>
          <TimeSliderConnect sourceId="timeslider_1" mapId="map_A" />
        </div>
      </div>
    </CoreThemeStoreProvider>
  );
};
