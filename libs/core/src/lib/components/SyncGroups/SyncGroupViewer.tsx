/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { ToolContainerDraggable } from '@opengeoweb/shared';
import { SyncGroupList } from './SyncGroupList';
import { groupTypes } from './selector';
import { SyncGroupViewState } from './types';
import * as SyncgroupTypes from '../../store/generic/synchronizationGroups/types';

interface SyncGroupViewerProps {
  onClose: () => void;
  isOpen: boolean;
  syncGroupViewState: SyncGroupViewState;
  syncGroupAddTarget: (
    payload: SyncgroupTypes.SyncGroupAddTargetPayload,
  ) => void;
  syncGroupRemoveTarget: (
    payload: SyncgroupTypes.SyncGroupRemoveTargetPayload,
  ) => void;
  syncGroupAddGroup: (payload: SyncgroupTypes.SyncGroupAddGroupPayload) => void;
  syncGroupRemoveGroup: (
    payload: SyncgroupTypes.SyncGroupRemoveGroupPayload,
  ) => void;
  onMouseDown?: () => void;
  order?: number;
}

export const SyncGroupViewer: React.FC<SyncGroupViewerProps> = ({
  onClose,
  isOpen,
  syncGroupViewState,
  syncGroupAddTarget,
  syncGroupRemoveTarget,
  syncGroupAddGroup,
  syncGroupRemoveGroup,
  onMouseDown,
  order = 0,
}: SyncGroupViewerProps) => {
  /*
      _____ Handle Toggle _____
        - Handles checking, unchecking and toggling between in groups
        1. If checkbox is checked      -> uncheck it
        2. If checkbox is not checked  -> check if its checked elsewhere and uncheck it if found
  */
  const handleToggle = (
    groupCategory: string,
    groupId: string,
    sourceId: string,
  ): void => {
    const { selected } = syncGroupViewState[groupCategory].groups.find(
      (groupObject) => groupObject.id === groupId,
    );
    const isChecked = selected.includes(sourceId);
    if (isChecked) {
      syncGroupRemoveTarget({
        groupId: String(groupId),
        targetId: String(sourceId),
      });
    } else {
      const { groups } = syncGroupViewState[groupCategory];
      // Check all other groups and if checkmark is found -> uncheck it
      for (const group of groups) {
        if (group.id !== groupId && group.selected.includes(sourceId)) {
          syncGroupRemoveTarget({
            groupId: String(group.id),
            targetId: String(sourceId),
          });
        }
      }
      // Add new checkmark
      syncGroupAddTarget({
        groupId: String(groupId),
        targetId: String(sourceId),
      });
    }
  };

  /*
      _____ Add new group _____
        - Called when user clicks on + in the sidepane
        - Sends an action to add a new group in Redux
  */
  const addNewGroup = (groupCategory: string, groupId: string): void => {
    groupTypes.forEach(({ title, syncType, groupType }) => {
      if (groupCategory === groupType) {
        syncGroupAddGroup({
          groupId: String(groupId + Math.floor(Math.random() * 1000)),
          title,
          type: syncType,
        });
      }
    });
  };

  /*
      _____ Remove Group _____
        - Called when user clicks on the trashcan icon in sidepane
        - Send an action to remove the group in Redux
  */
  const removeGroup = (groupCategory: string, groupId: string): void => {
    groupTypes.forEach(({ groupType }) => {
      if (groupCategory === groupType) {
        syncGroupRemoveGroup({ groupId: String(groupId) });
      }
    });
  };

  return (
    <ToolContainerDraggable
      title="Sync"
      startSize={{ width: 349, height: 800 }}
      headerSize="medium"
      isOpen={isOpen}
      onClose={onClose}
      data-testid="syncGroupViewer"
      onMouseDown={onMouseDown}
      order={order}
    >
      <div>
        <SyncGroupList
          viewStateData={syncGroupViewState.zoompane}
          title="ZoomPane"
          handleToggle={(groupId, sourceId): void =>
            handleToggle('zoompane', groupId, sourceId)
          }
          addNewGroup={(groupId): void => addNewGroup('zoompane', groupId)}
          removeGroup={(groupId): void => removeGroup('zoompane', groupId)}
        />
        <SyncGroupList
          viewStateData={syncGroupViewState.timeslider}
          title="Timeslider"
          handleToggle={(groupId, sourceId): void =>
            handleToggle('timeslider', groupId, sourceId)
          }
          addNewGroup={(groupId): void => addNewGroup('timeslider', groupId)}
          removeGroup={(groupId): void => removeGroup('timeslider', groupId)}
        />
        <SyncGroupList
          viewStateData={syncGroupViewState.level}
          title="Level"
          handleToggle={(groupId, sourceId): void =>
            handleToggle('level', groupId, sourceId)
          }
          addNewGroup={(groupId): void => addNewGroup('level', groupId)}
          removeGroup={(groupId): void => removeGroup('level', groupId)}
        />
      </div>
    </ToolContainerDraggable>
  );
};
