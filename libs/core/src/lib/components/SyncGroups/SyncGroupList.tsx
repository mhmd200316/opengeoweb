/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Box,
  List,
  Typography,
  Tooltip,
  IconButton,
  Theme,
} from '@mui/material';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import { Add, Delete } from '@opengeoweb/theme';
import { SyncGroupListItem } from './SyncGroupListItem';
import { GroupsAndSources } from './types';

interface SyncGroupProps {
  viewStateData: GroupsAndSources;
  title: string;
  handleToggle: (arg1: string, arg2?: string) => void;
  addNewGroup: (number: string) => void;
  removeGroup: (number: string) => void;
}

export const SyncGroupList: React.FC<SyncGroupProps> = ({
  viewStateData,
  title,
  handleToggle,
  addNewGroup,
  removeGroup,
}: SyncGroupProps) => {
  const classes = useStyles();
  return (
    <List disablePadding={true}>
      {viewStateData.groups.map((groupObject, index) => {
        return (
          <Box
            key={groupObject.id}
            className={index === 0 ? null : classes.childListItem}
          >
            <div className={classes.syncGroupHeader}>
              <Typography>{title}</Typography>
              <Tooltip
                title={
                  index === 0
                    ? 'Add new group to list'
                    : 'Remove group from list'
                }
              >
                <IconButton
                  style={{ right: '10px' }}
                  data-testid="testAddButton"
                  onClick={
                    index === 0
                      ? (): void => addNewGroup(groupObject.id)
                      : (): void => removeGroup(groupObject.id)
                  }
                  size="large"
                >
                  {index === 0 ? <Add /> : <Delete />}
                </IconButton>
              </Tooltip>
            </div>
            <SyncGroupListItem
              viewStateData={viewStateData}
              groupId={groupObject.id}
              selected={groupObject.selected}
              sourcesById={viewStateData.sourcesById}
              handleToggle={(sourceId): void =>
                handleToggle(groupObject.id, sourceId)
              }
            />
          </Box>
        );
      })}
    </List>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    syncGroupHeader: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      color: theme.palette.geowebColors.typographyAndIcons.text,
      paddingLeft: '15px',
      paddingTop: '10px',
    },
    childListItem: {
      backgroundColor: theme.palette.geowebColors.syncGroups.drawerOpen.fill,
      boxShadow: 'inset 0px 7px 7px -7px, inset 0px -7px 7px -7px',
      color: '#000000',
    },
  }),
);
