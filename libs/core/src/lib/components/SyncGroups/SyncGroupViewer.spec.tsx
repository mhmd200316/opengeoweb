/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { ThemeWrapper } from '@opengeoweb/theme';
import { SyncGroupViewerConnect } from './SyncGroupViewerConnect';

const reduxState = {
  ui: {
    dialogs: {
      syncGroups: {
        isOpen: true,
        type: 'syncGroups',
      },
    },
  },
  syncronizationGroupStore: {
    viewState: {
      timeslider: {
        groups: [
          {
            id: 'SYNCGROUPS_TYPE_SETTIME_A',
            selected: ['radarView'],
          },
          {
            id: 'SYNCGROUPS_TYPE_SETTIME_A274',
            selected: [],
          },
        ],
        sourcesById: [
          {
            id: 'radarView',
            name: 'radarView',
          },
        ],
      },
      zoompane: {
        groups: [
          {
            id: 'SYNCGROUPS_TYPE_SETBBOX_A',
            selected: [],
          },
        ],
        sourcesById: [
          {
            id: 'radarView',
            name: 'radarView',
          },
        ],
      },
      level: {
        groups: [],
        sourcesById: [],
      },
    },
  },
};

describe('src/components/SyncGroups/SyncGroupViewer', () => {
  it('should render correct component', () => {
    const mockStore = configureStore();
    const store = mockStore(reduxState);
    const { queryByTestId } = render(
      <Provider store={store}>
        <ThemeWrapper>
          <SyncGroupViewerConnect />
        </ThemeWrapper>
      </Provider>,
    );
    expect(queryByTestId('syncGroupViewer')).toBeTruthy();
  });
});
