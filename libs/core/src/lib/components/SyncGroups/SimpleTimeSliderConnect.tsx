/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { SimpleTimeSlider } from './SimpleTimeSlider';
import { genericActions, genericSelectors } from '../../store';
import { AppStore } from '../../types/types';
import { SYNCGROUPS_TYPE_SETTIME } from '../../store/generic/synchronizationGroups/constants';
import { getSynchronizationGroupStore } from '../../store/generic/selectors';
import { handleMomentISOString } from '../../utils/dimensionUtils';

interface SimpleTimeSliderConnectComponentProps {
  id: string;
  timeValue?: string;
  setTime?: typeof genericActions.setTime;
  syncGroupAddSource?: typeof genericActions.syncGroupAddSource;
  syncGroupRemoveSource?: typeof genericActions.syncGroupRemoveSource;
}

const connectReduxSimpleTimeSlider = connect(
  (store: AppStore, props: SimpleTimeSliderConnectComponentProps) => ({
    timeValue: genericSelectors.getTime(
      getSynchronizationGroupStore(store),
      props.id,
    ),
  }),
  {
    setTime: genericActions.setTime,
    syncGroupAddSource: genericActions.syncGroupAddSource,
    syncGroupRemoveSource: genericActions.syncGroupRemoveSource,
  },
);

const SimpleTimeSliderConnectComponent: React.FC<SimpleTimeSliderConnectComponentProps> =
  ({
    id,
    timeValue,
    setTime,
    syncGroupAddSource,
    syncGroupRemoveSource,
  }: SimpleTimeSliderConnectComponentProps) => {
    React.useEffect(() => {
      syncGroupAddSource({
        id,
        type: [SYNCGROUPS_TYPE_SETTIME],
      });
      return (): void => {
        syncGroupRemoveSource({ id });
      };
    }, [id, syncGroupAddSource, syncGroupRemoveSource]);
    return (
      <SimpleTimeSlider
        setTime={(time): void => {
          setTime({
            sourceId: id,
            origin: 'SimpleTimeSliderConnect, 72',
            value: handleMomentISOString(time),
          });
        }}
        timeValue={timeValue}
        startValue={moment.utc().subtract(6, 'h')}
        endValue={moment.utc().add(-5, 'm')}
      />
    );
  };

export const SimpleTimeSliderConnect = connectReduxSimpleTimeSlider(
  SimpleTimeSliderConnectComponent,
);
