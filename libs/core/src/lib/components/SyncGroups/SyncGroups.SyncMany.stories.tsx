/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { connect } from 'react-redux';
import { Button } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import { lightTheme } from '@opengeoweb/theme';
import { store } from '../../storybookUtils/store';
import {
  msgAshEUMETSAT,
  msgCthEUMETSAT,
  msgFogEUMETSAT,
  msgNaturalenhncdEUMETSAT,
  msgNaturalEUMETSAT,
  obsAirPressureAtSeaLevel,
  obsAirTemperature,
  obsPrecipitationIntensityPWS,
  obsWind,
  radarLayer,
} from '../../utils/publicLayers';
import { SimpleTimeSliderConnect } from './SimpleTimeSliderConnect';
import { ConfigurableMapConnect } from '../ConfigurableMap';
import { SyncGroupViewerConnect } from './SyncGroupViewerConnect';
import {
  SYNCGROUPS_TYPE_SETBBOX,
  SYNCGROUPS_TYPE_SETTIME,
} from '../../store/generic/synchronizationGroups/constants';
import { actions as synchronizationGroupActions } from '../../store/generic/synchronizationGroups/reducer';
import { uiActions } from '../../store';
import { CoreThemeStoreProvider } from '../Providers/Providers';

const useStyles = makeStyles(() => ({
  root: {
    padding: 0,
    margin: 0,
  },
  container: {
    display: 'grid',
    gridTemplateAreas: '"header"\n" content"\n',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '40px 1fr',
    gridGap: '0px',
    height: '100vh',
    background: 'black',
  },
  header: {
    gridArea: 'header',
    margin: '0',
    padding: '2px',
    background: 'white',
  },
  content: {
    gridArea: 'content',
    margin: '0',
    padding: 0,
    display: 'grid',
    gridTemplateAreas: '"windowA windowB""windowC windowD"',
    gridTemplateColumns: '1fr 1fr',
    gridTemplateRows: '1fr 1fr',
    gridGap: '0px',
    height: '100%',
    overflow: 'hidden',
    zIndex: 3,
  },
  windowA: {
    gridArea: 'windowA',
    background: 'lightgray',
    height: '100%',
    display: 'grid',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr 40px',
    gridGap: '0px',
    zIndex: 1,
    margin: '0',
  },
  windowB: {
    gridArea: 'windowB',
    height: '100%',
    display: 'grid',
    gridTemplateAreas: '"mapA""timeSlider"',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr 40px',
    gridGap: '0px',
    overflow: 'hidden',
    margin: '0',
  },
  windowC: {
    gridArea: 'windowC',
    height: '100%',
    display: 'grid',
    gridTemplateAreas: '"mapA""timeSlider"',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr 40px',
    gridGap: '0px',
    overflow: 'hidden',
    margin: '0',
  },
  windowD: {
    gridArea: 'windowD',
    background: 'lightgray',
    height: '100%',
    display: 'grid',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr 40px',
    gridGap: '0px',
    zIndex: 1,
    margin: '0',
  },
  singleMap: {
    gridColumn: 1,
    gridRowStart: 1,
    gridRowEnd: 3,
    background: 'green',

    height: '100%',
    display: 'grid',
    gridTemplateAreas: '"map"',
    gridTemplateColumns: '1fr',
    gridTemplateRows: '1fr',
    gridGap: '0px',
    overflow: 'hidden',
    zIndex: 1,
  },
  fourMaps: {
    gridColumn: 1,
    gridRowStart: 1,
    gridRowEnd: 3,
    background: 'green',

    height: '100%',
    display: 'grid',
    gridTemplateAreas: '"mapA mapB ""mapC mapD"',
    gridTemplateColumns: '1fr 1fr',
    gridTemplateRows: '1fr 1fr',
    gridGap: '0px',
    overflow: 'hidden',
    zIndex: 1,
  },
  timeSlider: {
    zIndex: 2,
    gridColumn: 1,
    gridRowStart: 2,
    gridRowEnd: 3,
  },
  mapTitle: {
    position: 'absolute',
    padding: '5px',
    zIndex: 1000,
  },
}));

interface InitSyncGroupsProps {
  syncGroupAddGroup: typeof synchronizationGroupActions.syncGroupAddGroup;
  syncGroupAddTarget: typeof synchronizationGroupActions.syncGroupAddTarget;
}
const InitSyncGroups = connect(null, {
  syncGroupAddGroup: synchronizationGroupActions.syncGroupAddGroup,
  syncGroupAddTarget: synchronizationGroupActions.syncGroupAddTarget,
})(({ syncGroupAddGroup, syncGroupAddTarget }: InitSyncGroupsProps) => {
  React.useEffect(() => {
    syncGroupAddGroup({
      groupId: 'Time_A',
      title: 'Group 1 for time',
      type: SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_A',
      title: 'Group 2 for area',
      type: SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddGroup({
      groupId: 'Time_B',
      title: 'Group 3 for time',
      type: SYNCGROUPS_TYPE_SETTIME,
    });
    syncGroupAddGroup({
      groupId: 'Area_B',
      title: 'Group 4 for area',
      type: SYNCGROUPS_TYPE_SETBBOX,
    });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_C' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_D' });
    syncGroupAddTarget({ groupId: 'Area_A', targetId: 'map_E' });

    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_F' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_G' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_H' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_I' });
    syncGroupAddTarget({ groupId: 'Area_B', targetId: 'map_J' });

    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_F' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_G' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_H' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_I' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'map_J' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'timesliderC' });
    syncGroupAddTarget({ groupId: 'Time_B', targetId: 'timesliderD' });

    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'timesliderB' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_E' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'timesliderA' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_A' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_B' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_C' });
    syncGroupAddTarget({ groupId: 'Time_A', targetId: 'map_D' });
  }, [syncGroupAddGroup, syncGroupAddTarget]);
  return null;
});

export const SyncMany = (): React.ReactElement => {
  const classes = useStyles();

  const openSyncgroupsDialog = React.useCallback(() => {
    store.dispatch(
      uiActions.setToggleOpenDialog({ type: 'syncGroups', setOpen: true }),
    );
  }, []);

  const bboxEurope = {
    left: -918150.9603356163,
    bottom: 4839330.715139853,
    right: 2745893.275372317,
    top: 9376670.468166774,
  };

  return (
    <CoreThemeStoreProvider store={store} theme={lightTheme}>
      <InitSyncGroups />
      <SyncGroupViewerConnect />
      <div className={classes.container}>
        <div className={classes.header}>
          <Button
            variant="contained"
            color="primary"
            onClick={(): void => {
              openSyncgroupsDialog();
            }}
          >
            Open Sync
          </Button>
        </div>
        <div className={classes.content}>
          <div className={classes.windowA}>
            <div className={classes.fourMaps}>
              <ConfigurableMapConnect
                id="map_A"
                layers={[obsAirTemperature]}
                showTimeSlider={false}
                shouldShowZoomControls={false}
              />
              <ConfigurableMapConnect
                id="map_B"
                layers={[obsWind]}
                showTimeSlider={false}
                shouldShowZoomControls={false}
              />
              <ConfigurableMapConnect
                id="map_C"
                layers={[obsPrecipitationIntensityPWS]}
                showTimeSlider={false}
                shouldShowZoomControls={false}
              />
              <ConfigurableMapConnect
                id="map_D"
                layers={[obsAirPressureAtSeaLevel]}
                showTimeSlider={false}
                shouldShowZoomControls={false}
              />
            </div>
            <div className={classes.timeSlider}>
              <SimpleTimeSliderConnect id="timesliderA" />
            </div>
          </div>
          <div className={classes.windowB}>
            <div className={classes.singleMap}>
              <ConfigurableMapConnect
                id="map_E"
                bbox={bboxEurope}
                srs="EPSG:3857"
                layers={[radarLayer, obsAirTemperature]}
                showTimeSlider={false}
                shouldShowZoomControls={false}
              />
            </div>
            <div className={classes.timeSlider}>
              <SimpleTimeSliderConnect id="timesliderB" />
            </div>
          </div>
          <div className={classes.windowC}>
            <div className={classes.singleMap}>
              <ConfigurableMapConnect
                layers={[msgNaturalenhncdEUMETSAT]}
                id="map_F"
                showTimeSlider={false}
                shouldShowZoomControls={false}
              />
            </div>
            <div className={classes.timeSlider}>
              <SimpleTimeSliderConnect id="timesliderC" />
            </div>
          </div>
          <div className={classes.windowD}>
            <div className={classes.fourMaps}>
              <ConfigurableMapConnect
                id="map_G"
                bbox={bboxEurope}
                srs="EPSG:3857"
                layers={[msgAshEUMETSAT]}
                showTimeSlider={false}
                shouldShowZoomControls={false}
              />
              <ConfigurableMapConnect
                id="map_H"
                bbox={bboxEurope}
                srs="EPSG:3857"
                layers={[msgCthEUMETSAT]}
                showTimeSlider={false}
                shouldShowZoomControls={false}
              />
              <ConfigurableMapConnect
                id="map_I"
                bbox={bboxEurope}
                srs="EPSG:3857"
                layers={[msgFogEUMETSAT]}
                showTimeSlider={false}
                shouldShowZoomControls={false}
              />
              <ConfigurableMapConnect
                id="map_J"
                bbox={bboxEurope}
                srs="EPSG:3857"
                layers={[msgNaturalEUMETSAT]}
                showTimeSlider={false}
                shouldShowZoomControls={false}
              />
            </div>
            <div className={classes.timeSlider}>
              <SimpleTimeSliderConnect id="timesliderD" />
            </div>
          </div>
        </div>
      </div>
    </CoreThemeStoreProvider>
  );
};
