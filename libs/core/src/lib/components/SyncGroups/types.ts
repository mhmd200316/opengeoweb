/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

export interface Group {
  id: string;
  selected: number[];
}

export interface SourceById {
  id: string;
  name: string;
}

export interface GroupsAndSources {
  groups: Group[];
  sourcesById: SourceById[];
}

export interface SyncGroupViewState {
  timeslider: GroupsAndSources;
  zoompane: GroupsAndSources;
  level: GroupsAndSources;
}
