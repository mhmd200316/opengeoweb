/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Paper, Typography, IconButton } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import React from 'react';

import Draggable from 'react-draggable';
import { Close } from '@opengeoweb/theme';
import { radarLayer } from '../../utils/publicLayers';
import { SimpleTimeSliderConnect } from './SimpleTimeSliderConnect';
import { ConfigurableMapConnect } from '../ConfigurableMap';

const useStyles = makeStyles(() => ({
  draggableThing: {
    boxShadow: '4px 4px 15px 1px rgba(0,0,0,0.75)',
  },
  mapTitle: {
    position: 'absolute',
    padding: '5px',
    zIndex: 1000,
  },
  mapContainer: { background: 'red', zIndex: 10000, display: 'inline' },
}));

export interface DraggableThingProps {
  id: string;
  type: string;
  width: number;
  height: number;
}

interface DraggableThingsProps {
  openDialogs: DraggableThingProps[];
  handleClose: (id: string) => void;
}

interface DraggableDialogProps {
  dialogProps: DraggableThingProps;
  handleClose: () => void;
}

const DraggableDialog: React.FC<DraggableDialogProps> = ({
  dialogProps,
  handleClose,
}: DraggableDialogProps) => {
  const classes = useStyles();
  return (
    <Draggable
      bounds="body"
      handle=".handle"
      defaultPosition={{ x: 50, y: 50 }}
      position={null}
    >
      <Paper
        className={classes.draggableThing}
        style={{
          padding: '5px',
          background: 'white',
          width: `${dialogProps.width}px`,
          height: `${dialogProps.height + 30}px`,
          display: 'grid',
          gridTemplateAreas: '"header"\n"content"',
          gridTemplateColumns: '1fr',
          gridTemplateRows: '30px 1fr',
          gridGap: '0px',
          position: 'absolute',
          zIndex: 20000,
        }}
      >
        <span
          className="handle"
          style={{
            cursor: 'grab',
            gridArea: 'header',
          }}
        >
          <Typography>
            {dialogProps.id}
            <IconButton
              onClick={handleClose}
              size="small"
              style={{ float: 'right' }}
            >
              <Close />
            </IconButton>
          </Typography>
        </span>

        <div style={{ gridArea: 'content' }}>
          {dialogProps.type === 'map' && (
            <ConfigurableMapConnect
              layers={[radarLayer]}
              id={dialogProps.id}
              showTimeSlider={false}
            />
          )}
          {dialogProps.type === 'slider' && (
            <SimpleTimeSliderConnect id={dialogProps.id} />
          )}
        </div>
      </Paper>
    </Draggable>
  );
};

export const DraggableThings: React.FC<DraggableThingsProps> = ({
  openDialogs,
  handleClose,
}: DraggableThingsProps) => {
  return (
    <div>
      {openDialogs.map((dialog) => {
        return (
          <DraggableDialog
            key={dialog.id}
            dialogProps={dialog}
            handleClose={(): void => {
              handleClose(dialog.id);
            }}
          />
        );
      })}
    </div>
  );
};
