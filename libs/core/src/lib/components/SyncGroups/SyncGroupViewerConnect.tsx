/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { actions as SyncgroupActions } from '../../store/generic/synchronizationGroups';
import * as SyncgroupTypes from '../../store/generic/synchronizationGroups/types';
import * as uiSelectors from '../../store/ui/selectors';
import { uiActions } from '../../store';
import { SyncGroupViewer } from './SyncGroupViewer';
import { syncGroupGetViewState } from '../../store/generic/synchronizationGroups/selectors';

export const SyncGroupViewerConnect: React.FC = () => {
  const dispatch = useDispatch();

  // TODO
  // Improve the selector to prevent re-renders
  // For example dusting anination syncgroups should not re-render
  const viewState = useSelector(syncGroupGetViewState);

  const syncGroupAddGroup = React.useCallback(
    (payload: SyncgroupTypes.SyncGroupAddGroupPayload): void => {
      dispatch(SyncgroupActions.syncGroupAddGroup(payload));
    },
    [dispatch],
  );

  const syncGroupRemoveGroup = React.useCallback(
    (payload: SyncgroupTypes.SyncGroupRemoveGroupPayload): void => {
      dispatch(SyncgroupActions.syncGroupRemoveGroup(payload));
    },
    [dispatch],
  );

  const syncGroupAddTarget = React.useCallback(
    (payload: SyncgroupTypes.SyncGroupAddTargetPayload): void => {
      dispatch(SyncgroupActions.syncGroupAddTarget(payload));
    },
    [dispatch],
  );

  const syncGroupRemoveTarget = React.useCallback(
    (payload: SyncgroupTypes.SyncGroupRemoveTargetPayload): void => {
      dispatch(SyncgroupActions.syncGroupRemoveTarget(payload));
    },
    [dispatch],
  );

  const isOpenInStore = useSelector((store) =>
    uiSelectors.getisDialogOpen(store, 'syncGroups'),
  );

  const onClose = React.useCallback(
    () =>
      dispatch(
        uiActions.setToggleOpenDialog({ type: 'syncGroups', setOpen: false }),
      ),
    [dispatch],
  );

  const uiOrder = useSelector((store) =>
    uiSelectors.getDialogOrder(store, 'syncGroups'),
  );

  const uiIsOrderedOnTop = useSelector((store) =>
    uiSelectors.getDialogIsOrderedOnTop(store, 'syncGroups'),
  );

  const registerDialog = React.useCallback(
    () =>
      dispatch(
        uiActions.registerDialog({
          type: 'syncGroups',
          setOpen: false,
        }),
      ),
    [dispatch],
  );

  const unregisterDialog = React.useCallback(
    () => dispatch(uiActions.unregisterDialog({ type: 'syncGroups' })),
    [dispatch],
  );

  const onOrderDialog = React.useCallback(() => {
    if (!uiIsOrderedOnTop) {
      dispatch(uiActions.orderDialog({ type: 'syncGroups' }));
    }
  }, [dispatch, uiIsOrderedOnTop]);

  React.useEffect(() => {
    registerDialog();
    return (): void => {
      unregisterDialog();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <SyncGroupViewer
      onClose={onClose}
      isOpen={isOpenInStore}
      syncGroupViewState={viewState}
      syncGroupAddTarget={syncGroupAddTarget}
      syncGroupRemoveTarget={syncGroupRemoveTarget}
      syncGroupAddGroup={syncGroupAddGroup}
      syncGroupRemoveGroup={syncGroupRemoveGroup}
      order={uiOrder}
      onMouseDown={onOrderDialog}
    />
  );
};
