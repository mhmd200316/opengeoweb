/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { Layer } from '../../store/mapStore/types';
import { generateLayerId } from '../../store/mapStore/utils/helpers';

export const generateUniqueLayerIds = (
  layers: Layer[],
  activeLayerId: string,
): { layersNewIds: Layer[]; activeLayerNewId: string } => {
  // if there is an activeLayerId given, generate a unique id for it
  const activeLayerNewId = activeLayerId ? generateLayerId() : activeLayerId;

  // first match is set as activeLayer because there can only be one
  const activeLayerIndex = layers.findIndex(
    (layer) => layer.id === activeLayerId,
  );

  const layersNewIds = layers.map((layer, index) => {
    if (layer.id && index === activeLayerIndex) {
      return { ...layer, id: activeLayerNewId };
    }
    return { ...layer, id: generateLayerId() };
  });
  return { layersNewIds, activeLayerNewId };
};
