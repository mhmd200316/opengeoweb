/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

export const simplePolygonGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        stroke: '#8F8',
        'stroke-width': 4,
        'stroke-opacity': 1,
        fill: '#33ccFF',
        'fill-opacity': 0.5,
        country: 'Netherlands',
        details: [
          {
            event: 'Gale',
            sent: '2022-04-06T06:42:09+00:00',
            expires: '2022-04-06T14:00:00+00:00',
            area: '',
            web: '',
            sender: '',
            identifier: '1.578.0.220406064209738.1298',
          },
        ],
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [2.0167417739837967, 51.17995697758499],
            [4.257084968220221, 49.29134268590647],
            [8.635937575136854, 51.624654070318876],
            [8.228602448912055, 53.72352125601693],
            [6.191926817788037, 53.5423727705048],
            [2.0167417739837967, 51.17995697758499],
          ],
        ],
      },
    },
    {
      type: 'Feature',
      properties: {
        stroke: '#8F8',
        'stroke-width': 4,
        'stroke-opacity': 1,
        fill: '#33FFcc',
        'fill-opacity': 0.5,
        country: 'Finland',
        details: [
          {
            event: 'Gale',
            sent: '2022-04-06T06:42:09+00:00',
            expires: '2022-04-06T14:00:00+00:00',
            area: 'Helsinki',
            web: '',
            sender: 'noreply@fmi.fi',
            identifier: '2.49.0.1.578.0.220406064209738',
          },
        ],
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [26.253181784359604, 68.27806559514066],
            [20.855991361880957, 59.89443696044347],
            [29.308195231045634, 60.19950189496858],
            [33.78888161951847, 69.41694899886294],
            [28.493524978596028, 70.4304787447596],
            [26.253181784359604, 68.27806559514066],
          ],
        ],
      },
    },
    {
      type: 'Feature',
      properties: {
        stroke: '#8F8',
        'stroke-width': 4,
        'stroke-opacity': 1,
        fill: '#ffcc33',
        'fill-opacity': 0.5,
        country: 'Norway',
        warning: 'Wind',
        details: [
          {
            event: 'Gale',
            sent: '2022-04-06T06:42:09+00:00',
            expires: '2022-04-06T14:00:00+00:00',
            area: 'Andenes - Hekkingen',
            web: 'https://www.met.no/vaer-og-klima/Ekstremvaervarsler-og-andre-farevarsler',
            sender: 'noreply@met.no',
            identifier: '2.49.0.1.578.0.220406064209738.1298',
          },
        ],
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [5.377256565338432, 62.16231256814888],
            [5.580924128450831, 58.111828399316614],
            [10.061610516923672, 58.751545357585904],
            [20.75415758032477, 69.91236644763461],
            [15.967969847183323, 68.83644729778501],
            [5.377256565338432, 62.16231256814888],
          ],
        ],
      },
    },
  ],
};

export const simplePointsGeojson: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: { name: 'First point' },
      geometry: {
        type: 'Point',
        coordinates: [3.1640625, 53.12040528310657],
      },
    },
    {
      type: 'Feature',
      properties: { name: 'Second point' },
      geometry: {
        type: 'Point',
        coordinates: [-2.4609375, 48.22467264956519],
      },
    },
    {
      type: 'Feature',
      properties: { name: 'Third point' },
      geometry: {
        type: 'Point',
        coordinates: [5.9765625, 48.922499263758255],
      },
    },
    {
      type: 'Feature',
      properties: { name: 'Fourth point' },
      geometry: {
        type: 'Point',
        coordinates: [1.40625, 48.922499263758255],
      },
    },
  ],
};

export const simpleSmallLineStringGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        stroke: '#66F',
        'stroke-width': 5,
        'stroke-opacity': '1',
      },
      geometry: {
        type: 'LineString',
        coordinates: [
          [13.005656257245073, 53.618161003652624],
          [8.73272402958337, 48.28535054578368],
        ],
      },
    },
  ],
};

export const simpleFlightRoutePointsGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        fill: 'red',
        'fill-opacity': 0.5,
      },
      geometry: {
        type: 'Point',
        coordinates: [9.988228, 53.630389],
      },
    },
    {
      type: 'Feature',
      properties: {
        fill: 'red',
        'fill-opacity': 0.5,
      },
      geometry: {
        type: 'Point',
        coordinates: [9.683522, 52.460214],
      },
    },
    {
      type: 'Feature',
      properties: {
        fill: 'red',
        'fill-opacity': 0.5,
      },
      geometry: {
        type: 'Point',
        coordinates: [8.570456, 50.033306],
      },
    },
    {
      type: 'Feature',
      properties: {
        fill: 'red',
        'fill-opacity': 0.5,
      },
      geometry: {
        type: 'Point',
        coordinates: [9.221964, 48.689878],
      },
    },
    {
      type: 'Feature',
      properties: {
        fill: 'red',
        'fill-opacity': 0.5,
      },
      geometry: {
        type: 'Point',
        coordinates: [11.078008, 49.4987],
      },
    },
    {
      type: 'Feature',
      properties: {
        fill: 'red',
        'fill-opacity': 0.5,
      },
      geometry: {
        type: 'Point',
        coordinates: [11.786086, 48.353783],
      },
    },
    {
      type: 'Feature',
      properties: {
        fill: 'red',
        'fill-opacity': 0.5,
      },
      geometry: {
        type: 'Point',
        coordinates: [12.0818, 49.142],
      },
    },
    {
      type: 'Feature',
      properties: {
        fill: 'red',
        'fill-opacity': 0.5,
      },
      geometry: {
        type: 'Point',
        coordinates: [10.958106, 50.979811],
      },
    },
    {
      type: 'Feature',
      properties: {
        fill: 'red',
        'fill-opacity': 0.5,
      },
      geometry: {
        type: 'Point',
        coordinates: [13.500672, 52.362247],
      },
    },
  ],
};

export const simpleLineStringGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'LineString',
        coordinates: [
          [3.2958984375, 51.31001339554934],
          [6.30615234375, 52.696361078274485],
          [8.72314453125, 52.61639023304539],
        ],
      },
    },
    {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'LineString',
        coordinates: [
          [9.228515625, 48.922499263758255],
          [-4.21875, 47.338822694822],
          [-1.318359375, 54.521081495443596],
          [8.61328125, 55.3791104480105],
        ],
      },
    },
  ],
};

export const simpleBoxGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        _adaguctype: 'box',
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [3.6764699246101573, 60.0554711506609],
            [3.6764699246101573, 63.564699956881675],
            [15.801179052423734, 63.564699956881675],
            [15.801179052423734, 60.0554711506609],
            [3.6764699246101573, 60.0554711506609],
          ],
        ],
      },
    },
  ],
};

export const simpleFlightRouteLineStringGeoJSON: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'LineString',
        coordinates: [
          [9.988228, 53.630389],
          [9.683522, 52.460214],
          [8.570456, 50.033306],
          [9.221964, 48.689878],
          [11.078008, 49.4987],
          [11.786086, 48.353783],
          [12.0818, 49.142],
          [10.958106, 50.979811],
          [13.500672, 52.362247],
        ],
      },
    },
  ],
};

export const simpleMultiPolygon: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      properties: {
        stroke: '#8F8',
        'stroke-width': 4,
        'stroke-opacity': 1,
        fill: '#33ccFF',
        'fill-opacity': 0.5,
      },
      geometry: {
        type: 'MultiPolygon',
        coordinates: [
          [
            [
              [4.365729269907419, 55.315807175634454],
              [3.299427839634416, 55.605958027800156],
              [2.368817, 55.764314],
              [4.331914, 59.332644],
              [4.365729269907419, 55.315807175634454],
            ],
          ],
          [
            [
              [6.500001602574285, 54.735051191917506],
              [5.526314849017847, 58.0000007017522],
              [18.500002, 55.000002],
              [6.500001602574285, 54.735051191917506],
            ],
          ],
        ],
      },
    },
  ],
};
