/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import React, { useState } from 'react';
import {
  Button,
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  Grid,
  Box,
  TextField,
} from '@mui/material';

import { WithStyles } from '@mui/styles';
import makeStyles from '@mui/styles/makeStyles';
import withStyles from '@mui/styles/withStyles';

import { MapView, MapViewLayer } from '../../components/MapView';

import {
  simplePolygonGeoJSON,
  simplePointsGeojson,
  simpleBoxGeoJSON,
} from '../geojsonExamples';
import { lineString } from '../../components/ReactMapView/AdagucMapDraw';

import { initialBbox } from '../defaultStorySettings';
import mapDrawGeoJSONStyles from './MapDrawGeoJSONStyles';
import { baseLayer } from '../../utils/publicLayers';

export const useDrawPolyStoryStyles = makeStyles({
  MapDrawGeoJSONContainer: {
    display: 'grid',
    gridTemplateColumns: '70% 30%',
    gridTemplateRows: '150px 1fr',
    gridTemplateAreas: "'map controls'\n'map textarea'",
    width: '100%',
    height: '100vh',
  },
  MapDrawGeoJSONMapContainer: {
    gridArea: 'map',
    width: '100%',
    height: '100%',
  },
  MapDrawGeoJSONControlsContainer: {
    gridArea: 'controls',
    width: '100%',
    height: '100%',
  },
  MapDrawGeoJSONTextAreaContainer: {
    gridArea: 'textarea',
    width: '100%',
    height: '100%',
  },
  MapDrawGeoJSONTextArea: {
    padding: 0,
    border: 'none',
    width: '100%',
    height: '100%',
    fontSize: '10px',
  },
});

export const useGeoJSON = (): {
  editModes: { key: string; value: string }[];
  isInEditMode: boolean;
  geojson: GeoJSON.FeatureCollection;
  setGeojson: (geojson: GeoJSON.FeatureCollection) => void;
  geojsonText: string;
  setGeojsonText: (text: string) => void;
  drawMode: string;
  currentFeatureNrToEdit: number;
  setCurrentFeatureNrToEdit: (featureNumber: number) => void;
  changeDrawMode: (mode: string) => void;
  setEditMode: (on: boolean) => void;
} => {
  const editModes = [
    { key: 'POLYGON', value: 'Polygon' },
    { key: 'POINT', value: 'Point' },
    { key: 'BOX', value: 'Box' },
    { key: 'LINESTRING', value: 'LineString' },
  ];

  const [isInEditMode, setEditMode] = useState(false);
  const [geojson, setGeojson] = useState(lineString);
  const [geojsonText, setGeojsonText] = useState('');
  const [drawMode, setDrawMode] = useState('LINESTRING');
  const [currentFeatureNrToEdit, setCurrentFeatureNrToEdit] = useState(0);

  const changeDrawMode = (selectedKey: string): void => {
    setDrawMode(selectedKey);

    if (selectedKey === 'POLYGON') {
      setGeojson(simplePolygonGeoJSON);
      setGeojsonText(JSON.stringify(simplePolygonGeoJSON, null, 2));
    }
    if (selectedKey === 'BOX') {
      setGeojson(simplePolygonGeoJSON);
      setGeojsonText(JSON.stringify(simpleBoxGeoJSON, null, 2));
    }
    if (selectedKey === 'LINESTRING') {
      setGeojson(lineString);
      setGeojsonText(JSON.stringify(lineString, null, 2));
    }
    if (selectedKey === 'POINT' || selectedKey === 'MULTIPOINT') {
      setGeojson(simplePointsGeojson);
      setGeojsonText(JSON.stringify(simplePointsGeojson, null, 2));
    }
  };
  return {
    editModes,
    isInEditMode,
    geojson,
    setGeojson,
    geojsonText,
    setGeojsonText,
    drawMode,
    currentFeatureNrToEdit,
    setCurrentFeatureNrToEdit,
    changeDrawMode,
    setEditMode,
  };
};

const DrawPolyStory: React.FC<WithStyles<typeof mapDrawGeoJSONStyles>> = () => {
  const {
    editModes,
    isInEditMode,
    geojson,
    setGeojson,
    geojsonText,
    setGeojsonText,
    drawMode,
    currentFeatureNrToEdit,
    setCurrentFeatureNrToEdit,
    changeDrawMode,
    setEditMode,
  } = useGeoJSON();

  const [isValid, setValidity] = useState(true);

  const classes = useDrawPolyStoryStyles();
  const geojsonState = geojson;
  const numFeatures =
    geojsonState && geojsonState.features ? geojsonState.features.length : 0;
  const featureToEditList = [];
  for (let j = 0; j < numFeatures; j += 1) {
    featureToEditList.push({ key: `${j}`, value: j });
  }

  return (
    <div>
      <div className={classes.MapDrawGeoJSONContainer}>
        <div className={classes.MapDrawGeoJSONMapContainer}>
          <MapView
            mapId="mapDrawGeoJSONExample"
            srs={initialBbox.srs}
            bbox={initialBbox.bbox}
            onMount={(id, webMap): void => {
              webMap.mapPin.hideMapPin();
            }}
          >
            <MapViewLayer {...baseLayer} />
            <MapViewLayer
              id="geojson-layer"
              geojson={geojsonState}
              isInEditMode={isInEditMode}
              drawMode={drawMode}
              updateGeojson={(updatedGeojson): void => {
                setGeojson(updatedGeojson);
                setGeojsonText(JSON.stringify(geojsonState, null, 2));
              }}
              exitDrawModeCallback={(): void => {
                setEditMode(!isInEditMode);
              }}
              featureNrToEdit={currentFeatureNrToEdit}
            />
          </MapView>
        </div>
        <div className={classes.MapDrawGeoJSONControlsContainer}>
          <Box p={2}>
            <Grid spacing={2} container>
              <Grid item sm={6}>
                <FormControl variant="filled" style={{ minWidth: 120 }}>
                  <InputLabel id="demo-feature-type">Feature type</InputLabel>
                  <Select
                    labelId="demo-feature-type"
                    value={drawMode}
                    onChange={(
                      event: React.ChangeEvent<HTMLInputElement>,
                    ): void => {
                      changeDrawMode(event.target.value);
                    }}
                  >
                    {editModes.map((mode) => {
                      return (
                        <MenuItem key={mode.key} value={mode.key}>
                          {mode.value}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={6}>
                <FormControl style={{ minWidth: 120 }} variant="filled">
                  <InputLabel id="demo-feature-number">
                    Feature number
                  </InputLabel>
                  <Select
                    labelId="demo-feature-type"
                    value={`${currentFeatureNrToEdit}`}
                    onChange={(
                      event: React.ChangeEvent<HTMLInputElement>,
                    ): void => {
                      const featureNr = parseInt(event.target.value, 10);
                      setCurrentFeatureNrToEdit(featureNr);
                    }}
                  >
                    {featureToEditList.map((listItem) => {
                      return (
                        <MenuItem key={listItem.key} value={listItem.key}>
                          {listItem.value}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={6}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={(): void => {
                    setEditMode(!isInEditMode);
                  }}
                >
                  {isInEditMode ? 'Finish edit' : 'Start edit'}
                </Button>
              </Grid>
            </Grid>
          </Box>
        </div>
        <div className={classes.MapDrawGeoJSONTextAreaContainer}>
          <TextField
            className={classes.MapDrawGeoJSONTextArea}
            multiline
            error={!isValid}
            maxRows="18"
            label={!isValid ? 'GeoJSON not valid' : 'GeoJSON text'}
            variant="filled"
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
              setGeojsonText(event.target.value);
              try {
                setGeojson(JSON.parse(event.target.value));
                setValidity(true);
              } catch (e) {
                setGeojson(null);
                setValidity(false);
              }
            }}
            value={geojsonText || JSON.stringify(geojsonState, null, 2)}
          />
        </div>
      </div>
    </div>
  );
};

export default withStyles(mapDrawGeoJSONStyles)(DrawPolyStory);
