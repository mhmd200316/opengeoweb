/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Theme } from '@mui/material';

import { StyleRulesCallback } from '@mui/styles';

const mapDrawGeoJSONStyles: StyleRulesCallback<
  Theme,
  Record<string, unknown>
> = () => ({
  MapDrawGeoJSONContainer: {
    display: 'grid',
    gridTemplateColumns: '70% 30%',
    gridTemplateRows: '150px 1fr',
    gridTemplateAreas: "'map controls'\n'map textarea'",
    width: '100%',
    height: '100vh',
  },
  MapDrawGeoJSONMapContainer: {
    gridArea: 'map',
    width: '100%',
    height: '100%',
  },
  MapDrawGeoJSONControlsContainer: {
    gridArea: 'controls',
    width: '100%',
    height: '100%',
  },
  MapDrawGeoJSONTextAreaContainer: {
    gridArea: 'textarea',
    width: '100%',
    height: '100%',
  },
  MapDrawGeoJSONTextArea: {
    padding: 0,
    border: 'none',
    width: '100%',
    height: '100%',
    fontSize: '10px',
  },
});

export default mapDrawGeoJSONStyles;
