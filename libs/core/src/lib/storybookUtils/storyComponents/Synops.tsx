/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useEffect, useState } from 'react';
import moment from 'moment';

import { MapView, MapViewLayer } from '../../components/MapView';
import { overLayer } from '../../utils/publicLayers';
import { simpleSmallLineStringGeoJSON } from '../geojsonExamples';

interface SynopsState {
  featureInfo: { name: string; temp: string }[];
  gaforResult: GeoJSON.FeatureCollection;
}

const round = (
  date: moment.Moment,
  duration: moment.Duration,
  method: string,
): moment.Moment => {
  return moment(Math[method](+date / +duration) * +duration);
};

const Map: React.FC = () => {
  // dwd:MOSMIX_L_Punktterminprognosen can be used as well but will offer mulitple features per forecast point (timesteps), dwd:RBSN_T2m, Wetter_Beobachtungen
  // could be further restricted via &propertyName=NAME&sortBy=NAME+A
  const wfsUrl =
    'https://maps.dwd.de/geoserver/dwd/ows?request=GetFeature&service=WFS&version=1.0.0&typeName=dwd:RBSN_T2m&maxFeatures=999' +
    '&outputFormat=application/json';

  // for Beobachtungen get timestamp 0-1 hours in the past rounded to 10 minutes, for RBSN use past day and round to hour
  const querytime = round(
    moment().add(-1, 'd'),
    moment.duration(1, 'hours'),
    'ceil',
  ).toISOString();

  // M_DATE for RBSN-layers, OBSERVATION_TIME for Beobachtungen
  const cqlfilter = `${
    '&cql_filter=INTERSECTS(THE_GEOM, BUFFER(LINESTRING(13.00565 53.6181, 8.732724 48.28535), 0.2))' +
    ' AND M_DATE = '
  }${querytime}`;

  const [synopsState, setSynopsState] = useState<SynopsState>({
    featureInfo: [{ name: 'Stationname', temp: 'Temperature [°C]' }],
    gaforResult: {
      type: 'FeatureCollection',
      features: [],
    },
  });

  useEffect(() => {
    fetch(wfsUrl + cqlfilter, {
      method: 'GET',
    })
      .then((data) => data.json())
      .then((data: GeoJSON.FeatureCollection) => {
        const fetchedFeatureInfo = data.features.map((feature) => ({
          name: feature.properties.NAME,
          temp: feature.properties.TEMPERATURE,
        }));

        setSynopsState({
          gaforResult: data,
          featureInfo: fetchedFeatureInfo,
        });
      });
  }, [wfsUrl, cqlfilter]);

  return (
    <div>
      <div style={{ height: '100vh' }}>
        <MapView
          mapId="mapDrawSynopsExample"
          srs="EPSG:3857"
          bbox={{
            left: -881510.2930351471,
            bottom: 5913027.1888274085,
            right: 2405411.4864381333,
            top: 7298922.732565979,
          }}
        >
          <MapViewLayer {...overLayer} />
          <MapViewLayer
            id="simpleSmallLineStringGeoJSON"
            geojson={simpleSmallLineStringGeoJSON}
          />
          <MapViewLayer
            id="gaforResultGeoJSON"
            geojson={synopsState.gaforResult}
          />
        </MapView>
      </div>
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 10000,
          backgroundColor: '#CCCCCCC0',
          padding: '20px',
          overflow: 'auto',
          width: '33%',
          fontSize: '11px',
        }}
      >
        <div>
          <h5>Query features along buffered line using WFS and CQL</h5>
        </div>
        <div>
          WFS JSON URL: <pre>{wfsUrl}</pre>
        </div>
        <div>
          CQL expression: <pre>{cqlfilter}</pre>
        </div>
        <div>
          <pre>{JSON.stringify(synopsState.featureInfo, null, 2)}</pre>
        </div>
      </div>
    </div>
  );
};

export default Map;
