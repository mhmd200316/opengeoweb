/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useEffect, useState, useCallback } from 'react';
import { MapView, MapViewLayer } from '../../components/MapView';

import {
  getWMLayerById,
  generateMapId,
} from '../../store/mapStore/utils/helpers';

import { baseLayer, radarLayer, overLayer } from '../../utils/publicLayers';

const FeatureInfo: React.FC = () => {
  const initialFeatureInfoUrl = 'Click on the map to trigger a getfeatureinfo.';
  const [featureInfoUrl, setFeatureInfoUrl] = useState(initialFeatureInfoUrl);
  const [featureInfoResult, setFeatureInfoResult] = useState('');

  /**
   * This function is triggered when the map is clicked
   * @param {*} webMap The WebMap instance
   * @param {*} mouse The mouse object from the webMap, contains the following props:
   * {
   *  map: <the same webmap instance>,
   *  x: <X pixel coordinate on the map>,
   *  y, <Y pixel coodinate on the map>
   * }
   */

  const mapMouseClicked = useCallback(
    (webMap, mouse) => {
      /* Compose the getfeatureinfo URL for a layer based on the map's pixel coordinates, use json as format */
      setFeatureInfoUrl(
        webMap.getWMSGetFeatureInfoRequestURL(
          getWMLayerById(radarLayer.id),
          mouse.x,
          mouse.y,
          'application/json',
        ),
      );
    },
    [setFeatureInfoUrl],
  );

  useEffect(() => {
    /* Start fetching data from the obtained getfeatureinfo url */

    if (featureInfoUrl === initialFeatureInfoUrl) {
      // Do not try fetching any data with the initial URL.
      return;
    }

    fetch(featureInfoUrl, {
      method: 'GET',
      mode: 'cors',
    })
      .then((data) => {
        return data.json();
      })
      .then((data) => {
        setFeatureInfoResult(data);
      });
  }, [featureInfoUrl]);

  return (
    <div>
      <div style={{ height: '100vh' }}>
        <MapView
          mapId={generateMapId()}
          displayMapPin
          onMount={(id, webMap): void => {
            /* Add a listener which is triggered when you click on the map */
            webMap.addListener(
              'mouseclicked',
              (mouse) => {
                mapMouseClicked(webMap, mouse);
              },
              true,
            );
          }}
        >
          <MapViewLayer {...baseLayer} />
          <MapViewLayer
            {...radarLayer}
            onLayerReady={(layer): void => {
              layer.zoomToLayer();
            }}
          />
          <MapViewLayer {...overLayer} />
        </MapView>
      </div>
      <div
        style={{
          position: 'absolute',
          left: '50px',
          top: '10px',
          zIndex: 10000,
          backgroundColor: '#CCCCCCC0',
          padding: '20px',
          overflow: 'auto',
          width: '80%',
          fontSize: '11px',
        }}
      >
        <div>
          URL:
          <pre>{featureInfoUrl}</pre>
        </div>
        <div>
          GetFeatureInfo result:
          <pre>
            {featureInfoResult === ''
              ? ''
              : JSON.stringify(featureInfoResult, null, 2)}
          </pre>
        </div>
      </div>
    </div>
  );
};

export default FeatureInfo;
