/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render } from '@testing-library/react';

import { ThemeWrapper } from '../Theme';
import * as Icons from './Icons';

describe('Icons', () => {
  it('should all render icons successfully', () => {
    Object.values(Icons).forEach((Icon) => {
      if (typeof Icon === 'boolean') return;
      const { baseElement } = render(
        <ThemeWrapper>
          <Icon />
        </ThemeWrapper>,
      );
      expect(baseElement).toBeTruthy();
    });
  });
});
