/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { Button, Theme, useTheme } from '@mui/material';
import { ThemeWrapper, useThemeContext } from './ThemeContext';
import { lightTheme } from './lightTheme';
import { darkTheme } from './darkTheme';

interface TestComponentProps {
  onChangeTheme: (theme: Theme) => void;
}

const TestComponent: React.FC<TestComponentProps> = ({
  onChangeTheme,
}: TestComponentProps) => {
  const theme = useTheme();
  const { isDark, toggleTheme } = useThemeContext();

  React.useEffect(() => {
    onChangeTheme(theme);
  }, [theme, onChangeTheme]);
  return (
    <div className="test-component">
      <Button data-testid="test-btn" color="secondary" onClick={toggleTheme}>
        {isDark ? 'toggle to light' : 'toggle to dark'}
      </Button>
    </div>
  );
};

describe('Theme/ThemeContext', () => {
  it('wrap a component with a provider with default theme', () => {
    const props = {
      onChangeTheme: jest.fn(),
    };
    const { container } = render(
      <ThemeWrapper>
        <TestComponent {...props} />
      </ThemeWrapper>,
    );

    expect(container.querySelector('.test-component')).toBeTruthy();
    expect(props.onChangeTheme).toHaveBeenCalledWith(lightTheme);
  });
  it('wrap a component with a provider with given theme 2', () => {
    const props = {
      onChangeTheme: jest.fn(),
    };
    const { getByRole } = render(
      <ThemeWrapper>
        <TestComponent {...props} />
      </ThemeWrapper>,
    );
    expect(props.onChangeTheme).toHaveBeenCalledWith(lightTheme);

    const themeButton = getByRole('button');
    expect(themeButton).toBeTruthy();
    expect(themeButton.textContent).toEqual('toggle to dark');

    // change theme
    fireEvent.click(themeButton);
    expect(props.onChangeTheme).toHaveBeenCalledWith(darkTheme);
    expect(themeButton.textContent).toEqual('toggle to light');
  });
});
