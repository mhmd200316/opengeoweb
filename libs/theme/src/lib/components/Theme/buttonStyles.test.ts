/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { ComponentsVariants } from '@mui/material';
import { buttonStyle, variantsToClassName } from './buttonStyles';

describe('Theme/buttonStyles', () => {
  it('should convert variant styling to classNames with styling', () => {
    const style = {
      primary: {
        default: {
          fill: '#186DFF',
          color: '#FFFFFF',
          borderColor: 'transparent',
        },
        mouseOver: {
          fill: '#1047B0',
          color: '#FFFFFF',
          borderColor: '#71A6FF',
        },
        active: {
          fill: '#186DFF',
          color: '#FFFFFF',
          borderColor: 'transparent',
        },
        activeMouseOver: {},
        disabled: {
          fill: 'transparent',
          color: '#B0B0B0',
          borderColor: 'transparent',
        },
      },
    };
    const variants: ComponentsVariants['MuiButton'] = [
      {
        props: { variant: 'primary' },
        style: buttonStyle(style.primary),
      },
    ];

    expect(variantsToClassName(variants)).toEqual({
      '&.primary': buttonStyle(style.primary),
    });
  });
});
