/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { GeowebColorPalette } from './types';
import { createShadows, hex2rgba, parseColors } from './utils';

describe('components/Theme/utils', () => {
  describe('hex2rgba', () => {
    it('should create a rgba value from hex value', () => {
      expect(hex2rgba('#000000')).toEqual('rgba(0,0,0,1)');
      expect(hex2rgba('#000000', 0)).toEqual('rgba(0,0,0,0)');
      expect(hex2rgba('#FFFFFF')).toEqual('rgba(255,255,255,1)');
      expect(hex2rgba('#FFFFFF', 0)).toEqual('rgba(255,255,255,0)');
      expect(hex2rgba('#FFFFFF', 0.25)).toEqual('rgba(255,255,255,0.25)');
      expect(hex2rgba('#FF0000', 0.75)).toEqual('rgba(255,0,0,0.75)');
    });
  });
  describe('parseColors', () => {
    it('should add a rgba property if fill or color and opacity are given', () => {
      const playerNeedleTimeTop = {
        fill: '#000000',
        opacity: 1,
      };
      const playerNeedleTime = {
        color: '#ffffff',
        opacity: 1,
      };
      const playerNeedlePlayerTop = {
        fill: '#000000',
      };
      const testColors: GeowebColorPalette = {
        timeSlider: {
          playerNeedleTimeTop,
          playerNeedleTime,
          playerNeedlePlayerTop,
        },
      } as GeowebColorPalette;

      const result = parseColors(testColors);
      expect(result.timeSlider.playerNeedleTimeTop.rgba).toEqual(
        hex2rgba(playerNeedleTimeTop.fill, playerNeedleTimeTop.opacity),
      );
      expect(result.timeSlider.playerNeedleTime.rgba).toEqual(
        hex2rgba(playerNeedleTime.color, playerNeedleTime.opacity),
      );
      expect(result.timeSlider.playerNeedlePlayerTop.rgba).toBeUndefined();
    });
  });
  describe('createShadows', () => {
    it('should create an array with 25 elements with empty shadow styling', () => {
      const result = createShadows({});
      expect(result).toHaveLength(25);
      result.forEach((element) => {
        expect(element).toEqual('none');
      });
    });

    it('should overwrite empty shadows with given elevations', () => {
      const elevations = {
        name_1: 'box-shadow(0px, 0px, 0px)',
        name_2: 'box-shadow(1px, 1px, 1px)',
        name_3: 'box-shadow(2px, 2px, 2px)',
      };
      const result = createShadows(elevations);
      expect(result).toHaveLength(25);
      result.forEach((element, index) => {
        switch (index) {
          case 0:
            expect(element).toEqual('none');
            break;
          case 1:
            expect(element).toEqual(elevations.name_1);
            break;
          case 2:
            expect(element).toEqual(elevations.name_2);
            break;
          case 3:
            expect(element).toEqual(elevations.name_3);
            break;
          default:
            expect(element).toEqual('none');
            break;
        }
      });
    });
  });
});
