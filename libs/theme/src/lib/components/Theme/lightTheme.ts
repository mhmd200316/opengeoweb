/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { createTheme, createShadows, parseColors } from './utils';
import { Elevations, GeowebColorPalette } from './types';

// https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf84d920b99428d7f98555
export const colors: GeowebColorPalette = {
  background: {
    surface: '#FFFFFF',
    surfaceApp: '#F5F5F5',
    surfaceBrowser: '#CFCFCF',
  },
  brand: {
    brand: '#00547d',
  },
  buttons: {
    primary: {
      default: {
        fill: '#186DFF',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      mouseOver: {
        fill: '#1047B0',
        color: '#FFFFFF',
        borderColor: '#71A6FF',
      },
      active: {
        fill: '#186DFF',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      activeMouseOver: {},
      disabled: {
        fill: 'transparent',
        color: '#707070',
        borderColor: 'transparent',
      },
    },
    secondary: {
      default: {
        fill: '#0876b6',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      mouseOver: {
        fill: '#064c84',
        color: '#FFFFFF',
        borderColor: '#3DA6FF',
      },
      active: {
        fill: '#0876b6',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      activeMouseOver: {},
      disabled: {
        fill: 'transparent',
        color: '#707070',
        borderColor: 'transparent',
      },
    },
    tertiary: {
      default: {
        fill: 'transparent',
        color: '#575F7A',
        borderColor: '#575F7A',
      },
      mouseOver: {
        fill: '#E3E4E7',
        color: '#575F7A',
        borderColor: '#575F7A',
      },
      active: {
        fill: 'transparent',
        color: '#186DFF',
        borderColor: '#186DFF',
      },
      activeMouseOver: {
        fill: 'rgba(24, 109, 255, 0.1)',
        color: '#186DFF',
        borderColor: '#186DFF',
      },
      disabled: {
        fill: 'transparent',
        color: '#707070',
        borderColor: 'transparent',
      },
    },
    flat: {
      default: {
        fill: 'transparent',
        color: '#575F7A',
        borderColor: 'transparent',
      },
      mouseOver: {
        fill: '#E3E4E7',
        color: '#575F7A',
        borderColor: 'transparent',
      },
      active: {
        fill: 'transparent',
        color: '#186DFF',
        borderColor: 'transparent',
      },
      activeMouseOver: {
        fill: 'rgba(24, 109, 255, 0.1)',
        color: '#186DFF',
        borderColor: 'transparent',
      },
      disabled: {
        fill: 'transparent',
        color: '#707070',
        borderColor: 'transparent',
      },
    },
    tool: {
      default: {
        fill: '#ECEDEE',
        color: '#575F7A',
        borderColor: 'transparent',
      },
      mouseOver: {
        fill: '#E3E4E7',
        color: '#575F7A',
        borderColor: 'transparent',
      },
      active: {
        fill: '#186DFF',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      activeMouseOver: {
        fill: '#1047B0',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      disabled: {
        fill: 'transparent',
        color: '#707070',
        borderColor: 'transparent',
      },
    },
    boxed: {
      default: {
        fill: 'transparent',
        color: '#575F7A',
        borderColor: '#575f7a',
      },
      mouseOver: {
        fill: '#E3E4E7',
        color: '#575F7A',
        borderColor: '#575f7a',
      },
      active: {
        fill: '#186DFF',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      activeMouseOver: {
        fill: '#1047B0',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      disabled: {
        fill: 'transparent',
        color: '#707070',
        borderColor: 'transparent',
      },
    },
    focusDefault: {
      outline: 'solid 2px #419cfe',
    },
  },
  typographyAndIcons: {
    text: '#051039',
    icon: '#051039',
    buttonIcon: '#FFFFFF',
    iconLinkActive: '#186DFF',
    iconLinkDisabled: '#CCCCCC',
    textLinkActive: '#186DFF',
    buttonIconTertiaryFlat: '#575F7A',
    inactiveText: '#707070',
  },
  textInputField: {
    default: {
      fill: '#051039',
      opacity: 0.07,
    },
    active: {
      fill: '#051039',
      opacity: 0.02,
    },
    disabled: {
      fill: '#000000',
      opacity: 0,
    },
  },
  tab: {
    mouseOver: {
      fill: '#051039',
      opacity: 0.07,
    },
  },
  greys: {
    accessible: '#767676',
    accessibleLight: '#C5C5C5',
  },
  cards: {
    cardContainer: '#FFFFFF',
    cardContainerBorder: '#EEEEEE',
  },
  backdrops: {
    black: {
      fill: '#000000',
      opacity: 0.5,
    },
    white: {
      fill: '#FFFFFF',
      opacity: 0.24,
    },
  },
  screenManager: {
    activeWindow: '#FFFFFF',
    inactiveWindow: '#E8E5E5',
  },
  timeSlider: {
    // player
    playerNeedleTimeTop: {
      fill: '#E3004F',
      opacity: 100,
    },
    playerNeedleTime: {
      fill: '#FFD523',
      opacity: 0.75,
    },
    playerNeedlePlayerTop: {
      fill: '#051039',
      opacity: 100,
    },
    playerNeedlePlayer: {
      fill: '#67F0FF',
      opacity: 0.75,
    },
    playerRailObservation: {
      fill: '#0876B6',
      opacity: 100,
    },
    playerRailSelection: {
      fill: '#C6D111',
      opacity: 0.5,
    },
    playerRailForecastData: {
      fill: '#FD64FF',
      opacity: 0.6,
    },
    playerRailForecastNoData: {
      fill: '#051039',
      opacity: 0.67,
    },
    playerTimeMarkers: {
      fill: '#2C2C2C',
    },
    playerTimeText: {
      color: '#FFFFFF',
      fontFamily: 'Roboto',
      fontSize: 14,
    },
    // timeline
    timelineIndicator: {
      fill: '#767676',
    },
    timelineText: {
      fontSize: 14,
      color: '#051039',
      fontFamily: 'Roboto',
    },
    timelineTimeScale: {
      fill: '#979797',
    },
    timelineNightTime: {
      fill: '#070808',
      opacity: 0.1,
    },
    timelineTimelineSurface: {
      fill: '#FFFFFF',
    },
    timelineMonthChangeDash: {
      fill: '#767676',
      opacity: 0.5,
    },
    timelineSelectionBackground: {
      fill: '#11D1C6',
      opacity: 0.25,
    },
    // time scale
    timeScaleText: {
      fontSize: 12,
    },
    timeScalePointer: {
      fill: '#F8F9F9',
    },
    timeScaleHorizontalScale: {
      fill: '#070808',
      opacity: 0.2,
    },
    timeScaleTimeIndicators: {
      fill: '#767676',
    },
    timeScaleTimeIndicatorsActive: {
      fill: '#051039',
    },
    timeScaleShadowButtonScale: {
      filter:
        'drop-shadow(0px 0px 2px rgba(0, 0, 0, 0.28)) drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.24)) drop-shadow(0px 1px 3px rgba(0, 0, 0, 0.40))',
    },
  },
  customSlider: {
    rail: '#488BFD',
    railDisabled: '#CCCCCC',
    track: '#186DFF',
    trackDisabled: '#CCCCCC',
    thumb: '#F8F9F9',
    thumbDisabled: '#CCCCCC',
    mark: '#FFFFFF',
    markDisabled: '#FFFFFF',
  },
  customSwitch: {
    thumb: '#F8F9F9',
    thumbDisabled: '#F5F5F5',
    track: '#707070',
    trackActive: '#186DFF',
  },
  syncGroups: {
    drawerOpen: {
      fill: '#E8E5E5',
    },
  },
  layerManager: {
    tableRowDefaultText: {
      color: '#051039',
      opacity: 0.87,
      fontSize: 12,
    },
    tableRowDefaultCardContainer: {
      fill: '#FFFFFF',
      border: '1px solid #EEEEEE',
      borderColor: '#EEEEEE',
    },
    tableRowDisabledText: {
      color: '#767676',
      opacity: 0.87,
      fontSize: 12,
    },
    tableRowDisabledCardContainer: {
      fill: '#F5F5F5',
      border: '1px solid #EEEEEE',
      borderColor: '#EEEEEE',
    },
  },
  tooltips: {
    tooltipContainer: {
      fill: '#232323',
      opacity: 1,
    },
    tooltipText: {
      color: '#FFFFFF',
    },
  },
  captions: {
    captionStatus: { color: '#051039', opacity: 0.67 },
    captionInformation: { color: '#1100FF', opacity: 1 },
    captionError: { color: '#C00000', opacity: 1 },
    captionDisabled: { color: '#707070', opacity: 1 },
  },
  functional: {
    success: '#72BB23',
    successOutline: { fill: '#FCFFEB', border: ' 1px solid #72BB23' },
    error: '#C00000',
    errorOutline: { fill: '#F9E8EA', border: '1px solid #C00000' },
    warning: '#FFA800',
    warningOutline: { fill: '#FFF8EB', border: '1px solid #FFA800' },
    warningHighlight: { fill: '#FFA800', opacity: 0.2 },
    notification: '#0062E6',
    notificationOutline: { fill: '#DFF2FD', border: '1px solid #0062E6' },
  },
  snackbar: {
    background: '#051039',
    text: '#FFFFFF',
    action: '#98e1f1',
    actionHover: '#494949',
  },
};

// https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609bb6a5287bd1abe0de39a7
const elevations: Elevations = {
  elevation_01:
    '0 1px 3px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.12), 0 0 2px 0 rgba(0, 0, 0, 0.14)',
  elevation_02:
    '0 1px 5px 0 rgba(0, 0, 0, 0.2), 0 3px 4px 0 rgba(0, 0, 0, 0.12), 0 2px 4px 0 rgba(0, 0, 0, 0.14)',
  elevation_03:
    '0 1px 8px 0 rgba(0, 0, 0, 0.2), 0 3px 4px 0 rgba(0, 0, 0, 0.12), 0 3px 3px 0 rgba(0, 0, 0, 0.14)',
  elevation_04:
    '0 1px 10px 0 rgba(0, 0, 0, 0.2), 0 4px 5px 0 rgba(0, 0, 0, 0.12), 0 2px 4px 0 rgba(0, 0, 0, 0.14)',
  elevation_06:
    '0 3px 5px 0 rgba(0, 0, 0, 0.2), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 6px 10px 0 rgba(0, 0, 0, 0.14)',
  elevation_08:
    '0 4px 5px 0 rgba(0, 0, 0, 0.2), 0 3px 14px 3px rgba(0, 0, 0, 0.12), 0 8px 10px 1px rgba(0, 0, 0, 0.14)',
  elevation_09:
    '0 5px 6px 0 rgba(0, 0, 0, 0.2), 0 3px 16px 2px rgba(0, 0, 0, 0.12), 0 9px 12px 1px rgba(0, 0, 0, 0.14)',
  elevation_12:
    '0 7px 8px 0 rgba(0, 0, 0, 0.2), 0 5px 22px 4px rgba(0, 0, 0, 0.12), 0 12px 17px 2px rgba(0, 0, 0, 0.14)',
  elevation_16:
    '0 8px 10px 0 rgba(0, 0, 0, 0.2), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 16px 24px 2px rgba(0, 0, 0, 0.14)',
  elevation_24:
    '0 11px 15px 0 rgba(0, 0, 0, 0.2), 0 9px 46px 8px rgba(0, 0, 0, 0.12), 0 24px 38px 3px rgba(0, 0, 0, 0.14)',
};

export const lightTheme = createTheme(
  'light',
  parseColors(colors),
  createShadows(elevations),
);
