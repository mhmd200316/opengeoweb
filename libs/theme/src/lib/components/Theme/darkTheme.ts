/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { createTheme, createShadows, parseColors } from './utils';
import { Elevations, GeowebColorPalette } from './types';

// https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e5dc12278e35d433aa2d
export const colors: GeowebColorPalette = {
  background: {
    surface: '#303030',
    surfaceApp: '#2B2B2B',
    surfaceBrowser: '#181818',
  },
  brand: {
    brand: '#404040',
  },
  buttons: {
    primary: {
      default: {
        fill: '#186DFF',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      mouseOver: {
        fill: '#1047B0',
        color: '#FFFFFF',
        borderColor: '#71A6FF',
      },
      active: {
        fill: '#186DFF',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      activeMouseOver: {},
      disabled: {
        fill: 'transparent',
        color: '#B0B0B0',
        borderColor: 'transparent',
      },
    },
    secondary: {
      default: {
        fill: '#0876b6',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      mouseOver: {
        fill: '#064C84',
        color: '#FFFFFF',
        borderColor: '#3DA6FF',
      },
      active: {
        fill: '#0876B6',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      activeMouseOver: {},
      disabled: {
        fill: 'transparent',
        color: '#707070',
        borderColor: 'transparent',
      },
    },
    tertiary: {
      default: {
        fill: 'transparent',
        color: '#E5E5E5',
        borderColor: '#E5E5E5',
      },
      mouseOver: {
        fill: '#494949',
        color: '#E5E5E5',
        borderColor: '#E5E5E5',
      },
      active: {
        fill: 'transparent',
        color: '#488BFD',
        borderColor: '#48A8FA',
      },
      activeMouseOver: {
        fill: 'rgba(24, 109, 255, 0.1)',
        color: '#488BFD',
        borderColor: '#488BFD',
      },
      disabled: {
        fill: 'transparent',
        color: '#B0B0B0',
        borderColor: 'transparent',
      },
    },
    flat: {
      default: {
        fill: 'transparent',
        color: '#E5E5E5',
        borderColor: 'transparent',
      },
      mouseOver: {
        fill: '#494949',
        color: '#E5E5E5',
        borderColor: 'transparent',
      },
      active: {
        fill: 'transparent',
        color: '#488BFD',
        borderColor: 'transparent',
      },
      activeMouseOver: {
        fill: 'rgba(24, 109, 255, 0.1)',
        color: '#488BFD',
        borderColor: 'transparent',
      },
      disabled: {
        fill: 'transparent',
        color: '#B0B0B0',
        borderColor: 'transparent',
      },
    },
    tool: {
      default: {
        fill: '#393939',
        color: '#E5E5E5',
        borderColor: 'transparent',
      },
      mouseOver: {
        fill: '#494949',
        color: '#E5E5E5',
        borderColor: 'transparent',
      },
      active: {
        fill: '#186DFF',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      activeMouseOver: {
        fill: '#1047B0',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      disabled: {
        fill: 'transparent',
        color: '#B0B0B0',
        borderColor: 'transparent',
      },
    },
    boxed: {
      default: {
        fill: 'transparent',
        color: '#E5E5E5',
        borderColor: '#e5e5e5',
      },
      mouseOver: {
        fill: '#494949',
        color: '#E5E5E5',
        borderColor: '#e5e5e5',
      },
      active: {
        fill: '#186DFF',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      activeMouseOver: {
        fill: '#1047B0',
        color: '#FFFFFF',
        borderColor: 'transparent',
      },
      disabled: {
        fill: 'transparent',
        color: '#B0B0B0',
        borderColor: 'transparent',
      },
    },

    focusDefault: {
      outline: 'solid 2px #419cfe',
    },
  },
  typographyAndIcons: {
    text: '#FFFFFF',
    icon: '#FFFFFF',
    buttonIcon: '#FFFFFF',
    iconLinkActive: '#488BFD',
    iconLinkDisabled: '#5E5E5E',
    textLinkActive: '#488BFD',
    buttonIconTertiaryFlat: '#C5C5C5',
    inactiveText: '#B0B0B0',
  },
  textInputField: {
    default: {
      fill: '#000000',
      opacity: 0.15,
    },
    active: {
      fill: '#909090',
      opacity: 0.2,
    },
    disabled: {
      color: '#000000',
      opacity: 0,
    },
  },
  tab: {
    mouseOver: {
      fill: '#000000',
      opacity: 0.17,
    },
  },
  greys: {
    accessible: '#A2A2A2',
    accessibleLight: '#6F6F6F',
  },
  cards: {
    cardContainer: '#303030',
    cardContainerBorder: '#404040',
  },
  backdrops: {
    black: {
      fill: '#000000',
      opacity: 0.5,
    },
    white: {
      fill: '#FFFFFF',
      opacity: 0.24,
    },
  },
  screenManager: {
    activeWindow: '#242424',
    inactiveWindow: '#303030',
  },
  timeSlider: {
    // player
    playerNeedleTimeTop: {
      fill: '#E3004F',
      opacity: 100,
    },
    playerNeedleTime: {
      fill: '#FFD523',
      opacity: 0.75,
    },
    playerNeedlePlayerTop: {
      fill: '#051039',
      opacity: 100,
    },
    playerNeedlePlayer: {
      fill: '#67F0FF',
      opacity: 0.75,
    },
    playerRailObservation: {
      fill: '#0876B6',
      opacity: 100,
    },
    playerRailSelection: {
      fill: '#C6D111',
      opacity: 0.5,
    },
    playerRailForecastData: {
      fill: '#FD64FF',
      opacity: 0.6,
    },
    playerRailForecastNoData: {
      fill: '#051039',
      opacity: 0.67,
    },
    playerTimeMarkers: {
      fill: '#2C2C2C',
    },
    playerTimeText: {
      color: '#FFFFFF',
      fontFamily: 'Roboto',
      fontSize: 14,
    },
    // timeline
    timelineIndicator: {
      fill: '#A2A2A2',
    },
    timelineText: {
      fontSize: 14,
      color: '#FFFFFF',
      fontFamily: 'Roboto',
    },
    timelineTimeScale: {
      fill: '#979797',
    },
    timelineNightTime: {
      fill: '#070808',
      opacity: 0.3,
    },
    timelineTimelineSurface: {
      fill: '#303030',
    },
    timelineMonthChangeDash: {
      fill: '#A2A2A2',
      opacity: 0.5,
    },
    timelineSelectionBackground: {
      fill: '#11D1C6',
      opacity: 0.25,
    },
    // time scale
    timeScaleText: {
      fontSize: 12,
    },
    timeScalePointer: {
      fill: '#F8F9F9',
    },
    timeScaleHorizontalScale: {
      fill: '#000000',
      opacity: 0.8,
    },
    timeScaleTimeIndicators: {
      fill: '#A2A2A2',
    },
    timeScaleTimeIndicatorsActive: {
      fill: '#FFFFFF',
    },
    timeScaleShadowButtonScale: {
      filter:
        'drop-shadow(0px 0px 2px rgba(0, 0, 0, 0.28)) drop-shadow(0px 2px 2px rgba(0, 0, 0, 0.24)) drop-shadow(0px 1px 3px rgba(0, 0, 0, 0.40))',
    },
  },
  customSlider: {
    rail: '#488BFD',
    railDisabled: '#5E5E5E',
    track: '#186DFF',
    trackDisabled: '#5E5E5E',
    thumb: '#F8F9F9',
    thumbDisabled: '#5E5E5E',
    mark: '#FFFFFF',
    markDisabled: '#303030',
  },
  customSwitch: {
    thumb: '#F8F9F9',
    thumbDisabled: '#F5F5F5',
    track: '#BDBDBD',
    trackActive: '#488BFD',
  },
  syncGroups: {
    drawerOpen: {
      fill: '#232323',
    },
  },
  layerManager: {
    tableRowDefaultText: {
      color: '#FFFFFF',
      opacity: 0.87,
      fontSize: 12,
    },
    tableRowDefaultCardContainer: {
      fill: '#303030',
      border: '1px solid #404040',
      borderColor: '#404040',
    },
    tableRowDisabledText: {
      color: '#909090',
      opacity: 0.87,
      fontSize: 12,
    },
    tableRowDisabledCardContainer: {
      fill: '#2B2B2B',
      border: '1px solid #404040',
      borderColor: '#404040',
    },
  },
  tooltips: {
    tooltipContainer: {
      fill: '#232323',
      opacity: 1,
    },
    tooltipText: {
      color: '#FFFFFF',
    },
  },
  captions: {
    captionStatus: { color: '#FFFFFF', opacity: 0.7 },
    captionInformation: { color: '#9EC2FF', opacity: 1 },
    captionError: { color: '#FF8F8F', opacity: 1 },
    captionDisabled: { color: '#B0B0B0', opacity: 1 },
  },
  functional: {
    success: '#72BB23',
    successOutline: {
      fill: 'rgba(114, 187, 35, 0.25)',
      border: ' 1px solid #72BB23',
    },
    error: '#C00000',
    errorOutline: {
      fill: 'rgba(192, 0, 0, 0.25)',
      border: '1px solid #C00000',
    },
    warning: '#FFA800',
    warningOutline: {
      fill: 'rgba(255, 168, 0, 0.25)',
      border: '1px solid #FFA800',
    },
    warningHighlight: { fill: '#FFA800', opacity: 0.2 },
    notification: '#186DFF',
    notificationOutline: {
      fill: 'rgba(24, 109, 255, 0.25)',
      border: '1px solid #186DFF',
    },
  },
  snackbar: {
    background: '#051039',
    text: '#FFFFFF',
    action: '#98e1f1',
    actionHover: '#494949',
  },
};

// https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e694124ecf3886de76d4
const elevations: Elevations = {
  elevation_01:
    '0 1px 3px 0 rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.12), 0 0 2px 0 rgba(0, 0, 0, 0.14)',
  elevation_02:
    '0 1px 5px 0 rgba(0, 0, 0, 0.2), 0 3px 4px 0 rgba(0, 0, 0, 0.12), 0 2px 4px 0 rgba(0, 0, 0, 0.14)',
  elevation_03:
    '0 1px 8px 0 rgba(0, 0, 0, 0.2), 0 3px 4px 0 rgba(0, 0, 0, 0.12), 0 3px 3px 0 rgba(0, 0, 0, 0.14)',
  elevation_04:
    '0 1px 10px 0 rgba(0, 0, 0, 0.2), 0 4px 5px 0 rgba(0, 0, 0, 0.12), 0 2px 4px 0 rgba(0, 0, 0, 0.14)',
  elevation_06:
    '0 3px 5px 0 rgba(0, 0, 0, 0.2), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 6px 10px 0 rgba(0, 0, 0, 0.14)',
  elevation_08:
    '0 4px 5px 0 rgba(0, 0, 0, 0.2), 0 3px 14px 3px rgba(0, 0, 0, 0.12), 0 8px 10px 1px rgba(0, 0, 0, 0.14)',
  elevation_09:
    '0 5px 6px 0 rgba(0, 0, 0, 0.2), 0 3px 16px 2px rgba(0, 0, 0, 0.12), 0 9px 12px 1px rgba(0, 0, 0, 0.14)',
  elevation_12:
    '0 7px 8px 0 rgba(0, 0, 0, 0.2), 0 5px 22px 4px rgba(0, 0, 0, 0.12), 0 12px 17px 2px rgba(0, 0, 0, 0.14)',
  elevation_16:
    '0 8px 10px 0 rgba(0, 0, 0, 0.2), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 16px 24px 2px rgba(0, 0, 0, 0.14)',
  elevation_24:
    '0 11px 15px 0 rgba(0, 0, 0, 0.2), 0 9px 46px 8px rgba(0, 0, 0, 0.12), 0 24px 38px 3px rgba(0, 0, 0, 0.14)',
};

export const darkTheme = createTheme(
  'dark',
  parseColors(colors),
  createShadows(elevations),
);
