/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

// custom added properties
interface CSSProperties extends React.CSSProperties {
  rgba?: string;
}

export const gwButtonVariants = [
  'primary',
  'secondary',
  'tertiary',
  'flat',
  'tool',
  'boxed',
] as const;

export type ButtonVariant = typeof gwButtonVariants[number];

export interface ButtonStyle {
  default: CSSProperties;
  mouseOver: CSSProperties;
  active: CSSProperties;
  activeMouseOver: CSSProperties;
  disabled: CSSProperties;
}

export interface ButtonStyles {
  primary: ButtonStyle;
  secondary: ButtonStyle;
  tertiary: ButtonStyle;
  flat: ButtonStyle;
  tool: ButtonStyle;
  boxed: ButtonStyle;

  focusDefault: CSSProperties;
}

// see zeplin for naming convention https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609bb6a5287bd1abe0de39a7
export type GeowebColorPalette = {
  background: {
    surface: CSSProperties['color'];
    surfaceApp: CSSProperties['color'];
    surfaceBrowser: CSSProperties['color'];
  };
  brand: {
    brand: CSSProperties['color'];
  };
  buttons: ButtonStyles;
  typographyAndIcons: {
    text: CSSProperties['color'];
    icon: CSSProperties['color'];
    buttonIcon: CSSProperties['color'];
    iconLinkActive: CSSProperties['color'];
    iconLinkDisabled: CSSProperties['color'];
    textLinkActive: CSSProperties['color'];
    buttonIconTertiaryFlat: CSSProperties['color'];
    inactiveText: CSSProperties['color'];
  };
  textInputField: {
    default: CSSProperties;
    active: CSSProperties;
    disabled: CSSProperties;
  };
  tab: {
    mouseOver: CSSProperties;
  };
  greys: {
    accessible: CSSProperties['color'];
    accessibleLight: CSSProperties['color'];
  };
  cards: {
    cardContainer: CSSProperties['color'];
    cardContainerBorder?: CSSProperties['color'];
  };
  backdrops: {
    black: CSSProperties;
    white: CSSProperties;
  };
  screenManager: {
    activeWindow: CSSProperties['color'];
    inactiveWindow: CSSProperties['color'];
  };
  timeSlider: {
    // player
    playerNeedleTimeTop: CSSProperties;
    playerNeedleTime: CSSProperties;
    playerNeedlePlayerTop: CSSProperties;
    playerNeedlePlayer: CSSProperties;
    playerRailObservation: CSSProperties;
    playerRailSelection: CSSProperties;
    playerRailForecastData: CSSProperties;
    playerRailForecastNoData: CSSProperties;
    playerTimeMarkers: CSSProperties;
    playerTimeText: CSSProperties;
    // timeline
    timelineIndicator: CSSProperties;
    timelineText: CSSProperties;
    timelineTimeScale: CSSProperties;
    timelineNightTime: CSSProperties;
    timelineTimelineSurface: CSSProperties;
    timelineMonthChangeDash: CSSProperties;
    timelineSelectionBackground: CSSProperties;
    // time scale
    timeScaleText: CSSProperties;
    timeScalePointer: CSSProperties;
    timeScaleHorizontalScale: CSSProperties;
    timeScaleTimeIndicators: CSSProperties;
    timeScaleTimeIndicatorsActive: CSSProperties;
    timeScaleShadowButtonScale: CSSProperties;
  };
  customSlider: {
    rail: CSSProperties['color'];
    railDisabled: CSSProperties['color'];
    track: CSSProperties['color'];
    trackDisabled: CSSProperties['color'];
    thumb: CSSProperties['color'];
    thumbDisabled: CSSProperties['color'];
    mark: CSSProperties['color'];
    markDisabled: CSSProperties['color'];
  };
  customSwitch: {
    thumb: CSSProperties['color'];
    thumbDisabled: CSSProperties['color'];
    track: CSSProperties['color'];
    trackActive: CSSProperties['color'];
  };
  syncGroups: {
    drawerOpen: CSSProperties;
  };
  layerManager: {
    tableRowDefaultText: CSSProperties;
    tableRowDefaultCardContainer: CSSProperties;
    tableRowDisabledText: CSSProperties;
    tableRowDisabledCardContainer: CSSProperties;
  };
  tooltips: {
    tooltipContainer: CSSProperties;
    tooltipText: CSSProperties;
  };
  captions: {
    captionStatus: CSSProperties;
    captionInformation: CSSProperties;
    captionError: CSSProperties;
    captionDisabled: CSSProperties;
  };
  functional: {
    success: CSSProperties['color'];
    successOutline: CSSProperties;
    error: CSSProperties['color'];
    errorOutline: CSSProperties;
    warning: CSSProperties['color'];
    warningOutline: CSSProperties;
    warningHighlight: CSSProperties;
    notification: CSSProperties['color'];
    notificationOutline: CSSProperties;
  };
  snackbar: {
    background: CSSProperties['color'];
    text: CSSProperties['color'];
    action: CSSProperties['color'];
    actionHover: CSSProperties['color'];
  };
};

export type Elevations = {
  [id: string]: string;
};

declare module '@mui/material/styles' {
  interface Palette {
    geowebColors: GeowebColorPalette;
  }
  interface PaletteOptions {
    geowebColors: GeowebColorPalette;
  }
}

declare module '@mui/material/Button' {
  interface ButtonPropsVariantOverrides {
    primary: true;
    secondary: true;
    tertiary: true;
    flat: true;
    tool: true;
    boxed: true;
  }
}
