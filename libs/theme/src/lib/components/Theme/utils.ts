/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  Theme,
  PaletteMode,
  createTheme as createMuiTheme,
} from '@mui/material';
import { Shadows } from '@mui/material/styles/shadows';
import {
  buttonVariants,
  defaultButtonStyle,
  variantsToClassName,
} from './buttonStyles';
import { Elevations, GeowebColorPalette } from './types';

export const hex2rgba = (hex: string, alpha = 1): string => {
  const [r, g, b] = hex.match(/\w\w/g).map((x) => parseInt(x, 16));
  return `rgba(${r},${g},${b},${alpha})`;
};

export const parseColors = (colors: GeowebColorPalette): GeowebColorPalette =>
  Object.keys(colors).reduce((segmentList, key) => {
    const segment = colors[key];
    const parsedSegment = Object.keys(segment).reduce((list, name) => {
      const value = segment[name];
      const color = value.fill || value.color;
      // converts hex to rgba value if color and opacity property has been set
      if (value && color && (value.opacity || value.opacity === 0)) {
        return {
          ...list,
          [name]: {
            ...value,
            rgba: hex2rgba(color, value.opacity),
          },
        };
      }

      return {
        ...list,
        [name]: value,
      };
    }, {});
    return {
      ...segmentList,
      [key]: parsedSegment,
    };
  }, {} as GeowebColorPalette);

export const createShadows = (elevations: Elevations): Shadows => {
  const SHADOW_LIST_SIZE = 25;
  const elevationList = Object.keys(elevations).map(
    (elevationName) => elevations[elevationName],
  );
  return Array(SHADOW_LIST_SIZE)
    .fill('none')
    .map((value, index) => {
      if (index > 0) {
        const elevationIndex = index - 1;
        if (elevationList[elevationIndex]) {
          return elevationList[elevationIndex];
        }
      }
      return value;
    }) as Shadows;
};

const BORDER_RADIUS = 3;

export const breakpoints = {
  mobile: 0,
  tablet: 768,
  desktop: 1024,
  dekstopHD: 1440,
  dekstopHDWide: 1920,
};

export const createTheme = (
  paletteType: PaletteMode,
  geowebColors: GeowebColorPalette,
  shadows: Shadows,
): Theme =>
  createMuiTheme({
    breakpoints: {
      values: {
        xs: breakpoints.mobile,
        sm: breakpoints.tablet,
        md: breakpoints.desktop,
        lg: breakpoints.dekstopHD,
        xl: breakpoints.dekstopHDWide,
      },
    },
    palette: {
      mode: paletteType,
      secondary: {
        main: geowebColors.typographyAndIcons.iconLinkActive,
      },
      background: {
        paper: geowebColors.background.surfaceApp,
        default: geowebColors.background.surfaceBrowser,
      },
      text: {
        primary: geowebColors.typographyAndIcons.text,
      },
      // geoweb color palette
      geowebColors,
    },
    shape: { borderRadius: BORDER_RADIUS },
    typography: {
      fontFamily: ['Roboto', 'Helvetica', 'Arial', 'sans-serif'].join(','),
    },
    shadows,
    components: {
      MuiButton: {
        variants: buttonVariants(geowebColors.buttons),
      },
      MuiToggleButton: {
        styleOverrides: {
          root: variantsToClassName(buttonVariants(geowebColors.buttons)),
        },
      },
      MuiIconButton: {
        // TODO: https://gitlab.com/opengeoweb/opengeoweb/-/issues/1926 remove default styling and create CustomIconButton
        styleOverrides: {
          root: defaultButtonStyle(geowebColors.buttons.flat),
        },
      },
      MuiCssBaseline: {
        styleOverrides: `
          html {
            -webkit-font-smoothing: auto;
          }
        `,
      },
      MuiMenuItem: {
        styleOverrides: {
          root: {
            minHeight: '48px!important',
            fontSize: '0.875rem',
            letterSpacing: '0.25px',
            divider: {
              borderBottomColor: geowebColors.greys.accessibleLight,
            },
            li: {
              '&:hover': {
                backgroundColor: geowebColors.tab.mouseOver.rgba,
              },
            },
            '&.Mui-selected': {
              background: 'none',
              boxShadow: `inset 4px 0px 0px 0px ${geowebColors.typographyAndIcons.iconLinkActive}`,
            },
          },
        },
      },
      MuiRadio: {
        styleOverrides: {
          root: {
            color: geowebColors.typographyAndIcons.iconLinkActive,
            '&.Mui-disabled': {
              color: `${geowebColors.typographyAndIcons.iconLinkDisabled}!important`,
            },
            '&.Mui-checked': {
              color: geowebColors.typographyAndIcons.iconLinkActive,
            },
          },
        },
      },
      MuiCheckbox: {
        styleOverrides: {
          root: {
            color: geowebColors.typographyAndIcons.iconLinkActive,
            '&.Mui-disabled': {
              color: `${geowebColors.typographyAndIcons.iconLinkDisabled}!important`,
            },
            '&.Mui-checked': {
              color: geowebColors.typographyAndIcons.iconLinkActive,
            },
          },
        },
      },
      MuiPaper: {
        styleOverrides: {
          root: { backgroundImage: 'unset' },
          outlined: {
            borderColor: geowebColors.cards.cardContainerBorder,
          },
        },
      },
      MuiCard: {
        styleOverrides: {
          root: {
            backgroundColor: geowebColors.cards.cardContainer,
          },
        },
      },
      MuiSlider: {
        styleOverrides: {
          root: {
            color: geowebColors.typographyAndIcons.iconLinkActive,
          },
        },
      },
      MuiTable: {
        styleOverrides: {
          root: {
            borderSpacing: '0 0.25rem',
            borderCollapse: 'separate',
          },
        },
      },
      MuiTableRow: {
        styleOverrides: {
          head: {
            '&:hover': {
              boxShadow: shadows[0],
            },
          },
          root: {
            transition: 'box-shadow 100ms ease-out',
            boxShadow: shadows[0],
            borderRadius: BORDER_RADIUS,
            '&:hover': {
              boxShadow: shadows[1],
            },
            position: 'relative',
          },
        },
      },
      MuiTableCell: {
        styleOverrides: {
          head: {
            borderBottom: 'none',
            fontSize: '0.875rem',
            fontWeight: 400,
          },
          body: {
            padding: 12,
            fontWeight: 500,
            backgroundColor: geowebColors.background.surface,
            borderStyle: 'solid',
            borderColor: 'transparent',
            borderWidth: 1,
            borderTopColor: geowebColors.cards.cardContainerBorder,
            borderBottomColor: geowebColors.cards.cardContainerBorder,
            '&:first-child': {
              borderLeftColor: geowebColors.cards.cardContainerBorder,
              borderRadius: `${BORDER_RADIUS}px 0px 0px ${BORDER_RADIUS}px`,
            },
            '&:last-child': {
              borderRightColor: geowebColors.cards.cardContainerBorder,
              borderRadius: `0px ${BORDER_RADIUS}px ${BORDER_RADIUS}px 0px`,
            },
          },
        },
      },
      MuiTabs: {
        styleOverrides: {
          root: {
            position: 'relative',
            '&:after': {
              content: '""',
              position: 'absolute',
              bottom: 0,
              height: 1,
              width: '100%',
              zIndex: 0,
              backgroundColor: hex2rgba(geowebColors.greys.accessible, 0.2),
            },
          },
          indicator: {
            zIndex: 1,
            backgroundColor: geowebColors.typographyAndIcons.iconLinkActive,
          },
        },
      },
      MuiTab: {
        styleOverrides: {
          root: {
            textTransform: 'initial',
            '&.Mui-selected': {
              color: geowebColors.typographyAndIcons.iconLinkActive,
            },
            '&:hover': {
              opacity: 1,
              color: geowebColors.typographyAndIcons.iconLinkActive,
              backgroundColor: geowebColors.tab.mouseOver.rgba,
            },
          },
        },
      },
      MuiListItem: {
        styleOverrides: {
          root: {
            color: geowebColors.typographyAndIcons.text,
            fontSize: '14px',
            minHeight: '36px',
          },
          divider: {
            borderBottomColor: hex2rgba(geowebColors.greys.accessible, 0.2),
          },
        },
      },
      MuiListItemIcon: {
        styleOverrides: {
          root: {
            color: geowebColors.typographyAndIcons.icon,
            width: '24px',
            height: '24px',
          },
        },
      },
      MuiBackdrop: {
        styleOverrides: {
          root: {
            backgroundColor: geowebColors.backdrops.black.rgba,
          },
        },
      },
      MuiTooltip: {
        styleOverrides: {
          tooltip: {
            backgroundColor: geowebColors.tooltips.tooltipContainer.rgba,
            color: geowebColors.tooltips.tooltipText.color,
            padding: '8px 12px',
            fontSize: 16,
            fontWeight: 400,
            fontStretch: 'normal',
            fontStyle: 'normal',
            letterSpacing: 0.44,
            lineHeight: 1.56,
          },
        },
      },
      MuiLink: {
        styleOverrides: {
          root: {
            color: geowebColors.typographyAndIcons.textLinkActive,
            textDecoration: 'none',
            '&:hover': {
              textDecoration: 'underline',
              textDecorationColor:
                geowebColors.typographyAndIcons.textLinkActive,
            },
          },
        },
      },
      MuiAlert: {
        styleOverrides: {
          standardError: {
            border: geowebColors.functional.errorOutline.border,
            borderRadius: '0px',
            backgroundColor: geowebColors.functional.errorOutline.fill,
            '& .MuiAlert-icon': {
              color: geowebColors.functional.error,
              opacity: 1,
            },
          },
          standardWarning: {
            border: geowebColors.functional.warningOutline.border,
            borderRadius: '0px',
            backgroundColor: geowebColors.functional.warningOutline.fill,
            '& .MuiAlert-icon': {
              color: geowebColors.functional.warning,
              opacity: 1,
            },
          },
          standardInfo: {
            border: geowebColors.functional.notificationOutline.border,
            borderRadius: '0px',
            backgroundColor: geowebColors.functional.notificationOutline.fill,
            '& .MuiAlert-icon': {
              color: geowebColors.functional.notification,
              opacity: 1,
            },
          },
          standardSuccess: {
            border: geowebColors.functional.successOutline.border,
            borderRadius: '0px',
            backgroundColor: geowebColors.functional.successOutline.fill,
            '& .MuiAlert-icon': {
              color: geowebColors.functional.success,
              opacity: 1,
            },
          },
          action: { color: geowebColors.buttons.tertiary.default.color },
          message: {
            fontSize: '14px',
            lineHeight: 1.43,
            letterSpacing: '0.25px',
            color: geowebColors.typographyAndIcons.text,
          },
        },
      },
      MuiTextField: {
        defaultProps: {
          variant: 'filled',
        },
      },
      MuiFilledInput: {
        styleOverrides: {
          root: {
            backgroundColor: geowebColors.textInputField.default.rgba,
            '&.Mui-focused': {
              backgroundColor: geowebColors.textInputField.active.rgba,
              '&:after': {
                borderColor: 'transparent',
              },
            },
            '&.Mui-disabled': {
              backgroundColor: geowebColors.textInputField.disabled.rgba,
            },
            '&.Mui-error': {
              '&:after': {
                borderColor: geowebColors.captions.captionError.color,
              },
            },
          },
          underline: {
            '&:after': {
              borderWidth: '1px',
            },
            '&.Mui-disabled:before': {
              borderBottomStyle: 'solid',
            },
          },
        },
      },
      MuiFormLabel: {
        styleOverrides: {
          root: {
            color: geowebColors.captions.captionStatus.color,
            opacity: geowebColors.captions.captionStatus.opacity,
            '&.Mui-focused': {
              color: geowebColors.captions.captionInformation.color,
              opacity: geowebColors.captions.captionInformation.opacity,
            },
            '&.Mui-error': {
              color: geowebColors.captions.captionError.color,
              opacity: geowebColors.captions.captionError.opacity,
            },
            '&.Mui-disabled': {
              color: geowebColors.captions.captionDisabled.color,
              opacity: geowebColors.captions.captionDisabled.opacity,
            },
          },
        },
      },
      MuiFormHelperText: {
        styleOverrides: {
          root: {
            color: geowebColors.captions.captionStatus.color,
            opacity: geowebColors.captions.captionStatus.opacity,
            '&.Mui-error': {
              color: geowebColors.captions.captionError.color,
              opacity: geowebColors.captions.captionError.opacity,
            },
            '&.Mui-disabled': {
              color: geowebColors.captions.captionDisabled.color,
              opacity: geowebColors.captions.captionDisabled.opacity,
            },
          },
        },
      },
      MuiSwitch: {
        styleOverrides: {
          switchBase: {
            color: geowebColors.customSwitch.thumb,
            '&.Mui-checked': {
              color: geowebColors.customSwitch.thumb,
            },
            '&.Mui-disabled': {
              color: geowebColors.customSwitch.thumbDisabled,
            },
            '&.Mui-checked.Mui-disabled': {
              color: geowebColors.customSwitch.thumbDisabled,
            },
            '&.Mui-checked+.MuiSwitch-track': {
              backgroundColor: geowebColors.customSwitch.trackActive,
            },
            '&.Mui-disabled+.MuiSwitch-track': {
              backgroundColor: geowebColors.customSwitch.track,
            },
            '&.Mui-checked.Mui-disabled+.MuiSwitch-track': {
              backgroundColor: geowebColors.customSwitch.trackActive,
            },
          },
          track: {
            backgroundColor: geowebColors.customSwitch.track,
            opacity: '1 !important',
          },
        },
      },
      MuiSnackbarContent: {
        styleOverrides: {
          root: {
            backgroundColor: geowebColors.snackbar.background,
            color: geowebColors.snackbar.text,
            fontSize: '16px',
            letterSpacing: '0.44px',
            borderRadius: '2px',
          },
        },
      },
    },
  });
