/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { ComponentsVariants, CSSInterpolation, CSSObject } from '@mui/material';
import { ButtonStyles, ButtonStyle, gwButtonVariants } from './types';

export const defaultButtonStyle = (button: ButtonStyle): CSSObject => ({
  // default styling
  color: button.default.color,
  backgroundColor: button.default.fill,
  textTransform: 'capitalize',
  // hover styling
  '&:hover': {
    backgroundColor: button.mouseOver.fill,
    borderColor: button.mouseOver.borderColor,
  },
  // active styling
  '&.Mui-selected': {
    backgroundColor: button.active.fill,
    color: button.active.color,
    borderColor: button.active.borderColor,
    '&:hover': {
      backgroundColor: button.activeMouseOver.fill,
    },
  },
  // disabled styling
  '&.Mui-disabled': {
    backgroundColor: button.disabled.fill,
    color: button.disabled.color,
    borderColor: button.disabled.borderColor,
  },
});

export const buttonStyle = (button: ButtonStyle): CSSInterpolation => ({
  ...defaultButtonStyle(button),
  padding: '12px 8px',
  height: '40px',
  borderStyle: 'solid',
  borderWidth: '1px',
  borderColor: button.default.borderColor,
  borderRadius: '5px',
  // size styling
  '&[class*="sizeSmall"]': {
    fontSize: '12px',
    height: '32px',
    lineHeight: '12px',
  },
  // icon styling
  '& .MuiButton-startIcon': {
    margin: 0,
  },
  '& .MuiSvgIcon-root': {
    marginRight: '8px',
    fontSize: '24px!important',
  },
});

export const variantsToClassName = (
  buttonVariants: ComponentsVariants['MuiButton'],
): CSSInterpolation =>
  buttonVariants.reduce(
    (variants, { props: { variant }, style }) => ({
      ...variants,
      [`&.${variant}`]: style,
    }),
    {},
  );

export const buttonVariants = (
  buttons: ButtonStyles,
): ComponentsVariants['MuiButton'] =>
  gwButtonVariants.map((variant) => ({
    props: { variant },
    style: buttonStyle(buttons[variant]),
  }));
