/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  CssBaseline,
  Theme as MUITheme,
  ThemeProvider as MUIThemeProvider,
  StyledEngineProvider,
} from '@mui/material';
import { lightTheme } from './lightTheme';
import { darkTheme } from './darkTheme';
import { GWTheme } from './gwTheme';

interface ThemeContextProps {
  toggleTheme?: () => void;
  isDark?: boolean;
}

export const ThemeContext = React.createContext<Partial<ThemeContextProps>>({
  toggleTheme: null,
  isDark: null,
});

export interface ThemeProviderProps {
  children: React.ReactNode;
  theme?: MUITheme;
  disableCssBaseline?: boolean;
}

export const ThemeProvider: React.FC<ThemeProviderProps> = ({
  children,
  theme: defaultTheme = lightTheme,
  disableCssBaseline = false,
}: ThemeProviderProps) => {
  const [isDark, setDark] = React.useState(false);
  const currentTheme = React.useMemo(
    () => (isDark ? darkTheme : defaultTheme),
    [isDark, defaultTheme],
  );
  const toggleTheme = (): void => setDark(!isDark);

  return (
    <ThemeContext.Provider value={{ toggleTheme, isDark }}>
      <MUIThemeProvider theme={currentTheme}>
        {!disableCssBaseline && <CssBaseline />}
        {children}
      </MUIThemeProvider>
    </ThemeContext.Provider>
  );
};

export const useThemeContext = (): Partial<ThemeContextProps> =>
  React.useContext<ThemeContextProps>(ThemeContext);

// Wrapper for storybook stories. Will include later an additional wrapper after upgrade to MUI5
export const ThemeWrapper: React.FC<ThemeProviderProps> = ({
  children,
  theme = lightTheme,
  disableCssBaseline = false,
}: ThemeProviderProps) => (
  <StyledEngineProvider injectFirst>
    <ThemeProvider theme={theme} disableCssBaseline={disableCssBaseline}>
      {children}
    </ThemeProvider>
  </StyledEngineProvider>
);

// Wrapper for storybook stories with GWTheme. Will be deprecated
export const ThemeWrapperOldTheme: React.FC<ThemeProviderProps> = ({
  children,
  ...props
}: ThemeProviderProps) => (
  <ThemeWrapper theme={GWTheme} disableCssBaseline {...props}>
    {children}
  </ThemeWrapper>
);
