/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { createTheme } from '@mui/material';
import { parseColors } from './utils';
import { colors as lightThemeColors } from './lightTheme';
import { buttonVariants, variantsToClassName } from './buttonStyles';

// temporary copy of theme color values to make sure GWTheme is backwards compatible while working on new theme.
const geowebColors = {
  // copy of lightTheme values
  ...lightThemeColors,
};

export const GWTheme = createTheme({
  palette: {
    secondary: { main: '#0075a9' },
    action: { active: '#ffa800' },
    text: {
      disabled: '#051039',
    },
    geowebColors: parseColors(geowebColors),
  },
  shape: { borderRadius: 3 },
  typography: {
    h6: {
      fontSize: '16px',
    },
    subtitle1: {
      fontSize: '16px',
      fontWeight: 500,
      lineHeight: 1.5,
      letterSpacing: '0.15px',
      opacity: 0.87,
      color: '#051039',
    },
    subtitle2: {
      fontSize: '14px',
      fontWeight: 500,
      lineHeight: 1.71,
      letterSpacing: '0.1px',
      opacity: 0.87,
      color: '#051039',
    },
    body1: {
      fontSize: '16px',
      lineHeight: 1.56,
      letterSpacing: '0.44px',
      color: '#051039',
    },
    body2: {
      fontSize: '14px',
      lineHeight: 1.43,
      letterSpacing: '0.25px',
      color: '#051039',
    },
    overline: {
      textTransform: 'none',
      fontSize: '12px',
      lineHeight: 1.43,
      letterSpacing: '0.25px',
      color: '#051039',
      opacity: 0.7,
    },
    caption: {
      fontSize: '12px',
      lineHeight: 1.33,
      letterSpacing: '0.4px',
      color: '#051039',
      opacity: 0.7,
    },
    button: {
      fontWeight: 500,
      fontSize: '14px',
      color: '0075a9',
    },
  },
  components: {
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          borderRadius: '2px',
          fontSize: '16px',
          fontWeight: 'normal',
          padding: '10px',
          backgroundColor: '#232323',
        },
      },
    },
    MuiIconButton: {
      styleOverrides: {
        root: {
          color: '#0075a9',
        },
      },
    },
    MuiCheckbox: {
      styleOverrides: {
        root: {
          '&.Mui-disabled': {
            color: '#CCCCCC!important',
          },
          '&.Mui-checked': {
            color: '#0075a9',
          },
        },
      },
    },
    MuiSelect: {
      styleOverrides: {
        icon: {
          color: '#0075a9',
        },
        select: { '&:focus': { backgroundColor: 'rgba(0, 117, 169, 0.05)' } },
      },
    },
    MuiAlert: {
      styleOverrides: {
        standardError: {
          border: '1px solid #c00000',
          borderRadius: '0px',
          backgroundColor: '#fff0ea',
        },
        standardWarning: {
          border: '1px solid #ffa800',
          borderRadius: '0px',
          backgroundColor: '#fff8eb',
        },
        standardInfo: {
          border: '1px solid #0062e6',
          borderRadius: '0px',
          backgroundColor: '#eaf3ff',
        },
        standardSuccess: {
          border: '1px solid #72bb23',
          borderRadius: '0px',
          backgroundColor: '#fcffeb',
        },
        action: { color: '#0075a9' },
        message: {
          fontSize: '14px',
          lineHeight: 1.43,
          letterSpacing: '0.25px',
          color: '#051039',
        },
      },
    },
    MuiTabs: {
      styleOverrides: {
        root: {
          backgroundColor: '#ffffff',
          minHeight: '30px',
        },
        scrollButtons: {
          width: '20px',
        },
      },
    },
    MuiTab: {
      styleOverrides: {
        root: {
          minHeight: '30px',
          textTransform: 'none',
          minWidth: '76px', // 100 minus 24 padding (12 left and 12 right)
          '@media (min-width: 600px)': {
            minWidth: '76px',
          },
        },
        labelIcon: {
          minHeight: '30px',
        },
      },
    },
    MuiListItem: {
      styleOverrides: {
        root: {
          '&$selected': {
            backgroundColor: 'rgba(0, 117, 169, 0.15)',
            borderLeft: '4px solid #0075a9',
            '&:hover': {
              backgroundColor: 'rgba(0, 117, 169, 0.10)',
            },
          },
        },
        button: {
          '&:hover': {
            backgroundColor: 'rgba(0, 117, 169, 0.15)',
          },
        },
      },
    },
    MuiMenuItem: {
      styleOverrides: {
        root: {
          '&.Mui-selected': {
            backgroundColor: 'rgba(0, 117, 169, 0.15)',
            borderLeft: '4px solid #0075a9',
            '&:hover': {
              backgroundColor: 'rgba(0, 117, 169, 0.10)',
            },
          },
          '&:hover': {
            backgroundColor: 'rgba(0, 117, 169, 0.15)',
          },
        },
      },
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          color: 'rgba(5, 16, 57, 0.7)',
          '&.Mui-focused': {
            color: '#0075a9',
          },
          '&.Mui-error': {
            color: '#d32f2f!important',
          },
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          '&$focused $notchedOutline': {
            borderColor: '#0075a9',
          },
        },
      },
    },
    MuiFilledInput: {
      styleOverrides: {
        root: {
          backgroundColor: 'rgba(0, 117, 169, 0.05)',
          color: '#051039',
          '&.Mui-disabled': {
            backgroundColor: 'white',
          },
          '&.Mui-focused': {
            backgroundColor: 'rgba(0,0,0,0.09)',
          },
        },
        underline: {
          '&.Mui-disabled:before': {
            borderBottomStyle: 'none',
          },
        },
      },
    },
    MuiRadio: {
      styleOverrides: {
        root: {
          '&.Mui-disabled': {
            color: '#CCCCCC!important',
          },
          '&.Mui-checked': {
            color: '#0075a9',
          },
        },
      },
    },
    MuiCardContent: {
      styleOverrides: {
        root: {
          '&:last-child': {
            paddingBottom: 0,
          },
        },
      },
    },
    MuiDialog: {
      styleOverrides: {
        paperWidthLg: {
          maxWidth: '1280px',
        },
      },
    },
    MuiButton: {
      variants: buttonVariants(geowebColors.buttons),
    },
    MuiToggleButton: {
      styleOverrides: {
        root: variantsToClassName(buttonVariants(geowebColors.buttons)),
      },
    },
  },
});
