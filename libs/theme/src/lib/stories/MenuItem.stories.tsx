/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  ListItemIcon,
  Paper,
  MenuItem as MuiMenuItem,
  Typography,
} from '@mui/material';
import { Add } from '../components/Icons';
import { StoryWrapper } from './StoryWrapper';

export const MenuItemDemo: React.FC = () => (
  <Paper sx={{ width: '200px' }}>
    <MuiMenuItem divider>
      <Typography>Divider</Typography>
    </MuiMenuItem>
    <MuiMenuItem>
      <Typography>Text</Typography>
    </MuiMenuItem>
    <MuiMenuItem selected>
      <Typography>Selected</Typography>
    </MuiMenuItem>
    <MuiMenuItem>
      <ListItemIcon>
        <Add />
      </ListItemIcon>
      <Typography>Icon</Typography>
    </MuiMenuItem>
  </Paper>
);

export const MenuItem = (): React.ReactElement => (
  <StoryWrapper
    title="MenuItem"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61f2bfb9270cdcb4ca52f48f"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61f2bfc1b1c08bb05ac7bb99"
  >
    <MenuItemDemo />
  </StoryWrapper>
);
