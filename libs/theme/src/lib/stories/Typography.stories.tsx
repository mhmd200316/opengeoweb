/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  FormControlLabel,
  Grid,
  Switch,
  Typography as MuiTypography,
  Link,
} from '@mui/material';

import { ThemeWrapperOldTheme } from '../components/Theme/ThemeContext';
import { StoryWrapper } from './StoryWrapper';

// TODO: needs to be updated with styles from design
export const TypographyDemo: React.FC = () => {
  return (
    <Grid container>
      {/** Typography */}
      <Grid item xs={12}>
        <MuiTypography variant="h1">h1</MuiTypography>
        <MuiTypography variant="h2">h2</MuiTypography>
        <MuiTypography variant="h3">h3</MuiTypography>
        <MuiTypography variant="h4">h4</MuiTypography>
        <MuiTypography variant="h5">h5</MuiTypography>
        <MuiTypography variant="h6">h6</MuiTypography>
        <MuiTypography variant="subtitle1">subtitle1</MuiTypography>
        <MuiTypography variant="subtitle2">subtitle2</MuiTypography>
        <MuiTypography variant="body1">body1</MuiTypography>
        <MuiTypography variant="body2">body2</MuiTypography>
        <MuiTypography variant="overline">overline</MuiTypography>
        <br />
        <MuiTypography variant="caption">caption</MuiTypography>
        <br />
        <MuiTypography variant="button">button</MuiTypography>
        <br />
        <Link
          href="#"
          onClick={(event: React.MouseEvent): void => {
            event.preventDefault();
          }}
        >
          Link
        </Link>
      </Grid>
    </Grid>
  );
};

export const TypographyGWTheme: React.FC = () => {
  const [hasTheme, setHasTheme] = React.useState(true);

  return (
    <Grid container>
      <FormControlLabel
        control={
          <Switch
            checked={!hasTheme}
            onChange={(): void => setHasTheme(!hasTheme)}
          />
        }
        label={hasTheme ? 'GWTheme' : 'no theme'}
      />
      {hasTheme ? (
        <ThemeWrapperOldTheme>
          <TypographyDemo />
        </ThemeWrapperOldTheme>
      ) : (
        <TypographyDemo />
      )}
    </Grid>
  );
};

export const Typography: React.FC = () => (
  <StoryWrapper title="Typography">
    <TypographyDemo />
  </StoryWrapper>
);
