/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Card, Typography, Grid } from '@mui/material';

import { StoryWrapper } from './StoryWrapper';

const styles = {
  card: {
    height: '300px',
    width: '300px',
    padding: 2,
    display: 'inline-block',
    margin: 2,
  },
};

export const CardDemo: React.FC = () => {
  return (
    <Grid container>
      <Card variant="outlined" elevation={0} sx={styles.card}>
        <Typography>with border (outlined)</Typography>
      </Card>
      <Card elevation={0} sx={styles.card}>
        <Typography>without border</Typography>
      </Card>
    </Grid>
  );
};

export const Cards: React.FC = () => (
  <StoryWrapper
    title="Cards"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf9271ed18a9466d4e2e50"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e6930d8bc230b183870c"
  >
    <CardDemo />
  </StoryWrapper>
);
