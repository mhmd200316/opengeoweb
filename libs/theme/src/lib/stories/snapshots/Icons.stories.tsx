/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { darkTheme, lightTheme, ThemeWrapper } from '../../components/Theme';
import { StoryIcons } from '../Icons.stories';

export default { title: 'snapshots/Icons' };
export interface IconGridItemProps {
  name: string;
  icon: React.ReactNode;
}

export const IconsLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <StoryIcons />
  </ThemeWrapper>
);

export const IconsDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <StoryIcons />
  </ThemeWrapper>
);
