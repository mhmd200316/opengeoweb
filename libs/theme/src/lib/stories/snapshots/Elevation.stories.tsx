/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { darkTheme, lightTheme, ThemeWrapper } from '../../components/Theme';
import { ElevationDemo } from '../Elevation.stories';

export default { title: 'snapshots/Elevation' };

export const ElevationLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <ElevationDemo />
  </ThemeWrapper>
);

export const ElevationDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <ElevationDemo />
  </ThemeWrapper>
);

ElevationLight.parameters = {
  layout: 'fullscreen',
};

ElevationDark.parameters = {
  layout: 'fullscreen',
};
