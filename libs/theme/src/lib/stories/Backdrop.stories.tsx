/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Backdrop as MuiBackdrop, Button, Card, Grid } from '@mui/material';
import { StoryWrapper } from './StoryWrapper';

interface BackdropDemoProps {
  isOpen?: boolean;
  handleClose?: () => void;
}

export const BackdropDemo: React.FC<BackdropDemoProps> = ({
  isOpen = true,
  handleClose = (): void => null,
}: BackdropDemoProps) => {
  return <MuiBackdrop open={isOpen} onClick={handleClose} />;
};

const BackDropWrapper: React.FC = () => {
  const [isOpen, setIsOpen] = React.useState<boolean>(true);

  const onToggleBackDrop = (): void => setIsOpen(!isOpen);

  return (
    <Grid container>
      <Card
        elevation={0}
        sx={{
          padding: 2,
          display: 'inline-block',
        }}
      >
        <Button onClick={onToggleBackDrop}>Toggle backdrop</Button>
      </Card>
      <BackdropDemo
        isOpen={isOpen}
        handleClose={(): void => setIsOpen(false)}
      />
    </Grid>
  );
};

export const Backdrop: React.FC = () => (
  <StoryWrapper
    title="Backdrop"
    // Backdrop has no seperate design, so linked to colors
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf84d920b99428d7f98555"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e5dc12278e35d433aa2d"
  >
    <BackDropWrapper />
  </StoryWrapper>
);
