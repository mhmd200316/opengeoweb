/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Button, Card, Grid, Tooltip } from '@mui/material';
import { StoryWrapper } from './StoryWrapper';

const styles = {
  card: {
    padding: 2,
    width: 600,
    height: 112,
    display: 'inline-block',
  },
  button: {
    color: 'text.primary',
  },
};

export const TooltipDemo: React.FC = () => {
  return (
    <Card elevation={0} sx={styles.card}>
      <Grid container>
        <Grid item>
          <Tooltip title="Tooltip top placement" placement="top">
            <Button sx={styles.button}>top placement</Button>
          </Tooltip>
          <Tooltip open title="Tooltip bottom placement" placement="bottom">
            <Button sx={styles.button}>bottom placement</Button>
          </Tooltip>
          <Tooltip title="Tooltip right placement" placement="right">
            <Button sx={styles.button}>right placement</Button>
          </Tooltip>
          <Tooltip title="Tooltip left placement" placement="left">
            <Button sx={styles.button}>left placement</Button>
          </Tooltip>
        </Grid>
      </Grid>
    </Card>
  );
};

export const Tooltips: React.FC = () => (
  <StoryWrapper
    title="Tooltip"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf92699c931a2a11621c19"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e6920eaaea0ab7da3d16"
  >
    <TooltipDemo />
  </StoryWrapper>
);
