/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  FormControlLabel,
  Grid,
  Link,
  Paper,
  Switch,
  Typography,
} from '@mui/material';
import {
  useThemeContext,
  ThemeWrapper,
} from '../components/Theme/ThemeContext';

interface StoryLayoutProps {
  children: React.ReactNode;
  title: string;
  linkToLightThemeDesign?: string;
  linkToDarkThemeDesign?: string;
}

const StoryLayout: React.FC<StoryLayoutProps> = ({
  children,
  title,
  linkToLightThemeDesign,
  linkToDarkThemeDesign,
}: StoryLayoutProps) => {
  const { isDark, toggleTheme } = useThemeContext();

  const linkToZeplinDesign = isDark
    ? linkToDarkThemeDesign
    : linkToLightThemeDesign;

  return (
    <Paper
      sx={{
        padding: 2,
      }}
    >
      <Grid container direction="row">
        <Grid
          sx={{
            marginBottom: '50px',
          }}
          container
          alignItems="flex-start"
        >
          <Grid item xs={9}>
            <Typography variant="h1">{title}</Typography>
            {linkToZeplinDesign && (
              <Typography variant="body1">
                <Link
                  href={linkToZeplinDesign}
                  target="_blank"
                >{`Follows color and naming convention as defined in ${linkToZeplinDesign}`}</Link>
              </Typography>
            )}
          </Grid>
          <Grid item xs={3} container justifyContent="flex-end">
            <FormControlLabel
              control={<Switch checked={isDark} onChange={toggleTheme} />}
              label={!isDark ? 'light theme' : 'dark theme'}
            />
          </Grid>
        </Grid>
        <Grid container>{children}</Grid>
      </Grid>
    </Paper>
  );
};

export const StoryWrapper: React.FC<StoryLayoutProps> = (
  props: StoryLayoutProps,
) => (
  <ThemeWrapper>
    <StoryLayout {...props} />
  </ThemeWrapper>
);
