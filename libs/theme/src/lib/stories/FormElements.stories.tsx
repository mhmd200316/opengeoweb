/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  Button,
  ButtonGroup,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  InputLabel,
  MenuItem,
  Paper,
  Radio,
  RadioGroup,
  Select,
  Switch,
  Tab,
  Tabs,
  TextField,
  Tooltip,
  Typography,
} from '@mui/material';
import { Notifications } from '../components/Icons';

import { ThemeWrapperOldTheme } from '../components/Theme/ThemeContext';
import { StoryWrapper } from './StoryWrapper';

// TODO: this is just a part of all styles, there are more defined in Theme.tsx. It would be nice to have a complete overview so we can use it as a styleguide.
export const FormElementsDemo: React.FC = () => {
  return (
    <Paper style={{ width: '500px' }}>
      <Grid container spacing={1}>
        {/** MUI Elements */}
        <Grid item xs={12} container spacing={1}>
          <Grid item xs={12}>
            <Typography
              variant="h1"
              sx={{
                fontSize: 18,
                borderBottom: '1px solid black',
              }}
            >
              Form elements
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <ButtonGroup>
              <Tooltip title="Button tooltip">
                <Button color="secondary">One</Button>
              </Tooltip>
              <Button color="secondary">Two</Button>
              <Button color="secondary">Three</Button>
            </ButtonGroup>
          </Grid>
          <Grid item xs={12}>
            <Tabs
              indicatorColor="secondary"
              textColor="secondary"
              variant="scrollable"
              scrollButtons="auto"
              value="ONE"
            >
              <Tab
                label="One"
                value="ONE"
                icon={<Notifications fontSize="small" color="action" />}
              />
              <Tab label="Two" value="TWO" />
            </Tabs>
          </Grid>
          <Grid item xs={12}>
            <TextField
              label="TextfieldLabel"
              value="Content"
              helperText="Some helpertext"
              variant="standard"
            />
            &nbsp;
            <TextField
              label="TextfieldLabel"
              value="Disabled textfield"
              helperText="Some helpertext"
              disabled
              variant="standard"
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              label="Textfield"
              value="Text content filled variant"
              helperText="Some helpertext"
              variant="filled"
            />
            <TextField
              label="Textfield"
              value="But this one is disabled"
              helperText="Some helpertext"
              disabled
              variant="filled"
            />
          </Grid>
          <Grid item xs={12}>
            <FormControl component="fieldset">
              <FormLabel component="legend">Radio Group</FormLabel>
              <RadioGroup
                name="Radio Button"
                value="Clouds"
                onChange={
                  // eslint-disable-next-line @typescript-eslint/no-empty-function
                  (): void => {}
                }
              >
                <FormControlLabel
                  value="Clouds"
                  control={<Radio />}
                  label="Clouds"
                />
                <FormControlLabel
                  value="Rain"
                  control={<Radio />}
                  label="Rain"
                />
              </RadioGroup>
            </FormControl>
            &nbsp;
            <FormControl component="fieldset">
              <FormLabel component="legend">Radio Group Disabled</FormLabel>
              <RadioGroup
                name="Radio Button"
                value="Clouds"
                onChange={
                  // eslint-disable-next-line @typescript-eslint/no-empty-function
                  (): void => {}
                }
              >
                <FormControlLabel
                  value="Clouds"
                  control={<Radio />}
                  label="Clouds"
                  disabled
                />
                <FormControlLabel
                  value="Rain"
                  control={<Radio />}
                  label="Rain"
                  disabled
                />
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <FormControl variant="standard">
              <InputLabel>Select</InputLabel>
              <Select
                value={20}
                onChange={
                  // eslint-disable-next-line @typescript-eslint/no-empty-function
                  (): void => {}
                }
                autoWidth={false}
                style={{ width: '200px' }}
              >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>
                  Thirty and some really long text stuff
                </MenuItem>
              </Select>
            </FormControl>
            &nbsp;
            <FormControl variant="standard">
              <InputLabel>Select Disabled</InputLabel>
              <Select
                value={20}
                onChange={
                  // eslint-disable-next-line @typescript-eslint/no-empty-function
                  (): void => {}
                }
                disabled
                autoWidth={false}
                style={{ width: '200px' }}
              >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>
                  Thirty and some really long text stuff
                </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <FormControl variant="filled">
              <InputLabel>Select Filled</InputLabel>
              <Select
                value={20}
                onChange={
                  // eslint-disable-next-line @typescript-eslint/no-empty-function
                  (): void => {}
                }
                autoWidth={false}
                style={{ width: '200px' }}
              >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>
                  Thirty and some really long text stuff
                </MenuItem>
              </Select>
            </FormControl>
            &nbsp;
            <FormControl variant="filled">
              <InputLabel>Select Filled Disabled</InputLabel>
              <Select
                value={20}
                onChange={
                  // eslint-disable-next-line @typescript-eslint/no-empty-function
                  (): void => {}
                }
                disabled
                autoWidth={false}
                style={{ width: '200px' }}
              >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>
                  Thirty and some really long text stuff
                </MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item container xs={12}>
            <Grid item>
              <FormControlLabel
                control={
                  <Checkbox
                    defaultChecked
                    color="secondary"
                    inputProps={{ 'aria-label': 'checkbox' }}
                  />
                }
                label="Checkbox"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    defaultChecked
                    color="secondary"
                    inputProps={{ 'aria-label': 'disabled' }}
                    disabled
                  />
                }
                label="Disabled"
              />
            </Grid>
            <Grid item>
              <FormControlLabel
                control={
                  <Checkbox
                    color="secondary"
                    inputProps={{ 'aria-label': 'unchecked' }}
                  />
                }
                label="Unchecked"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    color="secondary"
                    inputProps={{ 'aria-label': 'disabled unchecked' }}
                    disabled
                  />
                }
                label="Disabled unchecked"
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  );
};

export const FormElementsGWTheme: React.FC = () => {
  const [hasTheme, setHasTheme] = React.useState(true);

  return (
    <>
      <FormControlLabel
        control={
          <Switch
            checked={!hasTheme}
            onChange={(): void => setHasTheme(!hasTheme)}
          />
        }
        label={hasTheme ? 'GWTheme' : 'no theme'}
      />
      {hasTheme ? (
        <ThemeWrapperOldTheme>
          <FormElementsDemo />
        </ThemeWrapperOldTheme>
      ) : (
        <FormElementsDemo />
      )}
    </>
  );
};

export const FormElements: React.FC = () => (
  <StoryWrapper title="FormElements">
    <FormElementsDemo />
  </StoryWrapper>
);
