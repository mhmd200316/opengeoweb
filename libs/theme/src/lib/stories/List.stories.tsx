/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box, Card, List as MuiList, ListItem } from '@mui/material';

import { StoryWrapper } from './StoryWrapper';

export const ListDemo: React.FC = () => {
  return (
    <Box
      sx={{
        padding: 2,
        width: 600,
      }}
      component="div"
    >
      <MuiList>
        <ListItem divider>item</ListItem>
        <ListItem divider>item</ListItem>
        <ListItem>item</ListItem>
      </MuiList>
    </Box>
  );
};

export const List: React.FC = () => (
  <StoryWrapper
    title="List"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea1ddffa198a4932b6977a"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e69240e0eb31b66b4b17"
  >
    <Card elevation={0}>
      <ListDemo />
    </Card>
  </StoryWrapper>
);
