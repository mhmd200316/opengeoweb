/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { ToggleButton, Typography, Box } from '@mui/material';
import { Add } from '../components/Icons';
import { StoryWrapper } from './StoryWrapper';
import { gwButtonVariants } from '../components/Theme/types';
import { StoryHeader } from './StoryHeader';

const Row: React.FC = ({ children }) => (
  <Box sx={{ width: '100%', button: { margin: '0px 10px 10px 0' } }}>
    {children}
  </Box>
);

// Note: Please be aware that ToggleButton does not support variants yet, so use className in the meantime
export const ToggleButtonDemo: React.FC = () => (
  <Box>
    <Typography sx={{ fontSize: '1.6rem', margin: '10px 0' }}>
      Desktop 14px
    </Typography>
    <Row>
      <StoryHeader title="default" />
      {gwButtonVariants.map((variant) => (
        <>
          <ToggleButton value="0" className={variant}>
            {variant}
          </ToggleButton>
          <ToggleButton value="0" className={variant}>
            <Add />
            {variant}
          </ToggleButton>
        </>
      ))}
    </Row>

    <Row>
      <StoryHeader title="active" />

      {gwButtonVariants.map((variant) => (
        <>
          <ToggleButton value="0" className={variant} selected>
            {variant}
          </ToggleButton>
          <ToggleButton value="0" className={variant} selected>
            <Add />
            {variant}
          </ToggleButton>
        </>
      ))}
    </Row>

    <Row>
      <StoryHeader title="disabled" />

      {gwButtonVariants.map((variant) => (
        <>
          <ToggleButton value="0" className={variant} disabled>
            {variant}
          </ToggleButton>
          <ToggleButton value="0" className={variant} disabled>
            <Add />
            {variant}
          </ToggleButton>
        </>
      ))}
    </Row>

    <Typography sx={{ fontSize: '1.6rem', margin: '10px 0' }}>
      Application 12px
    </Typography>
    <Row>
      <StoryHeader title="default" />

      {gwButtonVariants.map((variant) => (
        <>
          <ToggleButton value="0" className={variant} size="small">
            {variant}
          </ToggleButton>
          <ToggleButton value="0" className={variant} size="small">
            <Add />
            {variant}
          </ToggleButton>
        </>
      ))}
    </Row>

    <Row>
      <StoryHeader title="active" />

      {gwButtonVariants.map((variant) => (
        <>
          <ToggleButton value="0" className={variant} size="small" selected>
            {variant}
          </ToggleButton>
          <ToggleButton value="0" className={variant} size="small" selected>
            <Add />
            {variant}
          </ToggleButton>
        </>
      ))}
    </Row>

    <Row>
      <StoryHeader title="disabled" />

      {gwButtonVariants.map((variant) => (
        <>
          <ToggleButton value="0" className={variant} size="small" disabled>
            {variant}
          </ToggleButton>
          <ToggleButton value="0" className={variant} size="small" disabled>
            <Add />
            {variant}
          </ToggleButton>
        </>
      ))}
    </Row>
  </Box>
);

export const ToggleButtons = (): React.ReactElement => (
  <StoryWrapper
    title="ToggleButton"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf85c60f301e47ca4eee55"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68ceb304607400f6d5e"
  >
    <ToggleButtonDemo />
  </StoryWrapper>
);

ToggleButtons.storyName = 'ToggleButton';
