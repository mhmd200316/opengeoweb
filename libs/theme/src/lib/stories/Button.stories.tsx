/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Button, Typography, Box } from '@mui/material';
import { Add } from '../components/Icons';
import { StoryWrapper } from './StoryWrapper';
import { gwButtonVariants } from '../components/Theme/types';
import { StoryHeader } from './StoryHeader';

const Row: React.FC = ({ children }) => (
  <Box sx={{ width: '100%', button: { margin: '0px 10px 10px 0' } }}>
    {children}
  </Box>
);

export const ButtonDemo: React.FC = () => (
  <Box>
    <Typography sx={{ fontSize: '1.6rem', margin: '10px 0' }}>
      Desktop 14px
    </Typography>
    <Row>
      <StoryHeader title="default" />
      {gwButtonVariants.map((variant) => (
        <>
          <Button variant={variant}>{variant}</Button>
          <Button variant={variant} startIcon={<Add />}>
            {variant}
          </Button>
        </>
      ))}
    </Row>
    <Row>
      <StoryHeader title="disabled" />
      {gwButtonVariants.map((variant) => (
        <>
          <Button variant={variant} disabled>
            {variant}
          </Button>
          <Button variant={variant} disabled startIcon={<Add />}>
            {variant}
          </Button>
        </>
      ))}
    </Row>

    <Typography sx={{ fontSize: '1.6rem', margin: '10px 0' }}>
      Application 12px
    </Typography>
    <Row>
      <StoryHeader title="default" />
      {gwButtonVariants.map((variant) => (
        <>
          <Button variant={variant} size="small">
            {variant}
          </Button>
          <Button variant={variant} size="small" startIcon={<Add />}>
            {variant}
          </Button>
        </>
      ))}
    </Row>
    <Row>
      <StoryHeader title="disabled" />
      {gwButtonVariants.map((variant) => (
        <>
          <Button variant={variant} size="small" disabled>
            {variant}
          </Button>
          <Button variant={variant} size="small" disabled startIcon={<Add />}>
            {variant}
          </Button>
        </>
      ))}
    </Row>
  </Box>
);

export const Buttons = (): React.ReactElement => (
  <StoryWrapper
    title="Button"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf85c60f301e47ca4eee55"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68ceb304607400f6d5e"
  >
    <ButtonDemo />
  </StoryWrapper>
);

Buttons.storyName = 'Button';
