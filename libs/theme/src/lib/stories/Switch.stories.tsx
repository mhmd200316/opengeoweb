/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  FormGroup,
  FormControlLabel,
  Switch as MuiSwitch,
} from '@mui/material';
import { StoryWrapper } from './StoryWrapper';

export const SwitchDemo: React.FC = () => {
  return (
    <FormGroup
      sx={{
        width: 300,
        padding: 1,
      }}
    >
      <FormControlLabel control={<MuiSwitch />} label="Unchecked" />
      <FormControlLabel
        control={<MuiSwitch defaultChecked />}
        label="Checked"
      />
      <FormControlLabel
        disabled
        control={<MuiSwitch />}
        label="Unchecked Disabled"
      />
      <FormControlLabel
        disabled
        control={<MuiSwitch defaultChecked />}
        label="Checked Disabled"
      />
    </FormGroup>
  );
};

export const Switch: React.FC = () => (
  <StoryWrapper
    title="Switch"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62d933eb1a6eee4974f11745"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62e25835359f7084c3e32f1b"
  >
    <SwitchDemo />
  </StoryWrapper>
);
