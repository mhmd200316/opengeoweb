/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Typography, Box, IconButton } from '@mui/material';
import { Add } from '../components/Icons';
import { StoryWrapper } from './StoryWrapper';
import { StoryHeader } from './StoryHeader';

const Row: React.FC = ({ children }) => (
  <Box
    sx={{
      width: '100%',
      height: '50px',
      button: { margin: '0px 10px 10px 0' },
    }}
  >
    {children}
  </Box>
);

export const IconButtonDemo: React.FC = () => (
  <Box>
    <Typography sx={{ fontSize: '1.6rem', margin: '10px 0' }}>
      Default icon color (flat)
    </Typography>

    <Row>
      <StoryHeader title="default" />

      <IconButton size="small">
        <Add />
      </IconButton>

      <IconButton size="medium">
        <Add />
      </IconButton>

      <IconButton size="large">
        <Add />
      </IconButton>
    </Row>

    <Row>
      <StoryHeader title="disabled" />

      <IconButton size="small" disabled>
        <Add />
      </IconButton>

      <IconButton size="medium" disabled>
        <Add />
      </IconButton>

      <IconButton size="large" disabled>
        <Add />
      </IconButton>
    </Row>
  </Box>
);

export const IconButtons = (): React.ReactElement => (
  <StoryWrapper
    title="IconButton"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf85c60f301e47ca4eee55"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68ceb304607400f6d5e"
  >
    <IconButtonDemo />
  </StoryWrapper>
);

IconButtons.storyName = 'IconButton';
