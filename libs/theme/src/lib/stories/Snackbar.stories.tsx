/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Button, IconButton, Snackbar as MuiSnackbar } from '@mui/material';
import { Close } from '../components/Icons';
import { StoryWrapper } from './StoryWrapper';

interface SnackbarDemoProps {
  open?: boolean;
  handleClose?: () => void;
}

export const SnackbarDemo: React.FC<SnackbarDemoProps> = ({
  open = true,
  handleClose = (): void => null,
}: SnackbarDemoProps) => {
  return (
    <MuiSnackbar
      open={open}
      onClose={handleClose}
      message="This is a snackbar notification"
      action={
        <IconButton
          aria-label="close"
          onClick={handleClose}
          sx={{
            color: 'geowebColors.snackbar.action',
            '&:hover': {
              backgroundColor: 'geowebColors.snackbar.actionHover',
            },
          }}
        >
          <Close />
        </IconButton>
      }
    />
  );
};

const SnackbarWrapper: React.FC = () => {
  const [open, setOpen] = React.useState(true);

  const handleToggle = (): void => {
    setOpen(!open);
  };

  const handleClose = (): void => {
    setOpen(false);
  };

  return (
    <>
      <Button variant="outlined" onClick={handleToggle}>
        Toggle snackbar
      </Button>
      <SnackbarDemo open={open} handleClose={handleClose} />
    </>
  );
};

export const Snackbar: React.FC = () => (
  <StoryWrapper
    title="Snackbar"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf926c9c931a2a11621c31"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e6973d055b0846449d58"
  >
    <SnackbarWrapper />
  </StoryWrapper>
);
