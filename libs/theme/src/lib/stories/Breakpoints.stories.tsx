/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box, Typography } from '@mui/material';

import { StoryWrapper } from './StoryWrapper';

import { breakpoints } from '../components/Theme/utils';

interface DisplayBoxProps {
  children: React.ReactNode;
  breakpoint: string;
}

const DisplayBox: React.FC<DisplayBoxProps> = ({
  children,
  breakpoint,
}: DisplayBoxProps) => {
  const display = {
    xs: 'none',
    sm: 'none',
    md: 'none',
    lg: 'none',
    xl: 'none',
    [breakpoint]: 'block',
  };
  return <Box display={display}>{children}</Box>;
};

export const BreakpointDemo: React.FC = () => {
  const breakpointsList = ['xs', 'sm', 'md', 'lg', 'xl'];
  const breakpointsWithLabel = Object.keys(breakpoints).map((key, index) => {
    return {
      name: key,
      value: breakpointsList[index],
    };
  });
  return (
    <Box>
      {breakpointsWithLabel.map(({ name, value }) => (
        <DisplayBox key={name} breakpoint={value}>
          <Typography
            sx={{ fontWeight: 'bold', fontSize: 16 }}
          >{`${name} ${value}`}</Typography>
        </DisplayBox>
      ))}
    </Box>
  );
};

export const Breakpoints: React.FC = () => (
  <StoryWrapper
    title="Breakpoints"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/dashboard?q=Grids%20(responsiveness)"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/dashboard?q=Grids%20(responsiveness)"
  >
    <BreakpointDemo />
  </StoryWrapper>
);
