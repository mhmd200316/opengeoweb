/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  Typography,
  Table as MuiTable,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';

import { StoryWrapper } from './StoryWrapper';

type ExampleRow = {
  name: string;
  calories: number;
  fat: number;
  carbs: number;
  protein: number;
};

const createRowData = (name, calories, fat, carbs, protein): ExampleRow => {
  return { name, calories, fat, carbs, protein };
};

const items: ExampleRow[] = [
  createRowData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createRowData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createRowData('Eclair', 262, 16.0, 24, 6.0),
  createRowData('Cupcake', 305, 3.7, 67, 4.3),
  createRowData('Gingerbread', 356, 16.0, 49, 3.9),
];

// TODO: add more cell content variants like Select and (icon) Buttons https://gitlab.com/opengeoweb/opengeoweb/-/issues/1050
const CustomTableRow: React.FC<ExampleRow> = ({
  name,
  calories,
  fat,
  carbs,
  protein,
}: ExampleRow) => {
  return (
    <TableRow>
      <TableCell component="th" scope="row">
        {name}
      </TableCell>
      <TableCell>{calories}</TableCell>
      <TableCell>{fat}</TableCell>
      <TableCell>{carbs}</TableCell>
      <TableCell>{protein}</TableCell>
    </TableRow>
  );
};

export const TableDemo: React.FC = () => {
  return (
    <TableContainer style={{ padding: '0 10px' }}>
      <MuiTable>
        <TableHead>
          <TableRow>
            <TableCell>Dessert (100g serving)</TableCell>
            <TableCell>Calories</TableCell>
            <TableCell>Fat&nbsp;(g)</TableCell>
            <TableCell>Carbs&nbsp;(g)</TableCell>
            <TableCell>Protein&nbsp;(g)</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {items.map((row) => (
            <CustomTableRow key={row.name} {...row} />
          ))}
        </TableBody>
      </MuiTable>
    </TableContainer>
  );
};

export const Table: React.FC = () => (
  <StoryWrapper
    title="Table"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf927271b4154a3d0a25be"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e69005029c358090bd4e"
  >
    <Typography
      variant="h2"
      style={{
        fontSize: 14,
        color: 'rgba(255, 0, 255, 0.98)',
        fontWeight: 500,
      }}
    >
      New & accessible table (14 pts)
    </Typography>
    <TableDemo />
  </StoryWrapper>
);
