/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  Typography,
  Box,
  TextField as MuiTextField,
  TextFieldProps,
} from '@mui/material';
import { StoryWrapper } from './StoryWrapper';
import { StoryHeader } from './StoryHeader';

// wrapper for state
const TextField: React.FC<TextFieldProps> = ({ value = '', ...props }) => {
  const [newValue, setValue] = React.useState(value);
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setValue(event.target.value);
  };
  return (
    <MuiTextField
      value={newValue}
      onChange={handleChange}
      {...props}
      sx={{ marginRight: '10px' }}
    />
  );
};

export const TextFieldDemo: React.FC = () => (
  <Box>
    <Typography sx={{ fontSize: '1.6rem', margin: '10px 0' }}>
      TextField variants
    </Typography>
    <Box>
      <StoryHeader title="filled" />

      <TextField label="Label" helperText="assistive text" />

      <TextField label="Label" value="input text" />

      <TextField label="Label" value="input text focused" focused />

      <TextField
        label="Label"
        value="input text error"
        error
        helperText="assistive text"
      />

      <TextField label="Label" disabled />

      <TextField label="Label" value="input disabled" disabled />
    </Box>
  </Box>
);

export const TextFields = (): React.ReactElement => (
  <StoryWrapper
    title="TextField"
    linkToLightThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf92695e71d82acd448ea2"
    linkToDarkThemeDesign="https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e68f80d83109782953ac"
  >
    <TextFieldDemo />
  </StoryWrapper>
);

TextFields.storyName = 'TextField';
