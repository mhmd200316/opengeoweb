/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { createApiInstance, Credentials } from '@opengeoweb/api';
import { AxiosInstance, AxiosResponse } from 'axios';

import { ViewPreset, ViewPresetListItem } from '../store/viewPresets/types';

export const getIdFromUrl = (url: string): string =>
  url && url.match(/[^/]+$/)[0];

export type PresetsApi = {
  getViewPresets: () => Promise<{ data: ViewPresetListItem[] }>;
  getViewPreset: (presetId: string) => Promise<{ data: ViewPreset }>;
  saveViewPreset: (presetId: string, data: ViewPreset) => Promise<void>;
  saveViewPresetAs: (data: ViewPreset) => Promise<string>;
  deleteViewPreset: (presetId: string) => Promise<void>;
};

const getApiRoutes = (axiosInstance: AxiosInstance): PresetsApi => ({
  getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
    return axiosInstance.get('/viewpreset');
  },
  getViewPreset: (presetId: string): Promise<{ data: ViewPreset }> => {
    return axiosInstance.get(`/viewpreset/${presetId}`);
  },
  saveViewPreset: (presetId: string, data: ViewPreset): Promise<void> => {
    return axiosInstance.put(`/viewpreset/${presetId}/`, data, {
      headers: { 'content-type': 'application/json' },
    });
  },
  saveViewPresetAs: (data: ViewPreset): Promise<string> => {
    return axiosInstance
      .post(`/viewpreset/`, data, {
        headers: { 'content-type': 'application/json' },
      })
      .then((response: AxiosResponse): string => {
        const headerLocation = response?.request.getResponseHeader('Location');
        return getIdFromUrl(headerLocation);
      });
  },
  deleteViewPreset: (presetId: string): Promise<void> => {
    return axiosInstance.delete(`/viewpreset/${presetId}`);
  },
});

export const createApi = (
  url: string,
  appUrl: string,
  auth: Credentials,
  setAuth: (cred: Credentials) => void,
  authTokenUrl: string,
  authClientId: string,
): PresetsApi => {
  const instance = createApiInstance(
    url,
    appUrl,
    auth,
    setAuth,
    authTokenUrl,
    authClientId,
  );

  return getApiRoutes(instance);
};
