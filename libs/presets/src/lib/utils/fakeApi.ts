/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { AxiosInstance } from 'axios';
import { createFakeApiInstance } from '@opengeoweb/api';
import { ViewPreset, ViewPresetListItem } from '../store/viewPresets/types';
import { PresetsApi } from './api';
import defaultInitialViewPresets from './fakeApi/viewpresets.json';
import defaultInitialViewPresetsRadar from './fakeApi/viewpresets/Radar.json';
import defaultInitialViewPresetsTemperature from './fakeApi/viewpresets/Temperature.json';
import defaultInitialViewPresetsSatellite from './fakeApi/viewpresets/Satellite.json';

const getApiRoutes = (axiosInstance: AxiosInstance): PresetsApi => ({
  getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
    return axiosInstance.get('/viewpreset').then(() => {
      return {
        data: defaultInitialViewPresets as ViewPresetListItem[],
      };
    });
  },
  getViewPreset: (presetId: string): Promise<{ data: ViewPreset }> => {
    return axiosInstance.get(`/viewpreset/${presetId}`).then(() => {
      const selectedPreset = (id: string): ViewPreset => {
        switch (id) {
          case 'Radar':
            return defaultInitialViewPresetsRadar as ViewPreset;
          case 'Temperature':
            return defaultInitialViewPresetsTemperature as ViewPreset;
          case 'Satellite':
            return defaultInitialViewPresetsSatellite as ViewPreset;
          default:
            return defaultInitialViewPresetsRadar as ViewPreset;
        }
      };

      return {
        data: selectedPreset(presetId),
      };
    });
  },
  saveViewPreset: (presetId: string, data: ViewPreset): Promise<void> => {
    // eslint-disable-next-line no-console
    console.log('api: save view preset', presetId, data);
    return axiosInstance.put(`/viewpreset/${presetId}/`, data);
  },
  saveViewPresetAs: (data: ViewPreset): Promise<string> => {
    // eslint-disable-next-line no-console
    console.log('api: save view preset as', data);
    return axiosInstance.post(`/viewpreset/`, data).then(() => null);
  },
  deleteViewPreset: (presetId: string): Promise<void> => {
    // eslint-disable-next-line no-console
    console.log('api: delete view preset', presetId);
    return axiosInstance.delete(`/viewpreset/${presetId}`);
  },
});

export const createApi = (): PresetsApi => {
  const instance = createFakeApiInstance();
  return getApiRoutes(instance);
};
