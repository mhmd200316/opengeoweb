/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as utils from '@opengeoweb/api';
import { createFakeApiInstance } from '@opengeoweb/api';
import { LayerType } from '@opengeoweb/webmap';
import { ViewPreset } from '../store/viewPresets/types';
import { createApi, getIdFromUrl } from './api';

describe('src/utils/api', () => {
  const fakeAxiosInstance = createFakeApiInstance();
  describe('getIdFromUrl', () => {
    it('should an id from url', () => {
      expect(getIdFromUrl(null)).toBeNull();
      expect(
        getIdFromUrl(
          'https://0.0.0.1:8081/viewpreset/bcf71f54-0da9-11ed-b5bd-0644f2de783d',
        ),
      ).toEqual('bcf71f54-0da9-11ed-b5bd-0644f2de783d');
      expect(
        getIdFromUrl(
          'https://0.0.0.1:8081/bcf71f54-0da9-11ed-b5bd-0644f2de783d',
        ),
      ).toEqual('bcf71f54-0da9-11ed-b5bd-0644f2de783d');
      expect(
        getIdFromUrl(
          'https://0.0.0.1:8081/viewpreset/acf71f54-xs90-11ed-b5bd-0644f2de783d',
        ),
      ).toEqual('acf71f54-xs90-11ed-b5bd-0644f2de783d');
    });
  });
  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );
      expect(api.getViewPresets).toBeTruthy();
    });

    it('should call with the right params for getViewPresets', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      await api.getViewPresets();
      expect(spy).toHaveBeenCalledWith('/viewpreset');
    });

    it('should call with the right params for getViewPreset', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const presetId = 'test-1';
      await api.getViewPreset(presetId);
      expect(spy).toHaveBeenCalledWith(`/viewpreset/${presetId}`);
    });

    it('should call with the right params for saveViewPreset', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'put');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const preset: ViewPreset = {
        componentType: 'Map',
        id: 'Radar',
        keywords: 'WMS,Radar',
        title: 'My new radar',
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'base-layer-1',
                name: 'OpenStreetMap_NL',
                type: 'twms',
                layerType: LayerType.baseLayer,
                enabled: true,
              },
              {
                enabled: true,
                format: 'image/png',
                layerType: LayerType.mapLayer,
                name: 'RAD_NL25_PCP_CM',
                service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                style: 'precip-blue-transparent/nearest',
              },
            ],
            proj: {
              bbox: {
                bottom: 6408480.4514,
                left: 58703.6377,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            showTimeSlider: true,
          },
        },
      };

      await api.saveViewPreset(preset.id, preset);
      expect(spy).toHaveBeenCalledWith(`/viewpreset/${preset.id}/`, preset, {
        headers: { 'content-type': 'application/json' },
      });
    });

    it('should call with the right params for saveViewPresetAs', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(
        'fakeURL',
        'fakeUrl',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'anotherFakeUrl',
        'fakeauthClientId',
      );

      const preset: ViewPreset = {
        componentType: 'Map',
        id: 'Radar',
        keywords: 'WMS,Radar',
        title: 'My new radar',
        initialProps: {
          mapPreset: {
            layers: [
              {
                id: 'base-layer-1',
                name: 'OpenStreetMap_NL',
                type: 'twms',
                layerType: LayerType.baseLayer,
                enabled: true,
              },
              {
                enabled: true,
                format: 'image/png',
                layerType: LayerType.mapLayer,
                name: 'RAD_NL25_PCP_CM',
                service: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
                style: 'precip-blue-transparent/nearest',
              },
            ],
            proj: {
              bbox: {
                bottom: 6408480.4514,
                left: 58703.6377,
                right: 3967387.5161,
                top: 11520588.9031,
              },
              srs: 'EPSG:3857',
            },
            showTimeSlider: true,
          },
        },
      };

      await api.saveViewPresetAs(preset);
      expect(spy).toHaveBeenCalledWith(`/viewpreset/`, preset, {
        headers: { 'content-type': 'application/json' },
      });
    });
  });
});
