/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import {
  createEntityAdapter,
  createSlice,
  PayloadAction,
} from '@reduxjs/toolkit';
import {
  LoadViewPresetsPayload,
  ViewPresetListItem,
  ViewPresetState,
} from './types';

export const presetsAdapter = createEntityAdapter<ViewPresetListItem>();

export const initialState: ViewPresetState = presetsAdapter.getInitialState();

const slice = createSlice({
  initialState,
  name: 'presets',
  reducers: {
    loadViewPresets: (draft, action: PayloadAction<LoadViewPresetsPayload>) => {
      const { viewPresets } = action.payload;
      if (!viewPresets || !viewPresets.length) {
        presetsAdapter.removeAll(draft);
        return;
      }
      presetsAdapter.setAll(draft, action.payload.viewPresets);
    },
  },
});

export const { reducer } = slice;
export const viewPresetActions = slice.actions;
