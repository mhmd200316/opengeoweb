/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import {
  reducer as viewPresetsReducer,
  initialState,
  viewPresetActions,
} from './reducer';
import { PresetScope, ViewPresetListItem, ViewPresetState } from './types';

describe('store/viewPresets/reducer', () => {
  describe('loadViewPresets', () => {
    it('should set viewPresets', () => {
      const viewPresets: ViewPresetListItem[] = [
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset',
          scope: 'system',
          title: 'Layer manager preset',
        },
        {
          date: '2022-06-01T12:34:27.787192',
          id: 'preset_2',
          scope: 'system',
          title: 'Layer manager preset 2',
        },
      ];
      const result = viewPresetsReducer(
        undefined,
        viewPresetActions.loadViewPresets({
          viewPresets,
        }),
      );

      expect(result).toEqual({
        ids: [viewPresets[0].id, viewPresets[1].id],
        entities: {
          [viewPresets[0].id]: viewPresets[0],
          [viewPresets[1].id]: viewPresets[1],
        },
      });
    });

    it('should return initialState when empty viewPresets passed', () => {
      const state: ViewPresetState = {
        entities: {
          preset_1: {
            date: '2022-06-01T12:34:27.787192',
            id: 'preset_1',
            scope: 'system' as PresetScope,
            title: 'Layer manager preset',
          },
        },
        ids: ['preset_1'],
      };
      const result1 = viewPresetsReducer(
        state,
        viewPresetActions.loadViewPresets({ viewPresets: null }),
      );
      expect(result1).toEqual(initialState);
      const result2 = viewPresetsReducer(
        state,
        viewPresetActions.loadViewPresets({ viewPresets: [] }),
      );
      expect(result2).toEqual(initialState);
    });
  });
});
