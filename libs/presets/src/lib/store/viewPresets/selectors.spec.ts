/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as viewPresetSelectors from './selectors';
import { ViewPresetListItem } from './types';

describe('store/viewPresets/selectors', () => {
  describe('getViewPresetsStore', () => {
    it('should return the viewstore', () => {
      const mockStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getViewPresetsStore(mockStore)).toEqual(
        mockStore.viewPresets,
      );

      expect(viewPresetSelectors.getViewPresetsStore(null)).toBeNull();
    });
  });

  describe('getViewPresets', () => {
    it('should return viewPresets', () => {
      const mockStore = {
        viewPresets: {
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
            preset_2: {
              id: 'mapPreset-2',
              scope: 'system',
              title: 'Layer manager preset 2',
              date: '2022-06-01T12:34:27.787192',
            } as ViewPresetListItem,
          },
          ids: ['preset', 'preset_2'],
        },
      };

      expect(viewPresetSelectors.getViewPresets(mockStore)).toEqual(
        mockStore.viewPresets.ids.map(
          (id) => mockStore.viewPresets.entities[id],
        ),
      );
    });

    it('should return empty list when no viewPresets in store', () => {
      const mockStore = {
        viewPresets: {
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getViewPresets(mockStore)).toEqual([]);
    });
  });
});
