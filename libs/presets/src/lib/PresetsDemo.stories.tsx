/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
  LegendConnect,
  LegendMapButtonConnect,
  MapControls,
  MapViewConnect,
  publicLayers,
  store,
  TimeSliderConnect,
  ZoomControlConnect,
} from '@opengeoweb/core';
import { useDefaultMapSettings } from './storyUtils/defaultStorySettings';
import { MapPresetsDialogConnect } from './components/MapPresetsDialog';
import { PresetsThemeStoreProvider } from './components/Providers';
import { PresetsApi } from './utils/api';
import { createApi as createFakeApi } from './utils/fakeApi';
import { ViewPreset } from './store/viewPresets/types';

export default { title: 'application/demo' };

interface MapWithLayerManagerProps {
  mapId: string;
}

const MapWithLayerManager: React.FC<MapWithLayerManagerProps> = ({
  mapId,
}: MapWithLayerManagerProps) => {
  useDefaultMapSettings({
    mapId,
    layers: [
      { ...publicLayers.radarLayer, id: `radar-${mapId}` },
      { ...publicLayers.harmonieWindPl, id: `harmonieWindPl-${mapId}` },
      { ...publicLayers.harmonieAirTemperature, id: `temp-${mapId}` },
    ],
    baseLayers: [
      { ...publicLayers.baseLayerGrey, id: `baseGrey-${mapId}` },
      publicLayers.overLayer,
    ],
  });

  return (
    <div style={{ height: '100vh' }}>
      <LayerManagerConnect leftComponent={<MapPresetsDialogConnect />} />
      <MapControls style={{ top: 0 }}>
        <ZoomControlConnect mapId={mapId} />
        <LayerManagerMapButtonConnect mapId={mapId} />
        <LegendMapButtonConnect mapId={mapId} />
      </MapControls>
      <LegendConnect mapId={mapId} />
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 50,
          width: '100%',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
      <MapViewConnect mapId={mapId} controls={{}} />
    </div>
  );
};

export const PresetsDemoOnMapLightTheme: React.FC = () => (
  <PresetsThemeStoreProvider store={store}>
    <MapWithLayerManager mapId="mapid_1" />
  </PresetsThemeStoreProvider>
);

export const PresetsDemoOnMapDarkTheme: React.FC = () => (
  <PresetsThemeStoreProvider theme={darkTheme} store={store}>
    <MapWithLayerManager mapId="mapid_1" />
  </PresetsThemeStoreProvider>
);

export const PresetsDemoWithErrorOnSelectPreset: React.FC = () => {
  const createApi = (): PresetsApi =>
    ({
      ...createFakeApi(),
      getViewPreset: (): Promise<{ data: ViewPreset }> =>
        new Promise((_, reject) =>
          setTimeout(() => {
            reject(new Error('loading preset failed'));
          }, 1000),
        ),
    } as PresetsApi);

  return (
    <PresetsThemeStoreProvider store={store} createApi={createApi}>
      <MapWithLayerManager mapId="mapid_1" />
    </PresetsThemeStoreProvider>
  );
};

export const PresetsDemoWithErrorOnSaveAndSaveAs: React.FC = () => {
  const createApi = (): PresetsApi =>
    ({
      ...createFakeApi(),
      saveViewPreset: (): Promise<void> =>
        new Promise((_, reject) =>
          setTimeout(() => {
            reject(new Error('Saving preset failed'));
          }, 1000),
        ),
      saveViewPresetAs: (): Promise<string> =>
        new Promise((_, reject) =>
          setTimeout(() => {
            reject(new Error('Example error from backend: duplicate title.'));
          }, 1000),
        ),
    } as PresetsApi);

  return (
    <PresetsThemeStoreProvider store={store} createApi={createApi}>
      <MapWithLayerManager mapId="mapid_1" />
    </PresetsThemeStoreProvider>
  );
};
