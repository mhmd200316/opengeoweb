/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Box } from '@mui/material';
import { store } from '@opengeoweb/core';
import MapPresetsDialogConnect from './MapPresetsDialogConnect';
import { PresetsThemeStoreProvider } from '../Providers';
import { PresetsApi } from '../../utils/api';
import { ViewPresetListItem } from '../../store/viewPresets/types';

export default { title: 'components/MapPresetsDialog' };

const Demo: React.FC = () => (
  <Box sx={{ width: '200px' }}>
    <MapPresetsDialogConnect />
  </Box>
);

export const MapPresetsDialogWithApi: React.FC = () => {
  return (
    <PresetsThemeStoreProvider store={store}>
      <Demo />
    </PresetsThemeStoreProvider>
  );
};

export const MapPresetsDialogWithErrorLoadPresetsList: React.FC = () => {
  const createApi = (): PresetsApi =>
    ({
      getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> =>
        new Promise((_, reject) =>
          setTimeout(() => {
            reject(new Error('error fetching presets list'));
          }, 3000),
        ),
    } as PresetsApi);

  return (
    <PresetsThemeStoreProvider store={store} createApi={createApi}>
      <Demo />
    </PresetsThemeStoreProvider>
  );
};
