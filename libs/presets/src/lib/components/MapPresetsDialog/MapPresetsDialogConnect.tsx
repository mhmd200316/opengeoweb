/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { useSelector, useDispatch } from 'react-redux';
import React from 'react';
import {
  AppStore,
  mapActions,
  uiSelectors,
  mapSelectors,
  mapTypes,
} from '@opengeoweb/core';
import { useApi, useApiContext } from '@opengeoweb/api';
import MapPresetsDialog from './MapPresetsDialog';
import { viewPresetActions, viewPresetSelectors } from '../../store';
import {
  PresetModuleStore,
  ViewPreset,
  ViewPresetListItem,
} from '../../store/viewPresets/types';
import { PresetAction } from '../MapPresetsMenuItem/MapPresetsMenuItem';
import { emptyMapViewPreset } from '../MapPresetsFormDialog/MapPresetsForm';
import { PresetsApi } from '../../utils/api';

export const MESSAGE_FETCH_PRESET =
  'Preset could not be loaded: Select a different one or try again.';
export const MESSAGE_SAVE_PRESET =
  'Map preset not saved: Something wrong occurred during the process. Try again.';

const MapPresetsDialogConnect: React.FC = () => {
  const dispatch = useDispatch();
  const { api } = useApiContext<PresetsApi>();
  const mapId = useSelector((store: AppStore) =>
    uiSelectors.getDialogMapId(store, 'layerManager'),
  );
  const mapPresets: ViewPresetListItem[] = useSelector(
    (store: PresetModuleStore) => viewPresetSelectors.getViewPresets(store),
  );
  const activeMapPresetId = useSelector((store: AppStore) =>
    mapSelectors.getActiveMapPresetId(store, mapId),
  );

  const currentMapPreset = useSelector((store: AppStore) =>
    mapSelectors.getMapPreset(store, mapId),
  );

  const hasMapPresetChanges = useSelector((store: AppStore) =>
    mapSelectors.getHasMapPresetChanges(store, mapId),
  );

  const loadPresets = React.useCallback(
    (viewPresets) => {
      dispatch(viewPresetActions.loadViewPresets({ viewPresets }));
    },
    [dispatch],
  );
  const setMapPreset = React.useCallback(
    (initialProps) => {
      dispatch(
        mapActions.setMapPreset({
          mapId,
          initialProps,
        }),
      );
    },
    [dispatch, mapId],
  );
  const setActiveMapPresetId = React.useCallback(
    (presetId) => {
      dispatch(
        mapActions.setActiveMapPresetId({
          mapId,
          presetId,
        }),
      );
    },
    [dispatch, mapId],
  );
  const setIsMapPresetLoading = React.useCallback(
    (isLoading) => {
      dispatch(
        mapActions.setIsMapPresetLoading({
          mapId,
          isLoading,
        }),
      );
    },
    [dispatch, mapId],
  );

  const setMapPresetError = React.useCallback(
    (error) => {
      dispatch(
        mapActions.setMapPresetError({
          mapId,
          error,
        }),
      );
    },
    [dispatch, mapId],
  );

  const resetMapPresetChanges = React.useCallback(() => {
    dispatch(
      mapActions.setHasMapPresetChanges({
        mapId,
        hasChanges: false,
      }),
    );
  }, [dispatch, mapId]);

  const {
    isLoading: isListLoading,
    error: listError,
    result: fetchedPresets,
    fetchApiData: fetchNewPresetList,
  } = useApi(api.getViewPresets);

  const onSuccess = async (
    action: PresetAction,
    presetId: string,
  ): Promise<void> => {
    if (action === PresetAction.DELETE && presetId === activeMapPresetId) {
      setActiveMapPresetId(null);
    }
    await fetchNewPresetList({});
    resetMapPresetChanges();
    if (action === PresetAction.SAVE_AS && presetId) {
      setActiveMapPresetId(presetId);
    }
  };

  const fetchViewPreset = async (presetId: string): Promise<void> => {
    try {
      setIsMapPresetLoading(true);
      setMapPresetError(null);
      const { data: fetchedPreset } = await api.getViewPreset(presetId);
      setIsMapPresetLoading(false);
      if (
        (fetchedPreset.initialProps as mapTypes.MapPresetInitialProps)
          ?.mapPreset
      ) {
        setActiveMapPresetId(presetId);
        setMapPreset(fetchedPreset.initialProps);
      }
    } catch (error) {
      setIsMapPresetLoading(false);
      setMapPresetError(MESSAGE_FETCH_PRESET);
    }
  };

  const onClickPreset = (presetId: string): void => {
    if (presetId === activeMapPresetId) {
      return; // preset is already selected
    }
    if (presetId !== null) {
      fetchViewPreset(presetId);
    } else {
      setActiveMapPresetId(presetId);
      setMapPreset(emptyMapViewPreset.initialProps);
      setMapPresetError(null);
    }
  };

  const onSavePreset = async (
    presetId: string,
    presetData: ViewPreset,
  ): Promise<void> => {
    try {
      setIsMapPresetLoading(true);
      setMapPresetError(null);
      await api.saveViewPreset(presetId, presetData);
      setIsMapPresetLoading(false);
      resetMapPresetChanges();
    } catch (error) {
      setIsMapPresetLoading(false);
      setMapPresetError(MESSAGE_SAVE_PRESET);
    }
  };

  React.useEffect(() => {
    loadPresets(fetchedPresets);
  }, [fetchedPresets, loadPresets]);

  return (
    <MapPresetsDialog
      activeMapPresetId={activeMapPresetId}
      mapPresets={mapPresets}
      onClickPreset={onClickPreset}
      onSuccess={onSuccess}
      isLoading={isListLoading}
      hasError={!!listError}
      hasMapPresetChanges={hasMapPresetChanges}
      currentMapPreset={currentMapPreset}
      onSavePreset={onSavePreset}
      fetchNewPresetList={fetchNewPresetList}
    />
  );
};

export default MapPresetsDialogConnect;
