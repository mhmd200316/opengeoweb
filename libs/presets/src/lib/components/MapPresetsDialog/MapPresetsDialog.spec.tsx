/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { mapTypes } from '@opengeoweb/core';
import { fireEvent, render, waitFor } from '@testing-library/react';
import React from 'react';
import { ViewPresetListItem } from '../../store/viewPresets/types';
import { emptyMapViewPreset } from '../MapPresetsFormDialog/MapPresetsForm';
import { getPresetTitle } from '../MapPresetsMenuItem/MapPresetsMenuItem';
import { PresetsThemeProvider } from '../Providers';

import MapPresetsDialog, {
  MapPresetsDialogProps,
  MAPPRESET_ACTIVE_TITLE,
} from './MapPresetsDialog';

describe('components/MapPresetsDialog/MapPresetsDialog', () => {
  it('should render with default props and open the menu ', async () => {
    const props = {
      mapPresets: [],
      onClickPreset: jest.fn(),
    };

    const { queryByTestId, findByText } = render(
      <PresetsThemeProvider>
        <MapPresetsDialog {...props} />
      </PresetsThemeProvider>,
    );
    expect(queryByTestId('mappresets-menu')).toBeFalsy();

    fireEvent.click(queryByTestId('mappresets-menubutton'));
    expect(queryByTestId('mappresets-menu')).toBeTruthy();
    expect(await findByText("You haven't saved any presets yet")).toBeTruthy();
    expect(queryByTestId('info-banner')).toBeTruthy();
    expect(queryByTestId('preset-dialog-spinner')).toBeFalsy();
    expect(queryByTestId('alert-banner')).toBeFalsy();
  });

  it('should show the loading indicator when loading the preset list', async () => {
    const props = {
      mapPresets: [],
      onClickPreset: jest.fn(),
      isLoading: true,
    };

    const { queryByTestId, findByTestId, queryByText } = render(
      <PresetsThemeProvider>
        <MapPresetsDialog {...props} />
      </PresetsThemeProvider>,
    );
    expect(queryByTestId('mappresets-menu')).toBeFalsy();

    fireEvent.click(queryByTestId('mappresets-menubutton'));
    expect(queryByTestId('mappresets-menu')).toBeTruthy();
    expect(await findByTestId('preset-dialog-spinner')).toBeTruthy();
    expect(queryByTestId('info-banner')).toBeFalsy();
    expect(queryByTestId('alert-banner')).toBeFalsy();
    expect(queryByText("You haven't saved any presets yet")).toBeFalsy();
  });

  it('should show an error message when loading the preset list failed and user should be able to try again', async () => {
    const props = {
      mapPresets: [],
      onClickPreset: jest.fn(),
      hasError: true,
      fetchNewPresetList: jest.fn(),
    };

    const { queryByTestId, queryByText } = render(
      <PresetsThemeProvider>
        <MapPresetsDialog {...props} />
      </PresetsThemeProvider>,
    );
    expect(queryByTestId('mappresets-menu')).toBeFalsy();

    fireEvent.click(queryByTestId('mappresets-menubutton'));
    expect(queryByTestId('mappresets-menu')).toBeTruthy();
    expect(queryByText('Map presets not available')).toBeTruthy();
    expect(queryByText('TRY AGAIN')).toBeTruthy();
    expect(queryByTestId('alert-banner')).toBeTruthy();
    expect(queryByTestId('info-banner')).toBeFalsy();
    expect(queryByTestId('preset-dialog-spinner')).toBeFalsy();
    fireEvent.click(queryByText('TRY AGAIN'));
    expect(props.fetchNewPresetList).toHaveBeenCalled();
  });

  it('should select a preset', async () => {
    // test wrapper for state to test button title change
    const MapPresetsDialogWrapper: React.FC<MapPresetsDialogProps> = (
      props: MapPresetsDialogProps,
    ) => {
      const [activeMapPresetId, setActiveMapPresetId] =
        React.useState<string>(undefined);

      return (
        <MapPresetsDialog
          {...props}
          activeMapPresetId={activeMapPresetId}
          onClickPreset={setActiveMapPresetId}
        />
      );
    };
    const props = {
      mapPresets: [
        {
          id: 'test-1',
          scope: 'system',
          title: 'test 1',
          date: '2022-06-01T12:34:27.787192',
        },
        {
          id: 'test-2',
          scope: 'system',
          title: 'test 2',
          date: '2022-06-01T12:34:27.787192',
        },
      ] as ViewPresetListItem[],
      onClickPreset: jest.fn(),
    };

    const { queryByTestId, queryAllByTestId } = render(
      <PresetsThemeProvider>
        <MapPresetsDialogWrapper {...props} />
      </PresetsThemeProvider>,
    );
    const menuButton = queryByTestId('mappresets-menubutton');
    expect(menuButton.innerHTML).toContain(MAPPRESET_ACTIVE_TITLE);
    expect(queryByTestId('mappresets-menu')).toBeFalsy();
    expect(queryAllByTestId('mappreset-menuitem')).toHaveLength(0);

    // open menu
    fireEvent.click(menuButton);
    expect(queryAllByTestId('mappreset-menuitem')).toHaveLength(
      props.mapPresets.length + 1,
    );

    // click first system mapPreset
    fireEvent.click(queryAllByTestId('mappreset-menuitem')[1]);
    await waitFor(() => {
      expect(queryByTestId('mappresets-menu')).toBeFalsy();
    });

    expect(queryByTestId('mappresets-menubutton').innerHTML).toContain(
      props.mapPresets[0].title,
    );

    // open menu again, new preset option should still be there
    fireEvent.click(menuButton);
    expect(queryAllByTestId('mappreset-menuitem')).toHaveLength(
      props.mapPresets.length + 1,
    );
  });

  it('should be able to select a new map preset', async () => {
    const props = {
      mapPresets: [
        {
          id: 'test-1',
          scope: 'system',
          title: 'test 1',
          date: '2022-06-01T12:34:27.787192',
        },
        {
          id: 'test-2',
          scope: 'system',
          title: 'test 2',
          date: '2022-06-01T12:34:27.787192',
        },
      ] as ViewPresetListItem[],
      onClickPreset: jest.fn(),
    };

    const { queryByTestId, queryAllByTestId } = render(
      <PresetsThemeProvider>
        <MapPresetsDialog {...props} />
      </PresetsThemeProvider>,
    );
    const menuButton = queryByTestId('mappresets-menubutton');
    expect(menuButton.innerHTML).toContain(MAPPRESET_ACTIVE_TITLE);
    expect(queryByTestId('mappresets-menu')).toBeFalsy();
    expect(queryAllByTestId('mappreset-menuitem')).toHaveLength(0);

    // open menu
    fireEvent.click(menuButton);
    expect(queryAllByTestId('mappreset-menuitem')).toHaveLength(
      props.mapPresets.length + 1,
    );

    // click new mapPreset
    fireEvent.click(queryAllByTestId('mappreset-menuitem')[0]);
    expect(props.onClickPreset).toHaveBeenCalledWith(null);
    await waitFor(() => {
      expect(queryByTestId('mappresets-menu')).toBeFalsy();
    });
  });

  it('should trigger correct callback when selecting a preset', async () => {
    const props = {
      mapPresets: [
        {
          id: 'test-1',
          scope: 'system',
          title: 'test 1',
          date: '2022-06-01T12:34:27.787192',
        },
        {
          id: 'test-2',
          scope: 'system',
          title: 'test 2',
          date: '2022-06-01T12:34:27.787192',
        },
      ] as ViewPresetListItem[],
      onClickPreset: jest.fn(),
    };

    const { queryByTestId, queryAllByTestId } = render(
      <PresetsThemeProvider>
        <MapPresetsDialog {...props} />
      </PresetsThemeProvider>,
    );
    const menuButton = queryByTestId('mappresets-menubutton');
    expect(menuButton.innerHTML).toContain(MAPPRESET_ACTIVE_TITLE);
    expect(queryByTestId('mappresets-menu')).toBeFalsy();
    expect(queryAllByTestId('mappreset-menuitem')).toHaveLength(0);

    // open menu
    fireEvent.click(menuButton);
    expect(queryAllByTestId('mappreset-menuitem')).toHaveLength(
      props.mapPresets.length + 1,
    );

    // click first system mapPreset
    fireEvent.click(queryAllByTestId('mappreset-menuitem')[1]);
    expect(props.onClickPreset).toHaveBeenCalledWith(props.mapPresets[0].id);
    await waitFor(() => {
      expect(queryByTestId('mappresets-menu')).toBeFalsy();
    });
  });

  it('should trigger correct callback when saving a preset', async () => {
    const props = {
      mapPresets: [
        {
          id: 'test-1',
          scope: 'user',
          title: 'test 1',
          date: '2022-06-01T12:34:27.787192',
        },
        {
          id: 'test-2',
          scope: 'user',
          title: 'test 2',
          date: '2022-06-01T12:34:27.787192',
        },
      ] as ViewPresetListItem[],
      onClickPreset: jest.fn(),
      onSavePreset: jest.fn(),
      activeMapPresetId: 'test-1',
      currentMapPreset: {
        layers: [
          {
            id: 'layerid_10',
            layerType: 'mapLayer',
            name: '10M/ta',
            service:
              'https://geoservices.knmi.nl/adagucserver?dataset=OBS&service=WMS&',
          },
          {
            enabled: true,
            id: 'layerid_20',
            layerType: 'mapLayer',
            name: 'air_pressure_at_sea_level',
            service: 'https://geoservices.knmi.nl/wms?DATASET=HARM_N25&',
          },
        ] as mapTypes.Layer[],
        proj: {
          bbox: {
            bottom: 6408480.4514,
            left: 58703.6377,
            right: 3967387.5161,
            top: 11520588.9031,
          },
          srs: 'EPSG:3857',
        },
        activeLayerId: 'layerid_10',
      },
    };

    const { queryByTestId, queryAllByTestId, getAllByTestId, getByText } =
      render(
        <PresetsThemeProvider>
          <MapPresetsDialog {...props} />
        </PresetsThemeProvider>,
      );
    const menuButton = queryByTestId('mappresets-menubutton');

    // open menu
    fireEvent.click(menuButton);
    expect(queryAllByTestId('mappreset-menuitem')).toHaveLength(
      props.mapPresets.length + 1,
    );

    const presetOptions = getAllByTestId('toggleMenuButton');

    fireEvent.click(presetOptions[0]);
    fireEvent.click(getByText('Save'), { exact: true });
    expect(props.onSavePreset).toHaveBeenCalledWith(props.mapPresets[0].id, {
      ...emptyMapViewPreset,
      title: props.mapPresets[0].title,
      initialProps: {
        mapPreset: props.currentMapPreset,
      },
    });

    await waitFor(() => {
      expect(queryByTestId('mappresets-menu')).toBeFalsy();
    });
  });

  it('should not fail when activeMapPresetId does not exist in the list', () => {
    const props = {
      mapPresets: [],
      activeMapPresetId: 'test',
      onClickPreset: jest.fn(),
    };

    const { queryByTestId } = render(
      <PresetsThemeProvider>
        <MapPresetsDialog {...props} />
      </PresetsThemeProvider>,
    );
    expect(queryByTestId('mappresets-menubutton')).toBeTruthy();
  });

  it('should show correct title for new preset', async () => {
    const props = {
      mapPresets: [],
      onClickPreset: jest.fn(),
      id: 'test-id',
    };

    const { findByText } = render(
      <PresetsThemeProvider>
        <MapPresetsDialog {...props} />
      </PresetsThemeProvider>,
    );

    expect(await findByText(MAPPRESET_ACTIVE_TITLE)).toBeTruthy();
  });

  it('should show correct title for new preset with changes', async () => {
    const props = {
      mapPresets: [],
      onClickPreset: jest.fn(),
      hasMapPresetChanges: true,
      id: 'test-id',
    };

    const { findByText } = render(
      <PresetsThemeProvider>
        <MapPresetsDialog {...props} />
      </PresetsThemeProvider>,
    );

    expect(
      await findByText(
        getPresetTitle(
          MAPPRESET_ACTIVE_TITLE,
          props.hasMapPresetChanges,
          true,
          true,
        ),
      ),
    ).toBeTruthy();
  });
});
