/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { Card } from '@mui/material';

import { darkTheme } from '@opengeoweb/theme';
import React from 'react';
import { ViewPresetListItem } from '../../store/viewPresets/types';
import { PresetsThemeProvider } from '../Providers';

import MapPresetsDialog from './MapPresetsDialog';

export default { title: 'components/MapPresetsDialog' };

const mapPresets: ViewPresetListItem[] = [
  {
    id: 'preset-1',
    title: 'Observation',
    date: '2022-06-01T12:34:27.787192',
    scope: 'system',
  },
  {
    id: 'preset-2',
    title: 'Observation & elevation',
    date: '2022-06-01T12:34:27.787192',
    scope: 'system',
  },
  {
    id: 'preset-3',
    title: 'Custom preset',
    date: '2022-06-01T12:34:27.787192',
    scope: 'user',
  },
];

const MapPresetsDialogDemo: React.FC = () => {
  const [activeMapPresetId, setActiveMapPresetId] =
    React.useState<string>(null);
  return (
    <Card sx={{ padding: 1, width: 300, height: 300 }}>
      <MapPresetsDialog
        activeMapPresetId={activeMapPresetId}
        mapPresets={mapPresets}
        onClickPreset={setActiveMapPresetId}
        isDefaultOpen
      />
    </Card>
  );
};

export const MapPresetsDialogLightTheme = (): React.ReactElement => (
  <PresetsThemeProvider>
    <MapPresetsDialogDemo />
  </PresetsThemeProvider>
);

MapPresetsDialogLightTheme.storyName =
  'MapPresetsDialog Light Theme (takeSnapshot)';

export const MapPresetsDialogDarkTheme = (): React.ReactElement => (
  <PresetsThemeProvider theme={darkTheme}>
    <MapPresetsDialogDemo />
  </PresetsThemeProvider>
);

MapPresetsDialogDarkTheme.storyName =
  'MapPresetsDialog Dark Theme (takeSnapshot)';

export const DialogLoading = (): React.ReactElement => {
  const [activeMapPresetId, setActiveMapPresetId] =
    React.useState<string>(null);
  return (
    <PresetsThemeProvider>
      <Card sx={{ padding: 1, width: 300, height: 300 }}>
        <MapPresetsDialog
          activeMapPresetId={activeMapPresetId}
          mapPresets={[]}
          onClickPreset={setActiveMapPresetId}
          isDefaultOpen
          isLoading
        />
      </Card>
    </PresetsThemeProvider>
  );
};

const NoPresets = (): React.ReactElement => {
  const [activeMapPresetId, setActiveMapPresetId] =
    React.useState<string>(null);
  return (
    <Card sx={{ padding: 1, width: 350, height: 300 }}>
      <MapPresetsDialog
        activeMapPresetId={activeMapPresetId}
        mapPresets={[]}
        onClickPreset={setActiveMapPresetId}
        isDefaultOpen
      />
    </Card>
  );
};

export const NoPresetsDarkTheme = (): React.ReactElement => (
  <PresetsThemeProvider theme={darkTheme}>
    <NoPresets />
  </PresetsThemeProvider>
);

NoPresetsDarkTheme.storyName = 'No Presets Dark Theme (takeSnapshot)';

export const NoPresetsLightTheme = (): React.ReactElement => (
  <PresetsThemeProvider>
    <NoPresets />
  </PresetsThemeProvider>
);

NoPresetsLightTheme.storyName = 'No Presets Light Theme (takeSnapshot)';

const DialogError = (): React.ReactElement => {
  const [activeMapPresetId, setActiveMapPresetId] =
    React.useState<string>(null);
  return (
    <Card sx={{ padding: 1, width: 350, height: 300 }}>
      <MapPresetsDialog
        activeMapPresetId={activeMapPresetId}
        mapPresets={[]}
        onClickPreset={setActiveMapPresetId}
        isDefaultOpen
        hasError
        fetchNewPresetList={async (): Promise<void> =>
          // eslint-disable-next-line no-console
          console.log('fetching new preset list')
        }
      />
    </Card>
  );
};

export const ErrorDarkTheme = (): React.ReactElement => (
  <PresetsThemeProvider theme={darkTheme}>
    <DialogError />
  </PresetsThemeProvider>
);

ErrorDarkTheme.storyName = 'Error Dark Theme (takeSnapshot)';

export const ErrorLightTheme = (): React.ReactElement => (
  <PresetsThemeProvider>
    <DialogError />
  </PresetsThemeProvider>
);

ErrorLightTheme.storyName = 'Error Light Theme (takeSnapshot)';
