/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { ListSubheader, Menu, Typography, Box, Tooltip } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';
import { mapTypes } from '@opengeoweb/core';
import { AlertBanner, CustomToggleButton } from '@opengeoweb/shared';
import React from 'react';

import { ViewPreset, ViewPresetListItem } from '../../store/viewPresets/types';
import { MapPresetsFormDialog } from '../MapPresetsFormDialog';
import {
  emptyMapViewPreset,
  MapPresetFormValues,
} from '../MapPresetsFormDialog/MapPresetsForm';
import MapPresetsMenuItem, {
  getPresetTitle,
  PresetAction,
} from '../MapPresetsMenuItem/MapPresetsMenuItem';

export const MAPPRESET_ACTIVE_TITLE = 'New map preset';

type DialogOptions = {
  title: string;
  action: PresetAction;
  presetId: string;
  formValues: MapPresetFormValues;
};

export interface MapPresetsDialogProps {
  activeMapPresetId?: string;
  mapPresets: ViewPresetListItem[];
  onClickPreset: (mapPresetId: string) => void;
  isDefaultOpen?: boolean;
  id?: string;
  onSuccess?: (action: PresetAction, presetId: string) => void;
  isLoading?: boolean;
  hasError?: boolean;
  hasMapPresetChanges?: boolean;
  currentMapPreset?: mapTypes.MapPreset;
  onSavePreset?: (presetId: string, mapPreset: ViewPreset) => void;
  fetchNewPresetList?: (params: unknown) => Promise<void>;
}

const MapPresetsDialog: React.FC<MapPresetsDialogProps> = ({
  activeMapPresetId = null,
  mapPresets = [],
  onClickPreset = (): void => {},
  isDefaultOpen = false,
  id = 'mappresets-menubutton',
  onSuccess = (): void => {},
  isLoading = false,
  hasError = false,
  hasMapPresetChanges = false,
  currentMapPreset = null,
  onSavePreset = (): void => {},
  fetchNewPresetList = async (): Promise<void> => {},
}: MapPresetsDialogProps) => {
  const [dialogOptions, setDialogOptions] = React.useState<DialogOptions>(null);
  const [anchorEl, setAnchorEl] = React.useState<HTMLInputElement>(null);
  const isOpen = Boolean(anchorEl);

  const onToggleMenu = (event: React.MouseEvent<HTMLInputElement>): void => {
    setAnchorEl(event.currentTarget);
  };

  const onCloseMenu = (): void => {
    setAnchorEl(null);
  };

  const onClickPresetItem = (
    presetId: string,
    presetAction: PresetAction,
  ): void => {
    const clickedPreset = mapPresets.find((preset) => preset.id === presetId);
    const presetTitle = clickedPreset?.title;
    switch (presetAction) {
      case PresetAction.OPEN: {
        onClickPreset(presetId);
        onCloseMenu();
        break;
      }
      case PresetAction.SAVE: {
        onSavePreset(presetId, {
          ...emptyMapViewPreset,
          title: presetTitle,
          initialProps: {
            mapPreset: currentMapPreset,
          },
        });
        onCloseMenu();
        break;
      }
      case PresetAction.SAVE_AS: {
        setDialogOptions({
          title: 'Save map preset as',
          action: presetAction,
          presetId,
          formValues: {
            title: presetTitle,
            initialProps: {
              mapPreset: currentMapPreset,
            },
          },
        });
        onCloseMenu();
        break;
      }
      case PresetAction.DELETE: {
        setDialogOptions({
          title: 'Delete map preset',
          action: presetAction,
          presetId,
          formValues: { title: presetTitle },
        });
        break;
      }
      default: {
        break;
      }
    }
  };

  const onClose = (): void => {
    setDialogOptions(null);
  };

  const activePresetTitle = activeMapPresetId
    ? mapPresets.find((preset) => preset.id === activeMapPresetId)?.title
    : MAPPRESET_ACTIVE_TITLE;

  React.useEffect(() => {
    if (isDefaultOpen) {
      setAnchorEl(document.getElementById(id) as HTMLInputElement);
    }
  }, [isDefaultOpen, id]);

  const isNewActive = activeMapPresetId === null;
  const presetTitle = getPresetTitle(
    activePresetTitle,
    hasMapPresetChanges,
    true,
    isNewActive,
  );

  return (
    <>
      <CustomToggleButton
        sx={{
          '&.MuiButtonBase-root': {
            height: '32px',
            fontSize: 11,
            textTransform: 'none',
            whiteSpace: 'nowrap',
            maxWidth: '100%',
            minWidth: '100%',
            ...((!activeMapPresetId || hasMapPresetChanges) && {
              fontStyle: 'italic',
            }),
          },
        }}
        onClick={onToggleMenu}
        variant="tool"
        selected={isOpen}
        data-testid={id}
        id={id}
        disableFocusRipple
      >
        <Tooltip
          title="Map presets"
          placement="bottom"
          sx={{ pointerEvents: 'none' }}
        >
          <Box
            sx={{
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              width: '100%',
              textAlign: 'left',
            }}
          >
            {presetTitle}
          </Box>
        </Tooltip>
      </CustomToggleButton>
      <Menu
        anchorEl={anchorEl}
        open={isOpen}
        onClose={onCloseMenu}
        BackdropProps={{
          style: {
            opacity: 0,
            transition: 'none',
          },
        }}
        sx={{
          '& .MuiMenu-list': {
            minWidth: '290px',
            padding: 0,
          },
        }}
        data-testid="mappresets-menu"
      >
        <ListSubheader
          sx={{
            padding: '12px',
          }}
        >
          <Typography
            sx={{
              letterSpacing: '0.4px',
              fontSize: 12,
            }}
          >
            Map presets
          </Typography>
        </ListSubheader>

        <MapPresetsMenuItem
          id={null}
          title={MAPPRESET_ACTIVE_TITLE}
          onClickPreset={onClickPresetItem}
          isActive={isNewActive}
          scope="user"
          isNew
          hasChanges={hasMapPresetChanges}
        />

        {isLoading && (
          <Box
            data-testid="preset-dialog-spinner"
            sx={{
              height: '48px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              margin: 2,
            }}
          >
            <CircularProgress size={48} color="secondary" />
          </Box>
        )}

        {!isLoading && hasError && (
          <Box
            sx={{
              margin: 2,
            }}
          >
            <AlertBanner
              title="Map presets not available"
              actionButtonProps={{
                title: 'Try again',
                onClick: fetchNewPresetList,
              }}
            />
          </Box>
        )}

        {!isLoading && !hasError && mapPresets.length
          ? mapPresets.map((preset) => (
              <MapPresetsMenuItem
                key={preset.id}
                id={preset.id}
                title={preset.title}
                scope={preset.scope}
                onClickPreset={onClickPresetItem}
                isActive={activeMapPresetId === preset.id}
                hasChanges={hasMapPresetChanges}
              />
            ))
          : null}

        {!isLoading && !hasError && !mapPresets.length && (
          <Box
            sx={{
              margin: 2,
            }}
          >
            <AlertBanner
              severity="info"
              dataTestId="info-banner"
              title="You haven't saved any presets yet"
            />
          </Box>
        )}
      </Menu>
      <MapPresetsFormDialog
        isOpen={Boolean(dialogOptions)}
        onClose={onClose}
        onSuccess={onSuccess}
        {...dialogOptions}
      />
    </>
  );
};
export default MapPresetsDialog;
