/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import { InitialMapProps, mapActions, mapTypes } from '@opengeoweb/core';
import MapPresetsDialogConnect, {
  MESSAGE_FETCH_PRESET,
  MESSAGE_SAVE_PRESET,
} from './MapPresetsDialogConnect';

import { PresetsThemeStoreProvider } from '../Providers';
import { PresetsApi } from '../../utils/api';
import {
  PresetModuleStore,
  ViewPreset,
  ViewPresetListItem,
} from '../../store/viewPresets/types';
import { emptyMapViewPreset } from '../MapPresetsFormDialog/MapPresetsForm';
import { viewPresetActions } from '../../store/viewPresets/reducer';

describe('src/components/MapPresetsDialog/MapPresetsDialogConnect', () => {
  it('should fetch presets on mount', async () => {
    const mockStore = configureStore();
    const mockState: PresetModuleStore = {
      viewPresets: {
        entities: {},
        ids: [],
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'system',
        title: 'Observation',
        date: '2022-06-01T12:34:27.787192',
      },
      {
        id: 'preset-2',
        scope: 'system',
        title: 'Observation & elevation',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const createApi = (): PresetsApi => {
      return {
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
      } as PresetsApi;
    };

    const { getByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('mappresets-menubutton')).toBeTruthy();

    // load presets
    const expectedAction = viewPresetActions.loadViewPresets({
      viewPresets: testViewPresets,
    });
    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedAction);
    });
  });

  it('should show error when fetching of presets returns error and user should be able to try again', async () => {
    const mockStore = configureStore();
    const mockState: PresetModuleStore = {
      viewPresets: {
        entities: {},
        ids: [],
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const createApi = (): PresetsApi => {
      return {
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((_, reject) => reject(new Error('testerror')));
        },
      } as PresetsApi;
    };

    const { getByTestId, findByText, getByText, queryByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    fireEvent.click(getByTestId('mappresets-menubutton'));

    await waitFor(() => expect(getByTestId('alert-banner')).toBeTruthy());
    expect(await findByText('Map presets not available')).toBeTruthy();
    fireEvent.click(getByText('TRY AGAIN'));
    await waitFor(() => {
      expect(queryByTestId('preset-dialog-spinner')).toBeTruthy();
    });
    await waitFor(() => {
      expect(queryByTestId('preset-dialog-spinner')).toBeFalsy();
    });
    expect(await findByText('Map presets not available')).toBeTruthy();
  });

  it('should set new map preset when selecting preset ', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'system',
        title: 'Observation',
        date: '2022-06-01T12:34:27.787192',
      },
      {
        id: 'preset-2',
        scope: 'system',
        title: 'Observation & elevation',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const initialProps = {
      mapPreset: {
        layers: [
          {
            id: 'layerid_10',
            layerType: 'mapLayer',
            name: '10M/ta',
            service:
              'https://geoservices.knmi.nl/adagucserver?dataset=OBS&service=WMS&',
          },
          {
            enabled: true,
            id: 'layerid_20',
            layerType: 'mapLayer',
            name: 'air_pressure_at_sea_level',
            service: 'https://geoservices.knmi.nl/wms?DATASET=HARM_N25&',
          },
        ] as mapTypes.Layer[],
        proj: {
          bbox: {
            bottom: 6408480.4514,
            left: 58703.6377,
            right: 3967387.5161,
            top: 11520588.9031,
          },
          srs: 'EPSG:3857',
        },
        showTimeSlider: true,
      },
      syncGroupsIds: ['Area_radarTemp', 'Time_radarTemp'],
    };

    const testViewPreset: ViewPreset = {
      componentType: 'Map',
      id: 'Temperature',
      keywords: 'WMS,Temperature',
      title: 'Temperature',
      initialProps,
    };

    const createApi = (): PresetsApi => {
      return {
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: (): Promise<{ data: ViewPreset }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPreset,
            }),
          );
        },
        saveViewPreset: jest.fn(),
        saveViewPresetAs: (): Promise<string> => {
          return new Promise((resolve) => resolve('test-id'));
        },
        deleteViewPreset: (): Promise<void> => {
          return new Promise((resolve) => resolve());
        },
      };
    };

    const mockState = {
      viewPresets: {
        entities: {
          'preset-1': testViewPresets[0],
          'preset-2': testViewPresets[1],
        },
        ids: ['preset-1', 'preset-2'],
      },
      ui: {
        order: ['layerManager'],
        dialogs: {
          layerManager: {
            activeMapId: mapId,
            isOpen: true,
            type: 'layerManager',
            source: 'app',
          },
        },
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, getAllByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    const presetButton = getByTestId('mappresets-menubutton');

    // button should be present
    expect(presetButton).toBeTruthy();

    presetButton.click();

    await waitFor(() => {
      expect(getByTestId('mappresets-menu')).toBeTruthy();
    });

    await waitFor(() => {
      expect(getAllByTestId('mappreset-menuitem')).toHaveLength(3);
    });

    const presets = getAllByTestId('mappreset-menuitem');

    presets[1].click();

    // set layers after fetching preset
    const expectedActions = [
      viewPresetActions.loadViewPresets({
        viewPresets: null,
      }),
      viewPresetActions.loadViewPresets({
        viewPresets: testViewPresets,
      }),
      mapActions.setIsMapPresetLoading({
        mapId,
        isLoading: true,
      }),
      mapActions.setMapPresetError({
        mapId,
        error: null,
      }),
      mapActions.setIsMapPresetLoading({
        mapId,
        isLoading: false,
      }),
      mapActions.setActiveMapPresetId({
        mapId,
        presetId: 'preset-1',
      }),
      mapActions.setMapPreset({
        mapId,
        initialProps,
      }),
    ];

    await waitFor(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should not fire any actions when selecting preset that is already selected', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'system',
        title: 'Observation',
        date: '2022-06-01T12:34:27.787192',
      },
      {
        id: 'preset-2',
        scope: 'system',
        title: 'Observation & elevation',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const initialProps = {
      mapPreset: {
        layers: [
          {
            id: 'layerid_10',
            layerType: 'mapLayer',
            name: '10M/ta',
            service:
              'https://geoservices.knmi.nl/adagucserver?dataset=OBS&service=WMS&',
          },
          {
            enabled: true,
            id: 'layerid_20',
            layerType: 'mapLayer',
            name: 'air_pressure_at_sea_level',
            service: 'https://geoservices.knmi.nl/wms?DATASET=HARM_N25&',
          },
        ] as mapTypes.Layer[],
        proj: {
          bbox: {
            bottom: 6408480.4514,
            left: 58703.6377,
            right: 3967387.5161,
            top: 11520588.9031,
          },
          srs: 'EPSG:3857',
        },
        showTimeSlider: true,
      },
      syncGroupsIds: ['Area_radarTemp', 'Time_radarTemp'],
    };

    const testViewPreset: ViewPreset = {
      componentType: 'Map',
      id: 'Temperature',
      keywords: 'WMS,Temperature',
      title: 'Temperature',
      initialProps,
    };

    const createApi = (): PresetsApi => {
      return {
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: (): Promise<{ data: ViewPreset }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPreset,
            }),
          );
        },
        saveViewPreset: jest.fn(),
        saveViewPresetAs: jest.fn(),
        deleteViewPreset: jest.fn(),
      };
    };

    const mockState = {
      viewPresets: {
        entities: {
          'preset-1': testViewPresets[0],
          'preset-2': testViewPresets[1],
        },
        ids: ['preset-1', 'preset-2'],
      },
      ui: {
        order: ['layerManager'],
        dialogs: {
          layerManager: {
            activeMapId: mapId,
            isOpen: true,
            type: 'layerManager',
            source: 'app',
          },
        },
      },
      webmap: {
        byId: {
          [mapId]: {
            activeMapPresetId: 'preset-1',
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
          },
        },
      },
      allIds: [mapId],
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, getAllByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    const menu = getByTestId('mappresets-menubutton');
    fireEvent.click(menu);

    await waitFor(() => {
      expect(getByTestId('mappresets-menu')).toBeTruthy();
    });

    await waitFor(() => {
      expect(getAllByTestId('mappreset-menuitem')).toHaveLength(3);
    });

    const presets = getAllByTestId('mappreset-menuitem');

    fireEvent.click(presets[1]);

    const expectedActions = [
      viewPresetActions.loadViewPresets({
        viewPresets: null,
      }),
      viewPresetActions.loadViewPresets({
        viewPresets: testViewPresets,
      }),
    ];

    await waitFor(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should set error when selecting preset returns an error ', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'system',
        title: 'Observation',
        date: '2022-06-01T12:34:27.787192',
      },
      {
        id: 'preset-2',
        scope: 'system',
        title: 'Observation & elevation',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const createApi = (): PresetsApi => {
      return {
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: (): Promise<{ data: ViewPreset }> => {
          return new Promise((_, reject) => reject(new Error('error')));
        },
        saveViewPreset: jest.fn(),
        saveViewPresetAs: (): Promise<string> => {
          return new Promise((resolve) => resolve('test-id'));
        },
        deleteViewPreset: (): Promise<void> => {
          return new Promise((resolve) => resolve());
        },
      };
    };

    const mockState = {
      viewPresets: {
        entities: {
          'preset-1': testViewPresets[0],
          'preset-2': testViewPresets[1],
        },
        ids: ['preset-1', 'preset-2'],
      },
      ui: {
        order: ['layerManager'],
        dialogs: {
          layerManager: {
            activeMapId: mapId,
            isOpen: true,
            type: 'layerManager',
            source: 'app',
          },
        },
      },
      webmap: {
        byId: {
          [mapId]: {
            activeMapPresetId: null,
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
          },
        },
      },
      allIds: [mapId],
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, getAllByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    const presetButton = getByTestId('mappresets-menubutton');

    // button should be present
    expect(presetButton).toBeTruthy();

    presetButton.click();

    await waitFor(() => {
      expect(getByTestId('mappresets-menu')).toBeTruthy();
    });

    await waitFor(() => {
      expect(getAllByTestId('mappreset-menuitem')).toHaveLength(3);
    });

    const presets = getAllByTestId('mappreset-menuitem');

    presets[1].click();

    const expectedActions = [
      viewPresetActions.loadViewPresets({
        viewPresets: null,
      }),
      viewPresetActions.loadViewPresets({
        viewPresets: testViewPresets,
      }),
      mapActions.setIsMapPresetLoading({
        mapId,
        isLoading: true,
      }),
      mapActions.setMapPresetError({
        mapId,
        error: null,
      }),
      mapActions.setIsMapPresetLoading({
        mapId,
        isLoading: false,
      }),
      mapActions.setMapPresetError({
        mapId,
        error: MESSAGE_FETCH_PRESET,
      }),
    ];

    await waitFor(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should set error when saving preset returns an error ', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'user',
        title: 'Observation',
        date: '2022-06-01T12:34:27.787192',
      },
      {
        id: 'preset-2',
        scope: 'system',
        title: 'Observation & elevation',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const createApi = (): PresetsApi => {
      return {
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: (): Promise<{ data: ViewPreset }> => {
          return new Promise((resolve) =>
            resolve({
              data: emptyMapViewPreset,
            }),
          );
        },
        saveViewPreset: (): Promise<void> => {
          return new Promise((_, reject) => reject(new Error('error')));
        },
        saveViewPresetAs: jest.fn(),
        deleteViewPreset: jest.fn(),
      };
    };

    const mockState = {
      viewPresets: {
        entities: {
          'preset-1': testViewPresets[0],
          'preset-2': testViewPresets[1],
        },
        ids: ['preset-1', 'preset-2'],
      },
      ui: {
        order: ['layerManager'],
        dialogs: {
          layerManager: {
            activeMapId: mapId,
            isOpen: true,
            type: 'layerManager',
            source: 'app',
          },
        },
      },
      webmap: {
        byId: {
          [mapId]: {
            activeMapPresetId: 'preset-1',
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
          },
        },
      },
      allIds: [mapId],
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, getAllByTestId, getByText } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    const presetButton = getByTestId('mappresets-menubutton');

    // button should be present
    expect(presetButton).toBeTruthy();

    presetButton.click();

    await waitFor(() => {
      expect(getByTestId('mappresets-menu')).toBeTruthy();
    });

    await waitFor(() => {
      expect(getAllByTestId('mappreset-menuitem')).toHaveLength(3);
    });

    const presetOptions = getAllByTestId('toggleMenuButton');

    fireEvent.click(presetOptions[0]);
    fireEvent.click(getByText('Save'), { exact: true });

    const expectedActions = [
      viewPresetActions.loadViewPresets({
        viewPresets: null,
      }),
      viewPresetActions.loadViewPresets({
        viewPresets: testViewPresets,
      }),
      mapActions.setIsMapPresetLoading({
        mapId,
        isLoading: true,
      }),
      mapActions.setMapPresetError({
        mapId,
        error: null,
      }),
      mapActions.setIsMapPresetLoading({
        mapId,
        isLoading: false,
      }),
      mapActions.setMapPresetError({
        mapId,
        error: MESSAGE_SAVE_PRESET,
      }),
    ];

    await waitFor(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should show spinner while loading the preset list and remove spinner when done', async () => {
    jest.useFakeTimers();

    const mockStore = configureStore();
    const mockState: PresetModuleStore = {
      viewPresets: {
        entities: {},
        ids: [],
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn();

    const createApi = (): PresetsApi =>
      ({
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> =>
          new Promise((resolve) =>
            setTimeout(() => {
              resolve({
                data: [],
              });
            }, 3000),
          ),
      } as PresetsApi);

    const { getByTestId, findByTestId, findByText, queryByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    fireEvent.click(getByTestId('mappresets-menubutton'));

    expect(await findByTestId('preset-dialog-spinner')).toBeTruthy();
    jest.runOnlyPendingTimers();
    await waitFor(() =>
      expect(queryByTestId('preset-dialog-spinner')).toBeFalsy(),
    );
    expect(await findByText("You haven't saved any presets yet")).toBeTruthy();

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should save an existing preset when choosing Save', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'user',
        title: 'Observation',
        date: '2022-06-01T12:34:27.787192',
      },
      {
        id: 'preset-2',
        scope: 'user',
        title: 'Observation & elevation',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const testViewPreset: ViewPreset = {
      ...emptyMapViewPreset,
      initialProps: {
        mapPreset: {
          layers: [
            {
              id: 'layerid_10',
              layerType: 'mapLayer',
              name: '10M/ta',
              service:
                'https://geoservices.knmi.nl/adagucserver?dataset=OBS&service=WMS&',
            },
            {
              enabled: true,
              id: 'layerid_20',
              layerType: 'mapLayer',
              name: 'air_pressure_at_sea_level',
              service: 'https://geoservices.knmi.nl/wms?DATASET=HARM_N25&',
            },
          ] as mapTypes.Layer[],
          proj: {
            bbox: {
              bottom: 6408480.4514,
              left: 58703.6377,
              right: 3967387.5161,
              top: 11520588.9031,
            },
            srs: 'EPSG:3857',
          },
          activeLayerId: 'layerid_10',
          animationPayload: {
            interval: 5,
            speed: 4,
          },
          displayMapPin: true,
          shouldAnimate: false,
          shouldAutoUpdate: true,
          shouldShowZoomControls: true,
          showTimeSlider: true,
        },
      },
      title: 'Observation',
    };

    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: (): Promise<{ data: ViewPreset }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPreset,
            }),
          );
        },
        saveViewPresetAs: jest.fn(),
        saveViewPreset: mockSave,
        deleteViewPreset: jest.fn(),
      };
    };

    const mockState = {
      viewPresets: {
        entities: {
          'preset-1': testViewPresets[0],
          'preset-2': testViewPresets[1],
        },
        ids: ['preset-1', 'preset-2'],
      },
      ui: {
        order: ['layerManager'],
        dialogs: {
          layerManager: {
            activeMapId: mapId,
            isOpen: true,
            type: 'layerManager',
            source: 'app',
          },
        },
      },
      webmap: {
        byId: {
          [mapId]: {
            activeMapPresetId: 'preset-1',
            mapLayers: ['layer_10', 'layer_20'],
            activeLayerId: 'layerid_10',
            baseLayers: [],
            overLayers: [],
            bbox: (testViewPreset.initialProps as InitialMapProps).mapPreset
              .proj.bbox,
            srs: (testViewPreset.initialProps as InitialMapProps).mapPreset.proj
              .srs,
            animationDelay: 250,
            timeStep: 5,
            displayMapPin: true,
            isAnimating: false,
            isAutoUpdating: true,
            shouldShowZoomControls: true,
            isTimeSliderVisible: true,
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          layer_10: {
            ...(testViewPreset.initialProps as InitialMapProps).mapPreset
              .layers[0],
          },
          layer_20: {
            ...(testViewPreset.initialProps as InitialMapProps).mapPreset
              .layers[1],
          },
        },
        allIds: ['layer_10', 'layer_20'],
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, getAllByTestId, getByText } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    fireEvent.click(getByTestId('mappresets-menubutton'));

    await waitFor(() => {
      expect(getByTestId('mappresets-menu')).toBeTruthy();
      expect(getAllByTestId('mappreset-menuitem')).toHaveLength(3);
    });

    const presetOptions = getAllByTestId('toggleMenuButton');

    fireEvent.click(presetOptions[0]);
    fireEvent.click(getByText('Save'), { exact: true });

    await waitFor(() => {
      expect(mockSave).toHaveBeenCalled();
    });
    expect(mockSave).toHaveBeenCalledWith(
      testViewPresets[0].id,
      testViewPreset,
    );

    const expectedActions = [
      viewPresetActions.loadViewPresets({
        viewPresets: null,
      }),
      viewPresetActions.loadViewPresets({
        viewPresets: testViewPresets,
      }),
      mapActions.setIsMapPresetLoading({
        mapId,
        isLoading: true,
      }),
      mapActions.setMapPresetError({
        mapId,
        error: null,
      }),
      mapActions.setIsMapPresetLoading({
        mapId,
        isLoading: false,
      }),
      mapActions.setHasMapPresetChanges({
        mapId,
        hasChanges: false,
      }),
    ];

    await waitFor(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should save an existing preset as a new preset when choosing Save As', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'system',
        title: 'Observation',
        date: '2022-06-01T12:34:27.787192',
      },
      {
        id: 'preset-2',
        scope: 'system',
        title: 'Observation & elevation',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const testViewPreset: ViewPreset = {
      ...emptyMapViewPreset,
      initialProps: {
        mapPreset: {
          layers: [
            {
              id: 'layerid_10',
              layerType: 'mapLayer',
              name: '10M/ta',
              service:
                'https://geoservices.knmi.nl/adagucserver?dataset=OBS&service=WMS&',
            },
            {
              enabled: true,
              id: 'layerid_20',
              layerType: 'mapLayer',
              name: 'air_pressure_at_sea_level',
              service: 'https://geoservices.knmi.nl/wms?DATASET=HARM_N25&',
            },
          ] as mapTypes.Layer[],
          proj: {
            bbox: {
              bottom: 6408480.4514,
              left: 58703.6377,
              right: 3967387.5161,
              top: 11520588.9031,
            },
            srs: 'EPSG:3857',
          },
          animationPayload: {
            interval: 5,
            speed: 4,
          },
          activeLayerId: 'layerid_10',
          displayMapPin: true,
          shouldAnimate: false,
          shouldAutoUpdate: true,
          shouldShowZoomControls: true,
          showTimeSlider: true,
        },
      },
    };
    const newTestId = 'new-test-id';
    const mockSaveAs = jest.fn(() => newTestId);
    const createApi = (): PresetsApi => {
      return {
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: (): Promise<{ data: ViewPreset }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPreset,
            }),
          );
        },
        saveViewPresetAs:
          mockSaveAs as unknown as PresetsApi['saveViewPresetAs'],
        saveViewPreset: jest.fn(),
        deleteViewPreset: jest.fn(),
      };
    };

    const mockState = {
      viewPresets: {
        entities: {
          'preset-1': testViewPresets[0],
          'preset-2': testViewPresets[1],
        },
        ids: ['preset-1', 'preset-2'],
      },
      ui: {
        order: ['layerManager'],
        dialogs: {
          layerManager: {
            activeMapId: mapId,
            isOpen: true,
            type: 'layerManager',
            source: 'app',
          },
        },
      },
      webmap: {
        byId: {
          [mapId]: {
            activeMapPresetId: 'preset-1',
            mapLayers: ['layer_10', 'layer_20'],
            activeLayerId: 'layerid_10',
            baseLayers: [],
            overLayers: [],
            bbox: (testViewPreset.initialProps as InitialMapProps).mapPreset
              .proj.bbox,
            srs: (testViewPreset.initialProps as InitialMapProps).mapPreset.proj
              .srs,
            animationDelay: 250,
            timeStep: 5,
            displayMapPin: true,
            isAnimating: false,
            isAutoUpdating: true,
            shouldShowZoomControls: true,
            isTimeSliderVisible: true,
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          layer_10: {
            ...(testViewPreset.initialProps as InitialMapProps).mapPreset
              .layers[0],
          },
          layer_20: {
            ...(testViewPreset.initialProps as InitialMapProps).mapPreset
              .layers[1],
          },
        },
        allIds: ['layer_10', 'layer_20'],
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, getAllByTestId, getByText, container } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    fireEvent.click(getByTestId('mappresets-menubutton'));

    await waitFor(() => {
      expect(getByTestId('mappresets-menu')).toBeTruthy();
      expect(getAllByTestId('mappreset-menuitem')).toHaveLength(3);
    });

    const presetOptions = getAllByTestId('toggleMenuButton');

    fireEvent.click(presetOptions[0]);
    fireEvent.click(getByText('Save as'));

    await waitFor(() => {
      expect(getByTestId('map-preset-dialog')).toBeTruthy();
    });

    const input = container.parentElement.querySelector('[name="title"]');
    expect(input).toBeTruthy();
    expect(input.getAttribute('value')).toEqual(testViewPresets[0].title);

    const newTitle = ' New title ';
    fireEvent.change(input, {
      target: { value: newTitle },
    });
    expect(input.getAttribute('value')).toEqual(newTitle);

    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(mockSaveAs).toHaveBeenCalled();
    });
    expect(mockSaveAs).toHaveBeenCalledWith({
      ...testViewPreset,
      title: newTitle.trim(),
      scope: 'user',
    });

    const expectedAction = mapActions.setHasMapPresetChanges({
      mapId,
      hasChanges: false,
    });
    expect(store.getActions()).toContainEqual(expectedAction);

    const expectedActiveAction = mapActions.setActiveMapPresetId({
      mapId,
      presetId: newTestId,
    });
    expect(store.getActions()).toContainEqual(expectedActiveAction);
  });

  it('should delete a user preset', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'user',
        title: 'Observations',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const mockDelete = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: jest.fn(),
        saveViewPreset: jest.fn(),
        saveViewPresetAs: jest.fn(),
        deleteViewPreset: mockDelete,
      };
    };

    const mockState = {
      viewPresets: {
        entities: {
          'preset-1': testViewPresets[0],
        },
        ids: ['preset-1'],
      },
      ui: {
        order: ['layerManager'],
        dialogs: {
          layerManager: {
            activeMapId: mapId,
            isOpen: true,
            type: 'layerManager',
            source: 'app',
          },
        },
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, getAllByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    fireEvent.click(getByTestId('mappresets-menubutton'));

    await waitFor(() => {
      expect(getByTestId('mappresets-menu')).toBeTruthy();
      expect(getAllByTestId('mappreset-menuitem')).toHaveLength(1);
    });

    const deleteButtons = getAllByTestId('deleteBtn');

    fireEvent.click(deleteButtons[0]);

    await waitFor(() => {
      expect(getByTestId('map-preset-dialog')).toBeTruthy();
    });

    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(mockDelete).toHaveBeenCalled();
    });
    expect(mockDelete).toHaveBeenCalledWith(testViewPresets[0].id);
  });

  it('should reset the active preset when user deletes it', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'user',
        title: 'Observations',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const mockDelete = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: jest.fn(),
        saveViewPreset: jest.fn(),
        saveViewPresetAs: jest.fn(),
        deleteViewPreset: mockDelete,
      };
    };

    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            activeMapPresetId: 'preset-1',
            mapLayers: [],
            overLayers: [],
            baseLayers: [],
          },
        },
        allIds: [mapId],
      },
      viewPresets: {
        entities: {
          'preset-1': testViewPresets[0],
        },
        ids: ['preset-1'],
      },
      ui: {
        order: ['layerManager'],
        dialogs: {
          layerManager: {
            activeMapId: mapId,
            isOpen: true,
            type: 'layerManager',
            source: 'app',
          },
        },
      },
      layers: {
        byId: {},
        allIds: [],
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, getAllByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    fireEvent.click(getByTestId('mappresets-menubutton'));

    await waitFor(() => {
      expect(getByTestId('mappresets-menu')).toBeTruthy();
      expect(getAllByTestId('mappreset-menuitem')).toHaveLength(1);
    });

    const deleteButtons = getAllByTestId('deleteBtn');

    fireEvent.click(deleteButtons[0]);

    await waitFor(() => {
      expect(getByTestId('map-preset-dialog')).toBeTruthy();
    });

    fireEvent.click(getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(mockDelete).toHaveBeenCalled();
    });
    expect(mockDelete).toHaveBeenCalledWith(testViewPresets[0].id);
    const expectedAction = mapActions.setActiveMapPresetId({
      mapId,
      presetId: null,
    });
    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedAction);
    });
  });

  it('should be able to set a new preset', async () => {
    const mockStore = configureStore();
    const mapId = 'mapId_123';
    const testViewPresets: ViewPresetListItem[] = [
      {
        id: 'preset-1',
        scope: 'user',
        title: 'Observations',
        date: '2022-06-01T12:34:27.787192',
      },
    ];

    const mockDelete = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        getViewPresets: (): Promise<{ data: ViewPresetListItem[] }> => {
          return new Promise((resolve) =>
            resolve({
              data: testViewPresets,
            }),
          );
        },
        getViewPreset: jest.fn(),
        saveViewPreset: jest.fn(),
        saveViewPresetAs: jest.fn(),
        deleteViewPreset: mockDelete,
      };
    };

    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            activeMapPresetId: 'preset-1',
            mapLayers: ['test-layer-1'],
            overLayers: ['test-layer-2'],
            baseLayers: ['test-layer-3'],
          },
        },
        allIds: [mapId],
      },
      viewPresets: {
        entities: {
          'preset-1': testViewPresets[0],
        },
        ids: ['preset-1'],
      },
      ui: {
        order: ['layerManager'],
        dialogs: {
          layerManager: {
            activeMapId: mapId,
            isOpen: true,
            type: 'layerManager',
            source: 'app',
          },
        },
      },
      layers: {
        byId: {
          'test-layer-1': {
            id: 'test-layer-1',
          },
          'test-layer-2': {
            id: 'test-layer-2',
          },
          'test-layer-3': {
            id: 'test-layer-3',
          },
        },
        allIds: ['test-layer-1', 'test-layer-2', 'test-layer-3'],
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, getAllByTestId, queryByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsDialogConnect />
      </PresetsThemeStoreProvider>,
    );

    expect(store.getState().webmap.byId[mapId].activeMapPresetId).toEqual(
      'preset-1',
    );

    fireEvent.click(getByTestId('mappresets-menubutton'));

    await waitFor(() => {
      expect(getByTestId('mappresets-menu')).toBeTruthy();
      expect(getAllByTestId('mappreset-menuitem')).toHaveLength(2);
    });

    const menuItems = getAllByTestId('mappreset-menuitem');

    await waitFor(() => {
      fireEvent.click(menuItems[0]);
    });

    await waitFor(() => {
      expect(queryByTestId('mappresets-menu')).toBeFalsy();
    });

    const expectedAction = mapActions.setMapPreset({
      mapId,
      initialProps: {
        mapPreset: {},
      },
    });
    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedAction);
    });
  });
});
