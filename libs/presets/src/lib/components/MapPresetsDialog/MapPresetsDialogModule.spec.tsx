/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsThemeStoreProvider } from '../Providers';
import MapPresetsDialogModule from './MapPresetsDialogModule';

describe('components/MapPresetsDialog/MapPresetsDialogModule', () => {
  it('should should show module when config is provided', async () => {
    const mockStore = configureStore();
    const mockState = {
      viewPresets: {
        entities: {},
        ids: [],
      },
    };
    const store = mockStore(mockState);
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const props = {
      config: {
        baseUrl: 'test.baseUrl',
        appUrl: 'test.appUrl',
        authTokenUrl: 'test.authTokenUrl',
        authClientId: 'test.authCLientId',
      },
      auth: {
        username: 'test_user',
        token: 'test_token',
        refresh_token: 'test_refresh_token',
      },
      onSetAuth: jest.fn(),
    };

    const { getByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createFakeApi}>
        <MapPresetsDialogModule {...props} />
      </PresetsThemeStoreProvider>,
    );

    // button should be present
    expect(getByTestId('mappresets-menubutton')).toBeTruthy();
  });
});
