/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Card, Box } from '@mui/material';
import { darkTheme } from '@opengeoweb/theme';
import { PresetsThemeProvider } from '../Providers';
import MapPresetsMenuItem from './MapPresetsMenuItem';

export default { title: 'components/MapPresetsMenuItem' };

const MapPresetsMenuItemDemo: React.FC = () => {
  return (
    <Card sx={{ padding: 1, width: 400, height: 300 }}>
      <Box sx={{ position: 'absolute', top: 100, width: 250 }}>
        <MapPresetsMenuItem
          id="radar"
          title="Radar"
          onClickPreset={(): void => null}
          scope="user"
          isOpen
          isActive
        />
      </Box>
    </Card>
  );
};

export const MapPresetsMenuItemLightTheme = (): React.ReactElement => (
  <PresetsThemeProvider>
    <MapPresetsMenuItemDemo />
  </PresetsThemeProvider>
);

export const MapPresetsMenuItemDarkTheme = (): React.ReactElement => (
  <PresetsThemeProvider theme={darkTheme}>
    <MapPresetsMenuItemDemo />
  </PresetsThemeProvider>
);

MapPresetsMenuItemLightTheme.storyName =
  'MapPresetsMenuItem Light Theme (takeSnapshot)';

MapPresetsMenuItemDarkTheme.storyName =
  'MapPresetsMenuItem Dark Theme (takeSnapshot)';
