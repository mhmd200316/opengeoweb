/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { ListItemText, MenuItem } from '@mui/material';
import React from 'react';
import { ToggleMenu, ToolButton } from '@opengeoweb/shared';
import { Delete } from '@opengeoweb/theme';
import { PresetScope } from '../../store/viewPresets/types';

export enum PresetAction {
  OPEN = 'Open',
  DELETE = 'Delete',
  SAVE = 'Save',
  SAVE_AS = 'Save as',
  SHARE = 'Share',
  RENAME = 'Rename',
}

export const getPresetTitle = (
  title: string,
  hasChanges: boolean,
  isActive: boolean,
  isNew: boolean,
): string => {
  if (hasChanges && isActive && isNew) return `${title} (not saved)`;
  if (hasChanges && isActive && !isNew) return `${title} (modified)`;
  return title;
};

interface MapPresetsProps {
  id: string;
  title: string;
  scope: PresetScope;
  onClickPreset: (presetId: string, presetAction: PresetAction) => void;
  isNew?: boolean;
  isOpen?: boolean;
  isActive?: boolean;
  hasChanges?: boolean;
}

const MapPresetsMenuItem: React.FC<MapPresetsProps> = ({
  id,
  title,
  scope,
  onClickPreset,
  isNew = false,
  isOpen = false,
  isActive = false,
  hasChanges = false,
}: MapPresetsProps) => {
  const isUserPreset = scope === 'user' && !isNew;
  const presetTitle = getPresetTitle(title, hasChanges, isActive, isNew);

  const onClickMenuItem = (action: PresetAction): void => {
    onClickPreset(id, action);
  };

  const items = isUserPreset
    ? [
        // TODO: enable again (in same order)
        PresetAction.SAVE,
        PresetAction.SAVE_AS,
        // PresetAction.RENAME, TODDO: enable again https://gitlab.com/opengeoweb/opengeoweb/-/issues/2257
        // PresetAction.SHARE, TODO:enable again https://gitlab.com/opengeoweb/opengeoweb/-/issues/2256
      ]
    : [PresetAction.SAVE_AS];

  const menuItems = items.map((item) => ({
    text: item,
    action: (event: React.MouseEvent): void => {
      event.stopPropagation();
      onClickMenuItem(item);
    },
  }));

  return (
    <MenuItem
      selected={isActive}
      onClick={(): void => onClickMenuItem(PresetAction.OPEN)}
      sx={{
        paddingLeft: '30px',
        paddingRight: '20px',
        ...(isNew && {
          color: 'geowebColors.buttons.tool.disabled.color',
        }),
      }}
      data-testid="mappreset-menuitem"
    >
      <ListItemText
        sx={{
          span: {
            fontWeight: 500,
            fontSize: 12,
            ...((isNew || (hasChanges && isActive)) && {
              fontStyle: 'italic',
              fontSize: 11,
            }),
          },
          paddingRight: '30px',
        }}
      >
        {presetTitle}
      </ListItemText>
      <>
        {isUserPreset && !isNew && (
          <ToolButton
            onClick={(event: React.MouseEvent): void => {
              event.stopPropagation();
              onClickMenuItem(PresetAction.DELETE);
            }}
            variant="flat"
            data-testid="deleteBtn"
          >
            <Delete />
          </ToolButton>
        )}
        {isActive && (
          <ToggleMenu
            variant="flat"
            buttonSx={{
              marginLeft: 0.5,
            }}
            menuItems={menuItems}
            menuPosition="right"
            isDefaultOpen={isOpen}
          />
        )}
      </>
    </MenuItem>
  );
};

export default MapPresetsMenuItem;
