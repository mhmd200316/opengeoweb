/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import { PresetsThemeProvider } from '../Providers';
import { MAPPRESET_ACTIVE_TITLE } from '../MapPresetsDialog/MapPresetsDialog';

import MapPresetsMenutItem, {
  getPresetTitle,
  PresetAction,
} from './MapPresetsMenuItem';

describe('components/MapPresetsDialog/MapPresetsMenuItem', () => {
  describe('getPresetTitle', () => {
    it('should return same title if no changes for new preset', () => {
      expect(getPresetTitle('test', false, true, true)).toEqual('test');
    });
    it('should return same title if no changes for map preset', () => {
      expect(getPresetTitle('test', false, true, false)).toEqual('test');
    });
    it('should return title with (not saved) when there are changes and new preset is active', () => {
      expect(getPresetTitle('test', true, true, true)).toEqual(
        'test (not saved)',
      );
    });
    it('should return title with (modified) if there are changes and map preset is active', () => {
      expect(getPresetTitle('test', true, true, false)).toEqual(
        'test (modified)',
      );
    });
    it('should return same title if there are changes but new preset is not active', () => {
      expect(getPresetTitle('test', true, false, true)).toEqual('test');
    });
    it('should return same title if there are changes but map preset is not active', () => {
      expect(getPresetTitle('test', true, false, false)).toEqual('test');
    });
  });

  it('should render with default props', async () => {
    const props = {
      id: 'test',
      title: 'test title',
      scope: 'system' as const,
      onClickPreset: jest.fn(),
    };

    const { getByTestId, findByText, queryByTestId } = render(
      <PresetsThemeProvider>
        <MapPresetsMenutItem {...props} />
      </PresetsThemeProvider>,
    );

    const menuItem = getByTestId('mappreset-menuitem');

    expect(menuItem).toBeTruthy();
    expect(await findByText(props.title)).toBeTruthy();

    fireEvent.click(menuItem);

    expect(props.onClickPreset).toHaveBeenCalledWith(
      props.id,
      PresetAction.OPEN,
    );

    expect(queryByTestId('deleteBtn')).toBeFalsy();
    expect(queryByTestId('toggleMenuButton')).toBeFalsy();
  });

  it('should show correct title for active map preset', async () => {
    const props = {
      id: 'test',
      title: 'test title',
      scope: 'system' as const,
      onClickPreset: jest.fn(),
      hasChanges: true,
      isActive: true,
      isNew: false,
    };

    const { findByText } = render(
      <PresetsThemeProvider>
        <MapPresetsMenutItem {...props} />
      </PresetsThemeProvider>,
    );

    expect(
      await findByText(
        getPresetTitle(
          props.title,
          props.hasChanges,
          props.isActive,
          props.isNew,
        ),
      ),
    ).toBeTruthy();
  });

  it('should render menu item with preset action buttons for scope user ', async () => {
    const props = {
      id: 'test',
      title: 'test title',
      scope: 'user' as const,
      onClickPreset: jest.fn(),
    };

    const { queryByTestId, findByText } = render(
      <PresetsThemeProvider>
        <MapPresetsMenutItem {...props} />
      </PresetsThemeProvider>,
    );

    const menuItem = queryByTestId('mappreset-menuitem');

    expect(menuItem).toBeTruthy();
    expect(await findByText(props.title)).toBeTruthy();

    fireEvent.click(menuItem);

    expect(queryByTestId('deleteBtn')).toBeTruthy();
    expect(queryByTestId('toggleMenuButton')).toBeFalsy();
  });

  it('should render menu item for a new preset', async () => {
    const props = {
      id: 'test',
      title: MAPPRESET_ACTIVE_TITLE,
      scope: 'system' as const,
      onClickPreset: jest.fn(),
      isNew: true,
    };

    const { getByTestId, getByText, queryByTestId } = render(
      <PresetsThemeProvider>
        <MapPresetsMenutItem {...props} />
      </PresetsThemeProvider>,
    );

    expect(getByTestId('mappreset-menuitem')).toBeTruthy();
    expect(getByText(props.title)).toBeTruthy();
    expect(queryByTestId('deleteBtn')).toBeFalsy();
    expect(queryByTestId('toggleMenuButton')).toBeFalsy();
  });

  it('should should not show the delete button for new preset', async () => {
    const props = {
      id: 'test',
      title: MAPPRESET_ACTIVE_TITLE,
      scope: 'system' as const,
      onClickPreset: jest.fn(),
      isNew: true,
    };

    const { getByTestId, queryByTestId } = render(
      <PresetsThemeProvider>
        <MapPresetsMenutItem {...props} />
      </PresetsThemeProvider>,
    );

    expect(getByTestId('mappreset-menuitem')).toBeTruthy();
    expect(queryByTestId('deleteBtn')).toBeFalsy();
  });

  it('should hide the toggle menu for non active presets ', async () => {
    const props = {
      id: 'test',
      title: 'test title',
      scope: 'user' as const,
      onClickPreset: jest.fn(),
      isActive: false,
    };

    const { queryByTestId } = render(
      <PresetsThemeProvider>
        <MapPresetsMenutItem {...props} />
      </PresetsThemeProvider>,
    );

    expect(queryByTestId('toggleMenuButton')).toBeFalsy();
  });

  it('should show the toggle menu active preset ', async () => {
    const props = {
      id: 'test',
      title: 'test title',
      scope: 'user' as const,
      onClickPreset: jest.fn(),
      isActive: true,
    };

    const { queryByTestId } = render(
      <PresetsThemeProvider>
        <MapPresetsMenutItem {...props} />
      </PresetsThemeProvider>,
    );

    expect(queryByTestId('toggleMenuButton')).toBeTruthy();
  });

  it('should be able to delete a preset', async () => {
    const props = {
      id: 'test',
      title: 'test title',
      scope: 'user' as const,
      onClickPreset: jest.fn(),
    };

    const { getByTestId, findByText } = render(
      <PresetsThemeProvider>
        <MapPresetsMenutItem {...props} />
      </PresetsThemeProvider>,
    );

    const menuItem = getByTestId('mappreset-menuitem');
    expect(menuItem).toBeTruthy();
    expect(await findByText(props.title)).toBeTruthy();

    // delete
    fireEvent.click(getByTestId('deleteBtn'));
    expect(props.onClickPreset).toHaveBeenCalledWith(
      props.id,
      PresetAction.DELETE,
    );
  });

  it('should be able save a preset', async () => {
    const props = {
      id: 'test',
      title: 'test title',
      scope: 'user' as const,
      onClickPreset: jest.fn(),
      isActive: true,
    };

    const { getByTestId, findByText } = render(
      <PresetsThemeProvider>
        <MapPresetsMenutItem {...props} />
      </PresetsThemeProvider>,
    );

    const menuItem = getByTestId('mappreset-menuitem');
    expect(menuItem).toBeTruthy();
    expect(await findByText(props.title)).toBeTruthy();

    fireEvent.click(menuItem);

    // open toggle menu
    fireEvent.click(getByTestId('toggleMenuButton'));

    // save
    fireEvent.click(await findByText(PresetAction.SAVE));
    expect(props.onClickPreset).toHaveBeenCalledWith(
      props.id,
      PresetAction.SAVE,
    );
  });

  it('should be able save a preset as', async () => {
    const props = {
      id: 'test',
      title: 'test title',
      scope: 'user' as const,
      onClickPreset: jest.fn(),
      isActive: true,
    };

    const { getByTestId, findByText } = render(
      <PresetsThemeProvider>
        <MapPresetsMenutItem {...props} />
      </PresetsThemeProvider>,
    );

    const menuItem = getByTestId('mappreset-menuitem');
    expect(menuItem).toBeTruthy();
    expect(await findByText(props.title)).toBeTruthy();

    fireEvent.click(menuItem);

    // open toggle menu
    fireEvent.click(getByTestId('toggleMenuButton'));

    // save as
    fireEvent.click(await findByText(PresetAction.SAVE_AS));
    expect(props.onClickPreset).toHaveBeenCalledWith(
      props.id,
      PresetAction.SAVE_AS,
    );
  });

  // TODO https://gitlab.com/opengeoweb/opengeoweb/-/issues/2256: enable again when able share a preset
  // it('should be able to share a preset', async () => {
  //   const props = {
  //     id: 'test',
  //     title: 'test title',
  //     scope: 'user' as const,
  //     onClickPreset: jest.fn(),
  //   };

  //   const { getByTestId, findByText } = render(
  //     <PresetsThemeProvider>
  //       <MapPresetsMenutItem {...props} />
  //     </PresetsThemeProvider>,
  //   );

  //   const menuItem = getByTestId('mappreset-menuitem');
  //   expect(menuItem).toBeTruthy();
  //   expect(await findByText(props.title)).toBeTruthy();

  //   fireEvent.click(menuItem);

  //   // open toggle menu
  //   fireEvent.click(getByTestId('toggleMenuButton'));

  //   // share
  //   fireEvent.click(await findByText(PresetAction.SHARE));
  //   expect(props.onClickPreset).toHaveBeenCalledWith(
  //     props.id,
  //     PresetAction.SHARE,
  //   );
  // });

  // TODO https://gitlab.com/opengeoweb/opengeoweb/-/issues/2257: enable again when able rename a preset
  // it('should be able to rename a preset', async () => {
  //   const props = {
  //     id: 'test',
  //     title: 'test title',
  //     scope: 'user' as const,
  //     onClickPreset: jest.fn(),
  //   };

  //   const { getByTestId, findByText } = render(
  //     <PresetsThemeProvider>
  //       <MapPresetsMenutItem {...props} />
  //     </PresetsThemeProvider>,
  //   );

  //   const menuItem = getByTestId('mappreset-menuitem');
  //   expect(menuItem).toBeTruthy();
  //   expect(await findByText(props.title)).toBeTruthy();

  //   fireEvent.click(menuItem);

  //   // open toggle menu
  //   fireEvent.click(getByTestId('toggleMenuButton'));

  //   // rename
  //   fireEvent.click(await findByText(PresetAction.RENAME));
  //   expect(props.onClickPreset).toHaveBeenCalledWith(
  //     props.id,
  //     PresetAction.RENAME,
  //   );
  // });
});
