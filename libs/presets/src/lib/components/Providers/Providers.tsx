/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import { DynamicModuleLoader } from 'redux-dynamic-modules';
import {
  lightTheme,
  ThemeProviderProps,
  ThemeWrapper,
} from '@opengeoweb/theme';
import { Store } from '@reduxjs/toolkit';
import {
  mapModuleConfig,
  uiModuleConfig,
  synchronizationGroupsConfig,
} from '@opengeoweb/core';
import { ApiProvider } from '@opengeoweb/api';
import presetsModuleConfig from '../../store/config';

import { createApi as createFakeApi } from '../../utils/fakeApi';

/**
 * A Provider component which provides the GeoWeb theme
 * @param children
 * @returns
 */
export const PresetsThemeProvider: React.FC<ThemeProviderProps> = ({
  children,
  theme = lightTheme,
}: ThemeProviderProps) => <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;

/**
 * A Provider component which provides the GeoWeb theme and store for the presets.
 * Note: Should only be used with presets components, as the provided store is only for presets.
 * @param children
 * @returns
 */
interface PresetsThemeStoreProviderProps extends ThemeProviderProps {
  store: Store;
  createApi?: () => void;
}

export const PresetsThemeStoreProvider: React.FC<PresetsThemeStoreProviderProps> =
  ({
    children,
    theme = lightTheme,
    store,
    createApi = createFakeApi,
  }: PresetsThemeStoreProviderProps) => (
    <ApiProvider createApi={createApi}>
      <Provider store={store}>
        <DynamicModuleLoader
          modules={[
            mapModuleConfig,
            uiModuleConfig,
            synchronizationGroupsConfig,
            presetsModuleConfig,
          ]}
        >
          <ThemeWrapper theme={theme}>{children}</ThemeWrapper>
        </DynamicModuleLoader>
      </Provider>
    </ApiProvider>
  );
