/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { store } from '@opengeoweb/core';
import { darkTheme } from '@opengeoweb/theme';
import React from 'react';
import { AxiosError, AxiosResponseHeaders } from 'axios';
import { PresetsThemeStoreProvider } from '../Providers';
import MapPresetsFormDialog from './MapPresetsFormDialog';
import { PresetAction } from '../MapPresetsMenuItem/MapPresetsMenuItem';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';

export default { title: 'components/MapPresetsFormDialog' };

const SaveAsDemo = (): React.ReactElement => (
  <MapPresetsFormDialog
    isOpen
    title="Save map preset as"
    action={PresetAction.SAVE_AS}
    presetId="Map"
    formValues={{
      title: 'Map preset',
    }}
    onClose={(): void => {}}
    onSuccess={(): void => {}}
  />
);

export const SaveAsDemoLightTheme = (): React.ReactElement => (
  <PresetsThemeStoreProvider store={store}>
    <SaveAsDemo />
  </PresetsThemeStoreProvider>
);

export const SaveAsDemoDarkTheme = (): React.ReactElement => (
  <PresetsThemeStoreProvider theme={darkTheme} store={store}>
    <SaveAsDemo />
  </PresetsThemeStoreProvider>
);

SaveAsDemoLightTheme.storyName = 'Save As light theme (takeSnapshot)';

SaveAsDemoDarkTheme.storyName = 'Save As dark theme (takeSnapshot)';

const DeleteDemo = (): React.ReactElement => (
  <MapPresetsFormDialog
    isOpen
    title="Delete map preset"
    action={PresetAction.DELETE}
    presetId="Map"
    formValues={{
      title: 'Radar',
    }}
    onClose={(): void => {}}
    onSuccess={(): void => {}}
  />
);

export const DeleteDemoLightTheme = (): React.ReactElement => (
  <PresetsThemeStoreProvider store={store}>
    <DeleteDemo />
  </PresetsThemeStoreProvider>
);

export const DeleteDemoDarkTheme = (): React.ReactElement => (
  <PresetsThemeStoreProvider theme={darkTheme} store={store}>
    <DeleteDemo />
  </PresetsThemeStoreProvider>
);

DeleteDemoLightTheme.storyName = 'Delete light theme (takeSnapshot)';

DeleteDemoDarkTheme.storyName = 'Delete dark theme (takeSnapshot)';

const fakeBackendErrorSaveAs: AxiosError = {
  isAxiosError: true,
  config: undefined,
  toJSON: undefined,
  name: 'API error',
  message: '',
  response: {
    data: 'Could not save preset: Preset title already in use, please choose a different title',
    status: 400,
    statusText: '',
    config: undefined,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

const fakeBackendErrorDelete: AxiosError = {
  isAxiosError: true,
  config: undefined,
  toJSON: undefined,
  name: 'API error',
  message: '',
  response: {
    data: 'Could not delete preset: Error deleting preset',
    status: 400,
    statusText: '',
    config: undefined,
    headers: [] as unknown as AxiosResponseHeaders,
  },
};

const createApiWithErrors = (): PresetsApi => {
  return {
    ...createFakeApi(),
    saveViewPresetAs: (): Promise<string> => {
      return new Promise((_, reject) =>
        setTimeout(() => {
          reject(fakeBackendErrorSaveAs);
        }, 1000),
      );
    },
    deleteViewPreset: (): Promise<void> => {
      return new Promise((_, reject) =>
        setTimeout(() => {
          reject(fakeBackendErrorDelete);
        }, 1000),
      );
    },
  };
};

export const SaveAsWithError = (): React.ReactElement => (
  <PresetsThemeStoreProvider store={store} createApi={createApiWithErrors}>
    <SaveAsDemo />
  </PresetsThemeStoreProvider>
);

export const DeleteWithError = (): React.ReactElement => (
  <PresetsThemeStoreProvider store={store} createApi={createApiWithErrors}>
    <DeleteDemo />
  </PresetsThemeStoreProvider>
);
