/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { store } from '@opengeoweb/core';
import MapPresetsFormDialog from './MapPresetsFormDialog';
import { PresetAction } from '../MapPresetsMenuItem/MapPresetsMenuItem';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsThemeStoreProvider } from '../Providers';
import { PresetsApi } from '../../utils/api';
import { emptyMapViewPreset } from './MapPresetsForm';

describe('components/MapPresetsDialog/MapPresetsFormDialog', () => {
  it('should cancel save as', async () => {
    const props = {
      isOpen: true,
      title: 'Save map preset as',
      action: PresetAction.SAVE_AS,
      presetId: 'Map',
      formValues: {
        title: 'Map preset',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const { getByTestId, findByText } = render(
      <PresetsThemeStoreProvider store={store}>
        <MapPresetsFormDialog {...props} />
      </PresetsThemeStoreProvider>,
    );

    expect(await findByText(props.title)).toBeTruthy();

    const dialog = getByTestId('map-preset-dialog');
    expect(getByTestId('map-preset-form')).toBeTruthy();
    expect(dialog).toBeTruthy();

    // should cancel
    fireEvent.click(getByTestId('confirmationDialog-cancel'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onSuccess).not.toHaveBeenCalled();
  });

  it('should close save as', async () => {
    const props = {
      isOpen: true,
      title: 'Save map preset as',
      action: PresetAction.SAVE_AS,
      presetId: 'Map',
      formValues: {
        title: 'Map preset',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const { getByTestId, findByText } = render(
      <PresetsThemeStoreProvider store={store}>
        <MapPresetsFormDialog {...props} />
      </PresetsThemeStoreProvider>,
    );

    expect(await findByText(props.title)).toBeTruthy();

    const dialog = getByTestId('map-preset-dialog');
    expect(getByTestId('map-preset-form')).toBeTruthy();
    expect(dialog).toBeTruthy();

    // should close
    fireEvent.click(getByTestId('confirmationDialog-close'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onSuccess).not.toHaveBeenCalled();
  });

  it('should save a preset as', async () => {
    jest.useFakeTimers();

    const props = {
      isOpen: true,
      title: 'Save map preset as',
      action: PresetAction.SAVE_AS,
      presetId: 'Map',
      formValues: {
        title: '  Map preset  ',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const newTestId = 'new-test-id';

    const saveViewPresetAsWatcher = jest.fn(
      () =>
        new Promise((resolve) =>
          setTimeout(() => {
            resolve(newTestId);
          }, 1000),
        ),
    );

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPresetAs:
          saveViewPresetAsWatcher as unknown as PresetsApi['saveViewPresetAs'],
      };
    };

    const { getByTestId, queryByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </PresetsThemeStoreProvider>,
    );

    const dialog = getByTestId('map-preset-dialog');
    expect(getByTestId('map-preset-form')).toBeTruthy();
    expect(dialog).toBeTruthy();

    // should post
    fireEvent.click(getByTestId('confirmationDialog-confirm'));

    // should show spinner
    await waitFor(() =>
      expect(queryByTestId('confirm-dialog-spinner')).toBeTruthy(),
    );
    // spinner should be gone
    jest.runOnlyPendingTimers();
    await waitFor(() =>
      expect(queryByTestId('confirm-dialog-spinner')).toBeFalsy(),
    );

    await waitFor(() => {
      expect(saveViewPresetAsWatcher).toHaveBeenCalledWith({
        ...emptyMapViewPreset,
        title: props.formValues.title.trim(),
        scope: 'user',
      });
    });

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalledTimes(1);
      expect(props.onSuccess).toHaveBeenCalledTimes(1);
      expect(props.onSuccess).toHaveBeenCalledWith(
        PresetAction.SAVE_AS,
        newTestId,
      );
    });
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not post empty title', async () => {
    const props = {
      isOpen: true,
      title: 'Save map preset as',
      action: PresetAction.SAVE_AS,
      presetId: 'Map',
      formValues: {
        title: '',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const saveViewPresetAsWatcher = jest.fn();

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPresetAs: saveViewPresetAsWatcher,
      };
    };

    const { getByTestId, findByText } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </PresetsThemeStoreProvider>,
    );

    const dialog = getByTestId('map-preset-dialog');
    expect(getByTestId('map-preset-form')).toBeTruthy();
    expect(dialog).toBeTruthy();

    // try to post
    await waitFor(() => {
      fireEvent.click(getByTestId('confirmationDialog-confirm'));
    });
    expect(saveViewPresetAsWatcher).not.toHaveBeenCalled();
    expect(props.onClose).toHaveBeenCalledTimes(0);

    expect(dialog.querySelector('.Mui-error')).toBeTruthy();
    expect(await findByText('This field is required')).toBeTruthy();

    await waitFor(() => {
      fireEvent.change(dialog.querySelector('[name="title"]'), {
        target: {
          value: 'new dialog title',
        },
      });
    });

    expect(dialog.querySelector('.Mui-error')).toBeFalsy();

    await waitFor(() => {
      fireEvent.click(getByTestId('confirmationDialog-confirm'));
    });
    await waitFor(() => {
      expect(saveViewPresetAsWatcher).toHaveBeenCalled();
    });

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalledTimes(1);
      expect(props.onSuccess).toHaveBeenCalledTimes(1);
    });
  });

  it('should show error', async () => {
    const props = {
      isOpen: true,
      title: 'Save map preset as',
      action: PresetAction.SAVE_AS,
      presetId: 'Map',
      formValues: {
        title: 'Map preset',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const error = new Error('test error');

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPresetAs: (): Promise<string> => {
          return new Promise((resolve, reject) => reject(error));
        },
      };
    };

    const { getByTestId, queryByTestId, findByText } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </PresetsThemeStoreProvider>,
    );

    const dialog = getByTestId('map-preset-dialog');
    expect(getByTestId('map-preset-form')).toBeTruthy();
    expect(dialog).toBeTruthy();

    expect(queryByTestId('alert-banner')).toBeFalsy();

    // should not post
    await waitFor(() => {
      fireEvent.click(getByTestId('confirmationDialog-confirm'));
    });

    await waitFor(async () => {
      expect(queryByTestId('alert-banner')).toBeTruthy();
      expect(await findByText(error.message)).toBeTruthy();
      expect(props.onClose).toHaveBeenCalledTimes(0);
    });
  });

  it('should delete a preset', async () => {
    const props = {
      isOpen: true,
      title: 'Delete map preset ',
      action: PresetAction.DELETE,
      presetId: 'Map',
      formValues: {
        title: '  Map preset  ',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const mockDeletePreset = jest.fn();

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        deleteViewPreset: mockDeletePreset,
      };
    };

    const { getByTestId } = render(
      <PresetsThemeStoreProvider store={store} createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </PresetsThemeStoreProvider>,
    );

    expect(getByTestId('map-preset-dialog')).toBeTruthy();

    // should delete
    await waitFor(() => {
      fireEvent.click(getByTestId('confirmationDialog-confirm'));
    });

    await waitFor(() => {
      expect(mockDeletePreset).toHaveBeenCalledWith(props.presetId);
    });

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalledTimes(1);
      expect(props.onSuccess).toHaveBeenCalledTimes(1);
    });
  });
});
