/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';

import MapPresetsForm, { emptyMapViewPreset } from './MapPresetsForm';
import { PresetAction } from '../MapPresetsMenuItem/MapPresetsMenuItem';

describe('components/MapPresetsDialog/MapPresetsForm', () => {
  it('should work with default props', () => {
    const props = {
      formValues: {
        ...emptyMapViewPreset,
        title: 'test title',
      },
      action: PresetAction.SAVE_AS,
    };

    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <MapPresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = container.querySelector('[name="title"]');

    expect(document.activeElement).toEqual(input);
    expect(input.getAttribute('value')).toEqual(props.formValues.title);
    expect(
      container.querySelector('[name="initialProps"]').getAttribute('value'),
    ).toEqual('[object Object]');
    expect(
      container.querySelector('[name="scope"]').getAttribute('value'),
    ).toEqual('user');
    expect(
      container.querySelector('[name="componentType"]').getAttribute('value'),
    ).toEqual('Map');
    expect(
      container.querySelector('[name="keywords"]').getAttribute('value'),
    ).toEqual('mapPreset,viewPreset,layerManager');
  });

  it('should validate required', async () => {
    const props = {
      formValues: {
        title: 'test title',
      },
      action: PresetAction.SAVE_AS,
    };

    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <MapPresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = container.querySelector('[name="title"]');

    expect(document.activeElement).toEqual(input);
    expect(input.getAttribute('value')).toEqual(props.formValues.title);
    expect(container.querySelector('.Mui-error')).toBeFalsy();

    await waitFor(() => {
      fireEvent.change(input, { target: { value: '' } });
    });
    expect(container.querySelector('.Mui-error')).toBeTruthy();

    await waitFor(() => {
      fireEvent.change(input, { target: { value: 'new value' } });
    });
    expect(container.querySelector('.Mui-error')).toBeFalsy();
  });

  it('should show error when title only contains spaces', async () => {
    const props = {
      formValues: {
        title: '',
      },
      action: PresetAction.SAVE_AS,
    };

    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <MapPresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = container.querySelector('[name="title"]');
    fireEvent.change(input, { target: { value: '  ' } });
    await waitFor(() => {
      expect(container.querySelector('.Mui-error')).toBeTruthy();
    });
  });

  it('should show error', async () => {
    const errorMessage = 'test could not save preset';
    const props = {
      formValues: {
        title: 'test title',
      },
      error: errorMessage,
      action: PresetAction.SAVE_AS,
    };

    const { findByText } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <MapPresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    expect(await findByText(errorMessage)).toBeTruthy();
  });

  it('should submit the form when submitting the title field', async () => {
    const props = {
      formValues: {
        title: 'test submit on enter',
      },
      onSubmit: jest.fn(),
      action: PresetAction.SAVE_AS,
    };

    const { container } = render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <MapPresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = container.querySelector('[name="title"]');
    expect(input).toBeTruthy();
    fireEvent.submit(input);
    await waitFor(() => {
      expect(props.onSubmit).toHaveBeenCalled();
    });
  });
});
