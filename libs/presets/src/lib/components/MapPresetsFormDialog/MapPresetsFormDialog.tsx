/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { useApiContext } from '@opengeoweb/api';
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';
import {
  ConfirmationDialog,
  getAxiosErrorMessage,
  isAxiosError,
} from '@opengeoweb/shared';
import React from 'react';
import { useFormContext } from 'react-hook-form';
import { ViewPreset } from '../../store/viewPresets/types';
import MapPresetsForm, { MapPresetFormValues } from './MapPresetsForm';
import { PresetAction } from '../MapPresetsMenuItem/MapPresetsMenuItem';
import { PresetsApi } from '../../utils/api';

const getConfirmLabel = (action: PresetAction): string => {
  switch (action) {
    case PresetAction.DELETE: {
      return 'Delete';
    }
    default:
    case PresetAction.SAVE_AS: {
      return 'Save';
    }
  }
};
export interface MapPresetsFormDialogProps {
  isOpen: boolean;
  title: string;
  action: PresetAction;
  presetId: string;
  formValues: MapPresetFormValues;
  onSuccess: (action: PresetAction, presetId: string) => void;
  onClose: () => void;
}

const MapPresetsFormDialog: React.FC<MapPresetsFormDialogProps> = ({
  isOpen,
  action,
  presetId,
  formValues,
  onSuccess,
  onClose,
  title,
}: MapPresetsFormDialogProps) => {
  const { handleSubmit: handleFormSubmit } = useFormContext();
  const { api } = useApiContext<PresetsApi>();
  const [error, setError] = React.useState<string>();
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (!isOpen) {
      setError(null);
    }
  }, [isOpen]);

  const confirmLabel = getConfirmLabel(action);

  const onFormSubmit = async (newFormValues: ViewPreset): Promise<void> => {
    try {
      setError(null);
      setIsLoading(true);
      switch (action) {
        case PresetAction.SAVE_AS: {
          const newId = await api.saveViewPresetAs(newFormValues);
          setIsLoading(false);
          onClose();
          onSuccess(action, newId);
          break;
        }
        case PresetAction.DELETE: {
          await api.deleteViewPreset(presetId);
          setIsLoading(false);
          onClose();
          onSuccess(action, presetId);
          break;
        }
        default: {
          break;
        }
      }
    } catch (error) {
      setIsLoading(false);
      const message = isAxiosError(error)
        ? getAxiosErrorMessage(error)
        : error.message;
      setError(message);
    }
  };

  const onSubmit = (): void => {
    handleFormSubmit((newFormValues: ViewPreset): void => {
      const { title, ...otherFormValues } = newFormValues;
      const trimmedTitle = title?.trim();

      const parsedFormValues = {
        ...otherFormValues,
        title: trimmedTitle,
      };

      onFormSubmit(parsedFormValues);
    })();
  };

  return (
    <ConfirmationDialog
      data-testid="map-preset-dialog"
      title={title}
      open={isOpen}
      confirmLabel={confirmLabel}
      cancelLabel="Cancel"
      description=""
      onClose={onClose}
      onSubmit={onSubmit}
      isLoading={isLoading}
      content={
        <MapPresetsForm
          formValues={formValues}
          error={error}
          onSubmit={onSubmit}
          action={action}
        />
      }
    />
  );
};

const MapPresetsFormDialogWrapper: React.FC<MapPresetsFormDialogProps> = ({
  ...props
}: MapPresetsFormDialogProps) => (
  <ReactHookFormProvider
    options={{
      ...defaultFormOptions,
    }}
  >
    <MapPresetsFormDialog {...props} />
  </ReactHookFormProvider>
);

export default MapPresetsFormDialogWrapper;
