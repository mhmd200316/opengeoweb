![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/libs/presets/package.json)
![coverage](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg?job=test-presets)

# presets

GeoWeb presets library for the opengeoweb project.
This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test presets` to execute the unit tests via [Jest](https://jestjs.io).

## Image snapshot testing

The current regex filters on storyname takeSnapshot. So to add a story to the snapshot tests, simply add (takeSnapshot) to it's storyName and run the snapshot tests.

[Read more about snapshot testing](https://gitlab.com/opengeoweb/opengeoweb/#image-snapshot-testing)

### Running snapshot tests and updating snapshots locally

1. You need to have [docker](https://docs.docker.com/get-docker/) installed and running.
2. Start Chromium by running: `npm run start-chromium`. (This will start a docker container with chromium, to run snapshot tests in. We need this to make sure everyone gets the same snapshot results.)
3. Run the snapshot tests: `npm run test:image-snap-presets`. This will first create a new static storybook build and then run the tests.
4. If a snapshot test fails, you can find and inspect the differences in `libs/presets/src/lib/storyshots/__image_snapshots__/__diff_output__/`.
5. To update the snapshots, run `npm run test:image-snap-presets-update`. Snapshots are saved under `libs/presets/src/lib/storyshots/__image_snapshots__/`. Make sure to commit the new snapshots.
6. Stop Chromium by running: `npm run stop-chromium`.
