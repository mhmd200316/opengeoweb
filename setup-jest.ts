/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import 'jest-canvas-mock';
import { TextEncoder } from 'util';

// fix for onChange that is not firing for DateTimePicker inputs in jest https://github.com/mui-org/material-ui/issues/27038
jest.mock('@mui/lab/DateTimePicker', () => {
  return jest.requireActual('@mui/lab/DesktopDateTimePicker');
});

Object.defineProperty(window, 'crypto', {
  value: {
    getRandomValues: jest.fn(),
    subtle: {
      digest: jest.fn(),
    },
  },
});

global.TextEncoder = TextEncoder;

let consoleHasErrorOrWarning = false;
const { error, warn } = console;

const ignoredWarningMessages = [
  "Can't get DOM width or height", // ECharts print warning in tests when rendering canvas.
  'MSW_COOKIE_STORE', // msw prints warning in tests when mocked request sets a cookie
];

global.console.warn = (msg: string): void => {
  const ignore = ignoredWarningMessages.some((str) => msg.includes(str));
  if (!ignore) {
    consoleHasErrorOrWarning = true;
    warn(msg);
  }
};

global.console.error = (msg: string): void => {
  consoleHasErrorOrWarning = true;
  error(msg);
};

afterEach(() => {
  if (consoleHasErrorOrWarning) {
    consoleHasErrorOrWarning = false;
    throw new Error(
      'To keep the unit test output readable console errors and warnings are not allowed. Usually these are legitimate errors that should be fixed. If a console.error() or console.warn() is expected or caused by an underlying library, it can be mocked and asserted.',
    );
  }
});
