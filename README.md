[![pipeline status](https://gitlab.com/opengeoweb/opengeoweb/badges/master/pipeline.svg)](https://gitlab.com/opengeoweb/opengeoweb/-/commits/master)
[![current version](https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=version&url=https://gitlab.com/opengeoweb/opengeoweb/raw/master/package.json)](https://gitlab.com/opengeoweb/opengeoweb/-/tags)
[![coverage report](https://gitlab.com/opengeoweb/opengeoweb/badges/master/coverage.svg)](https://gitlab.com/opengeoweb/opengeoweb/-/commits/master)

# Opengeoweb

Nx workspace for geoweb products. This project was generated using [Nx](https://nx.dev).
This workspace contains the following products:

- geoweb: React frontend application (apps/geoweb)
- api: React component library (libs/api)
- authentication: React component library (libs/authentication)
- cap: React component library (libs/cap)
- core: React component library (libs/core) - published to NPM as [@opengeoweb/core](https://www.npmjs.com/package/@opengeoweb/core)
- form-fields: React component library (libs/form-fields) - published to NPM as [@opengeoweb/form-fields](https://www.npmjs.com/package/@opengeoweb/form-fields)
- presets: React component library (libs/presets)
- screenmanager: React component library (libs/screenmanager)
- shared: React component library (libs/shared) - published to NPM as [@opengeoweb/shared](https://www.npmjs.com/package/@opengeoweb/shared)
- sigmet-airmet: React component library (libs/sigmet-airmet) - published to NPM as [@opengeoweb/sigmet-airmet](https://www.npmjs.com/package/@opengeoweb/sigmet-airmet)
- spaceweather: React component library (libs/spaceweather) - published to NPM as [@opengeoweb/spaceweather](https://www.npmjs.com/package/@opengeoweb/spaceweather)
- taf: React component library (libs/taf) - published to NPM as [@opengeoweb/taf](https://www.npmjs.com/package/@opengeoweb/taf)
- theme: React component library (libs/theme) - published to NPM as [@opengeoweb/theme](https://www.npmjs.com/package/@opengeoweb/theme)
- timeseries: React component library (libs/timeseries)
- webmap: React component library (libs/webmap) - published to NPM as [@opengeoweb/webmap](https://www.npmjs.com/package/@opengeoweb/webmap)

Opengeoweb dependency graph - to see an interactive graph, see [Generating a dependency graph](#generating-a-dependency-graph)

![dep-graph](./assets/depGraph.png)

## Content

- [Opengeoweb](#opengeoweb)
  - [Content](#content)
  - [Installation](#installation)
    - [Setup project](#setup-project)
    - [Configure config.json](#configure-configjson)
  - [Running](#running)
    - [Build and run with docker only](#build-and-run-with-docker-only)
    - [Running geoweb application](#running-geoweb-application)
    - [Running storybook](#running-storybook)
  - [Testing](#testing)
    - [Running browser test](#running-browser-test)
    - [Unit tests](#unit-tests)
      - [Running unit tests](#running-unit-tests)
    - [Image snapshot testing](#image-snapshot-testing)
      - [Setup snapshot testing for a library](#setup-snapshot-testing-for-a-library)
      - [Add a new snapshot test](#add-a-new-snapshot-test)
        - [Theme](#theme)
        - [Other libraries](#other-libraries)
      - [Running snapshot tests and updating snapshots locally](#running-snapshot-tests-and-updating-snapshots-locally)
      - [Tips and tricks for creating snapshots](#tips-and-tricks-for-creating-snapshots)
  - [Code linting](#code-linting)
    - [lint workspace](#lint-workspace)
    - [lint affected project](#lint-affected-project)
    - [lint specific app/lib](#lint-specific-applib)
    - [Ignoring ESLint rules](#ignoring-eslint-rules)
  - [Nx Workspace](#nx-workspace)
    - [Updating packages](#updating-packages)
    - [Installing new dependencies](#installing-new-dependencies)
    - [Generating a dependency graph](#generating-a-dependency-graph)
    - [Adding an application to this workspace](#adding-an-application-to-this-workspace)
    - [Adding a new library to this workspace](#adding-a-new-library-to-this-workspace)
    - [Removing a project from this workspace](#removing-a-project-from-this-workspace)
    - [Generating a new React component in an existing app or lib](#generating-a-new-react-component-in-an-existing-app-or-lib)
    - [Custom build scripts](#custom-build-scripts)
    - [Further help](#further-help)
  - [Code guidelines](#code-guidelines)
    - [Functional programming](#functional-programming)
    - [Naming conventions](#naming-conventions)
    - [(redux) hooks](#redux-hooks)
      - [Caveats with redux hooks](#caveats-with-redux-hooks)
  - [Bundling, packaging and publishing to NPM](#bundling-packaging-and-publishing-to-npm)
  - [Generate TypeDoc Documentation](#generate-typedoc-documentation)
  - [Licence](#licence)
  - [Build and configure](#build-and-configure)
    - [Build and configure a bundle for the GeoWeb application](#build-and-configure-a-bundle-for-the-geoweb-application)
      - [Presets for the GeoWeb application](#presets-for-the-geoweb-application)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Installation

### Setup project

To run the opengeoweb commands you need both NPM and Nx. The easiest way to install NPM is via nvm. Please visit https://nvm.sh/ and follow the instructions. When nvm is installed, run the following commands to install NPM and Nx:

```
nvm install 14
npm install -g nx
npm install -g typescript@4.1.4
```

NOTES:

1. Minimum node version is 14!
1. If snapshots don't work and you are on a newer version of node try downgrading to node 14: `nvm use 14`.
1. If you get "command not found: nx": it was probably not installed for the node version you are using at that moment. Run `npm install -g nx` again.

After NPM installation clone the repo on your local machine and install the dependencies.

```
git clone https://gitlab.com/opengeoweb/opengeoweb.git
cd opengeoweb/
npm ci
```

### Configure config.json

Follow these steps to setup the institute specific config values

- duplicate `apps/geoweb/src/assets/config.example.json` and rename it to `config.json`
- fill in the correct values. In `apps/geoweb/src/app/utils/loadConfig.ts` you can find the complete list of supported keys, see the `ConfigType` type . Be aware that some values are depending on each other. See for example the const `requiredConfigKeysForAuthentication`.

## Running

### Build and run with docker only

Build docker image with `docker build -t gw-test .`
Run docker image with `docker run -it -p 8080:8080 gw-test` and visit http://localhost:8080
The geoweb docker container can be configured with environment variables prefixed with `GW_` which are reflected in the config.json of the application. You can check http://localhost:8080/assets/config.json for the current used configuration.

Run docker image with `docker run -it -p 8080:8080 -e GW_TEST="helloworld" gw-test` and visit http://localhost:8080

### Running geoweb application

Run `nx serve` or `npm run start` to start the default application, which currently is `geoweb`, also known as the `geoweb unified application`

### Running storybook

```
nx run core:storybook
nx run form-fields:storybook
nx run sigmet-airmet:storybook
nx run spaceweather:storybook
nx run taf:storybook
nx run shared:storybook
nx run cap:storybook
nx run presets:storybook
```

## Testing

### Running browser test

To run all the cypress tests locally (automatically starts up the corresponding application):

```
nx e2e geoweb-e2e
```

To keep the Cypress GUI open, add the watch flag:

```
nx e2e geoweb-e2e --watch
```

### Unit tests

- More info about testing React components [react-testing-library](https://testing-library.com/docs/react-testing-library/intro/)
- More info about testing React hooks [@testing-library/react-hooks](https://github.com/testing-library/react-hooks-testing-library)

#### Running unit tests

Run `nx test <lib-name>` to execute the unit tests for a single library.

Run `npm run test:all` to execute the unit tests from all libraries affected by a change.

Run `nx test <lib-name> --test-file <file-pattern>` to execute a specific unit test

### Image snapshot testing

Image snapshot tests verify that the visual appearance of a rendered view remains unchanged. This is specifically useful for components that are hard to unit test otherwise, such as canvas components. If a view is deliberately changed, new snapshots must be created. Up-to-date snapshots should be committed to source control. Currently this is configured for these libraries:

- core
- form-fields
- presets
- shared
- sigmet-airmet
- spaceweather
- taf
- theme
- timeseries

In below instructions, replace `<libname>` with `core`, `form-fields`, `presets`, `shared`, `sigmet-airmet`, `spaceweather`, `taf`, `theme` or `timeseries` accordingly.

#### Setup snapshot testing for a library

To enable snapshot testing in a library, follow these steps

1. Create a folder named `storyshots` inside `libs/<libname>/src/lib`
2. Create `Storyshots.spec.ts` file inside `libs/<libname>/src/lib/storyshots/Storyshots.spec.ts`

```javascript
// libs/<libname>/src/lib/storyshots/Storyshots.spec.ts
import initStoryshots from '@storybook/addon-storyshots';
import { pipelineTest, localTest } from '../../../../../setup-storyshots';

const chromePath = process.env.CHROME_BIN;

initStoryshots({
  configPath: './libs/<libname>/.storybook',
  test: chromePath ? pipelineTest('<libname>') : localTest('<libname>'),
  storyNameRegex: /.?takeSnapshot.?/,
});
```

> **note** with `storyNameRegex` decide how to find the snapshots inside stories. In this example every story with `takeSnapshot` in the title will get a snapshot

3. Create `jest.snapshots.config.js` file and put it inside `libs/<libname>` with the following content:

```javascript
// libs/<libname>/jest.snapshots.config.js
module.exports = {
  preset: '../../jest.preset.js',
  testMatch: ['**/Storyshots.spec.ts'],
  coverageDirectory: '../../coverage/libs/<libname>',
  displayName: '<libname>',
};
```

4. Jest needs to ignore the snapshots for normal unit tests, so we need to update the file `libs/<libname>/jest.config.js` and add the `testMatch` property:

```javascript
// libs/<libname>/jest.config.js
module.exports = {
  preset: '../../jest.preset.js',
  testMatch: ['**/+(*.)+(spec|test).+(ts|js)?(x)', '!**/Storyshots.spec.ts'],
  ...
};
```

5. In that same file we need to add `'!**/storyshots/**',` to the `collectCoverageFrom` property to exclude the storyshots from the jest coverage

6. Add the npm snapshot tasks to `package.json`, for both testing and updating snapshots

```
// package.json
{
  "test:image-snap-<libname>": "nx run <libname>:build-storybook && npx jest ./libs/<libname>/src/lib/storyshots/Storyshots.spec.ts -c ./libs/<libname>/jest.snapshots.config.js",
  "test:image-snap-<libname>-update": "npx jest ./libs/<libname>/src/lib/storyshots/Storyshots.spec.ts -u -c ./libs/<libname>/jest.snapshots.config.js",
}
```

7. Update the npm task `start-chromium` inside `package.json` with adding the path of the lib.

```
-v $(pwd)/dist/storybook/<libname>:/usr/src/dist/storybook/<libname>
```

8. The snapshots should work now if you run the npm scripts defined in step 5. The last step is to add the snapshot test to the pipeline. Inside `.gitlab-ci.yml` add:

```
snapshot-test-<libname>:
image:
    name: vanrijn/alpine-chrome:102-with-node-14
    entrypoint: ['']
stage: run
script:
  - npm run test:image-snap-<libname>
artifacts:
  when: on_failure
  paths:
    - libs/<libname>/src/lib/storyshots/__image_snapshots__/__diff_output__/
  expire_in: 1 day
only:
  - branches
except:
  - tags
interruptible: true
```

#### Add a new snapshot test

Image snapshot tests are automatically created for all Storybook stories matching the regex defined at `libs/<libname>/src/lib/storyshots/Storyshots.spec.ts`.

##### Theme

For Theme the current regex filters on storykind `snapshots/`. So that means all stories inside the snapshots folder in storybook will be included. To add a new snapshot test, add a story inside the folder `libs/theme/src/lib/stories/snapshots`, and make sure in storybook itself it's also shown under snapshots. Then run the snapshot tests. If no snapshot exists, a new one will be added automatically.

##### Other libraries

For the other libraries (`core`, `form-fields`, `presets`, `shared`, `sigmet-airmet`, `spaceweather`, `taf` and `timeseries`) the current regex filters on storyname `takeSnapshot`. So to add a story to the snapshot tests, simply add `(takeSnapshot)` to its storyName and run the snapshot tests. If no snapshot exists, a new one will be added.

#### Running snapshot tests and updating snapshots locally

1. You need to have [docker](https://docs.docker.com/get-docker/) installed and running.
2. Start Chromium by running: `npm run start-chromium`. (This will start a docker container with chromium, to run snapshot tests in. We need this to make sure everyone gets the same snapshot results.)
3. Run the snapshot tests: `npm run test:image-snap-<libname>`. This will first create a new static storybook build and then run the tests.
4. If a snapshot test fails, you can find and inspect the differences in `libs/<libname>/src/lib/storyshots/__image_snapshots__/__diff_output__/`.
5. To update the snapshots, run `npm run test:image-snap-<libname>-update`. Snapshots are saved under `libs/<libname>/src/lib/storyshots/__image_snapshots__/`. Make sure to commit the new snapshots.
6. Stop Chromium by running: `npm run stop-chromium`.

#### Tips and tricks for creating snapshots

- Don't use React.Fragments as a container for your component when using snapshots.

```javascript
// don't
export const MyComponent = (): React.ReactElement => (
  <>
    <p>test</p>
    <span>testing</span>
  </>
);

// do
export const MyComponent = (): React.ReactElement => (
  <div>
    <p>test</p>
    <span>testing</span>
  </div>
);
```

- Web elements that look different in different browsers (like scrollbars) can cause issues. Hide the elements for the snapshot story
- Make sure the snapshot is only showing the component that you want to test. Sometimes storybook examples have some additional description and that's perfect for developers but not so good for snapshot tests. Consider exporting the demo part of your story to use in your snapshot test, and use the description and other elements in the main story.

## Code linting

The code is linted by Eslint. All projects/libraries currently use the same configuration file: `.eslintrc`, located in the root of the project. If you need more or less rules for the whole project, edit this file. If you need specific rules for a app/lib (when you don't use React for example) then you can edit the `.eslintrc` file in the app/lib root folder.

### lint workspace

`npm run lint`

### lint affected project

`npm run affected:lint`

### lint specific app/lib

`nx lint sigmet-airmet`
or `nx run sigmet-airmet:lint`

### Ignoring ESLint rules

If, for some reason, ESLint needs to be ignored, a reason must be stated. For example:
`// eslint-disable-next-line`
would be considered incomplete. Instead, add the reason for ignoring the line:
`// eslint-disable-next-line no-unused-vars`

## Nx Workspace

### Updating packages

To update all Nx packages (@nrwl):

1. run `npm run update`
2. follow the instructions!
3. commit and merge the changes

To update other packages:

1. to see a list of which packages could need updating, run `npm outdated -l`
2. check if there are any breaking changes to be aware of in the new version of a package
3. update a single package: `npm install <package-name>@<version>` or `npm install <package-name>@<version> --save-dev`
4. commit the updated files package.json and package-lock.json
5. update the code related to changes if needed
6. commit and merge the changes

### Installing new dependencies

Installing new dependencies works the same as every project:
`npm install <package-name>@<version>` or `npm install <package-name>@<version> --save-dev`

When working on a library, make sure you include the dependency to build task in file 'project.json', under the key `external`. Example:

```
{
  "root": "libs/sigmet-airmet",
  "sourceRoot": "libs/sigmet-airmet/src",
  "projectType": "library",
  "generators": {},
  "targets": {
        "build": {
          "executor": "@nrwl/web:rollup",
          "options": {
            "external": ["react", "react-dom", "@mui/material"],
          }
        },
    }
}
```

### Generating a dependency graph

Run `nx graph` to see a diagram of the dependencies of your projects. Run `nx affected:dep-graph --watch` to see the dependencies that are affected by your changes. The `--watch` flag makes it automatically update on new changes.

### Adding an application to this workspace

Run `nx g @nrwl/react:app my-app` to generate an application.
Make sure to generate a new dependency graph and add it to this readme.

When using Nx, you can create multiple applications and libraries in the same workspace.

### Adding a new library to this workspace

Libraries are shareable across other libraries and applications. They can be imported from `@opengeoweb/mylib`.

1. Run `nx g @nrwl/react:lib <name>` to generate a react library. Run `nx g @nrwl/react:lib <name> --publishable --importPath="@opengeoweb/<name>"` to generate a react library that can be published to npm.
1. Configure storybook by running `nx g @nrwl/react:storybook-configuration <name>`. Make sure it uses a unique port, you can configure this in `project.json`. Compare the contents of the `.storybook` folder with one of the other libraries and copy/update the files to be similar.
1. Make sure to update the `sync-version` script in package.json, this will keep the versions synchronized automatically. If you made a non-publishable library, you have to manually add the package.json file.
1. Tag the project in `project.json`. These tags provide constraints on importing as defined in `.eslintrc.json`. For more info see [Nx tags](https://nx.dev/latest/angular/workspace/structure/monorepo-tags). To see changes in tags take effect, you have to reopen VSCode.
1. Add the necessary custom scripts in `project.json`: typecheck | docs.
1. Make sure `tsconfig.lib.json` does not exclude stories or tests, and has the necessary types (copy from another lib). And make sure the typecheck script in the `project.json` file points to this `tsconfig.lib.json`.
1. Update the test script in `project.json` to include coverage (copy from one of the other test scripts).
1. Update the `eslintrc.json`, copy the contents from one of the other libs.
1. Make sure the pipeline jobs include the new library where needed, at least create a new job to run the tests.
1. If you run into the error `Buildable libraries cannot import non-buildable libraries`, add a build script for your new library in `project.json`.
1. Make sure to generate a new dependency graph and add it to this readme.

### Removing a project from this workspace

Run `nx g @nrwl/workspace:remove <name>`.

### Generating a new React component in an existing app or lib

For example, run `nx g @nrwl/react:component my-component --project=sigmet-airmet` to generate a new component inside the sigmet-airmet library.

### Custom build scripts

Custom scripts are currently defined in two places; in `project.json` and `package.json`. Tasks need to be defined in workspace, and be used via npm script. We're currently investigating if this is the way to go (Nx builders vs Nx schematics), so it might change in the future. Current tasks (as defined in `project.json`):

To run a command for a specific app or library:
`nx typecheck sigmet-airmet`

To use these commands with the affected:
`nx affected --target=typecheck`

For now the 'global' scripts are prefixed with geoweb, and the project specific with sigmet-airmet. We use these prefixes to differentiate default scripts with our custom ones.

### Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.

## Code guidelines

### Functional programming

Use pure functions and make sure values are immutable. This will make the code more robust, easier to read and easier to test. A easy way to write pure functions is to not use `let` and only use `const`. For more info about functional programming check [this article](https://opensource.com/article/17/6/functional-javascript).

### Naming conventions

Regarding our file structure:

- In general, folders shall be named using camelCase
- Files and folders containing components shall be named using PascalCase
- Files with genetic names (such as 'reducer.ts', 'index.ts') shall be named using camelCase.

### (redux) hooks

- When creating a state for your component, consider extracting it to a hook. That way your functionality is nicely bundled, able to be shared and easy to test. For examples [see](https://reactjs.org/docs/hooks-custom.html#extracting-a-custom-hook)
- When creating a new component which is connected to redux, use redux hooks for this instead of wrapping the component with connect. For examples [see](https://react-redux.js.org/api/hooks)

#### Caveats with redux hooks

- When making use of redux useDispatch, use it in combination with React.useCallback to prevent unneccesary re-rendering of your functional component. For examples [see](https://react-redux.js.org/api/hooks#usedispatch)
- When making use of redux useSelector, be careful to use the selector that only returns the necessary information. Otherwise re-renders will occur with information updates you don't need.
- When implementing a selector, try to return a single value as these can be memoized. For details [see here](https://react-redux.js.org/api/hooks#equality-comparisons-and-updates)

Example on how not to work with selectors:

```
  const [currentActiveMapId, isOpenInStore] = useSelector((store: AppStore) =>
    uiSelectors.getisDialogOpenAndMapId(store, 'layerManager'),
  );
```

The above will always trigger re-renders. Use instead:

```
    const currentActiveMapId = useSelector((store: AppStore) =>
      uiSelectors.getDialogMapId(store, 'layerManager'),
    );

    const isOpenInStore = useSelector((store: AppStore) =>
      uiSelectors.getisDialogOpen(store, 'layerManager'),
    );
```

These values can be memoized and will not trigger re-renders if they are unchanged.

## Bundling, packaging and publishing to NPM

We use Semantic Versioning, see the [documentation](https://semver.org) for details.

1. Decide how to bump the version, is it a major, minor or patch version?
2. Create a new branch based off master
3. Make sure there is an upstream branch (check with `git push`)
4. Make sure your local git tags are in sync with the remote (check with `git tag` for local tags and fetch remote tags using `git fetch --all --tags`). If you have local tags that are not on the remote, remove them with `git tag -d tagname`.
5. Update the version by running `npm version major|minor|patch`
6. The changes are committed and pushed automatically. Get the branch merged into master.
7. The release tag was created and pushed automatically. Make sure to add a description of changes in the Release notes for the tag.
8. Go to the gitlab pipeline of master and run the publish job for any affected libraries
9. The libraries will be published to the `@opengeoweb` organisation on npmjs.

## Generate TypeDoc Documentation

It is possible to automatically generate documentation from the source code using [TypeDoc](http://typedoc.org/).

In order to do so, run `nx run <project-name>:docs`.

Please note that nothing will be created if the code contains errors. Otherwise, a new `docs/<project-name>` folder will be created under the root folder.

Documentation is also published from the gitlab pipeline of master and can be found here:

- [Core](https://opengeoweb.gitlab.io/opengeoweb/docs/core/)
- [Sigmet-airmet](https://opengeoweb.gitlab.io/opengeoweb/docs/sigmet-airmet/)
- [Taf](https://opengeoweb.gitlab.io/opengeoweb/docs/taf/)
- [Spaceweather](https://opengeoweb.gitlab.io/opengeoweb/docs/spaceweather/)
- [ScreenManager](https://opengeoweb.gitlab.io/opengeoweb/docs/screenmanager/)
- [Theme](https://opengeoweb.gitlab.io/opengeoweb/docs/theme/)

## Licence

This project is licensed under the terms of the Apache 2.0 licence.
Every file in this project has a header that specifies the licence and copyright. It is possible to check/add/remove the licence header using the following commands:

`npm run licence:check` => checks all the files for having the header text as specified in the LICENCE file. The format and which files to include is specified in `licence.config.json`.

`npm run licence:add` => adds licence to files without it. This action can require a manual check when the file contains a wrong header (i.g. a simple word is missed), because it just adds another header without removing the wrong one.

`npm run licence:remove` => removes licence from all files. This action can require a manual check when the file contains a wrong header (i.g. a simple word is missed), because it only removes a correct header and not a wrong one.

## Build and configure

Below are instructions how to build and configure the applications.

### Build and configure a bundle for the GeoWeb application

After checking out the correct tag from this repository and running `npm ci`, a bundle for the Geoweb frontend can be generated with the following command:

`rm -rf dist && npm run geoweb:build`

The geoweb frontend will be written to the `./dist/apps/geoweb` folder.

This folder can be hosted from a bucket or tested out locally using `npx serve -p 5400 -s dist/apps/geoweb`

The configuration file of the frontend is in `geoweb/assets/config.json`. This configuration file should be generated and written by CI/CD. The configuration file is a json with the properties as defined in ConfigType (see https://gitlab.com/opengeoweb/opengeoweb/-/blob/master/apps/geoweb/src/app/utils/loadConfig.ts).

##### Presets for the GeoWeb application

The user retrieves GitLab access token at login if the authentication process is successful. Access token is added to the header of the preset API requests.
The preset API takes care of validation of the access token and checking the user permissions.

Preset default values are used

- in case the user is not logged in,
- if the logged-in user does not have access permissions to the presets API,
- if the user logs in, but retrieving presets from the API fails for some reason.

The default preset settings in `./apps/geoweb/src/app/utils` folder are divided into two parts/files

- initialPresets.json (predefined base layers, over layers and services for the layer manager)
- screenPresets.json (predefined screen manager views, layers and settings)

Presets retrieved from the presets API will contain the same two files, but with settings configured for the specific environment.
