/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import '@testing-library/cypress/add-commands';

Cypress.Commands.add('mockCognitoLoginSuccess', () => {
  cy.mockGetVersion();
  cy.mockExampleConfig();
  cy.fixture('mockLoginResponseCognito.json').then((json) => {
    cy.intercept('POST', '**/auth/token', {
      statusCode: 200,
      body: json,
    }).as('token');
  });
  cy.visit('/').then(() => {
    cy.wait('@exampleConfig').then((interception) => {
      assert.isNotNull(interception.response.body);
      cy.url().should('contain', 'login?client_id=');
      // eslint-disable-next-line @typescript-eslint/naming-convention
      const { oauth_state } = window.sessionStorage;
      cy.visit(`/code?state=${oauth_state}&code=dummyCodeToMockLogin`).then(
        () => {
          cy.wait('@token');
          // User should be at the homepage
          cy.url().should('eq', 'http://localhost:5400/');
        },
      );
    });
  });
});

Cypress.Commands.add('mockGitlabLoginSuccess', () => {
  cy.mockGetVersion();
  cy.mockExampleConfigGitlabLogin();
  cy.fixture('mockLoginResponseGitlab.json').then((json) => {
    cy.intercept('POST', '**/oauth/token', {
      statusCode: 200,
      body: json,
    }).as('tokencall');
  });
  cy.visit('/').then(() => {
    cy.wait('@exampleConfigGitlabLogin').then((interception) => {
      assert.isNotNull(interception.response.body);
      cy.url().should('contain', 'https://gitlab.com/users/sign_in');
      // eslint-disable-next-line @typescript-eslint/naming-convention
      const { oauth_state } = window.sessionStorage;
      cy.visit(`/code?state=${oauth_state}&code=dummyCodeToMockLogin`).then(
        () => {
          cy.wait('@tokencall');
          // User should be at the homepage
          cy.url().should('eq', 'http://localhost:5400/');
        },
      );
    });
  });
});

Cypress.Commands.add('mockLoginFailure', () => {
  cy.mockExampleConfig();
  cy.intercept('POST', '**/auth/token', {
    statusCode: 403,
  }).as('loginFailed');

  cy.visit('/').then(() => {
    cy.wait('@exampleConfig').then((interception) => {
      assert.isNotNull(interception.response.body);
      cy.url().should('contain', 'login?client_id=');
      // eslint-disable-next-line @typescript-eslint/naming-convention
      const { oauth_state } = window.sessionStorage;
      cy.visit(`/code?state=${oauth_state}&code=dummyCodeToMockLogin`).then(
        () => {
          cy.wait('@loginFailed');
        },
      );
    });
  });
});

Cypress.Commands.add('mockCognitoLogoutSuccess', () => {
  cy.intercept('GET', /\/logout/, {
    statusCode: 302,
    headers: {
      location: 'http://localhost:5400/login',
    },
  }).as('logout');

  cy.findByTestId('userButton').click();
  cy.findByText('Logout')
    .click()
    .then(() => {
      cy.wait('@logout');
    });
});

Cypress.Commands.add('mockFetchTAFList', () => {
  cy.fixture('mockTAFList').then((json) => {
    cy.intercept('GET', /\/aviation\/taflist/, {
      statusCode: 200,
      body: json,
    }).as('taflist');
  });
});

Cypress.Commands.add('mockGetTac', () => {
  cy.fixture('mockTAC').then((text) => {
    cy.intercept('POST', /\/aviation\/taf2tac/, {
      statusCode: 200,
      body: text,
    }).as('getTac');
  });
});

Cypress.Commands.add(
  'dragTo',
  { prevSubject: true },
  (subject, dropSelector) => {
    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wrap(subject)
      .trigger('pointerdown', {
        button: 0,
      })
      .wait(500)
      .trigger('pointermove', {
        button: 0,
      })
      .wait(500);
    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.get(dropSelector)
      .trigger('pointermove', {
        button: 0,
      })
      .wait(500)
      .trigger('pointerup')
      .wait(500)
      .trigger('mouseup')
      .wait(500);
  },
);

Cypress.Commands.add('mockRadarGetCapabilities', () => {
  cy.fixture('mockMetNoGetCapabilities.xml').then((data) => {
    cy.intercept(
      'GET',
      /public-wms.met.no\/verportal\/verportal.map\?service=WMS&request=GetCapabilities/,
      {
        statusCode: 200,
        body: data,
        headers: {
          'Content-Type': 'text/xml',
        },
      },
    ).as('getCapabilitiesMetNO');
  });

  cy.fixture('mockKNMIGetCapabilities.xml').then((data) => {
    cy.intercept(
      'GET',
      /geoservices.knmi.nl\/wms\?dataset=RADAR&service=WMS&request=GetCapabilities/,
      {
        statusCode: 200,
        body: data,
        headers: {
          'Content-Type': 'text/xml',
        },
      },
    ).as('getCapabilitiesKNMI');
  });

  cy.fixture('mockFMIGetCapabilities.xml').then((data) => {
    cy.intercept(
      'GET',
      /openwms.fmi.fi\/geoserver\/wms\?service=WMS&request=GetCapabilities/,
      {
        statusCode: 200,
        body: data,
        headers: {
          'Content-Type': 'text/xml',
        },
      },
    ).as('getCapabilitiesFMI');
  });
});

Cypress.Commands.add('waitOnRadarGetCapabilities', () => {
  cy.wait('@getCapabilitiesMetNO');
  cy.wait('@getCapabilitiesFMI');
  cy.wait('@getCapabilitiesKNMI');
});

Cypress.Commands.add('mockGetVersion', () => {
  cy.intercept('GET', /\/infra\/version/, {
    statusCode: 200,
    body: { version: 'tst' },
  }).as('getVersion');
});

Cypress.Commands.add('mockExampleConfig', () => {
  cy.intercept('GET', '/assets/config.json', {
    fixture: 'mockExampleConfigCognitoLogin.json',
  }).as('exampleConfig');
});

Cypress.Commands.add('mockExampleConfigGitlabLogin', () => {
  cy.intercept('GET', '/assets/config.json', {
    fixture: 'mockExampleConfigGitlabLogin.json',
  }).as('exampleConfigGitlabLogin');
});

Cypress.Commands.add('mockExampleConfigNoFeatures', () => {
  cy.intercept('GET', '/assets/config.json', {
    fixture: 'mockExampleConfigNoFeatures.json',
  }).as('exampleConfigNoFeatures');
});

Cypress.Commands.add('mockFetchPresetList', () => {
  cy.fixture('mockPresetList').then((json) => {
    cy.intercept('GET', /\/viewpreset/, {
      statusCode: 200,
      body: json,
    }).as('presetlist');
  });
});

Cypress.Commands.add('mockFetchPreset', () => {
  cy.fixture('mockPreset').then((json) => {
    cy.intercept('GET', /\/NLwarnings/, {
      statusCode: 200,
      body: json,
    }).as('preset');
  });
});

Cypress.Commands.add('mockDeletePreset', () => {
  cy.intercept('DELETE', /\/NLwarnings/, {
    statusCode: 204,
  }).as('deletePreset');
});

Cypress.Commands.add('mockSavePresetAs', () => {
  cy.intercept('POST', /\/viewpreset/, {
    statusCode: 200,
  }).as('savePresetAs');
});

Cypress.Commands.add('mockSavePreset', () => {
  cy.intercept('PUT', /\/NLwarnings/, {
    statusCode: 201,
  }).as('savePreset');
});

Cypress.Commands.add('mockPresets', () => {
  cy.mockFetchPresetList();
  cy.mockFetchPreset();
  cy.mockDeletePreset();
  cy.mockSavePresetAs();
  cy.mockSavePreset();
});

Cypress.on('uncaught:exception', (err) => {
  // sometimes a service won't load
  // returning false here prevents cypress from failing the test
  if (err.message.includes('No Capability element found in service')) {
    return false;
  }
  // we still want to ensure there are no other unexpected
  // errors, so we let them fail the test
  return true;
});
