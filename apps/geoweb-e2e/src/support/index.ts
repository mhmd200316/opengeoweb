/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
/* eslint-disable @typescript-eslint/no-namespace */
/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */

declare namespace Cypress {
  interface Chainable {
    /**
     * Mock a failed login
     * @example cy.mockLoginFailure()
     */
    mockLoginFailure(): void;
    /**
     * Mock a successful login
     * @example cy.mockCognitoLoginSuccess()
     */
    mockCognitoLoginSuccess(): void;
    /**
     * Mock a successful login
     * @example cy.mockGitlabLoginSuccess()
     */
    mockGitlabLoginSuccess(): void;
    /**
     * Mock a successful logout
     * @example cy.mockCognitoLogoutSuccess()
     */
    mockCognitoLogoutSuccess(): void;
    /**
     * Mock the GET /aviation/taflist
     * @example cy.mockFetchTAFList()
     */
    mockFetchTAFList(): void;
    /**
     * Mock POST /aviation/taf2tac
     * @example cy.mockGetTac()
     */
    mockGetTac(): void;
    /**
     * Custom drag and drop command that works for react-sortablejs
     * @param dropSelector string
     * @example cy.get('dragSelector').dragTo('dropSelector');
     */
    dragTo(dropSelector: string): void;
    /**
     * Mock the GET capabilities requests for Radar wms services
     * @example cy.mockRadarGetCapabilities()
     */
    mockRadarGetCapabilities(): void;
    /**
     * Wait until the GET capabilities requests for Radar are finished
     * @example cy.waitOnRadarGetCapabilities()
     */
    waitOnRadarGetCapabilities(): void;
    /**
     * Mock the GET version request
     * @example cy.mockGetVersion()
     */
    mockGetVersion(): void;
    /**
     * Mock the config request
     * @example cy.mockExampleConfig()
     */
    mockExampleConfig(): void;
    /**
     * Mock the config request
     * @example cy.mockExampleConfigGitlabLogin()
     */
    mockExampleConfigGitlabLogin(): void;
    /**
     * Mock the config request
     * @example cy.mockExampleConfigNoFeatures()
     */
    mockExampleConfigNoFeatures(): void;
    /**
     * Mock the GET /viewpreset request
     * @example cy.mockFetchPresetList()
     */
    mockFetchPresetList(): void;
    /**
     * Mock the GET /NLwarnings preset request
     * @example cy.mockFetchPreset()
     */
    mockFetchPreset(): void;
    /**
     * Mock the DELETE /NLwarnings preset request
     * @example cy.mockDeletePreset()
     */
    mockDeletePreset(): void;
    /**
     * Mock the POST /viewpreset request
     * @example cy.mockSavePresetAs()
     */
    mockSavePresetAs(): void;
    /**
     * Mock the PUT /NLwarnings preset request
     * @example cy.mockSavePreset()
     */
    mockSavePreset(): void;
    /**
     * Mock all preset requests
     * @example cy.mockPresets()
     */
    mockPresets(): void;
  }
}
