/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

describe('LayerManager', () => {
  it('should duplicate and remove a layer', () => {
    cy.mockExampleConfigNoFeatures();
    cy.mockRadarGetCapabilities();
    cy.visit('/');

    // select preset radar preset
    cy.findByTestId('menuButton').click();
    cy.findByText('Presets').should('be.visible');
    cy.findByText('Radar view').click();
    cy.findByText('Presets').should('not.exist');

    // open layermanager
    cy.findByTestId('layerManagerButton').click();
    cy.waitOnRadarGetCapabilities();
    cy.findAllByTestId('activateLayerButton').then(($elements) => {
      const listLength = $elements.length;

      // duplicate a layer
      cy.findAllByTestId('openMenuButton').last().click();
      cy.findByText('Duplicate layer').click();
      cy.findAllByTestId('activateLayerButton').should(
        'have.length',
        listLength + 1,
      );

      // open legend
      cy.findByTestId('open-Legend').click();
      cy.findAllByTestId('legend').should('have.length', listLength + 1);

      // remove a layer
      cy.findAllByTestId('deleteButton').last().click();
      cy.findAllByTestId('activateLayerButton').should(
        'have.length',
        listLength,
      );

      cy.findAllByTestId('legend').should('have.length', listLength);

      // close the layermanager
      cy.findByTestId('layerManagerWindow').findByTestId('closeBtn').click();
      cy.findByTestId('layerManagerWindow').should('not.exist');
    });
  });

  it('should drag and drop layers', () => {
    cy.mockExampleConfigNoFeatures();
    cy.mockRadarGetCapabilities();
    cy.visit('/');

    // select preset radar preset
    cy.findByTestId('menuButton').click();
    cy.findByText('Presets').should('be.visible');
    cy.findByText('Radar view').click();
    cy.findByText('Presets').should('not.exist');

    // open layermanager
    cy.findByTestId('layerManagerButton').click();
    cy.waitOnRadarGetCapabilities();
    // Check that Layer Manager is shown
    cy.findByTestId('layerManagerWindow').should('be.visible');
    cy.findAllByTestId('selectOpacity').should('have.length', 3);

    // change the opacity of the first layer so you can check after dragging this layer has moved down
    cy.findAllByTestId('selectOpacity').first().click();
    cy.findByText('50 %').click();
    cy.findAllByTestId('selectOpacity').first().click();
    cy.findAllByTestId('selectOpacity').first().contains('50');
    cy.findAllByTestId('selectOpacity').eq(1).contains('100'); // second
    cy.findAllByTestId('selectOpacity').last().contains('100');

    // Move the second layer up
    cy.get('[data-testid=dragHandle-1]').dragTo('[data-testid=dragHandle-0]');
    cy.findAllByTestId('selectOpacity')
      .first()
      .contains('100', { timeout: 5000 });
    cy.findAllByTestId('selectOpacity').eq(1).contains('50'); // second
    cy.findAllByTestId('selectOpacity').last().contains('100');

    // close the layermanager
    cy.findByTestId('layerManagerWindow').findByTestId('closeBtn').click();
    cy.findByTestId('layerManagerWindow').should('not.exist');
  });

  it('should not show the Layer Select when feature GW_FEATURE_LAYER_SELECT is off', () => {
    cy.mockExampleConfigNoFeatures();
    cy.visit('/');
    cy.findByTestId('layerManagerWindow').should('be.visible');
    cy.findByTestId('layerSelectButton').should('not.exist');
  });

  it('should not show the Map presets when there is no GW_PRESET_BACKEND_URL', () => {
    cy.mockExampleConfigNoFeatures();
    cy.visit('/');
    cy.findByTestId('layerManagerWindow').should('be.visible');
    cy.findByText('New map preset').should('not.exist');
  });

  it('should show the Layer Select when feature GW_FEATURE_LAYER_SELECT is on', () => {
    cy.mockCognitoLoginSuccess();
    cy.findByTestId('layerManagerWindow').should('be.visible');
    cy.findByTestId('layerSelectButton').should('be.visible');
    cy.mockCognitoLogoutSuccess();
  });

  it('should show the Map presets when there is a GW_PRESET_BACKEND_URL', () => {
    cy.mockCognitoLoginSuccess();
    cy.findByTestId('layerManagerWindow').should('be.visible');
    cy.findByText('New map preset').should('be.visible');
    cy.mockCognitoLogoutSuccess();
  });
});
