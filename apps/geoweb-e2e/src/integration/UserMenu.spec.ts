/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

describe('UserMenu', () => {
  it('should show the default username and login button when not logged in', () => {
    cy.mockExampleConfigNoFeatures();
    cy.visit('/');
    cy.findByTestId('userButton').click();
    cy.findByText('Guest').should('be.visible');
    cy.findByTestId('loginButton').should('be.visible');
  });

  it('should only show the default menu items when all features are disabled', () => {
    cy.mockExampleConfigNoFeatures();
    cy.visit('/');
    cy.findByTestId('userButton').click();
    cy.findByText('Guest').should('be.visible');
    cy.findByText('Choose your default app mode').should('be.visible');
    cy.findByTestId('version-menu').should('not.exist');
    cy.findByTestId('cheatsheet-btn').should('be.visible');
    cy.findByText('Feedback').should('not.exist');
    cy.findByText('Info').should('not.exist');
    cy.findByTestId('loginButton').should('be.visible');
  });

  it('should show all default and feature menu items when all features are enabled', () => {
    cy.mockCognitoLoginSuccess();
    cy.findByTestId('userButton').click();
    cy.findByText('max.verstappen').should('be.visible');
    cy.findByText('Choose your default app mode').should('be.visible');
    cy.findByTestId('version-menu').should('be.visible');
    cy.findByTestId('cheatsheet-btn').should('be.visible');
    cy.findByText('Feedback').should('be.visible');
    cy.findByText('Info').should('be.visible');
    cy.findByTestId('loginButton').should('not.exist');
    cy.mockCognitoLogoutSuccess();
  });

  it('should show the username when logged in with cognito ', () => {
    cy.mockCognitoLoginSuccess();
    cy.findByTestId('userButton').click();
    cy.findByText('max.verstappen').should('be.visible');
    cy.findByTestId('loginButton').should('not.exist');
    cy.mockCognitoLogoutSuccess();
  });

  it('should show the users email when logged in with gitlab', () => {
    cy.mockGitlabLoginSuccess();
    cy.findByTestId('userButton').click();
    cy.findByText('neil.armstrong@fakeemail.com').should('be.visible');
    cy.findByTestId('loginButton').should('not.exist');
    cy.findByTestId('userButton').click();
    cy.findByText('Logout').click();
    cy.findByTestId('userButton').should('not.exist');
    cy.url().should('not.contain', 'logout');
    cy.url().should('contain', 'sign_in');
  });

  it('should redirect to login after logout', () => {
    cy.mockCognitoLoginSuccess();
    cy.mockCognitoLogoutSuccess();
    cy.findByTestId('userButton').should('not.exist');
    cy.url().should('not.contain', 'logout');
    cy.url().should('contain', 'login');
  });
});
