/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

describe('geoweb', () => {
  it('should redirect to login when not logged in and GW_FEATURE_FORCE_AUTHENTICATION is on', () => {
    cy.mockExampleConfig();
    cy.visit('/');
    cy.url().should('contain', 'login');
  });

  it('should redirect to homepage when not logged in and GW_FEATURE_FORCE_AUTHENTICATION is off', () => {
    cy.mockExampleConfigNoFeatures();
    cy.visit('/');
    cy.url().should('eq', 'http://localhost:5400/');
    cy.findAllByText('Mocked GeoWeb No Features').should('have.length', 2);
  });

  it('should redirect to the error page when login fails', () => {
    cy.mockLoginFailure();
    cy.url().should('eq', 'http://localhost:5400/error');
  });

  it('should redirect to the error page when going directly to /code', () => {
    cy.mockExampleConfig();
    cy.visit('/code');
    cy.url().should('eq', 'http://localhost:5400/error');
  });

  it('should redirect to homepage after successful cognito login', () => {
    cy.mockCognitoLoginSuccess();
    cy.url().should('eq', 'http://localhost:5400/');
    cy.findAllByText('Mocked GeoWeb Cognito Login').should('have.length', 2);
  });

  it('should redirect to homepage after successful gitlab login', () => {
    cy.mockGitlabLoginSuccess();
    cy.url().should('eq', 'http://localhost:5400/');
    cy.findAllByText('Mocked GeoWeb Gitlab Login').should('have.length', 2);
  });
});
