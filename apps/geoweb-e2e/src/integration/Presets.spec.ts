/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

describe('Presets', () => {
  beforeEach(() => {
    cy.mockRadarGetCapabilities();
    cy.mockPresets();
    cy.mockCognitoLoginSuccess();
  });

  afterEach(() => {
    cy.mockCognitoLogoutSuccess();
  });

  it('should select a preset and save a user preset', () => {
    cy.findByText('New map preset').click();
    cy.wait('@presetlist');
    cy.findByTestId('mappresets-menu').should('be.visible');
    cy.findByText('NL warnings').click();
    cy.findByTestId('mappresets-menu').should('not.exist');
    cy.wait('@preset');
    cy.findByTestId('mappresets-menubutton').should('have.text', 'NL warnings');
    cy.findAllByTestId('activateLayerButton').should('have.length', 2);
    cy.findByText('NL warnings').click();
    cy.findByTestId('toggleMenuButton').click();
    cy.findByText('Save').click();
    cy.wait('@savePreset');
    cy.findByTestId('mappresets-menu').should('not.exist');
  });

  it('should delete a user preset and save a new preset as by pressing Enter', () => {
    cy.findByText('New map preset').click();
    cy.wait('@presetlist');
    cy.findByTestId('mappresets-menu').should('be.visible');
    cy.findByTestId('deleteBtn').click();
    cy.findByText('Delete').click();
    cy.wait('@deletePreset');
    cy.wait('@presetlist');
    cy.findByTestId('mappresets-menu').should('be.visible');
    cy.findByTestId('toggleMenuButton').click();
    cy.findByText('Save as').click();
    cy.findByTestId('mappresets-menu').should('not.exist');
    cy.get('[name="title"]').type('Custom preset');
    cy.get('[name="title"]').should('have.value', 'Custom preset');
    cy.get('[name="title"]').type('{enter}');
    cy.wait('@savePresetAs');
  });
});
