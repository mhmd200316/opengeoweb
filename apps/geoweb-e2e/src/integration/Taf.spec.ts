/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

describe('Taf', () => {
  beforeEach(() => {
    cy.mockCognitoLoginSuccess();
    cy.mockFetchTAFList();
    cy.mockGetTac();
  });

  afterEach(() => {
    cy.mockCognitoLogoutSuccess();
  });

  it('should drag and drop a changegroup', () => {
    // Open the menu
    cy.findByTestId('menuButton').click();
    // Check that Aviation list is shown
    cy.findByText('Aviation').should('be.visible');
    // Open the TAF module
    cy.findByText('TAF').click();
    // Check that menu is closed
    cy.findByText('Aviation').should('not.exist');
    // Check that the TAF module is shown
    cy.get('#tafmodule').should('be.visible');
    cy.wait('@taflist');
    // The first TAF should be active
    cy.findByTestId('location-tabs')
      .get('li')
      .first()
      .should('have.class', 'Mui-selected');
    // Make sure we are in editor mode
    cy.findByTestId('switchMode').should('have.class', 'Mui-checked');
    // Add a new changegroup at the bottom
    cy.get('[data-testid="tafFormOptions[0]"]').click();
    cy.findByText('Insert 1 row below').click();
    cy.get('[name="changeGroups[1].change"]').should('have.value', '');
    // Add data to the first changegroup
    cy.get('[name="changeGroups[0].change"]').type('f');
    cy.get('[name="changeGroups[0].change"]').should('have.value', 'FM');
    // Click publish to trigger validations
    cy.get('[data-testid="publishtaf"]').click();
    // Make sure the correct number of issues is shown
    cy.findByTestId('issuesButton').invoke('text').should('equal', '9 issues');
    // Save the number of issues shown to check later
    cy.findByTestId('issuesButton')
      .invoke('text')
      .then((issuesText) => {
        // Move the new changegroup up
        cy.get('[data-testid=dragHandle-1]').dragTo(
          '[data-testid=dragHandle-0]',
        );
        cy.get('[name="changeGroups[0].change"]').should('have.value', '');
        cy.get('[name="changeGroups[1].change"]').should('have.value', 'FM');
        // Check that the number of issues is still the same
        cy.findByTestId('issuesButton')
          .invoke('text')
          .should('equal', issuesText);
      });
  });

  it('should import a TAF from TAC Overview', () => {
    // Open the menu
    cy.findByTestId('menuButton').click();
    // Check that Aviation list is shown
    cy.findByText('Aviation').should('be.visible');
    // Open the TAF module
    cy.findByText('TAF').click();
    // Check that menu is closed
    cy.findByText('Aviation').should('not.exist');
    // Check that the TAF module is shown
    cy.get('#tafmodule').should('be.visible');
    cy.wait('@taflist');
    // The first TAF should be active
    cy.findByTestId('location-tabs')
      .get('li')
      .first()
      .should('have.class', 'Mui-selected');
    // Make sure we are in editor mode
    cy.findByTestId('switchMode').should('have.class', 'Mui-checked');
    // Import TAF from TAC Overview
    cy.findByText('Current & Expired').click();
    cy.wait('@getTac');
    cy.findByTestId('export-tac-button').click();
    // Form values should be imported
    cy.get('[name="baseForecast.wind"]').should('have.value', '18010');
    cy.get('[name="baseForecast.visibility"]').should('have.value', 'CAVOK');
    // There should be no changegroups
    cy.findByTestId('row-changeGroups[0]').should('not.exist');
    // There should be no validation errors
    cy.findByText('0 issues').should('be.visible');
    // Tac should be updated
    cy.findByText('No Tac Available').should('not.exist');
  });
});
