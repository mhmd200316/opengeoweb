/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

describe('Menu', () => {
  it('should add the HARM view preset and load layers', () => {
    cy.mockExampleConfigNoFeatures();
    cy.visit('/');
    // Open the menu
    cy.findByTestId('menuButton').click();
    // Check that Presets is shown
    cy.findByText('Presets').should('be.visible');
    // Add the Current Weather preset
    cy.findByText('HARM').click();
    // Check that menu is closed
    cy.findByText('Presets').should('not.exist');
    // Open legend
    cy.findByTestId('open-Legend').click();
    // Check that there are layers loaded
    cy.findAllByTestId('legend').should('have.length', '8');
  });

  it('should not show menu items for features that are not active', () => {
    cy.mockExampleConfigNoFeatures();
    cy.visit('/');
    // Open the menu
    cy.findByTestId('menuButton').click();
    // Check that Presets is shown
    cy.findByText('Presets').should('be.visible');
    cy.findByText('Space Weather Forecast').should('not.exist');
    cy.findByText('AIRMET').should('not.exist');
    cy.findByText('SIGMET').should('not.exist');
    cy.findByText('TAF').should('not.exist');
    cy.findByText('Aviation').should('not.exist');
    cy.findByText('Satcomp presets').should('not.exist');
  });

  it('should show the Satcomp presets when GW_FEATURE_MENU_SATCOMP is on', () => {
    cy.mockCognitoLoginSuccess();
    cy.findByTestId('menuButton').click();
    // Close the presets list so Satcomp presets come into view
    cy.findByText('Presets').click();
    cy.findByText('Satcomp presets').should('be.visible');
    // Clicking one of the presets should close the menu
    cy.findByText('Satcomp general 3h').click();
    cy.findByText('Satcomp presets').should('not.exist');
    cy.mockCognitoLogoutSuccess();
  });

  it('should open and close the Sigmet dialog', () => {
    cy.mockCognitoLoginSuccess();
    // Open the menu
    cy.findByTestId('menuButton').click();
    // Check that Aviation list is shown
    cy.findByText('Aviation').should('be.visible');
    // Open the Sigmet
    cy.findByText('SIGMET').click();
    // Check that menu is closed
    cy.findByText('Presets').should('not.exist');
    // Check that the Sigmet dialog is shown
    cy.findByTestId('SigmetModule').should('be.visible');
    // Close the dialog
    cy.findByTestId('SigmetModule').findByTestId('closeBtn').click();
    // Check that the Sigmet dialog is closed
    cy.findByTestId('SigmetModule').should('not.exist');
    cy.mockCognitoLogoutSuccess();
  });

  it('should open the TAF module', () => {
    cy.mockCognitoLoginSuccess();
    cy.mockFetchTAFList();
    // Open the menu
    cy.findByTestId('menuButton').click();
    // Check that Aviation list is shown
    cy.findByText('Aviation').should('be.visible');
    // Open the TAF module
    cy.findByText('TAF').click();
    // Check that menu is closed
    cy.findByText('Presets').should('not.exist');
    // Check that the TAF module is shown
    cy.wait('@taflist');
    cy.get('#tafmodule').should('be.visible');
    cy.mockCognitoLogoutSuccess();
  });

  it('should open and close the AIRMET dialog', () => {
    cy.mockCognitoLoginSuccess();
    // Open the menu
    cy.findByTestId('menuButton').click();
    // Check that Aviation list is shown
    cy.findByText('Aviation').should('be.visible');
    // Open the AIRMET dialog
    cy.findByText('AIRMET').click();
    // Check that menu is closed
    cy.findByText('Presets').should('not.exist');
    // Check that the AIRMET dialog is shown
    cy.findByTestId('AirmetModule').should('be.visible');
    // Close the dialog
    cy.findByTestId('AirmetModule').findByTestId('closeBtn').click();
    // Check that the AIRMET dialog is closed
    cy.findByTestId('AirmetModule').should('not.exist');
    cy.mockCognitoLogoutSuccess();
  });

  it('should open and close the Space Weather Forecast dialog', () => {
    cy.mockCognitoLoginSuccess();
    // Open the menu
    cy.findByTestId('menuButton').click();
    // Open the Space Weather Forecast
    cy.findByText('Space Weather Forecast').click();
    // Check that menu is closed
    cy.findByText('Presets').should('not.exist');
    // Check that the Space Weather dialog is shown
    cy.findByTestId('SpaceWeatherModule').should('be.visible');
    // Close the dialog
    cy.findByTestId('SpaceWeatherModule').findByTestId('closeBtn').click();
    // Check that the Space Weather dialog is closed
    cy.findByTestId('SpaceWeatherModule').should('not.exist');
    cy.mockCognitoLogoutSuccess();
  });

  it('should show the last opened module on top', () => {
    cy.mockCognitoLoginSuccess();
    cy.findByTestId('menuButton').click();
    cy.findByText('AIRMET').click();
    cy.findByTestId('AirmetModule').should('be.visible');
    cy.findByTestId('menuButton').click();
    cy.findByText('Space Weather Forecast').click();
    cy.findByTestId('SpaceWeatherModule').should('be.visible');
    cy.findByTestId('AirmetModule').should('exist');
    // make sure SpaceWeatherModule is on top by checking that AirmetModule is not clickable
    cy.once('fail', (err) => {
      expect(err.message).contains('failed because this element');
      expect(err.message).contains('is being covered by another element');
      return false;
    });
    cy.findByTestId('AirmetModule')
      .findByTestId('closeBtn')
      .click({ timeout: 500 });
    cy.mockCognitoLogoutSuccess();
  });
});
