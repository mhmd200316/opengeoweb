/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { coreModuleConfig, SnackbarWrapperConnect } from '@opengeoweb/core';
import { DynamicModuleLoader } from 'redux-dynamic-modules';

import './app.css';

import { AlertBanner } from '@opengeoweb/shared';
import {
  AuthenticationProvider,
  getAuthConfig,
  AuthenticationConfig,
  Login,
  Logout,
  Code,
  getSessionStorageProvider,
  getRandomString,
  getCodeChallenge,
  PrivateRoute,
} from '@opengeoweb/authentication';
import { screenManagerModuleModuleConfig } from '@opengeoweb/screenmanager';

import { ThemeWrapper } from '@opengeoweb/theme';
import { timeSeriesModuleConfig } from '@opengeoweb/timeseries';
import { presetsModuleConfig } from '@opengeoweb/presets';
import { isArray } from 'lodash';
import HomePage from './pages/Home';
import ErrorPage from './pages/Error';
import NotFound from './pages/404';
import {
  useConfig,
  isValidConfig,
  isValidConfigWithAuthentication,
  ConfigType,
  sortErrors,
  ValidationError,
} from './utils/loadConfig';
import { store } from './store';

interface RoutesProps {
  forceAuth?: boolean;
  config?: ConfigType;
}

const Routes: React.FC<RoutesProps> = ({
  forceAuth = false,
  config,
}: RoutesProps) => {
  return (
    <Router>
      <Switch>
        {forceAuth ? (
          <PrivateRoute exact path="/">
            <HomePage config={config} />
          </PrivateRoute>
        ) : (
          <Route exact path="/">
            <HomePage config={config} />
          </Route>
        )}
        <Route exact path="/login" component={Login} />
        <Route exact path="/logout" component={Logout} />
        <Route exact path="/code" component={Code} />
        <Route exact path="/error" component={ErrorPage} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
};

const App: React.FC = () => {
  const [configObject] = useConfig();
  document.title = configObject?.GW_FEATURE_APP_TITLE || 'GeoWeb';

  React.useEffect(() => {
    const sessionStorageProvider = getSessionStorageProvider();

    if (!sessionStorageProvider.getOauthState()) {
      const oauthState = getRandomString();
      sessionStorageProvider.setOauthState(oauthState);
    }

    if (
      !sessionStorageProvider.getOauthCodeVerifier() ||
      !sessionStorageProvider.getOauthCodeChallenge()
    ) {
      const codeVerifier = getRandomString();
      getCodeChallenge(codeVerifier).then((codeChallenge) => {
        // setting both codeChallenge and codeVerifier at the same time since they are related
        sessionStorageProvider.setOauthCodeVerifier(codeVerifier);
        sessionStorageProvider.setOauthCodeChallenge(codeChallenge);
      });
    }
  }, []);

  if (!configObject) {
    return null; // configuration is still loading, not showing anything to avoid flashing
  }

  // validates config.json. If there are any empty or non existing keys, it will show an error
  const validateRequiredFields = isValidConfig(configObject);
  if (validateRequiredFields !== true && isArray(validateRequiredFields)) {
    const errors = sortErrors(validateRequiredFields);

    return (
      <ThemeWrapper>
        <AlertBanner
          severity="error"
          title="Configuration (config.json) is not valid and has the following errors:"
          info={
            <>
              {Object.keys(errors).map((key) => (
                <p key={key}>
                  <strong>{key}: </strong>
                  {errors[key]
                    .map((error: ValidationError) => error.key)
                    .join(', ')}
                </p>
              ))}
            </>
          }
        />
      </ThemeWrapper>
    );
  }

  // validates config.json for valid auth keys. If it's not valid, it will show the default application without errors
  if (isValidConfigWithAuthentication(configObject) !== true) {
    return (
      <ThemeWrapper>
        <Provider store={store}>
          <DynamicModuleLoader
            modules={[
              ...coreModuleConfig,
              screenManagerModuleModuleConfig,
              timeSeriesModuleConfig,
              presetsModuleConfig,
            ]}
          >
            <SnackbarWrapperConnect>
              <Routes config={configObject} />
            </SnackbarWrapperConnect>
          </DynamicModuleLoader>
        </Provider>
      </ThemeWrapper>
    );
  }

  // renders the validated homepage
  return (
    <AuthenticationProvider
      configURLS={getAuthConfig(configObject as AuthenticationConfig)}
    >
      <ThemeWrapper>
        <Provider store={store}>
          <DynamicModuleLoader
            modules={[
              ...coreModuleConfig,
              screenManagerModuleModuleConfig,
              timeSeriesModuleConfig,
              presetsModuleConfig,
            ]}
          >
            <SnackbarWrapperConnect>
              <Routes
                config={configObject}
                forceAuth={configObject?.GW_FEATURE_FORCE_AUTHENTICATION}
              />
            </SnackbarWrapperConnect>
          </DynamicModuleLoader>
        </Provider>
      </ThemeWrapper>
    </AuthenticationProvider>
  );
};

export default App;
