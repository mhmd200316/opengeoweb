/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import { render } from '@testing-library/react';

import { TestWrapper } from '../utils/mockdata';
import Header from './Header';
import { ConfigType } from '../utils/loadConfig';

describe('components/Header', () => {
  it('should have the configured header text', async () => {
    const titleText = 'Test title';
    const mockStore = configureStore();
    const store = mockStore();
    const { findByText } = render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Header
              showSpaceWeather={jest.fn()}
              showSigmet={jest.fn()}
              showAirmet={jest.fn()}
              config={
                {
                  GW_PRESET_BASE_URL: '',
                  GW_GITLAB_PRESETS_API_PATH: '',
                  GW_GITLAB_PROJECT_ID: '',
                  GW_GITLAB_PRESETS_PATH: '',
                  GW_GITLAB_BRANCH: '',
                  GW_MENU_PRESETS: '',
                  GW_FEATURE_APP_TITLE: titleText,
                } as unknown as ConfigType
              }
            />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );

    expect(await findByText(titleText)).toBeTruthy();
  });

  it('should have the the default header', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    const { findByText } = render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Header
              showSpaceWeather={jest.fn()}
              showSigmet={jest.fn()}
              showAirmet={jest.fn()}
              config={
                {
                  GW_PRESET_BASE_URL: '',
                  GW_GITLAB_PRESETS_API_PATH: '',
                  GW_GITLAB_PROJECT_ID: '',
                  GW_GITLAB_PRESETS_PATH: '',
                  GW_GITLAB_BRANCH: '',
                  GW_MENU_PRESETS: '',
                } as unknown as ConfigType
              }
            />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );

    expect(await findByText('GeoWeb')).toBeTruthy();
  });
});
