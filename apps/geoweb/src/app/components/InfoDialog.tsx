/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Box,
  Typography,
  MenuItem,
  Link,
} from '@mui/material';

export const InfoDialog: React.FC = () => {
  const [open, setOpen] = React.useState(false);

  const handleToggleDialog = (): void => {
    setOpen(!open);
  };

  return (
    <>
      <MenuItem onClick={handleToggleDialog} divider>
        Info
      </MenuItem>
      <Dialog
        fullWidth
        maxWidth="sm"
        open={open}
        onClose={handleToggleDialog}
        aria-labelledby="form-dialog-title"
        style={{ zIndex: 10000 }}
      >
        <DialogTitle id="form-dialog-title">GeoWeb</DialogTitle>
        <DialogContent>
          <DialogContentText>
            A demonstration on how to use GeoWeb-Core inside of your application
          </DialogContentText>
          <Box>
            <Typography variant="body1">
              The modules of the GeoWeb-Core have been imported as a dependency.
              <br />
              The following modules are used:
              <br />
              - TimeSlider
              <br />
              - MultiDimensionSelect
              <br />
              - Legend
              <br />
              - LayerManager
              <br />
              Useful links:
              <br />-
              <Link
                href="https://www.npmjs.com/package/@opengeoweb/core"
                target="_blank"
              >
                npm package
              </Link>
              <br />-
              <Link
                href="https://gitlab.com/opengeoweb/opengeoweb"
                target="_blank"
              >
                Nx workspace for geoweb products
              </Link>
              <br />-
              <Link
                href="https://opengeoweb.gitlab.io/opengeoweb/docs/core/"
                target="_blank"
              >
                API documentation
              </Link>
            </Typography>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleToggleDialog} color="secondary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};
