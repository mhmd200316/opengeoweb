/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';

import ReleaseNotes from './ReleaseNotes';

describe('ReleaseNotes', () => {
  it('should show the version', async () => {
    const props = {
      beVersion: '1.0.1',
      handleClose: jest.fn(),
    };
    const { getByText } = render(<ReleaseNotes {...props} />);

    expect(getByText(`v${props.beVersion} has been released!`)).toBeTruthy();
  });
  it('should close the dialog', async () => {
    const props = {
      beVersion: '1.0.1',
      handleClose: jest.fn(),
    };
    const { getByTestId } = render(<ReleaseNotes {...props} />);

    fireEvent.click(getByTestId('closeReleaseNotes'));
    expect(props.handleClose).toHaveBeenCalled();
  });
  it('should open the release notes page in a new tab', async () => {
    const storedWindowOpen = window.open;
    // mock window.open
    delete window.open;
    window.open = jest.fn();

    const props = {
      beVersion: '1.0.1',
      handleClose: jest.fn(),
    };
    const { getByTestId } = render(<ReleaseNotes {...props} />);

    fireEvent.click(getByTestId('openConfluence'));
    await waitFor(() =>
      expect(window.open).toHaveBeenCalledWith(
        'http://confluence.knmi.nl/display/GW/Release+notes',
        '_blank',
      ),
    );
    window.open = storedWindowOpen;
  });
});
