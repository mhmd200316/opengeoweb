/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  Layout,
  Bulletin,
  Notifications,
  TimeSeries,
  NotificationTrigger,
  NotificationTriggerProvider,
  createApi,
} from '@opengeoweb/spaceweather';
import { ApiProvider } from '@opengeoweb/api';
import {
  ErrorBoundary,
  ToolContainerDraggable,
  calculateDialogSizeAndPosition,
} from '@opengeoweb/shared';
import { useAuthenticationContext } from '@opengeoweb/authentication';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import { ConfigType } from '../utils/loadConfig';

interface SpaceWeatherProps {
  handleClose: () => void;
  order?: number;
  config?: ConfigType;
}

const SpaceWeather: React.FC<SpaceWeatherProps> = ({
  handleClose,
  order,
  config,
}: SpaceWeatherProps) => {
  const { auth, onSetAuth } = useAuthenticationContext();
  const { width, height, top, left } = calculateDialogSizeAndPosition();

  return config ? (
    <ToolContainerDraggable
      onClose={handleClose}
      title="Space Weather Forecast"
      startPosition={{ top, left }}
      startSize={{ width, height }}
      minWidth={1150}
      minHeight={300}
      order={order}
      data-testid="SpaceWeatherModule"
    >
      <ApiProvider
        baseURL={config.GW_SW_BASE_URL}
        appURL={config.GW_APP_URL}
        auth={auth}
        onSetAuth={onSetAuth}
        createApi={createApi}
        authTokenUrl={config.GW_AUTH_TOKEN_URL}
        authClientId={config.GW_AUTH_CLIENT_ID}
      >
        <NotificationTriggerProvider>
          <NotificationTrigger />
          <ErrorBoundary>
            <Layout
              leftTop={<Bulletin />}
              leftBottom={<Notifications />}
              right={<TimeSeries />}
            />
          </ErrorBoundary>
        </NotificationTriggerProvider>
      </ApiProvider>
    </ToolContainerDraggable>
  ) : null;
};

const SpaceWeatherModule: React.FC<SpaceWeatherProps> = ({
  ...props
}: SpaceWeatherProps) => (
  <ThemeWrapperOldTheme>
    <SpaceWeather {...props} />
  </ThemeWrapperOldTheme>
);

export default SpaceWeatherModule;
