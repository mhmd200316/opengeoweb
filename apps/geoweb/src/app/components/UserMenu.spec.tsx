/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';

import UserMenu from './UserMenu';
import { TestWrapper } from '../utils/mockdata';
import * as api from '../utils/api';
import { ConfigType } from '../utils/loadConfig';
import { createFakeApi } from '../utils/api';

describe('/components/UserMenu', () => {
  const testConfig: ConfigType = {
    GW_GITLAB_PRESETS_BASE_URL: '',
    GW_GITLAB_PRESETS_API_PATH: '',
    GW_GITLAB_PROJECT_ID: '',
    GW_GITLAB_PRESETS_PATH: '',
    GW_GITLAB_BRANCH: '',
    GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
    GW_SCREEN_PRESETS_FILENAME: 'screenPresets.json',
    GW_FEATURE_APP_TITLE: 'TestTitle',
    GW_FEATURE_FORCE_AUTHENTICATION: false,
    GW_FEATURE_MODULE_SPACE_WEATHER: false,
    GW_FEATURE_MODULE_TAF: false,
    GW_FEATURE_MODULE_SIGMET: false,
    GW_FEATURE_MODULE_AIRMET: false,
    GW_FEATURE_MENU_SATCOMP: false,
    GW_FEATURE_MENU_FEEDBACK: true,
    GW_FEATURE_MENU_INFO: true,
    GW_FEATURE_MENU_VERSION: true,
  };

  it('should show the BE version info', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    const { getByTestId } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('userButton'));
    await waitFor(() => {
      expect(getByTestId('version-menu').textContent).toContain(
        'version 1.0.1',
      );
    });
  });

  it('should show the user info', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    const { getByTestId, queryByTestId, getByText } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('userButton'));
    // Wait for the version to have loaded
    await waitFor(() => {
      expect(getByTestId('version-menu').textContent).toContain(
        'version 1.0.1',
      );
    });

    expect(queryByTestId('userMenu')).toBeTruthy();
    expect(getByText(props.userName)).toBeTruthy();
    expect(getByText(props.userRole)).toBeTruthy();
  });

  it('should show the info dialog', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    const { getByTestId, findByText, getByText } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('userButton'));
    fireEvent.click(await findByText('Info'));
    await waitFor(() => {
      expect(
        getByText(
          'A demonstration on how to use GeoWeb-Core inside of your application',
        ),
      ).toBeTruthy();
    });
  });

  it('should toggle the menu', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    expect(queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(getByTestId('userButton'));
    // Wait for the version to have loaded
    await waitFor(() => {
      expect(getByTestId('version-menu').textContent).toContain(
        'version 1.0.1',
      );
    });

    expect(queryByTestId('userMenu')).toBeTruthy();
    fireEvent.click(getByTestId('userButton'));
    expect(queryByTestId('userMenu')).toBeFalsy();
  });

  it('should show the loading message when BE version is still loading', async () => {
    jest.spyOn(api, 'createFakeApi').mockReturnValueOnce({
      ...createFakeApi(),
      getBeVersion: (): Promise<{ data: { version: string } }> =>
        Promise.resolve({ data: null }),
    });
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    const { getByTestId } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('userButton'));
    await waitFor(() => {
      expect(getByTestId('version-menu').textContent).toContain('Loading...');
    });
  });

  it('should change theming', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    const { getByTestId, findByText } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('userButton'));

    const lightElement = await findByText('Light');
    const darkElement = await findByText('Dark');
    expect(lightElement.parentElement.querySelector('input').checked).toEqual(
      true,
    );
    expect(darkElement.parentElement.querySelector('input').checked).toEqual(
      false,
    );

    // toggle
    await waitFor(() => {
      fireEvent.click(darkElement.parentElement.querySelector('input'));
      expect(lightElement.parentElement.querySelector('input').checked).toEqual(
        false,
      );
      expect(darkElement.parentElement.querySelector('input').checked).toEqual(
        true,
      );
    });
  });

  it('should show the error message when BE version failed to load', async () => {
    jest.spyOn(api, 'createFakeApi').mockReturnValueOnce({
      ...createFakeApi(),
      getBeVersion: (): Promise<{ data: { version: string } }> =>
        Promise.reject(new Error('version error')),
    });

    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    const { getByTestId } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('userButton'));
    await waitFor(() => {
      expect(getByTestId('version-menu').textContent).toContain(
        'Loading failed: version error',
      );
    });
  });

  it('should load version and open the feedback page in a new tab', async () => {
    const storedWindowOpen = window.open;
    delete window.open;
    window.open = jest.fn();

    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    const { getByTestId } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('userButton'));
    // Wait for the version to have loaded
    await waitFor(() => {
      expect(getByTestId('version-menu').textContent).toContain(
        'version 1.0.1',
      );
    });

    fireEvent.click(getByTestId('open-feedback'));
    await waitFor(() =>
      expect(window.open).toHaveBeenCalledWith(
        'http://confluence.knmi.nl/display/GW/Feedback',
        '_blank',
      ),
    );
    window.open = storedWindowOpen;
  });

  it('should open and close the release notes', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    expect(queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(getByTestId('userButton'));
    // Wait for the version to have loaded
    await waitFor(() => {
      expect(getByTestId('version-menu').textContent).toContain(
        'version 1.0.1',
      );
    });

    expect(queryByTestId('userMenu')).toBeTruthy();
    fireEvent.click(getByTestId('version-menu'));
    expect(queryByTestId('userMenu')).toBeFalsy();
    expect(queryByTestId('releaseNotes')).toBeTruthy();
    fireEvent.click(getByTestId('closeReleaseNotes'));
    expect(queryByTestId('releaseNotes')).toBeFalsy();
    expect(queryByTestId('userMenu')).toBeFalsy();
  });

  it('should show logout option when authentication configured and user is logged in', async () => {
    const props = {
      userName: 'daan.storm@gmail.com',
      userRole: 'Meteorologist',
      isAuthConfigured: true,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    expect(queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(getByTestId('userButton'));
    await waitFor(() => {
      expect(queryByTestId('userMenu')).toBeTruthy();
      expect(queryByTestId('logoutButton')).toBeTruthy();
    });
  });

  it('should show login option when authentication is configured and user is not logged in', async () => {
    const props = {
      userName: 'Guest',
      isAuthConfigured: true,
    };
    const { getByTestId, findByTestId } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    fireEvent.click(getByTestId('userButton'));
    expect(await findByTestId('userMenu')).toBeTruthy();
    expect(await findByTestId('loginButton')).toBeTruthy();
  });

  it('should not show logout/login option when no authentication configured', async () => {
    const props = {
      userName: 'daan.storm@gmail.com',
      userRole: 'Meteorologist',
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <UserMenu {...props} />
      </TestWrapper>,
    );

    expect(queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(getByTestId('userButton'));
    await waitFor(() => {
      expect(queryByTestId('userMenu')).toBeTruthy();
      expect(queryByTestId('logoutButton')).toBeFalsy();
      expect(queryByTestId('loginButton')).toBeFalsy();
    });
  });

  it('should handle logout', async () => {
    const history = createMemoryHistory();

    const props = {
      userName: 'daan.storm@gmail.com',
      userRole: 'Meteorologist',
      isAuthConfigured: true,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <Router history={history}>
          <UserMenu {...props} />
        </Router>
      </TestWrapper>,
    );

    expect(queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(getByTestId('userButton'));
    await waitFor(() => {
      expect(queryByTestId('userMenu')).toBeTruthy();
      expect(queryByTestId('logoutButton')).toBeTruthy();
    });
    fireEvent.click(getByTestId('logoutButton'));

    expect(history.location.pathname).toBe('/logout');
  });

  it('should handle login', async () => {
    const history = createMemoryHistory();

    const props = {
      userName: 'Guest',
      isAuthConfigured: true,
    };
    const { getByTestId, queryByTestId } = render(
      <TestWrapper>
        <Router history={history}>
          <UserMenu {...props} />
        </Router>
      </TestWrapper>,
    );

    expect(queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(getByTestId('userButton'));
    await waitFor(() => {
      expect(queryByTestId('userMenu')).toBeTruthy();
      expect(queryByTestId('loginButton')).toBeTruthy();
    });
    fireEvent.click(getByTestId('loginButton'));

    expect(history.location.pathname).toBe('/login');
  });
});
