/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { IconButton, Paper, Typography, Button, Box } from '@mui/material';
import { Close } from '@opengeoweb/theme';

const styles = {
  paper: {
    position: 'absolute',
    top: '60px',
    right: '0',
    marginTop: '12px',
    marginRight: '24px',
    boxShadow:
      '0 3px 5px 0 rgba(0, 0, 0, 0.2), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 6px 10px 0 rgba(0, 0, 0, 0.14)',
    width: '300px',
    display: 'flex',
    flexDirection: 'column',
  },
  close: {
    marginTop: '8px',
    marginRight: '8px',
    alignSelf: 'flex-end',
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    padding: '0 24px 32px 24px',
  },
  message: {
    marginTop: '16px',
    marginBottom: '16px',
    lineHeight: 1.75,
  },
};

interface ReleaseNotesProps {
  beVersion: string;
  handleClose: () => void;
}

const ReleaseNotes: React.FC<ReleaseNotesProps> = ({
  beVersion,
  handleClose,
}: ReleaseNotesProps) => {
  return (
    <Paper sx={styles.paper} data-testid="releaseNotes">
      <IconButton
        data-testid="closeReleaseNotes"
        aria-label="close"
        onClick={(): void => handleClose()}
        size="small"
        sx={styles.close}
      >
        <Close />
      </IconButton>
      <Box sx={styles.content}>
        <Typography variant="h5">v{beVersion} has been released!</Typography>
        <Typography variant="caption" sx={styles.message}>
          We&apos;re constantly working to improve your GeoWeb experience. Here
          you can find a summary of what has changed:
        </Typography>

        <Button
          variant="contained"
          color="secondary"
          data-testid="openConfluence"
          onClick={(): void => {
            window.open(
              'http://confluence.knmi.nl/display/GW/Release+notes',
              '_blank',
            );
          }}
        >
          View full release notes
        </Button>
      </Box>
    </Paper>
  );
};

export default ReleaseNotes;
