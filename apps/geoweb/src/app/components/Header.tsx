/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { useAuthenticationContext } from '@opengeoweb/authentication';
import { AppHeader } from '@opengeoweb/shared';
import { ApiProvider } from '@opengeoweb/api';
import Menu from './Menu';
import UserMenu from './UserMenu';
import { ConfigType } from '../utils/loadConfig';
import { createApi } from '../utils/api';

interface HeaderProps {
  showSpaceWeather: () => void;
  showSigmet: () => void;
  showAirmet: () => void;
  config?: ConfigType;
}

const Header: React.FC<HeaderProps> = ({
  showSpaceWeather,
  showSigmet,
  showAirmet,
  config,
}: HeaderProps) => {
  const { auth, onSetAuth, authConfig } = useAuthenticationContext();

  return (
    <AppHeader
      title={config?.GW_FEATURE_APP_TITLE || 'GeoWeb'}
      menu={
        <Menu
          showSpaceWeather={showSpaceWeather}
          showSigmet={showSigmet}
          showAirmet={showAirmet}
          config={config}
        />
      }
      userMenu={
        authConfig ? (
          <ApiProvider
            baseURL={authConfig.GW_INFRA_BASE_URL}
            appURL={authConfig.GW_APP_URL}
            auth={auth}
            onSetAuth={onSetAuth}
            createApi={createApi}
            authTokenUrl={authConfig.GW_AUTH_TOKEN_URL}
            authClientId={authConfig.GW_AUTH_CLIENT_ID}
          >
            <UserMenu
              userName={auth && auth.username ? auth.username : 'Guest'}
              isAuthConfigured={true}
              config={config}
            />
          </ApiProvider>
        ) : (
          <UserMenu userName="Guest" />
        )
      }
    />
  );
};

export default Header;
