/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  HamburgerMenu,
  ChevronUp,
  ChevronDown,
  PresetWorkspace,
  Preset,
  PresetTiles,
} from '@opengeoweb/theme';
import { useApiContext, useApi } from '@opengeoweb/api';
import {
  List,
  ListItem,
  ListItemText,
  Drawer,
  IconButton,
  ListItemIcon,
  Collapse,
  Theme,
  SxProps,
  Box,
} from '@mui/material';
import {
  actions as screenManagerActions,
  selectors as screenManagerSelectors,
  AppStore,
  createBalancedMosaicNode,
  ScreenManagerPreset,
  getScreenPresets,
} from '@opengeoweb/screenmanager';
import { uiActions } from '@opengeoweb/core';
import { tafScreenPreset } from '@opengeoweb/taf';
import { ConfigType } from '../utils/loadConfig';
import defaultScreenPresets from '../../assets/screenPresets.json';
import defaultScreenPresetsSatcomp from '../../assets/screenPresetsSatcomp.json';
import { AppApi } from '../utils/api';

const styles = {
  paper: {
    '& .MuiDrawer-paper': {
      top: '40px',
    },
  },
  drawer: {
    width: '250px',
    height: 'calc(100% -  40px)',
    overflowY: 'scroll',
  },
  nested: (theme: Theme): SxProps<Theme> => ({
    paddingLeft: theme.spacing(4),
  }),
  listItemPrimary: {
    '& .MuiListItemText-primary': {
      fontWeight: '500',
    },
  },
  button: {
    padding: 0,
  },
};

const getPresetIcon = (preset: ScreenManagerPreset): React.ReactElement => {
  switch (preset.viewType) {
    case 'tiledWindow':
      return <PresetTiles />;
    case 'multiWindow':
      return <PresetWorkspace />;
    case 'singleWindow':
    default:
      return <Preset />;
  }
};

interface PresetMenuItemProps {
  presetItemTitle: string;
  itemIcon: React.ReactElement;
  onClickPreset: () => void;
}

export const PresetMenuItem: React.FC<PresetMenuItemProps> = ({
  presetItemTitle,
  itemIcon,
  onClickPreset,
}: PresetMenuItemProps) => {
  return (
    <ListItem
      button
      dense
      sx={styles.nested as SxProps<Theme>}
      onClick={(): void => {
        onClickPreset();
      }}
      data-testid="presetsListItem"
    >
      <ListItemIcon>{itemIcon}</ListItemIcon>
      <ListItemText primary={presetItemTitle} />
    </ListItem>
  );
};

interface PresetMenuProps {
  presets: ScreenManagerPreset[];
  setScreenPreset: (preset: ScreenManagerPreset) => void;
  name: string;
}

export const PresetMenu: React.FC<PresetMenuProps> = ({
  presets,
  setScreenPreset,
  name,
}: PresetMenuProps) => {
  const [openPresets, setOpenPresets] = React.useState(true);

  return (
    <>
      <ListItem
        button
        data-testid="presetsListButton"
        onClick={(event: React.MouseEvent): void => {
          event.stopPropagation();
          setOpenPresets(!openPresets);
        }}
      >
        <ListItemText sx={styles.listItemPrimary} primary={name} />
        {openPresets ? <ChevronUp /> : <ChevronDown />}
      </ListItem>
      <Collapse in={openPresets} timeout="auto" unmountOnExit>
        <List component="div" disablePadding data-testid={`${name}-list`}>
          {presets && presets.length > 0 ? (
            presets.map((preset) => (
              <PresetMenuItem
                presetItemTitle={preset.title}
                key={preset.id}
                itemIcon={getPresetIcon(preset)}
                onClickPreset={(): void => {
                  setScreenPreset(preset);
                }}
              />
            ))
          ) : (
            <ListItem dense sx={styles.nested as SxProps<Theme>}>
              <ListItemText primary={`${name} not available`} />
            </ListItem>
          )}
        </List>
      </Collapse>
    </>
  );
};

export interface MenuProps {
  showSpaceWeather: () => void;
  showSigmet: () => void;
  showAirmet: () => void;
  config?: ConfigType;
}

const Menu: React.FC<MenuProps> = ({
  showSpaceWeather,
  showSigmet,
  showAirmet,
  config,
}: MenuProps) => {
  const dispatch = useDispatch();
  const { api } = useApiContext<AppApi>();
  const { result } = useApi(api.getScreenPresets, config);
  const presets = getScreenPresets(
    result,
    defaultScreenPresets as ScreenManagerPreset[],
  );
  const screenPresetsSatcomp = getScreenPresets(
    null,
    defaultScreenPresetsSatcomp as ScreenManagerPreset[],
    false,
  );
  const [openDrawer, toggleOpenDrawer] = React.useState(false);
  const [openAviation, setOpenAviation] = React.useState(true);

  const showAviationMenu =
    config?.GW_FEATURE_MODULE_AIRMET ||
    config?.GW_FEATURE_MODULE_SIGMET ||
    config?.GW_FEATURE_MODULE_TAF;

  const toggleDrawer =
    (_open: boolean) =>
    (event): void => {
      if (
        event.type === 'keydown' &&
        (event.key === 'Tab' || event.key === 'Shift')
      ) {
        return;
      }

      toggleOpenDrawer(_open);
    };

  const setScreenPreset = useCallback(
    (newScreenPreset: ScreenManagerPreset): void => {
      dispatch(
        screenManagerActions.changePreset({
          screenManagerPreset: newScreenPreset,
        }),
      );
    },
    [dispatch],
  );

  const mosaicNode = useSelector((store: AppStore) =>
    screenManagerSelectors.getMosaicNode(store),
  );

  const autoArrange = (): void => {
    dispatch(
      screenManagerActions.updateScreenManagerViews({
        mosaicNode: createBalancedMosaicNode(mosaicNode),
      }),
    );
  };

  const openSyncgroupsDialog = React.useCallback(() => {
    dispatch(
      uiActions.setToggleOpenDialog({ type: 'syncGroups', setOpen: true }),
    );
  }, [dispatch]);

  return (
    <div>
      <IconButton
        sx={styles.button}
        data-testid="menuButton"
        onClick={toggleDrawer(true)}
        size="large"
      >
        <HamburgerMenu style={{ color: 'white' }} />
      </IconButton>
      <Drawer
        anchor="left"
        open={openDrawer}
        onClose={toggleDrawer(false)}
        sx={styles.paper}
      >
        <Box
          role="presentation"
          onClick={toggleDrawer(false)}
          onKeyDown={toggleDrawer(false)}
          sx={styles.drawer}
        >
          <List>
            {/* Auto arrange windows */}
            <ListItem
              button
              onClick={(): void => {
                autoArrange();
              }}
            >
              <ListItemText
                sx={styles.listItemPrimary}
                primary="Auto Arrange Windows"
              />
            </ListItem>
            {/* Syn Groups */}
            <ListItem
              data-testid="syncGroupsButton"
              button
              onClick={(): void => {
                openSyncgroupsDialog();
              }}
            >
              <ListItemText sx={styles.listItemPrimary} primary="Sync Groups" />
            </ListItem>
            {
              /* Space Weather */
              config?.GW_FEATURE_MODULE_SPACE_WEATHER && (
                <ListItem
                  button
                  onClick={(): void => {
                    showSpaceWeather();
                  }}
                >
                  <ListItemText
                    sx={styles.listItemPrimary}
                    primary="Space Weather Forecast"
                  />
                </ListItem>
              )
            }
            {
              /* Aviation Menu */
              showAviationMenu && (
                <ListItem
                  button
                  onClick={(event: React.MouseEvent): void => {
                    event.stopPropagation();
                    setOpenAviation(!openAviation);
                  }}
                >
                  <ListItemText
                    sx={styles.listItemPrimary}
                    primary="Aviation"
                  />
                  {openAviation ? <ChevronUp /> : <ChevronDown />}
                </ListItem>
              )
            }
            {showAviationMenu && (
              <Collapse in={openAviation} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  {config?.GW_FEATURE_MODULE_TAF && (
                    <ListItem
                      button
                      dense
                      sx={styles.nested as SxProps<Theme>}
                      onClick={(): void => {
                        setScreenPreset(tafScreenPreset);
                      }}
                    >
                      <ListItemText primary="TAF" />
                    </ListItem>
                  )}
                  {config?.GW_FEATURE_MODULE_SIGMET && (
                    <ListItem
                      button
                      dense
                      sx={styles.nested as SxProps<Theme>}
                      onClick={(): void => {
                        showSigmet();
                      }}
                    >
                      <ListItemText primary="SIGMET" />
                    </ListItem>
                  )}
                  {config?.GW_FEATURE_MODULE_AIRMET && (
                    <ListItem
                      button
                      dense
                      sx={styles.nested as SxProps<Theme>}
                      onClick={(): void => {
                        showAirmet();
                      }}
                    >
                      <ListItemText primary="AIRMET" />
                    </ListItem>
                  )}
                </List>
              </Collapse>
            )}
            <PresetMenu
              name="Presets"
              presets={presets}
              setScreenPreset={setScreenPreset}
            />
            {config?.GW_FEATURE_MENU_SATCOMP && (
              <PresetMenu
                name="Satcomp presets"
                presets={screenPresetsSatcomp}
                setScreenPreset={setScreenPreset}
              />
            )}
          </List>
        </Box>
      </Drawer>
    </div>
  );
};

export default Menu;
