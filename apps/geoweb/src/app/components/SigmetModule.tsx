/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box } from '@mui/material';
import { createApi, MetInfoWrapper } from '@opengeoweb/sigmet-airmet';
import { ApiProvider } from '@opengeoweb/api';
import { useAuthenticationContext } from '@opengeoweb/authentication';
import { ThemeWrapperOldTheme } from '@opengeoweb/theme';
import {
  ErrorBoundary,
  ToolContainerDraggable,
  calculateDialogSizeAndPosition,
} from '@opengeoweb/shared';
import { ConfigType } from '../utils/loadConfig';

interface SigmetProps {
  handleClose: () => void;
  order?: number;
  config?: ConfigType;
}

const Sigmet: React.FC<SigmetProps> = ({
  handleClose,
  order,
  config,
}: SigmetProps) => {
  const { auth, onSetAuth } = useAuthenticationContext();
  const { width, height, top, left } = calculateDialogSizeAndPosition();

  return config ? (
    <ToolContainerDraggable
      onClose={handleClose}
      title="SIGMET list"
      startPosition={{ top, left }}
      startSize={{ width, height }}
      minWidth={600}
      minHeight={300}
      order={order}
      data-testid="SigmetModule"
    >
      <Box
        sx={{
          padding: 3,
          backgroundColor: '#f1f1f1',
        }}
      >
        <ApiProvider
          baseURL={config.GW_SIGMET_BASE_URL}
          appURL={config.GW_APP_URL}
          auth={auth}
          onSetAuth={onSetAuth}
          createApi={createApi}
          authTokenUrl={config.GW_AUTH_TOKEN_URL}
          authClientId={config.GW_AUTH_CLIENT_ID}
        >
          <ErrorBoundary>
            <MetInfoWrapper productType="sigmet" />
          </ErrorBoundary>
        </ApiProvider>
      </Box>
    </ToolContainerDraggable>
  ) : null;
};

const SigmetModule: React.FC<SigmetProps> = ({ ...props }: SigmetProps) => (
  <ThemeWrapperOldTheme>
    <Sigmet {...props} />
  </ThemeWrapperOldTheme>
);
export default SigmetModule;
