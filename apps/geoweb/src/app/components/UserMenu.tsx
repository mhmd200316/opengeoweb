/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { MenuItem, Grid } from '@mui/material';
import { useHistory } from 'react-router-dom';
import {
  UserMenu as SharedUserMenu,
  useUserMenu,
  getInitials,
  UserMenuItemTheme,
  UserMenuItemCheatSheet,
} from '@opengeoweb/shared';
import { useApiContext, useApi } from '@opengeoweb/api';
import { InfoDialog } from './InfoDialog';
import ReleaseNotes from './ReleaseNotes';
import { ConfigType } from '../utils/loadConfig';
import { AppApi } from '../utils/api';

const styles = {
  username: {
    fontWeight: 500,
  },
};

interface UserProps {
  userName: string;
  userRole?: string;
  isAuthConfigured?: boolean;
  config?: ConfigType;
}

const UserMenu: React.FC<UserProps> = ({
  userName,
  userRole = '',
  isAuthConfigured = false,
  config,
}: UserProps) => {
  const { api } = useApiContext<AppApi>();
  const history = useHistory();
  const { isOpen, onClose, onToggle } = useUserMenu();
  const { error, result } = useApi(api.getBeVersion);
  const [showReleaseNotes, setShowReleaseNotes] = React.useState(false);

  const versionMessage = error
    ? `Loading failed: ${error.message}`
    : 'Loading...';

  const handleLogout = (): void => {
    onClose();
    history.push('/logout');
  };

  const handleLogin = (): void => {
    onClose();
    history.push('/login');
  };

  const openReleaseNotes = (): void => {
    onClose();
    setShowReleaseNotes(true);
  };

  const onOpenFeedback = (): void => {
    window.open('http://confluence.knmi.nl/display/GW/Feedback', '_blank');
  };

  const loginLogoutObject =
    userName === 'Guest' ? (
      <MenuItem onClick={handleLogin} data-testid="loginButton">
        Login
      </MenuItem>
    ) : (
      <MenuItem onClick={handleLogout} data-testid="logoutButton">
        Logout
      </MenuItem>
    );

  return (
    <>
      <SharedUserMenu
        isOpen={isOpen}
        onClose={onClose}
        onToggle={onToggle}
        initials={getInitials(userName)}
      >
        <MenuItem selected divider>
          <Grid container>
            {userRole && <Grid container>{userRole}</Grid>}
            <Grid item sx={styles.username}>
              {userName}
            </Grid>
          </Grid>
        </MenuItem>
        <UserMenuItemTheme />
        {config?.GW_FEATURE_MENU_VERSION && (
          <MenuItem
            data-testid="version-menu"
            onClick={openReleaseNotes}
            divider
          >
            {!result ? versionMessage : `v${result.version} has been released!`}
          </MenuItem>
        )}
        <UserMenuItemCheatSheet />
        {config?.GW_FEATURE_MENU_FEEDBACK && (
          <MenuItem
            data-testid="open-feedback"
            onClick={onOpenFeedback}
            divider
          >
            Feedback
          </MenuItem>
        )}
        {config?.GW_FEATURE_MENU_INFO && <InfoDialog />}
        {isAuthConfigured ? loginLogoutObject : null}
      </SharedUserMenu>
      {config?.GW_FEATURE_MENU_VERSION && showReleaseNotes && (
        <ReleaseNotes
          beVersion={!result ? versionMessage : result.version}
          handleClose={(): void => setShowReleaseNotes(false)}
        />
      )}
    </>
  );
};

export default UserMenu;
