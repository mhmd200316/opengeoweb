/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import {
  actions as screenManagerActions,
  ScreenManagerPreset,
} from '@opengeoweb/screenmanager';
import { InitialAppPreset, uiActions } from '@opengeoweb/core';
import { Flag } from '@opengeoweb/theme';
import Menu, { MenuProps, PresetMenuItem, PresetMenu } from './Menu';
import { TestWrapper } from '../utils/mockdata';
import * as api from '../utils/api';
import defaultScreenPresets from '../../assets/screenPresets.json';
import defaultSatCompPresets from '../../assets/screenPresetsSatcomp.json';
import { createFakeApi } from '../utils/api';

describe('/components/Menu', () => {
  const satellitePreset = defaultScreenPresets.find(
    (preset) => preset.title === 'Satellite',
  );
  const satcompPreset = defaultSatCompPresets.find(
    (preset) => preset.title === 'Satcomp general 3h',
  );
  const props: MenuProps = {
    showSpaceWeather: jest.fn(),
    showSigmet: jest.fn(),
    showAirmet: jest.fn(),
    config: {
      GW_GITLAB_PRESETS_BASE_URL: '',
      GW_GITLAB_PRESETS_API_PATH: '',
      GW_GITLAB_PROJECT_ID: '',
      GW_GITLAB_PRESETS_PATH: '',
      GW_GITLAB_BRANCH: '',
      GW_INITIAL_PRESETS_FILENAME: '',
      GW_SCREEN_PRESETS_FILENAME: '',
      GW_FEATURE_APP_TITLE: 'TestTitle',
      GW_FEATURE_FORCE_AUTHENTICATION: false,
      GW_FEATURE_MODULE_SPACE_WEATHER: true,
      GW_FEATURE_MODULE_TAF: true,
      GW_FEATURE_MODULE_SIGMET: true,
      GW_FEATURE_MODULE_AIRMET: true,
      GW_FEATURE_MENU_SATCOMP: false,
    },
  };

  it('should toggle the preset menu', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    const { getByTestId, queryByTestId } = render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Menu {...props} />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );
    expect(queryByTestId('Presets-list')).toBeFalsy();

    fireEvent.click(getByTestId('menuButton'));
    expect(queryByTestId('Presets-list')).toBeTruthy();

    fireEvent.click(queryByTestId('Presets-list').firstChild);
    await waitFor(() => {
      expect(queryByTestId('Presets-list')).toBeFalsy();
    });
  });

  it('should toggle the preset list and show default presets from the fake API in preset menu. Empty map present should sit on top of list', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    const { getByTestId, queryByTestId, queryAllByTestId } = render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Menu {...props} />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );
    await waitFor(() => {
      expect(queryByTestId('presetsListItem')).toBeFalsy();
    });

    fireEvent.click(getByTestId('menuButton'));
    await waitFor(() => {
      expect(queryAllByTestId('presetsListItem')).toHaveLength(16);
      expect(queryAllByTestId('presetsListItem')[0].textContent).toContain(
        'Empty Map',
      );
    });

    fireEvent.click(getByTestId('presetsListButton'));
    await waitFor(() => {
      expect(queryByTestId('presetsListItem')).toBeFalsy();
    });
  });

  it('should show default presets in case presets not returned from the API', async () => {
    const spy = jest.spyOn(api, 'createFakeApi').mockImplementationOnce(() => {
      return {
        ...createFakeApi(),
        getInitialPresets: (): Promise<{ data: InitialAppPreset }> =>
          Promise.resolve({ data: { preset: undefined } }),
      };
    });
    const mockStore = configureStore();
    const store = mockStore();
    const { getByTestId, queryByTestId } = render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Menu {...props} />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );
    expect(spy).toHaveBeenCalled();

    await waitFor(() => {
      fireEvent.click(getByTestId('menuButton'));
    });
    expect(queryByTestId('Presets-list').firstChild.textContent).toContain(
      'Empty Map',
    );
  });

  it('should set layers and bbox when clicking on one of the presets', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, getByText, findByText } = render(
      <Provider store={store}>
        <TestWrapper>
          <BrowserRouter>
            <Menu {...props} />
          </BrowserRouter>
        </TestWrapper>
      </Provider>,
    );

    fireEvent.click(getByTestId('menuButton'));
    expect(await findByText('Satellite')).toBeTruthy();

    fireEvent.click(getByText('Satellite'));
    const expectedActions = [
      screenManagerActions.changePreset({
        screenManagerPreset: satellitePreset as ScreenManagerPreset,
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should open spaceweather/SIGMET/AIRMET when menu item clicked', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByTestId, getByText, findByText } = render(
      <Provider store={store}>
        <TestWrapper>
          <BrowserRouter>
            <Menu {...props} />
          </BrowserRouter>
        </TestWrapper>
      </Provider>,
    );

    fireEvent.click(getByTestId('menuButton'));
    expect(await findByText('Space Weather Forecast')).toBeTruthy();

    fireEvent.click(getByText('Space Weather Forecast'));
    expect(props.showSpaceWeather).toHaveBeenCalled();

    fireEvent.click(getByText('SIGMET'));
    expect(props.showSigmet).toHaveBeenCalled();

    fireEvent.click(getByText('AIRMET'));
    expect(props.showAirmet).toHaveBeenCalled();
  });

  it('should open and close aviation menu item when clicking on it', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByTestId, getByText, queryByText, findByText } = render(
      <Provider store={store}>
        <TestWrapper>
          <BrowserRouter>
            <Menu {...props} />
          </BrowserRouter>
        </TestWrapper>
      </Provider>,
    );

    fireEvent.click(getByTestId('menuButton'));

    expect(await findByText('Aviation')).toBeTruthy();
    expect(await findByText('SIGMET')).toBeTruthy();

    fireEvent.click(getByText('Aviation'));
    expect(await findByText('Aviation')).toBeTruthy();
    expect(queryByText('SIGMET')).toBeFalsy();
  });

  it('should open and close presets menu item when clicking on it', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByTestId, getByText, queryByText } = render(
      <Provider store={store}>
        <TestWrapper>
          <BrowserRouter>
            <Menu {...props} />
          </BrowserRouter>
        </TestWrapper>
      </Provider>,
    );

    fireEvent.click(getByTestId('menuButton'));
    await waitFor(() => {
      expect(getByText('Presets')).toBeTruthy();
      expect(getByText('Radar view')).toBeTruthy();
    });

    fireEvent.click(getByText('Presets'));
    await waitFor(() => {
      expect(getByText('Presets')).toBeTruthy();
      expect(queryByText('Radar view')).toBeFalsy();
    });
  });

  it('should support keyboard navigation', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByTestId, queryByText } = render(
      <Provider store={store}>
        <TestWrapper>
          <BrowserRouter>
            <Menu {...props} />
          </BrowserRouter>
        </TestWrapper>
      </Provider>,
    );

    fireEvent.click(getByTestId('menuButton'));
    await waitFor(() => {
      expect(queryByText('Presets')).toBeTruthy();
    });
    userEvent.tab();
    expect(document.activeElement.textContent).toEqual('Auto Arrange Windows');
    userEvent.tab();
    expect(document.activeElement.textContent).toEqual('Sync Groups');
    userEvent.tab();
    expect(document.activeElement.textContent).toEqual(
      'Space Weather Forecast',
    );
    userEvent.tab();
    expect(document.activeElement.textContent).toEqual('Aviation');
    userEvent.keyboard('{Enter}');
    await waitFor(() => expect(queryByText('SIGMET')).toBeFalsy());
    userEvent.tab({ shift: true });
    expect(document.activeElement.textContent).toEqual(
      'Space Weather Forecast',
    );
  });

  it('should open the Sync groups', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    const { getByText, getByTestId } = render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Menu {...props} />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );

    const expectedAction = uiActions.setToggleOpenDialog({
      type: 'syncGroups',
      setOpen: true,
    });

    await waitFor(() => {
      fireEvent.click(getByTestId('menuButton'));
    });
    await waitFor(() => {
      fireEvent.click(getByText('Sync Groups'));
    });
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should auto arrange windows', async () => {
    const screenConfig = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1', 'screen2', 'screen3', 'screen4'],
        byId: {
          screen1: {
            title: 'screen 1',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen2: {
            title: 'screen 2',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen3: {
            title: 'screen 3',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen4: {
            title: 'screen 4',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
        },
      },
      mosaicNode: {
        direction: 'row',
        second: {
          direction: 'column',
          first: {
            direction: 'row',
            second: 'screen1',
            first: 'screen2',
          },
          second: 'screen3',
        },
        first: 'screen4',
      },
    };

    const mockState = {
      screenManagerStore: screenConfig,
    };

    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { getByText, getByTestId } = render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Menu {...props} />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );

    await waitFor(() => {
      fireEvent.click(getByTestId('menuButton'));
    });
    await waitFor(() => {
      fireEvent.click(getByText('Auto Arrange Windows'));
    });

    const expectedAction = screenManagerActions.updateScreenManagerViews({
      mosaicNode: {
        direction: 'row',
        first: {
          direction: 'column',
          first: 'screen4',
          second: 'screen2',
        },
        second: {
          direction: 'column',
          first: 'screen1',
          second: 'screen3',
        },
      },
    });
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should show the satcomp presets when enabled', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    const { getByTestId, queryByTestId, queryAllByTestId } = render(
      <BrowserRouter>
        <Provider store={store}>
          <TestWrapper>
            <Menu
              {...props}
              config={{
                GW_GITLAB_PRESETS_BASE_URL: '',
                GW_GITLAB_PRESETS_API_PATH: '',
                GW_GITLAB_PROJECT_ID: '',
                GW_GITLAB_PRESETS_PATH: '',
                GW_GITLAB_BRANCH: '',
                GW_INITIAL_PRESETS_FILENAME: '',
                GW_SCREEN_PRESETS_FILENAME: '',
                GW_FEATURE_APP_TITLE: 'TestTitle',
                GW_FEATURE_FORCE_AUTHENTICATION: false,
                GW_FEATURE_MODULE_SPACE_WEATHER: true,
                GW_FEATURE_MODULE_TAF: true,
                GW_FEATURE_MODULE_SIGMET: true,
                GW_FEATURE_MODULE_AIRMET: true,
                GW_FEATURE_MENU_SATCOMP: true,
              }}
            />
          </TestWrapper>
        </Provider>
      </BrowserRouter>,
    );
    await waitFor(() => {
      expect(queryByTestId('presetsListItem')).toBeFalsy();
    });

    fireEvent.click(getByTestId('menuButton'));
    await waitFor(() => {
      expect(queryAllByTestId('presetsListItem')).toHaveLength(26);
      expect(queryAllByTestId('presetsListItem')[16].textContent).toContain(
        'Satcomp general 3h',
      );
    });
  });

  it('should open and close satcomp presets menu item when clicking on it', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addModules = jest.fn(); // mocking the dynamic module loader
    const { getByTestId, getByText, queryByText } = render(
      <Provider store={store}>
        <TestWrapper>
          <BrowserRouter>
            <Menu
              {...props}
              config={{ ...props.config, GW_FEATURE_MENU_SATCOMP: true }}
            />
          </BrowserRouter>
        </TestWrapper>
      </Provider>,
    );

    fireEvent.click(getByTestId('menuButton'));
    await waitFor(() => {
      expect(getByText('Satcomp presets')).toBeTruthy();
      expect(getByText('Satcomp general 3h')).toBeTruthy();
    });

    fireEvent.click(getByText('Satcomp presets'));
    await waitFor(() => {
      expect(getByText('Presets')).toBeTruthy();
      expect(queryByText('Satcomp general 3h')).toBeFalsy();
    });
  });

  it('should set layers and bbox when clicking on one of the satcomp presets', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    store.addModules = jest.fn(); // mocking the dynamic module loader

    const { getByTestId, getByText, findByText } = render(
      <Provider store={store}>
        <TestWrapper>
          <BrowserRouter>
            <Menu
              {...props}
              config={{ ...props.config, GW_FEATURE_MENU_SATCOMP: true }}
            />
          </BrowserRouter>
        </TestWrapper>
      </Provider>,
    );

    fireEvent.click(getByTestId('menuButton'));
    expect(await findByText('Satcomp general 3h')).toBeTruthy();

    fireEvent.click(getByText('Satcomp general 3h'));
    const expectedActions = [
      screenManagerActions.changePreset({
        screenManagerPreset: satcompPreset as ScreenManagerPreset,
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  describe('PresetMenuItem', () => {
    it('should render the listItem with all passed properties', async () => {
      const props = {
        presetItemTitle: 'TITLE',
        itemIcon: <Flag data-testid="FLAG_ICON" />,
        onClickPreset: jest.fn(),
      };
      const { getByText, getByTestId } = render(
        <TestWrapper>
          <PresetMenuItem {...props} />
        </TestWrapper>,
      );

      expect(getByText('TITLE')).toBeTruthy();
      expect(getByTestId('FLAG_ICON')).toBeTruthy();
      fireEvent.click(getByText('TITLE'));
      expect(props.onClickPreset).toHaveBeenCalled();
    });
  });

  describe('PresetMenu', () => {
    it('should show a message when the list of presets is empty', async () => {
      const props = {
        presets: [],
        name: 'Test presets',
        setScreenPreset: jest.fn(),
      };
      const { getByText } = render(
        <TestWrapper>
          <PresetMenu {...props} />
        </TestWrapper>,
      );

      expect(getByText(props.name)).toBeTruthy();
      expect(getByText('Test presets not available')).toBeTruthy();
    });
  });
});
