/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render } from '@testing-library/react';

import { AuthenticationProvider } from '@opengeoweb/authentication';
import SigmetModule from './SigmetModule';
import { ConfigType } from '../utils/loadConfig';

describe('SigmetModule', () => {
  it('should render successfully', async () => {
    const { findByText, getByTestId } = render(
      <AuthenticationProvider>
        <SigmetModule
          handleClose={jest.fn()}
          config={
            {
              GW_SW_BASE_URL: 'spaceweather',
              GW_SIGMET_BASE_URL: 'sigmet',
              GW_AIRMET_BASE_URL: 'airmet',
              GW_AUTH_LOGIN_URL: 'login',
              GW_AUTH_LOGOUT_URL: 'logout',
              GW_AUTH_TOKEN_URL: 'token',
              GW_AUTH_CLIENT_ID: 'test',
              GW_INFRA_BASE_URL: 'infra',
              GW_APP_URL: 'http://localhost:4200',
            } as ConfigType
          }
        />
      </AuthenticationProvider>,
    );

    expect(await findByText('SIGMET list')).toBeTruthy();
    expect(getByTestId('SigmetModule')).toBeTruthy();
  });

  it('should not render without config', async () => {
    const { queryByTestId } = render(
      <AuthenticationProvider>
        <SigmetModule handleClose={jest.fn()} />
      </AuthenticationProvider>,
    );

    expect(queryByTestId('SigmetModule')).toBeFalsy();
  });
});
