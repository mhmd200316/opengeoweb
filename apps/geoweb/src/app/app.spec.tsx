/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, waitFor } from '@testing-library/react';

import App from './app';
import { ConfigType } from './utils/loadConfig';

describe('App', () => {
  const storedFetch = global['fetch'];
  const storedWindowLocation = window.location;

  beforeEach(() => {
    // mock window.location
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = {
      assign: (): void => {},
    };
  });

  afterEach(() => {
    global['fetch'] = storedFetch;
    window.location = storedWindowLocation;
  });

  it('should render successfully', async () => {
    // mock the loadConfig fetch
    const mockConfig: ConfigType = {
      GW_INITIAL_PRESETS_FILENAME: '',
      GW_SCREEN_PRESETS_FILENAME: '',
    };
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );

    const { baseElement } = render(<App />);
    await waitFor(() => expect(baseElement).toBeTruthy());
  });

  it('should display the configured page title', async () => {
    // mock the loadConfig fetch
    const mockConfig: ConfigType = {
      GW_INITIAL_PRESETS_FILENAME: '',
      GW_SCREEN_PRESETS_FILENAME: '',
      GW_FEATURE_APP_TITLE: 'testTitle',
    };
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );

    const { baseElement } = render(<App />);

    await waitFor(() => {
      expect(baseElement).toBeTruthy();
      expect(document.title).toEqual('testTitle');
    });
  });

  it('should redirect user to login screen', async () => {
    // mock the loadConfig fetch with a valid configuration
    const mockConfig: ConfigType = {
      GW_GITLAB_PRESETS_BASE_URL: 'http://fakeurl',
      GW_GITLAB_PRESETS_API_PATH: '/path/{variable}',
      GW_GITLAB_PROJECT_ID: '123456',
      GW_GITLAB_PRESETS_PATH: 'encoded%2path',
      GW_GITLAB_BRANCH: 'branch',
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
      GW_SCREEN_PRESETS_FILENAME: 'screenPresets.json',
      GW_AUTH_LOGIN_URL: 'login',
      GW_AUTH_LOGOUT_URL: 'logout',
      GW_AUTH_TOKEN_URL: 'token',
      GW_AUTH_CLIENT_ID: '123456',
      GW_INFRA_BASE_URL: '-',
      GW_APP_URL: 'http://fakeurl',
      GW_FEATURE_APP_TITLE: 'title',
      GW_FEATURE_FORCE_AUTHENTICATION: true,
    };
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );
    const spy = jest.spyOn(window.location, 'assign');
    render(<App />);
    await waitFor(() => expect(spy).toHaveBeenCalledWith('login'));
  });

  it('should show error when config is not valid', async () => {
    // mock the loadConfig fetch with a valid configuration
    const mockConfig: ConfigType = {
      GW_GITLAB_PRESETS_BASE_URL: '',
      GW_GITLAB_PRESETS_API_PATH: '',
      GW_GITLAB_PROJECT_ID: '',
      GW_GITLAB_PRESETS_PATH: '',
      GW_GITLAB_BRANCH: '',
      GW_INITIAL_PRESETS_FILENAME: '',
      GW_SCREEN_PRESETS_FILENAME: '',
      GW_AUTH_LOGIN_URL: '',
      GW_AUTH_LOGOUT_URL: '',
      GW_AUTH_TOKEN_URL: '',
      GW_AUTH_CLIENT_ID: '',
      GW_INFRA_BASE_URL: '-',
      GW_APP_URL: '',
      GW_FEATURE_APP_TITLE: 'title',
      GW_FEATURE_FORCE_AUTHENTICATION: true,
    };
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );
    const { queryByText } = render(<App />);
    await waitFor(() => {
      expect(
        queryByText(
          'Configuration (config.json) is not valid and has the following errors:',
        ),
      ).toBeTruthy();

      expect(
        queryByText(
          'GW_GITLAB_PRESETS_BASE_URL, GW_GITLAB_PRESETS_API_PATH, GW_GITLAB_PROJECT_ID, GW_GITLAB_PRESETS_PATH, GW_GITLAB_BRANCH, GW_INITIAL_PRESETS_FILENAME, GW_SCREEN_PRESETS_FILENAME, GW_AUTH_LOGIN_URL, GW_AUTH_LOGOUT_URL, GW_AUTH_TOKEN_URL, GW_AUTH_CLIENT_ID, GW_APP_URL',
        ),
      ).toBeTruthy();
    });
  });

  it('should show multiple types of errors when config is not valid', async () => {
    // mock the loadConfig fetch with a valid configuration
    const mockConfig: ConfigType = {
      GW_GITLAB_PRESETS_BASE_URL: '',
      GW_GITLAB_PRESETS_API_PATH: '',
      GW_I_DONT_EXIST: 'test',
      GW_I_DONT_EXIST2: 'test-2',
    } as unknown as ConfigType;
    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockConfig),
      }),
    );
    const { queryByText } = render(<App />);
    await waitFor(() => {
      expect(
        queryByText(
          'Configuration (config.json) is not valid and has the following errors:',
        ),
      ).toBeTruthy();

      const nonExisting = queryByText('NonExisting keys:');
      expect(nonExisting).toBeTruthy();
      expect(nonExisting.parentElement.innerHTML).toContain(
        'GW_I_DONT_EXIST, GW_I_DONT_EXIST2',
      );

      const empty = queryByText('Empty keys:');
      expect(empty).toBeTruthy();
      expect(empty.parentElement.innerHTML).toContain(
        'GW_GITLAB_PRESETS_BASE_URL, GW_GITLAB_PRESETS_API_PATH',
      );
    });
  });
});
