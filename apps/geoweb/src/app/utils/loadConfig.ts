/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';

export interface ConfigType {
  // auth
  GW_AUTH_LOGIN_URL?: string;
  GW_AUTH_LOGOUT_URL?: string;
  GW_AUTH_TOKEN_URL?: string;
  GW_AUTH_CLIENT_ID?: string;
  // app
  GW_INFRA_BASE_URL?: string;
  GW_APP_URL?: string;
  GW_INITIAL_PRESETS_FILENAME?: string;
  GW_SCREEN_PRESETS_FILENAME?: string;
  // gitlab
  GW_GITLAB_PROJECT_ID?: string;
  GW_GITLAB_PRESETS_PATH?: string;
  GW_GITLAB_BRANCH?: string;
  GW_GITLAB_PRESETS_BASE_URL?: string;
  GW_GITLAB_PRESETS_API_PATH?: string;
  // backend urls
  GW_SW_BASE_URL?: string;
  GW_SIGMET_BASE_URL?: string;
  GW_AIRMET_BASE_URL?: string;
  GW_TAF_BASE_URL?: string;
  GW_PRESET_BACKEND_URL?: string;
  // app features
  GW_FEATURE_APP_TITLE?: string;
  GW_FEATURE_LAYER_SELECT?: boolean;
  GW_FEATURE_FORCE_AUTHENTICATION?: boolean;
  GW_FEATURE_MODULE_SPACE_WEATHER?: boolean;
  GW_FEATURE_MODULE_TAF?: boolean;
  GW_FEATURE_MODULE_SIGMET?: boolean;
  GW_FEATURE_MODULE_AIRMET?: boolean;
  GW_FEATURE_MENU_SATCOMP?: boolean;
  GW_FEATURE_MENU_FEEDBACK?: boolean;
  GW_FEATURE_MENU_INFO?: boolean;
  GW_FEATURE_MENU_VERSION?: boolean;
}

const allConfigKeys = {
  GW_AUTH_LOGIN_URL: '',
  GW_AUTH_LOGOUT_URL: '',
  GW_AUTH_TOKEN_URL: '',
  GW_AUTH_CLIENT_ID: '',
  GW_INFRA_BASE_URL: '',
  GW_APP_URL: '',
  GW_INITIAL_PRESETS_FILENAME: '',
  GW_SCREEN_PRESETS_FILENAME: '',
  GW_GITLAB_PROJECT_ID: '',
  GW_GITLAB_PRESETS_PATH: '',
  GW_GITLAB_BRANCH: '',
  GW_GITLAB_PRESETS_BASE_URL: '',
  GW_GITLAB_PRESETS_API_PATH: '',
  GW_SW_BASE_URL: '',
  GW_SIGMET_BASE_URL: '',
  GW_AIRMET_BASE_URL: '',
  GW_TAF_BASE_URL: '',
  GW_PRESET_BACKEND_URL: '',
  GW_FEATURE_APP_TITLE: '',
  GW_FEATURE_LAYER_SELECT: false,
  GW_FEATURE_FORCE_AUTHENTICATION: false,
  GW_FEATURE_MODULE_SPACE_WEATHER: false,
  GW_FEATURE_MODULE_TAF: false,
  GW_FEATURE_MODULE_SIGMET: false,
  GW_FEATURE_MODULE_AIRMET: false,
  GW_FEATURE_MENU_SATCOMP: false,
  GW_FEATURE_MENU_FEEDBACK: false,
  GW_FEATURE_MENU_INFO: false,
  GW_FEATURE_MENU_VERSION: false,
};

const requiredConfigKeysForAuthentication: ConfigType = {
  GW_AUTH_CLIENT_ID: '',
  GW_AUTH_LOGIN_URL: '',
  GW_AUTH_LOGOUT_URL: '',
  GW_AUTH_TOKEN_URL: '',
  GW_INFRA_BASE_URL: '',
  GW_APP_URL: '',
};

export enum ValidationType {
  missing = 'Missing keys',
  empty = 'Empty keys',
  nonExist = 'NonExisting keys',
}

export type ValidationError = {
  key: string;
  error: ValidationType;
};

const validateRequiredConfigKeys = (
  requiredKeys: ConfigType,
  config: ConfigType,
): ValidationError[] =>
  Object.keys(requiredKeys).reduce((list, key) => {
    const value = config[key];
    if (value === undefined) {
      return list.concat({ key, error: ValidationType.missing });
    }
    return list;
  }, []);

const validateConfigKeys = (keys: ConfigType): ValidationError[] => {
  return Object.keys(keys).reduce((list, key) => {
    // check if exist
    if (allConfigKeys[key] === undefined) {
      return list.concat({ key, error: ValidationType.nonExist });
    }
    const value = keys[key];
    if (typeof value === 'string' && value === '') {
      return list.concat({ key, error: ValidationType.empty });
    }
    return list;
  }, []);
};

export const isValidConfig = (
  configObject: ConfigType,
): boolean | ValidationError[] => {
  const errors = [...validateConfigKeys(configObject)];

  return errors.length ? errors : true;
};

export const isValidConfigWithAuthentication = (
  configObject: ConfigType,
): boolean | ValidationError[] => {
  const errors = validateRequiredConfigKeys(
    requiredConfigKeysForAuthentication,
    configObject,
  );
  return errors.length ? errors : true;
};

export const sortErrors = (
  validationErrors: ValidationError[],
): {
  [ValidationType.missing]?: [];
  [ValidationType.empty]?: [];
  [ValidationType.nonExist]?: [];
} =>
  validationErrors.reduce((list, error) => {
    return {
      ...list,
      [error.error]: list[error.error]
        ? list[error.error].concat(error)
        : [error],
    };
  }, {});

const loadConfig = async (): Promise<ConfigType> => {
  const res = await fetch('./assets/config.json', {
    credentials: 'include',
  });
  const mod = await res.json();
  return mod;
};

export const useConfig = (): [ConfigType] => {
  const [configObject, setConfigObject] = React.useState<ConfigType>(null);
  React.useEffect(() => {
    loadConfig()
      .then((result): void => {
        setConfigObject(result);
      })
      .catch(() => {
        // eslint-disable-next-line no-console
        console.warn('Unable to load configuration file (config.json)');
      });
  }, []);
  return [configObject];
};
