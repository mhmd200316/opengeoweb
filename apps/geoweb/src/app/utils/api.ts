/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { createApiInstance, Credentials } from '@opengeoweb/api';
import { ScreenManagerPreset } from '@opengeoweb/screenmanager';
import { InitialAppPreset } from '@opengeoweb/core';
import { AxiosInstance } from 'axios';
import defaultScreenPresets from '../../assets/screenPresets.json';
import defaultInitialPresets from '../../assets/initialPresets.json';
import { ConfigType } from './loadConfig';

export const replaceTemplateKeys = (
  apiPath: string,
  projectId: string,
  presetsPath: string,
  branch: string,
  filename: string,
): string =>
  apiPath
    .replace('{project_id}', projectId)
    .replace('{presets_path}', presetsPath)
    .replace('{branch}', branch)
    .replace('{preset_filename}', filename);

export const isGitLabConfigured = (config: ConfigType): boolean =>
  config.GW_GITLAB_PRESETS_API_PATH?.length > 0 &&
  config.GW_GITLAB_PROJECT_ID?.length > 0 &&
  config.GW_GITLAB_PRESETS_PATH?.length > 0 &&
  config.GW_GITLAB_BRANCH?.length > 0;

export type AppApi = {
  getInitialPresets: (
    config: ConfigType,
  ) => Promise<{ data: InitialAppPreset }>;
  getScreenPresets: (
    config: ConfigType,
  ) => Promise<{ data: ScreenManagerPreset[] }>;
  getBeVersion: () => Promise<{ data: { version: string } }>;
};

const getApiRoutes = (axiosInstance: AxiosInstance): AppApi => ({
  getInitialPresets: (
    config: ConfigType,
  ): Promise<{ data: InitialAppPreset }> => {
    const hasGitLab = isGitLabConfigured(config);
    const initialPresetsUrl = hasGitLab
      ? replaceTemplateKeys(
          config.GW_GITLAB_PRESETS_API_PATH,
          config.GW_GITLAB_PROJECT_ID,
          config.GW_GITLAB_PRESETS_PATH,
          config.GW_GITLAB_BRANCH,
          config.GW_INITIAL_PRESETS_FILENAME,
        )
      : `./assets/${config.GW_INITIAL_PRESETS_FILENAME}`;
    return axiosInstance.get(initialPresetsUrl);
  },
  getScreenPresets: (
    config: ConfigType,
  ): Promise<{ data: ScreenManagerPreset[] }> => {
    const hasGitLab = isGitLabConfigured(config);
    const screenPresetsUrl = hasGitLab
      ? replaceTemplateKeys(
          config.GW_GITLAB_PRESETS_API_PATH,
          config.GW_GITLAB_PROJECT_ID,
          config.GW_GITLAB_PRESETS_PATH,
          config.GW_GITLAB_BRANCH,
          config.GW_SCREEN_PRESETS_FILENAME,
        )
      : `./assets/${config.GW_SCREEN_PRESETS_FILENAME}`;

    return axiosInstance.get(screenPresetsUrl);
  },
  getBeVersion: (): Promise<{ data: { version: string } }> => {
    return axiosInstance.get('/version');
  },
});

export const createApi = (
  url: string,
  appUrl: string,
  auth: Credentials,
  setAuth: (cred: Credentials) => void,
  authTokenUrl: string,
  authClientId: string,
): AppApi => {
  const axiosInstance = createApiInstance(
    url,
    appUrl,
    auth,
    setAuth,
    authTokenUrl,
    authClientId,
  );

  return getApiRoutes(axiosInstance);
};

export const createFakeApi = (): AppApi => {
  const api = {
    // dummy calls
    getInitialPresets: (): Promise<{ data: InitialAppPreset }> =>
      Promise.resolve({
        data: defaultInitialPresets as InitialAppPreset,
      }),
    getScreenPresets: (): Promise<{
      data: ScreenManagerPreset[];
    }> =>
      Promise.resolve({
        data: defaultScreenPresets as ScreenManagerPreset[],
      }),
    getBeVersion: (): Promise<{ data: { version: string } }> =>
      Promise.resolve({ data: { version: 'version 1.0.1' } }),
  };

  return api;
};
