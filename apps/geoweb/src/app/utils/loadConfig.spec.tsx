/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { renderHook } from '@testing-library/react-hooks';

import {
  ConfigType,
  isValidConfig,
  isValidConfigWithAuthentication,
  sortErrors,
  useConfig,
  ValidationType,
} from './loadConfig';

describe('utils/loadConfig/isValidConfig', () => {
  const storedFetch = global['fetch'];
  beforeEach(() => {
    jest.spyOn(console, 'warn').mockImplementation(() => {});
  });
  afterEach(() => {
    global['fetch'] = storedFetch;
  });

  it('should return true when config is valid', () => {
    const validConfig = {
      GW_GITLAB_PRESETS_BASE_URL: 'http://fakeurl',
      GW_GITLAB_PRESETS_API_PATH: '/path/{variable}',
      GW_GITLAB_PROJECT_ID: '123456',
      GW_GITLAB_PRESETS_PATH: 'encoded%2path',
      GW_GITLAB_BRANCH: 'branch',
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
      GW_MENU_PRESETS_FILENAME: 'menuPresets.json',
      GW_SCREEN_PRESETS_FILENAME: 'screenPresets.json',
    };
    expect({}).toBeTruthy();
    expect(isValidConfig(validConfig)).toBeTruthy();
    expect({
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
    }).toBeTruthy();
    expect({
      GW_SCREEN_PRESETS_FILENAME: 'initialScreenPresets.json',
    }).toBeTruthy();
  });

  it('should return list of errors when config is invalid', () => {
    expect(isValidConfig({} as ConfigType)).toBeTruthy();

    // empty keys
    expect(
      isValidConfig({
        GW_GITLAB_PRESETS_BASE_URL: '',
        GW_GITLAB_PRESETS_API_PATH: '',
        GW_GITLAB_PROJECT_ID: '',
        GW_GITLAB_PRESETS_PATH: '',
        GW_GITLAB_BRANCH: '',
        GW_INITIAL_PRESETS_FILENAME: '',
        GW_SCREEN_PRESETS_FILENAME: '',
      }),
    ).toEqual([
      { key: 'GW_GITLAB_PRESETS_BASE_URL', error: ValidationType.empty },
      { key: 'GW_GITLAB_PRESETS_API_PATH', error: ValidationType.empty },
      { key: 'GW_GITLAB_PROJECT_ID', error: ValidationType.empty },
      { key: 'GW_GITLAB_PRESETS_PATH', error: ValidationType.empty },
      { key: 'GW_GITLAB_BRANCH', error: ValidationType.empty },
      { key: 'GW_INITIAL_PRESETS_FILENAME', error: ValidationType.empty },
      { key: 'GW_SCREEN_PRESETS_FILENAME', error: ValidationType.empty },
    ]);

    // non existing keys
    expect(
      isValidConfig({
        TEST_URL: '',
        GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
        GW_SCREEN_PRESETS_FILENAME: 'screenPresets.json',
      } as ConfigType),
    ).toEqual([{ key: 'TEST_URL', error: ValidationType.nonExist }]);
  });

  it('should return list of mixed errors when config is invalid', () => {
    expect(isValidConfig({} as ConfigType)).toBeTruthy();

    expect(
      isValidConfig({
        NON_EXISTING_KEY: 'testing',
        GW_GITLAB_BRANCH: '',
      } as unknown as ConfigType),
    ).toEqual([
      { key: 'NON_EXISTING_KEY', error: ValidationType.nonExist },
      { key: 'GW_GITLAB_BRANCH', error: ValidationType.empty },
    ]);
  });

  it('should return true when authentication config is valid', () => {
    const validConfig = {
      GW_GITLAB_PRESETS_BASE_URL: 'http://fakeurl',
      GW_GITLAB_PRESETS_API_PATH: '/path/{variable}',
      GW_GITLAB_PROJECT_ID: '123456',
      GW_GITLAB_PRESETS_PATH: 'encoded%2path',
      GW_GITLAB_BRANCH: 'branch',
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
      GW_SCREEN_PRESETS_FILENAME: 'screenPresets.json',
      GW_AUTH_LOGIN_URL: 'http://fakeurl',
      GW_AUTH_LOGOUT_URL: 'http://fakeurl',
      GW_AUTH_TOKEN_URL: 'http://fakeurl',
      GW_AUTH_CLIENT_ID: '123456',
      GW_INFRA_BASE_URL: '-',
      GW_APP_URL: 'http://fakeurl',
      GW_FEATURE_APP_TITLE: 'title',
    };
    expect(isValidConfigWithAuthentication(validConfig)).toBeTruthy();
    expect(isValidConfig(validConfig)).toBeTruthy();
  });
});

describe('sortErrors', () => {
  it('should return undefined if no errors ', () => {
    const result = sortErrors([]);

    expect(result[ValidationType.missing]).toBeUndefined();
    expect(result[ValidationType.nonExist]).toBeUndefined();
    expect(result[ValidationType.empty]).toBeUndefined();
  });
  it('should sort errors on type', () => {
    const testErrors = [
      { key: 'GW_SCREEN_PRESETS_FILENAME', error: ValidationType.missing },
      { key: 'GW_INITIAL_PRESETS_FILENAME', error: ValidationType.missing },
      { key: 'NON_EXISTING_KEY', error: ValidationType.nonExist },
      { key: 'NON_EXISTING_KEY_2', error: ValidationType.nonExist },
      { key: 'GW_AUTH_LOGIN_URL', error: ValidationType.empty },
      { key: 'GW_GITLAB_BRANCH', error: ValidationType.empty },
    ];

    const result = sortErrors(testErrors);

    expect(result[ValidationType.missing]).toEqual([
      testErrors[0],
      testErrors[1],
    ]);

    expect(result[ValidationType.nonExist]).toEqual([
      testErrors[2],
      testErrors[3],
    ]);

    expect(result[ValidationType.empty]).toEqual([
      testErrors[4],
      testErrors[5],
    ]);
  });
});

describe('useConfig', () => {
  it('should load config.json', async () => {
    const validConfig = {
      GW_GITLAB_PRESETS_BASE_URL: 'http://fakeurl',
      GW_GITLAB_PRESETS_API_PATH: '/path/{variable}',
      GW_GITLAB_PROJECT_ID: '123456',
      GW_GITLAB_PRESETS_PATH: 'encoded%2path',
      GW_GITLAB_BRANCH: 'branch',
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
      GW_SCREEN_PRESETS_FILENAME: 'screenPresets.json',
      GW_AUTH_LOGIN_URL: 'http://fakeurl',
      GW_AUTH_LOGOUT_URL: 'http://fakeurl',
      GW_AUTH_TOKEN_URL: 'http://fakeurl',
      GW_AUTH_CLIENT_ID: '123456',
      GW_INFRA_BASE_URL: '-',
      GW_APP_URL: 'http://fakeurl',
      GW_FEATURE_APP_TITLE: 'title',
    };

    global['fetch'] = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(validConfig),
      }),
    );

    const { result, waitForNextUpdate } = renderHook(() => useConfig());
    await waitForNextUpdate();
    const [config] = result.current;
    expect(config).toEqual(validConfig);
  });

  it('should return minimal default config when config.json is not present', async () => {
    const spy = jest.spyOn(global.console, 'warn');
    global['fetch'] = jest.fn().mockImplementation(() => Promise.reject);

    const { result } = renderHook(() => useConfig());
    const [config] = result.current;
    expect(config).toEqual(null);
    expect(spy).not.toHaveBeenCalled();
  });
});
