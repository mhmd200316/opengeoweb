/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as utils from '@opengeoweb/api';
import { createFakeApiInstance } from '@opengeoweb/api';
import { createApi, isGitLabConfigured, replaceTemplateKeys } from './api';

jest.mock('@opengeoweb/api', () => ({
  __esModule: true,
  ...(jest.requireActual(
    '../../../../../libs/api/src/lib/components/ApiContext/utils',
  ) as Record<string, unknown>),
}));

describe('src/app/utils/api', () => {
  const fakeAxiosInstance = createFakeApiInstance();
  describe('isGitLabConfigured', () => {
    it('should validate if gitlab is configured', () => {
      expect(
        isGitLabConfigured({
          GW_GITLAB_PRESETS_API_PATH:
            '/{project_id}/repository/files/{presets_path}{preset_filename}/raw?ref={branch}',
          GW_GITLAB_PROJECT_ID: 'test_projectid',
          GW_GITLAB_PRESETS_PATH: 'test_path',
          GW_GITLAB_BRANCH: 'test_branch',
          GW_INITIAL_PRESETS_FILENAME: 'test_presets_filename',
          GW_SCREEN_PRESETS_FILENAME: 'test_screenpresets_filename',
        }),
      ).toBeTruthy();

      expect(
        isGitLabConfigured({
          GW_GITLAB_PRESETS_API_PATH: '',
          GW_GITLAB_PROJECT_ID: 'test_projectid',
          GW_GITLAB_PRESETS_PATH: 'test_path',
          GW_GITLAB_BRANCH: 'test_branch',
          GW_INITIAL_PRESETS_FILENAME: 'test_presets_filename',
          GW_SCREEN_PRESETS_FILENAME: 'test_screenpresets_filename',
        }),
      ).toBeFalsy();

      expect(
        isGitLabConfigured({
          GW_INITIAL_PRESETS_FILENAME: 'test_presets_filename',
          GW_SCREEN_PRESETS_FILENAME: 'test_screenpresets_filename',
        }),
      ).toBeFalsy();
    });
  });

  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi(
        'fakeURL',
        'fakeAppURL',
        {
          username: 'Michael Jackson',
          token: '1223344',
          refresh_token: '33455214',
        },
        jest.fn(),
        'fakeAuthTokenUrl',
        'fakeauthClientId',
      );
      expect(api.getInitialPresets).toBeTruthy();
      expect(api.getScreenPresets).toBeTruthy();
      expect(api.getBeVersion).toBeTruthy();
    });
  });

  it('should call with the right params for getInitialPresets when gitlab is configured', async () => {
    jest
      .spyOn(utils, 'createApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'get');
    const testConfig = {
      GW_GITLAB_PRESETS_API_PATH:
        '/{project_id}/repository/files/{presets_path}{preset_filename}/raw?ref={branch}',
      GW_GITLAB_PROJECT_ID: 'test_projectid',
      GW_GITLAB_PRESETS_PATH: 'test_path',
      GW_GITLAB_BRANCH: 'test_branch',
      GW_INITIAL_PRESETS_FILENAME: 'test_presets_filename',
      GW_SCREEN_PRESETS_FILENAME: 'test_screenpresets_filename',
    };
    const api = createApi(
      'fakeURL',
      'fakeUrl',
      {
        username: 'Michael Jackson',
        token: '1223344',
        refresh_token: '33455214',
      },
      jest.fn(),
      'anotherFakeUrl',
      'fakeauthClientId',
    );

    await api.getInitialPresets(testConfig);
    expect(spy).toHaveBeenCalledWith(
      replaceTemplateKeys(
        testConfig.GW_GITLAB_PRESETS_API_PATH,
        testConfig.GW_GITLAB_PROJECT_ID,
        testConfig.GW_GITLAB_PRESETS_PATH,
        testConfig.GW_GITLAB_BRANCH,
        testConfig.GW_INITIAL_PRESETS_FILENAME,
      ),
    );
  });

  it('should call with the right params for getInitialPresets when gitlab is not configured', async () => {
    jest
      .spyOn(utils, 'createApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'get');
    const testConfig = {
      GW_INITIAL_PRESETS_FILENAME: 'test_presets_filename',
      GW_SCREEN_PRESETS_FILENAME: 'test_screenpresets_filename',
    };
    const api = createApi(
      'fakeURL',
      'fakeUrl',
      {
        username: 'Michael Jackson',
        token: '1223344',
        refresh_token: '33455214',
      },
      jest.fn(),
      'anotherFakeUrl',
      'fakeauthClientId',
    );

    await api.getInitialPresets(testConfig);
    expect(spy).toHaveBeenCalledWith(
      `./assets/${testConfig.GW_INITIAL_PRESETS_FILENAME}`,
    );
  });

  it('should call with the right params for getScreenPresets when gitlab is configured', async () => {
    jest
      .spyOn(utils, 'createApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'get');
    const testConfig = {
      GW_GITLAB_PRESETS_API_PATH:
        '/{project_id}/repository/files/{presets_path}{preset_filename}/raw?ref={branch}',
      GW_GITLAB_PROJECT_ID: 'test_projectid',
      GW_GITLAB_PRESETS_PATH: 'test_path',
      GW_GITLAB_BRANCH: 'test_branch',
      GW_INITIAL_PRESETS_FILENAME: 'test_presets_filename',
      GW_SCREEN_PRESETS_FILENAME: 'test_screenpresets_filename',
    };
    const api = createApi(
      'fakeURL',
      'fakeUrl',
      {
        username: 'Michael Jackson',
        token: '1223344',
        refresh_token: '33455214',
      },
      jest.fn(),
      'anotherFakeUrl',
      'fakeauthClientId',
    );

    await api.getScreenPresets(testConfig);
    expect(spy).toHaveBeenCalledWith(
      replaceTemplateKeys(
        testConfig.GW_GITLAB_PRESETS_API_PATH,
        testConfig.GW_GITLAB_PROJECT_ID,
        testConfig.GW_GITLAB_PRESETS_PATH,
        testConfig.GW_GITLAB_BRANCH,
        testConfig.GW_SCREEN_PRESETS_FILENAME,
      ),
    );
  });

  it('should call with the right params for getScreenPresets when gitlab is not configured', async () => {
    jest
      .spyOn(utils, 'createApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'get');
    const testConfig = {
      GW_INITIAL_PRESETS_FILENAME: 'test_presets_filename',
      GW_SCREEN_PRESETS_FILENAME: 'test_screenpresets_filename',
    };
    const api = createApi(
      'fakeURL',
      'fakeUrl',
      {
        username: 'Michael Jackson',
        token: '1223344',
        refresh_token: '33455214',
      },
      jest.fn(),
      'anotherFakeUrl',
      'fakeauthClientId',
    );

    await api.getScreenPresets(testConfig);
    expect(spy).toHaveBeenCalledWith(
      `./assets/${testConfig.GW_SCREEN_PRESETS_FILENAME}`,
    );
  });

  it('should call with the right params for getBeVersion', async () => {
    jest
      .spyOn(utils, 'createApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'get');
    const api = createApi(
      'fakeURL',
      'fakeUrl',
      {
        username: 'Michael Jackson',
        token: '1223344',
        refresh_token: '33455214',
      },
      jest.fn(),
      'anotherFakeUrl',
      'fakeauthClientId',
    );

    await api.getBeVersion();
    expect(spy).toHaveBeenCalledWith('/version');
  });
});
