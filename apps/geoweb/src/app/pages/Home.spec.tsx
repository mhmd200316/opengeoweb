/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import { fireEvent, render, waitFor } from '@testing-library/react';
import configureStore from 'redux-mock-store';

import { ThemeWrapper } from '@opengeoweb/theme';
import {
  AuthenticationProvider,
  getSessionStorageProvider,
} from '@opengeoweb/authentication';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { tafScreenPreset } from '@opengeoweb/taf';
import { uiActions } from '@opengeoweb/core';
import {
  actions as screenManagerActions,
  ScreenManagerPreset,
} from '@opengeoweb/screenmanager';
import { ApiProvider } from '@opengeoweb/api';
import HomePage, { HomePageLayout } from './Home';
import { ConfigType } from '../utils/loadConfig';
import { createFakeApi } from '../utils/api';
import defaultScreenPresets from '../../assets/screenPresets.json';

describe('Home', () => {
  const testConfig: ConfigType = {
    GW_GITLAB_PRESETS_BASE_URL: '',
    GW_GITLAB_PRESETS_API_PATH: '',
    GW_GITLAB_PROJECT_ID: '',
    GW_GITLAB_PRESETS_PATH: '',
    GW_GITLAB_BRANCH: '',
    GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
    GW_SCREEN_PRESETS_FILENAME: 'screenPresets.json',
    GW_FEATURE_MODULE_TAF: true,
    GW_FEATURE_MODULE_SPACE_WEATHER: true,
    GW_FEATURE_MODULE_SIGMET: true,
    GW_FEATURE_MODULE_AIRMET: true,
    GW_SW_BASE_URL: 'spaceweather',
    GW_SIGMET_BASE_URL: 'sigmet',
    GW_AIRMET_BASE_URL: 'airmet',
    GW_TAF_BASE_URL: 'taf',
    GW_PRESET_BACKEND_URL: 'presets',
  };

  const storedWindowLocation = window.location;

  afterAll(() => {
    window.location = storedWindowLocation;
  });

  const mockState = {
    syncronizationGroupStore: {
      sources: {
        byId: {},
        allIds: [],
      },
      groups: {
        byId: {},
        allIds: [],
      },
      viewState: {
        timeslider: {
          groups: [],
          sourcesById: [],
        },
        zoompane: {
          groups: [],
          sourcesById: [],
        },
        level: {
          groups: [],
          sourcesById: [],
        },
      },
    },
  };

  it('should not render homepage when no config is loading', async () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { queryByTestId } = render(
      <ThemeWrapper>
        <ApiProvider createApi={createFakeApi}>
          <Provider store={store}>
            <HomePageLayout config={null} auth={null} onSetAuth={jest.fn()} />
          </Provider>
        </ApiProvider>
      </ThemeWrapper>,
    );
    await waitFor(() => expect(queryByTestId('appLayout')).toBeFalsy());
  });

  it('should not render homepage screenPresets are loading', async () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { queryByTestId } = render(
      <ThemeWrapper>
        <ApiProvider createApi={createFakeApi}>
          <Provider store={store}>
            <HomePageLayout
              config={{
                GW_INITIAL_PRESETS_FILENAME: 'initial-presets-filename',
                GW_SCREEN_PRESETS_FILENAME: 'initial-screenpresets-filename',
              }}
              auth={null}
              onSetAuth={jest.fn()}
            />
          </Provider>
        </ApiProvider>
      </ThemeWrapper>,
    );
    // loading config and screenPresets
    await waitFor(() => {
      expect(queryByTestId('appLayout')).toBeFalsy();
    });

    // config and screenPresets loaded
    await waitFor(() => {
      expect(queryByTestId('appLayout')).toBeTruthy();
    });
  });

  it('should render successfully with correct map', async () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { queryByTestId, baseElement } = render(
      <ThemeWrapper>
        <Provider store={store}>
          <HomePage />
        </Provider>
      </ThemeWrapper>,
    );

    expect(baseElement).toBeTruthy();
    expect(queryByTestId('ScreenManagerPage')).toBeFalsy();
  });

  it('should render successfully with default empty preset', async () => {
    const emptyMapPreset = defaultScreenPresets.find(
      (preset) => preset.title === 'Empty Map',
    );
    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { queryByTestId } = render(
      <ThemeWrapper>
        <Provider store={store}>
          <HomePage config={testConfig} />
        </Provider>
      </ThemeWrapper>,
    );
    await waitFor(() => {
      expect(queryByTestId('ScreenManagerPage')).toBeTruthy();
    });

    const expectedAction = screenManagerActions.changePreset({
      screenManagerPreset: emptyMapPreset as ScreenManagerPreset,
    });
    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should dispatch an action to open Sync Groups', async () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { queryByTestId, getByText } = render(
      <ThemeWrapper>
        <Provider store={store}>
          <HomePage config={testConfig} />
        </Provider>
      </ThemeWrapper>,
    );

    await waitFor(() => expect(queryByTestId('menuButton')).toBeTruthy());
    fireEvent.click(queryByTestId('menuButton'));
    fireEvent.click(getByText('Sync Groups'));

    const expectedAction = uiActions.setToggleOpenDialog({
      type: 'syncGroups',
      setOpen: true,
    });

    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should dispatch an action to open TAF', async () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { queryByTestId, queryByText } = render(
      <ThemeWrapper>
        <Provider store={store}>
          <HomePage config={testConfig} />
        </Provider>
      </ThemeWrapper>,
    );

    const expectedAction = screenManagerActions.changePreset({
      screenManagerPreset: tafScreenPreset,
    });

    await waitFor(() => expect(queryByTestId('menuButton')).toBeTruthy());
    fireEvent.click(queryByTestId('menuButton'));
    fireEvent.click(queryByText('TAF'));
    expect(store.getActions()).toContainEqual(expectedAction);
    // menu should be closed
    await waitFor(() => {
      expect(queryByText('TAF')).toBeFalsy();
    });
  });

  it('should open and close all modules', async () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    const { getByTestId, getByText, queryByText, queryByTestId } = render(
      <Router>
        <AuthenticationProvider>
          <ThemeWrapper>
            <Provider store={store}>
              <HomePage config={testConfig} />
            </Provider>
          </ThemeWrapper>
        </AuthenticationProvider>
      </Router>,
    );

    await waitFor(() => expect(queryByTestId('menuButton')).toBeTruthy());
    fireEvent.click(getByTestId('menuButton'));
    fireEvent.click(getByText('AIRMET'));
    await waitFor(() => {
      expect(getByText('AIRMET list')).toBeTruthy();
    });

    fireEvent.click(getByTestId('menuButton'));
    fireEvent.click(getByText('SIGMET'));
    await waitFor(() => {
      expect(getByText('SIGMET list')).toBeTruthy();
    });

    fireEvent.click(getByTestId('menuButton'));
    fireEvent.click(getByText('Space Weather Forecast'));
    await waitFor(() => {
      expect(getByTestId('SpaceWeatherModule')).toBeTruthy();
    });

    // closing the modules
    fireEvent.click(
      getByTestId('SpaceWeatherModule').querySelector('[data-testid=closeBtn]'),
    );
    await waitFor(() => {
      expect(queryByText('Space Weather Forecast')).toBeFalsy();
    });

    fireEvent.click(
      getByTestId('SigmetModule').querySelector('[data-testid=closeBtn]'),
    );
    await waitFor(() => {
      expect(queryByText('SIGMET list')).toBeFalsy();
    });

    fireEvent.click(
      getByTestId('AirmetModule').querySelector('[data-testid=closeBtn]'),
    );
    await waitFor(() => {
      expect(queryByText('AIRMET list')).toBeFalsy();
    });
  });

  it('should show the mappresets button', async () => {
    const mockStore = configureStore();
    const store = mockStore({
      ...mockState,
      screenManagerStore: {
        id: 'screenConfigA',
        title: 'Radar',
        views: {
          byId: {
            screenA: {
              title: 'Precipitation Radar',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [
                    {
                      service:
                        'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserver?dataset=RADAR',
                      name: 'precipitation',
                      format: 'image/png',
                      enabled: true,
                      style: 'radar/nearest',
                      id: 'test_layerid_active',
                      layerType: 'mapLayer',
                    },
                  ],
                  activeLayerId: 'test_layerid_active',
                },
                syncGroupsIds: ['Area_A', 'Time_A'],
              },
            },
          },
          allIds: ['screenA'],
        },
        mosaicNode: 'screenA',
      },
      ui: {
        order: ['layerManager'],
        dialogs: {
          layerManager: {
            activeMapId: 'screenA',
            isOpen: true,
            type: 'layerManager',
            source: 'app',
          },
        },
      },
      viewPresets: {
        entities: {},
        ids: [],
      },
    });
    const { findByTestId } = render(
      <ThemeWrapper>
        <Provider store={store}>
          <HomePage config={testConfig} />
        </Provider>
      </ThemeWrapper>,
    );
    expect(await findByTestId('layerManagerButton')).toBeTruthy();
    expect(await findByTestId('layerManagerWindow')).toBeTruthy();
    expect(await findByTestId('mappresets-menubutton')).toBeTruthy();
  });

  it('should redirect to login if authenticated and not logged in', async () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider: getSessionStorageProvider(),
    };

    // mock session storage
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('true');

    const { findByText, queryByText } = render(
      <AuthenticationProvider value={props}>
        <ThemeWrapper>
          <Provider store={store}>
            <Router>
              <Switch>
                <Route exact path="/">
                  <HomePage config={testConfig} />
                </Route>
                <Route path="/login">Redirected to /login</Route>
              </Switch>
            </Router>
          </Provider>
        </ThemeWrapper>
      </AuthenticationProvider>,
    );
    expect(await findByText('Redirected to /login')).toBeTruthy();

    expect(queryByText('GeoWeb')).toBeFalsy();
  });

  it('should not redirect to login if logged in', async () => {
    const mockStore = configureStore();
    const store = mockStore(mockState);
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL('http://localhost/');

    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: true,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider: getSessionStorageProvider(),
    };

    // mock session storage
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('true');

    const { findByText, queryByText } = render(
      <AuthenticationProvider value={props}>
        <ThemeWrapper>
          <Provider store={store}>
            <Router>
              <Switch>
                <Route exact path="/">
                  <HomePage config={testConfig} />
                </Route>
                <Route path="/login">Redirected to /login</Route>
              </Switch>
            </Router>
          </Provider>
        </ThemeWrapper>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(queryByText('Redirected to /login')).toBeFalsy();
    });

    expect(await findByText('GeoWeb')).toBeTruthy();
  });
});
