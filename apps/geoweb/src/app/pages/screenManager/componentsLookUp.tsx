/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  componentsLookUp as coreComponentsLookUp,
  ComponentsLookUpPayload as CoreComponentsLookUpPayload,
} from '@opengeoweb/core';
import {
  componentsLookUp as timeseriesComponentsLookUp,
  ComponentsLookUpPayload as TimeseriesComponentsLookUpPayload,
} from '@opengeoweb/timeseries';
import { Credentials } from '@opengeoweb/api';

import {
  ComponentsLookUp as capComponentsLookup,
  ComponentsLookUpPayload as CapComponentsLookUpPayload,
} from '@opengeoweb/cap';
import {
  componentsLookUp as tafComponentsLookUp,
  ComponentsLookUpPayload as TafComponentsLookUpPayload,
} from '@opengeoweb/taf';
import { ComponentsLookUpWrapper } from '@opengeoweb/screenmanager';

import capWarningPresets from '../../../assets/capWarningPresets.json';
import timeSeriesPresetLocations from '../../../assets/timeSeriesPresetLocations.json';
import { ConfigType } from '../../utils/loadConfig';

export type ComponentsLookUpPayload =
  | CoreComponentsLookUpPayload
  | TimeseriesComponentsLookUpPayload
  | TafComponentsLookUpPayload
  | CapComponentsLookUpPayload;

/**
 * The lookup table is for registering your own components with the screenmanager.
 * @param payload
 * @returns
 */
export const componentsLookUp = (
  payload: ComponentsLookUpPayload,
  config?: ConfigType,
  auth?: Credentials,
  onSetAuth?: (credentias: Credentials) => void,
): React.ReactElement => {
  const coreComponent = coreComponentsLookUp(
    payload as CoreComponentsLookUpPayload,
  );
  const timeseriesComponent = timeseriesComponentsLookUp({
    timeSeriesPresetLocations,
    ...payload,
  } as TimeseriesComponentsLookUpPayload);
  const tafComponent = tafComponentsLookUp(
    payload as TafComponentsLookUpPayload,
    {
      config: {
        baseUrl: config?.GW_TAF_BASE_URL,
        appUrl: config?.GW_APP_URL,
        authTokenUrl: config?.GW_AUTH_TOKEN_URL,
        authClientId: config?.GW_AUTH_CLIENT_ID,
      },
      auth,
      onSetAuth,
    },
    ComponentsLookUpWrapper,
  );

  const capComponent = capComponentsLookup({
    capWarningPresets,
    ...payload,
  } as CapComponentsLookUpPayload);

  return coreComponent || timeseriesComponent || tafComponent || capComponent;
};
