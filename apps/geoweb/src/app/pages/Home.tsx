/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Redirect } from 'react-router-dom';
import { Box } from '@mui/material';
import {
  ApiProvider,
  Credentials,
  useApi,
  useApiContext,
} from '@opengeoweb/api';
import {
  useAuthenticationContext,
  getSessionStorageProvider,
} from '@opengeoweb/authentication';
import {
  LayerManagerConnect,
  MultiMapDimensionSelectConnect,
  SyncGroupViewerConnect,
  InitialAppPreset,
  getInitialAppPresets,
} from '@opengeoweb/core';
import {
  InitSyncGroups,
  ScreenManagerViewConnect,
  getInitialScreenPreset,
  ScreenManagerPreset,
} from '@opengeoweb/screenmanager';
import { TimeSeriesManagerConnect } from '@opengeoweb/timeseries';
import { AppLayout, ErrorBoundary } from '@opengeoweb/shared';

import { MapPresetsDialogModule } from '@opengeoweb/presets';
import Header from '../components/Header';
import {
  componentsLookUp,
  ComponentsLookUpPayload,
} from './screenManager/componentsLookUp';
import { AppApi, createApi } from '../utils/api';
import { ConfigType } from '../utils/loadConfig';
import SpaceWeatherModule from '../components/SpaceWeatherModule';
import SigmetModule from '../components/SigmetModule';
import AirmetModule from '../components/AirmetModule';
import defaultInitialPresets from '../../assets/initialPresets.json';
import defaultScreenPresets from '../../assets/screenPresets.json';

const styles = {
  content: {
    gridArea: 'content',
    position: 'absolute',
    margin: 0,
    padding: 0,
    display: 'block',
    height: '100%',
    width: '100%',
    overflowY: 'auto',
    zIndex: 3,
  },
};

interface HomePageLayoutProps {
  config: ConfigType;
  auth: Credentials;
  onSetAuth: (credentias: Credentials) => void;
}

export const HomePageLayout: React.FC<HomePageLayoutProps> = ({
  config,
  auth,
  onSetAuth,
}) => {
  const { api } = useApiContext<AppApi>();
  const { result: screenPresetsResult, isLoading: isScreenPresetsLoading } =
    useApi(api.getScreenPresets, config);
  const initialScreenPreset = getInitialScreenPreset(
    screenPresetsResult,
    defaultScreenPresets as ScreenManagerPreset[],
  );
  const initialPresetsRes = useApi(api.getInitialPresets, config).result;

  const { baseLayers, services, baseServices } = getInitialAppPresets(
    initialPresetsRes,
    defaultInitialPresets as InitialAppPreset,
  );

  const [topPanel, setTopPanel] = React.useState(null);
  const [panels, setPanel] = React.useState({
    spaceweather: false,
    sigmet: false,
    airmet: false,
  });

  const onOpenPanel = (panel: string): void => {
    setPanel({
      ...panels,
      [panel]: true,
    });
    setTopPanel(panel);
  };
  const onClosePanel = (panel: string): void => {
    setPanel({
      ...panels,
      [panel]: false,
    });
  };

  const getOrder = (panel: string): number => (topPanel === panel ? 2 : 1);

  const onComponentsLookUp = (
    payload: ComponentsLookUpPayload,
  ): React.ReactElement => {
    return componentsLookUp(payload, config, auth, onSetAuth);
  };

  return config && !isScreenPresetsLoading ? (
    <AppLayout
      header={
        <Header
          showSpaceWeather={(): void => onOpenPanel('spaceweather')}
          showSigmet={(): void => onOpenPanel('sigmet')}
          showAirmet={(): void => onOpenPanel('airmet')}
          config={config}
        />
      }
    >
      <ErrorBoundary>
        {initialScreenPreset && (
          <InitSyncGroups screenPresets={initialScreenPreset} />
        )}
        <SyncGroupViewerConnect />
        <TimeSeriesManagerConnect />
        <LayerManagerConnect
          bounds="parent"
          showTitle
          preloadedAvailableBaseLayers={baseLayers}
          preloadedBaseServices={baseServices || services}
          preloadedMapServices={services}
          layerSelect={config?.GW_FEATURE_LAYER_SELECT}
          leftComponent={
            config?.GW_PRESET_BACKEND_URL ? (
              <MapPresetsDialogModule
                config={{
                  baseUrl: config?.GW_PRESET_BACKEND_URL,
                  appUrl: config?.GW_APP_URL,
                  authTokenUrl: config?.GW_AUTH_TOKEN_URL,
                  authClientId: config?.GW_AUTH_CLIENT_ID,
                }}
                auth={auth}
                onSetAuth={onSetAuth}
              />
            ) : null
          }
        />
        <MultiMapDimensionSelectConnect />
        <Box
          component="div"
          sx={styles.content}
          data-testid="ScreenManagerPage"
        >
          <ScreenManagerViewConnect componentsLookUp={onComponentsLookUp} />
        </Box>
      </ErrorBoundary>
      {panels.spaceweather && (
        <SpaceWeatherModule
          handleClose={(): void => onClosePanel('spaceweather')}
          order={getOrder('spaceweather')}
          config={config}
        />
      )}
      {panels.sigmet && (
        <SigmetModule
          handleClose={(): void => onClosePanel('sigmet')}
          order={getOrder('sigmet')}
          config={config}
        />
      )}
      {panels.airmet && (
        <AirmetModule
          handleClose={(): void => onClosePanel('airmet')}
          order={getOrder('airmet')}
          config={config}
        />
      )}
    </AppLayout>
  ) : null;
};

interface HomePageProps {
  config?: ConfigType;
}

const HomePage: React.FC<HomePageProps> = ({ config }: HomePageProps) => {
  const { auth, onSetAuth, isLoggedIn } = useAuthenticationContext();
  const sessionStorageProvider = getSessionStorageProvider();
  const hasAuthenticated =
    sessionStorageProvider.getHasAuthenticated() === 'true';

  if (config && hasAuthenticated && !auth && !isLoggedIn) {
    return (
      <Redirect
        to={{
          pathname: '/login',
        }}
      />
    );
  }

  return config ? (
    <ApiProvider
      baseURL={config.GW_GITLAB_PRESETS_BASE_URL}
      appURL={config.GW_APP_URL}
      auth={auth}
      onSetAuth={onSetAuth}
      createApi={createApi}
      authTokenUrl={config.GW_AUTH_TOKEN_URL}
      authClientId={config.GW_AUTH_CLIENT_ID}
    >
      <HomePageLayout config={config} auth={auth} onSetAuth={onSetAuth} />
    </ApiProvider>
  ) : null;
};

export default HomePage;
