/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import './style.css';

const viewPortHeight = 'calc(100vh - 100px)';
const customViewports = {
  mobile: {
    name: 'Mobile',
    styles: {
      width: '320px',
      height: viewPortHeight,
    },
  },
  tablet: {
    name: 'Tablet',
    styles: {
      width: '768px',
      height: viewPortHeight,
    },
  },
  desktop: {
    name: 'Desktop',
    styles: {
      width: '1024px',
      height: viewPortHeight,
    },
  },
  desktopHD: {
    name: 'DesktopHD',
    styles: {
      width: '1440px',
      height: viewPortHeight,
    },
  },
  desktopHDWide: {
    name: 'DesktopHDWide',
    styles: {
      width: '1920px',
      height: viewPortHeight,
    },
  },
};

export const parameters = {
  layout: 'fullscreen',
  options: {
    storySort: (a, b) => a[1].id.localeCompare(b[1].id),
  },
  viewport: {
    viewports: customViewports,
  },
};
