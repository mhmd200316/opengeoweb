const path = require('path');
const toPath = (filePath) => path.join(process.cwd(), filePath);

module.exports = {
  reactOptions: {
    fastRefresh: true,
    strictMode: true,
  },
  stories: [],
  addons: [
    {
      name: '@storybook/addon-essentials',
      options: {
        actions: false,
        backgrounds: false,
        controls: false,
        docs: false,
      },
    },
  ],
  features: {
    postcss: false,
  },
  webpackFinal: async (config, { configType }) => {
    config.resolve.alias['@opengeoweb/api'] = path.resolve(
      __dirname,
      '../libs/api/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/authentication'] = path.resolve(
      __dirname,
      '../libs/authentication/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/shared'] = path.resolve(
      __dirname,
      '../libs/shared/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/form-fields'] = path.resolve(
      __dirname,
      '../libs/form-fields/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/theme'] = path.resolve(
      __dirname,
      '../libs/theme/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/core'] = path.resolve(
      __dirname,
      '../libs/core/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/webmap'] = path.resolve(
      __dirname,
      '../libs/webmap/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/timeseries'] = path.resolve(
      __dirname,
      '../libs/timeseries/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/taf'] = path.resolve(
      __dirname,
      '../libs/taf/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/theme'] = path.resolve(
      __dirname,
      '../libs/theme/src/index.ts',
    );
    config.resolve.alias['@opengeoweb/cap'] = path.resolve(
      __dirname,
      '../libs/cap/src/index.ts',
    );
    return {
      ...config,
      resolve: {
        ...config.resolve,
        alias: {
          ...config.resolve.alias,
          '@emotion/core': toPath('node_modules/@emotion/react'),
          'emotion-theming': toPath('node_modules/@emotion/react'),
        },
      },
    };
  },
};
